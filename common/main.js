(global.webpackJsonp = global.webpackJsonp || []).push([
    ["common/main"], {
        "308a": function (t, e, o) {},
        5017: function (t, e, o) {
            o.r(e);
            var a = o("9367");
            for (var n in a) "default" !== n && function (t) {
                o.d(e, t, function () {
                    return a[t];
                });
            }(n);
            o("5efc");
            var u = o("2877"),
                r = Object(u.a)(a.default, void 0, void 0, !1, null, null, null);
            e.default = r.exports;
        },
        "5efc": function (t, e, o) {
            var a = o("308a");
            o.n(a).a;
        },
        9367: function (t, e, o) {
            o.r(e);
            var a = o("d1be"),
                n = o.n(a);
            for (var u in a) "default" !== u && function (t) {
                o.d(e, t, function () {
                    return a[t];
                });
            }(u);
            e.default = n.a;
        },
        bf17: function (t, e, p) {
            (function (t) {
                p("020c"), p("921b");
                var e = n(p("66fd")),
                    o = n(p("5017")),
                    a = n(p("ac78"));

                function n(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                e.default.component("auth", function () {
                    return Promise.all([p.e("common/vendor"), p.e("components/auth/auth")]).then(p.bind(null, "82fa"));
                });
                e.default.component("bindPhone", function () {
                    return Promise.all([p.e("common/vendor"), p.e("components/bindPhone/bindPhone")]).then(p.bind(null, "651a"));
                });
                e.default.component("myfooter", function () {
                    return p.e("components/myfooter/myfooter").then(p.bind(null, "5d66"));
                });
                e.default.component("copyright", function () {
                    return p.e("components/copyright/copyright").then(p.bind(null, "1c66"));
                }), e.default.config.productionTip = !1, e.default.prototype.$store = a.default;
                var u = p("3814");
                if ("wn" == u.platform) e.default.prototype.$baseurl = u.url, e.default.prototype.$imgurl = "https://" + u.url.match(/[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+\.?/)[0] + "/image/static/",
                    e.default.prototype.$host = "https://" + u.url.match(/[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+\.?/)[0],
                    e.default.prototype.$diyHost = "https://" + u.url.match(/[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+\.?/)[0];
                else {
                    var r = "https://" + u.url.match(/[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+\.?/)[0];
                    e.default.prototype.$imgurl = r + "/addons/worldidc_cloud/core/public/image/static/",
                        e.default.prototype.$host = r + "/addons/worldidc_cloud/core/public/index.php",
                        e.default.prototype.$baseurl = r + "/addons/worldidc_cloud/core/public/index.php/api/Wxapps/",
                        e.default.prototype.$diyHost = r + "/addons/worldidc_cloud/core/public/";
                }
                e.default.prototype.$platform = u.platform, e.default.prototype.$uniacid = u.uniacid;
                var l = p("2da0");
                e.default.prototype._baseMin = l.baseMin, e.default.prototype._showwxpay = l.showwxpay,
                    e.default.prototype._showalipay = l.showalipay, e.default.prototype._alih5pay = l.alih5pay,
                    e.default.prototype._wxh5pay = l.wxh5pay, e.default.prototype._baidupay = l.baidupay,
                    e.default.prototype._toutiaopay = l.toutiaopay, e.default.prototype._qqpay = l.qqpay,
                    e.default.prototype._checkBindPhone = l.checkBindPhone, e.default.prototype._redirectto = l.redirectto,
                    e.default.prototype._getSuperUserInfo = l.getSuperUserInfo, e.default.prototype._givepscore = l.givepscore,
                    o.default.mpType = "app", t(new e.default(function (n) {
                        for (var t = 1; t < arguments.length; t++) {
                            var u = null != arguments[t] ? arguments[t] : {},
                                e = Object.keys(u);
                            "function" == typeof Object.getOwnPropertySymbols && (e = e.concat(Object.getOwnPropertySymbols(u).filter(function (t) {
                                return Object.getOwnPropertyDescriptor(u, t).enumerable;
                            }))), e.forEach(function (t) {
                                var e, o, a;
                                e = n, a = u[o = t], o in e ? Object.defineProperty(e, o, {
                                    value: a,
                                    enumerable: !0,
                                    configurable: !0,
                                    writable: !0
                                }) : e[o] = a;
                            });
                        }
                        return n;
                    }({
                        store: a.default
                    }, o.default))).$mount();
            }).call(this, p("543d").createApp);
        },
        d1be: function (t, a, n) {
            (function (e) {
                var t;
                Object.defineProperty(a, "__esModule", {
                    value: !0
                }), a.default = void 0, (t = n("66fd")) && t.__esModule, n("3814");
                var o = {
                    onLaunch: function () {
                        e.setStorageSync("source", 1), e.getSystemInfo({
                            success: function (t) {
                                e.setStorageSync("systemInfo", t);
                            }
                        });
                    },
                    onShow: function () {
                        console.log("App Show"), e.request({
                            url: this.$baseurl + "/doPageSubscribe",
                            data: {
                                uniacid: this.$uniacid
                            },
                            success: function (t) {
                                e.setStorageSync("subscribe", t.data.data);
                            }
                        });
                    },
                    onHide: function () {
                      //清除开屏广告的的hide缓存标识
                      wx.removeStorageSync('hide');
                        console.log("App Hide");
                    }
                };
                a.default = o;
            }).call(this, n("543d").default);
        }
    },
    [
        ["bf17", "common/runtime", "common/vendor"]
    ]
]);