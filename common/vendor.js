var _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
    return typeof e;
  } : function (e) {
    return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
  },
  _siteinfo = require("../siteinfo.js"),
  _siteinfo2 = _interopRequireDefault(_siteinfo);

function _interopRequireDefault(e) {
  return e && e.__esModule ? e : {
    default: e
  };
}
var domain = _siteinfo2.default.siteroot.match(/[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+\.?/);
(global.webpackJsonp = global.webpackJsonp || []).push([
  ["common/vendor"], {
    "020c": function (e, l, a) {},
    "0671": function (e, l, a) {
      Object.defineProperty(l, "__esModule", {
        value: !0
      }), l.default = void 0;
      var t = [
        [{
          label: "北京市",
          value: "1101"
        }],
        [{
          label: "天津市",
          value: "1201"
        }],
        [{
          label: "石家庄市",
          value: "1301"
        }, {
          label: "唐山市",
          value: "1302"
        }, {
          label: "秦皇岛市",
          value: "1303"
        }, {
          label: "邯郸市",
          value: "1304"
        }, {
          label: "邢台市",
          value: "1305"
        }, {
          label: "保定市",
          value: "1306"
        }, {
          label: "张家口市",
          value: "1307"
        }, {
          label: "承德市",
          value: "1308"
        }, {
          label: "沧州市",
          value: "1309"
        }, {
          label: "廊坊市",
          value: "1310"
        }, {
          label: "衡水市",
          value: "1311"
        }],
        [{
          label: "太原市",
          value: "1401"
        }, {
          label: "大同市",
          value: "1402"
        }, {
          label: "阳泉市",
          value: "1403"
        }, {
          label: "长治市",
          value: "1404"
        }, {
          label: "晋城市",
          value: "1405"
        }, {
          label: "朔州市",
          value: "1406"
        }, {
          label: "晋中市",
          value: "1407"
        }, {
          label: "运城市",
          value: "1408"
        }, {
          label: "忻州市",
          value: "1409"
        }, {
          label: "临汾市",
          value: "1410"
        }, {
          label: "吕梁市",
          value: "1411"
        }],
        [{
          label: "呼和浩特市",
          value: "1501"
        }, {
          label: "包头市",
          value: "1502"
        }, {
          label: "乌海市",
          value: "1503"
        }, {
          label: "赤峰市",
          value: "1504"
        }, {
          label: "通辽市",
          value: "1505"
        }, {
          label: "鄂尔多斯市",
          value: "1506"
        }, {
          label: "呼伦贝尔市",
          value: "1507"
        }, {
          label: "巴彦淖尔市",
          value: "1508"
        }, {
          label: "乌兰察布市",
          value: "1509"
        }, {
          label: "兴安盟",
          value: "1522"
        }, {
          label: "锡林郭勒盟",
          value: "1525"
        }, {
          label: "阿拉善盟",
          value: "1529"
        }],
        [{
          label: "沈阳市",
          value: "2101"
        }, {
          label: "大连市",
          value: "2102"
        }, {
          label: "鞍山市",
          value: "2103"
        }, {
          label: "抚顺市",
          value: "2104"
        }, {
          label: "本溪市",
          value: "2105"
        }, {
          label: "丹东市",
          value: "2106"
        }, {
          label: "锦州市",
          value: "2107"
        }, {
          label: "营口市",
          value: "2108"
        }, {
          label: "阜新市",
          value: "2109"
        }, {
          label: "辽阳市",
          value: "2110"
        }, {
          label: "盘锦市",
          value: "2111"
        }, {
          label: "铁岭市",
          value: "2112"
        }, {
          label: "朝阳市",
          value: "2113"
        }, {
          label: "葫芦岛市",
          value: "2114"
        }],
        [{
          label: "长春市",
          value: "2201"
        }, {
          label: "吉林市",
          value: "2202"
        }, {
          label: "四平市",
          value: "2203"
        }, {
          label: "辽源市",
          value: "2204"
        }, {
          label: "通化市",
          value: "2205"
        }, {
          label: "白山市",
          value: "2206"
        }, {
          label: "松原市",
          value: "2207"
        }, {
          label: "白城市",
          value: "2208"
        }, {
          label: "延边朝鲜族自治州",
          value: "2224"
        }],
        [{
          label: "哈尔滨市",
          value: "2301"
        }, {
          label: "齐齐哈尔市",
          value: "2302"
        }, {
          label: "鸡西市",
          value: "2303"
        }, {
          label: "鹤岗市",
          value: "2304"
        }, {
          label: "双鸭山市",
          value: "2305"
        }, {
          label: "大庆市",
          value: "2306"
        }, {
          label: "伊春市",
          value: "2307"
        }, {
          label: "佳木斯市",
          value: "2308"
        }, {
          label: "七台河市",
          value: "2309"
        }, {
          label: "牡丹江市",
          value: "2310"
        }, {
          label: "黑河市",
          value: "2311"
        }, {
          label: "绥化市",
          value: "2312"
        }, {
          label: "大兴安岭地区",
          value: "2327"
        }],
        [{
          label: "上海市",
          value: "3101"
        }],
        [{
          label: "南京市",
          value: "3201"
        }, {
          label: "无锡市",
          value: "3202"
        }, {
          label: "徐州市",
          value: "3203"
        }, {
          label: "常州市",
          value: "3204"
        }, {
          label: "苏州市",
          value: "3205"
        }, {
          label: "南通市",
          value: "3206"
        }, {
          label: "连云港市",
          value: "3207"
        }, {
          label: "淮安市",
          value: "3208"
        }, {
          label: "盐城市",
          value: "3209"
        }, {
          label: "扬州市",
          value: "3210"
        }, {
          label: "镇江市",
          value: "3211"
        }, {
          label: "泰州市",
          value: "3212"
        }, {
          label: "宿迁市",
          value: "3213"
        }],
        [{
          label: "杭州市",
          value: "3301"
        }, {
          label: "宁波市",
          value: "3302"
        }, {
          label: "温州市",
          value: "3303"
        }, {
          label: "嘉兴市",
          value: "3304"
        }, {
          label: "湖州市",
          value: "3305"
        }, {
          label: "绍兴市",
          value: "3306"
        }, {
          label: "金华市",
          value: "3307"
        }, {
          label: "衢州市",
          value: "3308"
        }, {
          label: "舟山市",
          value: "3309"
        }, {
          label: "台州市",
          value: "3310"
        }, {
          label: "丽水市",
          value: "3311"
        }],
        [{
          label: "合肥市",
          value: "3401"
        }, {
          label: "芜湖市",
          value: "3402"
        }, {
          label: "蚌埠市",
          value: "3403"
        }, {
          label: "淮南市",
          value: "3404"
        }, {
          label: "马鞍山市",
          value: "3405"
        }, {
          label: "淮北市",
          value: "3406"
        }, {
          label: "铜陵市",
          value: "3407"
        }, {
          label: "安庆市",
          value: "3408"
        }, {
          label: "黄山市",
          value: "3410"
        }, {
          label: "滁州市",
          value: "3411"
        }, {
          label: "阜阳市",
          value: "3412"
        }, {
          label: "宿州市",
          value: "3413"
        }, {
          label: "六安市",
          value: "3415"
        }, {
          label: "亳州市",
          value: "3416"
        }, {
          label: "池州市",
          value: "3417"
        }, {
          label: "宣城市",
          value: "3418"
        }],
        [{
          label: "福州市",
          value: "3501"
        }, {
          label: "厦门市",
          value: "3502"
        }, {
          label: "莆田市",
          value: "3503"
        }, {
          label: "三明市",
          value: "3504"
        }, {
          label: "泉州市",
          value: "3505"
        }, {
          label: "漳州市",
          value: "3506"
        }, {
          label: "南平市",
          value: "3507"
        }, {
          label: "龙岩市",
          value: "3508"
        }, {
          label: "宁德市",
          value: "3509"
        }],
        [{
          label: "南昌市",
          value: "3601"
        }, {
          label: "景德镇市",
          value: "3602"
        }, {
          label: "萍乡市",
          value: "3603"
        }, {
          label: "九江市",
          value: "3604"
        }, {
          label: "新余市",
          value: "3605"
        }, {
          label: "鹰潭市",
          value: "3606"
        }, {
          label: "赣州市",
          value: "3607"
        }, {
          label: "吉安市",
          value: "3608"
        }, {
          label: "宜春市",
          value: "3609"
        }, {
          label: "抚州市",
          value: "3610"
        }, {
          label: "上饶市",
          value: "3611"
        }],
        [{
          label: "济南市",
          value: "3701"
        }, {
          label: "青岛市",
          value: "3702"
        }, {
          label: "淄博市",
          value: "3703"
        }, {
          label: "枣庄市",
          value: "3704"
        }, {
          label: "东营市",
          value: "3705"
        }, {
          label: "烟台市",
          value: "3706"
        }, {
          label: "潍坊市",
          value: "3707"
        }, {
          label: "济宁市",
          value: "3708"
        }, {
          label: "泰安市",
          value: "3709"
        }, {
          label: "威海市",
          value: "3710"
        }, {
          label: "日照市",
          value: "3711"
        }, {
          label: "莱芜市",
          value: "3712"
        }, {
          label: "临沂市",
          value: "3713"
        }, {
          label: "德州市",
          value: "3714"
        }, {
          label: "聊城市",
          value: "3715"
        }, {
          label: "滨州市",
          value: "3716"
        }, {
          label: "菏泽市",
          value: "3717"
        }],
        [{
          label: "郑州市",
          value: "4101"
        }, {
          label: "开封市",
          value: "4102"
        }, {
          label: "洛阳市",
          value: "4103"
        }, {
          label: "平顶山市",
          value: "4104"
        }, {
          label: "安阳市",
          value: "4105"
        }, {
          label: "鹤壁市",
          value: "4106"
        }, {
          label: "新乡市",
          value: "4107"
        }, {
          label: "焦作市",
          value: "4108"
        }, {
          label: "濮阳市",
          value: "4109"
        }, {
          label: "许昌市",
          value: "4110"
        }, {
          label: "漯河市",
          value: "4111"
        }, {
          label: "三门峡市",
          value: "4112"
        }, {
          label: "南阳市",
          value: "4113"
        }, {
          label: "商丘市",
          value: "4114"
        }, {
          label: "信阳市",
          value: "4115"
        }, {
          label: "周口市",
          value: "4116"
        }, {
          label: "驻马店市",
          value: "4117"
        }, {
          label: "省直辖县级行政区划",
          value: "4190"
        }],
        [{
          label: "武汉市",
          value: "4201"
        }, {
          label: "黄石市",
          value: "4202"
        }, {
          label: "十堰市",
          value: "4203"
        }, {
          label: "宜昌市",
          value: "4205"
        }, {
          label: "襄阳市",
          value: "4206"
        }, {
          label: "鄂州市",
          value: "4207"
        }, {
          label: "荆门市",
          value: "4208"
        }, {
          label: "孝感市",
          value: "4209"
        }, {
          label: "荆州市",
          value: "4210"
        }, {
          label: "黄冈市",
          value: "4211"
        }, {
          label: "咸宁市",
          value: "4212"
        }, {
          label: "随州市",
          value: "4213"
        }, {
          label: "恩施土家族苗族自治州",
          value: "4228"
        }, {
          label: "省直辖县级行政区划",
          value: "4290"
        }],
        [{
          label: "长沙市",
          value: "4301"
        }, {
          label: "株洲市",
          value: "4302"
        }, {
          label: "湘潭市",
          value: "4303"
        }, {
          label: "衡阳市",
          value: "4304"
        }, {
          label: "邵阳市",
          value: "4305"
        }, {
          label: "岳阳市",
          value: "4306"
        }, {
          label: "常德市",
          value: "4307"
        }, {
          label: "张家界市",
          value: "4308"
        }, {
          label: "益阳市",
          value: "4309"
        }, {
          label: "郴州市",
          value: "4310"
        }, {
          label: "永州市",
          value: "4311"
        }, {
          label: "怀化市",
          value: "4312"
        }, {
          label: "娄底市",
          value: "4313"
        }, {
          label: "湘西土家族苗族自治州",
          value: "4331"
        }],
        [{
          label: "广州市",
          value: "4401"
        }, {
          label: "韶关市",
          value: "4402"
        }, {
          label: "深圳市",
          value: "4403"
        }, {
          label: "珠海市",
          value: "4404"
        }, {
          label: "汕头市",
          value: "4405"
        }, {
          label: "佛山市",
          value: "4406"
        }, {
          label: "江门市",
          value: "4407"
        }, {
          label: "湛江市",
          value: "4408"
        }, {
          label: "茂名市",
          value: "4409"
        }, {
          label: "肇庆市",
          value: "4412"
        }, {
          label: "惠州市",
          value: "4413"
        }, {
          label: "梅州市",
          value: "4414"
        }, {
          label: "汕尾市",
          value: "4415"
        }, {
          label: "河源市",
          value: "4416"
        }, {
          label: "阳江市",
          value: "4417"
        }, {
          label: "清远市",
          value: "4418"
        }, {
          label: "东莞市",
          value: "4419"
        }, {
          label: "中山市",
          value: "4420"
        }, {
          label: "潮州市",
          value: "4451"
        }, {
          label: "揭阳市",
          value: "4452"
        }, {
          label: "云浮市",
          value: "4453"
        }],
        [{
          label: "南宁市",
          value: "4501"
        }, {
          label: "柳州市",
          value: "4502"
        }, {
          label: "桂林市",
          value: "4503"
        }, {
          label: "梧州市",
          value: "4504"
        }, {
          label: "北海市",
          value: "4505"
        }, {
          label: "防城港市",
          value: "4506"
        }, {
          label: "钦州市",
          value: "4507"
        }, {
          label: "贵港市",
          value: "4508"
        }, {
          label: "玉林市",
          value: "4509"
        }, {
          label: "百色市",
          value: "4510"
        }, {
          label: "贺州市",
          value: "4511"
        }, {
          label: "河池市",
          value: "4512"
        }, {
          label: "来宾市",
          value: "4513"
        }, {
          label: "崇左市",
          value: "4514"
        }],
        [{
          label: "海口市",
          value: "4601"
        }, {
          label: "三亚市",
          value: "4602"
        }, {
          label: "三沙市",
          value: "4603"
        }, {
          label: "儋州市",
          value: "4604"
        }, {
          label: "省直辖县级行政区划",
          value: "4690"
        }],
        [{
          label: "重庆市",
          value: "5001"
        }, {
          label: "县",
          value: "5002"
        }],
        [{
          label: "成都市",
          value: "5101"
        }, {
          label: "自贡市",
          value: "5103"
        }, {
          label: "攀枝花市",
          value: "5104"
        }, {
          label: "泸州市",
          value: "5105"
        }, {
          label: "德阳市",
          value: "5106"
        }, {
          label: "绵阳市",
          value: "5107"
        }, {
          label: "广元市",
          value: "5108"
        }, {
          label: "遂宁市",
          value: "5109"
        }, {
          label: "内江市",
          value: "5110"
        }, {
          label: "乐山市",
          value: "5111"
        }, {
          label: "南充市",
          value: "5113"
        }, {
          label: "眉山市",
          value: "5114"
        }, {
          label: "宜宾市",
          value: "5115"
        }, {
          label: "广安市",
          value: "5116"
        }, {
          label: "达州市",
          value: "5117"
        }, {
          label: "雅安市",
          value: "5118"
        }, {
          label: "巴中市",
          value: "5119"
        }, {
          label: "资阳市",
          value: "5120"
        }, {
          label: "阿坝藏族羌族自治州",
          value: "5132"
        }, {
          label: "甘孜藏族自治州",
          value: "5133"
        }, {
          label: "凉山彝族自治州",
          value: "5134"
        }],
        [{
          label: "贵阳市",
          value: "5201"
        }, {
          label: "六盘水市",
          value: "5202"
        }, {
          label: "遵义市",
          value: "5203"
        }, {
          label: "安顺市",
          value: "5204"
        }, {
          label: "毕节市",
          value: "5205"
        }, {
          label: "铜仁市",
          value: "5206"
        }, {
          label: "黔西南布依族苗族自治州",
          value: "5223"
        }, {
          label: "黔东南苗族侗族自治州",
          value: "5226"
        }, {
          label: "黔南布依族苗族自治州",
          value: "5227"
        }],
        [{
          label: "昆明市",
          value: "5301"
        }, {
          label: "曲靖市",
          value: "5303"
        }, {
          label: "玉溪市",
          value: "5304"
        }, {
          label: "保山市",
          value: "5305"
        }, {
          label: "昭通市",
          value: "5306"
        }, {
          label: "丽江市",
          value: "5307"
        }, {
          label: "普洱市",
          value: "5308"
        }, {
          label: "临沧市",
          value: "5309"
        }, {
          label: "楚雄彝族自治州",
          value: "5323"
        }, {
          label: "红河哈尼族彝族自治州",
          value: "5325"
        }, {
          label: "文山壮族苗族自治州",
          value: "5326"
        }, {
          label: "西双版纳傣族自治州",
          value: "5328"
        }, {
          label: "大理白族自治州",
          value: "5329"
        }, {
          label: "德宏傣族景颇族自治州",
          value: "5331"
        }, {
          label: "怒江傈僳族自治州",
          value: "5333"
        }, {
          label: "迪庆藏族自治州",
          value: "5334"
        }],
        [{
          label: "拉萨市",
          value: "5401"
        }, {
          label: "日喀则市",
          value: "5402"
        }, {
          label: "昌都市",
          value: "5403"
        }, {
          label: "林芝市",
          value: "5404"
        }, {
          label: "山南市",
          value: "5405"
        }, {
          label: "那曲地区",
          value: "5424"
        }, {
          label: "阿里地区",
          value: "5425"
        }],
        [{
          label: "西安市",
          value: "6101"
        }, {
          label: "铜川市",
          value: "6102"
        }, {
          label: "宝鸡市",
          value: "6103"
        }, {
          label: "咸阳市",
          value: "6104"
        }, {
          label: "渭南市",
          value: "6105"
        }, {
          label: "延安市",
          value: "6106"
        }, {
          label: "汉中市",
          value: "6107"
        }, {
          label: "榆林市",
          value: "6108"
        }, {
          label: "安康市",
          value: "6109"
        }, {
          label: "商洛市",
          value: "6110"
        }],
        [{
          label: "兰州市",
          value: "6201"
        }, {
          label: "嘉峪关市",
          value: "6202"
        }, {
          label: "金昌市",
          value: "6203"
        }, {
          label: "白银市",
          value: "6204"
        }, {
          label: "天水市",
          value: "6205"
        }, {
          label: "武威市",
          value: "6206"
        }, {
          label: "张掖市",
          value: "6207"
        }, {
          label: "平凉市",
          value: "6208"
        }, {
          label: "酒泉市",
          value: "6209"
        }, {
          label: "庆阳市",
          value: "6210"
        }, {
          label: "定西市",
          value: "6211"
        }, {
          label: "陇南市",
          value: "6212"
        }, {
          label: "临夏回族自治州",
          value: "6229"
        }, {
          label: "甘南藏族自治州",
          value: "6230"
        }],
        [{
          label: "西宁市",
          value: "6301"
        }, {
          label: "海东市",
          value: "6302"
        }, {
          label: "海北藏族自治州",
          value: "6322"
        }, {
          label: "黄南藏族自治州",
          value: "6323"
        }, {
          label: "海南藏族自治州",
          value: "6325"
        }, {
          label: "果洛藏族自治州",
          value: "6326"
        }, {
          label: "玉树藏族自治州",
          value: "6327"
        }, {
          label: "海西蒙古族藏族自治州",
          value: "6328"
        }],
        [{
          label: "银川市",
          value: "6401"
        }, {
          label: "石嘴山市",
          value: "6402"
        }, {
          label: "吴忠市",
          value: "6403"
        }, {
          label: "固原市",
          value: "6404"
        }, {
          label: "中卫市",
          value: "6405"
        }],
        [{
          label: "乌鲁木齐市",
          value: "6501"
        }, {
          label: "克拉玛依市",
          value: "6502"
        }, {
          label: "吐鲁番市",
          value: "6504"
        }, {
          label: "哈密市",
          value: "6505"
        }, {
          label: "昌吉回族自治州",
          value: "6523"
        }, {
          label: "博尔塔拉蒙古自治州",
          value: "6527"
        }, {
          label: "巴音郭楞蒙古自治州",
          value: "6528"
        }, {
          label: "阿克苏地区",
          value: "6529"
        }, {
          label: "克孜勒苏柯尔克孜自治州",
          value: "6530"
        }, {
          label: "喀什地区",
          value: "6531"
        }, {
          label: "和田地区",
          value: "6532"
        }, {
          label: "伊犁哈萨克自治州",
          value: "6540"
        }, {
          label: "塔城地区",
          value: "6542"
        }, {
          label: "阿勒泰地区",
          value: "6543"
        }, {
          label: "自治区直辖县级行政区划",
          value: "6590"
        }],
        [{
          label: "台北",
          value: "6601"
        }, {
          label: "高雄",
          value: "6602"
        }, {
          label: "基隆",
          value: "6603"
        }, {
          label: "台中",
          value: "6604"
        }, {
          label: "台南",
          value: "6605"
        }, {
          label: "新竹",
          value: "6606"
        }, {
          label: "嘉义",
          value: "6607"
        }, {
          label: "宜兰",
          value: "6608"
        }, {
          label: "桃园",
          value: "6609"
        }, {
          label: "苗栗",
          value: "6610"
        }, {
          label: "彰化",
          value: "6611"
        }, {
          label: "南投",
          value: "6612"
        }, {
          label: "云林",
          value: "6613"
        }, {
          label: "屏东",
          value: "6614"
        }, {
          label: "台东",
          value: "6615"
        }, {
          label: "花莲",
          value: "6616"
        }, {
          label: "澎湖",
          value: "6617"
        }],
        [{
          label: "香港岛",
          value: "6701"
        }, {
          label: "九龙",
          value: "6702"
        }, {
          label: "新界",
          value: "6703"
        }],
        [{
          label: "澳门半岛",
          value: "6801"
        }, {
          label: "氹仔岛",
          value: "6802"
        }, {
          label: "路环岛",
          value: "6803"
        }, {
          label: "路氹城",
          value: "6804"
        }]
      ];
      l.default = t;
    },
    "0f48": function (e, l, a) {
      var t = function () {
        function t(e, l) {
          for (var a = 0; a < l.length; a++) {
            var t = l[a];
            t.enumerable = t.enumerable || !1, t.configurable = !0, "value" in t && (t.writable = !0), Object.defineProperty(e, t.key, t);
          }
        }
        return function (e, l, a) {
          return l && t(e.prototype, l), a && t(e, a), e;
        };
      }();
      var i = 310,
        u = "请求参数信息有误",
        n = 600,
        o = "系统错误",
        r = 1e3,
        v = 200,
        c = "https://apis.map.qq.com/ws/",
        b = c + "geocoder/v1/",
        s = {
          location2query: function (e) {
            if ("string" == typeof e) return e;
            for (var l = "", a = 0; a < e.length; a++) {
              var t = e[a];
              l && (l += ";"), t.location && (l = l + t.location.lat + "," + t.location.lng), t.latitude && t.longitude && (l = l + t.latitude + "," + t.longitude);
            }
            return l;
          },
          getWXLocation: function (e, l, a) {
            wx.getLocation({
              type: "gcj02",
              success: e,
              fail: l,
              complete: a
            });
          },
          getLocationParam: function (e) {
            "string" == typeof e && (e = 2 === e.split(",").length ? {
              latitude: e.split(",")[0],
              longitude: e.split(",")[1]
            } : {});
            return e;
          },
          polyfillParam: function (e) {
            e.success = e.success || function () {}, e.fail = e.fail || function () {}, e.complete = e.complete || function () {};
          },
          checkParamKeyEmpty: function (e, l) {
            if (!e[l]) {
              var a = this.buildErrorConfig(i, u + l + "参数格式有误");
              return e.fail(a), e.complete(a), !0;
            }
            return !1;
          },
          checkKeyword: function (e) {
            return !this.checkParamKeyEmpty(e, "keyword");
          },
          checkLocation: function (e) {
            var l = this.getLocationParam(e.location);
            if (!l || !l.latitude || !l.longitude) {
              var a = this.buildErrorConfig(i, u + " location参数格式有误");
              return e.fail(a), e.complete(a), !1;
            }
            return !0;
          },
          buildErrorConfig: function (e, l) {
            return {
              status: e,
              message: l
            };
          },
          buildWxRequestConfig: function (a, e) {
            var t = this;
            return e.header = {
              "content-type": "application/json"
            }, e.method = "GET", e.success = function (e) {
              var l = e.data;
              0 === l.status ? a.success(l) : a.fail(l);
            }, e.fail = function (e) {
              e.statusCode = r, a.fail(t.buildErrorConfig(r, result.errMsg));
            }, e.complete = function (e) {
              switch (+e.statusCode) {
                case r:
                  a.complete(t.buildErrorConfig(r, e.errMsg));
                  break;
                case v:
                  var l = e.data;
                  0 === l.status ? a.complete(l) : a.complete(t.buildErrorConfig(l.status, l.message));
                  break;
                default:
                  a.complete(t.buildErrorConfig(n, o));
              }
            }, e;
          },
          locationProcess: function (l, e, a, t) {
            var i = this;
            (a = a || function (e) {
              e.statusCode = r, l.fail(i.buildErrorConfig(r, e.errMsg));
            }, t = t || function (e) {
              e.statusCode == r && l.complete(i.buildErrorConfig(r, e.errMsg));
            }, l.location) ? i.checkLocation(l) && e(s.getLocationParam(l.location)): i.getWXLocation(e, a, t);
          }
        },
        d = function () {
          function l(e) {
            if (function (e, l) {
                if (!(e instanceof l)) throw new TypeError("Cannot call a class as a function");
              }(this, l), !e.key) throw Error("key值不能为空");
            this.key = e.key;
          }
          return t(l, [{
            key: "search",
            value: function (l) {
              if (l = l || {}, s.polyfillParam(l), s.checkKeyword(l)) {
                var a = {
                  keyword: l.keyword,
                  orderby: l.orderby || "_distance",
                  page_size: l.page_size || 10,
                  page_index: l.page_index || 1,
                  output: "json",
                  key: this.key
                };
                l.address_format && (a.address_format = l.address_format), l.filter && (a.filter = l.filter);
                var t = l.distance || "1000",
                  i = l.auto_extend || 1;
                s.locationProcess(l, function (e) {
                  a.boundary = "nearby(" + e.latitude + "," + e.longitude + "," + t + "," + i + ")", wx.request(s.buildWxRequestConfig(l, {
                    url: "https://apis.map.qq.com/ws/place/v1/search",
                    data: a
                  }));
                });
              }
            }
          }, {
            key: "getSuggestion",
            value: function (e) {
              if (e = e || {}, s.polyfillParam(e), s.checkKeyword(e)) {
                var l = {
                  keyword: e.keyword,
                  region: e.region || "全国",
                  region_fix: e.region_fix || 0,
                  policy: e.policy || 0,
                  output: "json",
                  key: this.key
                };
                wx.request(s.buildWxRequestConfig(e, {
                  url: "https://apis.map.qq.com/ws/place/v1/suggestion",
                  data: l
                }));
              }
            }
          }, {
            key: "reverseGeocoder",
            value: function (l) {
              l = l || {}, s.polyfillParam(l);
              var a = {
                coord_type: l.coord_type || 5,
                get_poi: l.get_poi || 0,
                output: "json",
                key: this.key
              };
              l.poi_options && (a.poi_options = l.poi_options);
              s.locationProcess(l, function (e) {
                a.location = e.latitude + "," + e.longitude, wx.request(s.buildWxRequestConfig(l, {
                  url: b,
                  data: a
                }));
              });
            }
          }, {
            key: "geocoder",
            value: function (e) {
              if (e = e || {}, s.polyfillParam(e), !s.checkParamKeyEmpty(e, "address")) {
                var l = {
                  address: e.address,
                  output: "json",
                  key: this.key
                };
                wx.request(s.buildWxRequestConfig(e, {
                  url: b,
                  data: l
                }));
              }
            }
          }, {
            key: "getCityList",
            value: function (e) {
              e = e || {}, s.polyfillParam(e);
              var l = {
                output: "json",
                key: this.key
              };
              wx.request(s.buildWxRequestConfig(e, {
                url: "https://apis.map.qq.com/ws/district/v1/list",
                data: l
              }));
            }
          }, {
            key: "getDistrictByCityId",
            value: function (e) {
              if (e = e || {}, s.polyfillParam(e), !s.checkParamKeyEmpty(e, "id")) {
                var l = {
                  id: e.id || "",
                  output: "json",
                  key: this.key
                };
                wx.request(s.buildWxRequestConfig(e, {
                  url: "https://apis.map.qq.com/ws/district/v1/getchildren",
                  data: l
                }));
              }
            }
          }, {
            key: "calculateDistance",
            value: function (l) {
              if (l = l || {}, s.polyfillParam(l), !s.checkParamKeyEmpty(l, "to")) {
                var a = {
                  mode: l.mode || "walking",
                  to: s.location2query(l.to),
                  output: "json",
                  key: this.key
                };
                l.from && (l.location = l.from), s.locationProcess(l, function (e) {
                  a.from = e.latitude + "," + e.longitude, wx.request(s.buildWxRequestConfig(l, {
                    url: "https://apis.map.qq.com/ws/distance/v1/",
                    data: a
                  }));
                });
              }
            }
          }]), l;
        }();
      e.exports = d;
    },
    1426: function (e, l, a) {
      Object.defineProperty(l, "__esModule", {
        value: !0
      });
      l.default = void 0;
      l.default = {
        pages: {
          "pages/index/index": {
            navigationBarTitleText: "首页"
          },
          "pages/usercenter/usercenter": {
            navigationBarTitleText: "个人中心"
          },
          "pages/main_shop_order/main_shop_order": {
            navigationBarTitleText: "我的订单"
          },
          "pages/order_more_list/order_more_list": {
            navigationBarTitleText: "我的订单"
          },
          "pages/orderDetails/orderDetails": {
            navigationBarTitleText: "订单详情"
          },
          "pages/orderDetail/orderDetail": {
            navigationBarTitleText: "订单详情"
          },
          "pages/applyAfterSales/applyAfterSales": {
            navigationBarTitleText: "申请售后",
            enablePullDownRefresh: !1
          },
          "pages/orderAftersale_list/orderAftersale_list": {
            navigationBarTitleText: "售后订单"
          },
          "pages/orderAftersale/orderAftersale": {
            navigationBarTitleText: "售后详情"
          },
          "pages/address/address": {
            navigationBarTitleText: "地址列表"
          },
          "pages/showProMore/showProMore": {
            navigationStyle: "custom"
          },
          "pages/order_down/order_down": {
            navigationBarTitleText: "商品订单",
            enablePullDownRefresh: !1
          },
          "pages/chooseStore/chooseStore": {
            navigationBarTitleText: "门店列表"
          },
          "pages/collect/collect": {
            navigationBarTitleText: "我的收藏"
          },
          "pages/mycoupon/mycoupon": {
            navigationBarTitleText: "我的优惠券"
          },
          "pages/coupon/coupon": {
            navigationBarTitleText: "领取优惠券"
          },
          "pages/aliH5pay/aliH5pay": {
            navigationBarTitleText: "支付宝支付"
          },
          "pages/wxH5pay/wxH5pay": {
            navigationBarTitleText: "微信支付"
          },
          "pages/gwc/gwc": {
            navigationBarTitleText: "购物车"
          },
          "pages/showArt/showArt": {
            navigationBarTitleText: "文章详情页"
          },
          "pages/order_art/order_art": {
            navigationBarTitleText: "文章付费订单"
          },
          "pages/recharge/recharge": {
            navigationBarTitleText: "账户充值"
          },
          "pages/showPic/showPic": {
            navigationStyle: "custom"
          },
          "pages/showPiclist/showPiclist": {
            navigationBarTitleText: "组图"
          },
          "pages/listPic/listPic": {},
          "pages/search/search": {},
          "pages/diypage/diypage": {
            navigationBarTitleText: "DIY页面"
          },
          "pages/logistics_state/logistics_state": {
            navigationBarTitleText: "物流详情"
          },
          "pages/logistics_information/logistics_information": {
            navigationBarTitleText: "物流信息"
          },
          "pages/webpage/webpage": {
            navigationBarTitleText: "外部网页"
          },
          "pages/shoppay/shoppay": {
            navigationBarTitleText: "店内支付"
          },
          "pages/scorelist/scorelist": {},
          "pages/store/store": {},
          "pages/switchcity/switchcity": {
            path: "pages/switchcity/switchcity",
            style: {
              navigationBarTitleText: "切换城市"
            }
          },
          "pages/equity_show/equity_show": {},
          "pages/open1/open1": {},
          "pages/register/register": {},
          "pages/form_list/form_list": {},
          "pages/formcon/formcon": {},
          "pages/catelist/catelist": {},
          "pages/checkPro/checkPro": {
            navigationBarTitleText: "商品筛选"
          },
          "pagesFlashSale/showPro/showPro": {
            navigationStyle: "custom"
          },
          "pagesFlashSale/order_dan/order_dan": {
            navigationBarTitleText: "秒杀下单"
          },
          "pagesFlashSale/orderlist_dan/orderlist_dan": {
            navigationBarTitleText: "秒杀订单列表"
          },
          "pagesFlashSale/orderDetail_dan/orderDetail_dan": {
            navigationBarTitleText: "秒杀订单列表"
          },
          "pagesPt/products/products": {
            navigationStyle: "custom"
          },
          "pagesPt/order/order": {
            navigationBarTitleText: "下单"
          },
          "pagesPt/pt/pt": {
            navigationBarTitleText: "拼团"
          },
          "pagesPt/orderlist/orderlist": {
            navigationBarTitleText: "拼团"
          },
          "pagesPt/index/index": {
            navigationBarTitleText: "拼团首页"
          },
          "pagesReserve/orderList/orderList": {
            navigationBarTitleText: "预约预定订单列表"
          },
          "pagesReserve/proBuy/proBuy": {
            navigationBarTitleText: "预约预定订单下单",
            enablePullDownRefresh: !1
          },
          "pagesReserve/proDetail/proDetail": {
            navigationBarTitleText: "预约预定商品详情"
          },
          "pagesReserve/appointPage/appointPage": {
            navigationBarTitleText: ""
          },
          "pagesReserve/orderDetail/orderDetail": {
            navigationBarTitleText: ""
          },
          "pagesSign/index/index": {
            navigationBarTitleText: "积分签到页"
          },
          "pagesSign/new/new": {
            navigationBarTitleText: "最新签到"
          },
          "pagesSign/ranking/ranking": {
            navigationBarTitleText: "签到排行"
          },
          "pagesExchange/list/list": {
            navigationBarTitleText: "积分商城"
          },
          "pagesExchange/show/show": {
            navigationBarTitleText: "积分商品详情"
          },
          "pagesExchange/order/order": {
            navigationBarTitleText: "积分订单列表页"
          },
          "pagesCards/card_info/card_info": {
            navigationBarTitleText: "名片页"
          },
          "pagesCards/card_list/card_list": {
            navigationBarTitleText: "名片列表页",
            enablePullDownRefresh: !1
          },
          "pagesShake/index/index": {
            navigationBarTitleText: "摇一摇抽奖"
          },
          "pagesShake/integral_collect/integral_collect": {
            navigationBarTitleText: "获取积分"
          },
          "pagesShake/prize/prize": {
            navigationBarTitleText: ""
          },
          "pagesShake/win_prize/win_prize": {
            navigationBarTitleText: ""
          },
          "pagesFood/food/food": {
            navigationBarTitleText: ""
          },
          "pagesFood/food_my/food_my": {
            navigationBarTitleText: "我的餐饮订单"
          },
          "pagesFood/food_order/food_order": {
            navigationBarTitleText: "",
            enablePullDownRefresh: !1
          },
          "pagesFood/food_detail/food_detail": {
            navigationBarTitleText: ""
          },
          "pagesBargain/bargain/bargain": {
            navigationBarTitleText: ""
          },
          "pagesBargain/bargain_activity/bargain_activity": {
            navigationBarTitleText: ""
          },
          "pagesBargain/bargain_order/bargain_order": {
            navigationBarTitleText: ""
          },
          "pagesBargain/bargain_pro/bargain_pro": {
            navigationStyle: "custom"
          },
          "pagesBargain/my_bargain/my_bargain": {
            navigationBarTitleText: ""
          },
          "pagesBargain/orderDetail/orderDetail": {
            navigationBarTitleText: ""
          },
          "pagesBargain/orderlist/orderlist": {
            navigationBarTitleText: ""
          },
          "pagesPluginForum/collect/collect": {
            navigationBarTitleText: ""
          },
          "pagesPluginForum/forum/forum": {
            navigationBarTitleText: ""
          },
          "pagesPluginForum/forum_page/forum_page": {
            navigationBarTitleText: ""
          },
          "pagesPluginForum/index/index": {
            navigationBarTitleText: ""
          },
          "pagesPluginForum/release/release": {
            navigationBarTitleText: "",
            enablePullDownRefresh: !1
          },
          "pagesPluginForum/set_top/set_top": {
            navigationBarTitleText: ""
          },
          "pagesPluginShop/goods_buy/goods_buy": {
            navigationBarTitleText: "",
            enablePullDownRefresh: !1
          },
          "pagesPluginShop/goods_detail/goods_detail": {
            navigationBarTitleText: ""
          },
          "pagesPluginShop/goods_list/goods_list": {
            navigationBarTitleText: ""
          },
          "pagesPluginShop/list/list": {
            navigationBarTitleText: ""
          },
          "pagesPluginShop/manage_gotixian/manage_gotixian": {
            navigationBarTitleText: "",
            enablePullDownRefresh: !1
          },
          "pagesPluginShop/manage_index/manage_index": {
            navigationBarTitleText: ""
          },
          "pagesPluginShop/manage_order/manage_order": {
            navigationBarTitleText: ""
          },
          "pagesPluginShop/manage_pro/manage_pro": {
            navigationBarTitleText: ""
          },
          "pagesPluginShop/manage_prolist/manage_prolist": {
            navigationBarTitleText: ""
          },
          "pagesPluginShop/manage_shop/manage_shop": {
            navigationBarTitleText: "",
            enablePullDownRefresh: !1
          },
          "pagesPluginShop/manage_szjl/manage_szjl": {
            navigationBarTitleText: ""
          },
          "pagesPluginShop/manage_tixian/manage_tixian": {
            navigationBarTitleText: ""
          },
          "pagesPluginShop/manage_txjl/manage_txjl": {
            navigationBarTitleText: ""
          },
          "pagesPluginShop/register/register": {
            navigationBarTitleText: ""
          },
          "pagesPluginShop/shop/shop": {
            navigationBarTitleText: ""
          },
          "pagesPluginSupply/collect/collect": {
            navigationBarTitleText: "我的"
          },
          "pagesPluginSupply/page/page": {
            navigationBarTitleText: "信息详情"
          },
          "pagesPluginSupply/release/release": {
            navigationBarTitleText: "供求发布",
            enablePullDownRefresh: !1
          },
          "pagesPluginSupply/set_top/set_top": {
            navigationBarTitleText: "信息置顶"
          },
          "pagesPluginSupply/supply/supply": {
            navigationBarTitleText: "供求列表"
          },
          "pagesExternal/jdGoods/jdGoods": {
            navigationBarTitleText: "京东好券"
          },
          "pagesExternal/pddGoods/pddGoods": {
            navigationBarTitleText: "多多精选"
          },
          "pagesActive/active_info/active_info": {
            navigationBarTitleText: "活动详情"
          },
          "pagesActive/apply/apply": {
            navigationBarTitleText: ""
          },
          "pagesActive/apply_collect/apply_collect": {
            navigationBarTitleText: "活动报名"
          },
          "pagesActive/apply_list/apply_list": {
            navigationBarTitleText: "申请列表"
          },
          "pagesActive/index/index": {
            navigationBarTitleText: "活动列表"
          },
          "pagesFenxiao/fenxiao_account/fenxiao_account": {
            navigationBarTitleText: "我的账户"
          },
          "pagesFenxiao/fenxiao_apply/fenxiao_apply": {
            navigationBarTitleText: ""
          },
          "pagesFenxiao/fenxiao_center/fenxiao_center": {
            navigationBarTitleText: "分销中心"
          },
          "pagesFenxiao/fenxiao_order/fenxiao_order": {
            navigationBarTitleText: "分销订单"
          },
          "pagesFenxiao/fenxiao_s/fenxiao_s": {
            navigationBarTitleText: ""
          },
          "pagesFenxiao/fenxiao_team/fenxiao_team": {
            navigationBarTitleText: "我的团队"
          },
          "pagesFenxiao/fenxiao_tixian/fenxiao_tixian": {
            navigationBarTitleText: "我要提现"
          },
          "pagesFenxiao/fenxiao_tixian_do/fenxiao_tixian_do": {
            navigationBarTitleText: "提现记录"
          },
          "pagesOther/evaluate/evaluate": {
            navigationBarTitleText: "评价"
          },
          "pagesOther/evaluate_list/evaluate_list": {
            navigationBarTitleText: "评价列表"
          },
          "pagesOther/evaluate_detail/evaluate_detail": {
            navigationBarTitleText: "评价详情"
          },
          "pagesOther/evaluate_pro/evaluate_pro": {
            navigationBarTitleText: "评价"
          }
        },
        globalStyle: {
          navigationBarTextStyle: "black",
          navigationBarTitleText: "",
          navigationBarBackgroundColor: "#F8F8F8",
          backgroundColor: "#F8F8F8",
          enablePullDownRefresh: !0
        }
      };
    },
    2877: function (e, l, a) {
      function t(e, l, a, t, i, u, n, o) {
        var r, v = "function" == typeof e ? e.options : e;
        if (l && (v.render = l, v.staticRenderFns = a, v._compiled = !0), t && (v.functional = !0), u && (v._scopeId = "data-v-" + u), n ? (r = function (e) {
            (e = e || this.$vnode && this.$vnode.ssrContext || this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) || "undefined" == typeof __VUE_SSR_CONTEXT__ || (e = __VUE_SSR_CONTEXT__), i && i.call(this, e), e && e._registeredComponents && e._registeredComponents.add(n);
          }, v._ssrRegister = r) : i && (r = o ? function () {
            i.call(this, this.$root.$options.shadowRoot);
          } : i), r)
          if (v.functional) {
            v._injectStyles = r;
            var c = v.render;
            v.render = function (e, l) {
              return r.call(l), c(e, l);
            };
          } else {
            var b = v.beforeCreate;
            v.beforeCreate = b ? [].concat(b, r) : [r];
          }
        return {
          exports: e,
          options: v
        };
      }
      a.d(l, "a", function () {
        return t;
      });
    },
    "2da0": function (a, e, t) {
      (function (r) {
        var e;
        (e = t("66fd")) && e.__esModule;
        var l = {
          baseMin: function (l) {
            var e = r.getStorageSync("suid");
            r.request({
              url: l.$baseurl + "doPageBaseMin",
              data: {
                uniacid: l.$uniacid,
                vs1: 1,
                suid: e
              },
              header: {
                "custom-header": "hello"
              },
              success: function (e) {
                l.baseinfo = e.data.data, r.setStorageSync("base_tel", e.data.data.tel), r.setStorageSync("isshow", e.data.data.alish), r.setStorageSync("global_baseinfo", e.data.data), r.setStorageSync("can_show", e.data.data.can_show), r.setNavigationBarColor({
                  frontColor: l.baseinfo.base_tcolor,
                  backgroundColor: l.baseinfo.base_color
                }), l.tabbar = l.baseinfo.tabbar;
              }
            });
          },
          baseMin2: function (l) {
            r.request({
              url: l.$baseurl + "doPageBaseMin",
              data: {
                uniacid: l.$uniacid,
                vs1: 1
              },
              header: {
                "custom-header": "hello"
              },
              success: function (e) {
                l.baseinfo = e.data.data, l.tabbar = l.baseinfo.tabbar;
              }
            });
          },
          checkBindPhone: function (l) {
            console.log(l.needBind), 0 == l.needBind && r.getStorage({
              key: "suid",
              fail: function () {
                r.request({
                  url: l.$baseurl + "dopagehasBind",
                  data: {
                    uniacid: l.$uniacid,
                    identity: r.getStorageSync("openid"),
                    status: 1
                  },
                  success: function (e) {
                    console.log(e), "1" == e.data.data && e.data.suid ? r.setStorageSync("suid", e.data.suid) : "2" == e.data.data && (l.needBind = !0);
                  }
                });
              }
            });
          },
          getSuperUserInfo: function (l) {
            var e = r.getStorageSync("suid");
            e && r.request({
              url: l.$baseurl + "dopagegetSuperUserInfo",
              data: {
                suid: e,
                uniacid: l.$uniacid,
                source: r.getStorageSync("source")
              },
              success: function (e) {
                l.superuser = e.data.data;
              }
            });
          },
          redirectto: function (l, e) {
            switch (r.getStorageSync("uniacid"), e) {
              case "page":
                var a = l.indexOf("pages/index"),
                  t = l.indexOf("index?pageid"); - 1 == a && -1 == t ? r.navigateTo({
                  url: l
                }) : r.reLaunch({
                  url: l
                });
                break;
              case "web":
                r.navigateTo({
                  url: "/pages/webpage/webpage?url=" + encodeURIComponent(l)
                });
                break;
              case "tel":
                l = l.slice(4), r.showModal({
                  title: "提示",
                  content: "是否拨打电话:" + l,
                  success: function (e) {
                    1 == e.confirm && r.makePhoneCall({
                      phoneNumber: l
                    });
                  }
                });
                break;
              case "map":
                var i = l.split("##");
                l = i[0].split(","), r.openLocation({
                  latitude: parseFloat(l[0]),
                  longitude: parseFloat(l[1]),
                  name: i[1],
                  address: i[2]
                });
                break;
              case "mini":
                var u = (i = l.split(","))[0].slice(6);
                if (2 == i.length) var n = i[1].slice(9);
                else n = "";
                r.navigateToMiniProgram({
                  appId: u,
                  path: n,
                  success: function (e) {
                    console.log("打开成功"), console.log(u);
                  }
                });
            }
          },
          showwxpay: function (e, l, a, t, i, u) {
            r.request({
              url: e.$baseurl + "dopagebeforepay",
              data: {
                uniacid: e.$uniacid,
                openid: r.getStorageSync("openid"),
                price: l,
                order_id: t,
                types: a,
                formId: i,
                suid: r.getStorageSync("suid")
              },
              success: function (e) {
                if (e.data.data.return_code === 'FAIL') {} -
                1 != [1, 2, 3, 4].indexOf(e.data.data.err) && r.showModal({
                  title: "支付失败",
                  content: e.data.data.message ? e.data.data.message : e.data.data.msg,
                  showCancel: !1
                }), 0 == e.data.data.err && (console.log("调起支付"), console.log(e.data.data), r.requestPayment({
                  timeStamp: e.data.data.timeStamp,
                  nonceStr: e.data.data.nonceStr,
                  package: e.data.data.package,
                  signType: "MD5",
                  paySign: e.data.data.paySign,
                  success: function (e) {
                    console.log(e), r.showToast({
                      title: "支付成功",
                      icon: "success",
                      mask: !0,
                      duration: 3e3,
                      success: function (e) {
                        1 == u ? r.navigateBack({
                          delta: 1
                        }) : r.navigateTo({
                          url: u
                        });
                      }
                    });
                  },
                  fail: function (e) {
                    1 == u ? r.navigateBack({
                      delta: 1
                    }) : (r.navigateBack({
                      delta: 9
                    }), r.navigateTo({
                      url: u
                    }));
                  },
                  complete: function (e) {
                    console.log("complete");
                  }
                }));
              }
            });
          },
          showalipay: function (e, l, a, t, i, u) {
            console.log(20898989), r.request({
              url: e.$baseurl + "doPageAlipay",
              data: {
                uniacid: e.$uniacid,
                money: l,
                suid: r.getStorageSync("suid"),
                types: a,
                order_id: t
              },
              success: function (e) {
                e && r.tradePay({
                  tradeNO: e.data.trade_no,
                  success: function (e) {
                    1 == u ? r.navigateBack({
                      delta: 1
                    }) : r.navigateTo({
                      url: u
                    });
                  },
                  fail: function (e) {
                    1 == u ? r.navigateBack({
                      delta: 1
                    }) : r.navigateTo({
                      url: u
                    });
                  }
                });
              }
            });
          },
          alih5pay: function (e, l, a, t) {
            console.log("支付宝H5支付"), r.request({
              url: e.$baseurl + "doPageHfivepay",
              data: {
                uniacid: e.$uniacid,
                money: l,
                suid: r.getStorageSync("suid"),
                types: a,
                order_id: t
              },
              success: function (e) {
                1 == e.data.flag && r.navigateTo({
                  url: "/pages/aliH5pay/aliH5pay?id=" + e.data.data + "&type=" + a
                });
              }
            });
          },
          wxh5pay: function (e, l, a, t) {
            console.log("微信H5支付"), r.request({
              url: e.$baseurl + "doPageHfiveWxpay",
              data: {
                uniacid: e.$uniacid,
                order_id: t,
                money: l,
                suid: r.getStorageSync("suid"),
                types: a
              },
              success: function (e) {
                0 == e.data.error ? (r.setStorageSync(e.data.order_id, e.data.mweb_url), r.navigateTo({
                  url: "/pages/wxH5pay/wxH5pay?order_id=" + e.data.order_id
                })) : r.showModal({
                  title: "提示",
                  content: e.data.msg,
                  showCancel: !1
                });
              }
            });
          },
          baidupay: function (e, l, a, t, i) {
            console.log("百度支付"), r.request({
              url: e.$baseurl + "doPageBaidupay",
              data: {
                uniacid: e.$uniacid,
                order_id: t,
                money: l,
                suid: r.getStorageSync("suid"),
                types: a
              },
              success: function (e) {
                e.data && swan.requestPolymerPayment({
                  orderInfo: {
                    dealId: e.data.dealId,
                    appKey: e.data.appKey,
                    totalAmount: e.data.totalAmount,
                    tpOrderId: e.data.tpOrderId,
                    dealTitle: e.data.dealTitle,
                    signFieldsRange: "1",
                    rsaSign: e.data.rsaSign,
                    bizInfo: e.data.bizInfo
                  },
                  bannedChannels: ["BDWallet"],
                  success: function (e) {
                    r.showToast({
                      title: "支付成功",
                      icon: "success"
                    }), 1 == i ? r.navigateBack({
                      delta: 1
                    }) : r.redirectTo({
                      url: i
                    });
                  },
                  fail: function (e) {
                    r.showToast({
                      title: "支付失败"
                    }), console.log("pay fail", e), 1 == i ? r.navigateBack({
                      delta: 1
                    }) : r.redirectTo({
                      url: i
                    });
                  }
                });
              }
            });
          },
          toutiaopay: function (l, e, n, a, t, o) {
            r.request({
              url: l.$baseurl + "doPageToutiaopay",
              data: {
                uniacid: l.$uniacid,
                order_id: a,
                money: e,
                suid: r.getStorageSync("suid"),
                types: n,
                formId: t
              },
              success: function (e) {
                if (0 == e.data.return_code) {
                  var t = e.data.out_order_no,
                    i = l.$baseurl,
                    u = l.$uniacid;
                  tt.pay({
                    orderInfo: {
                      merchant_id: e.data.merchant_id,
                      app_id: e.data.app_id,
                      sign_type: "MD5",
                      timestamp: e.data.timestamp,
                      sign: e.data.sign,
                      product_code: "pay",
                      payment_type: "direct",
                      out_order_no: t,
                      uid: e.data.uid,
                      total_amount: e.data.total_amount,
                      currency: "CNY",
                      subject: e.data.subject,
                      body: e.data.body,
                      trade_time: e.data.trade_time,
                      valid_time: "1800",
                      notify_url: e.data.notify_url,
                      wx_url: e.data.wx_url,
                      wx_type: "MWEB",
                      trade_type: "H5",
                      version: "2.0",
                      risk_info: e.data.risk_info
                    },
                    service: 1,
                    getOrderStatus: function (e) {
                      return console.log(88888), console.log(e), new Promise(function (a, l) {
                        tt.request({
                          url: i + "doPageCheckToutiaoPay",
                          data: {
                            uniacid: u,
                            order_id: t,
                            types: n
                          },
                          success: function (e) {
                            console.log(e);
                            var l = e.data.data;
                            a(1 == l ? {
                              code: 0
                            } : {
                              code: 4
                            });
                          },
                          fail: function (e) {
                            l(e);
                          }
                        });
                      });
                    },
                    success: function (e) {
                      console.log(e), 0 == e.code ? (r.showToast({
                        title: "支付成功",
                        icon: "success"
                      }), console.log(o)) : r.showToast({
                        title: "支付失败"
                      }), 1 == o ? r.navigateBack({
                        delta: 1
                      }) : r.redirectTo({
                        url: o
                      });
                    },
                    fail: function (e) {
                      r.showToast({
                        title: "支付失败"
                      }), 1 == url ? r.navigateBack({
                        delta: 1
                      }) : r.redirectTo({
                        url: url
                      });
                    }
                  });
                } else r.showModal({
                  title: "提示",
                  content: e.data.return_msg,
                  showCancel: !1
                });
              }
            });
          },
          qqpay: function (e, l, a, t, i, u) {
            console.log("QQ支付"), r.request({
              url: e.$baseurl + "doPageqqpay",
              data: {
                uniacid: e.$uniacid,
                order_id: t,
                money: l,
                suid: r.getStorageSync("suid"),
                types: a,
                formId: i
              },
              success: function (e) {
                console.log(e), e.data && qq.requestPayment({
                  package: "prepay_id=" + e.data.data.prepay_id,
                  bargainor_id: e.data.data.mch_id,
                  success: function (e) {
                    console.log(6666), r.showToast({
                      title: "支付成功",
                      icon: "success"
                    }), 1 == u ? r.navigateBack({
                      delta: 1
                    }) : r.redirectTo({
                      url: u
                    });
                  },
                  fail: function (e) {
                    r.showToast({
                      title: "支付失败",
                      icon: "success"
                    }), 1 == u ? r.navigateBack({
                      delta: 1
                    }) : r.redirectTo({
                      url: u
                    });
                  }
                });
              }
            });
          },
          givepscore: function (e, l, a, t, i) {
            t != i && 0 != t && "" != t && null != t && r.request({
              url: e.$baseurl + "doPagegiveposcore",
              data: {
                id: l,
                types: a,
                suid: i,
                fxsid: t,
                uniacid: e.$uniacid,
                source: r.getStorageSync("source")
              },
              success: function (e) {}
            });
          }
        };
        a.exports = l;
      }).call(this, t("543d").default);
    },
    "2f62": function (e, l, a) {
      a.r(l), a.d(l, "Store", function () {
        return n;
      }), a.d(l, "install", function () {
        return p;
      }), a.d(l, "mapState", function () {
        return _;
      }), a.d(l, "mapMutations", function () {
        return m;
      }), a.d(l, "mapGetters", function () {
        return S;
      }), a.d(l, "mapActions", function () {
        return x;
      }), a.d(l, "createNamespacedHelpers", function () {
        return w;
      });
      var t = function (e) {
          if (2 <= Number(e.version.split(".")[0])) e.mixin({
            beforeCreate: a
          });
          else {
            var l = e.prototype._init;
            e.prototype._init = function (e) {
              void 0 === e && (e = {}), e.init = e.init ? [a].concat(e.init) : a, l.call(this, e);
            };
          }

          function a() {
            var e = this.$options;
            e.store ? this.$store = "function" == typeof e.store ? e.store() : e.store : e.parent && e.parent.$store && (this.$store = e.parent.$store);
          }
        },
        v = "undefined" != typeof window && window.__VUE_DEVTOOLS_GLOBAL_HOOK__;

      function o(l, a) {
        Object.keys(l).forEach(function (e) {
          return a(l[e], e);
        });
      }
      var u = function (e, l) {
          this.runtime = l, this._children = Object.create(null);
          var a = (this._rawModule = e).state;
          this.state = ("function" == typeof a ? a() : a) || {};
        },
        i = {
          namespaced: {
            configurable: !0
          }
        };
      i.namespaced.get = function () {
        return !!this._rawModule.namespaced;
      }, u.prototype.addChild = function (e, l) {
        this._children[e] = l;
      }, u.prototype.removeChild = function (e) {
        delete this._children[e];
      }, u.prototype.getChild = function (e) {
        return this._children[e];
      }, u.prototype.update = function (e) {
        this._rawModule.namespaced = e.namespaced, e.actions && (this._rawModule.actions = e.actions), e.mutations && (this._rawModule.mutations = e.mutations), e.getters && (this._rawModule.getters = e.getters);
      }, u.prototype.forEachChild = function (e) {
        o(this._children, e);
      }, u.prototype.forEachGetter = function (e) {
        this._rawModule.getters && o(this._rawModule.getters, e);
      }, u.prototype.forEachAction = function (e) {
        this._rawModule.actions && o(this._rawModule.actions, e);
      }, u.prototype.forEachMutation = function (e) {
        this._rawModule.mutations && o(this._rawModule.mutations, e);
      }, Object.defineProperties(u.prototype, i);
      var f, c = function (e) {
        this.register([], e, !1);
      };
      c.prototype.get = function (e) {
        return e.reduce(function (e, l) {
          return e.getChild(l);
        }, this.root);
      }, c.prototype.getNamespace = function (e) {
        var a = this.root;
        return e.reduce(function (e, l) {
          return e + ((a = a.getChild(l)).namespaced ? l + "/" : "");
        }, "");
      }, c.prototype.update = function (e) {
        ! function e(l, a, t) {
          if (a.update(t), t.modules)
            for (var i in t.modules) {
              if (!a.getChild(i)) return;
              e(l.concat(i), a.getChild(i), t.modules[i]);
            }
        }([], this.root, e);
      }, c.prototype.register = function (a, e, t) {
        var i = this;
        void 0 === t && (t = !0);
        var l = new u(e, t);
        0 === a.length ? this.root = l : this.get(a.slice(0, -1)).addChild(a[a.length - 1], l);
        e.modules && o(e.modules, function (e, l) {
          i.register(a.concat(l), e, t);
        });
      }, c.prototype.unregister = function (e) {
        var l = this.get(e.slice(0, -1)),
          a = e[e.length - 1];
        l.getChild(a).runtime && l.removeChild(a);
      };
      var n = function (e) {
          var l = this;
          void 0 === e && (e = {}), !f && "undefined" != typeof window && window.Vue && p(window.Vue);
          var a = e.plugins;
          void 0 === a && (a = []);
          var t = e.strict;
          void 0 === t && (t = !1);
          var i = e.state;
          void 0 === i && (i = {}), "function" == typeof i && (i = i() || {}), this._committing = !1, this._actions = Object.create(null), this._actionSubscribers = [], this._mutations = Object.create(null), this._wrappedGetters = Object.create(null), this._modules = new c(e), this._modulesNamespaceMap = Object.create(null), this._subscribers = [], this._watcherVM = new f();
          var u, n = this,
            o = this.dispatch,
            r = this.commit;
          this.dispatch = function (e, l) {
            return o.call(n, e, l);
          }, this.commit = function (e, l, a) {
            return r.call(n, e, l, a);
          }, this.strict = t, h(this, i, [], this._modules.root), d(this, i), a.forEach(function (e) {
            return e(l);
          }), f.config.devtools && (u = this, v && ((u._devtoolHook = v).emit("vuex:init", u), v.on("vuex:travel-to-state", function (e) {
            u.replaceState(e);
          }), u.subscribe(function (e, l) {
            v.emit("vuex:mutation", e, l);
          })));
        },
        r = {
          state: {
            configurable: !0
          }
        };

      function b(l, a) {
        return a.indexOf(l) < 0 && a.push(l),
          function () {
            var e = a.indexOf(l); - 1 < e && a.splice(e, 1);
          };
      }

      function s(e, l) {
        e._actions = Object.create(null), e._mutations = Object.create(null), e._wrappedGetters = Object.create(null), e._modulesNamespaceMap = Object.create(null);
        var a = e.state;
        h(e, a, [], e._modules.root, !0), d(e, a, l);
      }

      function d(a, e, l) {
        var t = a._vm;
        a.getters = {};
        var i = a._wrappedGetters,
          u = {};
        o(i, function (e, l) {
          u[l] = function () {
            return e(a);
          }, Object.defineProperty(a.getters, l, {
            get: function () {
              return a._vm[l];
            },
            enumerable: !0
          });
        });
        var n = f.config.silent;
        f.config.silent = !0, a._vm = new f({
          data: {
            $$state: e
          },
          computed: u
        }), f.config.silent = n, a.strict && a._vm.$watch(function () {
          return this._data.$$state;
        }, function () {}, {
          deep: !0,
          sync: !0
        }), t && (l && a._withCommit(function () {
          t._data.$$state = null;
        }), f.nextTick(function () {
          return t.$destroy();
        }));
      }

      function h(r, a, t, e, i) {
        var l = !t.length,
          v = r._modules.getNamespace(t);
        if (e.namespaced && (r._modulesNamespaceMap[v] = e), !l && !i) {
          var u = g(a, t.slice(0, -1)),
            n = t[t.length - 1];
          r._withCommit(function () {
            f.set(u, n, e.state);
          });
        }
        var o, c, b, s, d, p = e.context = (o = r, b = t, d = {
          dispatch: (s = "" === (c = v)) ? o.dispatch : function (e, l, a) {
            var t = y(e, l, a),
              i = t.payload,
              u = t.options,
              n = t.type;
            return u && u.root || (n = c + n), o.dispatch(n, i);
          },
          commit: s ? o.commit : function (e, l, a) {
            var t = y(e, l, a),
              i = t.payload,
              u = t.options,
              n = t.type;
            u && u.root || (n = c + n), o.commit(n, i, u);
          }
        }, Object.defineProperties(d, {
          getters: {
            get: s ? function () {
              return o.getters;
            } : function () {
              return a = o, i = {}, u = (t = c).length, Object.keys(a.getters).forEach(function (e) {
                if (e.slice(0, u) === t) {
                  var l = e.slice(u);
                  Object.defineProperty(i, l, {
                    get: function () {
                      return a.getters[e];
                    },
                    enumerable: !0
                  });
                }
              }), i;
              var a, t, i, u;
            }
          },
          state: {
            get: function () {
              return g(o.state, b);
            }
          }
        }), d);
        e.forEachMutation(function (e, l) {
          var a, t, i, u;
          t = v + l, i = e, u = p, ((a = r)._mutations[t] || (a._mutations[t] = [])).push(function (e) {
            i.call(a, u.state, e);
          });
        }), e.forEachAction(function (e, l) {
          var i, a, u, n, t = e.root ? l : v + l,
            o = e.handler || e;
          a = t, u = o, n = p, ((i = r)._actions[a] || (i._actions[a] = [])).push(function (e, l) {
            var a, t = u.call(i, {
              dispatch: n.dispatch,
              commit: n.commit,
              getters: n.getters,
              state: n.state,
              rootGetters: i.getters,
              rootState: i.state
            }, e, l);
            return (a = t) && "function" == typeof a.then || (t = Promise.resolve(t)), i._devtoolHook ? t.catch(function (e) {
              throw i._devtoolHook.emit("vuex:error", e), e;
            }) : t;
          });
        }), e.forEachGetter(function (e, l) {
          var a, t, i, u;
          t = v + l, i = e, u = p, (a = r)._wrappedGetters[t] || (a._wrappedGetters[t] = function (e) {
            return i(u.state, u.getters, e.state, e.getters);
          });
        }), e.forEachChild(function (e, l) {
          h(r, a, t.concat(l), e, i);
        });
      }

      function g(e, l) {
        return l.length ? l.reduce(function (e, l) {
          return e[l];
        }, e) : e;
      }

      function y(e, l, a) {
        return null !== (t = e) && "object" === (void 0 === t ? "undefined" : _typeof(t)) && e.type && (a = l, e = (l = e).type), {
          type: e,
          payload: l,
          options: a
        };
        var t;
      }

      function p(e) {
        f && e === f || t(f = e);
      }
      r.state.get = function () {
        return this._vm._data.$$state;
      }, r.state.set = function (e) {}, n.prototype.commit = function (e, l, a) {
        var t = this,
          i = y(e, l, a),
          u = i.type,
          n = i.payload,
          o = (i.options, {
            type: u,
            payload: n
          }),
          r = this._mutations[u];
        r && (this._withCommit(function () {
          r.forEach(function (e) {
            e(n);
          });
        }), this._subscribers.forEach(function (e) {
          return e(o, t.state);
        }));
      }, n.prototype.dispatch = function (e, l) {
        var a = this,
          t = y(e, l),
          i = t.type,
          u = t.payload,
          n = {
            type: i,
            payload: u
          },
          o = this._actions[i];
        if (o) return this._actionSubscribers.forEach(function (e) {
          return e(n, a.state);
        }), 1 < o.length ? Promise.all(o.map(function (e) {
          return e(u);
        })) : o[0](u);
      }, n.prototype.subscribe = function (e) {
        return b(e, this._subscribers);
      }, n.prototype.subscribeAction = function (e) {
        return b(e, this._actionSubscribers);
      }, n.prototype.watch = function (e, l, a) {
        var t = this;
        return this._watcherVM.$watch(function () {
          return e(t.state, t.getters);
        }, l, a);
      }, n.prototype.replaceState = function (e) {
        var l = this;
        this._withCommit(function () {
          l._vm._data.$$state = e;
        });
      }, n.prototype.registerModule = function (e, l, a) {
        void 0 === a && (a = {}), "string" == typeof e && (e = [e]), this._modules.register(e, l), h(this, this.state, e, this._modules.get(e), a.preserveState), d(this, this.state);
      }, n.prototype.unregisterModule = function (l) {
        var a = this;
        "string" == typeof l && (l = [l]), this._modules.unregister(l), this._withCommit(function () {
          var e = g(a.state, l.slice(0, -1));
          f.delete(e, l[l.length - 1]);
        }), s(this);
      }, n.prototype.hotUpdate = function (e) {
        this._modules.update(e), s(this, !0);
      }, n.prototype._withCommit = function (e) {
        var l = this._committing;
        this._committing = !0, e(), this._committing = l;
      }, Object.defineProperties(n.prototype, r);
      var _ = $(function (i, e) {
          var a = {};
          return T(e).forEach(function (e) {
            var l = e.key,
              t = e.val;
            a[l] = function () {
              var e = this.$store.state,
                l = this.$store.getters;
              if (i) {
                var a = k(this.$store, "mapState", i);
                if (!a) return;
                e = a.context.state, l = a.context.getters;
              }
              return "function" == typeof t ? t.call(this, e, l) : e[t];
            }, a[l].vuex = !0;
          }), a;
        }),
        m = $(function (u, e) {
          var a = {};
          return T(e).forEach(function (e) {
            var l = e.key,
              i = e.val;
            a[l] = function () {
              for (var e = [], l = arguments.length; l--;) e[l] = arguments[l];
              var a = this.$store.commit;
              if (u) {
                var t = k(this.$store, "mapMutations", u);
                if (!t) return;
                a = t.context.commit;
              }
              return "function" == typeof i ? i.apply(this, [a].concat(e)) : a.apply(this.$store, [i].concat(e));
            };
          }), a;
        }),
        S = $(function (t, e) {
          var i = {};
          return T(e).forEach(function (e) {
            var l = e.key,
              a = e.val;
            a = t + a, i[l] = function () {
              if (!t || k(this.$store, "mapGetters", t)) return this.$store.getters[a];
            }, i[l].vuex = !0;
          }), i;
        }),
        x = $(function (u, e) {
          var a = {};
          return T(e).forEach(function (e) {
            var l = e.key,
              i = e.val;
            a[l] = function () {
              for (var e = [], l = arguments.length; l--;) e[l] = arguments[l];
              var a = this.$store.dispatch;
              if (u) {
                var t = k(this.$store, "mapActions", u);
                if (!t) return;
                a = t.context.dispatch;
              }
              return "function" == typeof i ? i.apply(this, [a].concat(e)) : a.apply(this.$store, [i].concat(e));
            };
          }), a;
        }),
        w = function (e) {
          return {
            mapState: _.bind(null, e),
            mapGetters: S.bind(null, e),
            mapMutations: m.bind(null, e),
            mapActions: x.bind(null, e)
          };
        };

      function T(l) {
        return Array.isArray(l) ? l.map(function (e) {
          return {
            key: e,
            val: e
          };
        }) : Object.keys(l).map(function (e) {
          return {
            key: e,
            val: l[e]
          };
        });
      }

      function $(a) {
        return function (e, l) {
          return "string" != typeof e ? (l = e, e = "") : "/" !== e.charAt(e.length - 1) && (e += "/"), a(e, l);
        };
      }

      function k(e, l, a) {
        return e._modulesNamespaceMap[a];
      }
      var O = {
        Store: n,
        install: p,
        version: "3.0.1",
        mapState: _,
        mapMutations: m,
        mapGetters: S,
        mapActions: x,
        createNamespacedHelpers: w
      };
      l.default = O;
    },
    "32a0": function (e, l, a) {
      function m(e) {
        return e < 10 ? "0" + e : e + "";
      }
      Object.defineProperty(l, "__esModule", {
        value: !0
      }), l.initDays = function (e, l) {
        for (var a = new Date(e, l, 0).getDate(), t = [], i = 1; i <= a; i++) t.push(m(i));
        return t;
      }, l.initPicker = function (e, l) {
        for (var a = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : "date", t = 3 < arguments.length && void 0 !== arguments[3] ? arguments[3] : 1, i = new Date(e), u = new Date(l), n = i.getFullYear(), o = i.getMonth(), r = u.getFullYear(), v = [], c = [], b = [], s = [], d = [], p = new Date(n, o, 0).getDate(), f = n; f <= r; f++) v.push(f + "");
        for (var h = 1; h <= 12; h++) c.push(m(h));
        for (var g = 1; g <= p; g++) b.push(m(g));
        for (var y = 0; y < 24; y++) s.push(m(y));
        for (var _ = 0; _ < 60; _ += t) d.push(m(_));
        switch (a) {
          case "date":
            return {
              years: v, months: c, days: b
            };
          case "yearMonth":
            return {
              years: v, months: c
            };
          case "dateTime":
            return {
              years: v, months: c, days: b, hours: s, minutes: d
            };
          case "time":
            return {
              hours: s, minutes: d
            };
        }
      };
    },
    3584: function (e, l, a) {
      Object.defineProperty(l, "__esModule", {
        value: !0
      }), l.default = void 0;
      var c = /^<([-A-Za-z0-9_]+)((?:\s+[a-zA-Z_:][-a-zA-Z0-9_:.]*(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)>/,
        b = /^<\/([-A-Za-z0-9_]+)[^>]*>/,
        s = /([a-zA-Z_:][-a-zA-Z0-9_:.]*)(?:\s*=\s*(?:(?:"((?:\\.|[^"])*)")|(?:'((?:\\.|[^'])*)')|([^>\s]+)))?/g,
        d = t("area,base,basefont,br,col,frame,hr,img,input,link,meta,param,embed,command,keygen,source,track,wbr"),
        p = t("a,address,article,applet,aside,audio,blockquote,button,canvas,center,dd,del,dir,div,dl,dt,fieldset,figcaption,figure,footer,form,frameset,h1,h2,h3,h4,h5,h6,header,hgroup,hr,iframe,isindex,li,map,menu,noframes,noscript,object,ol,output,p,pre,section,script,table,tbody,td,tfoot,th,thead,tr,ul,video"),
        f = t("abbr,acronym,applet,b,basefont,bdo,big,br,button,cite,code,del,dfn,em,font,i,iframe,img,input,ins,kbd,label,map,object,q,s,samp,script,select,small,span,strike,strong,sub,sup,textarea,tt,u,var"),
        h = t("colgroup,dd,dt,li,options,p,td,tfoot,th,thead,tr"),
        g = t("checked,compact,declare,defer,disabled,ismap,multiple,nohref,noresize,noshade,nowrap,readonly,selected"),
        y = t("script,style");

      function t(e) {
        for (var l = {}, a = e.split(","), t = 0; t < a.length; t++) l[a[t]] = !0;
        return l;
      }
      var i = function (e) {
        e = e.replace(/<\?xml.*\?>\n/, "").replace(/<!doctype.*>\n/, "").replace(/<!DOCTYPE.*>\n/, "");
        var u = [],
          n = {
            node: "root",
            children: []
          };
        return function (e, u) {
          var l, a, t, n = [],
            i = e;
          for (n.last = function () {
              return this[this.length - 1];
            }; e;) {
            if (a = !0, n.last() && y[n.last()]) e = e.replace(new RegExp("([\\s\\S]*?)</" + n.last() + "[^>]*>"), function (e, l) {
              return l = l.replace(/<!--([\s\S]*?)-->|<!\[CDATA\[([\s\S]*?)]]>/g, "$1$2"), u.chars && u.chars(l), "";
            }), v(0, n.last());
            else if (0 == e.indexOf("\x3c!--") ? 0 <= (l = e.indexOf("--\x3e")) && (u.comment && u.comment(e.substring(4, l)), e = e.substring(l + 3), a = !1) : 0 == e.indexOf("</") ? (t = e.match(b)) && (e = e.substring(t[0].length), t[0].replace(b, v), a = !1) : 0 == e.indexOf("<") && (t = e.match(c)) && (e = e.substring(t[0].length), t[0].replace(c, r), a = !1), a) {
              var o = (l = e.indexOf("<")) < 0 ? e : e.substring(0, l);
              e = l < 0 ? "" : e.substring(l), u.chars && u.chars(o);
            }
            if (e == i) throw "Parse Error: " + e;
            i = e;
          }

          function r(e, l, a, t) {
            if (l = l.toLowerCase(), p[l])
              for (; n.last() && f[n.last()];) v(0, n.last());
            if (h[l] && n.last() == l && v(0, l), (t = d[l] || !!t) || n.push(l), u.start) {
              var i = [];
              a.replace(s, function (e, l) {
                var a = arguments[2] ? arguments[2] : arguments[3] ? arguments[3] : arguments[4] ? arguments[4] : g[l] ? l : "";
                i.push({
                  name: l,
                  value: a,
                  escaped: a.replace(/(^|[^\\])"/g, '$1\\"')
                });
              }), u.start && u.start(l, i, t);
            }
          }

          function v(e, l) {
            if (l)
              for (a = n.length - 1; 0 <= a && n[a] != l; a--);
            else var a = 0;
            if (0 <= a) {
              for (var t = n.length - 1; a <= t; t--) u.end && u.end(n[t]);
              n.length = a;
            }
          }
          v();
        }(e, {
          start: function (e, l, a) {
            var t = {
              name: e
            };
            if (0 !== l.length && (t.attrs = l.reduce(function (e, l) {
                var a = l.value,
                  t = l.name;
                return e[t] ? e[t] = e[t] + " " + a : e[t] = a, e;
              }, {})), a) {
              var i = u[0] || n;
              i.children || (i.children = []), i.children.push(t);
            } else u.unshift(t);
          },
          end: function (e) {
            var l = u.shift();
            if (l.name !== e && console.error("invalid state: mismatch end tag"), 0 === u.length) n.children.push(l);
            else {
              var a = u[0];
              a.children || (a.children = []), a.children.push(l);
            }
          },
          chars: function (e) {
            var l = {
              type: "text",
              text: e
            };
            if (0 === u.length) n.children.push(l);
            else {
              var a = u[0];
              a.children || (a.children = []), a.children.push(l);
            }
          },
          comment: function (e) {
            var l = {
                node: "comment",
                text: e
              },
              a = u[0];
            a.children || (a.children = []), a.children.push(l);
          }
        }), n.children;
      };
      l.default = i;
    },
    3814: function (e, l, a) {
      var t = {
        url: "https://" + domain[0] + "/api/Wxapps/",
        uniacid: _siteinfo2.default.uniacid,
        platform: _siteinfo2.default.platform
      };
      e.exports = t;
    },
    "3a48": function (e, l, a) {
      function n(e, l) {
        var a = {},
          t = new Array("星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"),
          i = new Date(e);
        i.setDate(i.getDate() + l);
        var u = i.getDay();
        return a.year = i.getFullYear(), a.month = i.getMonth() + 1 < 10 ? "0" + (i.getMonth() + 1) : i.getMonth() + 1, a.day = i.getDate() < 10 ? "0" + i.getDate() : i.getDate(), a.week = t[u], a;
      }
      e.exports = {
        getDates: function (e) {
          for (var l = (u = void 0, (u = new Date()).getFullYear() + "-" + (u.getMonth() + 1 < 10 ? "0" + (u.getMonth() + 1) : u.getMonth() + 1) + "-" + (u.getDate() < 10 ? "0" + u.getDate() : u.getDate())), a = [], t = 0; t < e; t++) {
            var i = n(l, t);
            a.push(i);
          }
          var u;
          return a;
        },
        dateLater: n,
        getLastDay: function (e, l) {
          var a = e,
            t = l++;
          12 < l && (t -= 12, a++);
          var i = new Date(a, t, 1);
          return new Date(i.getTime() - 864e5).getDate();
        }
      };
    },
    "543d": function (e, l, a) {
      Object.defineProperty(l, "__esModule", {
        value: !0
      }), l.createApp = Te, l.createComponent = Be, l.createPage = Pe, l.default = void 0;
      var t, s = (t = a("66fd")) && t.__esModule ? t : {
        default: t
      };

      function d(e, l) {
        return function (e) {
          if (Array.isArray(e)) return e;
        }(e) || function (e, l) {
          var a = [],
            t = !0,
            i = !1,
            u = void 0;
          try {
            for (var n, o = e[Symbol.iterator](); !(t = (n = o.next()).done) && (a.push(n.value), !l || a.length !== l); t = !0);
          } catch (e) {
            i = !0, u = e;
          } finally {
            try {
              t || null == o.return || o.return();
            } finally {
              if (i) throw u;
            }
          }
          return a;
        }(e, l) || function () {
          throw new TypeError("Invalid attempt to destructure non-iterable instance");
        }();
      }

      function i(e) {
        return u(e) || function (e) {
          if (Symbol.iterator in Object(e) || "[object Arguments]" === Object.prototype.toString.call(e)) return Array.from(e);
        }(e) || function () {
          throw new TypeError("Invalid attempt to spread non-iterable instance");
        }();
      }

      function u(e) {
        if (Array.isArray(e)) {
          for (var l = 0, a = new Array(e.length); l < e.length; l++) a[l] = e[l];
          return a;
        }
      }
      var n = Object.prototype.toString,
        o = Object.prototype.hasOwnProperty;

      function p(e) {
        return "function" == typeof e;
      }

      function f(e) {
        return "[object Object]" === n.call(e);
      }

      function h(e, l) {
        return o.call(e, l);
      }

      function b() {}

      function r(l) {
        var a = Object.create(null);
        return function (e) {
          return a[e] || (a[e] = l(e));
        };
      }
      var v = /-(\w)/g,
        c = r(function (e) {
          return e.replace(v, function (e, l) {
            return l ? l.toUpperCase() : "";
          });
        }),
        g = ["invoke", "success", "fail", "complete", "returnValue"],
        y = {},
        _ = {};

      function m(i, u) {
        Object.keys(u).forEach(function (e) {
          var l, a, t; - 1 !== g.indexOf(e) && p(u[e]) && (i[e] = (l = i[e], a = u[e], (t = a ? l ? l.concat(a) : Array.isArray(a) ? a : [a] : l) ? function (e) {
            for (var l = [], a = 0; a < e.length; a++) - 1 === l.indexOf(e[a]) && l.push(e[a]);
            return l;
          }(t) : t));
        });
      }

      function S(i, u) {
        i && u && Object.keys(u).forEach(function (e) {
          var l, a, t; - 1 !== g.indexOf(e) && p(u[e]) && (l = i[e], a = u[e], -1 !== (t = l.indexOf(a)) && l.splice(t, 1));
        });
      }

      function x(l) {
        return function (e) {
          return l(e) || e;
        };
      }

      function w(e) {
        return !!e && ("object" === (void 0 === e ? "undefined" : _typeof(e)) || "function" == typeof e) && "function" == typeof e.then;
      }

      function T(e, l) {
        for (var a = !1, t = 0; t < e.length; t++) {
          var i = e[t];
          if (a) a = Promise.then(x(i));
          else {
            var u = i(l);
            if (w(u) && (a = Promise.resolve(u)), !1 === u) return {
              then: function () {}
            };
          }
        }
        return a || {
          then: function (e) {
            return e(l);
          }
        };
      }

      function $(t) {
        var e = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : {};
        return ["success", "fail", "complete"].forEach(function (l) {
          if (Array.isArray(t[l])) {
            var a = e[l];
            e[l] = function (e) {
              T(t[l], e).then(function (e) {
                return p(a) && a(e) || e;
              });
            };
          }
        }), e;
      }

      function k(e, l) {
        var a = [];
        Array.isArray(y.returnValue) && a.push.apply(a, i(y.returnValue));
        var t = _[e];
        return t && Array.isArray(t.returnValue) && a.push.apply(a, i(t.returnValue)), a.forEach(function (e) {
          l = e(l) || l;
        }), l;
      }

      function O(e, l, a) {
        for (var t = arguments.length, i = new Array(3 < t ? t - 3 : 0), u = 3; u < t; u++) i[u - 3] = arguments[u];
        var n = function (e) {
          var l = Object.create(null);
          Object.keys(y).forEach(function (e) {
            "returnValue" !== e && (l[e] = y[e].slice());
          });
          var a = _[e];
          return a && Object.keys(a).forEach(function (e) {
            "returnValue" !== e && (l[e] = (l[e] || []).concat(a[e]));
          }), l;
        }(e);
        return n && Object.keys(n).length ? Array.isArray(n.invoke) ? T(n.invoke, a).then(function (e) {
          return l.apply(void 0, [$(n, e)].concat(i));
        }) : l.apply(void 0, [$(n, a)].concat(i)) : l.apply(void 0, [a].concat(i));
      }
      var A = /^\$|restoreGlobal|getCurrentSubNVue|getMenuButtonBoundingClientRect|^report|interceptors|Interceptor$|getSubNVueById|requireNativePlugin|upx2px|hideKeyboard|canIUse|^create|Sync$|Manager$|base64ToArrayBuffer|arrayBufferToBase64/,
        P = /^create|Manager$/,
        B = /^on/;

      function j(e) {
        return P.test(e);
      }

      function D(e) {
        return A.test(e);
      }

      function C(e) {
        return !(j(e) || D(e) || (l = e, B.test(l) && "onPush" !== l));
        var l;
      }

      function E(i, u) {
        return C(i) ? function () {
          for (var a = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {}, e = arguments.length, t = new Array(1 < e ? e - 1 : 0), l = 1; l < e; l++) t[l - 1] = arguments[l];
          return p(a.success) || p(a.fail) || p(a.complete) ? k(i, O.apply(void 0, [i, u, a].concat(t))) : k(i, new Promise(function (e, l) {
            O.apply(void 0, [i, u, Object.assign({}, a, {
              success: e,
              fail: l
            })].concat(t)), Promise.prototype.finally || (Promise.prototype.finally = function (l) {
              var a = this.constructor;
              return this.then(function (e) {
                return a.resolve(l()).then(function () {
                  return e;
                });
              }, function (e) {
                return a.resolve(l()).then(function () {
                  throw e;
                });
              });
            });
          }).then(function (e) {
            return [null, e];
          }).catch(function (e) {
            return [e];
          }));
        } : u;
      }
      var I = !1,
        q = 0,
        M = 0;
      var R = {
          promiseInterceptor: {
            returnValue: function (e) {
              return w(e) ? e.then(function (e) {
                return e[1];
              }).catch(function (e) {
                return e[0];
              }) : e;
            }
          }
        },
        N = Object.freeze({
          upx2px: function (e, l) {
            if (0 === q && (a = wx.getSystemInfoSync(), t = a.platform, i = a.pixelRatio, u = a.windowWidth, q = u, M = i, I = "ios" === t), 0 === (e = Number(e))) return 0;
            var a, t, i, u, n = e / 750 * (l || q);
            return n < 0 && (n = -n), 0 === (n = Math.floor(n + 1e-4)) ? 1 !== M && I ? .5 : 1 : e < 0 ? -n : n;
          },
          interceptors: R,
          addInterceptor: function (e, l) {
            "string" == typeof e && f(l) ? m(_[e] || (_[e] = {}), l) : f(e) && m(y, e);
          },
          removeInterceptor: function (e, l) {
            "string" == typeof e ? f(l) ? S(_[e], l) : delete _[e] : f(e) && S(y, e);
          }
        }),
        L = {
          previewImage: {
            args: function (e) {
              var a = parseInt(e.current);
              if (!isNaN(a)) {
                var t = e.urls;
                if (Array.isArray(t)) {
                  var l = t.length;
                  if (l) return a < 0 ? a = 0 : l <= a && (a = l - 1), 0 < a ? (e.current = t[a], e.urls = t.filter(function (e, l) {
                    return !(l < a) || e !== t[a];
                  })) : e.current = t[0], {
                    indicator: !1,
                    loop: !1
                  };
                }
              }
            }
          }
        },
        H = ["success", "fail", "cancel", "complete"];

      function U(l, a, t) {
        return function (e) {
          return a(V(l, e, t));
        };
      }

      function F(e, l) {
        var a = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {},
          t = 3 < arguments.length && void 0 !== arguments[3] ? arguments[3] : {},
          i = 4 < arguments.length && void 0 !== arguments[4] && arguments[4];
        if (f(l)) {
          var u = !0 === i ? l : {};
          for (var n in p(a) && (a = a(l, u) || {}), l)
            if (h(a, n)) {
              var o = a[n];
              p(o) && (o = o(l[n], l, u)), o ? "string" == typeof o ? u[o] = l[n] : f(o) && (u[o.name ? o.name : n] = o.value) : console.warn("微信小程序 ".concat(e, "暂不支持").concat(n));
            } else -1 !== H.indexOf(n) ? u[n] = U(e, l[n], t) : i || (u[n] = l[n]);
          return u;
        }
        return p(l) && (l = U(e, l, t)), l;
      }

      function V(e, l, a) {
        var t = 3 < arguments.length && void 0 !== arguments[3] && arguments[3];
        return p(L.returnValue) && (l = L.returnValue(e, l)), F(e, l, a, {}, t);
      }

      function z(u, e) {
        if (h(L, u)) {
          var n = L[u];
          return n ? function (e, l) {
            var a = n;
            p(n) && (a = n(e));
            var t = [e = F(u, e, a.args, a.returnValue)];
            void 0 !== l && t.push(l);
            var i = wx[a.name || u].apply(wx, t);
            return D(u) ? V(u, i, a.returnValue, j(u)) : i;
          } : function () {
            console.error("微信小程序 暂不支持".concat(u));
          };
        }
        return e;
      }
      var W = Object.create(null);
      ["onTabBarMidButtonTap", "subscribePush", "unsubscribePush", "onPush", "offPush", "share"].forEach(function (e) {
        var i;
        W[e] = (i = e, function (e) {
          var l = e.fail,
            a = e.complete,
            t = {
              errMsg: "".concat(i, ":fail:暂不支持 ").concat(i, " 方法")
            };
          p(l) && l(t), p(a) && a(t);
        });
      });
      var J = {
        oauth: ["weixin"],
        share: ["weixin"],
        payment: ["wxpay"],
        push: ["weixin"]
      };
      var G, Z = Object.freeze({
          getProvider: function (e) {
            var l = e.service,
              a = e.success,
              t = e.fail,
              i = e.complete,
              u = !1;
            J[l] ? (u = {
              errMsg: "getProvider:ok",
              service: l,
              provider: J[l]
            }, p(a) && a(u)) : (u = {
              errMsg: "getProvider:fail:服务[" + l + "]不存在"
            }, p(t) && t(u)), p(i) && i(u);
          }
        }),
        Y = "function" == typeof getUniEmitter ? getUniEmitter : function () {
          return G || (G = new s.default()), G;
        };

      function X(e, l, a) {
        return e[l].apply(e, a);
      }
      var K = Object.freeze({
          $on: function () {
            return X(Y(), "$on", Array.prototype.slice.call(arguments));
          },
          $off: function () {
            return X(Y(), "$off", Array.prototype.slice.call(arguments));
          },
          $once: function () {
            return X(Y(), "$once", Array.prototype.slice.call(arguments));
          },
          $emit: function () {
            return X(Y(), "$emit", Array.prototype.slice.call(arguments));
          }
        }),
        Q = Object.freeze({}),
        ee = Page,
        le = Component,
        ae = /:/g,
        te = r(function (e) {
          return c(e.replace(ae, "-"));
        });

      function ie(i) {
        if (wx.canIUse("nextTick")) {
          var u = i.triggerEvent;
          i.triggerEvent = function (e) {
            for (var l = arguments.length, a = new Array(1 < l ? l - 1 : 0), t = 1; t < l; t++) a[t - 1] = arguments[t];
            return u.apply(i, [te(e)].concat(a));
          };
        }
      }

      function ue(e, l) {
        var t = l[e];
        l[e] = t ? function () {
          ie(this);
          for (var e = arguments.length, l = new Array(e), a = 0; a < e; a++) l[a] = arguments[a];
          return t.apply(this, l);
        } : function () {
          ie(this);
        };
      }
      Page = function () {
        var e = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {};
        return ue("onLoad", e), ee(e);
      }, Component = function () {
        var e = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {};
        return ue("created", e), le(e);
      };

      function ne(e, l, a) {
        l.forEach(function (l) {
          (function l(a, e) {
            if (!e) return !0;
            if (s.default.options && Array.isArray(s.default.options[a])) return !0;
            if (p(e = e.default || e)) return !!p(e.extendOptions[a]) || !!(e.super && e.super.options && Array.isArray(e.super.options[a]));
            if (p(e[a])) return !0;
            var t = e.mixins;
            return Array.isArray(t) ? !!t.find(function (e) {
              return l(a, e);
            }) : void 0;
          })(l, a) && (e[l] = function (e) {
            return this.$vm && this.$vm.__call_hook(l, e);
          });
        });
      }
      var oe = [String, Number, Boolean, Object, Array, null];

      function re(a) {
        return function (e, l) {
          this.$vm && (this.$vm[a] = e);
        };
      }

      function ve(e, l, a, t) {
        return Array.isArray(l) && 1 === l.length ? l[0] : l;
      }

      function ce(i) {
        var e = 1 < arguments.length && void 0 !== arguments[1] && arguments[1],
          u = (2 < arguments.length && void 0 !== arguments[2] && arguments[2], {});
        return e || (u.vueId = {
          type: String,
          value: ""
        }, u.vueSlots = {
          type: null,
          value: [],
          observer: function (e, l) {
            var a = Object.create(null);
            e.forEach(function (e) {
              a[e] = !0;
            }), this.setData({
              $slots: a
            });
          }
        }), Array.isArray(i) ? i.forEach(function (e) {
          u[e] = {
            type: null,
            observer: re(e)
          };
        }) : f(i) && Object.keys(i).forEach(function (e) {
          var l = i[e];
          if (f(l)) {
            var a = l.default;
            p(a) && (a = a()), l.type = ve(0, l.type), u[e] = {
              type: -1 !== oe.indexOf(l.type) ? l.type : null,
              value: a,
              observer: re(e)
            };
          } else {
            var t = ve(0, l);
            u[e] = {
              type: -1 !== oe.indexOf(t) ? t : null,
              observer: re(e)
            };
          }
        }), u;
      }

      function be(a, e, t) {
        var i = {};
        return Array.isArray(e) && e.length && e.forEach(function (e, l) {
          var n, o;
          "string" == typeof e ? e ? "$event" === e ? i["$" + l] = t : 0 === e.indexOf("$event.") ? i["$" + l] = a.__get_value(e.replace("$event.", ""), t) : i["$" + l] = a.__get_value(e) : i["$" + l] = a : i["$" + l] = (o = n = a, e.forEach(function (e) {
            var l = e[0],
              a = e[2];
            if (l || void 0 !== a) {
              var t = e[1],
                i = e[3],
                u = l ? n.__get_value(l, o) : o;
              Number.isInteger(u) ? o = a : t ? Array.isArray(u) ? o = u.find(function (e) {
                return n.__get_value(t, e) === a;
              }) : f(u) ? o = Object.keys(u).find(function (e) {
                return n.__get_value(t, u[e]) === a;
              }) : console.error("v-for 暂不支持循环数据：", u) : o = u[a], i && (o = n.__get_value(i, o));
            }
          }), o);
        }), i;
      }

      function se(e, l) {
        var a = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : [],
          t = 3 < arguments.length && void 0 !== arguments[3] ? arguments[3] : [],
          i = 4 < arguments.length ? arguments[4] : void 0,
          u = 5 < arguments.length ? arguments[5] : void 0,
          n = !1;
        if (i && (n = l.currentTarget && l.currentTarget.dataset && "wx" === l.currentTarget.dataset.comType, !a.length)) return n ? [l] : l.detail.__args__ || l.detail;
        var o = be(e, t, l),
          r = [];
        return a.forEach(function (e) {
          "$event" === e ? "__set_model" !== u || i ? i && !n ? r.push(l.detail.__args__[0]) : r.push(l) : r.push(l.target.value) : Array.isArray(e) && "o" === e[0] ? r.push(function (e) {
            for (var l = {}, a = 1; a < e.length; a++) {
              var t = e[a];
              l[t[0]] = t[1];
            }
            return l;
          }(e)) : "string" == typeof e && h(o, e) ? r.push(o[e]) : r.push(e);
        }), r;
      }
      var de = "~",
        pe = "^";

      function fe(o) {
        var r = this,
          e = ((o = function (e) {
            try {
              e.mp = JSON.parse(JSON.stringify(e));
            } catch (e) {}
            return e.stopPropagation = b, e.preventDefault = b, e.target = e.target || {}, h(e, "detail") || (e.detail = {}), f(e.detail) && (e.target = Object.assign({}, e.target, e.detail)), e;
          }(o)).currentTarget || o.target).dataset;
        if (!e) return console.warn("事件信息不存在");
        var l = e.eventOpts || e["event-opts"];
        if (!l) return console.warn("事件信息不存在");
        var v = o.type,
          c = [];
        return l.forEach(function (e) {
          var l, a, t = e[0],
            i = e[1],
            u = t.charAt(0) === pe,
            n = (t = u ? t.slice(1) : t).charAt(0) === de;
          t = n ? t.slice(1) : t, i && ((l = v) === (a = t) || "regionchange" === a && ("begin" === l || "end" === l)) && i.forEach(function (e) {
            var l = e[0];
            if (l) {
              var a = r.$vm;
              if (a.$options.generic && a.$parent && a.$parent.$parent && (a = a.$parent.$parent), "$emit" === l) return void a.$emit.apply(a, se(r.$vm, o, e[1], e[2], u, l));
              var t = a[l];
              if (!p(t)) throw new Error(" _vm.".concat(l, " is not a function"));
              if (n) {
                if (t.once) return;
                t.once = !0;
              }
              c.push(t.apply(a, se(r.$vm, o, e[1], e[2], u, l)));
            }
          });
        }), "input" === v && 1 === c.length && void 0 !== c[0] ? c[0] : void 0;
      }
      var he = ["onShow", "onHide", "onError", "onPageNotFound"];

      function ge(l, e) {
        var n = e.mocks,
          o = e.initRefs;
        l.$options.store && (s.default.prototype.$store = l.$options.store), s.default.prototype.mpHost = "mp-weixin", s.default.mixin({
          beforeCreate: function () {
            var l, e, a, t, i, u;
            this.$options.mpType && (this.mpType = this.$options.mpType, this.$mp = (t = {
              data: {}
            }, i = this.mpType, u = this.$options.mpInstance, i in t ? Object.defineProperty(t, i, {
              value: u,
              enumerable: !0,
              configurable: !0,
              writable: !0
            }) : t[i] = u, t), this.$scope = this.$options.mpInstance, delete this.$options.mpType, delete this.$options.mpInstance, "app" !== this.mpType && (o(this), e = n, a = (l = this).$mp[l.mpType], e.forEach(function (e) {
              h(a, e) && (l[e] = a[e]);
            })));
          }
        });
        var a = {
          onLaunch: function (e) {
            this.$vm || (wx.canIUse("nextTick") || console.error("当前微信基础库版本过低，请将 微信开发者工具-详情-项目设置-调试基础库版本 更换为`2.3.0`以上"), this.$vm = l, this.$vm.$mp = {
              app: this
            }, (this.$vm.$scope = this).$vm.globalData = this.globalData, this.$vm._isMounted = !0, this.$vm.__call_hook("mounted", e), this.$vm.__call_hook("onLaunch", e));
          }
        };
        a.globalData = l.$options.globalData || {};
        var t = l.$options.methods;
        return t && Object.keys(t).forEach(function (e) {
          a[e] = t[e];
        }), ne(a, he), a;
      }
      var ye = ["__route__", "__wxExparserNodeId__", "__wxWebviewId__"];

      function _e(e) {
        return Behavior(e);
      }

      function me() {
        return !!this.route;
      }

      function Se(e) {
        this.triggerEvent("__l", e);
      }

      function xe(e) {
        var l = e.$scope;
        Object.defineProperty(e, "$refs", {
          get: function () {
            var a = {};
            return l.selectAllComponents(".vue-ref").forEach(function (e) {
              var l = e.dataset.ref;
              a[l] = e.$vm || e;
            }), l.selectAllComponents(".vue-ref-in-for").forEach(function (e) {
              var l = e.dataset.ref;
              a[l] || (a[l] = []), a[l].push(e.$vm || e);
            }), a;
          }
        });
      }

      function we(e) {
        var l, a = e.detail || e.value,
          t = a.vuePid,
          i = a.vueOptions;
        t && (l = function e(l, a) {
          var t = l.$children,
            i = t.find(function (e) {
              return e.$scope._$vueId === a;
            });
          if (i) return i;
          for (var u = t.length - 1; 0 <= u; u--)
            if (i = e(t[u], a)) return i;
        }(this.$vm, t)), l || (l = this.$vm), i.parent = l;
      }

      function Te(e) {
        return App(ge(e, {
          mocks: ye,
          initRefs: xe
        })), e;
      }

      function $e(e) {
        var l, a, t, i = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : {},
          u = i.isPage,
          n = i.initRelation,
          o = d((l = s.default, p(a = (a = e).default || a) ? a = (t = a).extendOptions : t = l.extend(a), [t, a]), 2),
          r = o[0],
          v = o[1],
          c = {
            multipleSlots: !0,
            addGlobalClass: !0
          };
        v["mp-weixin"] && v["mp-weixin"].options && Object.assign(c, v["mp-weixin"].options);
        var b = {
          options: c,
          data: function (e, l) {
            var a = e.data || {},
              t = e.methods || {};
            if ("function" == typeof a) try {
              a = a.call(l);
            } catch (e) {
              Object({
                NODE_ENV: "production",
                VUE_APP_PLATFORM: "mp-weixin",
                BASE_URL: "/"
              }).VUE_APP_DEBUG && console.warn("根据 Vue 的 data 函数初始化小程序 data 失败，请尽量确保 data 函数中不访问 vm 对象，否则可能影响首次数据渲染速度。", a);
            } else try {
              a = JSON.parse(JSON.stringify(a));
            } catch (e) {}
            return f(a) || (a = {}), Object.keys(t).forEach(function (e) {
              -1 !== l.__lifecycle_hooks__.indexOf(e) || h(a, e) || (a[e] = t[e]);
            }), a;
          }(v, s.default.prototype),
          behaviors: function (e, l) {
            var a = e.behaviors,
              t = e.extends,
              i = e.mixins,
              u = e.props;
            u || (e.props = u = []);
            var n = [];
            return Array.isArray(a) && a.forEach(function (e) {
              n.push(e.replace("uni://", "wx".concat("://"))), "uni://form-field" === e && (Array.isArray(u) ? (u.push("name"), u.push("value")) : (u.name = {
                type: String,
                default: ""
              }, u.value = {
                type: [String, Number, Boolean, Array, Object, Date],
                default: ""
              }));
            }), f(t) && t.props && n.push(l({
              properties: ce(t.props, !0)
            })), Array.isArray(i) && i.forEach(function (e) {
              f(e) && e.props && n.push(l({
                properties: ce(e.props, !0)
              }));
            }), n;
          }(v, _e),
          properties: ce(v.props, !1, v.__file),
          lifetimes: {
            attached: function () {
              var e, l, a, t = this.properties,
                i = {
                  mpType: u.call(this) ? "page" : "component",
                  mpInstance: this,
                  propsData: t
                };
              e = t.vueId, l = this, 1 === (a = (e = (e || "").split(",")).length) ? l._$vueId = e[0] : 2 === a && (l._$vueId = e[0], l._$vuePid = e[1]), n.call(this, {
                  vuePid: this._$vuePid,
                  vueOptions: i
                }), this.$vm = new r(i),
                function (e, l) {
                  if (Array.isArray(l) && l.length) {
                    var a = Object.create(null);
                    l.forEach(function (e) {
                      a[e] = !0;
                    }), e.$scopedSlots = e.$slots = a;
                  }
                }(this.$vm, t.vueSlots), this.$vm.$mount();
            },
            ready: function () {
              this.$vm && (this.$vm._isMounted = !0, this.$vm.__call_hook("mounted"), this.$vm.__call_hook("onReady"));
            },
            detached: function () {
              this.$vm.$destroy();
            }
          },
          pageLifetimes: {
            show: function (e) {
              this.$vm && this.$vm.__call_hook("onPageShow", e);
            },
            hide: function () {
              this.$vm && this.$vm.__call_hook("onPageHide");
            },
            resize: function (e) {
              this.$vm && this.$vm.__call_hook("onPageResize", e);
            }
          },
          methods: {
            __l: we,
            __e: fe
          }
        };
        return Array.isArray(v.wxsCallMethods) && v.wxsCallMethods.forEach(function (l) {
          b.methods[l] = function (e) {
            return this.$vm[l](e);
          };
        }), u ? b : [b, r];
      }

      function ke(e) {
        return $e(e, {
          isPage: me,
          initRelation: Se
        });
      }
      var Oe = ["onShow", "onHide", "onUnload"];

      function Ae(e) {
        return function (e, l) {
          l.isPage, l.initRelation;
          var a = ke(e);
          return ne(a.methods, Oe, e), a.methods.onLoad = function (e) {
            this.$vm.$mp.query = e, this.$vm.__call_hook("onLoad", e);
          }, a;
        }(e, {
          isPage: me,
          initRelation: Se
        });
      }

      function Pe(e) {
        return Component(Ae(e));
      }

      function Be(e) {
        return Component(ke(e));
      }
      Oe.push.apply(Oe, ["onPullDownRefresh", "onReachBottom", "onShareAppMessage", "onPageScroll", "onResize", "onTabItemTap"]), ["vibrate"].forEach(function (e) {
        L[e] = !1;
      }), [].forEach(function (e) {
        var l = L[e] && L[e].name ? L[e].name : e;
        wx.canIUse(l) || (L[e] = !1);
      });
      var je = {};
      "undefined" != typeof Proxy ? je = new Proxy({}, {
        get: function (e, l) {
          return e[l] ? e[l] : N[l] ? N[l] : Q[l] ? E(l, Q[l]) : Z[l] ? E(l, Z[l]) : W[l] ? E(l, W[l]) : K[l] ? K[l] : h(wx, l) || h(L, l) ? E(l, z(l, wx[l])) : void 0;
        },
        set: function (e, l, a) {
          return e[l] = a, !0;
        }
      }) : (Object.keys(N).forEach(function (e) {
        je[e] = N[e];
      }), Object.keys(W).forEach(function (e) {
        je[e] = E(e, W[e]);
      }), Object.keys(Z).forEach(function (e) {
        je[e] = E(e, W[e]);
      }), Object.keys(K).forEach(function (e) {
        je[e] = K[e];
      }), Object.keys(Q).forEach(function (e) {
        je[e] = E(e, Q[e]);
      }), Object.keys(wx).forEach(function (e) {
        (h(wx, e) || h(L, e)) && (je[e] = E(e, z(e, wx[e])));
      })), wx.createApp = Te, wx.createPage = Pe, wx.createComponent = Be;
      var De = je;
      l.default = De;
    },
    "66fd": function (e, Ma, l) {
      l.r(Ma),
        function (e) {
          var g = Object.freeze({});

          function y(e) {
            return null == e;
          }

          function _(e) {
            return null != e;
          }

          function m(e) {
            return !0 === e;
          }

          function f(e) {
            return "string" == typeof e || "number" == typeof e || "symbol" === (void 0 === e ? "undefined" : _typeof(e)) || "boolean" == typeof e;
          }

          function S(e) {
            return null !== e && "object" === (void 0 === e ? "undefined" : _typeof(e));
          }
          var t = Object.prototype.toString;

          function r(e) {
            return "[object Object]" === t.call(e);
          }

          function i(e) {
            var l = parseFloat(String(e));
            return 0 <= l && Math.floor(l) === l && isFinite(e);
          }

          function x(e) {
            return _(e) && "function" == typeof e.then && "function" == typeof e.catch;
          }

          function l(e) {
            return null == e ? "" : Array.isArray(e) || r(e) && e.toString === t ? JSON.stringify(e, null, 2) : String(e);
          }

          function a(e) {
            var l = parseFloat(e);
            return isNaN(l) ? e : l;
          }

          function u(e, l) {
            for (var a = Object.create(null), t = e.split(","), i = 0; i < t.length; i++) a[t[i]] = !0;
            return l ? function (e) {
              return a[e.toLowerCase()];
            } : function (e) {
              return a[e];
            };
          }
          u("slot,component", !0);
          var c = u("key,ref,slot,slot-scope,is");

          function w(e, l) {
            if (e.length) {
              var a = e.indexOf(l);
              if (-1 < a) return e.splice(a, 1);
            }
          }
          var n = Object.prototype.hasOwnProperty;

          function b(e, l) {
            return n.call(e, l);
          }

          function o(l) {
            var a = Object.create(null);
            return function (e) {
              return a[e] || (a[e] = l(e));
            };
          }
          var v = /-(\w)/g,
            s = o(function (e) {
              return e.replace(v, function (e, l) {
                return l ? l.toUpperCase() : "";
              });
            }),
            d = o(function (e) {
              return e.charAt(0).toUpperCase() + e.slice(1);
            }),
            p = /\B([A-Z])/g,
            T = o(function (e) {
              return e.replace(p, "-$1").toLowerCase();
            });
          var h = Function.prototype.bind ? function (e, l) {
            return e.bind(l);
          } : function (a, t) {
            function e(e) {
              var l = arguments.length;
              return l ? 1 < l ? a.apply(t, arguments) : a.call(t, e) : a.call(t);
            }
            return e._length = a.length, e;
          };

          function $(e, l) {
            l = l || 0;
            for (var a = e.length - l, t = new Array(a); a--;) t[a] = e[a + l];
            return t;
          }

          function k(e, l) {
            for (var a in l) e[a] = l[a];
            return e;
          }

          function O(e) {
            for (var l = {}, a = 0; a < e.length; a++) e[a] && k(l, e[a]);
            return l;
          }

          function A(e, l, a) {}
          var P = function (e, l, a) {
              return !1;
            },
            B = function (e) {
              return e;
            };

          function j(l, a) {
            if (l === a) return !0;
            var e = S(l),
              t = S(a);
            if (!e || !t) return !e && !t && String(l) === String(a);
            try {
              var i = Array.isArray(l),
                u = Array.isArray(a);
              if (i && u) return l.length === a.length && l.every(function (e, l) {
                return j(e, a[l]);
              });
              if (l instanceof Date && a instanceof Date) return l.getTime() === a.getTime();
              if (i || u) return !1;
              var n = Object.keys(l),
                o = Object.keys(a);
              return n.length === o.length && n.every(function (e) {
                return j(l[e], a[e]);
              });
            } catch (e) {
              return !1;
            }
          }

          function D(e, l) {
            for (var a = 0; a < e.length; a++)
              if (j(e[a], l)) return a;
            return -1;
          }

          function C(e) {
            var l = !1;
            return function () {
              l || (l = !0, e.apply(this, arguments));
            };
          }
          var E = ["component", "directive", "filter"],
            I = ["beforeCreate", "created", "beforeMount", "mounted", "beforeUpdate", "updated", "beforeDestroy", "destroyed", "activated", "deactivated", "errorCaptured", "serverPrefetch"],
            q = {
              optionMergeStrategies: Object.create(null),
              silent: !1,
              productionTip: !1,
              devtools: !1,
              performance: !1,
              errorHandler: null,
              warnHandler: null,
              ignoredElements: [],
              keyCodes: Object.create(null),
              isReservedTag: P,
              isReservedAttr: P,
              isUnknownElement: P,
              getTagNamespace: A,
              parsePlatformTagName: B,
              mustUseProp: P,
              async: !0,
              _lifecycleHooks: I
            };

          function M(e, l, a, t) {
            Object.defineProperty(e, l, {
              value: a,
              enumerable: !!t,
              writable: !0,
              configurable: !0
            });
          }
          var R = new RegExp("[^" + /a-zA-Z\u00B7\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u037D\u037F-\u1FFF\u200C-\u200D\u203F-\u2040\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD/.source + ".$_\\d]");
          var N, L = "__proto__" in {},
            H = "undefined" != typeof window,
            U = "undefined" != typeof WXEnvironment && !!WXEnvironment.platform,
            F = U && WXEnvironment.platform.toLowerCase(),
            V = H && window.navigator.userAgent.toLowerCase(),
            z = V && /msie|trident/.test(V),
            W = (V && V.indexOf("msie 9.0"), V && V.indexOf("edge/"), V && V.indexOf("android"), V && /iphone|ipad|ipod|ios/.test(V) || "ios" === F),
            J = (V && /chrome\/\d+/.test(V), V && /phantomjs/.test(V), V && V.match(/firefox\/(\d+)/), {}.watch);
          if (H) try {
            var G = {};
            Object.defineProperty(G, "passive", {
              get: function () {}
            }), window.addEventListener("test-passive", null, G);
          } catch (e) {}
          var Z = function () {
              return void 0 === N && (N = !H && !U && void 0 !== e && e.process && "server" === e.process.env.VUE_ENV), N;
            },
            Y = H && window.__VUE_DEVTOOLS_GLOBAL_HOOK__;

          function X(e) {
            return "function" == typeof e && /native code/.test(e.toString());
          }
          var K, Q = "undefined" != typeof Symbol && X(Symbol) && "undefined" != typeof Reflect && X(Reflect.ownKeys);
          K = "undefined" != typeof Set && X(Set) ? Set : function () {
            function e() {
              this.set = Object.create(null);
            }
            return e.prototype.has = function (e) {
              return !0 === this.set[e];
            }, e.prototype.add = function (e) {
              this.set[e] = !0;
            }, e.prototype.clear = function () {
              this.set = Object.create(null);
            }, e;
          }();
          var ee = A,
            le = 0,
            ae = function () {
              this.id = le++, this.subs = [];
            };

          function te(e) {
            ae.SharedObject.targetStack.push(e), ae.SharedObject.target = e;
          }

          function ie() {
            ae.SharedObject.targetStack.pop(), ae.SharedObject.target = ae.SharedObject.targetStack[ae.SharedObject.targetStack.length - 1];
          }
          ae.prototype.addSub = function (e) {
            this.subs.push(e);
          }, ae.prototype.removeSub = function (e) {
            w(this.subs, e);
          }, ae.prototype.depend = function () {
            ae.SharedObject.target && ae.SharedObject.target.addDep(this);
          }, ae.prototype.notify = function () {
            for (var e = this.subs.slice(), l = 0, a = e.length; l < a; l++) e[l].update();
          }, ae.SharedObject = "undefined" != typeof SharedObject ? SharedObject : {}, ae.SharedObject.target = null, ae.SharedObject.targetStack = [];
          var ue = function (e, l, a, t, i, u, n, o) {
              this.tag = e, this.data = l, this.children = a, this.text = t, this.elm = i, this.ns = void 0, this.context = u, this.fnContext = void 0, this.fnOptions = void 0, this.fnScopeId = void 0, this.key = l && l.key, this.componentOptions = n, this.componentInstance = void 0, this.parent = void 0, this.raw = !1, this.isStatic = !1, this.isRootInsert = !0, this.isComment = !1, this.isCloned = !1, this.isOnce = !1, this.asyncFactory = o, this.asyncMeta = void 0, this.isAsyncPlaceholder = !1;
            },
            ne = {
              child: {
                configurable: !0
              }
            };
          ne.child.get = function () {
            return this.componentInstance;
          }, Object.defineProperties(ue.prototype, ne);
          var oe = function (e) {
            void 0 === e && (e = "");
            var l = new ue();
            return l.text = e, l.isComment = !0, l;
          };

          function re(e) {
            return new ue(void 0, void 0, void 0, String(e));
          }
          var ve = Array.prototype,
            ce = Object.create(ve);
          ["push", "pop", "shift", "unshift", "splice", "sort", "reverse"].forEach(function (u) {
            var n = ve[u];
            M(ce, u, function () {
              for (var e = [], l = arguments.length; l--;) e[l] = arguments[l];
              var a, t = n.apply(this, e),
                i = this.__ob__;
              switch (u) {
                case "push":
                case "unshift":
                  a = e;
                  break;
                case "splice":
                  a = e.slice(2);
              }
              return a && i.observeArray(a), i.dep.notify(), t;
            });
          });
          var be = Object.getOwnPropertyNames(ce),
            se = !0;

          function de(e) {
            se = e;
          }
          var pe = function (e) {
            var l;
            this.value = e, this.dep = new ae(), this.vmCount = 0, M(e, "__ob__", this), Array.isArray(e) ? (L ? e.push !== e.__proto__.push ? fe(e, ce, be) : (l = ce, e.__proto__ = l) : fe(e, ce, be), this.observeArray(e)) : this.walk(e);
          };

          function fe(e, l, a) {
            for (var t = 0, i = a.length; t < i; t++) {
              var u = a[t];
              M(e, u, l[u]);
            }
          }

          function he(e, l) {
            var a;
            if (S(e) && !(e instanceof ue)) return b(e, "__ob__") && e.__ob__ instanceof pe ? a = e.__ob__ : se && !Z() && (Array.isArray(e) || r(e)) && Object.isExtensible(e) && !e._isVue && (a = new pe(e)), l && a && a.vmCount++, a;
          }

          function ge(a, e, t, l, i) {
            var u = new ae(),
              n = Object.getOwnPropertyDescriptor(a, e);
            if (!n || !1 !== n.configurable) {
              var o = n && n.get,
                r = n && n.set;
              o && !r || 2 !== arguments.length || (t = a[e]);
              var v = !i && he(t);
              Object.defineProperty(a, e, {
                enumerable: !0,
                configurable: !0,
                get: function () {
                  var e = o ? o.call(a) : t;
                  return ae.SharedObject.target && (u.depend(), v && (v.dep.depend(), Array.isArray(e) && function e(l) {
                    for (var a = void 0, t = 0, i = l.length; t < i; t++)(a = l[t]) && a.__ob__ && a.__ob__.dep.depend(), Array.isArray(a) && e(a);
                  }(e))), e;
                },
                set: function (e) {
                  var l = o ? o.call(a) : t;
                  e === l || e != e && l != l || o && !r || (r ? r.call(a, e) : t = e, v = !i && he(e), u.notify());
                }
              });
            }
          }

          function ye(e, l, a) {
            if (Array.isArray(e) && i(l)) return e.length = Math.max(e.length, l), e.splice(l, 1, a), a;
            if (l in e && !(l in Object.prototype)) return e[l] = a;
            var t = e.__ob__;
            return e._isVue || t && t.vmCount || (t ? (ge(t.value, l, a), t.dep.notify()) : e[l] = a), a;
          }

          function _e(e, l) {
            if (Array.isArray(e) && i(l)) e.splice(l, 1);
            else {
              var a = e.__ob__;
              e._isVue || a && a.vmCount || b(e, l) && (delete e[l], a && a.dep.notify());
            }
          }
          pe.prototype.walk = function (e) {
            for (var l = Object.keys(e), a = 0; a < l.length; a++) ge(e, l[a]);
          }, pe.prototype.observeArray = function (e) {
            for (var l = 0, a = e.length; l < a; l++) he(e[l]);
          };
          var me = q.optionMergeStrategies;

          function Se(e, l) {
            if (!l) return e;
            for (var a, t, i, u = Q ? Reflect.ownKeys(l) : Object.keys(l), n = 0; n < u.length; n++) "__ob__" !== (a = u[n]) && (t = e[a], i = l[a], b(e, a) ? t !== i && r(t) && r(i) && Se(t, i) : ye(e, a, i));
            return e;
          }

          function xe(a, t, i) {
            return i ? function () {
              var e = "function" == typeof t ? t.call(i, i) : t,
                l = "function" == typeof a ? a.call(i, i) : a;
              return e ? Se(e, l) : l;
            } : t ? a ? function () {
              return Se("function" == typeof t ? t.call(this, this) : t, "function" == typeof a ? a.call(this, this) : a);
            } : t : a;
          }

          function we(e, l) {
            var a = l ? e ? e.concat(l) : Array.isArray(l) ? l : [l] : e;
            return a ? function (e) {
              for (var l = [], a = 0; a < e.length; a++) - 1 === l.indexOf(e[a]) && l.push(e[a]);
              return l;
            }(a) : a;
          }

          function Te(e, l, a, t) {
            var i = Object.create(e || null);
            return l ? k(i, l) : i;
          }
          me.data = function (e, l, a) {
            return a ? xe(e, l, a) : l && "function" != typeof l ? e : xe(e, l);
          }, I.forEach(function (e) {
            me[e] = we;
          }), E.forEach(function (e) {
            me[e + "s"] = Te;
          }), me.watch = function (e, l, a, t) {
            if (e === J && (e = void 0), l === J && (l = void 0), !l) return Object.create(e || null);
            if (!e) return l;
            var i = {};
            for (var u in k(i, e), l) {
              var n = i[u],
                o = l[u];
              n && !Array.isArray(n) && (n = [n]), i[u] = n ? n.concat(o) : Array.isArray(o) ? o : [o];
            }
            return i;
          }, me.props = me.methods = me.inject = me.computed = function (e, l, a, t) {
            if (!e) return l;
            var i = Object.create(null);
            return k(i, e), l && k(i, l), i;
          }, me.provide = xe;
          var $e = function (e, l) {
            return void 0 === l ? e : l;
          };

          function ke(a, t, i) {
            if ("function" == typeof t && (t = t.options), function (e, l) {
                var a = e.props;
                if (a) {
                  var t, i, u = {};
                  if (Array.isArray(a))
                    for (t = a.length; t--;) "string" == typeof (i = a[t]) && (u[s(i)] = {
                      type: null
                    });
                  else if (r(a))
                    for (var n in a) i = a[n], u[s(n)] = r(i) ? i : {
                      type: i
                    };
                  e.props = u;
                }
              }(t), function (e, l) {
                var a = e.inject;
                if (a) {
                  var t = e.inject = {};
                  if (Array.isArray(a))
                    for (var i = 0; i < a.length; i++) t[a[i]] = {
                      from: a[i]
                    };
                  else if (r(a))
                    for (var u in a) {
                      var n = a[u];
                      t[u] = r(n) ? k({
                        from: u
                      }, n) : {
                        from: n
                      };
                    }
                }
              }(t), function (e) {
                var l = e.directives;
                if (l)
                  for (var a in l) {
                    var t = l[a];
                    "function" == typeof t && (l[a] = {
                      bind: t,
                      update: t
                    });
                  }
              }(t), !t._base && (t.extends && (a = ke(a, t.extends, i)), t.mixins))
              for (var e = 0, l = t.mixins.length; e < l; e++) a = ke(a, t.mixins[e], i);
            var u, n = {};
            for (u in a) o(u);
            for (u in t) b(a, u) || o(u);

            function o(e) {
              var l = me[e] || $e;
              n[e] = l(a[e], t[e], i, e);
            }
            return n;
          }

          function Oe(e, l, a, t) {
            if ("string" == typeof a) {
              var i = e[l];
              if (b(i, a)) return i[a];
              var u = s(a);
              if (b(i, u)) return i[u];
              var n = d(u);
              return b(i, n) ? i[n] : i[a] || i[u] || i[n];
            }
          }

          function Ae(e, l, a, t) {
            var i = l[e],
              u = !b(a, e),
              n = a[e],
              o = je(Boolean, i.type);
            if (-1 < o)
              if (u && !b(i, "default")) n = !1;
              else if ("" === n || n === T(e)) {
              var r = je(String, i.type);
              (r < 0 || o < r) && (n = !0);
            }
            if (void 0 === n) {
              n = function (e, l, a) {
                if (b(l, "default")) {
                  var t = l.default;
                  return e && e.$options.propsData && void 0 === e.$options.propsData[a] && void 0 !== e._props[a] ? e._props[a] : "function" == typeof t && "Function" !== Pe(l.type) ? t.call(e) : t;
                }
              }(t, i, e);
              var v = se;
              de(!0), he(n), de(v);
            }
            return n;
          }

          function Pe(e) {
            var l = e && e.toString().match(/^\s*function (\w+)/);
            return l ? l[1] : "";
          }

          function Be(e, l) {
            return Pe(e) === Pe(l);
          }

          function je(e, l) {
            if (!Array.isArray(l)) return Be(l, e) ? 0 : -1;
            for (var a = 0, t = l.length; a < t; a++)
              if (Be(l[a], e)) return a;
            return -1;
          }

          function De(e, l, a) {
            te();
            try {
              if (l)
                for (var t = l; t = t.$parent;) {
                  var i = t.$options.errorCaptured;
                  if (i)
                    for (var u = 0; u < i.length; u++) try {
                      if (!1 === i[u].call(t, e, l, a)) return;
                    } catch (e) {
                      Ee(e, t, "errorCaptured hook");
                    }
                }
              Ee(e, l, a);
            } finally {
              ie();
            }
          }

          function Ce(e, l, a, t, i) {
            var u;
            try {
              (u = a ? e.apply(l, a) : e.call(l)) && !u._isVue && x(u) && !u._handled && (u.catch(function (e) {
                return De(e, t, i + " (Promise/async)");
              }), u._handled = !0);
            } catch (e) {
              De(e, t, i);
            }
            return u;
          }

          function Ee(l, e, a) {
            if (q.errorHandler) try {
              return q.errorHandler.call(null, l, e, a);
            } catch (e) {
              e !== l && Ie(e, null, "config.errorHandler");
            }
            Ie(l, e, a);
          }

          function Ie(e, l, a) {
            if (!H && !U || "undefined" == typeof console) throw e;
            console.error(e);
          }
          var qe, Me = [],
            Re = !1;

          function Ne() {
            Re = !1;
            for (var e = Me.slice(0), l = Me.length = 0; l < e.length; l++) e[l]();
          }
          if ("undefined" != typeof Promise && X(Promise)) {
            var Le = Promise.resolve();
            qe = function () {
              Le.then(Ne), W && setTimeout(A);
            };
          } else if (z || "undefined" == typeof MutationObserver || !X(MutationObserver) && "[object MutationObserverConstructor]" !== MutationObserver.toString()) qe = "undefined" != typeof setImmediate && X(setImmediate) ? function () {
            setImmediate(Ne);
          } : function () {
            setTimeout(Ne, 0);
          };
          else {
            var He = 1,
              Ue = new MutationObserver(Ne),
              Fe = document.createTextNode(String(He));
            Ue.observe(Fe, {
              characterData: !0
            }), qe = function () {
              He = (He + 1) % 2, Fe.data = String(He);
            };
          }

          function Ve(e, l) {
            var a;
            if (Me.push(function () {
                if (e) try {
                  e.call(l);
                } catch (e) {
                  De(e, l, "nextTick");
                } else a && a(l);
              }), Re || (Re = !0, qe()), !e && "undefined" != typeof Promise) return new Promise(function (e) {
              a = e;
            });
          }
          var ze = new K();

          function We(e) {
            (function e(l, a) {
              var t, i, u = Array.isArray(l);
              if (!(!u && !S(l) || Object.isFrozen(l) || l instanceof ue)) {
                if (l.__ob__) {
                  var n = l.__ob__.dep.id;
                  if (a.has(n)) return;
                  a.add(n);
                }
                if (u)
                  for (t = l.length; t--;) e(l[t], a);
                else
                  for (i = Object.keys(l), t = i.length; t--;) e(l[i[t]], a);
              }
            })(e, ze), ze.clear();
          }
          var Je = o(function (e) {
            var l = "&" === e.charAt(0),
              a = "~" === (e = l ? e.slice(1) : e).charAt(0),
              t = "!" === (e = a ? e.slice(1) : e).charAt(0);
            return {
              name: e = t ? e.slice(1) : e,
              once: a,
              capture: t,
              passive: l
            };
          });

          function Ge(e, i) {
            function u() {
              var e = arguments,
                l = u.fns;
              if (!Array.isArray(l)) return Ce(l, null, arguments, i, "v-on handler");
              for (var a = l.slice(), t = 0; t < a.length; t++) Ce(a[t], null, e, i, "v-on handler");
            }
            return u.fns = e, u;
          }

          function Ze(e, l, a, t, i) {
            if (_(l)) {
              if (b(l, a)) return e[a] = l[a], i || delete l[a], !0;
              if (b(l, t)) return e[a] = l[t], i || delete l[t], !0;
            }
            return !1;
          }

          function Ye(e) {
            return f(e) ? [re(e)] : Array.isArray(e) ? function e(l, a) {
              var t, i, u, n, o = [];
              for (t = 0; t < l.length; t++) y(i = l[t]) || "boolean" == typeof i || (u = o.length - 1, n = o[u], Array.isArray(i) ? 0 < i.length && (Xe((i = e(i, (a || "") + "_" + t))[0]) && Xe(n) && (o[u] = re(n.text + i[0].text), i.shift()), o.push.apply(o, i)) : f(i) ? Xe(n) ? o[u] = re(n.text + i) : "" !== i && o.push(re(i)) : Xe(i) && Xe(n) ? o[u] = re(n.text + i.text) : (m(l._isVList) && _(i.tag) && y(i.key) && _(a) && (i.key = "__vlist" + a + "_" + t + "__"), o.push(i)));
              return o;
            }(e) : void 0;
          }

          function Xe(e) {
            return _(e) && _(e.text) && !1 === e.isComment;
          }

          function Ke(e) {
            var l = e.$options.provide;
            l && (e._provided = "function" == typeof l ? l.call(e) : l);
          }

          function Qe(l) {
            var a = el(l.$options.inject, l);
            a && (de(!1), Object.keys(a).forEach(function (e) {
              ge(l, e, a[e]);
            }), de(!0));
          }

          function el(e, l) {
            if (e) {
              for (var a = Object.create(null), t = Q ? Reflect.ownKeys(e) : Object.keys(e), i = 0; i < t.length; i++) {
                var u = t[i];
                if ("__ob__" !== u) {
                  for (var n = e[u].from, o = l; o;) {
                    if (o._provided && b(o._provided, n)) {
                      a[u] = o._provided[n];
                      break;
                    }
                    o = o.$parent;
                  }
                  if (!o && "default" in e[u]) {
                    var r = e[u].default;
                    a[u] = "function" == typeof r ? r.call(l) : r;
                  }
                }
              }
              return a;
            }
          }

          function ll(e, l) {
            if (!e || !e.length) return {};
            for (var a = {}, t = 0, i = e.length; t < i; t++) {
              var u = e[t],
                n = u.data;
              if (n && n.attrs && n.attrs.slot && delete n.attrs.slot, u.context !== l && u.fnContext !== l || !n || null == n.slot) u.asyncMeta && u.asyncMeta.data && "page" === u.asyncMeta.data.slot ? (a.page || (a.page = [])).push(u) : (a.default || (a.default = [])).push(u);
              else {
                var o = n.slot,
                  r = a[o] || (a[o] = []);
                "template" === u.tag ? r.push.apply(r, u.children || []) : r.push(u);
              }
            }
            for (var v in a) a[v].every(al) && delete a[v];
            return a;
          }

          function al(e) {
            return e.isComment && !e.asyncFactory || " " === e.text;
          }

          function tl(e, l, a) {
            var t, i = 0 < Object.keys(l).length,
              u = e ? !!e.$stable : !i,
              n = e && e.$key;
            if (e) {
              if (e._normalized) return e._normalized;
              if (u && a && a !== g && n === a.$key && !i && !a.$hasNormal) return a;
              for (var o in t = {}, e) e[o] && "$" !== o[0] && (t[o] = il(l, o, e[o]));
            } else t = {};
            for (var r in l) r in t || (t[r] = ul(l, r));
            return e && Object.isExtensible(e) && (e._normalized = t), M(t, "$stable", u), M(t, "$key", n), M(t, "$hasNormal", i), t;
          }

          function il(e, l, a) {
            var t = function () {
              var e = arguments.length ? a.apply(null, arguments) : a({});
              return (e = e && "object" === (void 0 === e ? "undefined" : _typeof(e)) && !Array.isArray(e) ? [e] : Ye(e)) && (0 === e.length || 1 === e.length && e[0].isComment) ? void 0 : e;
            };
            return a.proxy && Object.defineProperty(e, l, {
              get: t,
              enumerable: !0,
              configurable: !0
            }), t;
          }

          function ul(e, l) {
            return function () {
              return e[l];
            };
          }

          function nl(e, l) {
            var a, t, i, u, n;
            if (Array.isArray(e) || "string" == typeof e)
              for (a = new Array(e.length), t = 0, i = e.length; t < i; t++) a[t] = l(e[t], t);
            else if ("number" == typeof e)
              for (a = new Array(e), t = 0; t < e; t++) a[t] = l(t + 1, t);
            else if (S(e))
              if (Q && e[Symbol.iterator]) {
                a = [];
                for (var o = e[Symbol.iterator](), r = o.next(); !r.done;) a.push(l(r.value, a.length)), r = o.next();
              } else
                for (u = Object.keys(e), a = new Array(u.length), t = 0, i = u.length; t < i; t++) n = u[t], a[t] = l(e[n], n, t);
            return _(a) || (a = []), a._isVList = !0, a;
          }

          function ol(e, l, a, t) {
            var i, u = this.$scopedSlots[e];
            u ? (a = a || {}, t && (a = k(k({}, t), a)), i = u(a) || l) : i = this.$slots[e] || l;
            var n = a && a.slot;
            return n ? this.$createElement("template", {
              slot: n
            }, i) : i;
          }

          function rl(e) {
            return Oe(this.$options, "filters", e) || B;
          }

          function vl(e, l) {
            return Array.isArray(e) ? -1 === e.indexOf(l) : e !== l;
          }

          function cl(e, l, a, t, i) {
            var u = q.keyCodes[l] || a;
            return i && t && !q.keyCodes[l] ? vl(i, t) : u ? vl(u, e) : t ? T(t) !== l : void 0;
          }

          function bl(i, u, n, o, r) {
            if (n && S(n)) {
              var v;
              Array.isArray(n) && (n = O(n));
              var e = function (l) {
                if ("class" === l || "style" === l || c(l)) v = i;
                else {
                  var e = i.attrs && i.attrs.type;
                  v = o || q.mustUseProp(u, e, l) ? i.domProps || (i.domProps = {}) : i.attrs || (i.attrs = {});
                }
                var a = s(l),
                  t = T(l);
                a in v || t in v || (v[l] = n[l], !r) || ((i.on || (i.on = {}))["update:" + l] = function (e) {
                  n[l] = e;
                });
              };
              for (var l in n) e(l);
            }
            return i;
          }

          function sl(e, l) {
            var a = this._staticTrees || (this._staticTrees = []),
              t = a[e];
            return t && !l || pl(t = a[e] = this.$options.staticRenderFns[e].call(this._renderProxy, null, this), "__static__" + e, !1), t;
          }

          function dl(e, l, a) {
            return pl(e, "__once__" + l + (a ? "_" + a : ""), !0), e;
          }

          function pl(e, l, a) {
            if (Array.isArray(e))
              for (var t = 0; t < e.length; t++) e[t] && "string" != typeof e[t] && fl(e[t], l + "_" + t, a);
            else fl(e, l, a);
          }

          function fl(e, l, a) {
            e.isStatic = !0, e.key = l, e.isOnce = a;
          }

          function hl(e, l) {
            if (l && r(l)) {
              var a = e.on = e.on ? k({}, e.on) : {};
              for (var t in l) {
                var i = a[t],
                  u = l[t];
                a[t] = i ? [].concat(i, u) : u;
              }
            }
            return e;
          }

          function gl(e, l, a, t) {
            l = l || {
              $stable: !a
            };
            for (var i = 0; i < e.length; i++) {
              var u = e[i];
              Array.isArray(u) ? gl(u, l, a) : u && (u.proxy && (u.fn.proxy = !0), l[u.key] = u.fn);
            }
            return t && (l.$key = t), l;
          }

          function yl(e, l) {
            for (var a = 0; a < l.length; a += 2) {
              var t = l[a];
              "string" == typeof t && t && (e[l[a]] = l[a + 1]);
            }
            return e;
          }

          function _l(e, l) {
            return "string" == typeof e ? l + e : e;
          }

          function ml(e) {
            e._o = dl, e._n = a, e._s = l, e._l = nl, e._t = ol, e._q = j, e._i = D, e._m = sl, e._f = rl, e._k = cl, e._b = bl, e._v = re, e._e = oe, e._u = gl, e._g = hl, e._d = yl, e._p = _l;
          }

          function Sl(e, l, a, u, t) {
            var n, i = this,
              o = t.options;
            b(u, "_uid") ? (n = Object.create(u))._original = u : u = (n = u)._original;
            var r = m(o._compiled),
              v = !r;
            this.data = e, this.props = l, this.children = a, this.parent = u, this.listeners = e.on || g, this.injections = el(o.inject, u), this.slots = function () {
              return i.$slots || tl(e.scopedSlots, i.$slots = ll(a, u)), i.$slots;
            }, Object.defineProperty(this, "scopedSlots", {
              enumerable: !0,
              get: function () {
                return tl(e.scopedSlots, this.slots());
              }
            }), r && (this.$options = o, this.$slots = this.slots(), this.$scopedSlots = tl(e.scopedSlots, this.$slots)), o._scopeId ? this._c = function (e, l, a, t) {
              var i = Bl(n, e, l, a, t, v);
              return i && !Array.isArray(i) && (i.fnScopeId = o._scopeId, i.fnContext = u), i;
            } : this._c = function (e, l, a, t) {
              return Bl(n, e, l, a, t, v);
            };
          }

          function xl(e, l, a, t, i) {
            var u, n, o = ((n = new ue((u = e).tag, u.data, u.children && u.children.slice(), u.text, u.elm, u.context, u.componentOptions, u.asyncFactory)).ns = u.ns, n.isStatic = u.isStatic, n.key = u.key, n.isComment = u.isComment, n.fnContext = u.fnContext, n.fnOptions = u.fnOptions, n.fnScopeId = u.fnScopeId, n.asyncMeta = u.asyncMeta, n.isCloned = !0, n);
            return o.fnContext = a, o.fnOptions = t, l.slot && ((o.data || (o.data = {})).slot = l.slot), o;
          }

          function wl(e, l) {
            for (var a in l) e[s(a)] = l[a];
          }
          ml(Sl.prototype);
          var Tl = {
              init: function (e, l) {
                if (e.componentInstance && !e.componentInstance._isDestroyed && e.data.keepAlive) {
                  var a = e;
                  Tl.prepatch(a, a);
                } else {
                  (e.componentInstance = (i = {
                    _isComponent: !0,
                    _parentVnode: t = e,
                    parent: Rl
                  }, _(u = t.data.inlineTemplate) && (i.render = u.render, i.staticRenderFns = u.staticRenderFns), new t.componentOptions.Ctor(i))).$mount(l ? e.elm : void 0, l);
                }
                var t, i, u;
              },
              prepatch: function (e, l) {
                var a = l.componentOptions;
                ! function (e, l, a, t, i) {
                  var u = t.data.scopedSlots,
                    n = e.$scopedSlots,
                    o = !!(u && !u.$stable || n !== g && !n.$stable || u && e.$scopedSlots.$key !== u.$key),
                    r = !!(i || e.$options._renderChildren || o);
                  if (e.$options._parentVnode = t, e.$vnode = t, e._vnode && (e._vnode.parent = t), e.$options._renderChildren = i, e.$attrs = t.data.attrs || g, e.$listeners = a || g, l && e.$options.props) {
                    de(!1);
                    for (var v = e._props, c = e.$options._propKeys || [], b = 0; b < c.length; b++) {
                      var s = c[b],
                        d = e.$options.props;
                      v[s] = Ae(s, d, l, e);
                    }
                    de(!0), e.$options.propsData = l;
                  }
                  a = a || g;
                  var p = e.$options._parentListeners;
                  e.$options._parentListeners = a, Ml(e, a, p), r && (e.$slots = ll(i, t.context), e.$forceUpdate());
                }(l.componentInstance = e.componentInstance, a.propsData, a.listeners, l, a.children);
              },
              insert: function (e) {
                var l, a = e.context,
                  t = e.componentInstance;
                t._isMounted || (t._isMounted = !0, Hl(t, "mounted")), e.data.keepAlive && (a._isMounted ? ((l = t)._inactive = !1, Fl.push(l)) : Ll(t, !0));
              },
              destroy: function (e) {
                var l = e.componentInstance;
                l._isDestroyed || (e.data.keepAlive ? function e(l, a) {
                  if (!(a && (l._directInactive = !0, Nl(l)) || l._inactive)) {
                    l._inactive = !0;
                    for (var t = 0; t < l.$children.length; t++) e(l.$children[t]);
                    Hl(l, "deactivated");
                  }
                }(l, !0) : l.$destroy());
              }
            },
            $l = Object.keys(Tl);

          function kl(e, l, a, t, i) {
            if (!y(e)) {
              var u = a.$options._base;
              if (S(e) && (e = u.extend(e)), "function" == typeof e) {
                var n;
                if (y(e.cid) && void 0 === (e = function (l, a) {
                    if (m(l.error) && _(l.errorComp)) return l.errorComp;
                    if (_(l.resolved)) return l.resolved;
                    var e = Dl;
                    if (e && _(l.owners) && -1 === l.owners.indexOf(e) && l.owners.push(e), m(l.loading) && _(l.loadingComp)) return l.loadingComp;
                    if (e && !_(l.owners)) {
                      var t = l.owners = [e],
                        i = !0,
                        u = null,
                        n = null;
                      e.$on("hook:destroyed", function () {
                        return w(t, e);
                      });
                      var o = function (e) {
                          for (var l = 0, a = t.length; l < a; l++) t[l].$forceUpdate();
                          e && (t.length = 0, null !== u && (clearTimeout(u), u = null), null !== n && (clearTimeout(n), n = null));
                        },
                        r = C(function (e) {
                          l.resolved = Cl(e, a), i ? t.length = 0 : o(!0);
                        }),
                        v = C(function (e) {
                          _(l.errorComp) && (l.error = !0, o(!0));
                        }),
                        c = l(r, v);
                      return S(c) && (x(c) ? y(l.resolved) && c.then(r, v) : x(c.component) && (c.component.then(r, v), _(c.error) && (l.errorComp = Cl(c.error, a)), _(c.loading) && (l.loadingComp = Cl(c.loading, a), 0 === c.delay ? l.loading = !0 : u = setTimeout(function () {
                        u = null, y(l.resolved) && y(l.error) && (l.loading = !0, o(!1));
                      }, c.delay || 200)), _(c.timeout) && (n = setTimeout(function () {
                        n = null, y(l.resolved) && v(null);
                      }, c.timeout)))), i = !1, l.loading ? l.loadingComp : l.resolved;
                    }
                  }(n = e, u))) return b = n, s = l, d = a, p = t, f = i, (h = oe()).asyncFactory = b, h.asyncMeta = {
                  data: s,
                  context: d,
                  children: p,
                  tag: f
                }, h;
                l = l || {}, da(e), _(l.model) && function (e, l) {
                  var a = e.model && e.model.prop || "value",
                    t = e.model && e.model.event || "input";
                  (l.attrs || (l.attrs = {}))[a] = l.model.value;
                  var i = l.on || (l.on = {}),
                    u = i[t],
                    n = l.model.callback;
                  _(u) ? (Array.isArray(u) ? -1 === u.indexOf(n) : u !== n) && (i[t] = [n].concat(u)) : i[t] = n;
                }(e.options, l);
                var o = function (e, l, a) {
                  var t = l.options.props;
                  if (!y(t)) {
                    var i = {},
                      u = e.attrs,
                      n = e.props;
                    if (_(u) || _(n))
                      for (var o in t) {
                        var r = T(o);
                        Ze(i, n, o, r, !0) || Ze(i, u, o, r, !1);
                      }
                    return i;
                  }
                }(l, e);
                if (m(e.options.functional)) return function (e, l, a, t, i) {
                  var u = e.options,
                    n = {},
                    o = u.props;
                  if (_(o))
                    for (var r in o) n[r] = Ae(r, o, l || g);
                  else _(a.attrs) && wl(n, a.attrs), _(a.props) && wl(n, a.props);
                  var v = new Sl(a, n, i, t, e),
                    c = u.render.call(null, v._c, v);
                  if (c instanceof ue) return xl(c, a, v.parent, u);
                  if (Array.isArray(c)) {
                    for (var b = Ye(c) || [], s = new Array(b.length), d = 0; d < b.length; d++) s[d] = xl(b[d], a, v.parent, u);
                    return s;
                  }
                }(e, o, l, a, t);
                var r = l.on;
                if (l.on = l.nativeOn, m(e.options.abstract)) {
                  var v = l.slot;
                  l = {}, v && (l.slot = v);
                }! function (e) {
                  for (var l = e.hook || (e.hook = {}), a = 0; a < $l.length; a++) {
                    var t = $l[a],
                      i = l[t],
                      u = Tl[t];
                    i === u || i && i._merged || (l[t] = i ? Ol(u, i) : u);
                  }
                }(l);
                var c = e.options.name || i;
                return new ue("vue-component-" + e.cid + (c ? "-" + c : ""), l, void 0, void 0, void 0, a, {
                  Ctor: e,
                  propsData: o,
                  listeners: r,
                  tag: i,
                  children: t
                }, n);
              }
            }
            var b, s, d, p, f, h;
          }

          function Ol(a, t) {
            var e = function (e, l) {
              a(e, l), t(e, l);
            };
            return e._merged = !0, e;
          }
          var Al = 1,
            Pl = 2;

          function Bl(e, l, a, t, i, u) {
            return (Array.isArray(a) || f(a)) && (i = t, t = a, a = void 0), m(u) && (i = Pl), n = e, o = l, v = t, c = i, _(r = a) && _(r.__ob__) ? oe() : (_(r) && _(r.is) && (o = r.is), o ? (Array.isArray(v) && "function" == typeof v[0] && ((r = r || {}).scopedSlots = {
              default: v[0]
            }, v.length = 0), c === Pl ? v = Ye(v) : c === Al && (v = function (e) {
              for (var l = 0; l < e.length; l++)
                if (Array.isArray(e[l])) return Array.prototype.concat.apply([], e);
              return e;
            }(v)), "string" == typeof o ? (d = n.$vnode && n.$vnode.ns || q.getTagNamespace(o), s = q.isReservedTag(o) ? new ue(q.parsePlatformTagName(o), r, v, void 0, void 0, n) : r && r.pre || !_(p = Oe(n.$options, "components", o)) ? new ue(o, r, v, void 0, void 0, n) : kl(p, r, n, v, o)) : s = kl(o, r, n, v), Array.isArray(s) ? s : _(s) ? (_(d) && function e(l, a, t) {
              if (l.ns = a, "foreignObject" === l.tag && (t = !(a = void 0)), _(l.children))
                for (var i = 0, u = l.children.length; i < u; i++) {
                  var n = l.children[i];
                  _(n.tag) && (y(n.ns) || m(t) && "svg" !== n.tag) && e(n, a, t);
                }
            }(s, d), _(r) && (S((b = r).style) && We(b.style), S(b.class) && We(b.class)), s) : oe()) : oe());
            var n, o, r, v, c, b, s, d, p;
          }
          var jl, Dl = null;

          function Cl(e, l) {
            return (e.__esModule || Q && "Module" === e[Symbol.toStringTag]) && (e = e.default), S(e) ? l.extend(e) : e;
          }

          function El(e, l) {
            jl.$on(e, l);
          }

          function Il(e, l) {
            jl.$off(e, l);
          }

          function ql(l, a) {
            var t = jl;
            return function e() {
              null !== a.apply(null, arguments) && t.$off(l, e);
            };
          }

          function Ml(e, l, a) {
            (function (e, l, a, t, i, u) {
              var n, o, r, v;
              for (n in e) o = e[n], r = l[n], v = Je(n), y(o) || (y(r) ? (y(o.fns) && (o = e[n] = Ge(o, u)), m(v.once) && (o = e[n] = i(v.name, o, v.capture)), a(v.name, o, v.capture, v.passive, v.params)) : o !== r && (r.fns = o, e[n] = r));
              for (n in l) y(e[n]) && t((v = Je(n)).name, l[n], v.capture);
            })(l, a || {}, El, Il, ql, jl = e), jl = void 0;
          }
          var Rl = null;

          function Nl(e) {
            for (; e && (e = e.$parent);)
              if (e._inactive) return !0;
            return !1;
          }

          function Ll(e, l) {
            if (l) {
              if (e._directInactive = !1, Nl(e)) return;
            } else if (e._directInactive) return;
            if (e._inactive || null === e._inactive) {
              e._inactive = !1;
              for (var a = 0; a < e.$children.length; a++) Ll(e.$children[a]);
              Hl(e, "activated");
            }
          }

          function Hl(e, l) {
            te();
            var a = e.$options[l],
              t = l + " hook";
            if (a)
              for (var i = 0, u = a.length; i < u; i++) Ce(a[i], e, null, e, t);
            e._hasHookEvent && e.$emit("hook:" + l), ie();
          }
          var Ul = [],
            Fl = [],
            Vl = {},
            zl = !1,
            Wl = !1,
            Jl = 0;
          var Gl = Date.now;
          if (H && !z) {
            var Zl = window.performance;
            Zl && "function" == typeof Zl.now && Gl() > document.createEvent("Event").timeStamp && (Gl = function () {
              return Zl.now();
            });
          }

          function Yl() {
            var e, l;
            for (Gl(), Wl = !0, Ul.sort(function (e, l) {
                return e.id - l.id;
              }), Jl = 0; Jl < Ul.length; Jl++)(e = Ul[Jl]).before && e.before(), l = e.id, Vl[l] = null, e.run();
            var a = Fl.slice(),
              t = Ul.slice();
            Jl = Ul.length = Fl.length = 0, zl = Wl = !(Vl = {}),
              function (e) {
                for (var l = 0; l < e.length; l++) e[l]._inactive = !0, Ll(e[l], !0);
              }(a),
              function (e) {
                var l = e.length;
                for (; l--;) {
                  var a = e[l],
                    t = a.vm;
                  t._watcher === a && t._isMounted && !t._isDestroyed && Hl(t, "updated");
                }
              }(t), Y && q.devtools && Y.emit("flush");
          }
          var Xl = 0,
            Kl = function (e, l, a, t, i) {
              this.vm = e, i && (e._watcher = this), e._watchers.push(this), t ? (this.deep = !!t.deep, this.user = !!t.user, this.lazy = !!t.lazy, this.sync = !!t.sync, this.before = t.before) : this.deep = this.user = this.lazy = this.sync = !1, this.cb = a, this.id = ++Xl, this.active = !0, this.dirty = this.lazy, this.deps = [], this.newDeps = [], this.depIds = new K(), this.newDepIds = new K(), this.expression = "", "function" == typeof l ? this.getter = l : (this.getter = function (e) {
                if (!R.test(e)) {
                  var a = e.split(".");
                  return function (e) {
                    for (var l = 0; l < a.length; l++) {
                      if (!e) return;
                      e = e[a[l]];
                    }
                    return e;
                  };
                }
              }(l), this.getter || (this.getter = A)), this.value = this.lazy ? void 0 : this.get();
            };
          Kl.prototype.get = function () {
            var e;
            te(this);
            var l = this.vm;
            try {
              e = this.getter.call(l, l);
            } catch (e) {
              if (!this.user) throw e;
              De(e, l, 'getter for watcher "' + this.expression + '"');
            } finally {
              this.deep && We(e), ie(), this.cleanupDeps();
            }
            return e;
          }, Kl.prototype.addDep = function (e) {
            var l = e.id;
            this.newDepIds.has(l) || (this.newDepIds.add(l), this.newDeps.push(e), this.depIds.has(l) || e.addSub(this));
          }, Kl.prototype.cleanupDeps = function () {
            for (var e = this.deps.length; e--;) {
              var l = this.deps[e];
              this.newDepIds.has(l.id) || l.removeSub(this);
            }
            var a = this.depIds;
            this.depIds = this.newDepIds, this.newDepIds = a, this.newDepIds.clear(), a = this.deps, this.deps = this.newDeps, this.newDeps = a, this.newDeps.length = 0;
          }, Kl.prototype.update = function () {
            this.lazy ? this.dirty = !0 : this.sync ? this.run() : function (e) {
              var l = e.id;
              if (null == Vl[l]) {
                if (Vl[l] = !0, Wl) {
                  for (var a = Ul.length - 1; Jl < a && Ul[a].id > e.id;) a--;
                  Ul.splice(a + 1, 0, e);
                } else Ul.push(e);
                zl || (zl = !0, Ve(Yl));
              }
            }(this);
          }, Kl.prototype.run = function () {
            if (this.active) {
              var e = this.get();
              if (e !== this.value || S(e) || this.deep) {
                var l = this.value;
                if (this.value = e, this.user) try {
                  this.cb.call(this.vm, e, l);
                } catch (e) {
                  De(e, this.vm, 'callback for watcher "' + this.expression + '"');
                } else this.cb.call(this.vm, e, l);
              }
            }
          }, Kl.prototype.evaluate = function () {
            this.value = this.get(), this.dirty = !1;
          }, Kl.prototype.depend = function () {
            for (var e = this.deps.length; e--;) this.deps[e].depend();
          }, Kl.prototype.teardown = function () {
            if (this.active) {
              this.vm._isBeingDestroyed || w(this.vm._watchers, this);
              for (var e = this.deps.length; e--;) this.deps[e].removeSub(this);
              this.active = !1;
            }
          };
          var Ql = {
            enumerable: !0,
            configurable: !0,
            get: A,
            set: A
          };

          function ea(e, l, a) {
            Ql.get = function () {
              return this[l][a];
            }, Ql.set = function (e) {
              this[l][a] = e;
            }, Object.defineProperty(e, a, Ql);
          }

          function la(e) {
            e._watchers = [];
            var l = e.$options;
            l.props && function (a, t) {
              var i = a.$options.propsData || {},
                u = a._props = {},
                n = a.$options._propKeys = [];
              !a.$parent || de(!1);
              var e = function (e) {
                n.push(e);
                var l = Ae(e, t, i, a);
                ge(u, e, l), e in a || ea(a, "_props", e);
              };
              for (var l in t) e(l);
              de(!0);
            }(e, l.props), l.methods && function (e, l) {
              for (var a in e.$options.props, l) e[a] = "function" != typeof l[a] ? A : h(l[a], e);
            }(e, l.methods), l.data ? function (e) {
              var l = e.$options.data;
              r(l = e._data = "function" == typeof l ? function (e, l) {
                te();
                try {
                  return e.call(l, l);
                } catch (e) {
                  return De(e, l, "data()"), {};
                } finally {
                  ie();
                }
              }(l, e) : l || {}) || (l = {});
              var a = Object.keys(l),
                t = e.$options.props,
                i = (e.$options.methods, a.length);
              for (; i--;) {
                var u = a[i];
                t && b(t, u) || (void 0, 36 === (n = (u + "").charCodeAt(0)) || 95 === n) || ea(e, "_data", u);
              }
              var n;
              he(l, !0);
            }(e) : he(e._data = {}, !0), l.computed && function (e, l) {
              var a = e._computedWatchers = Object.create(null),
                t = Z();
              for (var i in l) {
                var u = l[i],
                  n = "function" == typeof u ? u : u.get;
                t || (a[i] = new Kl(e, n || A, A, aa)), i in e || ta(e, i, u);
              }
            }(e, l.computed), l.watch && l.watch !== J && function (e, l) {
              for (var a in l) {
                var t = l[a];
                if (Array.isArray(t))
                  for (var i = 0; i < t.length; i++) na(e, a, t[i]);
                else na(e, a, t);
              }
            }(e, l.watch);
          }
          var aa = {
            lazy: !0
          };

          function ta(e, l, a) {
            var t = !Z();
            "function" == typeof a ? (Ql.get = t ? ia(l) : ua(a), Ql.set = A) : (Ql.get = a.get ? t && !1 !== a.cache ? ia(l) : ua(a.get) : A, Ql.set = a.set || A), Object.defineProperty(e, l, Ql);
          }

          function ia(l) {
            return function () {
              var e = this._computedWatchers && this._computedWatchers[l];
              if (e) return e.dirty && e.evaluate(), ae.SharedObject.target && e.depend(), e.value;
            };
          }

          function ua(e) {
            return function () {
              return e.call(this, this);
            };
          }

          function na(e, l, a, t) {
            return r(a) && (a = (t = a).handler), "string" == typeof a && (a = e[a]), e.$watch(l, a, t);
          }
          var oa, ra, va, ca, ba, sa = 0;

          function da(e) {
            var l = e.options;
            if (e.super) {
              var a = da(e.super);
              if (a !== e.superOptions) {
                e.superOptions = a;
                var t = function (e) {
                  var l, a = e.options,
                    t = e.sealedOptions;
                  for (var i in a) a[i] !== t[i] && (l || (l = {}), l[i] = a[i]);
                  return l;
                }(e);
                t && k(e.extendOptions, t), (l = e.options = ke(a, e.extendOptions)).name && (l.components[l.name] = e);
              }
            }
            return l;
          }

          function pa(e) {
            this._init(e);
          }

          function fa(e) {
            e.cid = 0;
            var n = 1;
            e.extend = function (e) {
              e = e || {};
              var l = this,
                a = l.cid,
                t = e._Ctor || (e._Ctor = {});
              if (t[a]) return t[a];
              var i = e.name || l.options.name,
                u = function (e) {
                  this._init(e);
                };
              return ((u.prototype = Object.create(l.prototype)).constructor = u).cid = n++, u.options = ke(l.options, e), u.super = l, u.options.props && function (e) {
                var l = e.options.props;
                for (var a in l) ea(e.prototype, "_props", a);
              }(u), u.options.computed && function (e) {
                var l = e.options.computed;
                for (var a in l) ta(e.prototype, a, l[a]);
              }(u), u.extend = l.extend, u.mixin = l.mixin, u.use = l.use, E.forEach(function (e) {
                u[e] = l[e];
              }), i && (u.options.components[i] = u), u.superOptions = l.options, u.extendOptions = e, u.sealedOptions = k({}, u.options), t[a] = u;
            };
          }

          function ha(e) {
            return e && (e.Ctor.options.name || e.tag);
          }

          function ga(e, l) {
            return Array.isArray(e) ? -1 < e.indexOf(l) : "string" == typeof e ? -1 < e.split(",").indexOf(l) : (a = e, !("[object RegExp]" !== t.call(a)) && e.test(l));
            var a;
          }

          function ya(e, l) {
            var a = e.cache,
              t = e.keys,
              i = e._vnode;
            for (var u in a) {
              var n = a[u];
              if (n) {
                var o = ha(n.componentOptions);
                o && !l(o) && _a(a, u, t, i);
              }
            }
          }

          function _a(e, l, a, t) {
            var i = e[l];
            !i || t && i.tag === t.tag || i.componentInstance.$destroy(), e[l] = null, w(a, l);
          }
          pa.prototype._init = function (e) {
            var l = this;
            l._uid = sa++, l._isVue = !0, e && e._isComponent ? function (e, l) {
                var a = e.$options = Object.create(e.constructor.options),
                  t = l._parentVnode;
                a.parent = l.parent;
                var i = (a._parentVnode = t).componentOptions;
                a.propsData = i.propsData, a._parentListeners = i.listeners, a._renderChildren = i.children, a._componentTag = i.tag, l.render && (a.render = l.render, a.staticRenderFns = l.staticRenderFns);
              }(l, e) : l.$options = ke(da(l.constructor), e || {}, l),
              function (e) {
                var l = e.$options,
                  a = l.parent;
                if (a && !l.abstract) {
                  for (; a.$options.abstract && a.$parent;) a = a.$parent;
                  a.$children.push(e);
                }
                e.$parent = a, e.$root = a ? a.$root : e, e.$children = [], e.$refs = {}, e._watcher = null, e._inactive = null, e._directInactive = !1, e._isMounted = !1, e._isDestroyed = !1, e._isBeingDestroyed = !1;
              }((l._renderProxy = l)._self = l),
              function (e) {
                e._events = Object.create(null), e._hasHookEvent = !1;
                var l = e.$options._parentListeners;
                l && Ml(e, l);
              }(l),
              function (i) {
                i._vnode = null, i._staticTrees = null;
                var e = i.$options,
                  l = i.$vnode = e._parentVnode,
                  a = l && l.context;
                i.$slots = ll(e._renderChildren, a), i.$scopedSlots = g, i._c = function (e, l, a, t) {
                  return Bl(i, e, l, a, t, !1);
                }, i.$createElement = function (e, l, a, t) {
                  return Bl(i, e, l, a, t, !0);
                };
                var t = l && l.data;
                ge(i, "$attrs", t && t.attrs || g, null, !0), ge(i, "$listeners", e._parentListeners || g, null, !0);
              }(l), Hl(l, "beforeCreate"), "mp-toutiao" !== l.mpHost && Qe(l), la(l), "mp-toutiao" !== l.mpHost && Ke(l), "mp-toutiao" !== l.mpHost && Hl(l, "created"), l.$options.el && l.$mount(l.$options.el);
          }, ba = pa, Object.defineProperty(ba.prototype, "$data", {
            get: function () {
              return this._data;
            }
          }), Object.defineProperty(ba.prototype, "$props", {
            get: function () {
              return this._props;
            }
          }), ba.prototype.$set = ye, ba.prototype.$delete = _e, ba.prototype.$watch = function (e, l, a) {
            var t = this;
            if (r(l)) return na(t, e, l, a);
            (a = a || {}).user = !0;
            var i = new Kl(t, e, l, a);
            if (a.immediate) try {
              l.call(t, i.value);
            } catch (e) {
              De(e, t, 'callback for immediate watcher "' + i.expression + '"');
            }
            return function () {
              i.teardown();
            };
          }, ca = /^hook:/, (va = pa).prototype.$on = function (e, l) {
            var a = this;
            if (Array.isArray(e))
              for (var t = 0, i = e.length; t < i; t++) a.$on(e[t], l);
            else(a._events[e] || (a._events[e] = [])).push(l), ca.test(e) && (a._hasHookEvent = !0);
            return a;
          }, va.prototype.$once = function (e, l) {
            var a = this;

            function t() {
              a.$off(e, t), l.apply(a, arguments);
            }
            return t.fn = l, a.$on(e, t), a;
          }, va.prototype.$off = function (e, l) {
            var a = this;
            if (!arguments.length) return a._events = Object.create(null), a;
            if (Array.isArray(e)) {
              for (var t = 0, i = e.length; t < i; t++) a.$off(e[t], l);
              return a;
            }
            var u, n = a._events[e];
            if (!n) return a;
            if (!l) return a._events[e] = null, a;
            for (var o = n.length; o--;)
              if ((u = n[o]) === l || u.fn === l) {
                n.splice(o, 1);
                break;
              }
            return a;
          }, va.prototype.$emit = function (e) {
            var l = this._events[e];
            if (l) {
              l = 1 < l.length ? $(l) : l;
              for (var a = $(arguments, 1), t = 'event handler for "' + e + '"', i = 0, u = l.length; i < u; i++) Ce(l[i], this, a, this, t);
            }
            return this;
          }, (ra = pa).prototype._update = function (e, l) {
            var a, t = this,
              i = t.$el,
              u = t._vnode,
              n = (a = Rl, Rl = t, function () {
                Rl = a;
              });
            t._vnode = e, t.$el = u ? t.__patch__(u, e) : t.__patch__(t.$el, e, l, !1), n(), i && (i.__vue__ = null), t.$el && (t.$el.__vue__ = t), t.$vnode && t.$parent && t.$vnode === t.$parent._vnode && (t.$parent.$el = t.$el);
          }, ra.prototype.$forceUpdate = function () {
            this._watcher && this._watcher.update();
          }, ra.prototype.$destroy = function () {
            var e = this;
            if (!e._isBeingDestroyed) {
              Hl(e, "beforeDestroy"), e._isBeingDestroyed = !0;
              var l = e.$parent;
              !l || l._isBeingDestroyed || e.$options.abstract || w(l.$children, e), e._watcher && e._watcher.teardown();
              for (var a = e._watchers.length; a--;) e._watchers[a].teardown();
              e._data.__ob__ && e._data.__ob__.vmCount--, e._isDestroyed = !0, e.__patch__(e._vnode, null), Hl(e, "destroyed"), e.$off(), e.$el && (e.$el.__vue__ = null), e.$vnode && (e.$vnode.parent = null);
            }
          }, ml((oa = pa).prototype), oa.prototype.$nextTick = function (e) {
            return Ve(e, this);
          }, oa.prototype._render = function () {
            var l, a = this,
              e = a.$options,
              t = e.render,
              i = e._parentVnode;
            i && (a.$scopedSlots = tl(i.data.scopedSlots, a.$slots, a.$scopedSlots)), a.$vnode = i;
            try {
              Dl = a, l = t.call(a._renderProxy, a.$createElement);
            } catch (e) {
              De(e, a, "render"), l = a._vnode;
            } finally {
              Dl = null;
            }
            return Array.isArray(l) && 1 === l.length && (l = l[0]), l instanceof ue || (l = oe()), l.parent = i, l;
          };
          var ma, Sa, xa, wa = [String, RegExp, Array],
            Ta = {
              KeepAlive: {
                name: "keep-alive",
                abstract: !0,
                props: {
                  include: wa,
                  exclude: wa,
                  max: [String, Number]
                },
                created: function () {
                  this.cache = Object.create(null), this.keys = [];
                },
                destroyed: function () {
                  for (var e in this.cache) _a(this.cache, e, this.keys);
                },
                mounted: function () {
                  var e = this;
                  this.$watch("include", function (l) {
                    ya(e, function (e) {
                      return ga(l, e);
                    });
                  }), this.$watch("exclude", function (l) {
                    ya(e, function (e) {
                      return !ga(l, e);
                    });
                  });
                },
                render: function () {
                  var e = this.$slots.default,
                    l = function (e) {
                      if (Array.isArray(e))
                        for (var l = 0; l < e.length; l++) {
                          var a = e[l];
                          if (_(a) && (_(a.componentOptions) || (t = a).isComment && t.asyncFactory)) return a;
                        }
                      var t;
                    }(e),
                    a = l && l.componentOptions;
                  if (a) {
                    var t = ha(a),
                      i = this.include,
                      u = this.exclude;
                    if (i && (!t || !ga(i, t)) || u && t && ga(u, t)) return l;
                    var n = this.cache,
                      o = this.keys,
                      r = null == l.key ? a.Ctor.cid + (a.tag ? "::" + a.tag : "") : l.key;
                    n[r] ? (l.componentInstance = n[r].componentInstance, w(o, r), o.push(r)) : (n[r] = l, o.push(r), this.max && o.length > parseInt(this.max) && _a(n, o[0], o, this._vnode)), l.data.keepAlive = !0;
                  }
                  return l || e && e[0];
                }
              }
            };
          ma = pa, xa = {
            get: function () {
              return q;
            }
          }, Object.defineProperty(ma, "config", xa), ma.util = {
            warn: ee,
            extend: k,
            mergeOptions: ke,
            defineReactive: ge
          }, ma.set = ye, ma.delete = _e, ma.nextTick = Ve, ma.observable = function (e) {
            return he(e), e;
          }, ma.options = Object.create(null), E.forEach(function (e) {
            ma.options[e + "s"] = Object.create(null);
          }), k((ma.options._base = ma).options.components, Ta), ma.use = function (e) {
            var l = this._installedPlugins || (this._installedPlugins = []);
            if (-1 < l.indexOf(e)) return this;
            var a = $(arguments, 1);
            return a.unshift(this), "function" == typeof e.install ? e.install.apply(e, a) : "function" == typeof e && e.apply(null, a), l.push(e), this;
          }, ma.mixin = function (e) {
            return this.options = ke(this.options, e), this;
          }, fa(ma), Sa = ma, E.forEach(function (a) {
            Sa[a] = function (e, l) {
              return l ? ("component" === a && r(l) && (l.name = l.name || e, l = this.options._base.extend(l)), "directive" === a && "function" == typeof l && (l = {
                bind: l,
                update: l
              }), this.options[a + "s"][e] = l) : this.options[a + "s"][e];
            };
          }), Object.defineProperty(pa.prototype, "$isServer", {
            get: Z
          }), Object.defineProperty(pa.prototype, "$ssrContext", {
            get: function () {
              return this.$vnode && this.$vnode.ssrContext;
            }
          }), Object.defineProperty(pa, "FunctionalRenderContext", {
            value: Sl
          }), pa.version = "2.6.10";
          var $a = "[object Array]",
            ka = "[object Object]";

          function Oa(e, l) {
            var a = {};
            return function a(t, e) {
                if (t !== e) {
                  var l = Pa(t),
                    i = Pa(e);
                  if (l == ka && i == ka) {
                    if (Object.keys(t).length >= Object.keys(e).length)
                      for (var u in e) {
                        var n = t[u];
                        void 0 === n ? t[u] = null : a(n, e[u]);
                      }
                  } else l == $a && i == $a && t.length >= e.length && e.forEach(function (e, l) {
                    a(t[l], e);
                  });
                }
              }(e, l),
              function n(o, r, v, c) {
                if (o !== r) {
                  var e = Pa(o),
                    l = Pa(r);
                  if (e == ka)
                    if (l != ka || Object.keys(o).length < Object.keys(r).length) Aa(c, v, o);
                    else {
                      var a = function (a) {
                        var e = o[a],
                          t = r[a],
                          l = Pa(e),
                          i = Pa(t);
                        if (l != $a && l != ka) e != r[a] && Aa(c, ("" == v ? "" : v + ".") + a, e);
                        else if (l == $a) i != $a ? Aa(c, ("" == v ? "" : v + ".") + a, e) : e.length < t.length ? Aa(c, ("" == v ? "" : v + ".") + a, e) : e.forEach(function (e, l) {
                          n(e, t[l], ("" == v ? "" : v + ".") + a + "[" + l + "]", c);
                        });
                        else if (l == ka)
                          if (i != ka || Object.keys(e).length < Object.keys(t).length) Aa(c, ("" == v ? "" : v + ".") + a, e);
                          else
                            for (var u in e) n(e[u], t[u], ("" == v ? "" : v + ".") + a + "." + u, c);
                      };
                      for (var t in o) a(t);
                    }
                  else e == $a ? l != $a ? Aa(c, v, o) : o.length < r.length ? Aa(c, v, o) : o.forEach(function (e, l) {
                    n(e, r[l], v + "[" + l + "]", c);
                  }) : Aa(c, v, o);
                }
              }(e, l, "", a), a;
          }

          function Aa(e, l, a) {
            e[l] = a;
          }

          function Pa(e) {
            return Object.prototype.toString.call(e);
          }

          function Ba(e) {
            if (e.__next_tick_callbacks && e.__next_tick_callbacks.length) {
              if (Object({
                  NODE_ENV: "production",
                  VUE_APP_PLATFORM: "mp-weixin",
                  BASE_URL: "/"
                }).VUE_APP_DEBUG) {
                var l = e.$scope;
                console.log("[" + new Date() + "][" + (l.is || l.route) + "][" + e._uid + "]:flushCallbacks[" + e.__next_tick_callbacks.length + "]");
              }
              for (var a = e.__next_tick_callbacks.slice(0), t = e.__next_tick_callbacks.length = 0; t < a.length; t++) a[t]();
            }
          }

          function ja(l, e) {
            if (!l.__next_tick_pending && (t = l, !Ul.find(function (e) {
                return t._watcher === e;
              }))) {
              if (Object({
                  NODE_ENV: "production",
                  VUE_APP_PLATFORM: "mp-weixin",
                  BASE_URL: "/"
                }).VUE_APP_DEBUG) {
                var a = l.$scope;
                console.log("[" + new Date() + "][" + (a.is || a.route) + "][" + l._uid + "]:nextVueTick");
              }
              return Ve(e, l);
            }
            var t, i;
            if (Object({
                NODE_ENV: "production",
                VUE_APP_PLATFORM: "mp-weixin",
                BASE_URL: "/"
              }).VUE_APP_DEBUG) {
              var u = l.$scope;
              console.log("[" + new Date() + "][" + (u.is || u.route) + "][" + l._uid + "]:nextMPTick");
            }
            if (l.__next_tick_callbacks || (l.__next_tick_callbacks = []), l.__next_tick_callbacks.push(function () {
                if (e) try {
                  e.call(l);
                } catch (e) {
                  De(e, l, "nextTick");
                } else i && i(l);
              }), !e && "undefined" != typeof Promise) return new Promise(function (e) {
              i = e;
            });
          }

          function Da() {}

          function Ca(e) {
            return Array.isArray(e) ? function (e) {
              for (var l, a = "", t = 0, i = e.length; t < i; t++) _(l = Ca(e[t])) && "" !== l && (a && (a += " "), a += l);
              return a;
            }(e) : S(e) ? function (e) {
              var l = "";
              for (var a in e) e[a] && (l && (l += " "), l += a);
              return l;
            }(e) : "string" == typeof e ? e : "";
          }
          var Ea = o(function (e) {
            var a = {},
              t = /:(.+)/;
            return e.split(/;(?![^(]*\))/g).forEach(function (e) {
              if (e) {
                var l = e.split(t);
                1 < l.length && (a[l[0].trim()] = l[1].trim());
              }
            }), a;
          });
          var Ia = ["createSelectorQuery", "createIntersectionObserver", "selectAllComponents", "selectComponent"];
          var qa = ["onLaunch", "onShow", "onHide", "onUniNViewMessage", "onError", "onLoad", "onReady", "onUnload", "onPullDownRefresh", "onReachBottom", "onTabItemTap", "onShareAppMessage", "onResize", "onPageScroll", "onNavigationBarButtonTap", "onBackPress", "onNavigationBarSearchInputChanged", "onNavigationBarSearchInputConfirmed", "onNavigationBarSearchInputClicked", "onPageShow", "onPageHide", "onPageResize"];
          pa.prototype.__patch__ = function (e, l) {
              var a, t, i = this;
              if (null !== l && ("page" === this.mpType || "component" === this.mpType)) {
                var u = this.$scope,
                  n = Object.create(null);
                try {
                  a = this, t = Object.create(null), [].concat(Object.keys(a._data || {}), Object.keys(a._computedWatchers || {})).reduce(function (e, l) {
                    return e[l] = a[l], e;
                  }, t), Object.assign(t, a.$mp.data || {}), Array.isArray(a.$options.behaviors) && -1 !== a.$options.behaviors.indexOf("uni://form-field") && (t.name = a.name, t.value = a.value), n = JSON.parse(JSON.stringify(t));
                } catch (e) {
                  console.error(e);
                }
                n.__webviewId__ = u.data.__webviewId__;
                var o = Object.create(null);
                Object.keys(n).forEach(function (e) {
                  o[e] = u.data[e];
                });
                var r = Oa(n, o);
                Object.keys(r).length ? (Object({
                  NODE_ENV: "production",
                  VUE_APP_PLATFORM: "mp-weixin",
                  BASE_URL: "/"
                }).VUE_APP_DEBUG && console.log("[" + new Date() + "][" + (u.is || u.route) + "][" + this._uid + "]差量更新", JSON.stringify(r)), this.__next_tick_pending = !0, u.setData(r, function () {
                  i.__next_tick_pending = !1, Ba(i);
                })) : Ba(this);
              }
            }, pa.prototype.$mount = function (e, l) {
              return t = l, (a = this).mpType && ("app" === a.mpType && (a.$options.render = Da), a.$options.render || (a.$options.render = Da), "mp-toutiao" !== a.mpHost && Hl(a, "beforeMount"), new Kl(a, function () {
                a._update(a._render(), t);
              }, A, {
                before: function () {
                  a._isMounted && !a._isDestroyed && Hl(a, "beforeUpdate");
                }
              }, !0), t = !1), a;
              var a, t;
            },
            function (e) {
              var t = e.extend;
              e.extend = function (l) {
                var a = (l = l || {}).methods;
                return a && Object.keys(a).forEach(function (e) {
                  -1 !== qa.indexOf(e) && (l[e] = a[e], delete a[e]);
                }), t.call(this, l);
              };
              var l = e.config.optionMergeStrategies,
                a = l.created;
              qa.forEach(function (e) {
                l[e] = a;
              }), e.prototype.__lifecycle_hooks__ = qa;
            }(pa),
            function (e) {
              e.config.errorHandler = function (e) {
                console.error(e);
              };
              var l = e.prototype.$emit;
              e.prototype.$emit = function (e) {
                return this.$scope && e && this.$scope.triggerEvent(e, {
                  __args__: $(arguments, 1)
                }), l.apply(this, arguments);
              }, e.prototype.$nextTick = function (e) {
                return ja(this, e);
              }, Ia.forEach(function (l) {
                e.prototype[l] = function (e) {
                  if (this.$scope) return this.$scope[l](e);
                };
              }), e.prototype.__init_provide = Ke, e.prototype.__init_injections = Qe, e.prototype.__call_hook = function (e, l) {
                var a = this;
                te();
                var t, i = a.$options[e],
                  u = e + " hook";
                if (i)
                  for (var n = 0, o = i.length; n < o; n++) t = Ce(i[n], a, l ? [l] : null, a, u);
                return a._hasHookEvent && a.$emit("hook:" + e), ie(), t;
              }, e.prototype.__set_model = function (e, l, a, t) {
                Array.isArray(t) && (-1 !== t.indexOf("trim") && (a = a.trim()), -1 !== t.indexOf("number") && (a = this._n(a))), e || (e = this), e[l] = a;
              }, e.prototype.__set_sync = function (e, l, a) {
                e || (e = this), e[l] = a;
              }, e.prototype.__get_orig = function (e) {
                return r(e) && e.$orig || e;
              }, e.prototype.__get_value = function (e, l) {
                return function e(l, a) {
                  var t = a.split("."),
                    i = t[0];
                  return 0 === i.indexOf("__$n") && (i = parseInt(i.replace("__$n", ""))), 1 === t.length ? l[i] : e(l[i], t.slice(1).join("."));
                }(l || this, e);
              }, e.prototype.__get_class = function (e, l) {
                return t = e, _(a = l) || _(t) ? (i = a, u = Ca(t), i ? u ? i + " " + u : i : u || "") : "";
                var a, t, i, u;
              }, e.prototype.__get_style = function (e, l) {
                if (!e && !l) return "";
                var a, t = (a = e, Array.isArray(a) ? O(a) : "string" == typeof a ? Ea(a) : a),
                  i = l ? k(l, t) : t;
                return Object.keys(i).map(function (e) {
                  return T(e) + ":" + i[e];
                }).join(";");
              }, e.prototype.__map = function (e, l) {
                var a, t, i, u, n;
                if (Array.isArray(e)) {
                  for (a = new Array(e.length), t = 0, i = e.length; t < i; t++) a[t] = l(e[t], t);
                  return a;
                }
                if (S(e)) {
                  for (u = Object.keys(e), a = Object.create(null), t = 0, i = u.length; t < i; t++) a[n = u[t]] = l(e[n], n, t);
                  return a;
                }
                return [];
              };
            }(pa), Ma.default = pa;
        }.call(this, l("c8ba"));
    },
    "6e11": function (e, l, a) {
      Object.defineProperty(l, "__esModule", {
        value: !0
      }), l.default = void 0;
      l.default = {
        appid: "__UNI__9BE24D4"
      };
    },
    "74f4": function (e, l, a) {
      Object.defineProperty(l, "__esModule", {
        value: !0
      }), l.default = void 0;
      var t = [{
        label: "北京市",
        value: "11"
      }, {
        label: "天津市",
        value: "12"
      }, {
        label: "河北省",
        value: "13"
      }, {
        label: "山西省",
        value: "14"
      }, {
        label: "内蒙古自治区",
        value: "15"
      }, {
        label: "辽宁省",
        value: "21"
      }, {
        label: "吉林省",
        value: "22"
      }, {
        label: "黑龙江省",
        value: "23"
      }, {
        label: "上海市",
        value: "31"
      }, {
        label: "江苏省",
        value: "32"
      }, {
        label: "浙江省",
        value: "33"
      }, {
        label: "安徽省",
        value: "34"
      }, {
        label: "福建省",
        value: "35"
      }, {
        label: "江西省",
        value: "36"
      }, {
        label: "山东省",
        value: "37"
      }, {
        label: "河南省",
        value: "41"
      }, {
        label: "湖北省",
        value: "42"
      }, {
        label: "湖南省",
        value: "43"
      }, {
        label: "广东省",
        value: "44"
      }, {
        label: "广西壮族自治区",
        value: "45"
      }, {
        label: "海南省",
        value: "46"
      }, {
        label: "重庆市",
        value: "50"
      }, {
        label: "四川省",
        value: "51"
      }, {
        label: "贵州省",
        value: "52"
      }, {
        label: "云南省",
        value: "53"
      }, {
        label: "西藏自治区",
        value: "54"
      }, {
        label: "陕西省",
        value: "61"
      }, {
        label: "甘肃省",
        value: "62"
      }, {
        label: "青海省",
        value: "63"
      }, {
        label: "宁夏回族自治区",
        value: "64"
      }, {
        label: "新疆维吾尔自治区",
        value: "65"
      }, {
        label: "台湾",
        value: "66"
      }, {
        label: "香港",
        value: "67"
      }, {
        label: "澳门",
        value: "68"
      }];
      l.default = t;
    },
    8189: function (e) {
      e.exports = {
        _from: "@dcloudio/uni-stat@^2.0.0-alpha-24420191128001",
        _id: "@dcloudio/uni-stat@2.0.0-v3-24020191018001",
        _inBundle: !1,
        _integrity: "sha512-nYBm5pRrYzrj2dKMqucWSF2PwInUMnn3MLHM/ik3gnLUEKSW61rzcY1RPlUwaH7c+Snm6N+bAJzmj3GvlrlVXA==",
        _location: "/@dcloudio/uni-stat",
        _phantomChildren: {},
        _requested: {
          type: "range",
          registry: !0,
          raw: "@dcloudio/uni-stat@^2.0.0-alpha-24420191128001",
          name: "@dcloudio/uni-stat",
          escapedName: "@dcloudio%2funi-stat",
          scope: "@dcloudio",
          rawSpec: "^2.0.0-alpha-24420191128001",
          saveSpec: null,
          fetchSpec: "^2.0.0-alpha-24420191128001"
        },
        _requiredBy: ["/", "/@dcloudio/vue-cli-plugin-uni"],
        _resolved: "https://registry.npmjs.org/@dcloudio/uni-stat/-/uni-stat-2.0.0-v3-24020191018001.tgz",
        _shasum: "6ef04326cc0b945726413eebe442ab8f47c7536c",
        _spec: "@dcloudio/uni-stat@^2.0.0-alpha-24420191128001",
        _where: "/Users/guoshengqiang/Documents/dcloud-plugins/alpha/uniapp-cli",
        author: "",
        bugs: {
          url: "https://github.com/dcloudio/uni-app/issues"
        },
        bundleDependencies: !1,
        deprecated: !1,
        description: "",
        devDependencies: {
          "@babel/core": "^7.5.5",
          "@babel/preset-env": "^7.5.5",
          eslint: "^6.1.0",
          rollup: "^1.19.3",
          "rollup-plugin-babel": "^4.3.3",
          "rollup-plugin-clear": "^2.0.7",
          "rollup-plugin-commonjs": "^10.0.2",
          "rollup-plugin-copy": "^3.1.0",
          "rollup-plugin-eslint": "^7.0.0",
          "rollup-plugin-json": "^4.0.0",
          "rollup-plugin-node-resolve": "^5.2.0",
          "rollup-plugin-replace": "^2.2.0",
          "rollup-plugin-uglify": "^6.0.2"
        },
        files: ["dist", "package.json", "LICENSE"],
        gitHead: "197e8df53cc9d4c3f6eb722b918ccf51672b5cfe",
        homepage: "https://github.com/dcloudio/uni-app#readme",
        license: "Apache-2.0",
        main: "dist/index.js",
        name: "@dcloudio/uni-stat",
        repository: {
          type: "git",
          url: "git+https://github.com/dcloudio/uni-app.git",
          directory: "packages/uni-stat"
        },
        scripts: {
          build: "NODE_ENV=production rollup -c rollup.config.js",
          dev: "NODE_ENV=development rollup -w -c rollup.config.js"
        },
        version: "2.0.0-v3-24020191018001"
      };
    },
    8921: function (e, l, a) {
      var n = [{
          id: "35",
          provincecode: "150000",
          city: "阿拉善盟",
          code: "152900",
          initial: "A"
        }, {
          id: "38",
          provincecode: "210000",
          city: "鞍山市",
          code: "210300",
          initial: "A"
        }, {
          id: "105",
          provincecode: "340000",
          city: "安庆市",
          code: "340800",
          initial: "A"
        }, {
          id: "156",
          provincecode: "410000",
          city: "安阳市",
          code: "410500",
          initial: "A"
        }, {
          id: "256",
          provincecode: "510000",
          city: "阿坝藏族羌族自治州",
          code: "513200",
          initial: "A"
        }, {
          id: "262",
          provincecode: "520000",
          city: "安顺市",
          code: "520400",
          initial: "A"
        }, {
          id: "289",
          provincecode: "540000",
          city: "阿里地区",
          code: "542500",
          initial: "A"
        }, {
          id: "299",
          provincecode: "610000",
          city: "安康市",
          code: "610900",
          initial: "A"
        }, {
          id: "335",
          provincecode: "650000",
          city: "阿克苏地区",
          code: "652900",
          initial: "A"
        }, {
          id: "341",
          provincecode: "650000",
          city: "阿勒泰地区",
          code: "654300",
          initial: "A"
        }, {
          id: "1",
          provincecode: "110000",
          city: "北京市",
          code: "110100",
          initial: "B"
        }, {
          id: "7",
          provincecode: "130000",
          city: "保定市",
          code: "130600",
          initial: "B"
        }, {
          id: "25",
          provincecode: "150000",
          city: "包头市",
          code: "150200",
          initial: "B"
        }, {
          id: "31",
          provincecode: "150000",
          city: "巴彦淖尔市",
          code: "150800",
          initial: "B"
        }, {
          id: "40",
          provincecode: "210000",
          city: "本溪市",
          code: "210500",
          initial: "B"
        }, {
          id: "55",
          provincecode: "220000",
          city: "白山市",
          code: "220600",
          initial: "B"
        }, {
          id: "57",
          provincecode: "220000",
          city: "白城市",
          code: "220800",
          initial: "B"
        }, {
          id: "100",
          provincecode: "340000",
          city: "蚌埠市",
          code: "340300",
          initial: "B"
        }, {
          id: "150",
          provincecode: "370000",
          city: "滨州市",
          code: "371600",
          initial: "B"
        }, {
          id: "222",
          provincecode: "450000",
          city: "北海市",
          code: "450500",
          initial: "B"
        }, {
          id: "227",
          provincecode: "450000",
          city: "百色市",
          code: "451000",
          initial: "B"
        }, {
          id: "254",
          provincecode: "510000",
          city: "巴中市",
          code: "511900",
          initial: "B"
        }, {
          id: "265",
          provincecode: "520000",
          city: "毕节地区",
          code: "522400",
          initial: "B"
        }, {
          id: "271",
          provincecode: "530000",
          city: "保山市",
          code: "530500",
          initial: "B"
        }, {
          id: "293",
          provincecode: "610000",
          city: "宝鸡市",
          code: "610300",
          initial: "B"
        }, {
          id: "304",
          provincecode: "620000",
          city: "白银市",
          code: "620400",
          initial: "B"
        }, {
          id: "333",
          provincecode: "650000",
          city: "博尔塔拉蒙古自治州",
          code: "652700",
          initial: "B"
        }, {
          id: "334",
          provincecode: "650000",
          city: "巴音郭楞蒙古自治州",
          code: "652800",
          initial: "B"
        }, {
          id: "",
          provincecode: "500000",
          city: "重庆市",
          code: "500000",
          initial: "C"
        }, {
          id: "9",
          provincecode: "130000",
          city: "承德市",
          code: "130800",
          initial: "C"
        }, {
          id: "10",
          provincecode: "130000",
          city: "沧州市",
          code: "130900",
          initial: "C"
        }, {
          id: "16",
          provincecode: "140000",
          city: "长治市",
          code: "140400",
          initial: "C"
        }, {
          id: "27",
          provincecode: "150000",
          city: "赤峰市",
          code: "150400",
          initial: "C"
        }, {
          id: "48",
          provincecode: "210000",
          city: "朝阳市",
          code: "211300",
          initial: "C"
        }, {
          id: "50",
          provincecode: "220000",
          city: "长春市",
          code: "220100",
          initial: "C"
        }, {
          id: "77",
          provincecode: "320000",
          city: "常州市",
          code: "320400",
          initial: "C"
        }, {
          id: "107",
          provincecode: "340000",
          city: "滁州市",
          code: "341100",
          initial: "C"
        }, {
          id: "110",
          provincecode: "340000",
          city: "巢湖市",
          code: "341400",
          initial: "C"
        }, {
          id: "113",
          provincecode: "340000",
          city: "池州市",
          code: "341700",
          initial: "C"
        }, {
          id: "183",
          provincecode: "430000",
          city: "长沙市",
          code: "430100",
          initial: "C"
        }, {
          id: "189",
          provincecode: "430000",
          city: "常德市",
          code: "430700",
          initial: "C"
        }, {
          id: "192",
          provincecode: "430000",
          city: "郴州市",
          code: "431000",
          initial: "C"
        }, {
          id: "215",
          provincecode: "440000",
          city: "潮州市",
          code: "445100",
          initial: "C"
        }, {
          id: "231",
          provincecode: "450000",
          city: "崇左市",
          code: "451400",
          initial: "C"
        }, {
          id: "238",
          provincecode: "510000",
          city: "成都市",
          code: "510100",
          initial: "C"
        }, {
          id: "276",
          provincecode: "530000",
          city: "楚雄彝族自治州",
          code: "532300",
          initial: "C"
        }, {
          id: "285",
          provincecode: "540000",
          city: "昌都地区",
          code: "542100",
          initial: "C"
        }, {
          id: "332",
          provincecode: "650000",
          city: "昌吉回族自治州",
          code: "652300",
          initial: "C"
        }, {
          id: "14",
          provincecode: "140000",
          city: "大同市",
          code: "140200",
          initial: "D"
        }, {
          id: "37",
          provincecode: "210000",
          city: "大连市",
          code: "210200",
          initial: "D"
        }, {
          id: "41",
          provincecode: "210000",
          city: "丹东市",
          code: "210600",
          initial: "D"
        }, {
          id: "64",
          provincecode: "230000",
          city: "大庆市",
          code: "230600",
          initial: "D"
        }, {
          id: "71",
          provincecode: "230000",
          city: "大兴安岭地区",
          code: "232700",
          initial: "D"
        }, {
          id: "139",
          provincecode: "370000",
          city: "东营市",
          code: "370500",
          initial: "D"
        }, {
          id: "148",
          provincecode: "370000",
          city: "德州市",
          code: "371400",
          initial: "D"
        }, {
          id: "213",
          provincecode: "440000",
          city: "东莞市",
          code: "441900",
          initial: "D"
        }, {
          id: "242",
          provincecode: "510000",
          city: "德阳市",
          code: "510600",
          initial: "D"
        }, {
          id: "252",
          provincecode: "510000",
          city: "达州市",
          code: "511700",
          initial: "D"
        }, {
          id: "280",
          provincecode: "530000",
          city: "大理白族自治州",
          code: "532900",
          initial: "D"
        }, {
          id: "281",
          provincecode: "530000",
          city: "德宏傣族景颇族自治州",
          code: "533100",
          initial: "D"
        }, {
          id: "283",
          provincecode: "530000",
          city: "迪庆藏族自治州",
          code: "533400",
          initial: "D"
        }, {
          id: "311",
          provincecode: "620000",
          city: "定西市",
          code: "621100",
          initial: "D"
        }, {
          id: "29",
          provincecode: "150000",
          city: "鄂尔多斯市",
          code: "150600",
          initial: "E"
        }, {
          id: "174",
          provincecode: "420000",
          city: "鄂州市",
          code: "420700",
          initial: "E"
        }, {
          id: "181",
          provincecode: "420000",
          city: "恩施土家族苗族自治州",
          code: "422800",
          initial: "E"
        }, {
          id: "39",
          provincecode: "210000",
          city: "抚顺市",
          code: "210400",
          initial: "F"
        }, {
          id: "44",
          provincecode: "210000",
          city: "阜新市",
          code: "210900",
          initial: "F"
        }, {
          id: "108",
          provincecode: "340000",
          city: "阜阳市",
          code: "341200",
          initial: "F"
        }, {
          id: "115",
          provincecode: "350000",
          city: "福州市",
          code: "350100",
          initial: "F"
        }, {
          id: "133",
          provincecode: "360000",
          city: "抚州市",
          code: "361000",
          initial: "F"
        }, {
          id: "202",
          provincecode: "440000",
          city: "佛山市",
          code: "440600",
          initial: "F"
        }, {
          id: "223",
          provincecode: "450000",
          city: "防城港市",
          code: "450600",
          initial: "F"
        }, {
          id: "130",
          provincecode: "360000",
          city: "赣州市",
          code: "360700",
          initial: "G"
        }, {
          id: "197",
          provincecode: "440000",
          city: "广州市",
          code: "440100",
          initial: "G"
        }, {
          id: "220",
          provincecode: "450000",
          city: "桂林市",
          code: "450300",
          initial: "G"
        }, {
          id: "225",
          provincecode: "450000",
          city: "贵港市",
          code: "450800",
          initial: "G"
        }, {
          id: "244",
          provincecode: "510000",
          city: "广元市",
          code: "510800",
          initial: "G"
        }, {
          id: "251",
          provincecode: "510000",
          city: "广安市",
          code: "511600",
          initial: "G"
        }, {
          id: "257",
          provincecode: "510000",
          city: "甘孜藏族自治州",
          code: "513300",
          initial: "G"
        }, {
          id: "259",
          provincecode: "520000",
          city: "贵阳市",
          code: "520100",
          initial: "G"
        }, {
          id: "314",
          provincecode: "620000",
          city: "甘南藏族自治州",
          code: "623000",
          initial: "G"
        }, {
          id: "320",
          provincecode: "630000",
          city: "果洛藏族自治州",
          code: "632600",
          initial: "G"
        }, {
          id: "326",
          provincecode: "640000",
          city: "固原市",
          code: "640400",
          initial: "G"
        }, {
          id: "5",
          provincecode: "130000",
          city: "邯郸市",
          code: "130400",
          initial: "H"
        }, {
          id: "12",
          provincecode: "130000",
          city: "衡水市",
          code: "131100",
          initial: "H"
        }, {
          id: "24",
          provincecode: "150000",
          city: "呼和浩特市",
          code: "150100",
          initial: "H"
        }, {
          id: "30",
          provincecode: "150000",
          city: "呼伦贝尔市",
          code: "150700",
          initial: "H"
        }, {
          id: "49",
          provincecode: "210000",
          city: "葫芦岛市",
          code: "211400",
          initial: "H"
        }, {
          id: "59",
          provincecode: "230000",
          city: "哈尔滨市",
          code: "230100",
          initial: "H"
        }, {
          id: "62",
          provincecode: "230000",
          city: "鹤岗市",
          code: "230400",
          initial: "H"
        }, {
          id: "69",
          provincecode: "230000",
          city: "黑河市",
          code: "231100",
          initial: "H"
        }, {
          id: "81",
          provincecode: "320000",
          city: "淮安市",
          code: "320800",
          initial: "H"
        }, {
          id: "87",
          provincecode: "330000",
          city: "杭州市",
          code: "330100",
          initial: "H"
        }, {
          id: "91",
          provincecode: "330000",
          city: "湖州市",
          code: "330500",
          initial: "H"
        }, {
          id: "98",
          provincecode: "340000",
          city: "合肥市",
          code: "340100",
          initial: "H"
        }, {
          id: "101",
          provincecode: "340000",
          city: "淮南市",
          code: "340400",
          initial: "H"
        }, {
          id: "103",
          provincecode: "340000",
          city: "淮北市",
          code: "340600",
          initial: "H"
        }, {
          id: "106",
          provincecode: "340000",
          city: "黄山市",
          code: "341000",
          initial: "H"
        }, {
          id: "112",
          provincecode: "340000",
          city: "亳州市",
          code: "341600",
          initial: "H"
        }, {
          id: "151",
          provincecode: "370000",
          city: "荷泽市",
          code: "371700",
          initial: "H"
        }, {
          id: "157",
          provincecode: "410000",
          city: "鹤壁市",
          code: "410600",
          initial: "H"
        }, {
          id: "170",
          provincecode: "420000",
          city: "黄石市",
          code: "420200",
          initial: "H"
        }, {
          id: "178",
          provincecode: "420000",
          city: "黄冈市",
          code: "421100",
          initial: "H"
        }, {
          id: "186",
          provincecode: "430000",
          city: "衡阳市",
          code: "430400",
          initial: "H"
        }, {
          id: "194",
          provincecode: "430000",
          city: "怀化市",
          code: "431200",
          initial: "H"
        }, {
          id: "207",
          provincecode: "440000",
          city: "惠州市",
          code: "441300",
          initial: "H"
        }, {
          id: "210",
          provincecode: "440000",
          city: "河源市",
          code: "441600",
          initial: "H"
        }, {
          id: "228",
          provincecode: "450000",
          city: "贺州市",
          code: "451100",
          initial: "H"
        }, {
          id: "229",
          provincecode: "450000",
          city: "河池市",
          code: "451200",
          initial: "H"
        }, {
          id: "232",
          provincecode: "460000",
          city: "海口市",
          code: "460100",
          initial: "H"
        }, {
          id: "277",
          provincecode: "530000",
          city: "红河哈尼族彝族自治州",
          code: "532500",
          initial: "H"
        }, {
          id: "297",
          provincecode: "610000",
          city: "汉中市",
          code: "610700",
          initial: "H"
        }, {
          id: "316",
          provincecode: "630000",
          city: "海东地区",
          code: "632100",
          initial: "H"
        }, {
          id: "317",
          provincecode: "630000",
          city: "海北藏族自治州",
          code: "632200",
          initial: "H"
        }, {
          id: "318",
          provincecode: "630000",
          city: "黄南藏族自治州",
          code: "632300",
          initial: "H"
        }, {
          id: "319",
          provincecode: "630000",
          city: "海南藏族自治州",
          code: "632500",
          initial: "H"
        }, {
          id: "322",
          provincecode: "630000",
          city: "海西蒙古族藏族自治州",
          code: "632800",
          initial: "H"
        }, {
          id: "331",
          provincecode: "650000",
          city: "哈密地区",
          code: "652200",
          initial: "H"
        }, {
          id: "338",
          provincecode: "650000",
          city: "和田地区",
          code: "653200",
          initial: "H"
        }, {
          id: "17",
          provincecode: "140000",
          city: "晋城市",
          code: "140500",
          initial: "J"
        }, {
          id: "19",
          provincecode: "140000",
          city: "晋中市",
          code: "140700",
          initial: "J"
        }, {
          id: "42",
          provincecode: "210000",
          city: "锦州市",
          code: "210700",
          initial: "J"
        }, {
          id: "51",
          provincecode: "220000",
          city: "吉林市",
          code: "220200",
          initial: "J"
        }, {
          id: "61",
          provincecode: "230000",
          city: "鸡西市",
          code: "230300",
          initial: "J"
        }, {
          id: "66",
          provincecode: "230000",
          city: "佳木斯市",
          code: "230800",
          initial: "J"
        }, {
          id: "90",
          provincecode: "330000",
          city: "嘉兴市",
          code: "330400",
          initial: "J"
        }, {
          id: "93",
          provincecode: "330000",
          city: "金华市",
          code: "330700",
          initial: "J"
        }, {
          id: "125",
          provincecode: "360000",
          city: "景德镇市",
          code: "360200",
          initial: "J"
        }, {
          id: "127",
          provincecode: "360000",
          city: "九江市",
          code: "360400",
          initial: "J"
        }, {
          id: "131",
          provincecode: "360000",
          city: "吉安市",
          code: "360800",
          initial: "J"
        }, {
          id: "135",
          provincecode: "370000",
          city: "济南市",
          code: "370100",
          initial: "J"
        }, {
          id: "142",
          provincecode: "370000",
          city: "济宁市",
          code: "370800",
          initial: "J"
        }, {
          id: "159",
          provincecode: "410000",
          city: "焦作市",
          code: "410800",
          initial: "J"
        }, {
          id: "175",
          provincecode: "420000",
          city: "荆门市",
          code: "420800",
          initial: "J"
        }, {
          id: "177",
          provincecode: "420000",
          city: "荆州市",
          code: "421000",
          initial: "J"
        }, {
          id: "203",
          provincecode: "440000",
          city: "江门市",
          code: "440700",
          initial: "J"
        }, {
          id: "216",
          provincecode: "440000",
          city: "揭阳市",
          code: "445200",
          initial: "J"
        }, {
          id: "302",
          provincecode: "620000",
          city: "嘉峪关市",
          code: "620200",
          initial: "J"
        }, {
          id: "303",
          provincecode: "620000",
          city: "金昌市",
          code: "620300",
          initial: "J"
        }, {
          id: "309",
          provincecode: "620000",
          city: "酒泉市",
          code: "620900",
          initial: "J"
        }, {
          id: "153",
          provincecode: "410000",
          city: "开封市",
          code: "410200",
          initial: "K"
        }, {
          id: "268",
          provincecode: "530000",
          city: "昆明市",
          code: "530100",
          initial: "K"
        }, {
          id: "329",
          provincecode: "650000",
          city: "克拉玛依市",
          code: "650200",
          initial: "K"
        }, {
          id: "336",
          provincecode: "650000",
          city: "克孜勒苏柯尔克孜自治州",
          code: "653000",
          initial: "K"
        }, {
          id: "337",
          provincecode: "650000",
          city: "喀什地区",
          code: "653100",
          initial: "K"
        }, {
          id: "11",
          provincecode: "130000",
          city: "廊坊市",
          code: "131000",
          initial: "L"
        }, {
          id: "22",
          provincecode: "140000",
          city: "临汾市",
          code: "141000",
          initial: "L"
        }, {
          id: "23",
          provincecode: "140000",
          city: "吕梁市",
          code: "141100",
          initial: "L"
        }, {
          id: "45",
          provincecode: "210000",
          city: "辽阳市",
          code: "211000",
          initial: "L"
        }, {
          id: "53",
          provincecode: "220000",
          city: "辽源市",
          code: "220400",
          initial: "L"
        }, {
          id: "80",
          provincecode: "320000",
          city: "连云港市",
          code: "320700",
          initial: "L"
        }, {
          id: "97",
          provincecode: "330000",
          city: "丽水市",
          code: "331100",
          initial: "L"
        }, {
          id: "111",
          provincecode: "340000",
          city: "六安市",
          code: "341500",
          initial: "L"
        }, {
          id: "122",
          provincecode: "350000",
          city: "龙岩市",
          code: "350800",
          initial: "L"
        }, {
          id: "146",
          provincecode: "370000",
          city: "莱芜市",
          code: "371200",
          initial: "L"
        }, {
          id: "147",
          provincecode: "370000",
          city: "临沂市",
          code: "371300",
          initial: "L"
        }, {
          id: "149",
          provincecode: "370000",
          city: "聊城市",
          code: "371500",
          initial: "L"
        }, {
          id: "154",
          provincecode: "410000",
          city: "洛阳市",
          code: "410300",
          initial: "L"
        }, {
          id: "162",
          provincecode: "410000",
          city: "漯河市",
          code: "411100",
          initial: "L"
        }, {
          id: "195",
          provincecode: "430000",
          city: "娄底市",
          code: "431300",
          initial: "L"
        }, {
          id: "219",
          provincecode: "450000",
          city: "柳州市",
          code: "450200",
          initial: "L"
        }, {
          id: "230",
          provincecode: "450000",
          city: "来宾市",
          code: "451300",
          initial: "L"
        }, {
          id: "241",
          provincecode: "510000",
          city: "泸州市",
          code: "510500",
          initial: "L"
        }, {
          id: "247",
          provincecode: "510000",
          city: "乐山市",
          code: "511100",
          initial: "L"
        }, {
          id: "258",
          provincecode: "510000",
          city: "凉山彝族自治州",
          code: "513400",
          initial: "L"
        }, {
          id: "260",
          provincecode: "520000",
          city: "六盘水市",
          code: "520200",
          initial: "L"
        }, {
          id: "273",
          provincecode: "530000",
          city: "丽江市",
          code: "530700",
          initial: "L"
        }, {
          id: "275",
          provincecode: "530000",
          city: "临沧市",
          code: "530900",
          initial: "L"
        }, {
          id: "284",
          provincecode: "540000",
          city: "拉萨市",
          code: "540100",
          initial: "L"
        }, {
          id: "290",
          provincecode: "540000",
          city: "林芝地区",
          code: "542600",
          initial: "L"
        }, {
          id: "301",
          provincecode: "620000",
          city: "兰州市",
          code: "620100",
          initial: "L"
        }, {
          id: "312",
          provincecode: "620000",
          city: "陇南市",
          code: "621200",
          initial: "L"
        }, {
          id: "313",
          provincecode: "620000",
          city: "临夏回族自治州",
          code: "622900",
          initial: "L"
        }, {
          id: "68",
          provincecode: "230000",
          city: "牡丹江市",
          code: "231000",
          initial: "M"
        }, {
          id: "102",
          provincecode: "340000",
          city: "马鞍山市",
          code: "340500",
          initial: "M"
        }, {
          id: "205",
          provincecode: "440000",
          city: "茂名市",
          code: "440900",
          initial: "M"
        }, {
          id: "208",
          provincecode: "440000",
          city: "梅州市",
          code: "441400",
          initial: "M"
        }, {
          id: "243",
          provincecode: "510000",
          city: "绵阳市",
          code: "510700",
          initial: "M"
        }, {
          id: "249",
          provincecode: "510000",
          city: "眉山市",
          code: "511400",
          initial: "M"
        }, {
          id: "74",
          provincecode: "320000",
          city: "南京市",
          code: "320100",
          initial: "N"
        }, {
          id: "79",
          provincecode: "320000",
          city: "南通市",
          code: "320600",
          initial: "N"
        }, {
          id: "88",
          provincecode: "330000",
          city: "宁波市",
          code: "330200",
          initial: "N"
        }, {
          id: "121",
          provincecode: "350000",
          city: "南平市",
          code: "350700",
          initial: "N"
        }, {
          id: "123",
          provincecode: "350000",
          city: "宁德市",
          code: "350900",
          initial: "N"
        }, {
          id: "124",
          provincecode: "360000",
          city: "南昌市",
          code: "360100",
          initial: "N"
        }, {
          id: "164",
          provincecode: "410000",
          city: "南阳市",
          code: "411300",
          initial: "N"
        }, {
          id: "218",
          provincecode: "450000",
          city: "南宁市",
          code: "450100",
          initial: "N"
        }, {
          id: "246",
          provincecode: "510000",
          city: "内江市",
          code: "511000",
          initial: "N"
        }, {
          id: "248",
          provincecode: "510000",
          city: "南充市",
          code: "511300",
          initial: "N"
        }, {
          id: "282",
          provincecode: "530000",
          city: "怒江傈僳族自治州",
          code: "533300",
          initial: "N"
        }, {
          id: "288",
          provincecode: "540000",
          city: "那曲地区",
          code: "542400",
          initial: "N"
        }, {
          id: "46",
          provincecode: "210000",
          city: "盘锦市",
          code: "211100",
          initial: "P"
        }, {
          id: "117",
          provincecode: "350000",
          city: "莆田市",
          code: "350300",
          initial: "P"
        }, {
          id: "126",
          provincecode: "360000",
          city: "萍乡市",
          code: "360300",
          initial: "P"
        }, {
          id: "155",
          provincecode: "410000",
          city: "平顶山市",
          code: "410400",
          initial: "P"
        }, {
          id: "160",
          provincecode: "410000",
          city: "濮阳市",
          code: "410900",
          initial: "P"
        }, {
          id: "240",
          provincecode: "510000",
          city: "攀枝花市",
          code: "510400",
          initial: "P"
        }, {
          id: "308",
          provincecode: "620000",
          city: "平凉市",
          code: "620800",
          initial: "P"
        }, {
          id: "4",
          provincecode: "130000",
          city: "秦皇岛市",
          code: "130300",
          initial: "Q"
        }, {
          id: "60",
          provincecode: "230000",
          city: "齐齐哈尔市",
          code: "230200",
          initial: "Q"
        }, {
          id: "67",
          provincecode: "230000",
          city: "七台河市",
          code: "230900",
          initial: "Q"
        }, {
          id: "94",
          provincecode: "330000",
          city: "衢州市",
          code: "330800",
          initial: "Q"
        }, {
          id: "119",
          provincecode: "350000",
          city: "泉州市",
          code: "350500",
          initial: "Q"
        }, {
          id: "136",
          provincecode: "370000",
          city: "青岛市",
          code: "370200",
          initial: "Q"
        }, {
          id: "212",
          provincecode: "440000",
          city: "清远市",
          code: "441800",
          initial: "Q"
        }, {
          id: "224",
          provincecode: "450000",
          city: "钦州市",
          code: "450700",
          initial: "Q"
        }, {
          id: "264",
          provincecode: "520000",
          city: "黔西南布依族苗族自治州",
          code: "522300",
          initial: "Q"
        }, {
          id: "266",
          provincecode: "520000",
          city: "黔东南苗族侗族自治州",
          code: "522600",
          initial: "Q"
        }, {
          id: "267",
          provincecode: "520000",
          city: "黔南布依族苗族自治州",
          code: "522700",
          initial: "Q"
        }, {
          id: "269",
          provincecode: "530000",
          city: "曲靖市",
          code: "530300",
          initial: "Q"
        }, {
          id: "310",
          provincecode: "620000",
          city: "庆阳市",
          code: "621000",
          initial: "Q"
        }, {
          id: "145",
          provincecode: "370000",
          city: "日照市",
          code: "371100",
          initial: "R"
        }, {
          id: "287",
          provincecode: "540000",
          city: "日喀则地区",
          code: "542300",
          initial: "R"
        }, {
          id: "2",
          provincecode: "130000",
          city: "石家庄市",
          code: "130100",
          initial: "S"
        }, {
          id: "",
          provincecode: "310000",
          city: "上海市",
          code: "310100",
          initial: "S"
        }, {
          id: "18",
          provincecode: "140000",
          city: "朔州市",
          code: "140600",
          initial: "S"
        }, {
          id: "36",
          provincecode: "210000",
          city: "沈阳市",
          code: "210100",
          initial: "S"
        }, {
          id: "52",
          provincecode: "220000",
          city: "四平市",
          code: "220300",
          initial: "S"
        }, {
          id: "56",
          provincecode: "220000",
          city: "松原市",
          code: "220700",
          initial: "S"
        }, {
          id: "63",
          provincecode: "230000",
          city: "双鸭山市",
          code: "230500",
          initial: "S"
        }, {
          id: "70",
          provincecode: "230000",
          city: "绥化市",
          code: "231200",
          initial: "S"
        }, {
          id: "78",
          provincecode: "320000",
          city: "苏州市",
          code: "320500",
          initial: "S"
        }, {
          id: "86",
          provincecode: "320000",
          city: "宿迁市",
          code: "321300",
          initial: "S"
        }, {
          id: "92",
          provincecode: "330000",
          city: "绍兴市",
          code: "330600",
          initial: "S"
        }, {
          id: "109",
          provincecode: "340000",
          city: "宿州市",
          code: "341300",
          initial: "S"
        }, {
          id: "118",
          provincecode: "350000",
          city: "三明市",
          code: "350400",
          initial: "S"
        }, {
          id: "134",
          provincecode: "360000",
          city: "上饶市",
          code: "361100",
          initial: "S"
        }, {
          id: "163",
          provincecode: "410000",
          city: "三门峡市",
          code: "411200",
          initial: "S"
        }, {
          id: "165",
          provincecode: "410000",
          city: "商丘市",
          code: "411400",
          initial: "S"
        }, {
          id: "171",
          provincecode: "420000",
          city: "十堰市",
          code: "420300",
          initial: "S"
        }, {
          id: "180",
          provincecode: "420000",
          city: "随州市",
          code: "421300",
          initial: "S"
        }, {
          id: "187",
          provincecode: "430000",
          city: "邵阳市",
          code: "430500",
          initial: "S"
        }, {
          id: "198",
          provincecode: "440000",
          city: "韶关市",
          code: "440200",
          initial: "S"
        }, {
          id: "199",
          provincecode: "440000",
          city: "深圳市",
          code: "440300",
          initial: "S"
        }, {
          id: "201",
          provincecode: "440000",
          city: "汕头市",
          code: "440500",
          initial: "S"
        }, {
          id: "209",
          provincecode: "440000",
          city: "汕尾市",
          code: "441500",
          initial: "S"
        }, {
          id: "233",
          provincecode: "460000",
          city: "三亚市",
          code: "460200",
          initial: "S"
        }, {
          id: "245",
          provincecode: "510000",
          city: "遂宁市",
          code: "510900",
          initial: "S"
        }, {
          id: "274",
          provincecode: "530000",
          city: "思茅市",
          code: "530800",
          initial: "S"
        }, {
          id: "286",
          provincecode: "540000",
          city: "山南地区",
          code: "542200",
          initial: "S"
        }, {
          id: "300",
          provincecode: "610000",
          city: "商洛市",
          code: "611000",
          initial: "S"
        }, {
          id: "324",
          provincecode: "640000",
          city: "石嘴山市",
          code: "640200",
          initial: "S"
        }, {
          id: "3",
          provincecode: "130000",
          city: "唐山市",
          code: "130200",
          initial: "T"
        }, {
          id: "13",
          provincecode: "140000",
          city: "太原市",
          code: "140100",
          initial: "T"
        }, {
          id: "28",
          provincecode: "150000",
          city: "通辽市",
          code: "150500",
          initial: "T"
        }, {
          id: "47",
          provincecode: "210000",
          city: "铁岭市",
          code: "211200",
          initial: "T"
        }, {
          id: "54",
          provincecode: "220000",
          city: "通化市",
          code: "220500",
          initial: "T"
        }, {
          id: "85",
          provincecode: "320000",
          city: "泰州市",
          code: "321200",
          initial: "T"
        }, {
          id: "96",
          provincecode: "330000",
          city: "台州市",
          code: "331000",
          initial: "T"
        }, {
          id: "104",
          provincecode: "340000",
          city: "铜陵市",
          code: "340700",
          initial: "T"
        }, {
          id: "143",
          provincecode: "370000",
          city: "泰安市",
          code: "370900",
          initial: "T"
        }, {
          id: "263",
          provincecode: "520000",
          city: "铜仁地区",
          code: "522200",
          initial: "T"
        }, {
          id: "292",
          provincecode: "610000",
          city: "铜川市",
          code: "610200",
          initial: "T"
        }, {
          id: "305",
          provincecode: "620000",
          city: "天水市",
          code: "620500",
          initial: "T"
        }, {
          id: "330",
          provincecode: "650000",
          city: "吐鲁番地区",
          code: "652100",
          initial: "T"
        }, {
          id: "340",
          provincecode: "650000",
          city: "塔城地区",
          code: "654200",
          initial: "T"
        }, {
          id: "343",
          provincecode: "120000",
          city: "天津市",
          code: "120100",
          initial: "T"
        }, {
          id: "26",
          provincecode: "150000",
          city: "乌海市",
          code: "150300",
          initial: "W"
        }, {
          id: "32",
          provincecode: "150000",
          city: "乌兰察布市",
          code: "150900",
          initial: "W"
        }, {
          id: "75",
          provincecode: "320000",
          city: "无锡市",
          code: "320200",
          initial: "W"
        }, {
          id: "89",
          provincecode: "330000",
          city: "温州市",
          code: "330300",
          initial: "W"
        }, {
          id: "99",
          provincecode: "340000",
          city: "芜湖市",
          code: "340200",
          initial: "W"
        }, {
          id: "141",
          provincecode: "370000",
          city: "潍坊市",
          code: "370700",
          initial: "W"
        }, {
          id: "144",
          provincecode: "370000",
          city: "威海市",
          code: "371000",
          initial: "W"
        }, {
          id: "169",
          provincecode: "420000",
          city: "武汉市",
          code: "420100",
          initial: "W"
        }, {
          id: "221",
          provincecode: "450000",
          city: "梧州市",
          code: "450400",
          initial: "W"
        }, {
          id: "278",
          provincecode: "530000",
          city: "文山壮族苗族自治州",
          code: "532600",
          initial: "W"
        }, {
          id: "295",
          provincecode: "610000",
          city: "渭南市",
          code: "610500",
          initial: "W"
        }, {
          id: "306",
          provincecode: "620000",
          city: "武威市",
          code: "620600",
          initial: "W"
        }, {
          id: "325",
          provincecode: "640000",
          city: "吴忠市",
          code: "640300",
          initial: "W"
        }, {
          id: "328",
          provincecode: "650000",
          city: "乌鲁木齐市",
          code: "650100",
          initial: "W"
        }, {
          id: "6",
          provincecode: "130000",
          city: "邢台市",
          code: "130500",
          initial: "X"
        }, {
          id: "21",
          provincecode: "140000",
          city: "忻州市",
          code: "140900",
          initial: "X"
        }, {
          id: "33",
          provincecode: "150000",
          city: "兴安盟",
          code: "152200",
          initial: "X"
        }, {
          id: "34",
          provincecode: "150000",
          city: "锡林郭勒盟",
          code: "152500",
          initial: "X"
        }, {
          id: "76",
          provincecode: "320000",
          city: "徐州市",
          code: "320300",
          initial: "X"
        }, {
          id: "114",
          provincecode: "340000",
          city: "宣城市",
          code: "341800",
          initial: "X"
        }, {
          id: "116",
          provincecode: "350000",
          city: "厦门市",
          code: "350200",
          initial: "X"
        }, {
          id: "128",
          provincecode: "360000",
          city: "新余市",
          code: "360500",
          initial: "X"
        }, {
          id: "158",
          provincecode: "410000",
          city: "新乡市",
          code: "410700",
          initial: "X"
        }, {
          id: "161",
          provincecode: "410000",
          city: "许昌市",
          code: "411000",
          initial: "X"
        }, {
          id: "166",
          provincecode: "410000",
          city: "信阳市",
          code: "411500",
          initial: "X"
        }, {
          id: "173",
          provincecode: "420000",
          city: "襄樊市",
          code: "420600",
          initial: "X"
        }, {
          id: "176",
          provincecode: "420000",
          city: "孝感市",
          code: "420900",
          initial: "X"
        }, {
          id: "179",
          provincecode: "420000",
          city: "咸宁市",
          code: "421200",
          initial: "X"
        }, {
          id: "185",
          provincecode: "430000",
          city: "湘潭市",
          code: "430300",
          initial: "X"
        }, {
          id: "196",
          provincecode: "430000",
          city: "湘西土家族苗族自治州",
          code: "433100",
          initial: "X"
        }, {
          id: "279",
          provincecode: "530000",
          city: "西双版纳傣族自治州",
          code: "532800",
          initial: "X"
        }, {
          id: "291",
          provincecode: "610000",
          city: "西安市",
          code: "610100",
          initial: "X"
        }, {
          id: "294",
          provincecode: "610000",
          city: "咸阳市",
          code: "610400",
          initial: "X"
        }, {
          id: "315",
          provincecode: "630000",
          city: "西宁市",
          code: "630100",
          initial: "X"
        }, {
          id: "15",
          provincecode: "140000",
          city: "阳泉市",
          code: "140300",
          initial: "Y"
        }, {
          id: "20",
          provincecode: "140000",
          city: "运城市",
          code: "140800",
          initial: "Y"
        }, {
          id: "43",
          provincecode: "210000",
          city: "营口市",
          code: "210800",
          initial: "Y"
        }, {
          id: "58",
          provincecode: "220000",
          city: "延边朝鲜族自治州",
          code: "222400",
          initial: "Y"
        }, {
          id: "65",
          provincecode: "230000",
          city: "伊春市",
          code: "230700",
          initial: "Y"
        }, {
          id: "82",
          provincecode: "320000",
          city: "盐城市",
          code: "320900",
          initial: "Y"
        }, {
          id: "83",
          provincecode: "320000",
          city: "扬州市",
          code: "321000",
          initial: "Y"
        }, {
          id: "129",
          provincecode: "360000",
          city: "鹰潭市",
          code: "360600",
          initial: "Y"
        }, {
          id: "132",
          provincecode: "360000",
          city: "宜春市",
          code: "360900",
          initial: "Y"
        }, {
          id: "140",
          provincecode: "370000",
          city: "烟台市",
          code: "370600",
          initial: "Y"
        }, {
          id: "172",
          provincecode: "420000",
          city: "宜昌市",
          code: "420500",
          initial: "Y"
        }, {
          id: "188",
          provincecode: "430000",
          city: "岳阳市",
          code: "430600",
          initial: "Y"
        }, {
          id: "191",
          provincecode: "430000",
          city: "益阳市",
          code: "430900",
          initial: "Y"
        }, {
          id: "193",
          provincecode: "430000",
          city: "永州市",
          code: "431100",
          initial: "Y"
        }, {
          id: "211",
          provincecode: "440000",
          city: "阳江市",
          code: "441700",
          initial: "Y"
        }, {
          id: "217",
          provincecode: "440000",
          city: "云浮市",
          code: "445300",
          initial: "Y"
        }, {
          id: "226",
          provincecode: "450000",
          city: "玉林市",
          code: "450900",
          initial: "Y"
        }, {
          id: "250",
          provincecode: "510000",
          city: "宜宾市",
          code: "511500",
          initial: "Y"
        }, {
          id: "253",
          provincecode: "510000",
          city: "雅安市",
          code: "511800",
          initial: "Y"
        }, {
          id: "270",
          provincecode: "530000",
          city: "玉溪市",
          code: "530400",
          initial: "Y"
        }, {
          id: "296",
          provincecode: "610000",
          city: "延安市",
          code: "610600",
          initial: "Y"
        }, {
          id: "298",
          provincecode: "610000",
          city: "榆林市",
          code: "610800",
          initial: "Y"
        }, {
          id: "321",
          provincecode: "630000",
          city: "玉树藏族自治州",
          code: "632700",
          initial: "Y"
        }, {
          id: "323",
          provincecode: "640000",
          city: "银川市",
          code: "640100",
          initial: "Y"
        }, {
          id: "339",
          provincecode: "650000",
          city: "伊犁哈萨克自治州",
          code: "654000",
          initial: "Y"
        }, {
          id: "8",
          provincecode: "130000",
          city: "张家口市",
          code: "130700",
          initial: "Z"
        }, {
          id: "84",
          provincecode: "320000",
          city: "镇江市",
          code: "321100",
          initial: "Z"
        }, {
          id: "95",
          provincecode: "330000",
          city: "舟山市",
          code: "330900",
          initial: "Z"
        }, {
          id: "120",
          provincecode: "350000",
          city: "漳州市",
          code: "350600",
          initial: "Z"
        }, {
          id: "137",
          provincecode: "370000",
          city: "淄博市",
          code: "370300",
          initial: "Z"
        }, {
          id: "138",
          provincecode: "370000",
          city: "枣庄市",
          code: "370400",
          initial: "Z"
        }, {
          id: "152",
          provincecode: "410000",
          city: "郑州市",
          code: "410100",
          initial: "Z"
        }, {
          id: "167",
          provincecode: "410000",
          city: "周口市",
          code: "411600",
          initial: "Z"
        }, {
          id: "168",
          provincecode: "410000",
          city: "驻马店市",
          code: "411700",
          initial: "Z"
        }, {
          id: "184",
          provincecode: "430000",
          city: "株洲市",
          code: "430200",
          initial: "Z"
        }, {
          id: "190",
          provincecode: "430000",
          city: "张家界市",
          code: "430800",
          initial: "Z"
        }, {
          id: "200",
          provincecode: "440000",
          city: "珠海市",
          code: "440400",
          initial: "Z"
        }, {
          id: "204",
          provincecode: "440000",
          city: "湛江市",
          code: "440800",
          initial: "Z"
        }, {
          id: "206",
          provincecode: "440000",
          city: "肇庆市",
          code: "441200",
          initial: "Z"
        }, {
          id: "214",
          provincecode: "440000",
          city: "中山市",
          code: "442000",
          initial: "Z"
        }, {
          id: "239",
          provincecode: "510000",
          city: "自贡市",
          code: "510300",
          initial: "Z"
        }, {
          id: "255",
          provincecode: "510000",
          city: "资阳市",
          code: "512000",
          initial: "Z"
        }, {
          id: "261",
          provincecode: "520000",
          city: "遵义市",
          code: "520300",
          initial: "Z"
        }, {
          id: "272",
          provincecode: "530000",
          city: "昭通市",
          code: "530600",
          initial: "Z"
        }, {
          id: "307",
          provincecode: "620000",
          city: "张掖市",
          code: "620700",
          initial: "Z"
        }, {
          id: "327",
          provincecode: "640000",
          city: "中卫市",
          code: "640500",
          initial: "Z"
        }],
        o = ["A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "W", "X", "Y", "Z"];

      function o() {
        return o;
      }
      e.exports = {
        searchLetter: o,
        cityList: function () {
          for (var e = [], l = 0; l < o.length; l++) {
            var a = o[l],
              t = [],
              i = {};
            i.initial = a;
            for (var u = 0; u < n.length; u++) a == n[u].initial && t.push(n[u]);
            i.cityInfo = t, e.push(i);
          }
          return e;
        }
      };
    },
    "921b": function (e, l, E) {
      (function (d) {
        function a(e, l) {
          return !l || "object" !== (void 0 === l ? "undefined" : _typeof(l)) && "function" != typeof l ? function (e) {
            if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return e;
          }(e) : l;
        }

        function t(e) {
          return (t = Object.setPrototypeOf ? Object.getPrototypeOf : function (e) {
            return e.__proto__ || Object.getPrototypeOf(e);
          })(e);
        }

        function i(e, l) {
          return (i = Object.setPrototypeOf || function (e, l) {
            return e.__proto__ = l, e;
          })(e, l);
        }

        function u(e, l) {
          if (!(e instanceof l)) throw new TypeError("Cannot call a class as a function");
        }

        function n(e, l) {
          for (var a = 0; a < l.length; a++) {
            var t = l[a];
            t.enumerable = t.enumerable || !1, t.configurable = !0, "value" in t && (t.writable = !0), Object.defineProperty(e, t.key, t);
          }
        }

        function o(e, l, a) {
          return l && n(e.prototype, l), a && n(e, a), e;
        }
        var p = E("8189").version,
          r = "__DC_STAT_UUID",
          v = "__DC_UUID_VALUE";
        var e, f = function () {
            return parseInt(new Date().getTime() / 1e3);
          },
          h = function () {
            return "wx";
          },
          c = "First__Visit__Time",
          b = "Last__Visit__Time",
          g = "__page__residence__time",
          y = 0,
          _ = 0,
          m = function () {
            return y = f(), "n" === h() && d.setStorageSync(g, f()), y;
          },
          s = "Total__Visit__Count",
          S = 0,
          x = 0,
          w = function () {
            var e = new Date().getTime();
            return x = 0, S = e;
          },
          T = function () {
            var e = new Date().getTime();
            return x = e;
          },
          $ = function (e) {
            var l = 0;
            return 0 !== S && (l = x - S), l = (l = parseInt(l / 1e3)) < 1 ? 1 : l, "app" === e ? {
              residenceTime: l,
              overtime: 300 < l
            } : "page" === e ? {
              residenceTime: l,
              overtime: 1800 < l
            } : {
              residenceTime: l
            };
          },
          k = function (e) {
            var l = getCurrentPages(),
              a = l[l.length - 1].$vm,
              t = e._query,
              i = t && "{}" !== JSON.stringify(t) ? "?" + JSON.stringify(t) : "";
            return e._query = "", "bd" === h() ? a.$mp && a.$mp.page.is + i : a.$scope && a.$scope.route + i || a.$mp && a.$mp.page.route + i;
          },
          O = function (e) {
            return !!("page" === e.mpType || e.$mp && "page" === e.$mp.mpType || "page" === e.$options.mpType);
          },
          A = E("1426").default,
          P = E("6e11").default || E("6e11"),
          B = d.getSystemInfoSync(),
          j = function () {
            function a() {
              var e, l;
              u(this, a), this.self = "", this._retry = 0, this._platform = "", this._query = {}, this._navigationBarTitle = {
                config: "",
                page: "",
                report: "",
                lt: ""
              }, this._operatingTime = 0, this._reportingRequestData = {
                1: [],
                11: []
              }, this.__prevent_triggering = !1, this.__licationHide = !1, this.__licationShow = !1, this._lastPageRoute = "", this.statData = {
                uuid: function () {
                  var l = "";
                  if ("n" === h()) {
                    try {
                      l = plus.runtime.getDCloudId();
                    } catch (e) {
                      l = "";
                    }
                    return l;
                  }
                  try {
                    l = d.getStorageSync(r);
                  } catch (e) {
                    l = v;
                  }
                  if (!l) {
                    l = Date.now() + "" + Math.floor(1e7 * Math.random());
                    try {
                      d.setStorageSync(r, l);
                    } catch (e) {
                      d.setStorageSync(r, v);
                    }
                  }
                  return l;
                }(),
                ut: h(),
                mpn: (l = "", "wx" !== h() && "qq" !== h() || d.canIUse("getAccountInfoSync") && (l = d.getAccountInfoSync().miniProgram.appId || ""), l),
                ak: P.appid,
                usv: p,
                v: "n" === h() ? plus.runtime.version : "",
                ch: (e = "", "n" === h() && (e = plus.runtime.channel), e),
                cn: "",
                pn: "",
                ct: "",
                t: f(),
                tt: "",
                p: "android" === B.platform ? "a" : "i",
                brand: B.brand || "",
                md: B.model,
                sv: B.system.replace(/(Android|iOS)\s/, ""),
                mpsdk: B.SDKVersion || "",
                mpv: B.version || "",
                lang: B.language,
                pr: B.pixelRatio,
                ww: B.windowWidth,
                wh: B.windowHeight,
                sw: B.screenWidth,
                sh: B.screenHeight
              };
            }
            return o(a, [{
              key: "_applicationShow",
              value: function () {
                if (this.__licationHide) {
                  if (T(), $("app").overtime) {
                    var e = {
                      path: this._lastPageRoute,
                      scene: this.statData.sc
                    };
                    this._sendReportRequest(e);
                  }
                  this.__licationHide = !1;
                }
              }
            }, {
              key: "_applicationHide",
              value: function (e, l) {
                this.__licationHide = !0, T();
                var a = $();
                w();
                var t = k(this);
                this._sendHideRequest({
                  urlref: t,
                  urlref_ts: a.residenceTime
                }, l);
              }
            }, {
              key: "_pageShow",
              value: function () {
                var e, l, a = k(this),
                  t = (e = getCurrentPages(), l = e[e.length - 1].$vm, "bd" === h() ? l.$mp && l.$mp.page.is : l.$scope && l.$scope.route || l.$mp && l.$mp.page.route);
                if (this._navigationBarTitle.config = A && A.pages[t] && A.pages[t].titleNView && A.pages[t].titleNView.titleText || A && A.pages[t] && A.pages[t].navigationBarTitleText || "", this.__licationShow) return w(), this.__licationShow = !1, void(this._lastPageRoute = a);
                if (T(), this._lastPageRoute = a, $("page").overtime) {
                  var i = {
                    path: this._lastPageRoute,
                    scene: this.statData.sc
                  };
                  this._sendReportRequest(i);
                }
                w();
              }
            }, {
              key: "_pageHide",
              value: function () {
                if (!this.__licationHide) {
                  T();
                  var e = $("page");
                  return this._sendPageRequest({
                    url: this._lastPageRoute,
                    urlref: this._lastPageRoute,
                    urlref_ts: e.residenceTime
                  }), void(this._navigationBarTitle = {
                    config: "",
                    page: "",
                    report: "",
                    lt: ""
                  });
                }
              }
            }, {
              key: "_login",
              value: function () {
                this._sendEventRequest({
                  key: "login"
                }, 0);
              }
            }, {
              key: "_share",
              value: function () {
                this._sendEventRequest({
                  key: "share"
                }, 0);
              }
            }, {
              key: "_payment",
              value: function (e) {
                this._sendEventRequest({
                  key: e
                }, 0);
              }
            }, {
              key: "_sendReportRequest",
              value: function (e) {
                this._navigationBarTitle.lt = "1";
                var l, a, t, i, u, n, o, r, v = e.query && "{}" !== JSON.stringify(e.query) ? "?" + JSON.stringify(e.query) : "";
                this.statData.lt = "1", this.statData.url = e.path + v || "", this.statData.t = f(), this.statData.sc = (n = e.scene, o = h(), r = "", n || ("wx" === o && (r = d.getLaunchOptionsSync().scene), r)), this.statData.fvts = (i = d.getStorageSync(c), u = 0, i ? u = i : (u = f(), d.setStorageSync(c, u), d.removeStorageSync(b)), u), this.statData.lvts = (t = d.getStorageSync(b) || "", d.setStorageSync(b, f()), t), this.statData.tvc = (l = d.getStorageSync(s), a = 1, l && (a = l, a++), d.setStorageSync(s, a), a), "n" === h() ? this.getProperty() : this.getNetworkInfo();
              }
            }, {
              key: "_sendPageRequest",
              value: function (e) {
                var l = e.url,
                  a = e.urlref,
                  t = e.urlref_ts;
                this._navigationBarTitle.lt = "11";
                var i = {
                  ak: this.statData.ak,
                  uuid: this.statData.uuid,
                  lt: "11",
                  ut: this.statData.ut,
                  url: l,
                  tt: this.statData.tt,
                  urlref: a,
                  urlref_ts: t,
                  ch: this.statData.ch,
                  usv: this.statData.usv,
                  t: f(),
                  p: this.statData.p
                };
                this.request(i);
              }
            }, {
              key: "_sendHideRequest",
              value: function (e, l) {
                var a = e.urlref,
                  t = e.urlref_ts,
                  i = {
                    ak: this.statData.ak,
                    uuid: this.statData.uuid,
                    lt: "3",
                    ut: this.statData.ut,
                    urlref: a,
                    urlref_ts: t,
                    ch: this.statData.ch,
                    usv: this.statData.usv,
                    t: f(),
                    p: this.statData.p
                  };
                this.request(i, l);
              }
            }, {
              key: "_sendEventRequest",
              value: function () {
                var e = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {},
                  l = e.key,
                  a = void 0 === l ? "" : l,
                  t = e.value,
                  i = void 0 === t ? "" : t,
                  u = this._lastPageRoute,
                  n = {
                    ak: this.statData.ak,
                    uuid: this.statData.uuid,
                    lt: "21",
                    ut: this.statData.ut,
                    url: u,
                    ch: this.statData.ch,
                    e_n: a,
                    e_v: "object" === (void 0 === i ? "undefined" : _typeof(i)) ? JSON.stringify(i) : i.toString(),
                    usv: this.statData.usv,
                    t: f(),
                    p: this.statData.p
                  };
                this.request(n);
              }
            }, {
              key: "getNetworkInfo",
              value: function () {
                var l = this;
                d.getNetworkType({
                  success: function (e) {
                    l.statData.net = e.networkType, l.getLocation();
                  }
                });
              }
            }, {
              key: "getProperty",
              value: function () {
                var l = this;
                plus.runtime.getProperty(plus.runtime.appid, function (e) {
                  l.statData.v = e.version || "", l.getNetworkInfo();
                });
              }
            }, {
              key: "getLocation",
              value: function () {
                var l = this;
                P.getLocation ? d.getLocation({
                  type: "wgs84",
                  geocode: !0,
                  success: function (e) {
                    e.address && (l.statData.cn = e.address.country, l.statData.pn = e.address.province, l.statData.ct = e.address.city), l.statData.lat = e.latitude, l.statData.lng = e.longitude, l.request(l.statData);
                  }
                }) : (this.statData.lat = 0, this.statData.lng = 0, this.request(this.statData));
              }
            }, {
              key: "request",
              value: function (e, l) {
                var a = this,
                  t = f(),
                  i = this._navigationBarTitle;
                e.ttn = i.page, e.ttpj = i.config, e.ttc = i.report;
                var u = this._reportingRequestData;
                if ("n" === h() && (u = d.getStorageSync("__UNI__STAT__DATA") || {}), u[e.lt] || (u[e.lt] = []), u[e.lt].push(e), "n" === h() && d.setStorageSync("__UNI__STAT__DATA", u), _ = f(), "n" === h() && (y = d.getStorageSync(g)), !(_ - y < 10) || l) {
                  var n = this._reportingRequestData;
                  "n" === h() && (n = d.getStorageSync("__UNI__STAT__DATA")), m();
                  var o = [],
                    r = [],
                    v = [],
                    c = function (a) {
                      n[a].forEach(function (e) {
                        var l = function (e) {
                          var l = "";
                          for (var a in e) l += a + "=" + e[a] + "&";
                          return l.substr(0, l.length - 1);
                        }(e);
                        0 === a ? o.push(l) : 3 === a ? v.push(l) : r.push(l);
                      });
                    };
                  for (var b in n) c(b);
                  o.push.apply(o, r.concat(v));
                  var s = {
                    usv: p,
                    t: t,
                    requests: JSON.stringify(o)
                  };
                  this._reportingRequestData = {}, "n" === h() && d.removeStorageSync("__UNI__STAT__DATA"), "h5" !== e.ut ? "n" !== h() || "a" !== this.statData.p ? this._sendRequest(s) : setTimeout(function () {
                    a._sendRequest(s);
                  }, 200) : this.imageRequest(s);
                }
              }
            }, {
              key: "_sendRequest",
              value: function (l) {
                var a = this;
                d.request({
                  url: "/uni/stat",
                  method: "POST",
                  data: l,
                  success: function () {},
                  fail: function (e) {
                    ++a._retry < 3 && setTimeout(function () {
                      a._sendRequest(l);
                    }, 1e3);
                  }
                });
              }
            }, {
              key: "imageRequest",
              value: function (e) {
                var l = new Image(),
                  a = function (e) {
                    var l = Object.keys(e).sort(),
                      a = {},
                      t = "";
                    for (var i in l) a[l[i]] = e[l[i]], t += l[i] + "=" + e[l[i]] + "&";
                    return {
                      sign: "",
                      options: t.substr(0, t.length - 1)
                    };
                  }(function (e) {
                    var l = {};
                    for (var a in e) l[a] = encodeURIComponent(e[a]);
                    return l;
                  }(e)).options;
                l.src = "/uni/stat.gif?" + a;
              }
            }, {
              key: "sendEvent",
              value: function (e, l) {
                var a, t;
                t = l, ((a = e) ? "string" != typeof a ? (console.error("uni.report [eventName] 参数类型错误,只能为 String 类型"), 1) : 255 < a.length ? (console.error("uni.report [eventName] 参数长度不能大于 255"), 1) : "string" != typeof t && "object" !== (void 0 === t ? "undefined" : _typeof(t)) ? (console.error("uni.report [options] 参数类型错误,只能为 String 或 Object 类型"), 1) : "string" == typeof t && 255 < t.length ? (console.error("uni.report [options] 参数长度不能大于 255"), 1) : "title" === a && "string" != typeof t ? (console.error("uni.report [eventName] 参数为 title 时，[options] 参数只能为 String 类型"), 1) : void 0 : (console.error("uni.report 缺少 [eventName] 参数"), 1)) || ("title" !== e ? this._sendEventRequest({
                  key: e,
                  value: "object" === (void 0 === l ? "undefined" : _typeof(l)) ? JSON.stringify(l) : l
                }, 1) : this._navigationBarTitle.report = l);
              }
            }]), a;
          }(),
          D = function (e) {
            function l() {
              var e;
              return u(this, l), (e = a(this, t(l).call(this))).instance = null, "function" == typeof d.addInterceptor && (e.addInterceptorInit(), e.interceptLogin(), e.interceptShare(!0), e.interceptRequestPayment()), e;
            }
            return function (e, l) {
              if ("function" != typeof l && null !== l) throw new TypeError("Super expression must either be null or a function");
              e.prototype = Object.create(l && l.prototype, {
                constructor: {
                  value: e,
                  writable: !0,
                  configurable: !0
                }
              }), l && i(e, l);
            }(l, j), o(l, null, [{
              key: "getInstance",
              value: function () {
                return this.instance || (this.instance = new l()), this.instance;
              }
            }]), o(l, [{
              key: "addInterceptorInit",
              value: function () {
                var l = this;
                d.addInterceptor("setNavigationBarTitle", {
                  invoke: function (e) {
                    l._navigationBarTitle.page = e.title;
                  }
                });
              }
            }, {
              key: "interceptLogin",
              value: function () {
                var e = this;
                d.addInterceptor("login", {
                  complete: function () {
                    e._login();
                  }
                });
              }
            }, {
              key: "interceptShare",
              value: function (e) {
                var l = this;
                e ? d.addInterceptor("share", {
                  success: function () {
                    l._share();
                  },
                  fail: function () {
                    l._share();
                  }
                }) : l._share();
              }
            }, {
              key: "interceptRequestPayment",
              value: function () {
                var e = this;
                d.addInterceptor("requestPayment", {
                  success: function () {
                    e._payment("pay_success");
                  },
                  fail: function () {
                    e._payment("pay_fail");
                  }
                });
              }
            }, {
              key: "report",
              value: function (e, l) {
                this.self = l, m(), this.__licationShow = !0, this._sendReportRequest(e, !0);
              }
            }, {
              key: "load",
              value: function (e, l) {
                if (!l.$scope && !l.$mp) {
                  var a = getCurrentPages();
                  l.$scope = a[a.length - 1];
                }
                this.self = l, this._query = e;
              }
            }, {
              key: "show",
              value: function (e) {
                this.self = e, O(e) ? this._pageShow(e) : this._applicationShow(e);
              }
            }, {
              key: "ready",
              value: function (e) {}
            }, {
              key: "hide",
              value: function (e) {
                this.self = e, O(e) ? this._pageHide(e) : this._applicationHide(e, !0);
              }
            }, {
              key: "error",
              value: function (e) {
                this._platform;
                var l;
                l = e.message ? e.stack : JSON.stringify(e);
                var a = {
                  ak: this.statData.ak,
                  uuid: this.statData.uuid,
                  lt: "31",
                  ut: this.statData.ut,
                  ch: this.statData.ch,
                  mpsdk: this.statData.mpsdk,
                  mpv: this.statData.mpv,
                  v: this.statData.v,
                  em: l,
                  usv: this.statData.usv,
                  t: f(),
                  p: this.statData.p
                };
                this.request(a);
              }
            }]), l;
          }().getInstance(),
          l = !1,
          C = {
            onLaunch: function (e) {
              D.report(e, this);
            },
            onReady: function () {
              D.ready(this);
            },
            onLoad: function (e) {
              if (D.load(e, this), this.$scope && this.$scope.onShareAppMessage) {
                var l = this.$scope.onShareAppMessage;
                this.$scope.onShareAppMessage = function (e) {
                  return D.interceptShare(!1), l.call(this, e);
                };
              }
            },
            onShow: function () {
              l = !1, D.show(this);
            },
            onHide: function () {
              l = !0, D.hide(this);
            },
            onUnload: function () {
              l ? l = !1 : D.hide(this);
            },
            onError: function (e) {
              D.error(e);
            }
          };
        ((e = E("66fd")).default || e).mixin(C), d.report = function (e, l) {
          D.sendEvent(e, l);
        };
      }).call(this, E("543d").default);
    },
    ac78: function (e, l, a) {
      Object.defineProperty(l, "__esModule", {
        value: !0
      }), l.default = void 0;
      var t = u(a("66fd")),
        i = u(a("2f62"));

      function u(e) {
        return e && e.__esModule ? e : {
          default: e
        };
      }
      t.default.use(i.default);
      var n = new i.default.Store({
        state: {
          hasLogin: !1
        },
        mutations: {
          login: function (e) {
            e.hasLogin = !0;
          }
        }
      });
      l.default = n;
    },
    c5de: function (e, l, a) {
      Object.defineProperty(l, "__esModule", {
        value: !0
      }), l.default = void 0;
      var t = [
        [
          [{
            label: "东城区",
            value: "110101"
          }, {
            label: "西城区",
            value: "110102"
          }, {
            label: "朝阳区",
            value: "110105"
          }, {
            label: "丰台区",
            value: "110106"
          }, {
            label: "石景山区",
            value: "110107"
          }, {
            label: "海淀区",
            value: "110108"
          }, {
            label: "门头沟区",
            value: "110109"
          }, {
            label: "房山区",
            value: "110111"
          }, {
            label: "通州区",
            value: "110112"
          }, {
            label: "顺义区",
            value: "110113"
          }, {
            label: "昌平区",
            value: "110114"
          }, {
            label: "大兴区",
            value: "110115"
          }, {
            label: "怀柔区",
            value: "110116"
          }, {
            label: "平谷区",
            value: "110117"
          }, {
            label: "密云区",
            value: "110118"
          }, {
            label: "延庆区",
            value: "110119"
          }]
        ],
        [
          [{
            label: "和平区",
            value: "120101"
          }, {
            label: "河东区",
            value: "120102"
          }, {
            label: "河西区",
            value: "120103"
          }, {
            label: "南开区",
            value: "120104"
          }, {
            label: "河北区",
            value: "120105"
          }, {
            label: "红桥区",
            value: "120106"
          }, {
            label: "东丽区",
            value: "120110"
          }, {
            label: "西青区",
            value: "120111"
          }, {
            label: "津南区",
            value: "120112"
          }, {
            label: "北辰区",
            value: "120113"
          }, {
            label: "武清区",
            value: "120114"
          }, {
            label: "宝坻区",
            value: "120115"
          }, {
            label: "滨海新区",
            value: "120116"
          }, {
            label: "宁河区",
            value: "120117"
          }, {
            label: "静海区",
            value: "120118"
          }, {
            label: "蓟州区",
            value: "120119"
          }]
        ],
        [
          [{
            label: "长安区",
            value: "130102"
          }, {
            label: "桥西区",
            value: "130104"
          }, {
            label: "新华区",
            value: "130105"
          }, {
            label: "井陉矿区",
            value: "130107"
          }, {
            label: "裕华区",
            value: "130108"
          }, {
            label: "藁城区",
            value: "130109"
          }, {
            label: "鹿泉区",
            value: "130110"
          }, {
            label: "栾城区",
            value: "130111"
          }, {
            label: "井陉县",
            value: "130121"
          }, {
            label: "正定县",
            value: "130123"
          }, {
            label: "行唐县",
            value: "130125"
          }, {
            label: "灵寿县",
            value: "130126"
          }, {
            label: "高邑县",
            value: "130127"
          }, {
            label: "深泽县",
            value: "130128"
          }, {
            label: "赞皇县",
            value: "130129"
          }, {
            label: "无极县",
            value: "130130"
          }, {
            label: "平山县",
            value: "130131"
          }, {
            label: "元氏县",
            value: "130132"
          }, {
            label: "赵县",
            value: "130133"
          }, {
            label: "石家庄高新技术产业开发区",
            value: "130171"
          }, {
            label: "石家庄循环化工园区",
            value: "130172"
          }, {
            label: "辛集市",
            value: "130181"
          }, {
            label: "晋州市",
            value: "130183"
          }, {
            label: "新乐市",
            value: "130184"
          }],
          [{
            label: "路南区",
            value: "130202"
          }, {
            label: "路北区",
            value: "130203"
          }, {
            label: "古冶区",
            value: "130204"
          }, {
            label: "开平区",
            value: "130205"
          }, {
            label: "丰南区",
            value: "130207"
          }, {
            label: "丰润区",
            value: "130208"
          }, {
            label: "曹妃甸区",
            value: "130209"
          }, {
            label: "滦县",
            value: "130223"
          }, {
            label: "滦南县",
            value: "130224"
          }, {
            label: "乐亭县",
            value: "130225"
          }, {
            label: "迁西县",
            value: "130227"
          }, {
            label: "玉田县",
            value: "130229"
          }, {
            label: "唐山市芦台经济技术开发区",
            value: "130271"
          }, {
            label: "唐山市汉沽管理区",
            value: "130272"
          }, {
            label: "唐山高新技术产业开发区",
            value: "130273"
          }, {
            label: "河北唐山海港经济开发区",
            value: "130274"
          }, {
            label: "遵化市",
            value: "130281"
          }, {
            label: "迁安市",
            value: "130283"
          }],
          [{
            label: "海港区",
            value: "130302"
          }, {
            label: "山海关区",
            value: "130303"
          }, {
            label: "北戴河区",
            value: "130304"
          }, {
            label: "抚宁区",
            value: "130306"
          }, {
            label: "青龙满族自治县",
            value: "130321"
          }, {
            label: "昌黎县",
            value: "130322"
          }, {
            label: "卢龙县",
            value: "130324"
          }, {
            label: "秦皇岛市经济技术开发区",
            value: "130371"
          }, {
            label: "北戴河新区",
            value: "130372"
          }],
          [{
            label: "邯山区",
            value: "130402"
          }, {
            label: "丛台区",
            value: "130403"
          }, {
            label: "复兴区",
            value: "130404"
          }, {
            label: "峰峰矿区",
            value: "130406"
          }, {
            label: "肥乡区",
            value: "130407"
          }, {
            label: "永年区",
            value: "130408"
          }, {
            label: "临漳县",
            value: "130423"
          }, {
            label: "成安县",
            value: "130424"
          }, {
            label: "大名县",
            value: "130425"
          }, {
            label: "涉县",
            value: "130426"
          }, {
            label: "磁县",
            value: "130427"
          }, {
            label: "邱县",
            value: "130430"
          }, {
            label: "鸡泽县",
            value: "130431"
          }, {
            label: "广平县",
            value: "130432"
          }, {
            label: "馆陶县",
            value: "130433"
          }, {
            label: "魏县",
            value: "130434"
          }, {
            label: "曲周县",
            value: "130435"
          }, {
            label: "邯郸经济技术开发区",
            value: "130471"
          }, {
            label: "邯郸冀南新区",
            value: "130473"
          }, {
            label: "武安市",
            value: "130481"
          }],
          [{
            label: "桥东区",
            value: "130502"
          }, {
            label: "桥西区",
            value: "130503"
          }, {
            label: "邢台县",
            value: "130521"
          }, {
            label: "临城县",
            value: "130522"
          }, {
            label: "内丘县",
            value: "130523"
          }, {
            label: "柏乡县",
            value: "130524"
          }, {
            label: "隆尧县",
            value: "130525"
          }, {
            label: "任县",
            value: "130526"
          }, {
            label: "南和县",
            value: "130527"
          }, {
            label: "宁晋县",
            value: "130528"
          }, {
            label: "巨鹿县",
            value: "130529"
          }, {
            label: "新河县",
            value: "130530"
          }, {
            label: "广宗县",
            value: "130531"
          }, {
            label: "平乡县",
            value: "130532"
          }, {
            label: "威县",
            value: "130533"
          }, {
            label: "清河县",
            value: "130534"
          }, {
            label: "临西县",
            value: "130535"
          }, {
            label: "河北邢台经济开发区",
            value: "130571"
          }, {
            label: "南宫市",
            value: "130581"
          }, {
            label: "沙河市",
            value: "130582"
          }],
          [{
            label: "竞秀区",
            value: "130602"
          }, {
            label: "莲池区",
            value: "130606"
          }, {
            label: "满城区",
            value: "130607"
          }, {
            label: "清苑区",
            value: "130608"
          }, {
            label: "徐水区",
            value: "130609"
          }, {
            label: "涞水县",
            value: "130623"
          }, {
            label: "阜平县",
            value: "130624"
          }, {
            label: "定兴县",
            value: "130626"
          }, {
            label: "唐县",
            value: "130627"
          }, {
            label: "高阳县",
            value: "130628"
          }, {
            label: "容城县",
            value: "130629"
          }, {
            label: "涞源县",
            value: "130630"
          }, {
            label: "望都县",
            value: "130631"
          }, {
            label: "安新县",
            value: "130632"
          }, {
            label: "易县",
            value: "130633"
          }, {
            label: "曲阳县",
            value: "130634"
          }, {
            label: "蠡县",
            value: "130635"
          }, {
            label: "顺平县",
            value: "130636"
          }, {
            label: "博野县",
            value: "130637"
          }, {
            label: "雄县",
            value: "130638"
          }, {
            label: "保定高新技术产业开发区",
            value: "130671"
          }, {
            label: "保定白沟新城",
            value: "130672"
          }, {
            label: "涿州市",
            value: "130681"
          }, {
            label: "定州市",
            value: "130682"
          }, {
            label: "安国市",
            value: "130683"
          }, {
            label: "高碑店市",
            value: "130684"
          }],
          [{
            label: "桥东区",
            value: "130702"
          }, {
            label: "桥西区",
            value: "130703"
          }, {
            label: "宣化区",
            value: "130705"
          }, {
            label: "下花园区",
            value: "130706"
          }, {
            label: "万全区",
            value: "130708"
          }, {
            label: "崇礼区",
            value: "130709"
          }, {
            label: "张北县",
            value: "130722"
          }, {
            label: "康保县",
            value: "130723"
          }, {
            label: "沽源县",
            value: "130724"
          }, {
            label: "尚义县",
            value: "130725"
          }, {
            label: "蔚县",
            value: "130726"
          }, {
            label: "阳原县",
            value: "130727"
          }, {
            label: "怀安县",
            value: "130728"
          }, {
            label: "怀来县",
            value: "130730"
          }, {
            label: "涿鹿县",
            value: "130731"
          }, {
            label: "赤城县",
            value: "130732"
          }, {
            label: "张家口市高新技术产业开发区",
            value: "130771"
          }, {
            label: "张家口市察北管理区",
            value: "130772"
          }, {
            label: "张家口市塞北管理区",
            value: "130773"
          }],
          [{
            label: "双桥区",
            value: "130802"
          }, {
            label: "双滦区",
            value: "130803"
          }, {
            label: "鹰手营子矿区",
            value: "130804"
          }, {
            label: "承德县",
            value: "130821"
          }, {
            label: "兴隆县",
            value: "130822"
          }, {
            label: "滦平县",
            value: "130824"
          }, {
            label: "隆化县",
            value: "130825"
          }, {
            label: "丰宁满族自治县",
            value: "130826"
          }, {
            label: "宽城满族自治县",
            value: "130827"
          }, {
            label: "围场满族蒙古族自治县",
            value: "130828"
          }, {
            label: "承德高新技术产业开发区",
            value: "130871"
          }, {
            label: "平泉市",
            value: "130881"
          }],
          [{
            label: "新华区",
            value: "130902"
          }, {
            label: "运河区",
            value: "130903"
          }, {
            label: "沧县",
            value: "130921"
          }, {
            label: "青县",
            value: "130922"
          }, {
            label: "东光县",
            value: "130923"
          }, {
            label: "海兴县",
            value: "130924"
          }, {
            label: "盐山县",
            value: "130925"
          }, {
            label: "肃宁县",
            value: "130926"
          }, {
            label: "南皮县",
            value: "130927"
          }, {
            label: "吴桥县",
            value: "130928"
          }, {
            label: "献县",
            value: "130929"
          }, {
            label: "孟村回族自治县",
            value: "130930"
          }, {
            label: "河北沧州经济开发区",
            value: "130971"
          }, {
            label: "沧州高新技术产业开发区",
            value: "130972"
          }, {
            label: "沧州渤海新区",
            value: "130973"
          }, {
            label: "泊头市",
            value: "130981"
          }, {
            label: "任丘市",
            value: "130982"
          }, {
            label: "黄骅市",
            value: "130983"
          }, {
            label: "河间市",
            value: "130984"
          }],
          [{
            label: "安次区",
            value: "131002"
          }, {
            label: "广阳区",
            value: "131003"
          }, {
            label: "固安县",
            value: "131022"
          }, {
            label: "永清县",
            value: "131023"
          }, {
            label: "香河县",
            value: "131024"
          }, {
            label: "大城县",
            value: "131025"
          }, {
            label: "文安县",
            value: "131026"
          }, {
            label: "大厂回族自治县",
            value: "131028"
          }, {
            label: "廊坊经济技术开发区",
            value: "131071"
          }, {
            label: "霸州市",
            value: "131081"
          }, {
            label: "三河市",
            value: "131082"
          }],
          [{
            label: "桃城区",
            value: "131102"
          }, {
            label: "冀州区",
            value: "131103"
          }, {
            label: "枣强县",
            value: "131121"
          }, {
            label: "武邑县",
            value: "131122"
          }, {
            label: "武强县",
            value: "131123"
          }, {
            label: "饶阳县",
            value: "131124"
          }, {
            label: "安平县",
            value: "131125"
          }, {
            label: "故城县",
            value: "131126"
          }, {
            label: "景县",
            value: "131127"
          }, {
            label: "阜城县",
            value: "131128"
          }, {
            label: "河北衡水经济开发区",
            value: "131171"
          }, {
            label: "衡水滨湖新区",
            value: "131172"
          }, {
            label: "深州市",
            value: "131182"
          }]
        ],
        [
          [{
            label: "小店区",
            value: "140105"
          }, {
            label: "迎泽区",
            value: "140106"
          }, {
            label: "杏花岭区",
            value: "140107"
          }, {
            label: "尖草坪区",
            value: "140108"
          }, {
            label: "万柏林区",
            value: "140109"
          }, {
            label: "晋源区",
            value: "140110"
          }, {
            label: "清徐县",
            value: "140121"
          }, {
            label: "阳曲县",
            value: "140122"
          }, {
            label: "娄烦县",
            value: "140123"
          }, {
            label: "山西转型综合改革示范区",
            value: "140171"
          }, {
            label: "古交市",
            value: "140181"
          }],
          [{
            label: "城区",
            value: "140202"
          }, {
            label: "矿区",
            value: "140203"
          }, {
            label: "南郊区",
            value: "140211"
          }, {
            label: "新荣区",
            value: "140212"
          }, {
            label: "阳高县",
            value: "140221"
          }, {
            label: "天镇县",
            value: "140222"
          }, {
            label: "广灵县",
            value: "140223"
          }, {
            label: "灵丘县",
            value: "140224"
          }, {
            label: "浑源县",
            value: "140225"
          }, {
            label: "左云县",
            value: "140226"
          }, {
            label: "大同县",
            value: "140227"
          }, {
            label: "山西大同经济开发区",
            value: "140271"
          }],
          [{
            label: "城区",
            value: "140302"
          }, {
            label: "矿区",
            value: "140303"
          }, {
            label: "郊区",
            value: "140311"
          }, {
            label: "平定县",
            value: "140321"
          }, {
            label: "盂县",
            value: "140322"
          }, {
            label: "山西阳泉经济开发区",
            value: "140371"
          }],
          [{
            label: "城区",
            value: "140402"
          }, {
            label: "郊区",
            value: "140411"
          }, {
            label: "长治县",
            value: "140421"
          }, {
            label: "襄垣县",
            value: "140423"
          }, {
            label: "屯留县",
            value: "140424"
          }, {
            label: "平顺县",
            value: "140425"
          }, {
            label: "黎城县",
            value: "140426"
          }, {
            label: "壶关县",
            value: "140427"
          }, {
            label: "长子县",
            value: "140428"
          }, {
            label: "武乡县",
            value: "140429"
          }, {
            label: "沁县",
            value: "140430"
          }, {
            label: "沁源县",
            value: "140431"
          }, {
            label: "山西长治高新技术产业园区",
            value: "140471"
          }, {
            label: "潞城市",
            value: "140481"
          }],
          [{
            label: "城区",
            value: "140502"
          }, {
            label: "沁水县",
            value: "140521"
          }, {
            label: "阳城县",
            value: "140522"
          }, {
            label: "陵川县",
            value: "140524"
          }, {
            label: "泽州县",
            value: "140525"
          }, {
            label: "高平市",
            value: "140581"
          }],
          [{
            label: "朔城区",
            value: "140602"
          }, {
            label: "平鲁区",
            value: "140603"
          }, {
            label: "山阴县",
            value: "140621"
          }, {
            label: "应县",
            value: "140622"
          }, {
            label: "右玉县",
            value: "140623"
          }, {
            label: "怀仁县",
            value: "140624"
          }, {
            label: "山西朔州经济开发区",
            value: "140671"
          }],
          [{
            label: "榆次区",
            value: "140702"
          }, {
            label: "榆社县",
            value: "140721"
          }, {
            label: "左权县",
            value: "140722"
          }, {
            label: "和顺县",
            value: "140723"
          }, {
            label: "昔阳县",
            value: "140724"
          }, {
            label: "寿阳县",
            value: "140725"
          }, {
            label: "太谷县",
            value: "140726"
          }, {
            label: "祁县",
            value: "140727"
          }, {
            label: "平遥县",
            value: "140728"
          }, {
            label: "灵石县",
            value: "140729"
          }, {
            label: "介休市",
            value: "140781"
          }],
          [{
            label: "盐湖区",
            value: "140802"
          }, {
            label: "临猗县",
            value: "140821"
          }, {
            label: "万荣县",
            value: "140822"
          }, {
            label: "闻喜县",
            value: "140823"
          }, {
            label: "稷山县",
            value: "140824"
          }, {
            label: "新绛县",
            value: "140825"
          }, {
            label: "绛县",
            value: "140826"
          }, {
            label: "垣曲县",
            value: "140827"
          }, {
            label: "夏县",
            value: "140828"
          }, {
            label: "平陆县",
            value: "140829"
          }, {
            label: "芮城县",
            value: "140830"
          }, {
            label: "永济市",
            value: "140881"
          }, {
            label: "河津市",
            value: "140882"
          }],
          [{
            label: "忻府区",
            value: "140902"
          }, {
            label: "定襄县",
            value: "140921"
          }, {
            label: "五台县",
            value: "140922"
          }, {
            label: "代县",
            value: "140923"
          }, {
            label: "繁峙县",
            value: "140924"
          }, {
            label: "宁武县",
            value: "140925"
          }, {
            label: "静乐县",
            value: "140926"
          }, {
            label: "神池县",
            value: "140927"
          }, {
            label: "五寨县",
            value: "140928"
          }, {
            label: "岢岚县",
            value: "140929"
          }, {
            label: "河曲县",
            value: "140930"
          }, {
            label: "保德县",
            value: "140931"
          }, {
            label: "偏关县",
            value: "140932"
          }, {
            label: "五台山风景名胜区",
            value: "140971"
          }, {
            label: "原平市",
            value: "140981"
          }],
          [{
            label: "尧都区",
            value: "141002"
          }, {
            label: "曲沃县",
            value: "141021"
          }, {
            label: "翼城县",
            value: "141022"
          }, {
            label: "襄汾县",
            value: "141023"
          }, {
            label: "洪洞县",
            value: "141024"
          }, {
            label: "古县",
            value: "141025"
          }, {
            label: "安泽县",
            value: "141026"
          }, {
            label: "浮山县",
            value: "141027"
          }, {
            label: "吉县",
            value: "141028"
          }, {
            label: "乡宁县",
            value: "141029"
          }, {
            label: "大宁县",
            value: "141030"
          }, {
            label: "隰县",
            value: "141031"
          }, {
            label: "永和县",
            value: "141032"
          }, {
            label: "蒲县",
            value: "141033"
          }, {
            label: "汾西县",
            value: "141034"
          }, {
            label: "侯马市",
            value: "141081"
          }, {
            label: "霍州市",
            value: "141082"
          }],
          [{
            label: "离石区",
            value: "141102"
          }, {
            label: "文水县",
            value: "141121"
          }, {
            label: "交城县",
            value: "141122"
          }, {
            label: "兴县",
            value: "141123"
          }, {
            label: "临县",
            value: "141124"
          }, {
            label: "柳林县",
            value: "141125"
          }, {
            label: "石楼县",
            value: "141126"
          }, {
            label: "岚县",
            value: "141127"
          }, {
            label: "方山县",
            value: "141128"
          }, {
            label: "中阳县",
            value: "141129"
          }, {
            label: "交口县",
            value: "141130"
          }, {
            label: "孝义市",
            value: "141181"
          }, {
            label: "汾阳市",
            value: "141182"
          }]
        ],
        [
          [{
            label: "新城区",
            value: "150102"
          }, {
            label: "回民区",
            value: "150103"
          }, {
            label: "玉泉区",
            value: "150104"
          }, {
            label: "赛罕区",
            value: "150105"
          }, {
            label: "土默特左旗",
            value: "150121"
          }, {
            label: "托克托县",
            value: "150122"
          }, {
            label: "和林格尔县",
            value: "150123"
          }, {
            label: "清水河县",
            value: "150124"
          }, {
            label: "武川县",
            value: "150125"
          }, {
            label: "呼和浩特金海工业园区",
            value: "150171"
          }, {
            label: "呼和浩特经济技术开发区",
            value: "150172"
          }],
          [{
            label: "东河区",
            value: "150202"
          }, {
            label: "昆都仑区",
            value: "150203"
          }, {
            label: "青山区",
            value: "150204"
          }, {
            label: "石拐区",
            value: "150205"
          }, {
            label: "白云鄂博矿区",
            value: "150206"
          }, {
            label: "九原区",
            value: "150207"
          }, {
            label: "土默特右旗",
            value: "150221"
          }, {
            label: "固阳县",
            value: "150222"
          }, {
            label: "达尔罕茂明安联合旗",
            value: "150223"
          }, {
            label: "包头稀土高新技术产业开发区",
            value: "150271"
          }],
          [{
            label: "海勃湾区",
            value: "150302"
          }, {
            label: "海南区",
            value: "150303"
          }, {
            label: "乌达区",
            value: "150304"
          }],
          [{
            label: "红山区",
            value: "150402"
          }, {
            label: "元宝山区",
            value: "150403"
          }, {
            label: "松山区",
            value: "150404"
          }, {
            label: "阿鲁科尔沁旗",
            value: "150421"
          }, {
            label: "巴林左旗",
            value: "150422"
          }, {
            label: "巴林右旗",
            value: "150423"
          }, {
            label: "林西县",
            value: "150424"
          }, {
            label: "克什克腾旗",
            value: "150425"
          }, {
            label: "翁牛特旗",
            value: "150426"
          }, {
            label: "喀喇沁旗",
            value: "150428"
          }, {
            label: "宁城县",
            value: "150429"
          }, {
            label: "敖汉旗",
            value: "150430"
          }],
          [{
            label: "科尔沁区",
            value: "150502"
          }, {
            label: "科尔沁左翼中旗",
            value: "150521"
          }, {
            label: "科尔沁左翼后旗",
            value: "150522"
          }, {
            label: "开鲁县",
            value: "150523"
          }, {
            label: "库伦旗",
            value: "150524"
          }, {
            label: "奈曼旗",
            value: "150525"
          }, {
            label: "扎鲁特旗",
            value: "150526"
          }, {
            label: "通辽经济技术开发区",
            value: "150571"
          }, {
            label: "霍林郭勒市",
            value: "150581"
          }],
          [{
            label: "东胜区",
            value: "150602"
          }, {
            label: "康巴什区",
            value: "150603"
          }, {
            label: "达拉特旗",
            value: "150621"
          }, {
            label: "准格尔旗",
            value: "150622"
          }, {
            label: "鄂托克前旗",
            value: "150623"
          }, {
            label: "鄂托克旗",
            value: "150624"
          }, {
            label: "杭锦旗",
            value: "150625"
          }, {
            label: "乌审旗",
            value: "150626"
          }, {
            label: "伊金霍洛旗",
            value: "150627"
          }],
          [{
            label: "海拉尔区",
            value: "150702"
          }, {
            label: "扎赉诺尔区",
            value: "150703"
          }, {
            label: "阿荣旗",
            value: "150721"
          }, {
            label: "莫力达瓦达斡尔族自治旗",
            value: "150722"
          }, {
            label: "鄂伦春自治旗",
            value: "150723"
          }, {
            label: "鄂温克族自治旗",
            value: "150724"
          }, {
            label: "陈巴尔虎旗",
            value: "150725"
          }, {
            label: "新巴尔虎左旗",
            value: "150726"
          }, {
            label: "新巴尔虎右旗",
            value: "150727"
          }, {
            label: "满洲里市",
            value: "150781"
          }, {
            label: "牙克石市",
            value: "150782"
          }, {
            label: "扎兰屯市",
            value: "150783"
          }, {
            label: "额尔古纳市",
            value: "150784"
          }, {
            label: "根河市",
            value: "150785"
          }],
          [{
            label: "临河区",
            value: "150802"
          }, {
            label: "五原县",
            value: "150821"
          }, {
            label: "磴口县",
            value: "150822"
          }, {
            label: "乌拉特前旗",
            value: "150823"
          }, {
            label: "乌拉特中旗",
            value: "150824"
          }, {
            label: "乌拉特后旗",
            value: "150825"
          }, {
            label: "杭锦后旗",
            value: "150826"
          }],
          [{
            label: "集宁区",
            value: "150902"
          }, {
            label: "卓资县",
            value: "150921"
          }, {
            label: "化德县",
            value: "150922"
          }, {
            label: "商都县",
            value: "150923"
          }, {
            label: "兴和县",
            value: "150924"
          }, {
            label: "凉城县",
            value: "150925"
          }, {
            label: "察哈尔右翼前旗",
            value: "150926"
          }, {
            label: "察哈尔右翼中旗",
            value: "150927"
          }, {
            label: "察哈尔右翼后旗",
            value: "150928"
          }, {
            label: "四子王旗",
            value: "150929"
          }, {
            label: "丰镇市",
            value: "150981"
          }],
          [{
            label: "乌兰浩特市",
            value: "152201"
          }, {
            label: "阿尔山市",
            value: "152202"
          }, {
            label: "科尔沁右翼前旗",
            value: "152221"
          }, {
            label: "科尔沁右翼中旗",
            value: "152222"
          }, {
            label: "扎赉特旗",
            value: "152223"
          }, {
            label: "突泉县",
            value: "152224"
          }],
          [{
            label: "二连浩特市",
            value: "152501"
          }, {
            label: "锡林浩特市",
            value: "152502"
          }, {
            label: "阿巴嘎旗",
            value: "152522"
          }, {
            label: "苏尼特左旗",
            value: "152523"
          }, {
            label: "苏尼特右旗",
            value: "152524"
          }, {
            label: "东乌珠穆沁旗",
            value: "152525"
          }, {
            label: "西乌珠穆沁旗",
            value: "152526"
          }, {
            label: "太仆寺旗",
            value: "152527"
          }, {
            label: "镶黄旗",
            value: "152528"
          }, {
            label: "正镶白旗",
            value: "152529"
          }, {
            label: "正蓝旗",
            value: "152530"
          }, {
            label: "多伦县",
            value: "152531"
          }, {
            label: "乌拉盖管委会",
            value: "152571"
          }],
          [{
            label: "阿拉善左旗",
            value: "152921"
          }, {
            label: "阿拉善右旗",
            value: "152922"
          }, {
            label: "额济纳旗",
            value: "152923"
          }, {
            label: "内蒙古阿拉善经济开发区",
            value: "152971"
          }]
        ],
        [
          [{
            label: "和平区",
            value: "210102"
          }, {
            label: "沈河区",
            value: "210103"
          }, {
            label: "大东区",
            value: "210104"
          }, {
            label: "皇姑区",
            value: "210105"
          }, {
            label: "铁西区",
            value: "210106"
          }, {
            label: "苏家屯区",
            value: "210111"
          }, {
            label: "浑南区",
            value: "210112"
          }, {
            label: "沈北新区",
            value: "210113"
          }, {
            label: "于洪区",
            value: "210114"
          }, {
            label: "辽中区",
            value: "210115"
          }, {
            label: "康平县",
            value: "210123"
          }, {
            label: "法库县",
            value: "210124"
          }, {
            label: "新民市",
            value: "210181"
          }],
          [{
            label: "中山区",
            value: "210202"
          }, {
            label: "西岗区",
            value: "210203"
          }, {
            label: "沙河口区",
            value: "210204"
          }, {
            label: "甘井子区",
            value: "210211"
          }, {
            label: "旅顺口区",
            value: "210212"
          }, {
            label: "金州区",
            value: "210213"
          }, {
            label: "普兰店区",
            value: "210214"
          }, {
            label: "长海县",
            value: "210224"
          }, {
            label: "瓦房店市",
            value: "210281"
          }, {
            label: "庄河市",
            value: "210283"
          }],
          [{
            label: "铁东区",
            value: "210302"
          }, {
            label: "铁西区",
            value: "210303"
          }, {
            label: "立山区",
            value: "210304"
          }, {
            label: "千山区",
            value: "210311"
          }, {
            label: "台安县",
            value: "210321"
          }, {
            label: "岫岩满族自治县",
            value: "210323"
          }, {
            label: "海城市",
            value: "210381"
          }],
          [{
            label: "新抚区",
            value: "210402"
          }, {
            label: "东洲区",
            value: "210403"
          }, {
            label: "望花区",
            value: "210404"
          }, {
            label: "顺城区",
            value: "210411"
          }, {
            label: "抚顺县",
            value: "210421"
          }, {
            label: "新宾满族自治县",
            value: "210422"
          }, {
            label: "清原满族自治县",
            value: "210423"
          }],
          [{
            label: "平山区",
            value: "210502"
          }, {
            label: "溪湖区",
            value: "210503"
          }, {
            label: "明山区",
            value: "210504"
          }, {
            label: "南芬区",
            value: "210505"
          }, {
            label: "本溪满族自治县",
            value: "210521"
          }, {
            label: "桓仁满族自治县",
            value: "210522"
          }],
          [{
            label: "元宝区",
            value: "210602"
          }, {
            label: "振兴区",
            value: "210603"
          }, {
            label: "振安区",
            value: "210604"
          }, {
            label: "宽甸满族自治县",
            value: "210624"
          }, {
            label: "东港市",
            value: "210681"
          }, {
            label: "凤城市",
            value: "210682"
          }],
          [{
            label: "古塔区",
            value: "210702"
          }, {
            label: "凌河区",
            value: "210703"
          }, {
            label: "太和区",
            value: "210711"
          }, {
            label: "黑山县",
            value: "210726"
          }, {
            label: "义县",
            value: "210727"
          }, {
            label: "凌海市",
            value: "210781"
          }, {
            label: "北镇市",
            value: "210782"
          }],
          [{
            label: "站前区",
            value: "210802"
          }, {
            label: "西市区",
            value: "210803"
          }, {
            label: "鲅鱼圈区",
            value: "210804"
          }, {
            label: "老边区",
            value: "210811"
          }, {
            label: "盖州市",
            value: "210881"
          }, {
            label: "大石桥市",
            value: "210882"
          }],
          [{
            label: "海州区",
            value: "210902"
          }, {
            label: "新邱区",
            value: "210903"
          }, {
            label: "太平区",
            value: "210904"
          }, {
            label: "清河门区",
            value: "210905"
          }, {
            label: "细河区",
            value: "210911"
          }, {
            label: "阜新蒙古族自治县",
            value: "210921"
          }, {
            label: "彰武县",
            value: "210922"
          }],
          [{
            label: "白塔区",
            value: "211002"
          }, {
            label: "文圣区",
            value: "211003"
          }, {
            label: "宏伟区",
            value: "211004"
          }, {
            label: "弓长岭区",
            value: "211005"
          }, {
            label: "太子河区",
            value: "211011"
          }, {
            label: "辽阳县",
            value: "211021"
          }, {
            label: "灯塔市",
            value: "211081"
          }],
          [{
            label: "双台子区",
            value: "211102"
          }, {
            label: "兴隆台区",
            value: "211103"
          }, {
            label: "大洼区",
            value: "211104"
          }, {
            label: "盘山县",
            value: "211122"
          }],
          [{
            label: "银州区",
            value: "211202"
          }, {
            label: "清河区",
            value: "211204"
          }, {
            label: "铁岭县",
            value: "211221"
          }, {
            label: "西丰县",
            value: "211223"
          }, {
            label: "昌图县",
            value: "211224"
          }, {
            label: "调兵山市",
            value: "211281"
          }, {
            label: "开原市",
            value: "211282"
          }],
          [{
            label: "双塔区",
            value: "211302"
          }, {
            label: "龙城区",
            value: "211303"
          }, {
            label: "朝阳县",
            value: "211321"
          }, {
            label: "建平县",
            value: "211322"
          }, {
            label: "喀喇沁左翼蒙古族自治县",
            value: "211324"
          }, {
            label: "北票市",
            value: "211381"
          }, {
            label: "凌源市",
            value: "211382"
          }],
          [{
            label: "连山区",
            value: "211402"
          }, {
            label: "龙港区",
            value: "211403"
          }, {
            label: "南票区",
            value: "211404"
          }, {
            label: "绥中县",
            value: "211421"
          }, {
            label: "建昌县",
            value: "211422"
          }, {
            label: "兴城市",
            value: "211481"
          }]
        ],
        [
          [{
            label: "南关区",
            value: "220102"
          }, {
            label: "宽城区",
            value: "220103"
          }, {
            label: "朝阳区",
            value: "220104"
          }, {
            label: "二道区",
            value: "220105"
          }, {
            label: "绿园区",
            value: "220106"
          }, {
            label: "双阳区",
            value: "220112"
          }, {
            label: "九台区",
            value: "220113"
          }, {
            label: "农安县",
            value: "220122"
          }, {
            label: "长春经济技术开发区",
            value: "220171"
          }, {
            label: "长春净月高新技术产业开发区",
            value: "220172"
          }, {
            label: "长春高新技术产业开发区",
            value: "220173"
          }, {
            label: "长春汽车经济技术开发区",
            value: "220174"
          }, {
            label: "榆树市",
            value: "220182"
          }, {
            label: "德惠市",
            value: "220183"
          }],
          [{
            label: "昌邑区",
            value: "220202"
          }, {
            label: "龙潭区",
            value: "220203"
          }, {
            label: "船营区",
            value: "220204"
          }, {
            label: "丰满区",
            value: "220211"
          }, {
            label: "永吉县",
            value: "220221"
          }, {
            label: "吉林经济开发区",
            value: "220271"
          }, {
            label: "吉林高新技术产业开发区",
            value: "220272"
          }, {
            label: "吉林中国新加坡食品区",
            value: "220273"
          }, {
            label: "蛟河市",
            value: "220281"
          }, {
            label: "桦甸市",
            value: "220282"
          }, {
            label: "舒兰市",
            value: "220283"
          }, {
            label: "磐石市",
            value: "220284"
          }],
          [{
            label: "铁西区",
            value: "220302"
          }, {
            label: "铁东区",
            value: "220303"
          }, {
            label: "梨树县",
            value: "220322"
          }, {
            label: "伊通满族自治县",
            value: "220323"
          }, {
            label: "公主岭市",
            value: "220381"
          }, {
            label: "双辽市",
            value: "220382"
          }],
          [{
            label: "龙山区",
            value: "220402"
          }, {
            label: "西安区",
            value: "220403"
          }, {
            label: "东丰县",
            value: "220421"
          }, {
            label: "东辽县",
            value: "220422"
          }],
          [{
            label: "东昌区",
            value: "220502"
          }, {
            label: "二道江区",
            value: "220503"
          }, {
            label: "通化县",
            value: "220521"
          }, {
            label: "辉南县",
            value: "220523"
          }, {
            label: "柳河县",
            value: "220524"
          }, {
            label: "梅河口市",
            value: "220581"
          }, {
            label: "集安市",
            value: "220582"
          }],
          [{
            label: "浑江区",
            value: "220602"
          }, {
            label: "江源区",
            value: "220605"
          }, {
            label: "抚松县",
            value: "220621"
          }, {
            label: "靖宇县",
            value: "220622"
          }, {
            label: "长白朝鲜族自治县",
            value: "220623"
          }, {
            label: "临江市",
            value: "220681"
          }],
          [{
            label: "宁江区",
            value: "220702"
          }, {
            label: "前郭尔罗斯蒙古族自治县",
            value: "220721"
          }, {
            label: "长岭县",
            value: "220722"
          }, {
            label: "乾安县",
            value: "220723"
          }, {
            label: "吉林松原经济开发区",
            value: "220771"
          }, {
            label: "扶余市",
            value: "220781"
          }],
          [{
            label: "洮北区",
            value: "220802"
          }, {
            label: "镇赉县",
            value: "220821"
          }, {
            label: "通榆县",
            value: "220822"
          }, {
            label: "吉林白城经济开发区",
            value: "220871"
          }, {
            label: "洮南市",
            value: "220881"
          }, {
            label: "大安市",
            value: "220882"
          }],
          [{
            label: "延吉市",
            value: "222401"
          }, {
            label: "图们市",
            value: "222402"
          }, {
            label: "敦化市",
            value: "222403"
          }, {
            label: "珲春市",
            value: "222404"
          }, {
            label: "龙井市",
            value: "222405"
          }, {
            label: "和龙市",
            value: "222406"
          }, {
            label: "汪清县",
            value: "222424"
          }, {
            label: "安图县",
            value: "222426"
          }]
        ],
        [
          [{
            label: "道里区",
            value: "230102"
          }, {
            label: "南岗区",
            value: "230103"
          }, {
            label: "道外区",
            value: "230104"
          }, {
            label: "平房区",
            value: "230108"
          }, {
            label: "松北区",
            value: "230109"
          }, {
            label: "香坊区",
            value: "230110"
          }, {
            label: "呼兰区",
            value: "230111"
          }, {
            label: "阿城区",
            value: "230112"
          }, {
            label: "双城区",
            value: "230113"
          }, {
            label: "依兰县",
            value: "230123"
          }, {
            label: "方正县",
            value: "230124"
          }, {
            label: "宾县",
            value: "230125"
          }, {
            label: "巴彦县",
            value: "230126"
          }, {
            label: "木兰县",
            value: "230127"
          }, {
            label: "通河县",
            value: "230128"
          }, {
            label: "延寿县",
            value: "230129"
          }, {
            label: "尚志市",
            value: "230183"
          }, {
            label: "五常市",
            value: "230184"
          }],
          [{
            label: "龙沙区",
            value: "230202"
          }, {
            label: "建华区",
            value: "230203"
          }, {
            label: "铁锋区",
            value: "230204"
          }, {
            label: "昂昂溪区",
            value: "230205"
          }, {
            label: "富拉尔基区",
            value: "230206"
          }, {
            label: "碾子山区",
            value: "230207"
          }, {
            label: "梅里斯达斡尔族区",
            value: "230208"
          }, {
            label: "龙江县",
            value: "230221"
          }, {
            label: "依安县",
            value: "230223"
          }, {
            label: "泰来县",
            value: "230224"
          }, {
            label: "甘南县",
            value: "230225"
          }, {
            label: "富裕县",
            value: "230227"
          }, {
            label: "克山县",
            value: "230229"
          }, {
            label: "克东县",
            value: "230230"
          }, {
            label: "拜泉县",
            value: "230231"
          }, {
            label: "讷河市",
            value: "230281"
          }],
          [{
            label: "鸡冠区",
            value: "230302"
          }, {
            label: "恒山区",
            value: "230303"
          }, {
            label: "滴道区",
            value: "230304"
          }, {
            label: "梨树区",
            value: "230305"
          }, {
            label: "城子河区",
            value: "230306"
          }, {
            label: "麻山区",
            value: "230307"
          }, {
            label: "鸡东县",
            value: "230321"
          }, {
            label: "虎林市",
            value: "230381"
          }, {
            label: "密山市",
            value: "230382"
          }],
          [{
            label: "向阳区",
            value: "230402"
          }, {
            label: "工农区",
            value: "230403"
          }, {
            label: "南山区",
            value: "230404"
          }, {
            label: "兴安区",
            value: "230405"
          }, {
            label: "东山区",
            value: "230406"
          }, {
            label: "兴山区",
            value: "230407"
          }, {
            label: "萝北县",
            value: "230421"
          }, {
            label: "绥滨县",
            value: "230422"
          }],
          [{
            label: "尖山区",
            value: "230502"
          }, {
            label: "岭东区",
            value: "230503"
          }, {
            label: "四方台区",
            value: "230505"
          }, {
            label: "宝山区",
            value: "230506"
          }, {
            label: "集贤县",
            value: "230521"
          }, {
            label: "友谊县",
            value: "230522"
          }, {
            label: "宝清县",
            value: "230523"
          }, {
            label: "饶河县",
            value: "230524"
          }],
          [{
            label: "萨尔图区",
            value: "230602"
          }, {
            label: "龙凤区",
            value: "230603"
          }, {
            label: "让胡路区",
            value: "230604"
          }, {
            label: "红岗区",
            value: "230605"
          }, {
            label: "大同区",
            value: "230606"
          }, {
            label: "肇州县",
            value: "230621"
          }, {
            label: "肇源县",
            value: "230622"
          }, {
            label: "林甸县",
            value: "230623"
          }, {
            label: "杜尔伯特蒙古族自治县",
            value: "230624"
          }, {
            label: "大庆高新技术产业开发区",
            value: "230671"
          }],
          [{
            label: "伊春区",
            value: "230702"
          }, {
            label: "南岔区",
            value: "230703"
          }, {
            label: "友好区",
            value: "230704"
          }, {
            label: "西林区",
            value: "230705"
          }, {
            label: "翠峦区",
            value: "230706"
          }, {
            label: "新青区",
            value: "230707"
          }, {
            label: "美溪区",
            value: "230708"
          }, {
            label: "金山屯区",
            value: "230709"
          }, {
            label: "五营区",
            value: "230710"
          }, {
            label: "乌马河区",
            value: "230711"
          }, {
            label: "汤旺河区",
            value: "230712"
          }, {
            label: "带岭区",
            value: "230713"
          }, {
            label: "乌伊岭区",
            value: "230714"
          }, {
            label: "红星区",
            value: "230715"
          }, {
            label: "上甘岭区",
            value: "230716"
          }, {
            label: "嘉荫县",
            value: "230722"
          }, {
            label: "铁力市",
            value: "230781"
          }],
          [{
            label: "向阳区",
            value: "230803"
          }, {
            label: "前进区",
            value: "230804"
          }, {
            label: "东风区",
            value: "230805"
          }, {
            label: "郊区",
            value: "230811"
          }, {
            label: "桦南县",
            value: "230822"
          }, {
            label: "桦川县",
            value: "230826"
          }, {
            label: "汤原县",
            value: "230828"
          }, {
            label: "同江市",
            value: "230881"
          }, {
            label: "富锦市",
            value: "230882"
          }, {
            label: "抚远市",
            value: "230883"
          }],
          [{
            label: "新兴区",
            value: "230902"
          }, {
            label: "桃山区",
            value: "230903"
          }, {
            label: "茄子河区",
            value: "230904"
          }, {
            label: "勃利县",
            value: "230921"
          }],
          [{
            label: "东安区",
            value: "231002"
          }, {
            label: "阳明区",
            value: "231003"
          }, {
            label: "爱民区",
            value: "231004"
          }, {
            label: "西安区",
            value: "231005"
          }, {
            label: "林口县",
            value: "231025"
          }, {
            label: "牡丹江经济技术开发区",
            value: "231071"
          }, {
            label: "绥芬河市",
            value: "231081"
          }, {
            label: "海林市",
            value: "231083"
          }, {
            label: "宁安市",
            value: "231084"
          }, {
            label: "穆棱市",
            value: "231085"
          }, {
            label: "东宁市",
            value: "231086"
          }],
          [{
            label: "爱辉区",
            value: "231102"
          }, {
            label: "嫩江县",
            value: "231121"
          }, {
            label: "逊克县",
            value: "231123"
          }, {
            label: "孙吴县",
            value: "231124"
          }, {
            label: "北安市",
            value: "231181"
          }, {
            label: "五大连池市",
            value: "231182"
          }],
          [{
            label: "北林区",
            value: "231202"
          }, {
            label: "望奎县",
            value: "231221"
          }, {
            label: "兰西县",
            value: "231222"
          }, {
            label: "青冈县",
            value: "231223"
          }, {
            label: "庆安县",
            value: "231224"
          }, {
            label: "明水县",
            value: "231225"
          }, {
            label: "绥棱县",
            value: "231226"
          }, {
            label: "安达市",
            value: "231281"
          }, {
            label: "肇东市",
            value: "231282"
          }, {
            label: "海伦市",
            value: "231283"
          }],
          [{
            label: "加格达奇区",
            value: "232701"
          }, {
            label: "松岭区",
            value: "232702"
          }, {
            label: "新林区",
            value: "232703"
          }, {
            label: "呼中区",
            value: "232704"
          }, {
            label: "呼玛县",
            value: "232721"
          }, {
            label: "塔河县",
            value: "232722"
          }, {
            label: "漠河县",
            value: "232723"
          }]
        ],
        [
          [{
            label: "黄浦区",
            value: "310101"
          }, {
            label: "徐汇区",
            value: "310104"
          }, {
            label: "长宁区",
            value: "310105"
          }, {
            label: "静安区",
            value: "310106"
          }, {
            label: "普陀区",
            value: "310107"
          }, {
            label: "虹口区",
            value: "310109"
          }, {
            label: "杨浦区",
            value: "310110"
          }, {
            label: "闵行区",
            value: "310112"
          }, {
            label: "宝山区",
            value: "310113"
          }, {
            label: "嘉定区",
            value: "310114"
          }, {
            label: "浦东新区",
            value: "310115"
          }, {
            label: "金山区",
            value: "310116"
          }, {
            label: "松江区",
            value: "310117"
          }, {
            label: "青浦区",
            value: "310118"
          }, {
            label: "奉贤区",
            value: "310120"
          }, {
            label: "崇明区",
            value: "310151"
          }]
        ],
        [
          [{
            label: "玄武区",
            value: "320102"
          }, {
            label: "秦淮区",
            value: "320104"
          }, {
            label: "建邺区",
            value: "320105"
          }, {
            label: "鼓楼区",
            value: "320106"
          }, {
            label: "浦口区",
            value: "320111"
          }, {
            label: "栖霞区",
            value: "320113"
          }, {
            label: "雨花台区",
            value: "320114"
          }, {
            label: "江宁区",
            value: "320115"
          }, {
            label: "六合区",
            value: "320116"
          }, {
            label: "溧水区",
            value: "320117"
          }, {
            label: "高淳区",
            value: "320118"
          }],
          [{
            label: "锡山区",
            value: "320205"
          }, {
            label: "惠山区",
            value: "320206"
          }, {
            label: "滨湖区",
            value: "320211"
          }, {
            label: "梁溪区",
            value: "320213"
          }, {
            label: "新吴区",
            value: "320214"
          }, {
            label: "江阴市",
            value: "320281"
          }, {
            label: "宜兴市",
            value: "320282"
          }],
          [{
            label: "鼓楼区",
            value: "320302"
          }, {
            label: "云龙区",
            value: "320303"
          }, {
            label: "贾汪区",
            value: "320305"
          }, {
            label: "泉山区",
            value: "320311"
          }, {
            label: "铜山区",
            value: "320312"
          }, {
            label: "丰县",
            value: "320321"
          }, {
            label: "沛县",
            value: "320322"
          }, {
            label: "睢宁县",
            value: "320324"
          }, {
            label: "徐州经济技术开发区",
            value: "320371"
          }, {
            label: "新沂市",
            value: "320381"
          }, {
            label: "邳州市",
            value: "320382"
          }],
          [{
            label: "天宁区",
            value: "320402"
          }, {
            label: "钟楼区",
            value: "320404"
          }, {
            label: "新北区",
            value: "320411"
          }, {
            label: "武进区",
            value: "320412"
          }, {
            label: "金坛区",
            value: "320413"
          }, {
            label: "溧阳市",
            value: "320481"
          }],
          [{
            label: "虎丘区",
            value: "320505"
          }, {
            label: "吴中区",
            value: "320506"
          }, {
            label: "相城区",
            value: "320507"
          }, {
            label: "姑苏区",
            value: "320508"
          }, {
            label: "吴江区",
            value: "320509"
          }, {
            label: "苏州工业园区",
            value: "320571"
          }, {
            label: "常熟市",
            value: "320581"
          }, {
            label: "张家港市",
            value: "320582"
          }, {
            label: "昆山市",
            value: "320583"
          }, {
            label: "太仓市",
            value: "320585"
          }],
          [{
            label: "崇川区",
            value: "320602"
          }, {
            label: "港闸区",
            value: "320611"
          }, {
            label: "通州区",
            value: "320612"
          }, {
            label: "海安县",
            value: "320621"
          }, {
            label: "如东县",
            value: "320623"
          }, {
            label: "南通经济技术开发区",
            value: "320671"
          }, {
            label: "启东市",
            value: "320681"
          }, {
            label: "如皋市",
            value: "320682"
          }, {
            label: "海门市",
            value: "320684"
          }],
          [{
            label: "连云区",
            value: "320703"
          }, {
            label: "海州区",
            value: "320706"
          }, {
            label: "赣榆区",
            value: "320707"
          }, {
            label: "东海县",
            value: "320722"
          }, {
            label: "灌云县",
            value: "320723"
          }, {
            label: "灌南县",
            value: "320724"
          }, {
            label: "连云港经济技术开发区",
            value: "320771"
          }, {
            label: "连云港高新技术产业开发区",
            value: "320772"
          }],
          [{
            label: "淮安区",
            value: "320803"
          }, {
            label: "淮阴区",
            value: "320804"
          }, {
            label: "清江浦区",
            value: "320812"
          }, {
            label: "洪泽区",
            value: "320813"
          }, {
            label: "涟水县",
            value: "320826"
          }, {
            label: "盱眙县",
            value: "320830"
          }, {
            label: "金湖县",
            value: "320831"
          }, {
            label: "淮安经济技术开发区",
            value: "320871"
          }],
          [{
            label: "亭湖区",
            value: "320902"
          }, {
            label: "盐都区",
            value: "320903"
          }, {
            label: "大丰区",
            value: "320904"
          }, {
            label: "响水县",
            value: "320921"
          }, {
            label: "滨海县",
            value: "320922"
          }, {
            label: "阜宁县",
            value: "320923"
          }, {
            label: "射阳县",
            value: "320924"
          }, {
            label: "建湖县",
            value: "320925"
          }, {
            label: "盐城经济技术开发区",
            value: "320971"
          }, {
            label: "东台市",
            value: "320981"
          }],
          [{
            label: "广陵区",
            value: "321002"
          }, {
            label: "邗江区",
            value: "321003"
          }, {
            label: "江都区",
            value: "321012"
          }, {
            label: "宝应县",
            value: "321023"
          }, {
            label: "扬州经济技术开发区",
            value: "321071"
          }, {
            label: "仪征市",
            value: "321081"
          }, {
            label: "高邮市",
            value: "321084"
          }],
          [{
            label: "京口区",
            value: "321102"
          }, {
            label: "润州区",
            value: "321111"
          }, {
            label: "丹徒区",
            value: "321112"
          }, {
            label: "镇江新区",
            value: "321171"
          }, {
            label: "丹阳市",
            value: "321181"
          }, {
            label: "扬中市",
            value: "321182"
          }, {
            label: "句容市",
            value: "321183"
          }],
          [{
            label: "海陵区",
            value: "321202"
          }, {
            label: "高港区",
            value: "321203"
          }, {
            label: "姜堰区",
            value: "321204"
          }, {
            label: "泰州医药高新技术产业开发区",
            value: "321271"
          }, {
            label: "兴化市",
            value: "321281"
          }, {
            label: "靖江市",
            value: "321282"
          }, {
            label: "泰兴市",
            value: "321283"
          }],
          [{
            label: "宿城区",
            value: "321302"
          }, {
            label: "宿豫区",
            value: "321311"
          }, {
            label: "沭阳县",
            value: "321322"
          }, {
            label: "泗阳县",
            value: "321323"
          }, {
            label: "泗洪县",
            value: "321324"
          }, {
            label: "宿迁经济技术开发区",
            value: "321371"
          }]
        ],
        [
          [{
            label: "上城区",
            value: "330102"
          }, {
            label: "下城区",
            value: "330103"
          }, {
            label: "江干区",
            value: "330104"
          }, {
            label: "拱墅区",
            value: "330105"
          }, {
            label: "西湖区",
            value: "330106"
          }, {
            label: "滨江区",
            value: "330108"
          }, {
            label: "萧山区",
            value: "330109"
          }, {
            label: "余杭区",
            value: "330110"
          }, {
            label: "富阳区",
            value: "330111"
          }, {
            label: "临安区",
            value: "330112"
          }, {
            label: "桐庐县",
            value: "330122"
          }, {
            label: "淳安县",
            value: "330127"
          }, {
            label: "建德市",
            value: "330182"
          }],
          [{
            label: "海曙区",
            value: "330203"
          }, {
            label: "江北区",
            value: "330205"
          }, {
            label: "北仑区",
            value: "330206"
          }, {
            label: "镇海区",
            value: "330211"
          }, {
            label: "鄞州区",
            value: "330212"
          }, {
            label: "奉化区",
            value: "330213"
          }, {
            label: "象山县",
            value: "330225"
          }, {
            label: "宁海县",
            value: "330226"
          }, {
            label: "余姚市",
            value: "330281"
          }, {
            label: "慈溪市",
            value: "330282"
          }],
          [{
            label: "鹿城区",
            value: "330302"
          }, {
            label: "龙湾区",
            value: "330303"
          }, {
            label: "瓯海区",
            value: "330304"
          }, {
            label: "洞头区",
            value: "330305"
          }, {
            label: "永嘉县",
            value: "330324"
          }, {
            label: "平阳县",
            value: "330326"
          }, {
            label: "苍南县",
            value: "330327"
          }, {
            label: "文成县",
            value: "330328"
          }, {
            label: "泰顺县",
            value: "330329"
          }, {
            label: "温州经济技术开发区",
            value: "330371"
          }, {
            label: "瑞安市",
            value: "330381"
          }, {
            label: "乐清市",
            value: "330382"
          }],
          [{
            label: "南湖区",
            value: "330402"
          }, {
            label: "秀洲区",
            value: "330411"
          }, {
            label: "嘉善县",
            value: "330421"
          }, {
            label: "海盐县",
            value: "330424"
          }, {
            label: "海宁市",
            value: "330481"
          }, {
            label: "平湖市",
            value: "330482"
          }, {
            label: "桐乡市",
            value: "330483"
          }],
          [{
            label: "吴兴区",
            value: "330502"
          }, {
            label: "南浔区",
            value: "330503"
          }, {
            label: "德清县",
            value: "330521"
          }, {
            label: "长兴县",
            value: "330522"
          }, {
            label: "安吉县",
            value: "330523"
          }],
          [{
            label: "越城区",
            value: "330602"
          }, {
            label: "柯桥区",
            value: "330603"
          }, {
            label: "上虞区",
            value: "330604"
          }, {
            label: "新昌县",
            value: "330624"
          }, {
            label: "诸暨市",
            value: "330681"
          }, {
            label: "嵊州市",
            value: "330683"
          }],
          [{
            label: "婺城区",
            value: "330702"
          }, {
            label: "金东区",
            value: "330703"
          }, {
            label: "武义县",
            value: "330723"
          }, {
            label: "浦江县",
            value: "330726"
          }, {
            label: "磐安县",
            value: "330727"
          }, {
            label: "兰溪市",
            value: "330781"
          }, {
            label: "义乌市",
            value: "330782"
          }, {
            label: "东阳市",
            value: "330783"
          }, {
            label: "永康市",
            value: "330784"
          }],
          [{
            label: "柯城区",
            value: "330802"
          }, {
            label: "衢江区",
            value: "330803"
          }, {
            label: "常山县",
            value: "330822"
          }, {
            label: "开化县",
            value: "330824"
          }, {
            label: "龙游县",
            value: "330825"
          }, {
            label: "江山市",
            value: "330881"
          }],
          [{
            label: "定海区",
            value: "330902"
          }, {
            label: "普陀区",
            value: "330903"
          }, {
            label: "岱山县",
            value: "330921"
          }, {
            label: "嵊泗县",
            value: "330922"
          }],
          [{
            label: "椒江区",
            value: "331002"
          }, {
            label: "黄岩区",
            value: "331003"
          }, {
            label: "路桥区",
            value: "331004"
          }, {
            label: "三门县",
            value: "331022"
          }, {
            label: "天台县",
            value: "331023"
          }, {
            label: "仙居县",
            value: "331024"
          }, {
            label: "温岭市",
            value: "331081"
          }, {
            label: "临海市",
            value: "331082"
          }, {
            label: "玉环市",
            value: "331083"
          }],
          [{
            label: "莲都区",
            value: "331102"
          }, {
            label: "青田县",
            value: "331121"
          }, {
            label: "缙云县",
            value: "331122"
          }, {
            label: "遂昌县",
            value: "331123"
          }, {
            label: "松阳县",
            value: "331124"
          }, {
            label: "云和县",
            value: "331125"
          }, {
            label: "庆元县",
            value: "331126"
          }, {
            label: "景宁畲族自治县",
            value: "331127"
          }, {
            label: "龙泉市",
            value: "331181"
          }]
        ],
        [
          [{
            label: "瑶海区",
            value: "340102"
          }, {
            label: "庐阳区",
            value: "340103"
          }, {
            label: "蜀山区",
            value: "340104"
          }, {
            label: "包河区",
            value: "340111"
          }, {
            label: "长丰县",
            value: "340121"
          }, {
            label: "肥东县",
            value: "340122"
          }, {
            label: "肥西县",
            value: "340123"
          }, {
            label: "庐江县",
            value: "340124"
          }, {
            label: "合肥高新技术产业开发区",
            value: "340171"
          }, {
            label: "合肥经济技术开发区",
            value: "340172"
          }, {
            label: "合肥新站高新技术产业开发区",
            value: "340173"
          }, {
            label: "巢湖市",
            value: "340181"
          }],
          [{
            label: "镜湖区",
            value: "340202"
          }, {
            label: "弋江区",
            value: "340203"
          }, {
            label: "鸠江区",
            value: "340207"
          }, {
            label: "三山区",
            value: "340208"
          }, {
            label: "芜湖县",
            value: "340221"
          }, {
            label: "繁昌县",
            value: "340222"
          }, {
            label: "南陵县",
            value: "340223"
          }, {
            label: "无为县",
            value: "340225"
          }, {
            label: "芜湖经济技术开发区",
            value: "340271"
          }, {
            label: "安徽芜湖长江大桥经济开发区",
            value: "340272"
          }],
          [{
            label: "龙子湖区",
            value: "340302"
          }, {
            label: "蚌山区",
            value: "340303"
          }, {
            label: "禹会区",
            value: "340304"
          }, {
            label: "淮上区",
            value: "340311"
          }, {
            label: "怀远县",
            value: "340321"
          }, {
            label: "五河县",
            value: "340322"
          }, {
            label: "固镇县",
            value: "340323"
          }, {
            label: "蚌埠市高新技术开发区",
            value: "340371"
          }, {
            label: "蚌埠市经济开发区",
            value: "340372"
          }],
          [{
            label: "大通区",
            value: "340402"
          }, {
            label: "田家庵区",
            value: "340403"
          }, {
            label: "谢家集区",
            value: "340404"
          }, {
            label: "八公山区",
            value: "340405"
          }, {
            label: "潘集区",
            value: "340406"
          }, {
            label: "凤台县",
            value: "340421"
          }, {
            label: "寿县",
            value: "340422"
          }],
          [{
            label: "花山区",
            value: "340503"
          }, {
            label: "雨山区",
            value: "340504"
          }, {
            label: "博望区",
            value: "340506"
          }, {
            label: "当涂县",
            value: "340521"
          }, {
            label: "含山县",
            value: "340522"
          }, {
            label: "和县",
            value: "340523"
          }],
          [{
            label: "杜集区",
            value: "340602"
          }, {
            label: "相山区",
            value: "340603"
          }, {
            label: "烈山区",
            value: "340604"
          }, {
            label: "濉溪县",
            value: "340621"
          }],
          [{
            label: "铜官区",
            value: "340705"
          }, {
            label: "义安区",
            value: "340706"
          }, {
            label: "郊区",
            value: "340711"
          }, {
            label: "枞阳县",
            value: "340722"
          }],
          [{
            label: "迎江区",
            value: "340802"
          }, {
            label: "大观区",
            value: "340803"
          }, {
            label: "宜秀区",
            value: "340811"
          }, {
            label: "怀宁县",
            value: "340822"
          }, {
            label: "潜山县",
            value: "340824"
          }, {
            label: "太湖县",
            value: "340825"
          }, {
            label: "宿松县",
            value: "340826"
          }, {
            label: "望江县",
            value: "340827"
          }, {
            label: "岳西县",
            value: "340828"
          }, {
            label: "安徽安庆经济开发区",
            value: "340871"
          }, {
            label: "桐城市",
            value: "340881"
          }],
          [{
            label: "屯溪区",
            value: "341002"
          }, {
            label: "黄山区",
            value: "341003"
          }, {
            label: "徽州区",
            value: "341004"
          }, {
            label: "歙县",
            value: "341021"
          }, {
            label: "休宁县",
            value: "341022"
          }, {
            label: "黟县",
            value: "341023"
          }, {
            label: "祁门县",
            value: "341024"
          }],
          [{
            label: "琅琊区",
            value: "341102"
          }, {
            label: "南谯区",
            value: "341103"
          }, {
            label: "来安县",
            value: "341122"
          }, {
            label: "全椒县",
            value: "341124"
          }, {
            label: "定远县",
            value: "341125"
          }, {
            label: "凤阳县",
            value: "341126"
          }, {
            label: "苏滁现代产业园",
            value: "341171"
          }, {
            label: "滁州经济技术开发区",
            value: "341172"
          }, {
            label: "天长市",
            value: "341181"
          }, {
            label: "明光市",
            value: "341182"
          }],
          [{
            label: "颍州区",
            value: "341202"
          }, {
            label: "颍东区",
            value: "341203"
          }, {
            label: "颍泉区",
            value: "341204"
          }, {
            label: "临泉县",
            value: "341221"
          }, {
            label: "太和县",
            value: "341222"
          }, {
            label: "阜南县",
            value: "341225"
          }, {
            label: "颍上县",
            value: "341226"
          }, {
            label: "阜阳合肥现代产业园区",
            value: "341271"
          }, {
            label: "阜阳经济技术开发区",
            value: "341272"
          }, {
            label: "界首市",
            value: "341282"
          }],
          [{
            label: "埇桥区",
            value: "341302"
          }, {
            label: "砀山县",
            value: "341321"
          }, {
            label: "萧县",
            value: "341322"
          }, {
            label: "灵璧县",
            value: "341323"
          }, {
            label: "泗县",
            value: "341324"
          }, {
            label: "宿州马鞍山现代产业园区",
            value: "341371"
          }, {
            label: "宿州经济技术开发区",
            value: "341372"
          }],
          [{
            label: "金安区",
            value: "341502"
          }, {
            label: "裕安区",
            value: "341503"
          }, {
            label: "叶集区",
            value: "341504"
          }, {
            label: "霍邱县",
            value: "341522"
          }, {
            label: "舒城县",
            value: "341523"
          }, {
            label: "金寨县",
            value: "341524"
          }, {
            label: "霍山县",
            value: "341525"
          }],
          [{
            label: "谯城区",
            value: "341602"
          }, {
            label: "涡阳县",
            value: "341621"
          }, {
            label: "蒙城县",
            value: "341622"
          }, {
            label: "利辛县",
            value: "341623"
          }],
          [{
            label: "贵池区",
            value: "341702"
          }, {
            label: "东至县",
            value: "341721"
          }, {
            label: "石台县",
            value: "341722"
          }, {
            label: "青阳县",
            value: "341723"
          }],
          [{
            label: "宣州区",
            value: "341802"
          }, {
            label: "郎溪县",
            value: "341821"
          }, {
            label: "广德县",
            value: "341822"
          }, {
            label: "泾县",
            value: "341823"
          }, {
            label: "绩溪县",
            value: "341824"
          }, {
            label: "旌德县",
            value: "341825"
          }, {
            label: "宣城市经济开发区",
            value: "341871"
          }, {
            label: "宁国市",
            value: "341881"
          }]
        ],
        [
          [{
            label: "鼓楼区",
            value: "350102"
          }, {
            label: "台江区",
            value: "350103"
          }, {
            label: "仓山区",
            value: "350104"
          }, {
            label: "马尾区",
            value: "350105"
          }, {
            label: "晋安区",
            value: "350111"
          }, {
            label: "闽侯县",
            value: "350121"
          }, {
            label: "连江县",
            value: "350122"
          }, {
            label: "罗源县",
            value: "350123"
          }, {
            label: "闽清县",
            value: "350124"
          }, {
            label: "永泰县",
            value: "350125"
          }, {
            label: "平潭县",
            value: "350128"
          }, {
            label: "福清市",
            value: "350181"
          }, {
            label: "长乐市",
            value: "350182"
          }],
          [{
            label: "思明区",
            value: "350203"
          }, {
            label: "海沧区",
            value: "350205"
          }, {
            label: "湖里区",
            value: "350206"
          }, {
            label: "集美区",
            value: "350211"
          }, {
            label: "同安区",
            value: "350212"
          }, {
            label: "翔安区",
            value: "350213"
          }],
          [{
            label: "城厢区",
            value: "350302"
          }, {
            label: "涵江区",
            value: "350303"
          }, {
            label: "荔城区",
            value: "350304"
          }, {
            label: "秀屿区",
            value: "350305"
          }, {
            label: "仙游县",
            value: "350322"
          }],
          [{
            label: "梅列区",
            value: "350402"
          }, {
            label: "三元区",
            value: "350403"
          }, {
            label: "明溪县",
            value: "350421"
          }, {
            label: "清流县",
            value: "350423"
          }, {
            label: "宁化县",
            value: "350424"
          }, {
            label: "大田县",
            value: "350425"
          }, {
            label: "尤溪县",
            value: "350426"
          }, {
            label: "沙县",
            value: "350427"
          }, {
            label: "将乐县",
            value: "350428"
          }, {
            label: "泰宁县",
            value: "350429"
          }, {
            label: "建宁县",
            value: "350430"
          }, {
            label: "永安市",
            value: "350481"
          }],
          [{
            label: "鲤城区",
            value: "350502"
          }, {
            label: "丰泽区",
            value: "350503"
          }, {
            label: "洛江区",
            value: "350504"
          }, {
            label: "泉港区",
            value: "350505"
          }, {
            label: "惠安县",
            value: "350521"
          }, {
            label: "安溪县",
            value: "350524"
          }, {
            label: "永春县",
            value: "350525"
          }, {
            label: "德化县",
            value: "350526"
          }, {
            label: "金门县",
            value: "350527"
          }, {
            label: "石狮市",
            value: "350581"
          }, {
            label: "晋江市",
            value: "350582"
          }, {
            label: "南安市",
            value: "350583"
          }],
          [{
            label: "芗城区",
            value: "350602"
          }, {
            label: "龙文区",
            value: "350603"
          }, {
            label: "云霄县",
            value: "350622"
          }, {
            label: "漳浦县",
            value: "350623"
          }, {
            label: "诏安县",
            value: "350624"
          }, {
            label: "长泰县",
            value: "350625"
          }, {
            label: "东山县",
            value: "350626"
          }, {
            label: "南靖县",
            value: "350627"
          }, {
            label: "平和县",
            value: "350628"
          }, {
            label: "华安县",
            value: "350629"
          }, {
            label: "龙海市",
            value: "350681"
          }],
          [{
            label: "延平区",
            value: "350702"
          }, {
            label: "建阳区",
            value: "350703"
          }, {
            label: "顺昌县",
            value: "350721"
          }, {
            label: "浦城县",
            value: "350722"
          }, {
            label: "光泽县",
            value: "350723"
          }, {
            label: "松溪县",
            value: "350724"
          }, {
            label: "政和县",
            value: "350725"
          }, {
            label: "邵武市",
            value: "350781"
          }, {
            label: "武夷山市",
            value: "350782"
          }, {
            label: "建瓯市",
            value: "350783"
          }],
          [{
            label: "新罗区",
            value: "350802"
          }, {
            label: "永定区",
            value: "350803"
          }, {
            label: "长汀县",
            value: "350821"
          }, {
            label: "上杭县",
            value: "350823"
          }, {
            label: "武平县",
            value: "350824"
          }, {
            label: "连城县",
            value: "350825"
          }, {
            label: "漳平市",
            value: "350881"
          }],
          [{
            label: "蕉城区",
            value: "350902"
          }, {
            label: "霞浦县",
            value: "350921"
          }, {
            label: "古田县",
            value: "350922"
          }, {
            label: "屏南县",
            value: "350923"
          }, {
            label: "寿宁县",
            value: "350924"
          }, {
            label: "周宁县",
            value: "350925"
          }, {
            label: "柘荣县",
            value: "350926"
          }, {
            label: "福安市",
            value: "350981"
          }, {
            label: "福鼎市",
            value: "350982"
          }]
        ],
        [
          [{
            label: "东湖区",
            value: "360102"
          }, {
            label: "西湖区",
            value: "360103"
          }, {
            label: "青云谱区",
            value: "360104"
          }, {
            label: "湾里区",
            value: "360105"
          }, {
            label: "青山湖区",
            value: "360111"
          }, {
            label: "新建区",
            value: "360112"
          }, {
            label: "南昌县",
            value: "360121"
          }, {
            label: "安义县",
            value: "360123"
          }, {
            label: "进贤县",
            value: "360124"
          }],
          [{
            label: "昌江区",
            value: "360202"
          }, {
            label: "珠山区",
            value: "360203"
          }, {
            label: "浮梁县",
            value: "360222"
          }, {
            label: "乐平市",
            value: "360281"
          }],
          [{
            label: "安源区",
            value: "360302"
          }, {
            label: "湘东区",
            value: "360313"
          }, {
            label: "莲花县",
            value: "360321"
          }, {
            label: "上栗县",
            value: "360322"
          }, {
            label: "芦溪县",
            value: "360323"
          }],
          [{
            label: "濂溪区",
            value: "360402"
          }, {
            label: "浔阳区",
            value: "360403"
          }, {
            label: "柴桑区",
            value: "360404"
          }, {
            label: "武宁县",
            value: "360423"
          }, {
            label: "修水县",
            value: "360424"
          }, {
            label: "永修县",
            value: "360425"
          }, {
            label: "德安县",
            value: "360426"
          }, {
            label: "都昌县",
            value: "360428"
          }, {
            label: "湖口县",
            value: "360429"
          }, {
            label: "彭泽县",
            value: "360430"
          }, {
            label: "瑞昌市",
            value: "360481"
          }, {
            label: "共青城市",
            value: "360482"
          }, {
            label: "庐山市",
            value: "360483"
          }],
          [{
            label: "渝水区",
            value: "360502"
          }, {
            label: "分宜县",
            value: "360521"
          }],
          [{
            label: "月湖区",
            value: "360602"
          }, {
            label: "余江县",
            value: "360622"
          }, {
            label: "贵溪市",
            value: "360681"
          }],
          [{
            label: "章贡区",
            value: "360702"
          }, {
            label: "南康区",
            value: "360703"
          }, {
            label: "赣县区",
            value: "360704"
          }, {
            label: "信丰县",
            value: "360722"
          }, {
            label: "大余县",
            value: "360723"
          }, {
            label: "上犹县",
            value: "360724"
          }, {
            label: "崇义县",
            value: "360725"
          }, {
            label: "安远县",
            value: "360726"
          }, {
            label: "龙南县",
            value: "360727"
          }, {
            label: "定南县",
            value: "360728"
          }, {
            label: "全南县",
            value: "360729"
          }, {
            label: "宁都县",
            value: "360730"
          }, {
            label: "于都县",
            value: "360731"
          }, {
            label: "兴国县",
            value: "360732"
          }, {
            label: "会昌县",
            value: "360733"
          }, {
            label: "寻乌县",
            value: "360734"
          }, {
            label: "石城县",
            value: "360735"
          }, {
            label: "瑞金市",
            value: "360781"
          }],
          [{
            label: "吉州区",
            value: "360802"
          }, {
            label: "青原区",
            value: "360803"
          }, {
            label: "吉安县",
            value: "360821"
          }, {
            label: "吉水县",
            value: "360822"
          }, {
            label: "峡江县",
            value: "360823"
          }, {
            label: "新干县",
            value: "360824"
          }, {
            label: "永丰县",
            value: "360825"
          }, {
            label: "泰和县",
            value: "360826"
          }, {
            label: "遂川县",
            value: "360827"
          }, {
            label: "万安县",
            value: "360828"
          }, {
            label: "安福县",
            value: "360829"
          }, {
            label: "永新县",
            value: "360830"
          }, {
            label: "井冈山市",
            value: "360881"
          }],
          [{
            label: "袁州区",
            value: "360902"
          }, {
            label: "奉新县",
            value: "360921"
          }, {
            label: "万载县",
            value: "360922"
          }, {
            label: "上高县",
            value: "360923"
          }, {
            label: "宜丰县",
            value: "360924"
          }, {
            label: "靖安县",
            value: "360925"
          }, {
            label: "铜鼓县",
            value: "360926"
          }, {
            label: "丰城市",
            value: "360981"
          }, {
            label: "樟树市",
            value: "360982"
          }, {
            label: "高安市",
            value: "360983"
          }],
          [{
            label: "临川区",
            value: "361002"
          }, {
            label: "东乡区",
            value: "361003"
          }, {
            label: "南城县",
            value: "361021"
          }, {
            label: "黎川县",
            value: "361022"
          }, {
            label: "南丰县",
            value: "361023"
          }, {
            label: "崇仁县",
            value: "361024"
          }, {
            label: "乐安县",
            value: "361025"
          }, {
            label: "宜黄县",
            value: "361026"
          }, {
            label: "金溪县",
            value: "361027"
          }, {
            label: "资溪县",
            value: "361028"
          }, {
            label: "广昌县",
            value: "361030"
          }],
          [{
            label: "信州区",
            value: "361102"
          }, {
            label: "广丰区",
            value: "361103"
          }, {
            label: "上饶县",
            value: "361121"
          }, {
            label: "玉山县",
            value: "361123"
          }, {
            label: "铅山县",
            value: "361124"
          }, {
            label: "横峰县",
            value: "361125"
          }, {
            label: "弋阳县",
            value: "361126"
          }, {
            label: "余干县",
            value: "361127"
          }, {
            label: "鄱阳县",
            value: "361128"
          }, {
            label: "万年县",
            value: "361129"
          }, {
            label: "婺源县",
            value: "361130"
          }, {
            label: "德兴市",
            value: "361181"
          }]
        ],
        [
          [{
            label: "历下区",
            value: "370102"
          }, {
            label: "市中区",
            value: "370103"
          }, {
            label: "槐荫区",
            value: "370104"
          }, {
            label: "天桥区",
            value: "370105"
          }, {
            label: "历城区",
            value: "370112"
          }, {
            label: "长清区",
            value: "370113"
          }, {
            label: "章丘区",
            value: "370114"
          }, {
            label: "平阴县",
            value: "370124"
          }, {
            label: "济阳县",
            value: "370125"
          }, {
            label: "商河县",
            value: "370126"
          }, {
            label: "济南高新技术产业开发区",
            value: "370171"
          }],
          [{
            label: "市南区",
            value: "370202"
          }, {
            label: "市北区",
            value: "370203"
          }, {
            label: "黄岛区",
            value: "370211"
          }, {
            label: "崂山区",
            value: "370212"
          }, {
            label: "李沧区",
            value: "370213"
          }, {
            label: "城阳区",
            value: "370214"
          }, {
            label: "即墨区",
            value: "370215"
          }, {
            label: "青岛高新技术产业开发区",
            value: "370271"
          }, {
            label: "胶州市",
            value: "370281"
          }, {
            label: "平度市",
            value: "370283"
          }, {
            label: "莱西市",
            value: "370285"
          }],
          [{
            label: "淄川区",
            value: "370302"
          }, {
            label: "张店区",
            value: "370303"
          }, {
            label: "博山区",
            value: "370304"
          }, {
            label: "临淄区",
            value: "370305"
          }, {
            label: "周村区",
            value: "370306"
          }, {
            label: "桓台县",
            value: "370321"
          }, {
            label: "高青县",
            value: "370322"
          }, {
            label: "沂源县",
            value: "370323"
          }],
          [{
            label: "市中区",
            value: "370402"
          }, {
            label: "薛城区",
            value: "370403"
          }, {
            label: "峄城区",
            value: "370404"
          }, {
            label: "台儿庄区",
            value: "370405"
          }, {
            label: "山亭区",
            value: "370406"
          }, {
            label: "滕州市",
            value: "370481"
          }],
          [{
            label: "东营区",
            value: "370502"
          }, {
            label: "河口区",
            value: "370503"
          }, {
            label: "垦利区",
            value: "370505"
          }, {
            label: "利津县",
            value: "370522"
          }, {
            label: "广饶县",
            value: "370523"
          }, {
            label: "东营经济技术开发区",
            value: "370571"
          }, {
            label: "东营港经济开发区",
            value: "370572"
          }],
          [{
            label: "芝罘区",
            value: "370602"
          }, {
            label: "福山区",
            value: "370611"
          }, {
            label: "牟平区",
            value: "370612"
          }, {
            label: "莱山区",
            value: "370613"
          }, {
            label: "长岛县",
            value: "370634"
          }, {
            label: "烟台高新技术产业开发区",
            value: "370671"
          }, {
            label: "烟台经济技术开发区",
            value: "370672"
          }, {
            label: "龙口市",
            value: "370681"
          }, {
            label: "莱阳市",
            value: "370682"
          }, {
            label: "莱州市",
            value: "370683"
          }, {
            label: "蓬莱市",
            value: "370684"
          }, {
            label: "招远市",
            value: "370685"
          }, {
            label: "栖霞市",
            value: "370686"
          }, {
            label: "海阳市",
            value: "370687"
          }],
          [{
            label: "潍城区",
            value: "370702"
          }, {
            label: "寒亭区",
            value: "370703"
          }, {
            label: "坊子区",
            value: "370704"
          }, {
            label: "奎文区",
            value: "370705"
          }, {
            label: "临朐县",
            value: "370724"
          }, {
            label: "昌乐县",
            value: "370725"
          }, {
            label: "潍坊滨海经济技术开发区",
            value: "370772"
          }, {
            label: "青州市",
            value: "370781"
          }, {
            label: "诸城市",
            value: "370782"
          }, {
            label: "寿光市",
            value: "370783"
          }, {
            label: "安丘市",
            value: "370784"
          }, {
            label: "高密市",
            value: "370785"
          }, {
            label: "昌邑市",
            value: "370786"
          }],
          [{
            label: "任城区",
            value: "370811"
          }, {
            label: "兖州区",
            value: "370812"
          }, {
            label: "微山县",
            value: "370826"
          }, {
            label: "鱼台县",
            value: "370827"
          }, {
            label: "金乡县",
            value: "370828"
          }, {
            label: "嘉祥县",
            value: "370829"
          }, {
            label: "汶上县",
            value: "370830"
          }, {
            label: "泗水县",
            value: "370831"
          }, {
            label: "梁山县",
            value: "370832"
          }, {
            label: "济宁高新技术产业开发区",
            value: "370871"
          }, {
            label: "曲阜市",
            value: "370881"
          }, {
            label: "邹城市",
            value: "370883"
          }],
          [{
            label: "泰山区",
            value: "370902"
          }, {
            label: "岱岳区",
            value: "370911"
          }, {
            label: "宁阳县",
            value: "370921"
          }, {
            label: "东平县",
            value: "370923"
          }, {
            label: "新泰市",
            value: "370982"
          }, {
            label: "肥城市",
            value: "370983"
          }],
          [{
            label: "环翠区",
            value: "371002"
          }, {
            label: "文登区",
            value: "371003"
          }, {
            label: "威海火炬高技术产业开发区",
            value: "371071"
          }, {
            label: "威海经济技术开发区",
            value: "371072"
          }, {
            label: "威海临港经济技术开发区",
            value: "371073"
          }, {
            label: "荣成市",
            value: "371082"
          }, {
            label: "乳山市",
            value: "371083"
          }],
          [{
            label: "东港区",
            value: "371102"
          }, {
            label: "岚山区",
            value: "371103"
          }, {
            label: "五莲县",
            value: "371121"
          }, {
            label: "莒县",
            value: "371122"
          }, {
            label: "日照经济技术开发区",
            value: "371171"
          }, {
            label: "日照国际海洋城",
            value: "371172"
          }],
          [{
            label: "莱城区",
            value: "371202"
          }, {
            label: "钢城区",
            value: "371203"
          }],
          [{
            label: "兰山区",
            value: "371302"
          }, {
            label: "罗庄区",
            value: "371311"
          }, {
            label: "河东区",
            value: "371312"
          }, {
            label: "沂南县",
            value: "371321"
          }, {
            label: "郯城县",
            value: "371322"
          }, {
            label: "沂水县",
            value: "371323"
          }, {
            label: "兰陵县",
            value: "371324"
          }, {
            label: "费县",
            value: "371325"
          }, {
            label: "平邑县",
            value: "371326"
          }, {
            label: "莒南县",
            value: "371327"
          }, {
            label: "蒙阴县",
            value: "371328"
          }, {
            label: "临沭县",
            value: "371329"
          }, {
            label: "临沂高新技术产业开发区",
            value: "371371"
          }, {
            label: "临沂经济技术开发区",
            value: "371372"
          }, {
            label: "临沂临港经济开发区",
            value: "371373"
          }],
          [{
            label: "德城区",
            value: "371402"
          }, {
            label: "陵城区",
            value: "371403"
          }, {
            label: "宁津县",
            value: "371422"
          }, {
            label: "庆云县",
            value: "371423"
          }, {
            label: "临邑县",
            value: "371424"
          }, {
            label: "齐河县",
            value: "371425"
          }, {
            label: "平原县",
            value: "371426"
          }, {
            label: "夏津县",
            value: "371427"
          }, {
            label: "武城县",
            value: "371428"
          }, {
            label: "德州经济技术开发区",
            value: "371471"
          }, {
            label: "德州运河经济开发区",
            value: "371472"
          }, {
            label: "乐陵市",
            value: "371481"
          }, {
            label: "禹城市",
            value: "371482"
          }],
          [{
            label: "东昌府区",
            value: "371502"
          }, {
            label: "阳谷县",
            value: "371521"
          }, {
            label: "莘县",
            value: "371522"
          }, {
            label: "茌平县",
            value: "371523"
          }, {
            label: "东阿县",
            value: "371524"
          }, {
            label: "冠县",
            value: "371525"
          }, {
            label: "高唐县",
            value: "371526"
          }, {
            label: "临清市",
            value: "371581"
          }],
          [{
            label: "滨城区",
            value: "371602"
          }, {
            label: "沾化区",
            value: "371603"
          }, {
            label: "惠民县",
            value: "371621"
          }, {
            label: "阳信县",
            value: "371622"
          }, {
            label: "无棣县",
            value: "371623"
          }, {
            label: "博兴县",
            value: "371625"
          }, {
            label: "邹平县",
            value: "371626"
          }],
          [{
            label: "牡丹区",
            value: "371702"
          }, {
            label: "定陶区",
            value: "371703"
          }, {
            label: "曹县",
            value: "371721"
          }, {
            label: "单县",
            value: "371722"
          }, {
            label: "成武县",
            value: "371723"
          }, {
            label: "巨野县",
            value: "371724"
          }, {
            label: "郓城县",
            value: "371725"
          }, {
            label: "鄄城县",
            value: "371726"
          }, {
            label: "东明县",
            value: "371728"
          }, {
            label: "菏泽经济技术开发区",
            value: "371771"
          }, {
            label: "菏泽高新技术开发区",
            value: "371772"
          }]
        ],
        [
          [{
            label: "中原区",
            value: "410102"
          }, {
            label: "二七区",
            value: "410103"
          }, {
            label: "管城回族区",
            value: "410104"
          }, {
            label: "金水区",
            value: "410105"
          }, {
            label: "上街区",
            value: "410106"
          }, {
            label: "惠济区",
            value: "410108"
          }, {
            label: "中牟县",
            value: "410122"
          }, {
            label: "郑州经济技术开发区",
            value: "410171"
          }, {
            label: "郑州高新技术产业开发区",
            value: "410172"
          }, {
            label: "郑州航空港经济综合实验区",
            value: "410173"
          }, {
            label: "巩义市",
            value: "410181"
          }, {
            label: "荥阳市",
            value: "410182"
          }, {
            label: "新密市",
            value: "410183"
          }, {
            label: "新郑市",
            value: "410184"
          }, {
            label: "登封市",
            value: "410185"
          }],
          [{
            label: "龙亭区",
            value: "410202"
          }, {
            label: "顺河回族区",
            value: "410203"
          }, {
            label: "鼓楼区",
            value: "410204"
          }, {
            label: "禹王台区",
            value: "410205"
          }, {
            label: "祥符区",
            value: "410212"
          }, {
            label: "杞县",
            value: "410221"
          }, {
            label: "通许县",
            value: "410222"
          }, {
            label: "尉氏县",
            value: "410223"
          }, {
            label: "兰考县",
            value: "410225"
          }],
          [{
            label: "老城区",
            value: "410302"
          }, {
            label: "西工区",
            value: "410303"
          }, {
            label: "瀍河回族区",
            value: "410304"
          }, {
            label: "涧西区",
            value: "410305"
          }, {
            label: "吉利区",
            value: "410306"
          }, {
            label: "洛龙区",
            value: "410311"
          }, {
            label: "孟津县",
            value: "410322"
          }, {
            label: "新安县",
            value: "410323"
          }, {
            label: "栾川县",
            value: "410324"
          }, {
            label: "嵩县",
            value: "410325"
          }, {
            label: "汝阳县",
            value: "410326"
          }, {
            label: "宜阳县",
            value: "410327"
          }, {
            label: "洛宁县",
            value: "410328"
          }, {
            label: "伊川县",
            value: "410329"
          }, {
            label: "洛阳高新技术产业开发区",
            value: "410371"
          }, {
            label: "偃师市",
            value: "410381"
          }],
          [{
            label: "新华区",
            value: "410402"
          }, {
            label: "卫东区",
            value: "410403"
          }, {
            label: "石龙区",
            value: "410404"
          }, {
            label: "湛河区",
            value: "410411"
          }, {
            label: "宝丰县",
            value: "410421"
          }, {
            label: "叶县",
            value: "410422"
          }, {
            label: "鲁山县",
            value: "410423"
          }, {
            label: "郏县",
            value: "410425"
          }, {
            label: "平顶山高新技术产业开发区",
            value: "410471"
          }, {
            label: "平顶山市新城区",
            value: "410472"
          }, {
            label: "舞钢市",
            value: "410481"
          }, {
            label: "汝州市",
            value: "410482"
          }],
          [{
            label: "文峰区",
            value: "410502"
          }, {
            label: "北关区",
            value: "410503"
          }, {
            label: "殷都区",
            value: "410505"
          }, {
            label: "龙安区",
            value: "410506"
          }, {
            label: "安阳县",
            value: "410522"
          }, {
            label: "汤阴县",
            value: "410523"
          }, {
            label: "滑县",
            value: "410526"
          }, {
            label: "内黄县",
            value: "410527"
          }, {
            label: "安阳高新技术产业开发区",
            value: "410571"
          }, {
            label: "林州市",
            value: "410581"
          }],
          [{
            label: "鹤山区",
            value: "410602"
          }, {
            label: "山城区",
            value: "410603"
          }, {
            label: "淇滨区",
            value: "410611"
          }, {
            label: "浚县",
            value: "410621"
          }, {
            label: "淇县",
            value: "410622"
          }, {
            label: "鹤壁经济技术开发区",
            value: "410671"
          }],
          [{
            label: "红旗区",
            value: "410702"
          }, {
            label: "卫滨区",
            value: "410703"
          }, {
            label: "凤泉区",
            value: "410704"
          }, {
            label: "牧野区",
            value: "410711"
          }, {
            label: "新乡县",
            value: "410721"
          }, {
            label: "获嘉县",
            value: "410724"
          }, {
            label: "原阳县",
            value: "410725"
          }, {
            label: "延津县",
            value: "410726"
          }, {
            label: "封丘县",
            value: "410727"
          }, {
            label: "长垣县",
            value: "410728"
          }, {
            label: "新乡高新技术产业开发区",
            value: "410771"
          }, {
            label: "新乡经济技术开发区",
            value: "410772"
          }, {
            label: "新乡市平原城乡一体化示范区",
            value: "410773"
          }, {
            label: "卫辉市",
            value: "410781"
          }, {
            label: "辉县市",
            value: "410782"
          }],
          [{
            label: "解放区",
            value: "410802"
          }, {
            label: "中站区",
            value: "410803"
          }, {
            label: "马村区",
            value: "410804"
          }, {
            label: "山阳区",
            value: "410811"
          }, {
            label: "修武县",
            value: "410821"
          }, {
            label: "博爱县",
            value: "410822"
          }, {
            label: "武陟县",
            value: "410823"
          }, {
            label: "温县",
            value: "410825"
          }, {
            label: "焦作城乡一体化示范区",
            value: "410871"
          }, {
            label: "沁阳市",
            value: "410882"
          }, {
            label: "孟州市",
            value: "410883"
          }],
          [{
            label: "华龙区",
            value: "410902"
          }, {
            label: "清丰县",
            value: "410922"
          }, {
            label: "南乐县",
            value: "410923"
          }, {
            label: "范县",
            value: "410926"
          }, {
            label: "台前县",
            value: "410927"
          }, {
            label: "濮阳县",
            value: "410928"
          }, {
            label: "河南濮阳工业园区",
            value: "410971"
          }, {
            label: "濮阳经济技术开发区",
            value: "410972"
          }],
          [{
            label: "魏都区",
            value: "411002"
          }, {
            label: "建安区",
            value: "411003"
          }, {
            label: "鄢陵县",
            value: "411024"
          }, {
            label: "襄城县",
            value: "411025"
          }, {
            label: "许昌经济技术开发区",
            value: "411071"
          }, {
            label: "禹州市",
            value: "411081"
          }, {
            label: "长葛市",
            value: "411082"
          }],
          [{
            label: "源汇区",
            value: "411102"
          }, {
            label: "郾城区",
            value: "411103"
          }, {
            label: "召陵区",
            value: "411104"
          }, {
            label: "舞阳县",
            value: "411121"
          }, {
            label: "临颍县",
            value: "411122"
          }, {
            label: "漯河经济技术开发区",
            value: "411171"
          }],
          [{
            label: "湖滨区",
            value: "411202"
          }, {
            label: "陕州区",
            value: "411203"
          }, {
            label: "渑池县",
            value: "411221"
          }, {
            label: "卢氏县",
            value: "411224"
          }, {
            label: "河南三门峡经济开发区",
            value: "411271"
          }, {
            label: "义马市",
            value: "411281"
          }, {
            label: "灵宝市",
            value: "411282"
          }],
          [{
            label: "宛城区",
            value: "411302"
          }, {
            label: "卧龙区",
            value: "411303"
          }, {
            label: "南召县",
            value: "411321"
          }, {
            label: "方城县",
            value: "411322"
          }, {
            label: "西峡县",
            value: "411323"
          }, {
            label: "镇平县",
            value: "411324"
          }, {
            label: "内乡县",
            value: "411325"
          }, {
            label: "淅川县",
            value: "411326"
          }, {
            label: "社旗县",
            value: "411327"
          }, {
            label: "唐河县",
            value: "411328"
          }, {
            label: "新野县",
            value: "411329"
          }, {
            label: "桐柏县",
            value: "411330"
          }, {
            label: "南阳高新技术产业开发区",
            value: "411371"
          }, {
            label: "南阳市城乡一体化示范区",
            value: "411372"
          }, {
            label: "邓州市",
            value: "411381"
          }],
          [{
            label: "梁园区",
            value: "411402"
          }, {
            label: "睢阳区",
            value: "411403"
          }, {
            label: "民权县",
            value: "411421"
          }, {
            label: "睢县",
            value: "411422"
          }, {
            label: "宁陵县",
            value: "411423"
          }, {
            label: "柘城县",
            value: "411424"
          }, {
            label: "虞城县",
            value: "411425"
          }, {
            label: "夏邑县",
            value: "411426"
          }, {
            label: "豫东综合物流产业聚集区",
            value: "411471"
          }, {
            label: "河南商丘经济开发区",
            value: "411472"
          }, {
            label: "永城市",
            value: "411481"
          }],
          [{
            label: "浉河区",
            value: "411502"
          }, {
            label: "平桥区",
            value: "411503"
          }, {
            label: "罗山县",
            value: "411521"
          }, {
            label: "光山县",
            value: "411522"
          }, {
            label: "新县",
            value: "411523"
          }, {
            label: "商城县",
            value: "411524"
          }, {
            label: "固始县",
            value: "411525"
          }, {
            label: "潢川县",
            value: "411526"
          }, {
            label: "淮滨县",
            value: "411527"
          }, {
            label: "息县",
            value: "411528"
          }, {
            label: "信阳高新技术产业开发区",
            value: "411571"
          }],
          [{
            label: "川汇区",
            value: "411602"
          }, {
            label: "扶沟县",
            value: "411621"
          }, {
            label: "西华县",
            value: "411622"
          }, {
            label: "商水县",
            value: "411623"
          }, {
            label: "沈丘县",
            value: "411624"
          }, {
            label: "郸城县",
            value: "411625"
          }, {
            label: "淮阳县",
            value: "411626"
          }, {
            label: "太康县",
            value: "411627"
          }, {
            label: "鹿邑县",
            value: "411628"
          }, {
            label: "河南周口经济开发区",
            value: "411671"
          }, {
            label: "项城市",
            value: "411681"
          }],
          [{
            label: "驿城区",
            value: "411702"
          }, {
            label: "西平县",
            value: "411721"
          }, {
            label: "上蔡县",
            value: "411722"
          }, {
            label: "平舆县",
            value: "411723"
          }, {
            label: "正阳县",
            value: "411724"
          }, {
            label: "确山县",
            value: "411725"
          }, {
            label: "泌阳县",
            value: "411726"
          }, {
            label: "汝南县",
            value: "411727"
          }, {
            label: "遂平县",
            value: "411728"
          }, {
            label: "新蔡县",
            value: "411729"
          }, {
            label: "河南驻马店经济开发区",
            value: "411771"
          }],
          [{
            label: "济源市",
            value: "419001"
          }]
        ],
        [
          [{
            label: "江岸区",
            value: "420102"
          }, {
            label: "江汉区",
            value: "420103"
          }, {
            label: "硚口区",
            value: "420104"
          }, {
            label: "汉阳区",
            value: "420105"
          }, {
            label: "武昌区",
            value: "420106"
          }, {
            label: "青山区",
            value: "420107"
          }, {
            label: "洪山区",
            value: "420111"
          }, {
            label: "东西湖区",
            value: "420112"
          }, {
            label: "汉南区",
            value: "420113"
          }, {
            label: "蔡甸区",
            value: "420114"
          }, {
            label: "江夏区",
            value: "420115"
          }, {
            label: "黄陂区",
            value: "420116"
          }, {
            label: "新洲区",
            value: "420117"
          }],
          [{
            label: "黄石港区",
            value: "420202"
          }, {
            label: "西塞山区",
            value: "420203"
          }, {
            label: "下陆区",
            value: "420204"
          }, {
            label: "铁山区",
            value: "420205"
          }, {
            label: "阳新县",
            value: "420222"
          }, {
            label: "大冶市",
            value: "420281"
          }],
          [{
            label: "茅箭区",
            value: "420302"
          }, {
            label: "张湾区",
            value: "420303"
          }, {
            label: "郧阳区",
            value: "420304"
          }, {
            label: "郧西县",
            value: "420322"
          }, {
            label: "竹山县",
            value: "420323"
          }, {
            label: "竹溪县",
            value: "420324"
          }, {
            label: "房县",
            value: "420325"
          }, {
            label: "丹江口市",
            value: "420381"
          }],
          [{
            label: "西陵区",
            value: "420502"
          }, {
            label: "伍家岗区",
            value: "420503"
          }, {
            label: "点军区",
            value: "420504"
          }, {
            label: "猇亭区",
            value: "420505"
          }, {
            label: "夷陵区",
            value: "420506"
          }, {
            label: "远安县",
            value: "420525"
          }, {
            label: "兴山县",
            value: "420526"
          }, {
            label: "秭归县",
            value: "420527"
          }, {
            label: "长阳土家族自治县",
            value: "420528"
          }, {
            label: "五峰土家族自治县",
            value: "420529"
          }, {
            label: "宜都市",
            value: "420581"
          }, {
            label: "当阳市",
            value: "420582"
          }, {
            label: "枝江市",
            value: "420583"
          }],
          [{
            label: "襄城区",
            value: "420602"
          }, {
            label: "樊城区",
            value: "420606"
          }, {
            label: "襄州区",
            value: "420607"
          }, {
            label: "南漳县",
            value: "420624"
          }, {
            label: "谷城县",
            value: "420625"
          }, {
            label: "保康县",
            value: "420626"
          }, {
            label: "老河口市",
            value: "420682"
          }, {
            label: "枣阳市",
            value: "420683"
          }, {
            label: "宜城市",
            value: "420684"
          }],
          [{
            label: "梁子湖区",
            value: "420702"
          }, {
            label: "华容区",
            value: "420703"
          }, {
            label: "鄂城区",
            value: "420704"
          }],
          [{
            label: "东宝区",
            value: "420802"
          }, {
            label: "掇刀区",
            value: "420804"
          }, {
            label: "京山县",
            value: "420821"
          }, {
            label: "沙洋县",
            value: "420822"
          }, {
            label: "钟祥市",
            value: "420881"
          }],
          [{
            label: "孝南区",
            value: "420902"
          }, {
            label: "孝昌县",
            value: "420921"
          }, {
            label: "大悟县",
            value: "420922"
          }, {
            label: "云梦县",
            value: "420923"
          }, {
            label: "应城市",
            value: "420981"
          }, {
            label: "安陆市",
            value: "420982"
          }, {
            label: "汉川市",
            value: "420984"
          }],
          [{
            label: "沙市区",
            value: "421002"
          }, {
            label: "荆州区",
            value: "421003"
          }, {
            label: "公安县",
            value: "421022"
          }, {
            label: "监利县",
            value: "421023"
          }, {
            label: "江陵县",
            value: "421024"
          }, {
            label: "荆州经济技术开发区",
            value: "421071"
          }, {
            label: "石首市",
            value: "421081"
          }, {
            label: "洪湖市",
            value: "421083"
          }, {
            label: "松滋市",
            value: "421087"
          }],
          [{
            label: "黄州区",
            value: "421102"
          }, {
            label: "团风县",
            value: "421121"
          }, {
            label: "红安县",
            value: "421122"
          }, {
            label: "罗田县",
            value: "421123"
          }, {
            label: "英山县",
            value: "421124"
          }, {
            label: "浠水县",
            value: "421125"
          }, {
            label: "蕲春县",
            value: "421126"
          }, {
            label: "黄梅县",
            value: "421127"
          }, {
            label: "龙感湖管理区",
            value: "421171"
          }, {
            label: "麻城市",
            value: "421181"
          }, {
            label: "武穴市",
            value: "421182"
          }],
          [{
            label: "咸安区",
            value: "421202"
          }, {
            label: "嘉鱼县",
            value: "421221"
          }, {
            label: "通城县",
            value: "421222"
          }, {
            label: "崇阳县",
            value: "421223"
          }, {
            label: "通山县",
            value: "421224"
          }, {
            label: "赤壁市",
            value: "421281"
          }],
          [{
            label: "曾都区",
            value: "421303"
          }, {
            label: "随县",
            value: "421321"
          }, {
            label: "广水市",
            value: "421381"
          }],
          [{
            label: "恩施市",
            value: "422801"
          }, {
            label: "利川市",
            value: "422802"
          }, {
            label: "建始县",
            value: "422822"
          }, {
            label: "巴东县",
            value: "422823"
          }, {
            label: "宣恩县",
            value: "422825"
          }, {
            label: "咸丰县",
            value: "422826"
          }, {
            label: "来凤县",
            value: "422827"
          }, {
            label: "鹤峰县",
            value: "422828"
          }],
          [{
            label: "仙桃市",
            value: "429004"
          }, {
            label: "潜江市",
            value: "429005"
          }, {
            label: "天门市",
            value: "429006"
          }, {
            label: "神农架林区",
            value: "429021"
          }]
        ],
        [
          [{
            label: "芙蓉区",
            value: "430102"
          }, {
            label: "天心区",
            value: "430103"
          }, {
            label: "岳麓区",
            value: "430104"
          }, {
            label: "开福区",
            value: "430105"
          }, {
            label: "雨花区",
            value: "430111"
          }, {
            label: "望城区",
            value: "430112"
          }, {
            label: "长沙县",
            value: "430121"
          }, {
            label: "浏阳市",
            value: "430181"
          }, {
            label: "宁乡市",
            value: "430182"
          }],
          [{
            label: "荷塘区",
            value: "430202"
          }, {
            label: "芦淞区",
            value: "430203"
          }, {
            label: "石峰区",
            value: "430204"
          }, {
            label: "天元区",
            value: "430211"
          }, {
            label: "株洲县",
            value: "430221"
          }, {
            label: "攸县",
            value: "430223"
          }, {
            label: "茶陵县",
            value: "430224"
          }, {
            label: "炎陵县",
            value: "430225"
          }, {
            label: "云龙示范区",
            value: "430271"
          }, {
            label: "醴陵市",
            value: "430281"
          }],
          [{
            label: "雨湖区",
            value: "430302"
          }, {
            label: "岳塘区",
            value: "430304"
          }, {
            label: "湘潭县",
            value: "430321"
          }, {
            label: "湖南湘潭高新技术产业园区",
            value: "430371"
          }, {
            label: "湘潭昭山示范区",
            value: "430372"
          }, {
            label: "湘潭九华示范区",
            value: "430373"
          }, {
            label: "湘乡市",
            value: "430381"
          }, {
            label: "韶山市",
            value: "430382"
          }],
          [{
            label: "珠晖区",
            value: "430405"
          }, {
            label: "雁峰区",
            value: "430406"
          }, {
            label: "石鼓区",
            value: "430407"
          }, {
            label: "蒸湘区",
            value: "430408"
          }, {
            label: "南岳区",
            value: "430412"
          }, {
            label: "衡阳县",
            value: "430421"
          }, {
            label: "衡南县",
            value: "430422"
          }, {
            label: "衡山县",
            value: "430423"
          }, {
            label: "衡东县",
            value: "430424"
          }, {
            label: "祁东县",
            value: "430426"
          }, {
            label: "衡阳综合保税区",
            value: "430471"
          }, {
            label: "湖南衡阳高新技术产业园区",
            value: "430472"
          }, {
            label: "湖南衡阳松木经济开发区",
            value: "430473"
          }, {
            label: "耒阳市",
            value: "430481"
          }, {
            label: "常宁市",
            value: "430482"
          }],
          [{
            label: "双清区",
            value: "430502"
          }, {
            label: "大祥区",
            value: "430503"
          }, {
            label: "北塔区",
            value: "430511"
          }, {
            label: "邵东县",
            value: "430521"
          }, {
            label: "新邵县",
            value: "430522"
          }, {
            label: "邵阳县",
            value: "430523"
          }, {
            label: "隆回县",
            value: "430524"
          }, {
            label: "洞口县",
            value: "430525"
          }, {
            label: "绥宁县",
            value: "430527"
          }, {
            label: "新宁县",
            value: "430528"
          }, {
            label: "城步苗族自治县",
            value: "430529"
          }, {
            label: "武冈市",
            value: "430581"
          }],
          [{
            label: "岳阳楼区",
            value: "430602"
          }, {
            label: "云溪区",
            value: "430603"
          }, {
            label: "君山区",
            value: "430611"
          }, {
            label: "岳阳县",
            value: "430621"
          }, {
            label: "华容县",
            value: "430623"
          }, {
            label: "湘阴县",
            value: "430624"
          }, {
            label: "平江县",
            value: "430626"
          }, {
            label: "岳阳市屈原管理区",
            value: "430671"
          }, {
            label: "汨罗市",
            value: "430681"
          }, {
            label: "临湘市",
            value: "430682"
          }],
          [{
            label: "武陵区",
            value: "430702"
          }, {
            label: "鼎城区",
            value: "430703"
          }, {
            label: "安乡县",
            value: "430721"
          }, {
            label: "汉寿县",
            value: "430722"
          }, {
            label: "澧县",
            value: "430723"
          }, {
            label: "临澧县",
            value: "430724"
          }, {
            label: "桃源县",
            value: "430725"
          }, {
            label: "石门县",
            value: "430726"
          }, {
            label: "常德市西洞庭管理区",
            value: "430771"
          }, {
            label: "津市市",
            value: "430781"
          }],
          [{
            label: "永定区",
            value: "430802"
          }, {
            label: "武陵源区",
            value: "430811"
          }, {
            label: "慈利县",
            value: "430821"
          }, {
            label: "桑植县",
            value: "430822"
          }],
          [{
            label: "资阳区",
            value: "430902"
          }, {
            label: "赫山区",
            value: "430903"
          }, {
            label: "南县",
            value: "430921"
          }, {
            label: "桃江县",
            value: "430922"
          }, {
            label: "安化县",
            value: "430923"
          }, {
            label: "益阳市大通湖管理区",
            value: "430971"
          }, {
            label: "湖南益阳高新技术产业园区",
            value: "430972"
          }, {
            label: "沅江市",
            value: "430981"
          }],
          [{
            label: "北湖区",
            value: "431002"
          }, {
            label: "苏仙区",
            value: "431003"
          }, {
            label: "桂阳县",
            value: "431021"
          }, {
            label: "宜章县",
            value: "431022"
          }, {
            label: "永兴县",
            value: "431023"
          }, {
            label: "嘉禾县",
            value: "431024"
          }, {
            label: "临武县",
            value: "431025"
          }, {
            label: "汝城县",
            value: "431026"
          }, {
            label: "桂东县",
            value: "431027"
          }, {
            label: "安仁县",
            value: "431028"
          }, {
            label: "资兴市",
            value: "431081"
          }],
          [{
            label: "零陵区",
            value: "431102"
          }, {
            label: "冷水滩区",
            value: "431103"
          }, {
            label: "祁阳县",
            value: "431121"
          }, {
            label: "东安县",
            value: "431122"
          }, {
            label: "双牌县",
            value: "431123"
          }, {
            label: "道县",
            value: "431124"
          }, {
            label: "江永县",
            value: "431125"
          }, {
            label: "宁远县",
            value: "431126"
          }, {
            label: "蓝山县",
            value: "431127"
          }, {
            label: "新田县",
            value: "431128"
          }, {
            label: "江华瑶族自治县",
            value: "431129"
          }, {
            label: "永州经济技术开发区",
            value: "431171"
          }, {
            label: "永州市金洞管理区",
            value: "431172"
          }, {
            label: "永州市回龙圩管理区",
            value: "431173"
          }],
          [{
            label: "鹤城区",
            value: "431202"
          }, {
            label: "中方县",
            value: "431221"
          }, {
            label: "沅陵县",
            value: "431222"
          }, {
            label: "辰溪县",
            value: "431223"
          }, {
            label: "溆浦县",
            value: "431224"
          }, {
            label: "会同县",
            value: "431225"
          }, {
            label: "麻阳苗族自治县",
            value: "431226"
          }, {
            label: "新晃侗族自治县",
            value: "431227"
          }, {
            label: "芷江侗族自治县",
            value: "431228"
          }, {
            label: "靖州苗族侗族自治县",
            value: "431229"
          }, {
            label: "通道侗族自治县",
            value: "431230"
          }, {
            label: "怀化市洪江管理区",
            value: "431271"
          }, {
            label: "洪江市",
            value: "431281"
          }],
          [{
            label: "娄星区",
            value: "431302"
          }, {
            label: "双峰县",
            value: "431321"
          }, {
            label: "新化县",
            value: "431322"
          }, {
            label: "冷水江市",
            value: "431381"
          }, {
            label: "涟源市",
            value: "431382"
          }],
          [{
            label: "吉首市",
            value: "433101"
          }, {
            label: "泸溪县",
            value: "433122"
          }, {
            label: "凤凰县",
            value: "433123"
          }, {
            label: "花垣县",
            value: "433124"
          }, {
            label: "保靖县",
            value: "433125"
          }, {
            label: "古丈县",
            value: "433126"
          }, {
            label: "永顺县",
            value: "433127"
          }, {
            label: "龙山县",
            value: "433130"
          }, {
            label: "湖南吉首经济开发区",
            value: "433172"
          }, {
            label: "湖南永顺经济开发区",
            value: "433173"
          }]
        ],
        [
          [{
            label: "荔湾区",
            value: "440103"
          }, {
            label: "越秀区",
            value: "440104"
          }, {
            label: "海珠区",
            value: "440105"
          }, {
            label: "天河区",
            value: "440106"
          }, {
            label: "白云区",
            value: "440111"
          }, {
            label: "黄埔区",
            value: "440112"
          }, {
            label: "番禺区",
            value: "440113"
          }, {
            label: "花都区",
            value: "440114"
          }, {
            label: "南沙区",
            value: "440115"
          }, {
            label: "从化区",
            value: "440117"
          }, {
            label: "增城区",
            value: "440118"
          }],
          [{
            label: "武江区",
            value: "440203"
          }, {
            label: "浈江区",
            value: "440204"
          }, {
            label: "曲江区",
            value: "440205"
          }, {
            label: "始兴县",
            value: "440222"
          }, {
            label: "仁化县",
            value: "440224"
          }, {
            label: "翁源县",
            value: "440229"
          }, {
            label: "乳源瑶族自治县",
            value: "440232"
          }, {
            label: "新丰县",
            value: "440233"
          }, {
            label: "乐昌市",
            value: "440281"
          }, {
            label: "南雄市",
            value: "440282"
          }],
          [{
            label: "罗湖区",
            value: "440303"
          }, {
            label: "福田区",
            value: "440304"
          }, {
            label: "南山区",
            value: "440305"
          }, {
            label: "宝安区",
            value: "440306"
          }, {
            label: "龙岗区",
            value: "440307"
          }, {
            label: "盐田区",
            value: "440308"
          }, {
            label: "龙华区",
            value: "440309"
          }, {
            label: "坪山区",
            value: "440310"
          }],
          [{
            label: "香洲区",
            value: "440402"
          }, {
            label: "斗门区",
            value: "440403"
          }, {
            label: "金湾区",
            value: "440404"
          }],
          [{
            label: "龙湖区",
            value: "440507"
          }, {
            label: "金平区",
            value: "440511"
          }, {
            label: "濠江区",
            value: "440512"
          }, {
            label: "潮阳区",
            value: "440513"
          }, {
            label: "潮南区",
            value: "440514"
          }, {
            label: "澄海区",
            value: "440515"
          }, {
            label: "南澳县",
            value: "440523"
          }],
          [{
            label: "禅城区",
            value: "440604"
          }, {
            label: "南海区",
            value: "440605"
          }, {
            label: "顺德区",
            value: "440606"
          }, {
            label: "三水区",
            value: "440607"
          }, {
            label: "高明区",
            value: "440608"
          }],
          [{
            label: "蓬江区",
            value: "440703"
          }, {
            label: "江海区",
            value: "440704"
          }, {
            label: "新会区",
            value: "440705"
          }, {
            label: "台山市",
            value: "440781"
          }, {
            label: "开平市",
            value: "440783"
          }, {
            label: "鹤山市",
            value: "440784"
          }, {
            label: "恩平市",
            value: "440785"
          }],
          [{
            label: "赤坎区",
            value: "440802"
          }, {
            label: "霞山区",
            value: "440803"
          }, {
            label: "坡头区",
            value: "440804"
          }, {
            label: "麻章区",
            value: "440811"
          }, {
            label: "遂溪县",
            value: "440823"
          }, {
            label: "徐闻县",
            value: "440825"
          }, {
            label: "廉江市",
            value: "440881"
          }, {
            label: "雷州市",
            value: "440882"
          }, {
            label: "吴川市",
            value: "440883"
          }],
          [{
            label: "茂南区",
            value: "440902"
          }, {
            label: "电白区",
            value: "440904"
          }, {
            label: "高州市",
            value: "440981"
          }, {
            label: "化州市",
            value: "440982"
          }, {
            label: "信宜市",
            value: "440983"
          }],
          [{
            label: "端州区",
            value: "441202"
          }, {
            label: "鼎湖区",
            value: "441203"
          }, {
            label: "高要区",
            value: "441204"
          }, {
            label: "广宁县",
            value: "441223"
          }, {
            label: "怀集县",
            value: "441224"
          }, {
            label: "封开县",
            value: "441225"
          }, {
            label: "德庆县",
            value: "441226"
          }, {
            label: "四会市",
            value: "441284"
          }],
          [{
            label: "惠城区",
            value: "441302"
          }, {
            label: "惠阳区",
            value: "441303"
          }, {
            label: "博罗县",
            value: "441322"
          }, {
            label: "惠东县",
            value: "441323"
          }, {
            label: "龙门县",
            value: "441324"
          }],
          [{
            label: "梅江区",
            value: "441402"
          }, {
            label: "梅县区",
            value: "441403"
          }, {
            label: "大埔县",
            value: "441422"
          }, {
            label: "丰顺县",
            value: "441423"
          }, {
            label: "五华县",
            value: "441424"
          }, {
            label: "平远县",
            value: "441426"
          }, {
            label: "蕉岭县",
            value: "441427"
          }, {
            label: "兴宁市",
            value: "441481"
          }],
          [{
            label: "城区",
            value: "441502"
          }, {
            label: "海丰县",
            value: "441521"
          }, {
            label: "陆河县",
            value: "441523"
          }, {
            label: "陆丰市",
            value: "441581"
          }],
          [{
            label: "源城区",
            value: "441602"
          }, {
            label: "紫金县",
            value: "441621"
          }, {
            label: "龙川县",
            value: "441622"
          }, {
            label: "连平县",
            value: "441623"
          }, {
            label: "和平县",
            value: "441624"
          }, {
            label: "东源县",
            value: "441625"
          }],
          [{
            label: "江城区",
            value: "441702"
          }, {
            label: "阳东区",
            value: "441704"
          }, {
            label: "阳西县",
            value: "441721"
          }, {
            label: "阳春市",
            value: "441781"
          }],
          [{
            label: "清城区",
            value: "441802"
          }, {
            label: "清新区",
            value: "441803"
          }, {
            label: "佛冈县",
            value: "441821"
          }, {
            label: "阳山县",
            value: "441823"
          }, {
            label: "连山壮族瑶族自治县",
            value: "441825"
          }, {
            label: "连南瑶族自治县",
            value: "441826"
          }, {
            label: "英德市",
            value: "441881"
          }, {
            label: "连州市",
            value: "441882"
          }],
          [{
            label: "东莞市",
            value: "441900"
          }],
          [{
            label: "中山市",
            value: "442000"
          }],
          [{
            label: "湘桥区",
            value: "445102"
          }, {
            label: "潮安区",
            value: "445103"
          }, {
            label: "饶平县",
            value: "445122"
          }],
          [{
            label: "榕城区",
            value: "445202"
          }, {
            label: "揭东区",
            value: "445203"
          }, {
            label: "揭西县",
            value: "445222"
          }, {
            label: "惠来县",
            value: "445224"
          }, {
            label: "普宁市",
            value: "445281"
          }],
          [{
            label: "云城区",
            value: "445302"
          }, {
            label: "云安区",
            value: "445303"
          }, {
            label: "新兴县",
            value: "445321"
          }, {
            label: "郁南县",
            value: "445322"
          }, {
            label: "罗定市",
            value: "445381"
          }]
        ],
        [
          [{
            label: "兴宁区",
            value: "450102"
          }, {
            label: "青秀区",
            value: "450103"
          }, {
            label: "江南区",
            value: "450105"
          }, {
            label: "西乡塘区",
            value: "450107"
          }, {
            label: "良庆区",
            value: "450108"
          }, {
            label: "邕宁区",
            value: "450109"
          }, {
            label: "武鸣区",
            value: "450110"
          }, {
            label: "隆安县",
            value: "450123"
          }, {
            label: "马山县",
            value: "450124"
          }, {
            label: "上林县",
            value: "450125"
          }, {
            label: "宾阳县",
            value: "450126"
          }, {
            label: "横县",
            value: "450127"
          }],
          [{
            label: "城中区",
            value: "450202"
          }, {
            label: "鱼峰区",
            value: "450203"
          }, {
            label: "柳南区",
            value: "450204"
          }, {
            label: "柳北区",
            value: "450205"
          }, {
            label: "柳江区",
            value: "450206"
          }, {
            label: "柳城县",
            value: "450222"
          }, {
            label: "鹿寨县",
            value: "450223"
          }, {
            label: "融安县",
            value: "450224"
          }, {
            label: "融水苗族自治县",
            value: "450225"
          }, {
            label: "三江侗族自治县",
            value: "450226"
          }],
          [{
            label: "秀峰区",
            value: "450302"
          }, {
            label: "叠彩区",
            value: "450303"
          }, {
            label: "象山区",
            value: "450304"
          }, {
            label: "七星区",
            value: "450305"
          }, {
            label: "雁山区",
            value: "450311"
          }, {
            label: "临桂区",
            value: "450312"
          }, {
            label: "阳朔县",
            value: "450321"
          }, {
            label: "灵川县",
            value: "450323"
          }, {
            label: "全州县",
            value: "450324"
          }, {
            label: "兴安县",
            value: "450325"
          }, {
            label: "永福县",
            value: "450326"
          }, {
            label: "灌阳县",
            value: "450327"
          }, {
            label: "龙胜各族自治县",
            value: "450328"
          }, {
            label: "资源县",
            value: "450329"
          }, {
            label: "平乐县",
            value: "450330"
          }, {
            label: "荔浦县",
            value: "450331"
          }, {
            label: "恭城瑶族自治县",
            value: "450332"
          }],
          [{
            label: "万秀区",
            value: "450403"
          }, {
            label: "长洲区",
            value: "450405"
          }, {
            label: "龙圩区",
            value: "450406"
          }, {
            label: "苍梧县",
            value: "450421"
          }, {
            label: "藤县",
            value: "450422"
          }, {
            label: "蒙山县",
            value: "450423"
          }, {
            label: "岑溪市",
            value: "450481"
          }],
          [{
            label: "海城区",
            value: "450502"
          }, {
            label: "银海区",
            value: "450503"
          }, {
            label: "铁山港区",
            value: "450512"
          }, {
            label: "合浦县",
            value: "450521"
          }],
          [{
            label: "港口区",
            value: "450602"
          }, {
            label: "防城区",
            value: "450603"
          }, {
            label: "上思县",
            value: "450621"
          }, {
            label: "东兴市",
            value: "450681"
          }],
          [{
            label: "钦南区",
            value: "450702"
          }, {
            label: "钦北区",
            value: "450703"
          }, {
            label: "灵山县",
            value: "450721"
          }, {
            label: "浦北县",
            value: "450722"
          }],
          [{
            label: "港北区",
            value: "450802"
          }, {
            label: "港南区",
            value: "450803"
          }, {
            label: "覃塘区",
            value: "450804"
          }, {
            label: "平南县",
            value: "450821"
          }, {
            label: "桂平市",
            value: "450881"
          }],
          [{
            label: "玉州区",
            value: "450902"
          }, {
            label: "福绵区",
            value: "450903"
          }, {
            label: "容县",
            value: "450921"
          }, {
            label: "陆川县",
            value: "450922"
          }, {
            label: "博白县",
            value: "450923"
          }, {
            label: "兴业县",
            value: "450924"
          }, {
            label: "北流市",
            value: "450981"
          }],
          [{
            label: "右江区",
            value: "451002"
          }, {
            label: "田阳县",
            value: "451021"
          }, {
            label: "田东县",
            value: "451022"
          }, {
            label: "平果县",
            value: "451023"
          }, {
            label: "德保县",
            value: "451024"
          }, {
            label: "那坡县",
            value: "451026"
          }, {
            label: "凌云县",
            value: "451027"
          }, {
            label: "乐业县",
            value: "451028"
          }, {
            label: "田林县",
            value: "451029"
          }, {
            label: "西林县",
            value: "451030"
          }, {
            label: "隆林各族自治县",
            value: "451031"
          }, {
            label: "靖西市",
            value: "451081"
          }],
          [{
            label: "八步区",
            value: "451102"
          }, {
            label: "平桂区",
            value: "451103"
          }, {
            label: "昭平县",
            value: "451121"
          }, {
            label: "钟山县",
            value: "451122"
          }, {
            label: "富川瑶族自治县",
            value: "451123"
          }],
          [{
            label: "金城江区",
            value: "451202"
          }, {
            label: "宜州区",
            value: "451203"
          }, {
            label: "南丹县",
            value: "451221"
          }, {
            label: "天峨县",
            value: "451222"
          }, {
            label: "凤山县",
            value: "451223"
          }, {
            label: "东兰县",
            value: "451224"
          }, {
            label: "罗城仫佬族自治县",
            value: "451225"
          }, {
            label: "环江毛南族自治县",
            value: "451226"
          }, {
            label: "巴马瑶族自治县",
            value: "451227"
          }, {
            label: "都安瑶族自治县",
            value: "451228"
          }, {
            label: "大化瑶族自治县",
            value: "451229"
          }],
          [{
            label: "兴宾区",
            value: "451302"
          }, {
            label: "忻城县",
            value: "451321"
          }, {
            label: "象州县",
            value: "451322"
          }, {
            label: "武宣县",
            value: "451323"
          }, {
            label: "金秀瑶族自治县",
            value: "451324"
          }, {
            label: "合山市",
            value: "451381"
          }],
          [{
            label: "江州区",
            value: "451402"
          }, {
            label: "扶绥县",
            value: "451421"
          }, {
            label: "宁明县",
            value: "451422"
          }, {
            label: "龙州县",
            value: "451423"
          }, {
            label: "大新县",
            value: "451424"
          }, {
            label: "天等县",
            value: "451425"
          }, {
            label: "凭祥市",
            value: "451481"
          }]
        ],
        [
          [{
            label: "秀英区",
            value: "460105"
          }, {
            label: "龙华区",
            value: "460106"
          }, {
            label: "琼山区",
            value: "460107"
          }, {
            label: "美兰区",
            value: "460108"
          }],
          [{
            label: "海棠区",
            value: "460202"
          }, {
            label: "吉阳区",
            value: "460203"
          }, {
            label: "天涯区",
            value: "460204"
          }, {
            label: "崖州区",
            value: "460205"
          }],
          [{
            label: "西沙群岛",
            value: "460321"
          }, {
            label: "南沙群岛",
            value: "460322"
          }, {
            label: "中沙群岛的岛礁及其海域",
            value: "460323"
          }],
          [{
            label: "儋州市",
            value: "460400"
          }],
          [{
            label: "五指山市",
            value: "469001"
          }, {
            label: "琼海市",
            value: "469002"
          }, {
            label: "文昌市",
            value: "469005"
          }, {
            label: "万宁市",
            value: "469006"
          }, {
            label: "东方市",
            value: "469007"
          }, {
            label: "定安县",
            value: "469021"
          }, {
            label: "屯昌县",
            value: "469022"
          }, {
            label: "澄迈县",
            value: "469023"
          }, {
            label: "临高县",
            value: "469024"
          }, {
            label: "白沙黎族自治县",
            value: "469025"
          }, {
            label: "昌江黎族自治县",
            value: "469026"
          }, {
            label: "乐东黎族自治县",
            value: "469027"
          }, {
            label: "陵水黎族自治县",
            value: "469028"
          }, {
            label: "保亭黎族苗族自治县",
            value: "469029"
          }, {
            label: "琼中黎族苗族自治县",
            value: "469030"
          }]
        ],
        [
          [{
            label: "万州区",
            value: "500101"
          }, {
            label: "涪陵区",
            value: "500102"
          }, {
            label: "渝中区",
            value: "500103"
          }, {
            label: "大渡口区",
            value: "500104"
          }, {
            label: "江北区",
            value: "500105"
          }, {
            label: "沙坪坝区",
            value: "500106"
          }, {
            label: "九龙坡区",
            value: "500107"
          }, {
            label: "南岸区",
            value: "500108"
          }, {
            label: "北碚区",
            value: "500109"
          }, {
            label: "綦江区",
            value: "500110"
          }, {
            label: "大足区",
            value: "500111"
          }, {
            label: "渝北区",
            value: "500112"
          }, {
            label: "巴南区",
            value: "500113"
          }, {
            label: "黔江区",
            value: "500114"
          }, {
            label: "长寿区",
            value: "500115"
          }, {
            label: "江津区",
            value: "500116"
          }, {
            label: "合川区",
            value: "500117"
          }, {
            label: "永川区",
            value: "500118"
          }, {
            label: "南川区",
            value: "500119"
          }, {
            label: "璧山区",
            value: "500120"
          }, {
            label: "铜梁区",
            value: "500151"
          }, {
            label: "潼南区",
            value: "500152"
          }, {
            label: "荣昌区",
            value: "500153"
          }, {
            label: "开州区",
            value: "500154"
          }, {
            label: "梁平区",
            value: "500155"
          }, {
            label: "武隆区",
            value: "500156"
          }],
          [{
            label: "城口县",
            value: "500229"
          }, {
            label: "丰都县",
            value: "500230"
          }, {
            label: "垫江县",
            value: "500231"
          }, {
            label: "忠县",
            value: "500233"
          }, {
            label: "云阳县",
            value: "500235"
          }, {
            label: "奉节县",
            value: "500236"
          }, {
            label: "巫山县",
            value: "500237"
          }, {
            label: "巫溪县",
            value: "500238"
          }, {
            label: "石柱土家族自治县",
            value: "500240"
          }, {
            label: "秀山土家族苗族自治县",
            value: "500241"
          }, {
            label: "酉阳土家族苗族自治县",
            value: "500242"
          }, {
            label: "彭水苗族土家族自治县",
            value: "500243"
          }]
        ],
        [
          [{
            label: "锦江区",
            value: "510104"
          }, {
            label: "青羊区",
            value: "510105"
          }, {
            label: "金牛区",
            value: "510106"
          }, {
            label: "武侯区",
            value: "510107"
          }, {
            label: "成华区",
            value: "510108"
          }, {
            label: "龙泉驿区",
            value: "510112"
          }, {
            label: "青白江区",
            value: "510113"
          }, {
            label: "新都区",
            value: "510114"
          }, {
            label: "温江区",
            value: "510115"
          }, {
            label: "双流区",
            value: "510116"
          }, {
            label: "郫都区",
            value: "510117"
          }, {
            label: "金堂县",
            value: "510121"
          }, {
            label: "大邑县",
            value: "510129"
          }, {
            label: "蒲江县",
            value: "510131"
          }, {
            label: "新津县",
            value: "510132"
          }, {
            label: "都江堰市",
            value: "510181"
          }, {
            label: "彭州市",
            value: "510182"
          }, {
            label: "邛崃市",
            value: "510183"
          }, {
            label: "崇州市",
            value: "510184"
          }, {
            label: "简阳市",
            value: "510185"
          }],
          [{
            label: "自流井区",
            value: "510302"
          }, {
            label: "贡井区",
            value: "510303"
          }, {
            label: "大安区",
            value: "510304"
          }, {
            label: "沿滩区",
            value: "510311"
          }, {
            label: "荣县",
            value: "510321"
          }, {
            label: "富顺县",
            value: "510322"
          }],
          [{
            label: "东区",
            value: "510402"
          }, {
            label: "西区",
            value: "510403"
          }, {
            label: "仁和区",
            value: "510411"
          }, {
            label: "米易县",
            value: "510421"
          }, {
            label: "盐边县",
            value: "510422"
          }],
          [{
            label: "江阳区",
            value: "510502"
          }, {
            label: "纳溪区",
            value: "510503"
          }, {
            label: "龙马潭区",
            value: "510504"
          }, {
            label: "泸县",
            value: "510521"
          }, {
            label: "合江县",
            value: "510522"
          }, {
            label: "叙永县",
            value: "510524"
          }, {
            label: "古蔺县",
            value: "510525"
          }],
          [{
            label: "旌阳区",
            value: "510603"
          }, {
            label: "罗江区",
            value: "510604"
          }, {
            label: "中江县",
            value: "510623"
          }, {
            label: "广汉市",
            value: "510681"
          }, {
            label: "什邡市",
            value: "510682"
          }, {
            label: "绵竹市",
            value: "510683"
          }],
          [{
            label: "涪城区",
            value: "510703"
          }, {
            label: "游仙区",
            value: "510704"
          }, {
            label: "安州区",
            value: "510705"
          }, {
            label: "三台县",
            value: "510722"
          }, {
            label: "盐亭县",
            value: "510723"
          }, {
            label: "梓潼县",
            value: "510725"
          }, {
            label: "北川羌族自治县",
            value: "510726"
          }, {
            label: "平武县",
            value: "510727"
          }, {
            label: "江油市",
            value: "510781"
          }],
          [{
            label: "利州区",
            value: "510802"
          }, {
            label: "昭化区",
            value: "510811"
          }, {
            label: "朝天区",
            value: "510812"
          }, {
            label: "旺苍县",
            value: "510821"
          }, {
            label: "青川县",
            value: "510822"
          }, {
            label: "剑阁县",
            value: "510823"
          }, {
            label: "苍溪县",
            value: "510824"
          }],
          [{
            label: "船山区",
            value: "510903"
          }, {
            label: "安居区",
            value: "510904"
          }, {
            label: "蓬溪县",
            value: "510921"
          }, {
            label: "射洪县",
            value: "510922"
          }, {
            label: "大英县",
            value: "510923"
          }],
          [{
            label: "市中区",
            value: "511002"
          }, {
            label: "东兴区",
            value: "511011"
          }, {
            label: "威远县",
            value: "511024"
          }, {
            label: "资中县",
            value: "511025"
          }, {
            label: "内江经济开发区",
            value: "511071"
          }, {
            label: "隆昌市",
            value: "511083"
          }],
          [{
            label: "市中区",
            value: "511102"
          }, {
            label: "沙湾区",
            value: "511111"
          }, {
            label: "五通桥区",
            value: "511112"
          }, {
            label: "金口河区",
            value: "511113"
          }, {
            label: "犍为县",
            value: "511123"
          }, {
            label: "井研县",
            value: "511124"
          }, {
            label: "夹江县",
            value: "511126"
          }, {
            label: "沐川县",
            value: "511129"
          }, {
            label: "峨边彝族自治县",
            value: "511132"
          }, {
            label: "马边彝族自治县",
            value: "511133"
          }, {
            label: "峨眉山市",
            value: "511181"
          }],
          [{
            label: "顺庆区",
            value: "511302"
          }, {
            label: "高坪区",
            value: "511303"
          }, {
            label: "嘉陵区",
            value: "511304"
          }, {
            label: "南部县",
            value: "511321"
          }, {
            label: "营山县",
            value: "511322"
          }, {
            label: "蓬安县",
            value: "511323"
          }, {
            label: "仪陇县",
            value: "511324"
          }, {
            label: "西充县",
            value: "511325"
          }, {
            label: "阆中市",
            value: "511381"
          }],
          [{
            label: "东坡区",
            value: "511402"
          }, {
            label: "彭山区",
            value: "511403"
          }, {
            label: "仁寿县",
            value: "511421"
          }, {
            label: "洪雅县",
            value: "511423"
          }, {
            label: "丹棱县",
            value: "511424"
          }, {
            label: "青神县",
            value: "511425"
          }],
          [{
            label: "翠屏区",
            value: "511502"
          }, {
            label: "南溪区",
            value: "511503"
          }, {
            label: "宜宾县",
            value: "511521"
          }, {
            label: "江安县",
            value: "511523"
          }, {
            label: "长宁县",
            value: "511524"
          }, {
            label: "高县",
            value: "511525"
          }, {
            label: "珙县",
            value: "511526"
          }, {
            label: "筠连县",
            value: "511527"
          }, {
            label: "兴文县",
            value: "511528"
          }, {
            label: "屏山县",
            value: "511529"
          }],
          [{
            label: "广安区",
            value: "511602"
          }, {
            label: "前锋区",
            value: "511603"
          }, {
            label: "岳池县",
            value: "511621"
          }, {
            label: "武胜县",
            value: "511622"
          }, {
            label: "邻水县",
            value: "511623"
          }, {
            label: "华蓥市",
            value: "511681"
          }],
          [{
            label: "通川区",
            value: "511702"
          }, {
            label: "达川区",
            value: "511703"
          }, {
            label: "宣汉县",
            value: "511722"
          }, {
            label: "开江县",
            value: "511723"
          }, {
            label: "大竹县",
            value: "511724"
          }, {
            label: "渠县",
            value: "511725"
          }, {
            label: "达州经济开发区",
            value: "511771"
          }, {
            label: "万源市",
            value: "511781"
          }],
          [{
            label: "雨城区",
            value: "511802"
          }, {
            label: "名山区",
            value: "511803"
          }, {
            label: "荥经县",
            value: "511822"
          }, {
            label: "汉源县",
            value: "511823"
          }, {
            label: "石棉县",
            value: "511824"
          }, {
            label: "天全县",
            value: "511825"
          }, {
            label: "芦山县",
            value: "511826"
          }, {
            label: "宝兴县",
            value: "511827"
          }],
          [{
            label: "巴州区",
            value: "511902"
          }, {
            label: "恩阳区",
            value: "511903"
          }, {
            label: "通江县",
            value: "511921"
          }, {
            label: "南江县",
            value: "511922"
          }, {
            label: "平昌县",
            value: "511923"
          }, {
            label: "巴中经济开发区",
            value: "511971"
          }],
          [{
            label: "雁江区",
            value: "512002"
          }, {
            label: "安岳县",
            value: "512021"
          }, {
            label: "乐至县",
            value: "512022"
          }],
          [{
            label: "马尔康市",
            value: "513201"
          }, {
            label: "汶川县",
            value: "513221"
          }, {
            label: "理县",
            value: "513222"
          }, {
            label: "茂县",
            value: "513223"
          }, {
            label: "松潘县",
            value: "513224"
          }, {
            label: "九寨沟县",
            value: "513225"
          }, {
            label: "金川县",
            value: "513226"
          }, {
            label: "小金县",
            value: "513227"
          }, {
            label: "黑水县",
            value: "513228"
          }, {
            label: "壤塘县",
            value: "513230"
          }, {
            label: "阿坝县",
            value: "513231"
          }, {
            label: "若尔盖县",
            value: "513232"
          }, {
            label: "红原县",
            value: "513233"
          }],
          [{
            label: "康定市",
            value: "513301"
          }, {
            label: "泸定县",
            value: "513322"
          }, {
            label: "丹巴县",
            value: "513323"
          }, {
            label: "九龙县",
            value: "513324"
          }, {
            label: "雅江县",
            value: "513325"
          }, {
            label: "道孚县",
            value: "513326"
          }, {
            label: "炉霍县",
            value: "513327"
          }, {
            label: "甘孜县",
            value: "513328"
          }, {
            label: "新龙县",
            value: "513329"
          }, {
            label: "德格县",
            value: "513330"
          }, {
            label: "白玉县",
            value: "513331"
          }, {
            label: "石渠县",
            value: "513332"
          }, {
            label: "色达县",
            value: "513333"
          }, {
            label: "理塘县",
            value: "513334"
          }, {
            label: "巴塘县",
            value: "513335"
          }, {
            label: "乡城县",
            value: "513336"
          }, {
            label: "稻城县",
            value: "513337"
          }, {
            label: "得荣县",
            value: "513338"
          }],
          [{
            label: "西昌市",
            value: "513401"
          }, {
            label: "木里藏族自治县",
            value: "513422"
          }, {
            label: "盐源县",
            value: "513423"
          }, {
            label: "德昌县",
            value: "513424"
          }, {
            label: "会理县",
            value: "513425"
          }, {
            label: "会东县",
            value: "513426"
          }, {
            label: "宁南县",
            value: "513427"
          }, {
            label: "普格县",
            value: "513428"
          }, {
            label: "布拖县",
            value: "513429"
          }, {
            label: "金阳县",
            value: "513430"
          }, {
            label: "昭觉县",
            value: "513431"
          }, {
            label: "喜德县",
            value: "513432"
          }, {
            label: "冕宁县",
            value: "513433"
          }, {
            label: "越西县",
            value: "513434"
          }, {
            label: "甘洛县",
            value: "513435"
          }, {
            label: "美姑县",
            value: "513436"
          }, {
            label: "雷波县",
            value: "513437"
          }]
        ],
        [
          [{
            label: "南明区",
            value: "520102"
          }, {
            label: "云岩区",
            value: "520103"
          }, {
            label: "花溪区",
            value: "520111"
          }, {
            label: "乌当区",
            value: "520112"
          }, {
            label: "白云区",
            value: "520113"
          }, {
            label: "观山湖区",
            value: "520115"
          }, {
            label: "开阳县",
            value: "520121"
          }, {
            label: "息烽县",
            value: "520122"
          }, {
            label: "修文县",
            value: "520123"
          }, {
            label: "清镇市",
            value: "520181"
          }],
          [{
            label: "钟山区",
            value: "520201"
          }, {
            label: "六枝特区",
            value: "520203"
          }, {
            label: "水城县",
            value: "520221"
          }, {
            label: "盘州市",
            value: "520281"
          }],
          [{
            label: "红花岗区",
            value: "520302"
          }, {
            label: "汇川区",
            value: "520303"
          }, {
            label: "播州区",
            value: "520304"
          }, {
            label: "桐梓县",
            value: "520322"
          }, {
            label: "绥阳县",
            value: "520323"
          }, {
            label: "正安县",
            value: "520324"
          }, {
            label: "道真仡佬族苗族自治县",
            value: "520325"
          }, {
            label: "务川仡佬族苗族自治县",
            value: "520326"
          }, {
            label: "凤冈县",
            value: "520327"
          }, {
            label: "湄潭县",
            value: "520328"
          }, {
            label: "余庆县",
            value: "520329"
          }, {
            label: "习水县",
            value: "520330"
          }, {
            label: "赤水市",
            value: "520381"
          }, {
            label: "仁怀市",
            value: "520382"
          }],
          [{
            label: "西秀区",
            value: "520402"
          }, {
            label: "平坝区",
            value: "520403"
          }, {
            label: "普定县",
            value: "520422"
          }, {
            label: "镇宁布依族苗族自治县",
            value: "520423"
          }, {
            label: "关岭布依族苗族自治县",
            value: "520424"
          }, {
            label: "紫云苗族布依族自治县",
            value: "520425"
          }],
          [{
            label: "七星关区",
            value: "520502"
          }, {
            label: "大方县",
            value: "520521"
          }, {
            label: "黔西县",
            value: "520522"
          }, {
            label: "金沙县",
            value: "520523"
          }, {
            label: "织金县",
            value: "520524"
          }, {
            label: "纳雍县",
            value: "520525"
          }, {
            label: "威宁彝族回族苗族自治县",
            value: "520526"
          }, {
            label: "赫章县",
            value: "520527"
          }],
          [{
            label: "碧江区",
            value: "520602"
          }, {
            label: "万山区",
            value: "520603"
          }, {
            label: "江口县",
            value: "520621"
          }, {
            label: "玉屏侗族自治县",
            value: "520622"
          }, {
            label: "石阡县",
            value: "520623"
          }, {
            label: "思南县",
            value: "520624"
          }, {
            label: "印江土家族苗族自治县",
            value: "520625"
          }, {
            label: "德江县",
            value: "520626"
          }, {
            label: "沿河土家族自治县",
            value: "520627"
          }, {
            label: "松桃苗族自治县",
            value: "520628"
          }],
          [{
            label: "兴义市",
            value: "522301"
          }, {
            label: "兴仁县",
            value: "522322"
          }, {
            label: "普安县",
            value: "522323"
          }, {
            label: "晴隆县",
            value: "522324"
          }, {
            label: "贞丰县",
            value: "522325"
          }, {
            label: "望谟县",
            value: "522326"
          }, {
            label: "册亨县",
            value: "522327"
          }, {
            label: "安龙县",
            value: "522328"
          }],
          [{
            label: "凯里市",
            value: "522601"
          }, {
            label: "黄平县",
            value: "522622"
          }, {
            label: "施秉县",
            value: "522623"
          }, {
            label: "三穗县",
            value: "522624"
          }, {
            label: "镇远县",
            value: "522625"
          }, {
            label: "岑巩县",
            value: "522626"
          }, {
            label: "天柱县",
            value: "522627"
          }, {
            label: "锦屏县",
            value: "522628"
          }, {
            label: "剑河县",
            value: "522629"
          }, {
            label: "台江县",
            value: "522630"
          }, {
            label: "黎平县",
            value: "522631"
          }, {
            label: "榕江县",
            value: "522632"
          }, {
            label: "从江县",
            value: "522633"
          }, {
            label: "雷山县",
            value: "522634"
          }, {
            label: "麻江县",
            value: "522635"
          }, {
            label: "丹寨县",
            value: "522636"
          }],
          [{
            label: "都匀市",
            value: "522701"
          }, {
            label: "福泉市",
            value: "522702"
          }, {
            label: "荔波县",
            value: "522722"
          }, {
            label: "贵定县",
            value: "522723"
          }, {
            label: "瓮安县",
            value: "522725"
          }, {
            label: "独山县",
            value: "522726"
          }, {
            label: "平塘县",
            value: "522727"
          }, {
            label: "罗甸县",
            value: "522728"
          }, {
            label: "长顺县",
            value: "522729"
          }, {
            label: "龙里县",
            value: "522730"
          }, {
            label: "惠水县",
            value: "522731"
          }, {
            label: "三都水族自治县",
            value: "522732"
          }]
        ],
        [
          [{
            label: "五华区",
            value: "530102"
          }, {
            label: "盘龙区",
            value: "530103"
          }, {
            label: "官渡区",
            value: "530111"
          }, {
            label: "西山区",
            value: "530112"
          }, {
            label: "东川区",
            value: "530113"
          }, {
            label: "呈贡区",
            value: "530114"
          }, {
            label: "晋宁区",
            value: "530115"
          }, {
            label: "富民县",
            value: "530124"
          }, {
            label: "宜良县",
            value: "530125"
          }, {
            label: "石林彝族自治县",
            value: "530126"
          }, {
            label: "嵩明县",
            value: "530127"
          }, {
            label: "禄劝彝族苗族自治县",
            value: "530128"
          }, {
            label: "寻甸回族彝族自治县",
            value: "530129"
          }, {
            label: "安宁市",
            value: "530181"
          }],
          [{
            label: "麒麟区",
            value: "530302"
          }, {
            label: "沾益区",
            value: "530303"
          }, {
            label: "马龙县",
            value: "530321"
          }, {
            label: "陆良县",
            value: "530322"
          }, {
            label: "师宗县",
            value: "530323"
          }, {
            label: "罗平县",
            value: "530324"
          }, {
            label: "富源县",
            value: "530325"
          }, {
            label: "会泽县",
            value: "530326"
          }, {
            label: "宣威市",
            value: "530381"
          }],
          [{
            label: "红塔区",
            value: "530402"
          }, {
            label: "江川区",
            value: "530403"
          }, {
            label: "澄江县",
            value: "530422"
          }, {
            label: "通海县",
            value: "530423"
          }, {
            label: "华宁县",
            value: "530424"
          }, {
            label: "易门县",
            value: "530425"
          }, {
            label: "峨山彝族自治县",
            value: "530426"
          }, {
            label: "新平彝族傣族自治县",
            value: "530427"
          }, {
            label: "元江哈尼族彝族傣族自治县",
            value: "530428"
          }],
          [{
            label: "隆阳区",
            value: "530502"
          }, {
            label: "施甸县",
            value: "530521"
          }, {
            label: "龙陵县",
            value: "530523"
          }, {
            label: "昌宁县",
            value: "530524"
          }, {
            label: "腾冲市",
            value: "530581"
          }],
          [{
            label: "昭阳区",
            value: "530602"
          }, {
            label: "鲁甸县",
            value: "530621"
          }, {
            label: "巧家县",
            value: "530622"
          }, {
            label: "盐津县",
            value: "530623"
          }, {
            label: "大关县",
            value: "530624"
          }, {
            label: "永善县",
            value: "530625"
          }, {
            label: "绥江县",
            value: "530626"
          }, {
            label: "镇雄县",
            value: "530627"
          }, {
            label: "彝良县",
            value: "530628"
          }, {
            label: "威信县",
            value: "530629"
          }, {
            label: "水富县",
            value: "530630"
          }],
          [{
            label: "古城区",
            value: "530702"
          }, {
            label: "玉龙纳西族自治县",
            value: "530721"
          }, {
            label: "永胜县",
            value: "530722"
          }, {
            label: "华坪县",
            value: "530723"
          }, {
            label: "宁蒗彝族自治县",
            value: "530724"
          }],
          [{
            label: "思茅区",
            value: "530802"
          }, {
            label: "宁洱哈尼族彝族自治县",
            value: "530821"
          }, {
            label: "墨江哈尼族自治县",
            value: "530822"
          }, {
            label: "景东彝族自治县",
            value: "530823"
          }, {
            label: "景谷傣族彝族自治县",
            value: "530824"
          }, {
            label: "镇沅彝族哈尼族拉祜族自治县",
            value: "530825"
          }, {
            label: "江城哈尼族彝族自治县",
            value: "530826"
          }, {
            label: "孟连傣族拉祜族佤族自治县",
            value: "530827"
          }, {
            label: "澜沧拉祜族自治县",
            value: "530828"
          }, {
            label: "西盟佤族自治县",
            value: "530829"
          }],
          [{
            label: "临翔区",
            value: "530902"
          }, {
            label: "凤庆县",
            value: "530921"
          }, {
            label: "云县",
            value: "530922"
          }, {
            label: "永德县",
            value: "530923"
          }, {
            label: "镇康县",
            value: "530924"
          }, {
            label: "双江拉祜族佤族布朗族傣族自治县",
            value: "530925"
          }, {
            label: "耿马傣族佤族自治县",
            value: "530926"
          }, {
            label: "沧源佤族自治县",
            value: "530927"
          }],
          [{
            label: "楚雄市",
            value: "532301"
          }, {
            label: "双柏县",
            value: "532322"
          }, {
            label: "牟定县",
            value: "532323"
          }, {
            label: "南华县",
            value: "532324"
          }, {
            label: "姚安县",
            value: "532325"
          }, {
            label: "大姚县",
            value: "532326"
          }, {
            label: "永仁县",
            value: "532327"
          }, {
            label: "元谋县",
            value: "532328"
          }, {
            label: "武定县",
            value: "532329"
          }, {
            label: "禄丰县",
            value: "532331"
          }],
          [{
            label: "个旧市",
            value: "532501"
          }, {
            label: "开远市",
            value: "532502"
          }, {
            label: "蒙自市",
            value: "532503"
          }, {
            label: "弥勒市",
            value: "532504"
          }, {
            label: "屏边苗族自治县",
            value: "532523"
          }, {
            label: "建水县",
            value: "532524"
          }, {
            label: "石屏县",
            value: "532525"
          }, {
            label: "泸西县",
            value: "532527"
          }, {
            label: "元阳县",
            value: "532528"
          }, {
            label: "红河县",
            value: "532529"
          }, {
            label: "金平苗族瑶族傣族自治县",
            value: "532530"
          }, {
            label: "绿春县",
            value: "532531"
          }, {
            label: "河口瑶族自治县",
            value: "532532"
          }],
          [{
            label: "文山市",
            value: "532601"
          }, {
            label: "砚山县",
            value: "532622"
          }, {
            label: "西畴县",
            value: "532623"
          }, {
            label: "麻栗坡县",
            value: "532624"
          }, {
            label: "马关县",
            value: "532625"
          }, {
            label: "丘北县",
            value: "532626"
          }, {
            label: "广南县",
            value: "532627"
          }, {
            label: "富宁县",
            value: "532628"
          }],
          [{
            label: "景洪市",
            value: "532801"
          }, {
            label: "勐海县",
            value: "532822"
          }, {
            label: "勐腊县",
            value: "532823"
          }],
          [{
            label: "大理市",
            value: "532901"
          }, {
            label: "漾濞彝族自治县",
            value: "532922"
          }, {
            label: "祥云县",
            value: "532923"
          }, {
            label: "宾川县",
            value: "532924"
          }, {
            label: "弥渡县",
            value: "532925"
          }, {
            label: "南涧彝族自治县",
            value: "532926"
          }, {
            label: "巍山彝族回族自治县",
            value: "532927"
          }, {
            label: "永平县",
            value: "532928"
          }, {
            label: "云龙县",
            value: "532929"
          }, {
            label: "洱源县",
            value: "532930"
          }, {
            label: "剑川县",
            value: "532931"
          }, {
            label: "鹤庆县",
            value: "532932"
          }],
          [{
            label: "瑞丽市",
            value: "533102"
          }, {
            label: "芒市",
            value: "533103"
          }, {
            label: "梁河县",
            value: "533122"
          }, {
            label: "盈江县",
            value: "533123"
          }, {
            label: "陇川县",
            value: "533124"
          }],
          [{
            label: "泸水市",
            value: "533301"
          }, {
            label: "福贡县",
            value: "533323"
          }, {
            label: "贡山独龙族怒族自治县",
            value: "533324"
          }, {
            label: "兰坪白族普米族自治县",
            value: "533325"
          }],
          [{
            label: "香格里拉市",
            value: "533401"
          }, {
            label: "德钦县",
            value: "533422"
          }, {
            label: "维西傈僳族自治县",
            value: "533423"
          }]
        ],
        [
          [{
            label: "城关区",
            value: "540102"
          }, {
            label: "堆龙德庆区",
            value: "540103"
          }, {
            label: "林周县",
            value: "540121"
          }, {
            label: "当雄县",
            value: "540122"
          }, {
            label: "尼木县",
            value: "540123"
          }, {
            label: "曲水县",
            value: "540124"
          }, {
            label: "达孜县",
            value: "540126"
          }, {
            label: "墨竹工卡县",
            value: "540127"
          }, {
            label: "格尔木藏青工业园区",
            value: "540171"
          }, {
            label: "拉萨经济技术开发区",
            value: "540172"
          }, {
            label: "西藏文化旅游创意园区",
            value: "540173"
          }, {
            label: "达孜工业园区",
            value: "540174"
          }],
          [{
            label: "桑珠孜区",
            value: "540202"
          }, {
            label: "南木林县",
            value: "540221"
          }, {
            label: "江孜县",
            value: "540222"
          }, {
            label: "定日县",
            value: "540223"
          }, {
            label: "萨迦县",
            value: "540224"
          }, {
            label: "拉孜县",
            value: "540225"
          }, {
            label: "昂仁县",
            value: "540226"
          }, {
            label: "谢通门县",
            value: "540227"
          }, {
            label: "白朗县",
            value: "540228"
          }, {
            label: "仁布县",
            value: "540229"
          }, {
            label: "康马县",
            value: "540230"
          }, {
            label: "定结县",
            value: "540231"
          }, {
            label: "仲巴县",
            value: "540232"
          }, {
            label: "亚东县",
            value: "540233"
          }, {
            label: "吉隆县",
            value: "540234"
          }, {
            label: "聂拉木县",
            value: "540235"
          }, {
            label: "萨嘎县",
            value: "540236"
          }, {
            label: "岗巴县",
            value: "540237"
          }],
          [{
            label: "卡若区",
            value: "540302"
          }, {
            label: "江达县",
            value: "540321"
          }, {
            label: "贡觉县",
            value: "540322"
          }, {
            label: "类乌齐县",
            value: "540323"
          }, {
            label: "丁青县",
            value: "540324"
          }, {
            label: "察雅县",
            value: "540325"
          }, {
            label: "八宿县",
            value: "540326"
          }, {
            label: "左贡县",
            value: "540327"
          }, {
            label: "芒康县",
            value: "540328"
          }, {
            label: "洛隆县",
            value: "540329"
          }, {
            label: "边坝县",
            value: "540330"
          }],
          [{
            label: "巴宜区",
            value: "540402"
          }, {
            label: "工布江达县",
            value: "540421"
          }, {
            label: "米林县",
            value: "540422"
          }, {
            label: "墨脱县",
            value: "540423"
          }, {
            label: "波密县",
            value: "540424"
          }, {
            label: "察隅县",
            value: "540425"
          }, {
            label: "朗县",
            value: "540426"
          }],
          [{
            label: "乃东区",
            value: "540502"
          }, {
            label: "扎囊县",
            value: "540521"
          }, {
            label: "贡嘎县",
            value: "540522"
          }, {
            label: "桑日县",
            value: "540523"
          }, {
            label: "琼结县",
            value: "540524"
          }, {
            label: "曲松县",
            value: "540525"
          }, {
            label: "措美县",
            value: "540526"
          }, {
            label: "洛扎县",
            value: "540527"
          }, {
            label: "加查县",
            value: "540528"
          }, {
            label: "隆子县",
            value: "540529"
          }, {
            label: "错那县",
            value: "540530"
          }, {
            label: "浪卡子县",
            value: "540531"
          }],
          [{
            label: "那曲县",
            value: "542421"
          }, {
            label: "嘉黎县",
            value: "542422"
          }, {
            label: "比如县",
            value: "542423"
          }, {
            label: "聂荣县",
            value: "542424"
          }, {
            label: "安多县",
            value: "542425"
          }, {
            label: "申扎县",
            value: "542426"
          }, {
            label: "索县",
            value: "542427"
          }, {
            label: "班戈县",
            value: "542428"
          }, {
            label: "巴青县",
            value: "542429"
          }, {
            label: "尼玛县",
            value: "542430"
          }, {
            label: "双湖县",
            value: "542431"
          }],
          [{
            label: "普兰县",
            value: "542521"
          }, {
            label: "札达县",
            value: "542522"
          }, {
            label: "噶尔县",
            value: "542523"
          }, {
            label: "日土县",
            value: "542524"
          }, {
            label: "革吉县",
            value: "542525"
          }, {
            label: "改则县",
            value: "542526"
          }, {
            label: "措勤县",
            value: "542527"
          }]
        ],
        [
          [{
            label: "新城区",
            value: "610102"
          }, {
            label: "碑林区",
            value: "610103"
          }, {
            label: "莲湖区",
            value: "610104"
          }, {
            label: "灞桥区",
            value: "610111"
          }, {
            label: "未央区",
            value: "610112"
          }, {
            label: "雁塔区",
            value: "610113"
          }, {
            label: "阎良区",
            value: "610114"
          }, {
            label: "临潼区",
            value: "610115"
          }, {
            label: "长安区",
            value: "610116"
          }, {
            label: "高陵区",
            value: "610117"
          }, {
            label: "鄠邑区",
            value: "610118"
          }, {
            label: "蓝田县",
            value: "610122"
          }, {
            label: "周至县",
            value: "610124"
          }],
          [{
            label: "王益区",
            value: "610202"
          }, {
            label: "印台区",
            value: "610203"
          }, {
            label: "耀州区",
            value: "610204"
          }, {
            label: "宜君县",
            value: "610222"
          }],
          [{
            label: "渭滨区",
            value: "610302"
          }, {
            label: "金台区",
            value: "610303"
          }, {
            label: "陈仓区",
            value: "610304"
          }, {
            label: "凤翔县",
            value: "610322"
          }, {
            label: "岐山县",
            value: "610323"
          }, {
            label: "扶风县",
            value: "610324"
          }, {
            label: "眉县",
            value: "610326"
          }, {
            label: "陇县",
            value: "610327"
          }, {
            label: "千阳县",
            value: "610328"
          }, {
            label: "麟游县",
            value: "610329"
          }, {
            label: "凤县",
            value: "610330"
          }, {
            label: "太白县",
            value: "610331"
          }],
          [{
            label: "秦都区",
            value: "610402"
          }, {
            label: "杨陵区",
            value: "610403"
          }, {
            label: "渭城区",
            value: "610404"
          }, {
            label: "三原县",
            value: "610422"
          }, {
            label: "泾阳县",
            value: "610423"
          }, {
            label: "乾县",
            value: "610424"
          }, {
            label: "礼泉县",
            value: "610425"
          }, {
            label: "永寿县",
            value: "610426"
          }, {
            label: "彬县",
            value: "610427"
          }, {
            label: "长武县",
            value: "610428"
          }, {
            label: "旬邑县",
            value: "610429"
          }, {
            label: "淳化县",
            value: "610430"
          }, {
            label: "武功县",
            value: "610431"
          }, {
            label: "兴平市",
            value: "610481"
          }],
          [{
            label: "临渭区",
            value: "610502"
          }, {
            label: "华州区",
            value: "610503"
          }, {
            label: "潼关县",
            value: "610522"
          }, {
            label: "大荔县",
            value: "610523"
          }, {
            label: "合阳县",
            value: "610524"
          }, {
            label: "澄城县",
            value: "610525"
          }, {
            label: "蒲城县",
            value: "610526"
          }, {
            label: "白水县",
            value: "610527"
          }, {
            label: "富平县",
            value: "610528"
          }, {
            label: "韩城市",
            value: "610581"
          }, {
            label: "华阴市",
            value: "610582"
          }],
          [{
            label: "宝塔区",
            value: "610602"
          }, {
            label: "安塞区",
            value: "610603"
          }, {
            label: "延长县",
            value: "610621"
          }, {
            label: "延川县",
            value: "610622"
          }, {
            label: "子长县",
            value: "610623"
          }, {
            label: "志丹县",
            value: "610625"
          }, {
            label: "吴起县",
            value: "610626"
          }, {
            label: "甘泉县",
            value: "610627"
          }, {
            label: "富县",
            value: "610628"
          }, {
            label: "洛川县",
            value: "610629"
          }, {
            label: "宜川县",
            value: "610630"
          }, {
            label: "黄龙县",
            value: "610631"
          }, {
            label: "黄陵县",
            value: "610632"
          }],
          [{
            label: "汉台区",
            value: "610702"
          }, {
            label: "南郑区",
            value: "610703"
          }, {
            label: "城固县",
            value: "610722"
          }, {
            label: "洋县",
            value: "610723"
          }, {
            label: "西乡县",
            value: "610724"
          }, {
            label: "勉县",
            value: "610725"
          }, {
            label: "宁强县",
            value: "610726"
          }, {
            label: "略阳县",
            value: "610727"
          }, {
            label: "镇巴县",
            value: "610728"
          }, {
            label: "留坝县",
            value: "610729"
          }, {
            label: "佛坪县",
            value: "610730"
          }],
          [{
            label: "榆阳区",
            value: "610802"
          }, {
            label: "横山区",
            value: "610803"
          }, {
            label: "府谷县",
            value: "610822"
          }, {
            label: "靖边县",
            value: "610824"
          }, {
            label: "定边县",
            value: "610825"
          }, {
            label: "绥德县",
            value: "610826"
          }, {
            label: "米脂县",
            value: "610827"
          }, {
            label: "佳县",
            value: "610828"
          }, {
            label: "吴堡县",
            value: "610829"
          }, {
            label: "清涧县",
            value: "610830"
          }, {
            label: "子洲县",
            value: "610831"
          }, {
            label: "神木市",
            value: "610881"
          }],
          [{
            label: "汉滨区",
            value: "610902"
          }, {
            label: "汉阴县",
            value: "610921"
          }, {
            label: "石泉县",
            value: "610922"
          }, {
            label: "宁陕县",
            value: "610923"
          }, {
            label: "紫阳县",
            value: "610924"
          }, {
            label: "岚皋县",
            value: "610925"
          }, {
            label: "平利县",
            value: "610926"
          }, {
            label: "镇坪县",
            value: "610927"
          }, {
            label: "旬阳县",
            value: "610928"
          }, {
            label: "白河县",
            value: "610929"
          }],
          [{
            label: "商州区",
            value: "611002"
          }, {
            label: "洛南县",
            value: "611021"
          }, {
            label: "丹凤县",
            value: "611022"
          }, {
            label: "商南县",
            value: "611023"
          }, {
            label: "山阳县",
            value: "611024"
          }, {
            label: "镇安县",
            value: "611025"
          }, {
            label: "柞水县",
            value: "611026"
          }]
        ],
        [
          [{
            label: "城关区",
            value: "620102"
          }, {
            label: "七里河区",
            value: "620103"
          }, {
            label: "西固区",
            value: "620104"
          }, {
            label: "安宁区",
            value: "620105"
          }, {
            label: "红古区",
            value: "620111"
          }, {
            label: "永登县",
            value: "620121"
          }, {
            label: "皋兰县",
            value: "620122"
          }, {
            label: "榆中县",
            value: "620123"
          }, {
            label: "兰州新区",
            value: "620171"
          }],
          [{
            label: "嘉峪关市",
            value: "620201"
          }],
          [{
            label: "金川区",
            value: "620302"
          }, {
            label: "永昌县",
            value: "620321"
          }],
          [{
            label: "白银区",
            value: "620402"
          }, {
            label: "平川区",
            value: "620403"
          }, {
            label: "靖远县",
            value: "620421"
          }, {
            label: "会宁县",
            value: "620422"
          }, {
            label: "景泰县",
            value: "620423"
          }],
          [{
            label: "秦州区",
            value: "620502"
          }, {
            label: "麦积区",
            value: "620503"
          }, {
            label: "清水县",
            value: "620521"
          }, {
            label: "秦安县",
            value: "620522"
          }, {
            label: "甘谷县",
            value: "620523"
          }, {
            label: "武山县",
            value: "620524"
          }, {
            label: "张家川回族自治县",
            value: "620525"
          }],
          [{
            label: "凉州区",
            value: "620602"
          }, {
            label: "民勤县",
            value: "620621"
          }, {
            label: "古浪县",
            value: "620622"
          }, {
            label: "天祝藏族自治县",
            value: "620623"
          }],
          [{
            label: "甘州区",
            value: "620702"
          }, {
            label: "肃南裕固族自治县",
            value: "620721"
          }, {
            label: "民乐县",
            value: "620722"
          }, {
            label: "临泽县",
            value: "620723"
          }, {
            label: "高台县",
            value: "620724"
          }, {
            label: "山丹县",
            value: "620725"
          }],
          [{
            label: "崆峒区",
            value: "620802"
          }, {
            label: "泾川县",
            value: "620821"
          }, {
            label: "灵台县",
            value: "620822"
          }, {
            label: "崇信县",
            value: "620823"
          }, {
            label: "华亭县",
            value: "620824"
          }, {
            label: "庄浪县",
            value: "620825"
          }, {
            label: "静宁县",
            value: "620826"
          }, {
            label: "平凉工业园区",
            value: "620871"
          }],
          [{
            label: "肃州区",
            value: "620902"
          }, {
            label: "金塔县",
            value: "620921"
          }, {
            label: "瓜州县",
            value: "620922"
          }, {
            label: "肃北蒙古族自治县",
            value: "620923"
          }, {
            label: "阿克塞哈萨克族自治县",
            value: "620924"
          }, {
            label: "玉门市",
            value: "620981"
          }, {
            label: "敦煌市",
            value: "620982"
          }],
          [{
            label: "西峰区",
            value: "621002"
          }, {
            label: "庆城县",
            value: "621021"
          }, {
            label: "环县",
            value: "621022"
          }, {
            label: "华池县",
            value: "621023"
          }, {
            label: "合水县",
            value: "621024"
          }, {
            label: "正宁县",
            value: "621025"
          }, {
            label: "宁县",
            value: "621026"
          }, {
            label: "镇原县",
            value: "621027"
          }],
          [{
            label: "安定区",
            value: "621102"
          }, {
            label: "通渭县",
            value: "621121"
          }, {
            label: "陇西县",
            value: "621122"
          }, {
            label: "渭源县",
            value: "621123"
          }, {
            label: "临洮县",
            value: "621124"
          }, {
            label: "漳县",
            value: "621125"
          }, {
            label: "岷县",
            value: "621126"
          }],
          [{
            label: "武都区",
            value: "621202"
          }, {
            label: "成县",
            value: "621221"
          }, {
            label: "文县",
            value: "621222"
          }, {
            label: "宕昌县",
            value: "621223"
          }, {
            label: "康县",
            value: "621224"
          }, {
            label: "西和县",
            value: "621225"
          }, {
            label: "礼县",
            value: "621226"
          }, {
            label: "徽县",
            value: "621227"
          }, {
            label: "两当县",
            value: "621228"
          }],
          [{
            label: "临夏市",
            value: "622901"
          }, {
            label: "临夏县",
            value: "622921"
          }, {
            label: "康乐县",
            value: "622922"
          }, {
            label: "永靖县",
            value: "622923"
          }, {
            label: "广河县",
            value: "622924"
          }, {
            label: "和政县",
            value: "622925"
          }, {
            label: "东乡族自治县",
            value: "622926"
          }, {
            label: "积石山保安族东乡族撒拉族自治县",
            value: "622927"
          }],
          [{
            label: "合作市",
            value: "623001"
          }, {
            label: "临潭县",
            value: "623021"
          }, {
            label: "卓尼县",
            value: "623022"
          }, {
            label: "舟曲县",
            value: "623023"
          }, {
            label: "迭部县",
            value: "623024"
          }, {
            label: "玛曲县",
            value: "623025"
          }, {
            label: "碌曲县",
            value: "623026"
          }, {
            label: "夏河县",
            value: "623027"
          }]
        ],
        [
          [{
            label: "城东区",
            value: "630102"
          }, {
            label: "城中区",
            value: "630103"
          }, {
            label: "城西区",
            value: "630104"
          }, {
            label: "城北区",
            value: "630105"
          }, {
            label: "大通回族土族自治县",
            value: "630121"
          }, {
            label: "湟中县",
            value: "630122"
          }, {
            label: "湟源县",
            value: "630123"
          }],
          [{
            label: "乐都区",
            value: "630202"
          }, {
            label: "平安区",
            value: "630203"
          }, {
            label: "民和回族土族自治县",
            value: "630222"
          }, {
            label: "互助土族自治县",
            value: "630223"
          }, {
            label: "化隆回族自治县",
            value: "630224"
          }, {
            label: "循化撒拉族自治县",
            value: "630225"
          }],
          [{
            label: "门源回族自治县",
            value: "632221"
          }, {
            label: "祁连县",
            value: "632222"
          }, {
            label: "海晏县",
            value: "632223"
          }, {
            label: "刚察县",
            value: "632224"
          }],
          [{
            label: "同仁县",
            value: "632321"
          }, {
            label: "尖扎县",
            value: "632322"
          }, {
            label: "泽库县",
            value: "632323"
          }, {
            label: "河南蒙古族自治县",
            value: "632324"
          }],
          [{
            label: "共和县",
            value: "632521"
          }, {
            label: "同德县",
            value: "632522"
          }, {
            label: "贵德县",
            value: "632523"
          }, {
            label: "兴海县",
            value: "632524"
          }, {
            label: "贵南县",
            value: "632525"
          }],
          [{
            label: "玛沁县",
            value: "632621"
          }, {
            label: "班玛县",
            value: "632622"
          }, {
            label: "甘德县",
            value: "632623"
          }, {
            label: "达日县",
            value: "632624"
          }, {
            label: "久治县",
            value: "632625"
          }, {
            label: "玛多县",
            value: "632626"
          }],
          [{
            label: "玉树市",
            value: "632701"
          }, {
            label: "杂多县",
            value: "632722"
          }, {
            label: "称多县",
            value: "632723"
          }, {
            label: "治多县",
            value: "632724"
          }, {
            label: "囊谦县",
            value: "632725"
          }, {
            label: "曲麻莱县",
            value: "632726"
          }],
          [{
            label: "格尔木市",
            value: "632801"
          }, {
            label: "德令哈市",
            value: "632802"
          }, {
            label: "乌兰县",
            value: "632821"
          }, {
            label: "都兰县",
            value: "632822"
          }, {
            label: "天峻县",
            value: "632823"
          }, {
            label: "大柴旦行政委员会",
            value: "632857"
          }, {
            label: "冷湖行政委员会",
            value: "632858"
          }, {
            label: "茫崖行政委员会",
            value: "632859"
          }]
        ],
        [
          [{
            label: "兴庆区",
            value: "640104"
          }, {
            label: "西夏区",
            value: "640105"
          }, {
            label: "金凤区",
            value: "640106"
          }, {
            label: "永宁县",
            value: "640121"
          }, {
            label: "贺兰县",
            value: "640122"
          }, {
            label: "灵武市",
            value: "640181"
          }],
          [{
            label: "大武口区",
            value: "640202"
          }, {
            label: "惠农区",
            value: "640205"
          }, {
            label: "平罗县",
            value: "640221"
          }],
          [{
            label: "利通区",
            value: "640302"
          }, {
            label: "红寺堡区",
            value: "640303"
          }, {
            label: "盐池县",
            value: "640323"
          }, {
            label: "同心县",
            value: "640324"
          }, {
            label: "青铜峡市",
            value: "640381"
          }],
          [{
            label: "原州区",
            value: "640402"
          }, {
            label: "西吉县",
            value: "640422"
          }, {
            label: "隆德县",
            value: "640423"
          }, {
            label: "泾源县",
            value: "640424"
          }, {
            label: "彭阳县",
            value: "640425"
          }],
          [{
            label: "沙坡头区",
            value: "640502"
          }, {
            label: "中宁县",
            value: "640521"
          }, {
            label: "海原县",
            value: "640522"
          }]
        ],
        [
          [{
            label: "天山区",
            value: "650102"
          }, {
            label: "沙依巴克区",
            value: "650103"
          }, {
            label: "新市区",
            value: "650104"
          }, {
            label: "水磨沟区",
            value: "650105"
          }, {
            label: "头屯河区",
            value: "650106"
          }, {
            label: "达坂城区",
            value: "650107"
          }, {
            label: "米东区",
            value: "650109"
          }, {
            label: "乌鲁木齐县",
            value: "650121"
          }, {
            label: "乌鲁木齐经济技术开发区",
            value: "650171"
          }, {
            label: "乌鲁木齐高新技术产业开发区",
            value: "650172"
          }],
          [{
            label: "独山子区",
            value: "650202"
          }, {
            label: "克拉玛依区",
            value: "650203"
          }, {
            label: "白碱滩区",
            value: "650204"
          }, {
            label: "乌尔禾区",
            value: "650205"
          }],
          [{
            label: "高昌区",
            value: "650402"
          }, {
            label: "鄯善县",
            value: "650421"
          }, {
            label: "托克逊县",
            value: "650422"
          }],
          [{
            label: "伊州区",
            value: "650502"
          }, {
            label: "巴里坤哈萨克自治县",
            value: "650521"
          }, {
            label: "伊吾县",
            value: "650522"
          }],
          [{
            label: "昌吉市",
            value: "652301"
          }, {
            label: "阜康市",
            value: "652302"
          }, {
            label: "呼图壁县",
            value: "652323"
          }, {
            label: "玛纳斯县",
            value: "652324"
          }, {
            label: "奇台县",
            value: "652325"
          }, {
            label: "吉木萨尔县",
            value: "652327"
          }, {
            label: "木垒哈萨克自治县",
            value: "652328"
          }],
          [{
            label: "博乐市",
            value: "652701"
          }, {
            label: "阿拉山口市",
            value: "652702"
          }, {
            label: "精河县",
            value: "652722"
          }, {
            label: "温泉县",
            value: "652723"
          }],
          [{
            label: "库尔勒市",
            value: "652801"
          }, {
            label: "轮台县",
            value: "652822"
          }, {
            label: "尉犁县",
            value: "652823"
          }, {
            label: "若羌县",
            value: "652824"
          }, {
            label: "且末县",
            value: "652825"
          }, {
            label: "焉耆回族自治县",
            value: "652826"
          }, {
            label: "和静县",
            value: "652827"
          }, {
            label: "和硕县",
            value: "652828"
          }, {
            label: "博湖县",
            value: "652829"
          }, {
            label: "库尔勒经济技术开发区",
            value: "652871"
          }],
          [{
            label: "阿克苏市",
            value: "652901"
          }, {
            label: "温宿县",
            value: "652922"
          }, {
            label: "库车县",
            value: "652923"
          }, {
            label: "沙雅县",
            value: "652924"
          }, {
            label: "新和县",
            value: "652925"
          }, {
            label: "拜城县",
            value: "652926"
          }, {
            label: "乌什县",
            value: "652927"
          }, {
            label: "阿瓦提县",
            value: "652928"
          }, {
            label: "柯坪县",
            value: "652929"
          }],
          [{
            label: "阿图什市",
            value: "653001"
          }, {
            label: "阿克陶县",
            value: "653022"
          }, {
            label: "阿合奇县",
            value: "653023"
          }, {
            label: "乌恰县",
            value: "653024"
          }],
          [{
            label: "喀什市",
            value: "653101"
          }, {
            label: "疏附县",
            value: "653121"
          }, {
            label: "疏勒县",
            value: "653122"
          }, {
            label: "英吉沙县",
            value: "653123"
          }, {
            label: "泽普县",
            value: "653124"
          }, {
            label: "莎车县",
            value: "653125"
          }, {
            label: "叶城县",
            value: "653126"
          }, {
            label: "麦盖提县",
            value: "653127"
          }, {
            label: "岳普湖县",
            value: "653128"
          }, {
            label: "伽师县",
            value: "653129"
          }, {
            label: "巴楚县",
            value: "653130"
          }, {
            label: "塔什库尔干塔吉克自治县",
            value: "653131"
          }],
          [{
            label: "和田市",
            value: "653201"
          }, {
            label: "和田县",
            value: "653221"
          }, {
            label: "墨玉县",
            value: "653222"
          }, {
            label: "皮山县",
            value: "653223"
          }, {
            label: "洛浦县",
            value: "653224"
          }, {
            label: "策勒县",
            value: "653225"
          }, {
            label: "于田县",
            value: "653226"
          }, {
            label: "民丰县",
            value: "653227"
          }],
          [{
            label: "伊宁市",
            value: "654002"
          }, {
            label: "奎屯市",
            value: "654003"
          }, {
            label: "霍尔果斯市",
            value: "654004"
          }, {
            label: "伊宁县",
            value: "654021"
          }, {
            label: "察布查尔锡伯自治县",
            value: "654022"
          }, {
            label: "霍城县",
            value: "654023"
          }, {
            label: "巩留县",
            value: "654024"
          }, {
            label: "新源县",
            value: "654025"
          }, {
            label: "昭苏县",
            value: "654026"
          }, {
            label: "特克斯县",
            value: "654027"
          }, {
            label: "尼勒克县",
            value: "654028"
          }],
          [{
            label: "塔城市",
            value: "654201"
          }, {
            label: "乌苏市",
            value: "654202"
          }, {
            label: "额敏县",
            value: "654221"
          }, {
            label: "沙湾县",
            value: "654223"
          }, {
            label: "托里县",
            value: "654224"
          }, {
            label: "裕民县",
            value: "654225"
          }, {
            label: "和布克赛尔蒙古自治县",
            value: "654226"
          }],
          [{
            label: "阿勒泰市",
            value: "654301"
          }, {
            label: "布尔津县",
            value: "654321"
          }, {
            label: "富蕴县",
            value: "654322"
          }, {
            label: "福海县",
            value: "654323"
          }, {
            label: "哈巴河县",
            value: "654324"
          }, {
            label: "青河县",
            value: "654325"
          }, {
            label: "吉木乃县",
            value: "654326"
          }],
          [{
            label: "石河子市",
            value: "659001"
          }, {
            label: "阿拉尔市",
            value: "659002"
          }, {
            label: "图木舒克市",
            value: "659003"
          }, {
            label: "五家渠市",
            value: "659004"
          }, {
            label: "铁门关市",
            value: "659006"
          }]
        ],
        [
          [{
            label: "台北",
            value: "660101"
          }],
          [{
            label: "高雄",
            value: "660201"
          }],
          [{
            label: "基隆",
            value: "660301"
          }],
          [{
            label: "台中",
            value: "660401"
          }],
          [{
            label: "台南",
            value: "660501"
          }],
          [{
            label: "新竹",
            value: "660601"
          }],
          [{
            label: "嘉义",
            value: "660701"
          }],
          [{
            label: "宜兰",
            value: "660801"
          }],
          [{
            label: "桃园",
            value: "660901"
          }],
          [{
            label: "苗栗",
            value: "661001"
          }],
          [{
            label: "彰化",
            value: "661101"
          }],
          [{
            label: "南投",
            value: "661201"
          }],
          [{
            label: "云林",
            value: "661301"
          }],
          [{
            label: "屏东",
            value: "661401"
          }],
          [{
            label: "台东",
            value: "661501"
          }],
          [{
            label: "花莲",
            value: "661601"
          }],
          [{
            label: "澎湖",
            value: "661701"
          }]
        ],
        [
          [{
            label: "香港岛",
            value: "670101"
          }],
          [{
            label: "九龙",
            value: "670201"
          }],
          [{
            label: "新界",
            value: "670301"
          }]
        ],
        [
          [{
            label: "澳门半岛",
            value: "680101"
          }],
          [{
            label: "氹仔岛",
            value: "680201"
          }],
          [{
            label: "路环岛",
            value: "680301"
          }],
          [{
            label: "路氹城",
            value: "680401"
          }]
        ]
      ];
      l.default = t;
    },
    c8ba: function (e, l) {
      var a;
      a = function () {
        return this;
      }();
      try {
        a = a || new Function("return this")();
      } catch (e) {
        "object" === ("undefined" == typeof window ? "undefined" : _typeof(window)) && (a = window);
      }
      e.exports = a;
    },
    f571: function (l, e, a) {
      (function (o) {
        var e = a("3814");
        if ("wq" == e.platform) {
          var r = e.url;
          r = "https://" + r.match(/[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+\.?/)[0], r += "/addons/worldidc_cloud/core/public/index.php/api/Wxapps/";
        } else r = e.url;
        var v = e.uniacid,
          c = {
            getOpenid: function (a) {
              var t = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : "",
                i = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : "",
                u = 3 < arguments.length && void 0 !== arguments[3] ? arguments[3] : "";
              o.getStorage({
                key: "openid",
                success: function (e) {
                  o.setStorageSync("source", 1), o.request({
                    url: r + "dopageglobaluserinfo",
                    data: {
                      openid: e.data,
                      uniacid: v,
                      source: 1
                    },
                    success: function (e) {
                      var l = e.data.data;
                      l.nickname && l.avatar ? (o.setStorageSync("golobeuid", l.id), o.setStorageSync("golobeuser", l), o.getStorage({
                        key: "suid",
                        success: function (e) {
                          c.fxsbind(e.data, a);
                        },
                        fail: function () {
                          console.log(l.openid), o.request({
                            url: r + "dopagehasBind",
                            data: {
                              uniacid: v,
                              identity: l.openid,
                              status: 1
                            },
                            success: function (e) {
                              "1" == e.data.data && e.data.suid ? (o.setStorageSync("suid", e.data.suid), c.fxsbind(e.data.suid, a)) : "2" == e.data.data && "function" == typeof u && u();
                            }
                          });
                        }
                      })) : "function" == typeof i && i(), "function" == typeof t && t();
                    }
                  });
                },
                fail: function () {
                  o.login({
                    provider: "weixin",
                    success: function (e) {
                      o.request({
                        url: r + "doPageAppbase",
                        data: {
                          code: e.code,
                          uniacid: v
                        },
                        success: function (e) {
                          if (console.log("微信小程序初次登陆"), 2 == e.data.data.res) o.showModal({
                            title: "提醒",
                            content: "获取用户信息失败，请检查appid和appsecret是否正确！",
                            showCancel: !1
                          });
                          else {
                            o.setStorageSync("source", 1);
                            var l = e.data.data;
                            o.setStorageSync("openid", l.openid), l.nickname && l.avatar ? (o.setStorageSync("golobeuid", l.id), o.setStorageSync("golobeuser", l), o.getStorage({
                              key: "suid",
                              success: function (e) {
                                c.fxsbind(e.data, a);
                              },
                              fail: function () {
                                o.request({
                                  url: r + "dopagehasBind",
                                  data: {
                                    uniacid: v,
                                    identity: l.openid,
                                    status: 1
                                  },
                                  success: function (e) {
                                    "1" == e.data.data && e.data.suid ? (o.setStorageSync("suid", e.data.suid), c.fxsbind(e.data.suid, a)) : "2" == e.data.data && "function" == typeof u && u();
                                  }
                                });
                              }
                            })) : "function" == typeof i && i(), "function" == typeof t && t();
                          }
                        }
                      });
                    }
                  });
                }
              });
            },
            qqLogin: function (a) {
              var t = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : "",
                i = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : "",
                u = 3 < arguments.length && void 0 !== arguments[3] ? arguments[3] : "",
                n = function () {
                  o.login({
                    provider: "QQ",
                    success: function (e) {
                      e.code && o.request({
                        url: r + "dopageqqlogin",
                        data: {
                          code: e.code,
                          uniacid: v
                        },
                        success: function (e) {
                          if (console.log(e), 2 == e.data.data.res) return o.showModal({
                            title: "提示",
                            content: "登录失败，请检查小程序设置是否正确",
                            showCancel: !1
                          }), !1;
                          console.log("QQ小程序初次登陆"), o.setStorageSync("source", 6), o.setStorageSync("suid", e.data.data.suid);
                          var l = e.data.data;
                          o.setStorageSync("qq_uid", l.id), o.setStorageSync("qq_openid", l.openid), e.data.data.nickname && e.data.data.avatar ? (o.setStorageSync("qq_userinfo", l), c.fxsbind(e.data.data.suid, a), o.getStorage({
                            key: "suid",
                            success: function (e) {
                              e.data ? c.fxsbind(e.data, 19) : (console.log("shit"), o.request({
                                url: r + "dopagehasBind",
                                data: {
                                  uniacid: v,
                                  identity: l.openid,
                                  status: 6
                                },
                                success: function (e) {
                                  "1" == e.data.data && e.data.suid ? (o.setStorageSync("suid", e.data.suid), c.fxsbind(e.data.suid, a)) : "2" == e.data.data && "function" == typeof u && u();
                                }
                              }));
                            }
                          })) : "function" == typeof i && i(), "function" == typeof t && t();
                        }
                      });
                    },
                    fail: function (e) {
                      console.log("拒绝授权"), "function" == typeof t && t();
                    }
                  });
                };
              o.getStorage({
                key: "qq_userinfo",
                success: function (e) {
                  var l = e.data;
                  l ? (o.setStorageSync("source", 6), o.getStorage({
                    key: "suid",
                    success: function (e) {
                      e.data ? c.fxsbind(e.data, a) : (console.log("shit"), o.request({
                        url: r + "dopagehasBind",
                        data: {
                          uniacid: v,
                          identity: l.user_id,
                          status: 6
                        },
                        success: function (e) {
                          console.log(e), "1" == e.data.data && e.data.suid ? (o.setStorageSync("suid", e.data.suid), c.fxsbind(e.data.suid, a)) : "2" == e.data.data && "function" == typeof u && u();
                        }
                      }));
                    }
                  }), "function" == typeof t && t()) : n();
                },
                fail: function (e) {
                  n(), console.log("不在缓存中");
                }
              });
            },
            bezier: function (e, l) {
              for (var a, t, i, u = [], n = 0; n <= l; n++) {
                for (i = e.slice(0), t = []; a = i.shift();)
                  if (i.length) t.push((o = [a, i[0]], r = n / l, h = f = p = d = s = b = c = v = void 0, [], v = o[0], c = o[1], s = c.x - v.x, d = c.y - v.y, b = Math.pow(Math.pow(s, 2) + Math.pow(d, 2), .5), p = d / s, f = Math.atan(p), h = b * r, {
                    x: v.x + h * Math.cos(f),
                    y: v.y + h * Math.sin(f)
                  }));
                  else {
                    if (!(1 < t.length)) break;
                    i = t, t = [];
                  }
                u.push(t[0]);
              }
              var o, r, v, c, b, s, d, p, f, h;
              return {
                bezier_points: u
              };
            },
            fxsbind: function (e, l) {
              console.log("进绑定分销商"), console.log("suid:" + e), console.log("fxsid:" + l), l && l != e ? o.request({
                url: r + "doPagebindfxs",
                data: {
                  suid: e,
                  fxsid: l,
                  uniacid: v
                },
                success: function (e) {
                  console.log("绑定分销商:" + e.data.data);
                }
              }) : (l = o.getStorageSync("fxsid")) && "0" != l ? o.request({
                url: r + "doPagebindfxs",
                data: {
                  suid: e,
                  fxsid: l,
                  uniacid: v
                },
                success: function (e) {
                  console.log("绑定分销商-缓存:" + e.data.data);
                }
              }) : 0 !== l && o.setStorageSync("fxsid", 0);
            },
            authAgain: function (l) {
              var a = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : "";
              o.showModal({
                title: "提示",
                content: "必须授权后才能操作,是否重新授权？",
                showCancel: !0,
                success: function (e) {
                  e.confirm && o.openSetting({
                    success: function () {
                      o.getSetting({
                        success: function (e) {
                          e.authSetting["scope." + l] ? "function" == typeof a && a() : c.authAgain(l, a);
                        }
                      });
                    }
                  });
                }
              });
            },
            getUserid: function (a) {
              var t = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : "",
                i = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : "";
              o.getStorage({
                key: "ali_userinfo",
                success: function (e) {
                  var l = e.data;
                  l ? (o.setStorageSync("source", 2), o.getStorage({
                    key: "suid",
                    success: function (e) {
                      e.data ? c.fxsbind(e.data, a) : (console.log("shit"), o.request({
                        url: r + "dopagehasBind",
                        data: {
                          uniacid: v,
                          identity: l.user_id,
                          status: 2
                        },
                        success: function (e) {
                          console.log(e), "1" == e.data.data && e.data.suid ? (o.setStorageSync("suid", e.data.suid), c.fxsbind(e.data.suid, a)) : "2" == e.data.data && "function" == typeof i && i();
                        }
                      }));
                    }
                  }), "function" == typeof t && t()) : o.login({
                    provider: "alipay",
                    scopes: "auth_user",
                    success: function (e) {
                      e.authCode && o.request({
                        url: r + "dopagealipaylogin",
                        data: {
                          authCode: e.authCode,
                          uniacid: v
                        },
                        success: function (e) {
                          console.log("支付宝小程序初次登陆"), o.setStorageSync("source", 2), o.setStorageSync("suid", e.data.suid);
                          var l = e.data;
                          e.data.nick_name && e.data.avatar ? (o.setStorageSync("ali_uid", l.id), o.setStorageSync("ali_user_id", l.user_id), o.setStorageSync("ali_userinfo", l), "function" == typeof t && t()) : o.getUserInfo({
                            success: function (e) {
                              l.nick_name = e.nickName, l.avatar = e.avatar, o.request({
                                url: r + "dopagealiUpdateNickname",
                                data: {
                                  avatar: e.avatar,
                                  nick_name: e.nickName,
                                  uniacid: v,
                                  id: l.id
                                },
                                success: function (e) {
                                  o.setStorageSync("ali_uid", l.id), o.setStorageSync("ali_user_id", l.user_id), o.setStorageSync("ali_userinfo", l), "function" == typeof t && t();
                                }
                              });
                            }
                          }), o.getStorage({
                            key: "suid",
                            success: function (e) {
                              e.data ? c.fxsbind(e.data, a) : (console.log("shit"), o.request({
                                url: r + "dopagehasBind",
                                data: {
                                  uniacid: v,
                                  identity: l.user_id,
                                  status: 2
                                },
                                success: function (e) {
                                  console.log(e), "1" == e.data.data && e.data.suid ? (o.setStorageSync("suid", e.data.suid), c.fxsbind(e.data.suid, a)) : "2" == e.data.data && "function" == typeof i && i();
                                }
                              }));
                            }
                          });
                        }
                      });
                    },
                    fail: function (e) {
                      console.log(e), console.log("拒绝授权"), "function" == typeof t && t();
                    }
                  });
                },
                fail: function (e) {
                  console.log("不在缓存中");
                }
              });
            },
            toutiaoLogin: function (a) {
              var t = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : "",
                i = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : "";
              o.getProvider({
                service: "oauth",
                success: function (e) {
                  console.log(e.provider[0]), o.setStorageSync("provider", e.provider[0]);
                }
              });
              var u = function () {
                o.login({
                  provider: "toutiao",
                  success: function (e) {
                    e.code && (console.log(e), o.request({
                      url: r + "dopagetoutiaologin",
                      data: {
                        code: e.code,
                        anonymous_code: e.anonymousCode,
                        uniacid: v
                      },
                      success: function (e) {
                        if (console.log(e), 2 == e.data.data.res) return o.showModal({
                          title: "提示",
                          content: "登录失败，请检查小程序设置是否正确",
                          showCancel: !1
                        }), !1;
                        console.log("头条小程序初次登陆"), o.setStorageSync("source", 5), o.setStorageSync("suid", e.data.data.suid);
                        var l = e.data.data;
                        e.data.data.nickname && e.data.data.avatar ? (o.setStorageSync("toutiao_uid", l.id), o.setStorageSync("toutiao_openid", l.openid), o.setStorageSync("toutiao_userinfo", l), "function" == typeof t && t()) : o.getUserInfo({
                          success: function (e) {
                            l.nickname = e.userInfo.nickName, l.avatar = e.userInfo.avatarUrl, o.request({
                              url: r + "dopagetoutiaoUpdateNickname",
                              data: {
                                avatar: e.userInfo.avatarUrl,
                                nickname: e.userInfo.nickName,
                                uniacid: v,
                                id: l.id
                              },
                              success: function (e) {
                                o.setStorageSync("toutiao_uid", l.id), o.setStorageSync("toutiao_openid", l.openid), o.setStorageSync("toutiao_userinfo", l), "function" == typeof t && t();
                              }
                            });
                          },
                          fail: function (e) {
                            console.log(e);
                          }
                        }), o.getStorage({
                          key: "suid",
                          success: function (e) {
                            e.data ? c.fxsbind(e.data, a) : (console.log("shit"), o.request({
                              url: r + "dopagehasBind",
                              data: {
                                uniacid: v,
                                identity: l.openid,
                                status: 5
                              },
                              success: function (e) {
                                "1" == e.data.data && e.data.suid ? (o.setStorageSync("suid", e.data.suid), c.fxsbind(e.data.suid, a)) : "2" == e.data.data && "function" == typeof i && i();
                              }
                            }));
                          }
                        });
                      }
                    }));
                  },
                  fail: function (e) {
                    console.log("拒绝授权"), "function" == typeof t && t();
                  }
                });
              };
              o.getStorage({
                key: "toutiao_userinfo",
                success: function (e) {
                  var l = e.data;
                  l ? (o.setStorageSync("source", 5), o.getStorage({
                    key: "suid",
                    success: function (e) {
                      e.data ? c.fxsbind(e.data, a) : (console.log("shit"), o.request({
                        url: r + "dopagehasBind",
                        data: {
                          uniacid: v,
                          identity: l.user_id,
                          status: 5
                        },
                        success: function (e) {
                          console.log(e), "1" == e.data.data && e.data.suid ? (o.setStorageSync("suid", e.data.suid), c.fxsbind(e.data.suid, a)) : "2" == e.data.data && "function" == typeof i && i();
                        }
                      }));
                    }
                  }), "function" == typeof t && t()) : u();
                },
                fail: function (e) {
                  u(), console.log("不在缓存中");
                }
              });
            },
            h5login: function (a) {
              var e = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : "",
                t = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : "";
              o.getStorage({
                key: "suid",
                success: function (e) {
                  var l = e.data;
                  o.setStorageSync("source", 3), o.request({
                    url: r + "dopagecheckuniacid",
                    data: {
                      suid: l,
                      uniacid: o.getStorageSync("uniacid")
                    },
                    success: function (e) {
                      0 == e.data.data ? "function" == typeof t && t() : c.fxsbind(l, a);
                    }
                  });
                },
                fail: function () {
                  "function" == typeof t && t();
                },
                complete: function () {
                  "function" == typeof e && e();
                }
              });
            },
            bdLogin: function (l) {
              var t = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : "",
                i = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : "",
                a = function () {
                  o.login({
                    provider: "baidu",
                    success: function (e) {
                      o.request({
                        url: r + "doPageBdAppbase",
                        data: {
                          code: e.code,
                          uniacid: v
                        },
                        success: function (e) {
                          if (console.log("支付宝小程序初次登陆"), console.log(e), 2 == e.data.data.res) o.showModal({
                            title: "提醒",
                            content: "获取用户信息失败，请检查appid和appsecret是否正确！",
                            showCancel: !1
                          });
                          else {
                            o.setStorageSync("source", 4);
                            var a = e.data.data;
                            o.setStorageSync("openid", a.openid), console.log(a), a.nickname && a.avatar ? (o.setStorageSync("baidu_userinfo", a), "function" == typeof t && t()) : swan.authorize({
                              scope: "scope.userInfo",
                              success: function (e) {
                                swan.getUserInfo({
                                  success: function (l) {
                                    l.is_anonymous || o.request({
                                      url: r + "dopageBaiDuUpdate",
                                      data: {
                                        uniacid: v,
                                        openid: a.openid,
                                        avatar: l.userInfo.avatarUrl,
                                        nickName: l.userInfo.nickName,
                                        gender: l.userInfo.gender
                                      },
                                      success: function (e) {
                                        a.avatar = l.userInfo.avatarUrl, a.nickname = l.userInfo.nickName, e.data && o.setStorageSync("baidu_userinfo", a);
                                      }
                                    }), "function" == typeof t && t();
                                  }
                                });
                              }
                            }), o.getStorage({
                              key: "suid",
                              success: function (e) {
                                e.data ? c.fxsbind(e.data, l) : o.request({
                                  url: r + "dopagehasBind",
                                  data: {
                                    uniacid: v,
                                    identity: a.openid,
                                    status: 4
                                  },
                                  success: function (e) {
                                    "1" == e.data.data && e.data.suid ? (o.setStorageSync("suid", e.data.suid), c.fxsbind(e.data.suid, l)) : "2" == e.data.data && "function" == typeof i && i();
                                  }
                                });
                              },
                              fail: function () {}
                            });
                          }
                        }
                      });
                    }
                  });
                };
              o.getStorage({
                key: "openid",
                success: function (e) {
                  e.data ? (o.setStorageSync("source", 4), o.request({
                    url: r + "dopageglobaluserinfo",
                    data: {
                      openid: e.data,
                      uniacid: v,
                      source: 4
                    },
                    success: function (e) {
                      var a = e.data.data;
                      a.nickname && a.avatar ? (o.setStorageSync("baidu_userinfo", a), o.getStorage({
                        key: "suid",
                        success: function (e) {
                          e.data ? c.fxsbind(e.data, l) : o.request({
                            url: r + "dopagehasBind",
                            data: {
                              uniacid: v,
                              identity: a.openid,
                              status: 4
                            },
                            success: function (e) {
                              "1" == e.data.data && e.data.suid ? (o.setStorageSync("suid", e.data.suid), c.fxsbind(e.data.suid, l)) : "2" == e.data.data && "function" == typeof i && i();
                            }
                          });
                        },
                        fail: function () {
                          console.log(a.openid);
                        }
                      })) : swan.authorize({
                        scope: "scope.userInfo",
                        success: function (e) {
                          swan.getUserInfo({
                            success: function (e) {
                              console.log(12121212), console.log(e), e.is_anonymous ? swan.openSetting({
                                success: function (e) {
                                  console.log(e.authSetting["scope.userInfo"]), e.authSetting["scope.userInfo"] && swan.getUserInfo({
                                    success: function (l) {
                                      o.request({
                                        url: r + "dopageBaiDuUpdate",
                                        data: {
                                          uniacid: v,
                                          openid: a.openid,
                                          avatar: l.userInfo.avatarUrl,
                                          nickName: l.userInfo.nickName,
                                          gender: l.userInfo.gender
                                        },
                                        success: function (e) {
                                          userInfo.avatar = l.userInfo.avatarUrl, userInfo.nickname = l.userInfo.nickName, e.data && o.setStorageSync("baidu_userinfo", userInfo);
                                        }
                                      });
                                    }
                                  });
                                }
                              }) : o.request({
                                url: r + "dopageBaiDuUpdate",
                                data: {
                                  avatar: e.userInfo.avatarUrl,
                                  nickName: e.userInfo.nickName,
                                  gender: e.userInfo.gender,
                                  uniacid: v,
                                  openid: a.openid
                                },
                                success: function (e) {
                                  e && o.setStorageSync("baidu_userinfo", e.data);
                                }
                              }), "function" == typeof t && t();
                            }
                          });
                        }
                      }), "function" == typeof t && t();
                    }
                  })) : a();
                },
                fail: function () {
                  a();
                }
              });
            },
            throttle: function (l, a) {
              null != a && null != a || (a = 1500);
              var t = null;
              return function () {
                var e = +new Date();
                (a < e - t || !t) && (l.apply(this, arguments), t = e);
              };
            },
            getWxAuth: function (a, t, i) {
              console.log(a,t,i,'getWxAuth')
              if(a.nickname == ''){
                wx.showToast({
                  title: '昵称不能为空',
                  icon: 'none'
                })
                return
              }
              if(a.avatarUrl == ''){
                wx.showToast({
                  title: '头像不能为空',
                  icon: 'none'
                })
                return
              }
              var u = o.getStorageSync("openid");
              o.getUserInfo({
                success: function (e) {
                  var l = e.userInfo;
                  o.request({
                    url: a.$baseurl + "doPageUseupdate",
                    data: {
                      uniacid: a.$uniacid,
                      openid: u,
                      // nickname: l.nickName,
                      // avatarUrl: l.avatarUrl,
                      nickname: a.nickname,
                      avatarUrl: a.avatarUrl,
                      gender: l.gender,
                      province: l.province,
                      city: l.city,
                      country: l.country
                    },
                    success: function (e) {
                      // o.setStorageSync("golobeuid", e.data.data.id), o.setStorageSync("suid1", e.data.data.suid),o.setStorageSync("golobeuser1", e.data.data), t && "function" == typeof i && i();
                      o.setStorageSync("golobeuid", e.data.data.id),o.setStorageSync("golobeuser", e.data.data), t && "function" == typeof i && i();
                    }
                  });
                },
                fail: function (e) {
                  c.authAgain("userInfo", function () {
                    o.getUserInfo({
                      success: function (e) {
                        var l = e.userInfo;
                        o.request({
                          url: a.$baseurl + "doPageUseupdate",
                          data: {
                            uniacid: a.$uniacid,
                            openid: u,
                            nickname: l.nickName,
                            avatarUrl: l.avatarUrl,
                            gender: l.gender,
                            province: l.province,
                            city: l.city,
                            country: l.country
                          },
                          success: function (e) {
                            o.setStorageSync("golobeuid", e.data.data.id), o.setStorageSync("golobeuser", e.data.data), t && "function" == typeof i && i();
                          }
                        });
                      }
                    });
                  });
                }
              });
            }
          };
        l.exports = c;
      }).call(this, a("543d").default);
    }
  }
]);