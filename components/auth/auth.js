var config = require('../../siteinfo');
(global.webpackJsonp = global.webpackJsonp || []).push([
  ["components/auth/auth"], {
    1591: function (e, t, o) {
      var n = function () {
          this.$createElement;
          this._self._c;
        },
        a = [];
      o.d(t, "a", function () {
        return n;
      }), o.d(t, "b", function () {
        return a;
      });
    },
    5786: function (e, o, n) {
      (function (s) {
        Object.defineProperty(o, "__esModule", {
          value: !0
        }), o.default = void 0;
        var t = n("f571"),
          e = {
            props: ["needAuth"],
            data: function () {
              return {
                $imgurl: this.$imgurl,
                avatar: '',
                nickname: '',
                avatarUrl: '',

              };
            },
            created: function () {
              console.log("wx_auth");
            },
            methods: {
              // 昵称
              getNickname(e) {
                console.log(e.detail.value, 'getNickname')
                this.nickname =  e.detail.value
              },
              onChooseAvatar(e) {
                console.log(e.detail.avatarUrl, 'onChooseAvatar')
                this.avatar =  e.detail.avatarUrl
                var _this = this;
                wx.uploadFile({
                  url: config.siteroot + 'wxupimg?uniacid=' + config.uniacid, // 后台接口
                  filePath: e.detail.avatarUrl, // 上传图片 url
                  name: 'file',
                  header: {}, // header 值
                  success: res => {
                    console.log(res.data, 'dkdkdkdkkdkd')
                    _this.avatarUrl = res.data
                    // _this.setData({
                    //   avatarUrl: res.data
                    // })
                  },
                  fail: e => {

                  }
                });
              },
              login_close: function () {
                this.needAuth = !1, this.$emit("cell");
              },
              aliDoLogin: function () {
                console.log("ali_login");
                var o = this.$uniacid,
                  n = this.$baseurl,
                  a = this;
                s.login({
                  provider: "alipay",
                  scopes: "auth_user",
                  success: function (e) {
                    e.authCode && s.request({
                      url: n + "dopagealipaylogin",
                      data: {
                        authCode: e.authCode,
                        uniacid: o
                      },
                      success: function (e) {
                        console.log("支付宝小程序初次登陆"), s.setStorageSync("source", 2), s.setStorageSync("suid", e.data.suid);
                        var t = e.data;
                        e.data.nick_name && e.data.avatar ? (s.setStorageSync("ali_uid", t.id), s.setStorageSync("ali_user_id", t.user_id),
                          s.setStorageSync("ali_userinfo", t), t.suid ? (s.setStorageSync("suid", t.suid),
                            a.$emit("cell")) : a.$emit("closeAuth")) : s.getUserInfo({
                          success: function (e) {
                            t.nick_name = e.nickName, t.avatar = e.avatar, s.request({
                              url: n + "dopagealiUpdateNickname",
                              data: {
                                avatar: e.avatar,
                                nick_name: e.nickName,
                                uniacid: o,
                                id: t.id
                              },
                              success: function (e) {
                                s.setStorageSync("ali_uid", t.id), s.setStorageSync("ali_user_id", t.user_id), s.setStorageSync("ali_userinfo", t),
                                  a.$emit("closeAuth");
                              }
                            });
                          }
                        });
                      }
                    });
                  },
                  fail: function (e) {
                    console.log(e), console.log("拒绝授权"), a.$emit("cell");
                  }
                });
              },
              bDDoLogin: function () {
                var n = this.$uniacid,
                  a = this.$baseurl,
                  i = this;
                s.login({
                  provider: "baidu",
                  success: function (e) {
                    s.request({
                      url: a + "doPageBdAppbase",
                      data: {
                        code: e.code,
                        uniacid: n
                      },
                      success: function (e) {
                        if (console.log("百度小程序初次登陆"), console.log(e), 2 == e.data.data.res) s.showModal({
                          title: "提醒",
                          content: "获取用户信息失败，请检查appid和appsecret是否正确！",
                          showCancel: !1
                        });
                        else {
                          s.setStorageSync("source", 4);
                          var o = e.data.data;
                          s.setStorageSync("openid", o.openid), console.log(o), o.nickname && o.avatar ? (s.setStorageSync("baidu_userinfo", o),
                            o.suid ? (s.setStorageSync("suid", o.suid), i.$emit("cell")) : i.$emit("closeAuth")) : swan.authorize({
                            scope: "scope.userInfo",
                            success: function (e) {
                              swan.getUserInfo({
                                success: function (t) {
                                  console.log(t), t.is_anonymous || swan.request({
                                    url: a + "dopageBaiDuUpdate",
                                    data: {
                                      uniacid: n,
                                      openid: o.openid,
                                      avatar: t.userInfo.avatarUrl,
                                      nickName: t.userInfo.nickName,
                                      gender: t.userInfo.gender
                                    },
                                    success: function (e) {
                                      o.avatar = t.userInfo.avatarUrl, o.nickname = t.userInfo.nickName, e.data && s.setStorageSync("baidu_userinfo", o),
                                        i.$emit("closeAuth");
                                    }
                                  });
                                }
                              });
                            },
                            fail: function (e) {
                              swan.showModal({
                                title: "提示",
                                content: "必须授权后才能操作,是否重新授权？",
                                showCancel: !0,
                                success: function (e) {
                                  e.confirm ? swan.openSetting({
                                    success: function (e) {
                                      i.$emit("closeAuth");
                                    }
                                  }) : i.$emit("cell");
                                }
                              });
                            }
                          });
                        }
                      }
                    });
                  }
                });
              },
              h5DoLogin: function () {
                this.needAuth = !1, this.$emit("closeAuth");
              },
              getTUserInfo: function () {
                var o = this;
                s.getProvider({
                  service: "oauth",
                  success: function (e) {
                    console.log(e.provider[0]), s.setStorageSync("provider", e.provider[0]);
                  }
                }), s.login({
                  provider: "toutiao",
                  success: function (e) {
                    e.code && (console.log(e), s.request({
                      url: o.$baseurl + "dopagetoutiaologin",
                      data: {
                        code: e.code,
                        anonymous_code: e.anonymousCode,
                        uniacid: o.$uniacid
                      },
                      success: function (e) {
                        if (console.log(e), 2 == e.data.data.res) return s.showModal({
                          title: "提示",
                          content: "登录失败，请检查小程序设置是否正确",
                          showCancel: !1
                        }), !1;
                        console.log("头条小程序初次登陆"), s.setStorageSync("source", 5), s.setStorageSync("suid", e.data.data.suid);
                        var t = e.data.data;
                        e.data.data.nickname && e.data.data.avatar ? (s.setStorageSync("toutiao_uid", t.id),
                          s.setStorageSync("toutiao_openid", t.openid), s.setStorageSync("toutiao_userinfo", t),
                          t.suid ? (s.setStorageSync("suid", t.suid), o.$emit("cell")) : o.$emit("closeAuth")) : s.getUserInfo({
                          success: function (e) {
                            t.nickname = e.userInfo.nickName, t.avatar = e.userInfo.avatarUrl, s.request({
                              url: o.$baseurl + "dopagetoutiaoUpdateNickname",
                              data: {
                                avatar: e.userInfo.avatarUrl,
                                nickname: e.userInfo.nickName,
                                uniacid: o.$uniacid,
                                id: t.id
                              },
                              success: function (e) {
                                s.setStorageSync("toutiao_uid", t.id), s.setStorageSync("toutiao_openid", t.openid),
                                  s.setStorageSync("toutiao_userinfo", t), o.$emit("closeAuth");
                              }
                            });
                          },
                          fail: function (e) {
                            s.openSetting({
                              success: function (e) {
                                console.log(e), 1 == e.authSetting.scope.userInfo ? (console.log(e), o.$emit("closeAuth")) : console.log(e);
                              }
                            });
                          }
                        });
                      }
                    }));
                  },
                  fail: function (e) {
                    console.log("拒绝授权"), "function" == typeof cb && cb();
                  }
                });
              },
              getUserInfo: function () {
                var e = this;
                t.getWxAuth(this, this.needAuth, function () {
                  e.needAuth && e.$emit("closeAuth");
                });
              }
            }
          };
        o.default = e;
      }).call(this, n("543d").default);
    },
    "5fe6": function (e, t, o) {
      o.r(t);
      var n = o("5786"),
        a = o.n(n);
      for (var i in n) "default" !== i && function (e) {
        o.d(t, e, function () {
          return n[e];
        });
      }(i);
      t.default = a.a;
    },
    6341: function (e, t, o) {
      var n = o("a988");
      o.n(n).a;
    },
    "82fa": function (e, t, o) {
      o.r(t);
      var n = o("1591"),
        a = o("5fe6");
      for (var i in a) "default" !== i && function (e) {
        o.d(t, e, function () {
          return a[e];
        });
      }(i);
      o("6341");
      var s = o("2877"),
        c = Object(s.a)(a.default, n.a, n.b, !1, null, null, null);
      t.default = c.exports;
    },
    a988: function (e, t, o) {}
  }
]), (global.webpackJsonp = global.webpackJsonp || []).push(["components/auth/auth-create-component", {
    "components/auth/auth-create-component": function (e, t, o) {
      o("543d").createComponent(o("82fa"));
    }
  },
  [
    ["components/auth/auth-create-component"]
  ]
]);