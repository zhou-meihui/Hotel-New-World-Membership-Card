(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/bindPhone/bindPhone" ], {
    "55e0": function(e, n, t) {},
    "5c50": function(e, n, t) {
        var o = function() {
            this.$createElement;
            this._self._c;
        }, s = [];
        t.d(n, "a", function() {
            return o;
        }), t.d(n, "b", function() {
            return s;
        });
    },
    "651a": function(e, n, t) {
        t.r(n);
        var o = t("5c50"), s = t("87b3");
        for (var i in s) "default" !== i && function(e) {
            t.d(n, e, function() {
                return s[e];
            });
        }(i);
        t("7e64");
        var a = t("2877"), c = Object(a.a)(s.default, o.a, o.b, !1, null, null, null);
        n.default = c.exports;
    },
    "7e64": function(e, n, t) {
        var o = t("55e0");
        t.n(o).a;
    },
    "87b3": function(e, n, t) {
        t.r(n);
        var o = t("d8d3"), s = t.n(o);
        for (var i in o) "default" !== i && function(e) {
            t.d(n, e, function() {
                return o[e];
            });
        }(i);
        n.default = s.a;
    },
    d8d3: function(e, n, o) {
        (function(i) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var t = o("f571"), e = {
                data: function() {
                    return {
                        phone: "",
                        code: "",
                        str: "发送验证码",
                        btn_flag: !1,
                        baseinfo: "",
                        isshow: !0,
                        newSessionKey: ""
                    };
                },
                props: [ "needBind" ],
                created: function() {
                    this._baseMin(this), this.refreshSessionkey();
                    var e = i.getStorageSync("isshow");
                    console.log(e), this.isshow = 1 == e, console.log(this.isshow);
                    // console.log(e), this.isshow = true, console.log(this.isshow);
                },
                methods: {
                    refreshSessionkey: function() {
                        var n = this;
                        i.login({
                            success: function(e) {
                                i.request({
                                    url: n.$baseurl + "doPagegetNewSessionkey",
                                    data: {
                                        uniacid: n.$uniacid,
                                        code: e.code
                                    },
                                    success: function(e) {
                                        n.newSessionKey = e.data.data;
                                    }
                                });
                            }
                        });
                    },
                    sendCode: function() {
                        var e = this;
                        if (/^1[3456789]{1}\d{9}$/.test(this.phone)) {
                            var n, t = 60;
                            this.btn_flag = !0, this.str = "60s", n = setInterval(function() {
                                t--, e.str = t + "s", t <= 0 && (clearInterval(n), e.btn_flag = !1, e.str = "发送验证码");
                            }, 1e3), i.request({
                                url: this.$baseurl + "dopagebindPhone",
                                data: {
                                    phone: this.phone,
                                    uniacid: this.$uniacid
                                },
                                success: function(e) {
                                    console.log(e), "1" == e.data.data ? i.showModal({
                                        title: "发送失败",
                                        content: "参数设置有误",
                                        showCancel: !1
                                    }) : "2" == e.data.data ? i.showModal({
                                        title: "发送失败",
                                        content: "请您输入正确的手机号码",
                                        showCancel: !1
                                    }) : "3" == e.data.data && i.showModal({
                                        title: "发送失败",
                                        content: "距上一次发送不足60s",
                                        showCancel: !1
                                    });
                                }
                            });
                        } else i.showModal({
                            title: "提醒",
                            content: "请您输入正确的手机号码",
                            showCancel: !1
                        });
                    },
                    getPhoneNumber: function(e) {
                        var n = this, t = this, o = e.detail.iv, s = e.detail.encryptedData;
                        "getPhoneNumber:ok" == e.detail.errMsg ? i.checkSession({
                            success: function() {
                                i.request({
                                    url: t.$baseurl + "doPagejiemiNew",
                                    data: {
                                        uniacid: t.$uniacid,
                                        newSessionKey: t.newSessionKey,
                                        iv: o,
                                        encryptedData: s
                                    },
                                    success: function(e) {
                                        e.data.data ? (n.phone = e.data.data, n.confirmBind()) : i.showModal({
                                            title: "提示",
                                            content: "sessionKey已过期，请下拉刷新！"
                                        });
                                    },
                                    fail: function(e) {
                                        console.log(e);
                                    }
                                });
                            },
                            fail: function() {
                                i.showModal({
                                    title: "提示",
                                    content: "sessionKey已过期，请下拉刷新！"
                                });
                            }
                        }) : i.showModal({
                            title: "提示",
                            content: "请先授权获取您的手机号！",
                            showCancel: !1
                        });
                    },
                    confirmBind: function() {
                        var e, n = this;
                        e = i.getStorageSync("openid"), i.request({
                            url: this.$baseurl + "dopageconfirmBind",
                            data: {
                                phone: this.phone,
                                code: this.code,
                                uniacid: this.$uniacid,
                                status: 1,
                                identity: e,
                                sms: this.baseinfo.sms
                            },
                            success: function(e) {
                                console.log(e), "0" == e.data.data ? (e.data.suid && (i.setStorageSync("suid", e.data.suid), 
                                t.fxsbind(e.data.suid, 0)), i.showToast({
                                    title: "绑定成功！",
                                    icon: "success",
                                    success: function() {
                                        setTimeout(function() {
                                            n.needBind && n.$emit("closeBind");
                                        }, 1500);
                                    }
                                })) : "1" == e.data.data ? i.showModal({
                                    title: "提醒",
                                    content: "验证码有误",
                                    showCancel: !1
                                }) : "2" == e.data.data && (e.data.suid && (i.setStorageSync("suid", e.data.suid), 
                                t.fxsbind(e.data.suid, 0)), i.showToast({
                                    title: "登陆成功！",
                                    icon: "success",
                                    success: function() {
                                        setTimeout(function() {
                                            n.needBind && n.$emit("closeBind");
                                        }, 1500);
                                    }
                                }));
                            }
                        });
                    },
                    close: function() {
                        this.isshow = !1, this.$emit("closeBind");
                    }
                }
            };
            n.default = e;
        }).call(this, o("543d").default);
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/bindPhone/bindPhone-create-component", {
    "components/bindPhone/bindPhone-create-component": function(e, n, t) {
        t("543d").createComponent(t("651a"));
    }
}, [ [ "components/bindPhone/bindPhone-create-component" ] ] ]);