(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/copyright/copyright" ], {
    "1c66": function(n, e, t) {
        t.r(e);
        var o = t("f9b7"), c = t("4ebc");
        for (var a in c) "default" !== a && function(n) {
            t.d(e, n, function() {
                return c[n];
            });
        }(a);
        var r = t("2877"), u = Object(r.a)(c.default, o.a, o.b, !1, null, null, null);
        e.default = u.exports;
    },
    "4ebc": function(n, e, t) {
        t.r(e);
        var o = t("e905"), c = t.n(o);
        for (var a in o) "default" !== a && function(n) {
            t.d(e, n, function() {
                return o[n];
            });
        }(a);
        e.default = c.a;
    },
    e905: function(n, e, t) {
        (function(t) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var n = {
                props: [ "baseinfo" ],
                methods: {
                    makePhoneCallB: function(n) {
                        var e = this.baseinfo.tel_b;
                        e && t.makePhoneCall({
                            phoneNumber: e
                        });
                    }
                }
            };
            e.default = n;
        }).call(this, t("543d").default);
    },
    f9b7: function(n, e, t) {
        var o = function() {
            this.$createElement;
            this._self._c;
        }, c = [];
        t.d(e, "a", function() {
            return o;
        }), t.d(e, "b", function() {
            return c;
        });
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/copyright/copyright-create-component", {
    "components/copyright/copyright-create-component": function(n, e, t) {
        t("543d").createComponent(t("1c66"));
    }
}, [ [ "components/copyright/copyright-create-component" ] ] ]);