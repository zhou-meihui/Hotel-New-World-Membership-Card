(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/diy_components/banner" ], {
    "02c6": function(n, e, t) {
        var a = t("357e");
        t.n(a).a;
    },
    "357e": function(n, e, t) {},
    "3be5": function(n, t, e) {
        (function(n) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var e = {
                name: "banner",
                props: {
                    banners: {
                        type: Object,
                        value: "banners"
                    }
                },
                data: function() {
                    return {
                        baseinfo: "",
                        index: 0
                    };
                },
                created: function() {},
                methods: {
                    redirectto: function(n) {
                        var e = n.currentTarget.dataset.link, t = n.currentTarget.dataset.linktype;
                        this._redirectto(e, t);
                    },
                    currentChange: function(n) {
                        this.index = n.detail.current;
                    },
                    makephonecall: function() {
                        n.makePhoneCall({
                            phoneNumber: n.getStorageSync("base_tel")
                        });
                    }
                }
            };
            t.default = e;
        }).call(this, e("543d").default);
    },
    "51a3": function(n, e, t) {
        t.r(e);
        var a = t("3be5"), o = t.n(a);
        for (var c in a) "default" !== c && function(n) {
            t.d(e, n, function() {
                return a[n];
            });
        }(c);
        e.default = o.a;
    },
    "924a": function(n, e, t) {
        var a = function() {
            this.$createElement;
            this._self._c;
        }, o = [];
        t.d(e, "a", function() {
            return a;
        }), t.d(e, "b", function() {
            return o;
        });
    },
    f938: function(n, e, t) {
        t.r(e);
        var a = t("924a"), o = t("51a3");
        for (var c in o) "default" !== c && function(n) {
            t.d(e, n, function() {
                return o[n];
            });
        }(c);
        t("02c6");
        var r = t("2877"), u = Object(r.a)(o.default, a.a, a.b, !1, null, null, null);
        e.default = u.exports;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/diy_components/banner-create-component", {
    "components/diy_components/banner-create-component": function(n, e, t) {
        t("543d").createComponent(t("f938"));
    }
}, [ [ "components/diy_components/banner-create-component" ] ] ]);