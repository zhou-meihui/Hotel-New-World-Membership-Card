(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/diy_components/bigimg" ], {
    5222: function(n, e, t) {
        t.r(e);
        var o = t("92d0"), a = t("87f9");
        for (var c in a) "default" !== c && function(n) {
            t.d(e, n, function() {
                return a[n];
            });
        }(c);
        var i = t("2877"), r = Object(i.a)(a.default, o.a, o.b, !1, null, null, null);
        e.default = r.exports;
    },
    "87f9": function(n, e, t) {
        t.r(e);
        var o = t("abfc"), a = t.n(o);
        for (var c in o) "default" !== c && function(n) {
            t.d(e, n, function() {
                return o[n];
            });
        }(c);
        e.default = a.a;
    },
    "92d0": function(n, e, t) {
        var o = function() {
            this.$createElement;
            this._self._c;
        }, a = [];
        t.d(e, "a", function() {
            return o;
        }), t.d(e, "b", function() {
            return a;
        });
    },
    abfc: function(n, t, e) {
        (function(n) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var e = {
                name: "bigimg",
                props: {
                    bigimg: {
                        type: Object,
                        value: "bigimg"
                    }
                },
                data: function() {
                    return {
                        baseinfo: ""
                    };
                },
                created: function() {},
                methods: {
                    redirectto: function(n) {
                        var e = n.currentTarget.dataset.link, t = n.currentTarget.dataset.linktype;
                        this._redirectto(e, t);
                    },
                    makephonecall: function() {
                        n.makePhoneCall({
                            phoneNumber: n.getStorageSync("base_tel")
                        });
                    }
                }
            };
            t.default = e;
        }).call(this, e("543d").default);
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/diy_components/bigimg-create-component", {
    "components/diy_components/bigimg-create-component": function(n, e, t) {
        t("543d").createComponent(t("5222"));
    }
}, [ [ "components/diy_components/bigimg-create-component" ] ] ]);