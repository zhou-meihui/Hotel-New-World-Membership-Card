(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/diy_components/cases" ], {
    2344: function(n, e, t) {
        var o = function() {
            this.$createElement;
            this._self._c;
        }, a = [];
        t.d(e, "a", function() {
            return o;
        }), t.d(e, "b", function() {
            return a;
        });
    },
    "2fbd": function(n, e, t) {
        t.r(e);
        var o = t("2344"), a = t("64f5");
        for (var c in a) "default" !== c && function(n) {
            t.d(e, n, function() {
                return a[n];
            });
        }(c);
        t("a963");
        var r = t("2877"), s = Object(r.a)(a.default, o.a, o.b, !1, null, null, null);
        e.default = s.exports;
    },
    "64f5": function(n, e, t) {
        t.r(e);
        var o = t("f859"), a = t.n(o);
        for (var c in o) "default" !== c && function(n) {
            t.d(e, n, function() {
                return o[n];
            });
        }(c);
        e.default = a.a;
    },
    a963: function(n, e, t) {
        var o = t("e565");
        t.n(o).a;
    },
    e565: function(n, e, t) {},
    f859: function(n, e, t) {
        Object.defineProperty(e, "__esModule", {
            value: !0
        }), e.default = void 0;
        e.default = {
            name: "cases",
            props: {
                cases: {
                    params: {},
                    style: {},
                    data: {}
                }
            },
            data: function() {
                return {};
            },
            created: function() {},
            methods: {
                redirectto: function(n) {
                    var e = n.currentTarget.dataset.link, t = n.currentTarget.dataset.linktype;
                    this._redirectto(e, t);
                }
            }
        };
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/diy_components/cases-create-component", {
    "components/diy_components/cases-create-component": function(n, e, t) {
        t("543d").createComponent(t("2fbd"));
    }
}, [ [ "components/diy_components/cases-create-component" ] ] ]);