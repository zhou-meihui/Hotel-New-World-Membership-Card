(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/diy_components/classfit" ], {
    "7de9": function(n, t, e) {
        e.r(t);
        var o = e("cd30"), a = e.n(o);
        for (var c in o) "default" !== c && function(n) {
            e.d(t, n, function() {
                return o[n];
            });
        }(c);
        t.default = a.a;
    },
    "7e35": function(n, t, e) {
        e.r(t);
        var o = e("812c"), a = e("7de9");
        for (var c in a) "default" !== c && function(n) {
            e.d(t, n, function() {
                return a[n];
            });
        }(c);
        var r = e("2877"), s = Object(r.a)(a.default, o.a, o.b, !1, null, null, null);
        t.default = s.exports;
    },
    "812c": function(n, t, e) {
        var o = function() {
            this.$createElement;
            this._self._c;
        }, a = [];
        e.d(t, "a", function() {
            return o;
        }), e.d(t, "b", function() {
            return a;
        });
    },
    cd30: function(n, e, t) {
        (function(n) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var t = {
                name: "classfit",
                props: {
                    classfit: {
                        params: {},
                        style: {},
                        data: {}
                    }
                },
                data: function() {
                    return {
                        baseinfo: ""
                    };
                },
                created: function() {},
                methods: {
                    redirectto: function(n) {
                        var t = n.currentTarget.dataset.link, e = n.currentTarget.dataset.linktype;
                        this._redirectto(t, e);
                    },
                    makephonecall: function() {
                        n.makePhoneCall({
                            phoneNumber: n.getStorageSync("base_tel")
                        });
                    }
                }
            };
            e.default = t;
        }).call(this, t("543d").default);
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/diy_components/classfit-create-component", {
    "components/diy_components/classfit-create-component": function(n, t, e) {
        e("543d").createComponent(e("7e35"));
    }
}, [ [ "components/diy_components/classfit-create-component" ] ] ]);