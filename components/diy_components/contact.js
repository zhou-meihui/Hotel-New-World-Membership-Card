(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/diy_components/contact" ], {
    "0780": function(t, n, e) {
        var o = function() {
            this.$createElement;
            this._self._c;
        }, a = [];
        e.d(n, "a", function() {
            return o;
        }), e.d(n, "b", function() {
            return a;
        });
    },
    "51d9": function(t, n, e) {
        (function(e) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var t = {
                name: "contact",
                props: {
                    contact: {
                        type: Object,
                        value: "contact"
                    }
                },
                data: function() {
                    return {};
                },
                created: function() {},
                methods: {
                    redirectto: function(t) {
                        var n = t.currentTarget.dataset.link, e = t.currentTarget.dataset.linktype;
                        this._redirectto(n, e);
                    },
                    makephone: function(t) {
                        var n = t.currentTarget.dataset.tel;
                        e.makePhoneCall({
                            phoneNumber: n
                        });
                    },
                    ewmshow: function(t) {
                        e.previewImage({
                            current: t.currentTarget.dataset.ewm,
                            urls: [ t.currentTarget.dataset.ewm ]
                        });
                    }
                }
            };
            n.default = t;
        }).call(this, e("543d").default);
    },
    "728f": function(t, n, e) {
        e.r(n);
        var o = e("0780"), a = e("845f");
        for (var c in a) "default" !== c && function(t) {
            e.d(n, t, function() {
                return a[t];
            });
        }(c);
        var r = e("2877"), u = Object(r.a)(a.default, o.a, o.b, !1, null, null, null);
        n.default = u.exports;
    },
    "845f": function(t, n, e) {
        e.r(n);
        var o = e("51d9"), a = e.n(o);
        for (var c in o) "default" !== c && function(t) {
            e.d(n, t, function() {
                return o[t];
            });
        }(c);
        n.default = a.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/diy_components/contact-create-component", {
    "components/diy_components/contact-create-component": function(t, n, e) {
        e("543d").createComponent(e("728f"));
    }
}, [ [ "components/diy_components/contact-create-component" ] ] ]);