(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/diy_components/goods" ], {
    "0e6b": function(n, e, t) {
        t.r(e);
        var o = t("ed21"), a = t("2faf");
        for (var c in a) "default" !== c && function(n) {
            t.d(e, n, function() {
                return a[n];
            });
        }(c);
        t("40b6");
        var u = t("2877"), r = Object(u.a)(a.default, o.a, o.b, !1, null, null, null);
        e.default = r.exports;
    },
    "2faf": function(n, e, t) {
        t.r(e);
        var o = t("6024"), a = t.n(o);
        for (var c in o) "default" !== c && function(n) {
            t.d(e, n, function() {
                return o[n];
            });
        }(c);
        e.default = a.a;
    },
    "40b6": function(n, e, t) {
        var o = t("afb5");
        t.n(o).a;
    },
    6024: function(n, e, t) {
        (function(t) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var n = {
                name: "goods",
                props: {
                    goods: {
                        type: Object,
                        value: "goods"
                    },
                    baseinfo: {
                        type: Object,
                        value: "baseinfo"
                    }
                },
                data: function() {
                    return {};
                },
                created: function() {},
                methods: {
                    redirectto: function(n) {
                        var e = n.currentTarget.dataset.link;
                        n.currentTarget.dataset.linktype, t.navigateTo({
                            url: e
                        });
                    }
                }
            };
            e.default = n;
        }).call(this, t("543d").default);
    },
    afb5: function(n, e, t) {},
    ed21: function(n, e, t) {
        var o = function() {
            this.$createElement;
            this._self._c;
        }, a = [];
        t.d(e, "a", function() {
            return o;
        }), t.d(e, "b", function() {
            return a;
        });
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/diy_components/goods-create-component", {
    "components/diy_components/goods-create-component": function(n, e, t) {
        t("543d").createComponent(t("0e6b"));
    }
}, [ [ "components/diy_components/goods-create-component" ] ] ]);