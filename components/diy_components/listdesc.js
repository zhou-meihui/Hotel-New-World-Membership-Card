(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/diy_components/listdesc" ], {
    "56ba": function(t, e, n) {
        n.r(e);
        var o = n("f1e1"), c = n.n(o);
        for (var a in o) "default" !== a && function(t) {
            n.d(e, t, function() {
                return o[t];
            });
        }(a);
        e.default = c.a;
    },
    "85cf": function(t, e, n) {
        var o = function() {
            this.$createElement;
            this._self._c;
        }, c = [];
        n.d(e, "a", function() {
            return o;
        }), n.d(e, "b", function() {
            return c;
        });
    },
    d327: function(t, e, n) {
        n.r(e);
        var o = n("85cf"), c = n("56ba");
        for (var a in c) "default" !== a && function(t) {
            n.d(e, t, function() {
                return c[t];
            });
        }(a);
        var r = n("2877"), i = Object(r.a)(c.default, o.a, o.b, !1, null, null, null);
        e.default = i.exports;
    },
    f1e1: function(t, e, n) {
        Object.defineProperty(e, "__esModule", {
            value: !0
        }), e.default = void 0;
        var o = {
            name: "listdesc",
            props: {
                wenlist: {
                    type: Object,
                    value: "wenlist"
                }
            },
            data: function() {
                return {};
            },
            created: function() {},
            methods: {
                redirectto: function(t) {
                    var e = t.currentTarget.dataset.link, n = t.currentTarget.dataset.linktype;
                    this._redirectto(e, n);
                }
            }
        };
        e.default = o;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/diy_components/listdesc-create-component", {
    "components/diy_components/listdesc-create-component": function(t, e, n) {
        n("543d").createComponent(n("d327"));
    }
}, [ [ "components/diy_components/listdesc-create-component" ] ] ]);