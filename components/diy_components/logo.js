(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/diy_components/logo" ], {
    "69b7": function(n, o, t) {
        var e = function() {
            this.$createElement;
            this._self._c;
        }, a = [];
        t.d(o, "a", function() {
            return e;
        }), t.d(o, "b", function() {
            return a;
        });
    },
    "760b": function(n, o, t) {
        t.r(o);
        var e = t("69b7"), a = t("96a9");
        for (var c in a) "default" !== c && function(n) {
            t.d(o, n, function() {
                return a[n];
            });
        }(c);
        var r = t("2877"), u = Object(r.a)(a.default, e.a, e.b, !1, null, null, null);
        o.default = u.exports;
    },
    "96a9": function(n, o, t) {
        t.r(o);
        var e = t("b076"), a = t.n(e);
        for (var c in e) "default" !== c && function(n) {
            t.d(o, n, function() {
                return e[n];
            });
        }(c);
        o.default = a.a;
    },
    b076: function(n, o, t) {
        (function(t) {
            Object.defineProperty(o, "__esModule", {
                value: !0
            }), o.default = void 0;
            var n = {
                name: "logo",
                props: {
                    logo: {
                        type: Object,
                        value: "logo"
                    }
                },
                data: function() {
                    return {};
                },
                created: function() {
                    console.log(this.logo);
                },
                methods: {
                    redirectto: function(n) {
                        var o = n.currentTarget.dataset.link, t = n.currentTarget.dataset.linktype;
                        this._redirectto(o, t);
                    },
                    phoneshop: function(n) {
                        var o = n.currentTarget.dataset.num;
                        t.makePhoneCall({
                            phoneNumber: o
                        });
                    }
                }
            };
            o.default = n;
        }).call(this, t("543d").default);
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/diy_components/logo-create-component", {
    "components/diy_components/logo-create-component": function(n, o, t) {
        t("543d").createComponent(t("760b"));
    }
}, [ [ "components/diy_components/logo-create-component" ] ] ]);