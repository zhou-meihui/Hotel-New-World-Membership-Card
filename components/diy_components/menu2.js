(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/diy_components/menu2" ], {
    "0318": function(n, e, t) {
        t.r(e);
        var o = t("cb8a"), a = t.n(o);
        for (var c in o) "default" !== c && function(n) {
            t.d(e, n, function() {
                return o[n];
            });
        }(c);
        e.default = a.a;
    },
    "03f6": function(n, e, t) {
        var o = t("dadd");
        t.n(o).a;
    },
    "3fe4": function(n, e, t) {
        var o = function() {
            this.$createElement;
            this._self._c;
        }, a = [];
        t.d(e, "a", function() {
            return o;
        }), t.d(e, "b", function() {
            return a;
        });
    },
    "4aee": function(n, e, t) {
        t.r(e);
        var o = t("3fe4"), a = t("0318");
        for (var c in a) "default" !== c && function(n) {
            t.d(e, n, function() {
                return a[n];
            });
        }(c);
        t("03f6");
        var u = t("2877"), r = Object(u.a)(a.default, o.a, o.b, !1, null, null, null);
        e.default = r.exports;
    },
    cb8a: function(n, t, e) {
        (function(n) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var e = {
                name: "menu2",
                props: {
                    menu2: {
                        params: {},
                        style: {},
                        data: {},
                        count: ""
                    }
                },
                data: function() {
                    return {
                        baseinfo: ""
                    };
                },
                created: function() {},
                methods: {
                    redirectto: function(n) {
                        var e = n.currentTarget.dataset.link, t = n.currentTarget.dataset.linktype;
                        this._redirectto(e, t);
                    },
                    makephonecall: function() {
                        n.makePhoneCall({
                            phoneNumber: n.getStorageSync("base_tel")
                        });
                    }
                }
            };
            t.default = e;
        }).call(this, e("543d").default);
    },
    dadd: function(n, e, t) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/diy_components/menu2-create-component", {
    "components/diy_components/menu2-create-component": function(n, e, t) {
        t("543d").createComponent(t("4aee"));
    }
}, [ [ "components/diy_components/menu2-create-component" ] ] ]);