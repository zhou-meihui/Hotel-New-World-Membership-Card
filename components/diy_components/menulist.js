(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/diy_components/menulist" ], {
    "7ffd": function(n, t, e) {
        (function(n) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var e = {
                name: "menulist",
                props: {
                    menulist: {
                        type: Object,
                        value: "menulist"
                    }
                },
                data: function() {
                    return {
                        baseinfo: ""
                    };
                },
                created: function() {},
                methods: {
                    redirectto: function(n) {
                        var e = n.currentTarget.dataset.link, t = n.currentTarget.dataset.linktype;
                        this._redirectto(e, t);
                    },
                    makephonecall: function() {
                        n.makePhoneCall({
                            phoneNumber: n.getStorageSync("base_tel")
                        });
                    }
                }
            };
            t.default = e;
        }).call(this, e("543d").default);
    },
    8816: function(n, e, t) {},
    "8d16": function(n, e, t) {
        var o = function() {
            this.$createElement;
            this._self._c;
        }, a = [];
        t.d(e, "a", function() {
            return o;
        }), t.d(e, "b", function() {
            return a;
        });
    },
    9482: function(n, e, t) {
        t.r(e);
        var o = t("8d16"), a = t("abca");
        for (var c in a) "default" !== c && function(n) {
            t.d(e, n, function() {
                return a[n];
            });
        }(c);
        t("e7ff");
        var u = t("2877"), r = Object(u.a)(a.default, o.a, o.b, !1, null, null, null);
        e.default = r.exports;
    },
    abca: function(n, e, t) {
        t.r(e);
        var o = t("7ffd"), a = t.n(o);
        for (var c in o) "default" !== c && function(n) {
            t.d(e, n, function() {
                return o[n];
            });
        }(c);
        e.default = a.a;
    },
    e7ff: function(n, e, t) {
        var o = t("8816");
        t.n(o).a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/diy_components/menulist-create-component", {
    "components/diy_components/menulist-create-component": function(n, e, t) {
        t("543d").createComponent(t("9482"));
    }
}, [ [ "components/diy_components/menulist-create-component" ] ] ]);