(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/diy_components/personlist" ], {
    "2dd8": function(n, t, e) {
        e.r(t);
        var o = e("8667"), r = e.n(o);
        for (var a in o) "default" !== a && function(n) {
            e.d(t, n, function() {
                return o[n];
            });
        }(a);
        t.default = r.a;
    },
    "55d3": function(n, t, e) {
        var o = function() {
            this.$createElement;
            this._self._c;
        }, r = [];
        e.d(t, "a", function() {
            return o;
        }), e.d(t, "b", function() {
            return r;
        });
    },
    8667: function(n, t, e) {
        Object.defineProperty(t, "__esModule", {
            value: !0
        }), t.default = void 0;
        var o = {
            name: "personlist",
            props: {
                personlist: {
                    type: Object,
                    value: "personlist"
                }
            },
            data: function() {
                return {
                    $imgurl: this.$imgurl
                };
            },
            methods: {
                redirectto: function(n) {
                    var t = n.currentTarget.dataset.link, e = n.currentTarget.dataset.linktype;
                    this._redirectto(t, e);
                }
            }
        };
        t.default = o;
    },
    dd04: function(n, t, e) {
        e.r(t);
        var o = e("55d3"), r = e("2dd8");
        for (var a in r) "default" !== a && function(n) {
            e.d(t, n, function() {
                return r[n];
            });
        }(a);
        var c = e("2877"), s = Object(c.a)(r.default, o.a, o.b, !1, null, null, null);
        t.default = s.exports;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/diy_components/personlist-create-component", {
    "components/diy_components/personlist-create-component": function(n, t, e) {
        e("543d").createComponent(e("dd04"));
    }
}, [ [ "components/diy_components/personlist-create-component" ] ] ]);