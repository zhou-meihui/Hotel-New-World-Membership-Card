(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/diy_components/picturew" ], {
    "1fb0": function(n, e, t) {
        var o = function() {
            this.$createElement;
            this._self._c;
        }, c = [];
        t.d(e, "a", function() {
            return o;
        }), t.d(e, "b", function() {
            return c;
        });
    },
    "49a9": function(n, e, t) {
        var o = t("b86d");
        t.n(o).a;
    },
    6318: function(n, e, t) {
        t.r(e);
        var o = t("9c42"), c = t.n(o);
        for (var a in o) "default" !== a && function(n) {
            t.d(e, n, function() {
                return o[n];
            });
        }(a);
        e.default = c.a;
    },
    6445: function(n, e, t) {
        t.r(e);
        var o = t("1fb0"), c = t("6318");
        for (var a in c) "default" !== a && function(n) {
            t.d(e, n, function() {
                return c[n];
            });
        }(a);
        t("49a9");
        var r = t("2877"), u = Object(r.a)(c.default, o.a, o.b, !1, null, null, null);
        e.default = u.exports;
    },
    "9c42": function(n, t, e) {
        (function(n) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var e = {
                name: "picturew",
                props: {
                    picturew: {
                        type: Object,
                        value: "picturew"
                    }
                },
                data: function() {
                    return {
                        baseinfo: ""
                    };
                },
                created: function() {},
                methods: {
                    redirectto: function(n) {
                        var e = n.currentTarget.dataset.link, t = n.currentTarget.dataset.linktype;
                        this._redirectto(e, t);
                    },
                    makephonecall: function() {
                        n.makePhoneCall({
                            phoneNumber: n.getStorageSync("base_tel")
                        });
                    }
                }
            };
            t.default = e;
        }).call(this, e("543d").default);
    },
    b86d: function(n, e, t) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/diy_components/picturew-create-component", {
    "components/diy_components/picturew-create-component": function(n, e, t) {
        t("543d").createComponent(t("6445"));
    }
}, [ [ "components/diy_components/picturew-create-component" ] ] ]);