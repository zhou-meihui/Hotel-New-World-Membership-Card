(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/diy_components/reserve" ], {
    "0132": function(e, n, t) {
        Object.defineProperty(n, "__esModule", {
            value: !0
        }), n.default = void 0;
        var o = {
            name: "reserve",
            props: {
                goods: {
                    type: Object,
                    value: "goods"
                },
                baseinfo: {
                    type: Object,
                    value: "baseinfo"
                }
            },
            data: function() {
                return {};
            },
            created: function() {},
            methods: {
                redirectto: function(e) {
                    var n = e.currentTarget.dataset.link, t = e.currentTarget.dataset.linktype;
                    this._redirectto(n, t);
                }
            }
        };
        n.default = o;
    },
    "15d1": function(e, n, t) {
        t.r(n);
        var o = t("229e"), r = t("8f69");
        for (var c in r) "default" !== c && function(e) {
            t.d(n, e, function() {
                return r[e];
            });
        }(c);
        t("bef6");
        var a = t("2877"), u = Object(a.a)(r.default, o.a, o.b, !1, null, null, null);
        n.default = u.exports;
    },
    "229e": function(e, n, t) {
        var o = function() {
            this.$createElement;
            this._self._c;
        }, r = [];
        t.d(n, "a", function() {
            return o;
        }), t.d(n, "b", function() {
            return r;
        });
    },
    "8f69": function(e, n, t) {
        t.r(n);
        var o = t("0132"), r = t.n(o);
        for (var c in o) "default" !== c && function(e) {
            t.d(n, e, function() {
                return o[e];
            });
        }(c);
        n.default = r.a;
    },
    bef6: function(e, n, t) {
        var o = t("c45f");
        t.n(o).a;
    },
    c45f: function(e, n, t) {}
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/diy_components/reserve-create-component", {
    "components/diy_components/reserve-create-component": function(e, n, t) {
        t("543d").createComponent(t("15d1"));
    }
}, [ [ "components/diy_components/reserve-create-component" ] ] ]);