(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/diy_components/service" ], {
    "40b3": function(e, n, t) {
        t.r(n);
        var o = t("6ea5"), c = t("b737");
        for (var a in c) "default" !== a && function(e) {
            t.d(n, e, function() {
                return c[e];
            });
        }(a);
        var r = t("2877"), i = Object(r.a)(c.default, o.a, o.b, !1, null, null, null);
        n.default = i.exports;
    },
    "6ea5": function(e, n, t) {
        var o = function() {
            this.$createElement;
            this._self._c;
        }, c = [];
        t.d(n, "a", function() {
            return o;
        }), t.d(n, "b", function() {
            return c;
        });
    },
    "96e5": function(e, t, n) {
        (function(e) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var n = {
                name: "service",
                props: {
                    service: {
                        type: Object,
                        value: "service"
                    }
                },
                data: function() {
                    return {
                        baseinfo: ""
                    };
                },
                created: function() {
                    console.log(this.service);
                },
                methods: {
                    redirectto: function(e) {
                        var n = e.currentTarget.dataset.link, t = e.currentTarget.dataset.linktype;
                        console.log(n), console.log(t), this._redirectto(n, t);
                    },
                    makephonecall: function() {
                        e.makePhoneCall({
                            phoneNumber: e.getStorageSync("base_tel")
                        });
                    }
                }
            };
            t.default = n;
        }).call(this, n("543d").default);
    },
    b737: function(e, n, t) {
        t.r(n);
        var o = t("96e5"), c = t.n(o);
        for (var a in o) "default" !== a && function(e) {
            t.d(n, e, function() {
                return o[e];
            });
        }(a);
        n.default = c.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/diy_components/service-create-component", {
    "components/diy_components/service-create-component": function(e, n, t) {
        t("543d").createComponent(t("40b3"));
    }
}, [ [ "components/diy_components/service-create-component" ] ] ]);