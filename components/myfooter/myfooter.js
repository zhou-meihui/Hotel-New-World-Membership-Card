(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/myfooter/myfooter" ], {
    "05d2": function(e, n, t) {
        var o = t("e18d");
        t.n(o).a;
    },
    "5d66": function(e, n, t) {
        t.r(n);
        var o = t("a199"), a = t("f327");
        for (var r in a) "default" !== r && function(e) {
            t.d(n, e, function() {
                return a[e];
            });
        }(r);
        t("05d2");
        var c = t("2877"), u = Object(c.a)(a.default, o.a, o.b, !1, null, null, null);
        n.default = u.exports;
    },
    "6d09": function(e, n, t) {
        (function(r) {
            Object.defineProperty(n, "__esModule", {
                value: !0
            }), n.default = void 0;
            var e = {
                props: [ "page_signs", "baseinfo" ],
                data: function() {
                    return {
                        isIphoneX: ""
                    };
                },
                created: function() {
                    var n = this;
                    r.getSystemInfo({
                        success: function(e) {
                            -1 != e.model.search("iPhone X") && (n.isIphoneX = !0);
                        }
                    });
                },
                methods: {
                    redirectto: function(e) {
                        var n = e.currentTarget.dataset.link, t = e.currentTarget.dataset.linktype;
                        if ("page" == t) {
                            var o = n.indexOf("pages/index"), a = n.indexOf("index?pageid");
                            -1 == o && -1 == a ? r.navigateTo({
                                url: n
                            }) : r.reLaunch({
                                url: n
                            });
                        } else this._redirectto(n, t);
                    },
                    makephonecall: function() {
                        r.makePhoneCall({
                            phoneNumber: r.getStorageSync("base_tel")
                        });
                    }
                }
            };
            n.default = e;
        }).call(this, t("543d").default);
    },
    a199: function(e, n, t) {
        var o = function() {
            this.$createElement;
            this._self._c;
        }, a = [];
        t.d(n, "a", function() {
            return o;
        }), t.d(n, "b", function() {
            return a;
        });
    },
    e18d: function(e, n, t) {},
    f327: function(e, n, t) {
        t.r(n);
        var o = t("6d09"), a = t.n(o);
        for (var r in o) "default" !== r && function(e) {
            t.d(n, e, function() {
                return o[e];
            });
        }(r);
        n.default = a.a;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/myfooter/myfooter-create-component", {
    "components/myfooter/myfooter-create-component": function(e, n, t) {
        t("543d").createComponent(t("5d66"));
    }
}, [ [ "components/myfooter/myfooter-create-component" ] ] ]);