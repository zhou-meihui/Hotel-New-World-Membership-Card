(global.webpackJsonp = global.webpackJsonp || []).push([ [ "components/plugins/list_pro" ], {
    1351: function(n, t, e) {
        e.r(t);
        var o = e("52fb"), a = e.n(o);
        for (var r in o) "default" !== r && function(n) {
            e.d(t, n, function() {
                return o[n];
            });
        }(r);
        t.default = a.a;
    },
    "52fb": function(n, t, e) {
        (function(a) {
            Object.defineProperty(t, "__esModule", {
                value: !0
            }), t.default = void 0;
            var n = {
                name: "list_pro",
                props: [ "item", "c" ],
                methods: {
                    redirectto: function(n) {
                        var t, e = n.currentTarget.dataset.id, o = n.currentTarget.dataset.link;
                        t = "showPro" == o ? "/pagesFlashSale/showPro/showPro?id=" + e : "showPro_lv" == o ? "/pagesReserve/proDetail/proDetail?id=" + e : "/pages/" + o + "/" + o + "?id=" + e, 
                        a.navigateTo({
                            url: t
                        });
                    }
                }
            };
            t.default = n;
        }).call(this, e("543d").default);
    },
    dd4f: function(n, t, e) {
        var o = function() {
            this.$createElement;
            this._self._c;
        }, a = [];
        e.d(t, "a", function() {
            return o;
        }), e.d(t, "b", function() {
            return a;
        });
    },
    ddbc: function(n, t, e) {
        e.r(t);
        var o = e("dd4f"), a = e("1351");
        for (var r in a) "default" !== r && function(n) {
            e.d(t, n, function() {
                return a[n];
            });
        }(r);
        var l = e("2877"), c = Object(l.a)(a.default, o.a, o.b, !1, null, null, null);
        t.default = c.exports;
    }
} ]), (global.webpackJsonp = global.webpackJsonp || []).push([ "components/plugins/list_pro-create-component", {
    "components/plugins/list_pro-create-component": function(n, t, e) {
        e("543d").createComponent(e("ddbc"));
    }
}, [ [ "components/plugins/list_pro-create-component" ] ] ]);