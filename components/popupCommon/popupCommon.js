Component({  
  properties: {  
    show: {  
      type: Boolean,  
      value: false,  
    },  
  },  
  methods: {  
    hidePopup() {  
      this.setData({  
        show: false,  
      });  
      this.triggerEvent('hide'); // 触发自定义事件，通知父组件弹窗已隐藏  
    },  
  },  
});