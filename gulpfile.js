var gulp = require('gulp'),
  uglify = require('gulp-uglify-es').default;

//压缩小程序js文件
gulp.task('jsmin', function () {
  gulp.src(['common/*.js'])
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'))
});
