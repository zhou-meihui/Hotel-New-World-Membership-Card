(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/address/address"], {
        "05fa": function (e, i, d) {
            (function (f) {
                Object.defineProperty(i, "__esModule", {
                    value: !0
                }), i.default = void 0;
                var g = d("f571"),
                    o = d("74f4"),
                    u = d("0671"),
                    c = d("c5de"),
                    e = {
                        components: {
                            wPicker: function () {
                                return Promise.all([d.e("common/vendor"), d.e("components/w-picker/w-picker")]).then(d.bind(null, "8fa3"));
                            }
                        },
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                baseinfo: "",
                                address_is: 1,
                                moren: 0,
                                choose: 0,
                                id: 0,
                                choose_h: 0,
                                h: 0,
                                addressinfo: {},
                                myaddress: "",
                                needAuth: !1,
                                needBind: !1,
                                region: ["北京市", "北京市", "东城区"],
                                tabList: [{
                                    mode: "region",
                                    name: "省市区",
                                    value: [0, 0, 0]
                                }],
                                defaultVal: [0, 0, 0],
                                add_id: 0,
                                shop_id: 0,
                                from_gwc: 0,
                                nav: 1,
                                changeAdd: 0,
                                is_checked: "",
                                kuaidi: ""
                            };
                        },
                        onLoad: function (e) {
                            var i = this,
                                d = e.shareid;
                            null != d && "undefined" != d && (this.shareid = d);
                            var s = e.orderid;
                            null != s && "undefined" != s && (this.orderid = s);
                            var t = e.pid;
                            null != t && "undefined" != t && (this.pid = t);
                            var a = e.add_id;
                            null != a && "undefined" != a && (this.add_id = a);
                            var n = e.from_gwc;
                            null != n && "undefined" != n && (this.from_gwc = n);
                            var r = e.shop_id;
                            r && (this.shop_id = r);
                            var o = e.nav;
                            o && (this.nav = o);
                            var u = e.discounts;
                            null != u && (this.discounts = u);
                            var c = 0;
                            e.fxsid && (c = e.fxsid, this.fxsid = c);
                            var h = e.kuaidi;
                            null != h && (this.kuaidi = h);
                            var l = this;
                            f.getSystemInfo({
                                success: function (e) {
                                    l.choose_h = e.windowHeight, l.h = e.windowHeight - 60;
                                }
                            }), this._baseMin(this), f.getStorageSync("suid"), g.getOpenid(c, function () {
                                i.getMyAddress();
                            }, function () {
                                i.needAuth = !0;
                            }, function () {
                                i.needBind = !0;
                            });
                        },
                        computed: {
                            mode: function () {
                                return this.tabList[0].mode;
                            }
                        },
                        methods: {
                            input_name: function (e) {
                                var i = e.detail.value,
                                    d = this.addressinfo;
                                d.name = i, this.addressinfo = d;
                            },
                            input_mobile: function (e) {
                                var i = e.detail.value,
                                    d = this.addressinfo;
                                d.mobile = i, this.addressinfo = d;
                            },
                            input_address: function (e) {
                                var i = e.detail.value,
                                    d = this.addressinfo;
                                d.more_address = i, this.addressinfo = d;
                            },
                            toggleTab: function () {
                                this.$refs.picker.show();
                            },
                            onConfirm: function (e) {
                                var i = e.checkArr;
                                this.region = [i[0], i[1], i[2]];
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1, this.getMyAddress();
                            },
                            getSuid: function () {
                                if (f.getStorageSync("suid")) return !0;
                                return f.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                    !1;
                            },
                            getMyAddress: function () {
                                var i = this,
                                    e = f.getStorageSync("suid");
                                f.request({
                                    url: this.$baseurl + "doPagegetallmyaddress",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: e
                                    },
                                    success: function (e) {
                                        i.myaddress = e.data.data, 0 < i.myaddress.length ? i.address_is = 1 : i.address_is = 0;
                                    }
                                });
                            },
                            add_address: function (e) {
                                var r = this;
                                if (!this.getSuid()) return !1;
                                var i = e.currentTarget.dataset.id;
                                if (this.id = i) {
                                    var d = f.getStorageSync("suid");
                                    f.request({
                                        url: this.$baseurl + "doPagegetaddressdetail",
                                        data: {
                                            uniacid: this.$uniacid,
                                            suid: d,
                                            id: i
                                        },
                                        success: function (e) {
                                            if (e.data.data) {
                                                r.addressinfo = e.data.data;
                                                var i = e.data.data.region;
                                                r.region = e.data.data.region;
                                                for (var d = o.default, s = u.default, t = c.default, a = [], n = 0; n < d.length; n++)
                                                    if (d[n].label == i[0]) {
                                                        a.push(n);
                                                        break;
                                                    }
                                                for (s = s[a[0]], n = 0; n < s.length; n++)
                                                    if (s[n].label == i[1]) {
                                                        a.push(n);
                                                        break;
                                                    }
                                                for (t = t[a[0]][a[1]], n = 0; n < t.length; n++)
                                                    if (t[n].label == i[2]) {
                                                        a.push(n);
                                                        break;
                                                    }
                                                r.defaultVal = a, r.changeAdd = 1, r.choose = 1, r.is_mo = e.data.data.is_mo, 1 == r.is_mo ? r.is_checked = !1 : r.is_checked = !0;
                                            }
                                        }
                                    });
                                } else this.addressinfo = {}, this.region = ["北京市", "北京市", "东城区"], this.changeAdd = 0,
                                    this.choose = 1, this.is_mo = 1;
                            },
                            switchChange: function () {
                                0 == this.is_checked ? this.is_checked = !0 : this.is_checked = !1;
                            },
                            choose_close: function () {
                                this.choose = 0, this.is_checked = !1;
                            },
                            formSubmit: function (e) {
                                var i = this,
                                    d = this.id,
                                    s = e.detail.value,
                                    t = this.region,
                                    a = s.realname,
                                    n = s.mobile,
                                    r = s.postalcode,
                                    o = t[0] + " " + t[1] + " " + t[2],
                                    u = s.more_address,
                                    c = this.is_checked ? 1 : 0,
                                    h = f.getStorageSync("suid");
                                if (!/^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(16[0-9]{1})|(17[0-9]{1})|(18[0-9]{1})|(19[0-9]{1}))+\d{8})$/.test(n)) return f.showModal({
                                    title: "提醒",
                                    content: "请输入有效的手机号码！",
                                    showCancel: !1
                                }), !1;
                                f.request({
                                    url: this.$baseurl + "doPageaddmyaddress",
                                    data: {
                                        uniacid: this.$uniacid,
                                        id: d,
                                        suid: h,
                                        name: a,
                                        mobile: n,
                                        address: o,
                                        more_address: u,
                                        postalcode: r,
                                        froms: "selfadd",
                                        is_checked: c
                                    },
                                    success: function (e) {
                                        i.id = 0, f.showToast({
                                            title: "更新/新建成功",
                                            icon: "success"
                                        }), i.getMyAddress(), i.choose_close();
                                    }
                                });
                            },
                            deladdress: function (e) {
                                var i = e.currentTarget.dataset.id,
                                    d = this;
                                f.showModal({
                                    content: "确定要删除吗",
                                    success: function (e) {
                                        e.confirm && f.request({
                                            url: d.$baseurl + "doPagedelmyaddress",
                                            data: {
                                                uniacid: d.$uniacid,
                                                id: i
                                            },
                                            success: function (e) {
                                                f.showToast({
                                                    title: "删除成功",
                                                    icon: "success"
                                                }), d.getMyAddress();
                                            }
                                        });
                                    }
                                });
                            },
                            moren_set: function (e) {
                                var i = this,
                                    d = e.currentTarget.dataset.id,
                                    s = f.getStorageSync("suid");
                                f.request({
                                    url: this.$baseurl + "doPagesetdefaultaddress",
                                    data: {
                                        suid: s,
                                        id: d
                                    },
                                    success: function (e) {
                                        i.getMyAddress();
                                    }
                                });
                            },
                            bindRegionChange: function (e) {
                                this.region = e.detail.value;
                            },
                            xuanzaddress: function (e) {
                                var i = this,
                                    d = e.currentTarget.dataset.id,
                                    s = getCurrentPages();
                                if (1 < s.length) {
                                    var t = s[s.length - 2].route;
                                    f.redirectTo({
                                        url: "/" + t + "?addressid=" + d + "&shareid=" + i.shareid + "&id=" + i.pid + "&orderid=" + i.orderid + "&discounts=" + i.discounts + "&add_id=" + d + "&nav=" + this.nav + "&from_gwc=" + this.from_gwc + "&shop_id=" + this.shop_id + "&kuaidi=" + i.kuaidi
                                    });
                                }
                            },
                            wx_address: function () {
                                var n = this,
                                    r = f.getStorageSync("suid");
                                f.chooseAddress({
                                    success: function (e) {
                                        var i = e.provinceName + " " + e.cityName + " " + e.countyName,
                                            d = e.detailInfo,
                                            s = e.userName,
                                            t = e.telNumber,
                                            a = e.postalCode;
                                        f.request({
                                            url: n.$baseurl + "doPageaddmyaddress",
                                            data: {
                                                uniacid: n.$uniacid,
                                                suid: r,
                                                name: s,
                                                mobile: t,
                                                address: i,
                                                more_address: d,
                                                postalcode: a,
                                                froms: "weixin"
                                            },
                                            success: function (e) {
                                                n.getMyAddress();
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    };
                i.default = e;
            }).call(this, d("543d").default);
        },
        "297e": function (e, i, d) {
            var s = function () {
                    this.$createElement;
                    this._self._c;
                },
                t = [];
            d.d(i, "a", function () {
                return s;
            }), d.d(i, "b", function () {
                return t;
            });
        },
        3840: function (e, i, d) {
            d.r(i);
            var s = d("297e"),
                t = d("a4d3");
            for (var a in t) "default" !== a && function (e) {
                d.d(i, e, function () {
                    return t[e];
                });
            }(a);
            d("4abc");
            var n = d("2877"),
                r = Object(n.a)(t.default, s.a, s.b, !1, null, null, null);
            i.default = r.exports;
        },
        "4abc": function (e, i, d) {
            var s = d("98b0");
            d.n(s).a;
        },
        "98b0": function (e, i, d) {},
        a4d3: function (e, i, d) {
            d.r(i);
            var s = d("05fa"),
                t = d.n(s);
            for (var a in s) "default" !== a && function (e) {
                d.d(i, e, function () {
                    return s[e];
                });
            }(a);
            i.default = t.a;
        },
        d0da: function (e, i, d) {
            (function (e) {
                function i(e) {
                    return e && e.__esModule ? e : {
                        default: e
                    };
                }
                d("020c"), d("921b"), i(d("66fd")), e(i(d("3840")).default);
            }).call(this, d("543d").createPage);
        }
    },
    [
        ["d0da", "common/runtime", "common/vendor"]
    ]
]);