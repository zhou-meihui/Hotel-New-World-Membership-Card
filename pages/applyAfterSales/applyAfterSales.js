(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/applyAfterSales/applyAfterSales"], {
        "1d86": function (e, t, a) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                n = [];
            a.d(t, "a", function () {
                return i;
            }), a.d(t, "b", function () {
                return n;
            });
        },
        "287b": function (e, t, a) {
            var i = a("e0dd");
            a.n(i).a;
        },
        "3f51": function (e, t, a) {
            a.r(t);
            var i = a("1d86"),
                n = a("ed95");
            for (var o in n) "default" !== o && function (e) {
                a.d(t, e, function () {
                    return n[e];
                });
            }(o);
            a("287b");
            var r = a("2877"),
                s = Object(r.a)(n.default, i.a, i.b, !1, null, null, null);
            t.default = s.exports;
        },
        "7a3f": function (e, t, a) {
            (function (e) {
                function t(e) {
                    return e && e.__esModule ? e : {
                        default: e
                    };
                }
                a("020c"), a("921b"), t(a("66fd")), e(t(a("3f51")).default);
            }).call(this, a("543d").createPage);
        },
        b488: function (e, t, a) {
            (function (l) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var d = a("f571"),
                    e = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                $host: this.$host,
                                apply_box: 0,
                                chooseapplytype: -1,
                                num: 1,
                                evaluatecon: "",
                                nowcount: 0,
                                order_id: 0,
                                order_item_id: 0,
                                dataAll: "",
                                from_to: 0,
                                tk_msg: "请选择售后类型",
                                cancel_money: 0,
                                status: 0,
                                delivery_type: 0,
                                total_num: 0,
                                is_last_refund: 0,
                                is_add_freight: 0,
                                shen_all_can_refund_money: 0,
                                freight_all: 0
                            };
                        },
                        onLoad: function (e) {
                            var t = this;
                            this._baseMin(this);
                            var a = 0;
                            e.fxsid && (a = e.fxsid), this.fxsid = a;
                            var i = 0;
                            if (e.from_to && (i = e.from_to), 1 == (this.from_to = i)) {
                                var n = 0;
                                e.order_id && (n = e.order_id), this.order_id = n;
                            } else if (2 == i) {
                                var o = 0;
                                e.order_item_id && (o = e.order_item_id), this.order_item_id = o;
                            }
                            var r = l.getStorageSync("suid");
                            r && (this.suid = r);
                            var s = l.getStorageSync("source");
                            s && (this.source = s), d.getOpenid(a, function () {
                                1 == i ? t.getapplyall() : 2 == i && t.getapplydetail();
                            });
                        },
                        methods: {
                            getapplyall: function () {
                                var a = this;
                                l.request({
                                    url: this.$host + "/api/mainwxapp/applyAllAfterSales",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: this.suid,
                                        order_id: this.order_id
                                    },
                                    success: function (e) {
                                        console.log(e.data.data);
                                        for (var t = 0; t < e.data.data.order.order_items.length; t++) e.data.data.order.order_items[t].cancel_num = e.data.data.order.order_items[t].num;
                                        a.dataAll = e.data.data.order.order_items, a.cancel_money = e.data.data.order.pay_money;
                                    },
                                    fail: function (e) {
                                        console.log(e);
                                    }
                                });
                            },
                            getapplydetail: function () {
                                var a = this;
                                l.request({
                                    url: this.$host + "/api/mainwxapp/applyItemAfterSales",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: this.suid,
                                        order_item_id: this.order_item_id
                                    },
                                    success: function (e) {
                                        if (0 == e.data.data.error) {
                                            var t = [];
                                            t[0] = e.data.data.order_item, t[0].cancel_num = t[0].num, a.dataAll = t, a.status = t[0].status,
                                                a.delivery_type = t[0].delivery_type, a.is_last_refund = t[0].is_last_refund, a.is_add_freight = t[0].is_add_freight,
                                                a.shen_all_can_refund_money = t[0].shen_all_can_refund_money, 1 == a.is_last_refund ? 1 == a.is_add_freight ? a.cancel_money = a.shen_all_can_refund_money : (a.freight_all = t[0].freight_all,
                                                    a.cancel_money = (a.shen_all_can_refund_money - a.freight_all).toFixed(2)) : a.cancel_money = (t[0].pro_can_refound_price * t[0].num).toFixed(2);
                                        } else l.showModal({
                                            title: "提示",
                                            content: "申请失败，" + e.data.data.msg,
                                            showCancel: !1,
                                            success: function (e) {
                                                l.navigateBack({
                                                    delta: 2
                                                });
                                            }
                                        });
                                    },
                                    fail: function (e) {
                                        console.log(e);
                                    }
                                });
                            },
                            cancelPaymentOrder: function () {
                                var e = this.evaluatecon;
                                if (!e) return l.showModal({
                                    title: "提示",
                                    content: "申请原因不能为空",
                                    showCancel: !1
                                }), !1;
                                var t = this.source;
                                l.request({
                                    url: this.$host + "/api/mainwxapp/cancelPaymentOrder",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: this.suid,
                                        order_id: this.order_id,
                                        remark: e,
                                        type: 0,
                                        source: t
                                    },
                                    success: function (e) {
                                        if (0 == e.data.data.error) {
                                            var t = e.data.data.order_service.order_service_id;
                                            l.showModal({
                                                title: "提示",
                                                content: "退款申请成功",
                                                showCancel: !1,
                                                success: function (e) {
                                                    console.log(e), l.redirectTo({
                                                        url: "/pages/orderAftersale/orderAftersale?order_service_id=" + t
                                                    });
                                                }
                                            });
                                        } else l.showModal({
                                            title: "提示",
                                            content: "申请失败，" + e.data.data.msg,
                                            showCancel: !1
                                        });
                                    },
                                    fail: function (e) {
                                        console.log(e);
                                    }
                                });
                            },
                            cancelPaymentOrderItem: function () {
                                var e = this.evaluatecon;
                                if (!e) return l.showModal({
                                    title: "提示",
                                    content: "申请原因不能为空",
                                    showCancel: !1
                                }), !1;
                                var t = this.status,
                                    a = this.delivery_type,
                                    i = -1;
                                if (1 == t || 2 == t && 2 == a) i = 0;
                                else if (-1 == (i = this.chooseapplytype)) return void l.showModal({
                                    title: "提示",
                                    content: "请选择售后类型",
                                    showCancel: !1
                                });
                                var n = this.source;
                                l.request({
                                    url: this.$host + "/api/mainwxapp/applyItemSubmit",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: this.suid,
                                        order_item_id: this.order_item_id,
                                        num: this.dataAll[0].cancel_num,
                                        refund_money: this.cancel_money,
                                        type: i,
                                        source: n,
                                        remark: e
                                    },
                                    success: function (e) {
                                        if (0 == e.data.data.error) {
                                            var t = e.data.data.order_service.order_service_id;
                                            l.showModal({
                                                title: "提示",
                                                content: "退款申请成功",
                                                showCancel: !1,
                                                success: function (e) {
                                                    console.log(e), l.redirectTo({
                                                        url: "/pages/orderAftersale/orderAftersale?order_service_id=" + t
                                                    });
                                                }
                                            });
                                        } else l.showModal({
                                            title: "提示",
                                            content: "申请失败，" + e.data.data.msg,
                                            showCancel: !1
                                        });
                                    },
                                    fail: function (e) {
                                        console.log(e);
                                    }
                                });
                            },
                            chooseApply: function () {
                                0 == this.apply_box && (this.apply_box = 1), this.chooseapplytype = -1;
                            },
                            hideApply: function () {
                                1 == this.apply_box && (this.apply_box = 0), this.chooseapplytype = -1;
                            },
                            getApplyType: function () {
                                var e = this.chooseapplytype; -
                                1 != e ? (1 == this.apply_box && (this.apply_box = 0), 0 == e ? this.tk_msg = "仅退款" : 1 == e && (this.tk_msg = "退货退款")) : l.showModal({
                                    title: "提示",
                                    content: "请选择售后类型",
                                    showCancel: !1
                                });
                            },
                            chooseapplytypes: function (e) {
                                this.chooseapplytype = e.currentTarget.dataset.type;
                            },
                            num_change: function (e) {
                                var t = e.currentTarget.dataset.type;
                                if (2 == this.from_to) {
                                    var a = this.dataAll,
                                        i = 0;
                                    1 == t ? i = a[0].cancel_num - 1 : 2 == t && (i = a[0].cancel_num + 1), 0 < i && (i < a[0].num ? (a[0].cancel_num = i,
                                            this.cancel_money = (i * a[0].pro_can_refound_price).toFixed(2)) : i == a[0].num && (a[0].cancel_num = i,
                                            1 == this.is_last_refund ? 1 == this.is_add_freight ? this.cancel_money = this.shen_all_can_refund_money : (this.freight_all = a[0].freight_all,
                                                this.cancel_money = this.shen_all_can_refund_money - this.freight_all) : this.cancel_money = (i * a[0].pro_can_refound_price).toFixed(2))),
                                        this.dataAll = a;
                                }
                            },
                            evaluate: function (e) {
                                var t = e.detail.value,
                                    a = e.detail.cursor;
                                this.evaluatecon = t, this.nowcount = a;
                            }
                        }
                    };
                t.default = e;
            }).call(this, a("543d").default);
        },
        e0dd: function (e, t, a) {},
        ed95: function (e, t, a) {
            a.r(t);
            var i = a("b488"),
                n = a.n(i);
            for (var o in i) "default" !== o && function (e) {
                a.d(t, e, function () {
                    return i[e];
                });
            }(o);
            t.default = n.a;
        }
    },
    [
        ["7a3f", "common/runtime", "common/vendor"]
    ]
]);