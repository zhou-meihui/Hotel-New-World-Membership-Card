// pages/buyCardList/buyCardList.js
var config = require('../../siteinfo');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cardList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getCardList()
    
  },
  
  getCardList:function (){
    console.log('走了么',config.siteroot1)
    var _this = this;
    wx.request({
      url: config.siteroot1 + "/userClubCard",
      data: {
          uniacid: config.uniacid,
          suid: wx.getStorageSync("suid"),
          uid: wx.getStorageSync("golobeuid")
      },
      success: function (e) {
        console.log(e.data,'userClubCard')
        _this.setData({
          cardList: e.data
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },
  viewErCode(e){
    // console.log(e.currentTarget.dataset.qrcode,'viewErCode')
    let qrcode = e.currentTarget.dataset.qrcode;
    let codeImg = 'https://yzhy.cg500.com/' + qrcode;
    wx.previewImage({
      urls: [codeImg],
      current: 0
    });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})