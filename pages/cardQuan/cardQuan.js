// pages/cardQuan/cardQuan.js
var config = require('../../siteinfo');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    page_signs: "/pages/cardQuan/cardQuan",
    clubList: [],
    video_url: '',
    show: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  
  onLoad(options) {
    this.getConfig()
    this.getGoods()
    this.getVideo()
    console.log(config,'config')
  },
  openWexin(){
    this.setData({
      show: true
    })
  },
  closePop(){
    this.setData({
      show: false
    })
  },
  preview(){
    wx.previewImage({
      urls: ['https://yzhy.cg500.com/image/wxin-custom.jpg'],
      current: 0
});
  },
  // 获取宣传视频
  getVideo(){
    var _this = this;
    wx.request({
      url: config.siteroot1 +"getVideo",
      data: {
        uniacid: config.uniacid
      },
      header: {
        "custom-header": "hello"
      },
      success: function (e) {
        _this.setData({
          video_url: 'https://yzhy.cg500.com' + e.data
        })

        console.log(_this.data.video_url)
      }
    })
  },
  onPlayVideo: function (){
    console.log('播放了')
  },
  onPause: function(e){
    console.log('暂停了',e)
  },
  ended: function (){
    console.log('播放完了')
  },
  toList(){
    // https://yzhy.cg500.com/api/Wxapps/doPagegetNewSessionkey?uniacid=63
  },
  tocardList(e){
    console.log(e.currentTarget.dataset.id,'tocardList')
    let id = e.currentTarget.dataset.id
    if(id == 1){
      wx.navigateTo({
        url: '/pages/index/index?pageid=420',
      })
    }else if(id == 2){
      wx.navigateTo({
        url: '/pages/index/index?pageid=424',
      })
    }
  },
  callPhone(){
    wx.makePhoneCall({
      phoneNumber: '18627185806' //仅为示例，并非真实的电话号码
    })
    
  },
  getGoods(){
    var _this = this;
    wx.request({
      url: config.siteroot +"clubCard",
      data: {
        uniacid: config.uniacid
      },
      header: {
        "custom-header": "hello"
      },
      success: function (e) {
        console.log(e.data,'11111111')
        _this.setData({
          clubList: e.data
        })
      }
    })
  },
  getConfig(){
    var _this = this;
    wx.request({
      url: config.siteroot +"doPageBaseMin",
      data: {
        uniacid: config.uniacid,
        vs1: 1,
        suid: 2
      },
      header: {
        "custom-header": "hello"
      },
      success: function (e) {
        console.log(e.data.data,'ejejejeejj')
        _this.setData({
          baseinfo: e.data.data
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})