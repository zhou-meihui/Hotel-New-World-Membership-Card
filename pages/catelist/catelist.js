(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/catelist/catelist"], {
        "29ca": function (t, e, i) {
            var a = i("8ec8");
            i.n(a).a;
        },
        "8ec8": function (t, e, i) {},
        ac2d: function (t, e, i) {
            (function (u) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var a = i("f571"),
                    t = {
                        components: {
                            list_pro_td: function () {
                                return i.e("components/plugins/list_pro_td").then(i.bind(null, "5391"));
                            },
                            list_pro: function () {
                                return i.e("components/plugins/list_pro").then(i.bind(null, "ddbc"));
                            }
                        },
                        data: function () {
                            return {
                                $host: this.$host,
                                $imgurl: this.$imgurl,
                                page: 1,
                                minHeight: 180,
                                morePro: !0,
                                ProductsList: [],
                                baseinfo: {},
                                orderOrBusiness: "order",
                                block: !1,
                                logs: [],
                                goodsH: 0,
                                scrollToGoodsView: 0,
                                toView: "",
                                toViewType: "",
                                GOODVIEWID: "catGood_",
                                animation: !0,
                                goodsNumArr: [0],
                                shoppingCart: {},
                                shoppingCartGoodsId: [],
                                goodMap: {},
                                chooseGoodArr: [],
                                totalNum: 0,
                                totalPay: 0,
                                showShopCart: !1,
                                fromClickScroll: !1,
                                timeStart: "",
                                timeEnd: "",
                                hideCount: !0,
                                count: 0,
                                needAni: !1,
                                hide_good_box: !0,
                                url: "",
                                protype: 1,
                                heighthave: 0,
                                from_top: 0,
                                cid: 0,
                                cateslide: [],
                                cate_list: "",
                                chessRoomDetail: {},
                                catHighLightIndex: 0,
                                catList: {},
                                towlist: [],
                                page_signs: "/pages/catelist/catelist?type=showProMore",
                                type: "showProMore",
                                searchtitle: "",
                                slide_is: 0,
                                slides: "",
                                swiperCurrent: 0,
                                catestyle: 0,
                                prolists: "",
                                prolists_length: 0,
                                prodataid: 0,
                                sub_cate_name: "",
                                catfour: 0,
                                fourlist: [],
                                guige: 0,
                                pid: 0,
                                proinfo: "",
                                gzjson: "",
                                grouparr: "",
                                num: 1,
                                newstr: "",
                                use_more: 0,
                                discounts: 0,
                                gwc: "",
                                gm: "",
                                products: "",
                                needAuth: !1,
                                needBind: !1,
                                tobuy: 1,
                                baseproinfo: "",
                                top_cid: 0,
                                sub_cid: 0,
                                sub_cate: [],
                                sub_cate_length: 0
                            };
                        },
                        onLoad: function (t) {
                            var e = this;
                            this.cid = t.cid, 0 < this.cid && (this.top_cid = this.cid, this.page_signs = "/pages/catelist/catelist?cid=" + this.cid),
                                this._baseMin(this);
                            var i = 0;
                            t.fxsid && (i = t.fxsid), this.fxsid = i, t.type && (this.type = t.type), u.getStorageSync("suid"),
                                a.getOpenid(i, function () {
                                    e.getList(), e._getSuperUserInfo(e);
                                });
                        },
                        onPullDownRefresh: function () {
                            this.page = 1, this.sub_cid = 0, this.top_cid = this.cid, this.getList(), u.stopPullDownRefresh();
                        },
                        methods: {
                            scrollHandler: function () {
                                this.onReachBottoms();
                            },
                            onReachBottoms: function () {
                                var e = this;
                                if (this.morePro) {
                                    var i = e.page + 1;
                                    e.top_cid, u.request({
                                        url: e.$host + "/api/mainwxapp/doPagelistAllPic",
                                        data: {
                                            page: i,
                                            type: e.type,
                                            uniacid: e.$uniacid,
                                            top_cid: e.top_cid,
                                            sub_cid: e.sub_cid,
                                            suid: u.getStorageSync("suid")
                                        },
                                        success: function (t) {
                                            "" != t.data.data.prolists ? (e.prolists = e.prolists.concat(t.data.data.prolists),
                                                e.page = i) : e.morePro = !1;
                                        }
                                    });
                                }
                            },
                            toprodetail: function (t) {
                                var e = t.currentTarget.dataset.id;
                                if ("showProMore" == this.type) var i = "/pages/showProMore/showProMore?id=" + e;
                                else i = "flashSale" == this.type ? "/pagesFlashSale/showPro/showPro?id=" + e : "/pagesReserve/proDetail/proDetail?id=" + e;
                                u.navigateTo({
                                    url: i
                                });
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                console.log("closeBind"), this.needBind = !1, this.checkApply();
                            },
                            getSuid: function () {
                                if (u.getStorageSync("suid")) return !0;
                                return u.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                    !1;
                            },
                            checkvip: function (i) {
                                var a = this,
                                    t = (u.getStorageSync("openid"), u.getStorageSync("suid"));
                                u.request({
                                    url: a.$host + "/api/MainWxapp/doPagecheckvip",
                                    data: {
                                        uniacid: a.$uniacid,
                                        kwd: "duo",
                                        suid: t,
                                        id: a.pid,
                                        gz: 1
                                    },
                                    success: function (t) {
                                        if (!1 === t.data.data) return a.needvip = !0, u.showModal({
                                            title: "进入失败",
                                            content: "使用本功能需先开通vip!",
                                            showCancel: !1,
                                            success: function (t) {
                                                t.confirm && u.redirectTo({
                                                    url: "/pages/register/register"
                                                });
                                            }
                                        }), !1;
                                        if (0 < t.data.data.needgrade)
                                            if (a.needvip = !0, 0 < t.data.data.grade)
                                                if (t.data.data.grade < t.data.data.needgrade) u.showModal({
                                                    title: "进入失败",
                                                    content: "使用本功能需成为" + t.data.data.vipname + "等级会员,请先开通会员等级!",
                                                    showCancel: !1,
                                                    success: function (t) {
                                                        t.confirm && u.redirectTo({
                                                            url: "/pages/open1/open1"
                                                        });
                                                    }
                                                });
                                                else {
                                                    var e = i.currentTarget.dataset.type;
                                                    "gwc" == e ? a.gwcget() : "gm" == e && a.gmget();
                                                }
                                        else t.data.data.grade < t.data.data.needgrade ? u.showModal({
                                            title: "进入失败",
                                            content: "使用本功能需成为" + t.data.data.vipname + "(" + t.data.data.needgrade + "级)以上等级会员,请先开通会员后再升级会员等级!",
                                            showCancel: !1,
                                            success: function (t) {
                                                t.confirm && u.redirectTo({
                                                    url: "/pages/register/register"
                                                });
                                            }
                                        }) : "gwc" == (e = 1 == a.use_more ? i.currentTarget.dataset.type : a.buy_type) ? a.gwcget() : "gm" == e && a.gmget();
                                        else "gwc" == (e = i.currentTarget.dataset.type) ? a.gwcget() : "gm" == e && a.gmget();
                                    },
                                    fail: function (t) {}
                                });
                            },
                            swiperChange: function (t) {
                                this.swiperCurrent = t.target.current;
                            },
                            handleTap: function (t) {
                                var e = t.currentTarget.id.slice(1);
                                e && (this.cid = e, this.page = 1, this.from_top = 1, this.getList(e));
                            },
                            getList: function () {
                                var i = this;
                                i.page, u.request({
                                    url: i.$host + "/api/MainWxapp/doPagelistAllPic",
                                    data: {
                                        uniacid: i.$uniacid,
                                        type: i.type,
                                        top_cid: i.top_cid,
                                        sub_cid: i.sub_cid,
                                        suid: u.getStorageSync("suid")
                                    },
                                    success: function (t) {
                                        var e = u.getStorageSync("systemInfo");
                                        i.goodsH = e.windowHeight - 55, i.slide_is = t.data.data.slide_is, i.slides = t.data.data.slides,
                                            i.catestyle = t.data.data.catestyle, i.catList = t.data.data.top_cate, i.top_cid = t.data.data.top_cid,
                                            i.sub_cate = t.data.data.sub_cate, i.sub_cate_length = t.data.data.sub_cate.length,
                                            i.sub_cid = t.data.data.sub_cid, i.prolists = t.data.data.prolists, i.prolists_length = i.prolists.length,
                                            i.sub_cate_name = t.data.data.sub_cate_name, i.prodataid = 0;
                                    },
                                    fail: function (t) {
                                        console.log(t);
                                    }
                                });
                            },
                            getList1: function () {
                                var i = this;
                                i.page, u.request({
                                    url: i.$host + "/api/MainWxapp/doPagelistAllPic",
                                    data: {
                                        uniacid: i.$uniacid,
                                        type: i.type,
                                        top_cid: i.top_cid,
                                        sub_cid: i.sub_cid,
                                        suid: u.getStorageSync("suid")
                                    },
                                    success: function (t) {
                                        var e = u.getStorageSync("systemInfo");
                                        i.goodsH = e.windowHeight - 55, i.slide_is = t.data.data.slide_is, i.slides = t.data.data.slides,
                                            i.catestyle = t.data.data.catestyle, i.catList = t.data.data.top_cate, i.top_cid = t.data.data.top_cid,
                                            i.sub_cate = t.data.data.sub_cate, i.sub_cate_length = t.data.data.sub_cate.length,
                                            i.sub_cid = t.data.data.sub_cid, i.prolists = t.data.data.prolists, i.prolists_length = i.prolists.length,
                                            i.sub_cate_name = t.data.data.sub_cate_name;
                                    },
                                    fail: function (t) {
                                        console.log(t);
                                    }
                                });
                            },
                            swiperLoad: function (a) {
                                var s = this;
                                u.getSystemInfo({
                                    success: function (t) {
                                        var e = a.detail.width / a.detail.height,
                                            i = t.windowWidth / e;
                                        s.heighthave || (s.minHeight = i, s.heighthave = 1);
                                    }
                                });
                            },
                            goodsViewScrollFn: function (t) {
                                this.getIndexFromHArr(t.detail.scrollTop);
                            },
                            getIndexFromHArr: function (t) {
                                for (var e = 0; e < this.goodsNumArr.length; e++) {
                                    var i = t - 40 * e;
                                    i >= this.goodsNumArr[e] && i < this.goodsNumArr[e + 1] && (this.fromClickScroll || (this.catHighLightIndex = e));
                                }
                                this.fromClickScroll = !1;
                            },
                            catClickFn: function (t) {
                                var e = t.currentTarget.dataset.id,
                                    i = this.catestyle;
                                1 == i || 2 == i ? e != this.top_cid && (this.top_cid = e, this.sub_cid = 0) : this.sub_cid = e,
                                    this.page = 1, this.morePro = !0, this.getList();
                            },
                            catClickright: function (t) {
                                var e = t.currentTarget.dataset.id,
                                    i = this.sub_cate[e].id;
                                i != this.sub_cid && (this.prodataid = e, this.sub_cid = i, this.page = 1, this.morePro = !0,
                                    this.getList1());
                            },
                            getProModel: function () {
                                var a = this;
                                u.request({
                                    url: a.$host + "/api/MainWxapp/doPagegetProModel",
                                    data: {
                                        uniacid: a.$uniacid,
                                        suid: u.getStorageSync("suid"),
                                        pid: a.pid
                                    },
                                    success: function (t) {
                                        a.num = 1, a.proinfo = t.data.data, a.use_more = t.data.data.use_more, a.newstr = t.data.data.arr_selected,
                                            a.discounts = t.data.data.discount, a.gzjson = t.data.data.grouparr_val, a.grouparr = t.data.data.grouparr;
                                        var e = a.grouparr;
                                        if (e)
                                            for (var i = 0; i < e.length; i++) e[i];
                                        else a.newstr = "";
                                        a.guige = 1;
                                    },
                                    fail: function (t) {
                                        console.log(t);
                                    }
                                });
                            },
                            guige_block: function (t) {
                                this.pid = t.currentTarget.dataset.id, this.getProModel();
                            },
                            guige_hidden: function () {
                                this.guige = 0;
                            },
                            getproinfo: function () {
                                for (var i = this, t = i.gzjson, e = i.grouparr, a = "", s = "", o = i.pid, n = 0; n < e.length; n++) {
                                    var d = t[e[n]].val,
                                        r = t[e[n]].ck;
                                    a += d[r] + "######", s += d[r] + ";";
                                }
                                var c = a.substring(0, a.length - 6);
                                s = s.substring(0, s.length - 1), u.request({
                                    url: i.$host + "/api/MainWxapp/doPageGuigeInfo",
                                    data: {
                                        uniacid: i.$uniacid,
                                        id: o,
                                        str: c
                                    },
                                    success: function (t) {
                                        var e = t.data.data;
                                        (e.proinfo.discount_price = 0) < i.discounts && (e.proinfo.discount_price = (e.proinfo.price * i.discounts * .1).toFixed(2) < .01 ? .01 : (e.proinfo.price * i.discounts * .1).toFixed(2)),
                                            i.proinfo = e.proinfo, i.baseproinfo = e.baseinfo, i.newstr = s, 1 == e.baseinfo.is_sale && u.showModal({
                                                title: "提示",
                                                content: "该商品已下架,请选择其他商品",
                                                showCancel: !1,
                                                success: function () {
                                                    u.redirectTo({
                                                        url: "/pages/index/index"
                                                    });
                                                }
                                            });
                                    },
                                    fail: function (t) {
                                        console.log(t);
                                    }
                                });
                            },
                            changepro: function (t) {
                                var e = this,
                                    i = t.target.dataset.id,
                                    a = (e.grouparr, t.target.dataset.index),
                                    s = e.gzjson;
                                s[i].ck = a, e.gzjson = s, e.tobuy = 0, e.getproinfo();
                            },
                            num_add: function () {
                                var t = this.proinfo.kc,
                                    e = this.num;
                                t < (e += 1) && (u.showModal({
                                    title: "提醒",
                                    content: "您的购买数量超过了库存！",
                                    showCancel: !1
                                }), e--), this.num = e;
                            },
                            num_jian: function () {
                                var t = this.num;
                                1 == t ? this.num = 1 : (t -= 1, this.num = t);
                            },
                            gmget: function () {
                                if (this.getSuid()) {
                                    var t = this,
                                        e = t.tobuy,
                                        i = t.num;
                                    if (1 == e) {
                                        var a = t.proinfo;
                                        console.log(111);
                                    } else {
                                        console.log(111), a = t.proinfo;
                                        var s = t.baseproinfo;
                                        s.id = a.id, s.kc = a.kc, a = s;
                                    }
                                    if (1 == a.is_sale && u.showModal({
                                            title: "提示",
                                            content: "该商品已下架,请选择其他商品",
                                            showCancel: !1,
                                            success: function () {
                                                u.redirectTo({
                                                    url: "/pages/index/index"
                                                });
                                            }
                                        }), 0 == a.kc) return u.showModal({
                                        title: "提醒",
                                        content: "您来晚了，已经卖完了！",
                                        showCancel: !1
                                    }), !1;
                                    if (1 == a.use_more) var o = t.pid + "|" + a.id + "|" + i;
                                    else o = t.pid + "|-1|" + i;
                                    var n = [];
                                    n.push(o), u.setStorage({
                                        key: "buydata",
                                        data: n
                                    }), u.navigateTo({
                                        url: "/pages/order_down/order_down?discounts=" + t.discounts + "&stores=" + t.stores
                                    });
                                }
                            },
                            gwcget: function () {
                                if (this.getSuid()) {
                                    var e = this,
                                        t = e.proinfo,
                                        i = e.num;
                                    if (1 == (u.getStorageSync("openid"), u.getStorageSync("suid"), e.baseproinfo).is_sale && u.showModal({
                                            title: "提示",
                                            content: "该商品已下架,请选择其他商品",
                                            showCancel: !1,
                                            success: function () {
                                                u.redirectTo({
                                                    url: "/pages/index/index"
                                                });
                                            }
                                        }), 0 == t.kc) return u.showModal({
                                        title: "提醒",
                                        content: "您来晚了，已经卖完了！",
                                        showCancel: !1
                                    }), !1;
                                    u.request({
                                        url: e.$host + "/api/MainWxapp/dopagegwcadd",
                                        data: {
                                            uniacid: this.$uniacid,
                                            suid: u.getStorageSync("suid"),
                                            id: t.id,
                                            pid: e.pid,
                                            prokc: i
                                        },
                                        success: function (t) {
                                            u.showToast({
                                                title: "加入成功",
                                                icon: "success",
                                                duration: 2e3,
                                                success: function () {
                                                    e.guige_hidden();
                                                }
                                            });
                                        }
                                    });
                                }
                            },
                            tabChange: function (t) {
                                this.orderOrBusiness = t.currentTarget.dataset.id;
                            },
                            tiaozhuang: function (t) {
                                var e = t.currentTarget.dataset.id,
                                    i = (t.currentTarget.dataset.types, t.currentTarget.dataset.ismore);
                                i = "/pages/checkPro/checkPro?cid=" + e + "&type=" + this.type + "&from_top=1",
                                    u.navigateTo({
                                        url: i
                                    });
                            },
                            serachInput: function (t) {
                                this.searchtitle = t.detail.value;
                            },
                            search: function () {
                                var t = this.searchtitle;
                                t ? "showProMore" == this.type ? u.navigateTo({
                                    url: "/pages/checkPro/checkPro?title=" + t + "&type=" + this.type
                                }) : u.navigateTo({
                                    url: "/pages/search/search?title=" + t + "&type=" + this.type
                                }) : u.showModal({
                                    title: "提示",
                                    content: "请输入搜索内容！",
                                    showCancel: !1
                                });
                            }
                        }
                    };
                e.default = t;
            }).call(this, i("543d").default);
        },
        d7c8: function (t, e, i) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                s = [];
            i.d(e, "a", function () {
                return a;
            }), i.d(e, "b", function () {
                return s;
            });
        },
        d847: function (t, e, i) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                i("020c"), i("921b"), e(i("66fd")), t(e(i("f724")).default);
            }).call(this, i("543d").createPage);
        },
        dd05: function (t, e, i) {
            i.r(e);
            var a = i("ac2d"),
                s = i.n(a);
            for (var o in a) "default" !== o && function (t) {
                i.d(e, t, function () {
                    return a[t];
                });
            }(o);
            e.default = s.a;
        },
        f724: function (t, e, i) {
            i.r(e);
            var a = i("d7c8"),
                s = i("dd05");
            for (var o in s) "default" !== o && function (t) {
                i.d(e, t, function () {
                    return s[t];
                });
            }(o);
            i("29ca");
            var n = i("2877"),
                d = Object(n.a)(s.default, a.a, a.b, !1, null, null, null);
            e.default = d.exports;
        }
    },
    [
        ["d847", "common/runtime", "common/vendor"]
    ]
]);