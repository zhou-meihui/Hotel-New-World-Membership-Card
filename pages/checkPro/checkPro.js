(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/checkPro/checkPro"], {
        "67bf": function (t, e, i) {
            i.r(e);
            var a = i("6b5d"),
                s = i("aa2b");
            for (var n in s) "default" !== n && function (t) {
                i.d(e, t, function () {
                    return s[t];
                });
            }(n);
            i("a290");
            var o = i("2877"),
                r = Object(o.a)(s.default, a.a, a.b, !1, null, null, null);
            e.default = r.exports;
        },
        "6b5d": function (t, e, i) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                s = [];
            i.d(e, "a", function () {
                return a;
            }), i.d(e, "b", function () {
                return s;
            });
        },
        "9aee": function (t, e, i) {
            (function (u) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0, i("2f62");
                var a = i("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                $host: this.$host,
                                page_signs: "/pages/checkPro/checkPro",
                                baseinfo: {},
                                cid: 0,
                                goodsH: 0,
                                shaixuanBox: 0,
                                listStyle: 0,
                                search_keys: "",
                                sort_type: 1,
                                sort_type_attr: 1,
                                is_vip_price: 0,
                                price_min: "",
                                price_max: "",
                                page: 1,
                                nextPage: 2,
                                searchtitle: "",
                                checkprolist: "",
                                checkprolist_length: 0,
                                guige: 0,
                                pid: 0,
                                proinfo: "",
                                gzjson: "",
                                grouparr: "",
                                num: 1,
                                newstr: "",
                                use_more: 0,
                                discounts: 0,
                                gwc: "",
                                gm: "",
                                needAuth: !1,
                                needBind: !1,
                                baseproinfo: "",
                                tobuy: 1
                            };
                        },
                        onLoad: function (t) {
                            var e = this;
                            this.cid = t.cid, this.page_signs = "/pages/checkPro/checkPro?cid=" + this.cid,
                                this._baseMin(this);
                            var i = 0;
                            t.fxsid && (i = t.fxsid), this.fxsid = i, t.title && (this.search_keys = t.title),
                                a.getOpenid(i, function () {
                                    e.getLists();
                                });
                        },
                        onPullDownRefresh: function () {
                            this.getLists(), u.stopPullDownRefresh();
                        },
                        methods: {
                            onReachBottoms: function () {
                                var e = this,
                                    i = e.nextPage;
                                if (i <= 1) return !1;
                                u.request({
                                    url: e.$host + "/api/MainWxapp/doPageCheckPro",
                                    data: {
                                        page: i,
                                        uniacid: e.$uniacid,
                                        cate_id: e.cid,
                                        suid: u.getStorageSync("suid"),
                                        search_keys: e.search_keys,
                                        sort_type: e.sort_type,
                                        sort_type_attr: e.sort_type_attr,
                                        is_vip_price: e.is_vip_price,
                                        price_min: e.price_min,
                                        price_max: e.price_max
                                    },
                                    success: function (t) {
                                        0 < t.data.data.length && (e.checkprolist = e.checkprolist.concat(t.data.data),
                                            e.page = i, e.nextPage = i + 1);
                                    }
                                });
                            },
                            toprodetail: function (t) {
                                var e = "/pages/showProMore/showProMore?id=" + t.currentTarget.dataset.id;
                                u.navigateTo({
                                    url: e
                                });
                            },
                            getLists: function () {
                                var i = this;
                                u.request({
                                    url: i.$host + "/api/MainWxapp/doPageCheckPro",
                                    data: {
                                        uniacid: i.$uniacid,
                                        cate_id: i.cid,
                                        suid: u.getStorageSync("suid"),
                                        search_keys: i.search_keys,
                                        sort_type: i.sort_type,
                                        sort_type_attr: i.sort_type_attr,
                                        is_vip_price: i.is_vip_price,
                                        price_min: i.price_min,
                                        price_max: i.price_max,
                                        page: i.page
                                    },
                                    success: function (t) {
                                        i.nextPage = 2;
                                        var e = u.getStorageSync("systemInfo");
                                        i.goodsH = e.windowHeight - 55, i.checkprolist = t.data.data, i.checkprolist_length = t.data.data.length;
                                    },
                                    fail: function (t) {
                                        console.log(t);
                                    }
                                });
                            },
                            showShaixuan: function () {
                                0 == this.shaixuanBox ? this.shaixuanBox = 1 : this.shaixuanBox = 0;
                            },
                            closeShaixuan: function () {
                                0 == this.shaixuanBox ? this.shaixuanBox = 1 : this.shaixuanBox = 0;
                            },
                            changeListstyle: function () {
                                var t = this;
                                0 == t.listStyle ? t.listStyle = 1 : t.listStyle = 0;
                            },
                            sortType1: function () {
                                this.sort_type = 1, 2 == this.sort_type_attr ? this.sort_type_attr = 1 : this.sort_type_attr = 2,
                                    this.getLists();
                            },
                            sortType2: function () {
                                (this.sort_type = 2) == this.sort_type_attr ? this.sort_type_attr = 1 : this.sort_type_attr = 2,
                                    this.getLists();
                            },
                            sortType3: function () {
                                this.sort_type = 3, 2 == this.sort_type_attr ? this.sort_type_attr = 1 : this.sort_type_attr = 2,
                                    this.getLists();
                            },
                            vipPrice1: function () {
                                this.is_vip_price = 1;
                            },
                            vipPrice2: function () {
                                this.is_vip_price = 2;
                            },
                            getminPrice: function (t) {
                                this.price_min = t.detail.value;
                            },
                            getmaxPrice: function (t) {
                                this.price_max = t.detail.value;
                            },
                            resetData: function () {
                                this.is_vip_price = 0, this.price_min = "", this.price_max = "";
                            },
                            saveData: function () {
                                this.shaixuanBox = 0, this.getLists();
                            },
                            serachInput: function (t) {
                                this.search_keys = t.detail.value;
                            },
                            search: function () {
                                this.page = 1, this.search_keys ? this.getLists() : u.showModal({
                                    title: "提示",
                                    content: "请输入搜索内容！",
                                    showCancel: !1
                                });
                            },
                            getSuid: function () {
                                if (u.getStorageSync("suid")) return !0;
                                return u.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                    !1;
                            },
                            checkvip: function (i) {
                                var a = this,
                                    t = (u.getStorageSync("openid"), u.getStorageSync("suid"));
                                u.request({
                                    url: a.$host + "/api/MainWxapp/doPagecheckvip",
                                    data: {
                                        uniacid: a.$uniacid,
                                        kwd: "duo",
                                        suid: t,
                                        id: a.pid,
                                        gz: 1
                                    },
                                    success: function (t) {
                                        if (!1 === t.data.data) return a.needvip = !0, u.showModal({
                                            title: "进入失败",
                                            content: "使用本功能需先开通vip!",
                                            showCancel: !1,
                                            success: function (t) {
                                                t.confirm && u.redirectTo({
                                                    url: "/pages/register/register"
                                                });
                                            }
                                        }), !1;
                                        if (0 < t.data.data.needgrade)
                                            if (a.needvip = !0, 0 < t.data.data.grade)
                                                if (t.data.data.grade < t.data.data.needgrade) u.showModal({
                                                    title: "进入失败",
                                                    content: "使用本功能需成为" + t.data.data.vipname + "(" + t.data.data.needgrade + "级)以上等级会员,请先升级!",
                                                    showCancel: !1,
                                                    success: function (t) {
                                                        t.confirm && u.redirectTo({
                                                            url: "/pages/open1/open1"
                                                        });
                                                    }
                                                });
                                                else {
                                                    var e = i.currentTarget.dataset.type;
                                                    "gwc" == e ? a.gwcget() : "gm" == e && a.gmget();
                                                }
                                        else t.data.data.grade < t.data.data.needgrade ? u.showModal({
                                            title: "进入失败",
                                            content: "使用本功能需成为" + t.data.data.vipname + "(" + t.data.data.needgrade + "级)以上等级会员,请先开通会员后再升级会员等级!",
                                            showCancel: !1,
                                            success: function (t) {
                                                t.confirm && u.redirectTo({
                                                    url: "/pages/register/register"
                                                });
                                            }
                                        }) : "gwc" == (e = 1 == a.use_more ? i.currentTarget.dataset.type : a.buy_type) ? a.gwcget() : "gm" == e && a.gmget();
                                        else "gwc" == (e = i.currentTarget.dataset.type) ? a.gwcget() : "gm" == e && a.gmget();
                                    },
                                    fail: function (t) {}
                                });
                            },
                            getProModel: function () {
                                var a = this;
                                u.request({
                                    url: a.$host + "/api/MainWxapp/doPagegetProModel",
                                    data: {
                                        uniacid: a.$uniacid,
                                        suid: u.getStorageSync("suid"),
                                        pid: a.pid
                                    },
                                    success: function (t) {
                                        a.num = 1, a.proinfo = t.data.data, a.use_more = t.data.data.use_more, a.newstr = t.data.data.arr_selected,
                                            a.discounts = t.data.data.discount, a.gzjson = t.data.data.grouparr_val, a.grouparr = t.data.data.grouparr;
                                        var e = a.grouparr;
                                        if (e)
                                            for (var i = 0; i < e.length; i++) e[i];
                                        else a.newstr = "";
                                        a.guige = 1;
                                    },
                                    fail: function (t) {
                                        console.log(t);
                                    }
                                });
                            },
                            guige_block: function (t) {
                                this.pid = t.currentTarget.dataset.id, this.getProModel();
                            },
                            guige_hidden: function () {
                                this.guige = 0;
                            },
                            getproinfo: function () {
                                for (var i = this, t = i.gzjson, e = i.grouparr, a = "", s = "", n = i.pid, o = 0; o < e.length; o++) {
                                    var r = t[e[o]].val,
                                        c = t[e[o]].ck;
                                    a += r[c] + "######", s += r[c] + ";";
                                }
                                var d = a.substring(0, a.length - 6);
                                s = s.substring(0, s.length - 1), u.request({
                                    url: i.$host + "/api/MainWxapp/doPageGuigeInfo",
                                    data: {
                                        uniacid: i.$uniacid,
                                        id: n,
                                        str: d
                                    },
                                    success: function (t) {
                                        var e = t.data.data;
                                        (e.proinfo.discount_price = 0) < i.discounts && (e.proinfo.discount_price = (e.proinfo.price * i.discounts * .1).toFixed(2) < .01 ? .01 : (e.proinfo.price * i.discounts * .1).toFixed(2)),
                                            i.proinfo = e.proinfo, i.baseproinfo = e.baseinfo, i.newstr = s, 1 == e.baseinfo.is_sale && u.showModal({
                                                title: "提示",
                                                content: "该商品已下架,请选择其他商品",
                                                showCancel: !1,
                                                success: function () {
                                                    u.redirectTo({
                                                        url: "/pages/index/index"
                                                    });
                                                }
                                            });
                                    },
                                    fail: function (t) {
                                        console.log(t);
                                    }
                                });
                            },
                            changepro: function (t) {
                                var e = this,
                                    i = t.target.dataset.id,
                                    a = (e.grouparr, t.target.dataset.index),
                                    s = e.gzjson;
                                s[i].ck = a, e.gzjson = s, e.tobuy = 0, e.getproinfo();
                            },
                            num_add: function () {
                                var t = this.proinfo.kc,
                                    e = this.num;
                                t < (e += 1) && (u.showModal({
                                    title: "提醒",
                                    content: "您的购买数量超过了库存！",
                                    showCancel: !1
                                }), e--), this.num = e;
                            },
                            num_jian: function () {
                                var t = this.num;
                                1 == t ? this.num = 1 : (t -= 1, this.num = t);
                            },
                            gmget: function () {
                                if (this.getSuid()) {
                                    var t = this,
                                        e = t.tobuy,
                                        i = t.num;
                                    if (1 == e) var a = t.proinfo;
                                    else {
                                        a = t.proinfo;
                                        var s = t.baseproinfo;
                                        s.id = a.id, s.kc = a.kc, a = s;
                                    }
                                    if (1 == a.is_sale && u.showModal({
                                            title: "提示",
                                            content: "该商品已下架,请选择其他商品",
                                            showCancel: !1,
                                            success: function () {
                                                u.redirectTo({
                                                    url: "/pages/index/index"
                                                });
                                            }
                                        }), 0 == a.kc) return u.showModal({
                                        title: "提醒",
                                        content: "您来晚了，已经卖完了！",
                                        showCancel: !1
                                    }), !1;
                                    if (1 == a.use_more) var n = t.pid + "|" + a.id + "|" + i;
                                    else n = t.pid + "|-1|" + i;
                                    var o = [];
                                    o.push(n), u.setStorage({
                                        key: "buydata",
                                        data: o
                                    }), u.navigateTo({
                                        url: "/pages/order_down/order_down?discounts=" + t.discounts + "&stores=" + t.stores
                                    });
                                }
                            },
                            gwcget: function () {
                                if (this.getSuid()) {
                                    var e = this,
                                        t = e.proinfo,
                                        i = e.num;
                                    if (1 == (u.getStorageSync("openid"), u.getStorageSync("suid"), e.baseproinfo).is_sale && u.showModal({
                                            title: "提示",
                                            content: "该商品已下架,请选择其他商品",
                                            showCancel: !1,
                                            success: function () {
                                                u.redirectTo({
                                                    url: "/pages/index/index"
                                                });
                                            }
                                        }), 0 == t.kc) return u.showModal({
                                        title: "提醒",
                                        content: "您来晚了，已经卖完了！",
                                        showCancel: !1
                                    }), !1;
                                    u.request({
                                        url: e.$host + "/api/MainWxapp/dopagegwcadd",
                                        data: {
                                            uniacid: this.$uniacid,
                                            suid: u.getStorageSync("suid"),
                                            id: t.id,
                                            pid: e.pid,
                                            prokc: i
                                        },
                                        success: function (t) {
                                            u.showToast({
                                                title: "加入成功",
                                                icon: "success",
                                                duration: 2e3,
                                                success: function () {
                                                    e.guige_hidden();
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        }
                    };
                e.default = t;
            }).call(this, i("543d").default);
        },
        a290: function (t, e, i) {
            var a = i("b69f");
            i.n(a).a;
        },
        aa2b: function (t, e, i) {
            i.r(e);
            var a = i("9aee"),
                s = i.n(a);
            for (var n in a) "default" !== n && function (t) {
                i.d(e, t, function () {
                    return a[t];
                });
            }(n);
            e.default = s.a;
        },
        b69f: function (t, e, i) {},
        be35: function (t, e, i) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                i("020c"), i("921b"), e(i("66fd")), t(e(i("67bf")).default);
            }).call(this, i("543d").createPage);
        }
    },
    [
        ["be35", "common/runtime", "common/vendor"]
    ]
]);