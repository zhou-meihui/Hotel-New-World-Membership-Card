(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/chooseStore/chooseStore"], {
        "2cf1": function (i, t, a) {
            a.r(t);
            var e = a("2fa4"),
                n = a.n(e);
            for (var s in e) "default" !== s && function (i) {
                a.d(t, i, function () {
                    return e[i];
                });
            }(s);
            t.default = n.a;
        },
        "2fa4": function (i, t, a) {
            (function (r) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var c = a("f571"),
                    i = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                storelists: "",
                                stid: "",
                                address_id: "",
                                from_gwc: 0,
                                nav: 1,
                                type: "",
                                gid: 0,
                                kuaidi: ""
                            };
                        },
                        onLoad: function (i) {
                            var t = this,
                                a = i.gid;
                            a && (this.gid = a);
                            var e = i.stid;
                            e && (this.stid = e);
                            var n = i.address_id;
                            n && (this.address_id = n);
                            var s = i.kuaidi;
                            s && (this.kuaidi = s);
                            var d = i.from_gwc;
                            d && (this.from_gwc = d);
                            var r = i.type;
                            r && (this.type = r);
                            var o = i.nav;
                            o && (this.nav = o), this._baseMin(this);
                            var u = 0;
                            i.fxsid && (u = i.fxsid), this.fxsid = u, c.getOpenid(u, function () {
                                t.getstores();
                            });
                        },
                        onPullDownRefresh: function () {
                            this.getstores();
                        },
                        methods: {
                            getstores: function () {
                                var t = this;
                                r.request({
                                    url: t.$host + "/api/MainWxapp/getTakeSelfShopList",
                                    data: {
                                        uniacid: t.$uniacid,
                                        type: this.type,
                                        gid: this.gid
                                    },
                                    success: function (i) {
                                        t.storelists = i.data.shop_list;
                                    },
                                    fail: function (i) {
                                        console.log(i);
                                    }
                                });
                            },
                            chooseStore: function (i) {
                                var t = i.currentTarget.dataset.stid,
                                    a = this.address_id,
                                    e = this.nav,
                                    n = this.from_gwc,
                                    s = this.type,
                                    d = "";
                                "mainShop" == s ? d = "/pages/order_down/order_down?from_gwc=" + n + "&nav=" + e + "&add_id=" + a + "&shop_id=" + t + "&currentTab=1" : "flashSale" == s ? d = "/pagesFlashSale/order_dan/order_dan?addressid=" + a + "&shop_id=" + t + "&nav=2&id=" + this.gid : "pt" == s ? d = "/pagesPt/order/order?addressid=" + a + "&shop_id=" + t + "&nav=2&id=" + this.gid + "&kuaidi=" + this.kuaidi : "bargain" == s && (d = "/pagesBargain/bargain_order/bargain_order?addressid=" + a + "&shop_id=" + t + "&nav=2&id=" + this.gid),
                                    r.navigateTo({
                                        url: d
                                    });
                            }
                        }
                    };
                t.default = i;
            }).call(this, a("543d").default);
        },
        "4ba2": function (i, t, a) {
            var e = function () {
                    this.$createElement;
                    this._self._c;
                },
                n = [];
            a.d(t, "a", function () {
                return e;
            }), a.d(t, "b", function () {
                return n;
            });
        },
        "593d": function (i, t, a) {
            var e = a("e9cb");
            a.n(e).a;
        },
        bb8c: function (i, t, a) {
            (function (i) {
                function t(i) {
                    return i && i.__esModule ? i : {
                        default: i
                    };
                }
                a("020c"), a("921b"), t(a("66fd")), i(t(a("c5f0")).default);
            }).call(this, a("543d").createPage);
        },
        c5f0: function (i, t, a) {
            a.r(t);
            var e = a("4ba2"),
                n = a("2cf1");
            for (var s in n) "default" !== s && function (i) {
                a.d(t, i, function () {
                    return n[i];
                });
            }(s);
            a("593d");
            var d = a("2877"),
                r = Object(d.a)(n.default, e.a, e.b, !1, null, null, null);
            t.default = r.exports;
        },
        e9cb: function (i, t, a) {}
    },
    [
        ["bb8c", "common/runtime", "common/vendor"]
    ]
]);