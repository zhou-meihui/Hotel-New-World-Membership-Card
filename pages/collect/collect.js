(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/collect/collect"], {
        "2d93": function (t, e, n) {
            (function (i) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var o = n("f571"),
                    t = {
                        components: {
                            list_pro_td: function () {
                                return n.e("components/plugins/list_pro_td").then(n.bind(null, "5391"));
                            }
                        },
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                collectlist: [],
                                collectlist_length: 0,
                                baseinfo: {},
                                morePro: !1,
                                page: 1,
                                needAuth: !1,
                                needBind: !1,
                                type: 1
                            };
                        },
                        onLoad: function (t) {
                            var e = this;
                            i.getStorageSync("suid"), this._baseMin(this);
                            var n = 0;
                            t.fxsid && (n = t.fxsid), this.fxsid = n, o.getOpenid(n, function () {
                                e.getCollect();
                            }, function () {
                                e.needAuth = !0;
                            }, function () {
                                e.needBind = !0;
                            });
                        },
                        onPullDownRefresh: function () {
                            this.getCollect(), i.stopPullDownRefresh();
                        },
                        methods: {
                            changeTab: function (t) {
                                var e = t.currentTarget.dataset.id;
                                this.type = e, this.getCollect();
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1, this.getCollect();
                            },
                            getCollect: function () {
                                var n = this,
                                    t = i.getStorageSync("suid");
                                i.request({
                                    url: n.$baseurl + "doPagegetCollect",
                                    data: {
                                        suid: t,
                                        uniacid: n.$uniacid,
                                        page: 1,
                                        type: n.type
                                    },
                                    success: function (t) {
                                        console.log(t), n.collectlist = t.data.data.list, n.collectlist_length = n.collectlist.length;
                                        var e = t.data.data.num;
                                        n.morePro = 10 < e, i.setStorageSync("isShowLoading", !1), i.hideNavigationBarLoading(),
                                            i.stopPullDownRefresh();
                                    }
                                });
                            },
                            showMore: function () {
                                var e = this,
                                    n = e.page + 1,
                                    t = i.getStorageSync("suid");
                                i.request({
                                    url: e.$baseurl + "doPagegetCollect",
                                    data: {
                                        suid: t,
                                        page: n,
                                        uniacid: e.$uniacid
                                    },
                                    success: function (t) {
                                        "" != t.data.data.list && (e.collectlist = e.collectlist.concat(t.data.data.list),
                                            e.page = n), t.data.data.num == n && (e.morePro = !1);
                                    }
                                });
                            }
                        }
                    };
                e.default = t;
            }).call(this, n("543d").default);
        },
        "4dd0": function (t, e, n) {},
        "56a9": function (t, e, n) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                o = [];
            n.d(e, "a", function () {
                return i;
            }), n.d(e, "b", function () {
                return o;
            });
        },
        6914: function (t, e, n) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                n("020c"), n("921b"), e(n("66fd")), t(e(n("c9c4")).default);
            }).call(this, n("543d").createPage);
        },
        c81b: function (t, e, n) {
            var i = n("4dd0");
            n.n(i).a;
        },
        c9c4: function (t, e, n) {
            n.r(e);
            var i = n("56a9"),
                o = n("e938");
            for (var a in o) "default" !== a && function (t) {
                n.d(e, t, function () {
                    return o[t];
                });
            }(a);
            n("c81b");
            var c = n("2877"),
                l = Object(c.a)(o.default, i.a, i.b, !1, null, null, null);
            e.default = l.exports;
        },
        e938: function (t, e, n) {
            n.r(e);
            var i = n("2d93"),
                o = n.n(i);
            for (var a in i) "default" !== a && function (t) {
                n.d(e, t, function () {
                    return i[t];
                });
            }(a);
            e.default = o.a;
        }
    },
    [
        ["6914", "common/runtime", "common/vendor"]
    ]
]);