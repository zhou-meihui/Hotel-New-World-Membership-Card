// pages/personalInfo/personalInfo.js
var config = require('../../siteinfo');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // userInfo: {},
    avatar: '',
    nickname: '',
    avatarUrl: '',
    mobile: '',
    product_id: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // let userInfo = wx.getStorageSync('golobeuser')
    this.setData({
      product_id: options.product_id
    })
  },
  getNickname(e){
    console.log(e.detail.value,'getNickname')
    this.setData({
      nickname: e.detail.value
    })
  },
  getMoblie(e){
    console.log(e.detail.value,'getMoblie')
    this.setData({
      mobile: e.detail.value
    })
  },
  updataInfo(){
    if(this.data.nickname == ''){
      wx.showToast({
        title: '姓名不能为空',
        icon: 'none'
      })
      return
    }
    if(this.data.mobile == ''){
      wx.showToast({
        title: '手机号不能为空',
        icon: 'none'
      })
      return
    }
    const regex = /^1[3456789]\d{9}$/;  
    if(!regex.test(this.data.mobile)){
      wx.showToast({
        title: '手机号格式不正确',
        icon: 'none'
      })
      return
    }
    var _this = this;
    wx.showLoading({
      title: '上传中',
    })
    // wx.request({
    //   url: config.siteroot1 + "getClubCard",
    //   data: {
    //     uniacid: config.uniacid,
    //     suid: wx.getStorageSync('suid'),
    //     uid: wx.getStorageSync('golobeuid'),
    //     name: this.data.nickname,
    //     mobile: this.data.mobile,
    //     product_id: this.data.product_id
    //   },
    //   header: {
    //     "custom-header": "hello"
    //   },
    //   success: function (e) {
    //     console.log(e, 'getClubCard')
    //     if (e.data.err) {
    //       wx.showToast({
    //         title: e.data.msg,
    //         icon: 'none'
    //       })
    //       return
    //     }
    //     wx.hideLoading()
    //     wx.showToast({
    //       title: '提交成功',
    //       icon: 'none'
    //     })
    //     setTimeout(()=>{
    //       wx.redirectTo({
    //         url: '/pages/myCouponNew/myCouponNew',
    //       })
    //     },1000)
       
    //     // _this.setData({
    //     //   show: false
    //     // })
    //     // _this.getList()
    //   }
    // })
    wx.hideLoading()
    wx.showToast({
      title: '提交成功',
      icon: 'none'
    })
    setTimeout(()=>{
      wx.redirectTo({
        url: '/pages/myCouponNew/myCouponNew',
      })
    },1000)
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})