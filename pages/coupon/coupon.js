(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/coupon/coupon"], {
        "13ff": function (t, e, n) {
            (function (u) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var i = n("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                couponlist: [],
                                couponlist_lenght: 0,
                                baseinfo: {},
                                page_signs: "/pages/coupon/coupon",
                                needAuth: !1,
                                needBind: !1,
                                kaiq: 0,
                                disabled: !1,
                                hide_type: 0,
                                couid: ""
                            };
                        },
                        onLoad: function (t) {
                            var e = this;
                            u.getStorageSync("suid"), this.checkvip(), this._baseMin(this);
                            var n = 0;
                            t.fxsid && (n = t.fxsid), this.fxsid = n, i.getOpenid(n, function () {
                                e.getList(), e.couset();
                            }, function () {
                                e.needAuth = !0;
                            }, function () {
                                e.needBind = !0;
                            });
                        },
                        onPullDownRefresh: function () {
                            this.getList(), this.couset(), u.stopPullDownRefresh();
                        },
                        methods: {
                            showType: function (t) {
                                this.couid = t.currentTarget.dataset.couid, 0 == this.hide_type ? this.hide_type = 1 : this.hide_type = 0;
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1, this.getList(), this.couset();
                            },
                            getSuid: function () {
                                if (u.getStorageSync("suid")) return !0;
                                return u.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                    !1;
                            },
                            checkvip: function (t) {
                                var e = this,
                                    n = u.getStorageSync("suid");
                                u.request({
                                    url: e.$baseurl + "doPagecheckvip",
                                    data: {
                                        uniacid: e.$uniacid,
                                        kwd: "coupon",
                                        suid: n
                                    },
                                    success: function (t) {
                                        if (!1 === t.data.data) return e.needvip = !0, u.showModal({
                                            title: "进入失败",
                                            content: "使用本功能需先开通vip!",
                                            showCancel: !1,
                                            success: function (t) {
                                                t.confirm && u.redirectTo({
                                                    url: "/pages/register/register"
                                                });
                                            }
                                        }), !1;
                                    },
                                    fail: function (t) {}
                                });
                            },
                            getList: function () {
                                var e = this;
                                u.request({
                                    url: this.$baseurl + "doPageCoupon",
                                    data: {
                                        suid: u.getStorageSync("suid"),
                                        uniacid: this.$uniacid
                                    },
                                    success: function (t) {
                                        e.couponlist = t.data.data, e.couponlist_lenght = t.data.data.length, u.hideNavigationBarLoading(),
                                            u.stopPullDownRefresh();
                                    }
                                });
                            },
                            getit_zj: i.throttle(function (t) {
                                if (!this.disabled) {
                                    if (!this.getSuid()) return !1;
                                    this.disabled = !0;
                                    var n = this,
                                        i = t.currentTarget.dataset.id;
                                    u.getStorage({
                                        key: "suid",
                                        success: function (t) {
                                            var e = t.data;
                                            e && u.request({
                                                url: n.$baseurl + "doPagegetcoupon",
                                                data: {
                                                    id: i,
                                                    suid: e,
                                                    uniacid: n.$uniacid
                                                },
                                                success: function (t) {
                                                    1 == t.data.data && u.showToast({
                                                        title: "领取成功",
                                                        icon: "success",
                                                        duration: 2e3,
                                                        success: function () {
                                                            setTimeout(function () {
                                                                n.getList(), n.disabled = !1;
                                                            }, 500);
                                                        }
                                                    }), 2 == t.data.data && u.showToast({
                                                        title: "领取失败",
                                                        icon: "loading",
                                                        duration: 2e3,
                                                        success: function () {
                                                            setTimeout(function () {
                                                                n.getList(), n.disabled = !1;
                                                            }, 500);
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            }, 1e3),
                            mycoupp: function () {
                                u.redirectTo({
                                    url: "/pages/mycoupon/mycoupon"
                                });
                            },
                            couset: function () {
                                var n = this;
                                u.request({
                                    url: n.$baseurl + "doPagecouponset",
                                    data: {
                                        uniacid: n.$uniacid
                                    },
                                    success: function (t) {
                                        var e;
                                        e = t.data.data ? t.data.data.flag : 1, n.kaiq = e;
                                    }
                                });
                            }
                        }
                    };
                e.default = t;
            }).call(this, n("543d").default);
        },
        "33e1": function (t, e, n) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                n("020c"), n("921b"), e(n("66fd")), t(e(n("d828")).default);
            }).call(this, n("543d").createPage);
        },
        "6ee0": function (t, e, n) {
            n.r(e);
            var i = n("13ff"),
                u = n.n(i);
            for (var o in i) "default" !== o && function (t) {
                n.d(e, t, function () {
                    return i[t];
                });
            }(o);
            e.default = u.a;
        },
        ac63: function (t, e, n) {
            var i = n("fe8a");
            n.n(i).a;
        },
        b582: function (t, e, n) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                u = [];
            n.d(e, "a", function () {
                return i;
            }), n.d(e, "b", function () {
                return u;
            });
        },
        d828: function (t, e, n) {
            n.r(e);
            var i = n("b582"),
                u = n("6ee0");
            for (var o in u) "default" !== o && function (t) {
                n.d(e, t, function () {
                    return u[t];
                });
            }(o);
            n("ac63");
            var a = n("2877"),
                s = Object(a.a)(u.default, i.a, i.b, !1, null, null, null);
            e.default = s.exports;
        },
        fe8a: function (t, e, n) {}
    },
    [
        ["33e1", "common/runtime", "common/vendor"]
    ]
]);