(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/diypage/diypage"], {
        "0d6e": function (t, e, n) {},
        "14d9": function (t, e, n) {
            n.r(e);
            var i = n("d98a"),
                o = n.n(i);
            for (var a in i) "default" !== a && function (t) {
                n.d(e, t, function () {
                    return i[t];
                });
            }(a);
            e.default = o.a;
        },
        "236d": function (t, e, n) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                o = [];
            n.d(e, "a", function () {
                return i;
            }), n.d(e, "b", function () {
                return o;
            });
        },
        5841: function (t, e, n) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                n("020c"), n("921b"), e(n("66fd")), t(e(n("6c89")).default);
            }).call(this, n("543d").createPage);
        },
        "6c89": function (t, e, n) {
            n.r(e);
            var i = n("236d"),
                o = n("14d9");
            for (var a in o) "default" !== a && function (t) {
                n.d(e, t, function () {
                    return o[t];
                });
            }(a);
            n("d958");
            var s = n("2877"),
                u = Object(s.a)(o.default, i.a, i.b, !1, null, null, null);
            e.default = u.exports;
        },
        d958: function (t, e, n) {
            var i = n("0d6e");
            n.n(i).a;
        },
        d98a: function (t, n, o) {
            (function (c) {
                Object.defineProperty(n, "__esModule", {
                    value: !0
                }), n.default = void 0, o("2f62");
                var t, r = (t = o("3584")) && t.__esModule ? t : {
                    default: t
                };
                var i = o("f571"),
                    e = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                $host: this.$host,
                                pageid: "",
                                foot_is: 1,
                                list: [],
                                page_signs: "/pages/index/index",
                                miniad: "",
                                baseinfo: "",
                                showAd: 1,
                                showFad: 1,
                                searchtitle: "",
                                date_c: "",
                                time_c: "",
                                store: [],
                                currentId: 1,
                                minHeight: 220,
                                heighthave: 0,
                                content: [],
                                status: 0,
                                menuContent: [{
                                    title: "全部分类",
                                    content: ["全部分类"]
                                }, {
                                    title: "综合排序",
                                    content: ["综合排序", "距离最近"]
                                }, {
                                    title: "所有商家",
                                    content: ["所有商家", "优选商家"]
                                }],
                                showMenu: !0,
                                lastIndex: null,
                                times: 1,
                                longitude: "",
                                latitude: "",
                                yuyin: 1,
                                tabArr2: {
                                    curHdIndex: 0
                                },
                                formimg: [],
                                zhixin: !1,
                                show_page_tcgg: 1,
                                forminfo: [],
                                pd_val: [],
                                footmenu: 0,
                                footmenuh: 0,
                                foottext: 0,
                                page_is: 1,
                                homepageid: 2,
                                shanghu_goods: [],
                                page: "",
                                source: "",
                                sv_diy: 0,
                                pageset: {},
                                sec: -1,
                                formdescs: "",
                                newSessionKey: "",
                                msmkdata: "",
                                formid: "",
                                mstime: [],
                                list2: [],
                                ischecked: !1,
                                bartime: [],
                                markers: [],
                                counts: [],
                                yhqCounts: []
                            };
                        },
                        components: {
                            banner: function () {
                                return o.e("components/diy_components/banner").then(o.bind(null, "f938"));
                            },
                            mlist: function () {
                                return o.e("components/diy_components/mlist").then(o.bind(null, "36ec"));
                            },
                            goods: function () {
                                return o.e("components/diy_components/goods").then(o.bind(null, "0e6b"));
                            },
                            listdesc: function () {
                                return o.e("components/diy_components/listdesc").then(o.bind(null, "d327"));
                            },
                            menulist: function () {
                                return o.e("components/diy_components/menulist").then(o.bind(null, "9482"));
                            },
                            title2: function () {
                                return o.e("components/diy_components/title2").then(o.bind(null, "8ec3"));
                            },
                            classfit: function () {
                                return o.e("components/diy_components/classfit").then(o.bind(null, "7e35"));
                            },
                            menu2: function () {
                                return o.e("components/diy_components/menu2").then(o.bind(null, "4aee"));
                            },
                            ddlb: function () {
                                return o.e("components/diy_components/ddlb").then(o.bind(null, "ce46"));
                            },
                            anniu: function () {
                                return o.e("components/diy_components/anniu").then(o.bind(null, "7f54"));
                            },
                            multiple: function () {
                                return o.e("components/diy_components/multiple").then(o.bind(null, "78d0"));
                            },
                            cases: function () {
                                return o.e("components/diy_components/cases").then(o.bind(null, "2fbd"));
                            },
                            dnfw: function () {
                                return o.e("components/diy_components/dnfw").then(o.bind(null, "2e94"));
                            },
                            xnlf: function () {
                                return o.e("components/diy_components/xnlf").then(o.bind(null, "48f9"));
                            },
                            wyrq: function () {
                                return o.e("components/diy_components/wyrq").then(o.bind(null, "c1fc"));
                            },
                            logo: function () {
                                return o.e("components/diy_components/logo").then(o.bind(null, "760b"));
                            },
                            service: function () {
                                return o.e("components/diy_components/service").then(o.bind(null, "40b3"));
                            },
                            listmenu: function () {
                                return o.e("components/diy_components/listmenu").then(o.bind(null, "32a9"));
                            },
                            joblist: function () {
                                return o.e("components/diy_components/joblist").then(o.bind(null, "c551"));
                            },
                            xfk: function () {
                                return o.e("components/diy_components/xfk").then(o.bind(null, "bd44"));
                            },
                            contact: function () {
                                return o.e("components/diy_components/contact").then(o.bind(null, "728f"));
                            },
                            bigimg: function () {
                                return o.e("components/diy_components/bigimg").then(o.bind(null, "5222"));
                            },
                            page_line: function () {
                                return o.e("components/diy_components/line").then(o.bind(null, "743c"));
                            },
                            blank: function () {
                                return o.e("components/diy_components/blank").then(o.bind(null, "e4cb"));
                            },
                            picturew: function () {
                                return o.e("components/diy_components/picturew").then(o.bind(null, "6445"));
                            },
                            notice: function () {
                                return o.e("components/diy_components/notice").then(o.bind(null, "0237"));
                            },
                            ssk: function () {
                                return o.e("components/diy_components/ssk").then(o.bind(null, "ac0b"));
                            },
                            yhq: function () {
                                return o.e("components/diy_components/yhq").then(o.bind(null, "5ac4"));
                            },
                            diyform: function () {
                                return o.e("components/diyform/diyform").then(o.bind(null, "b16e"));
                            },
                            supply: function () {
                                return o.e("components/diy_components/supply").then(o.bind(null, "bcf3"));
                            },
                            msmk: function () {
                                return o.e("components/diy_components/msmk").then(o.bind(null, "5397"));
                            },
                            pt: function () {
                                return o.e("components/diy_components/pt").then(o.bind(null, "f4f8"));
                            },
                            reserve: function () {
                                return o.e("components/diy_components/reserve").then(o.bind(null, "15d1"));
                            },
                            personlist: function () {
                                return o.e("components/diy_components/personlist").then(o.bind(null, "dd04"));
                            },
                            bargain: function () {
                                return o.e("components/diy_components/bargain").then(o.bind(null, "b8b7"));
                            },
                            yhqgoods: function () {
                                return o.e("components/diy_components/yhqgoods").then(o.bind(null, "40d4"));
                            }
                        },
                        onReady: function () {
                            this.audioCtx = c.createAudioContext("myAudio");
                        },
                        onLoad: function (t) {
                            this._baseMin(this), this.source = 1, this.refreshSessionkey();
                            var e = 0;
                            t.fxsid && (e = t.fxsid, c.setStorageSync("fxsid", e)), this.fxsid = e;
                            var n = t.pageid;
                            this.pageid = n, c.getStorageSync("pageid") && (this.pageid = c.getStorageSync("pageid")),
                                null != n && "undefined" != n ? (this.getPage(n), this.page_is = 2, this.pageid = n,
                                    this.page_signs = "/pages/index/index?pageid=" + n) : this.homepage();
                        },
                        onPullDownRefresh: function () {
                            this.refreshSessionkey(), this._baseMin(this);
                            var t = this.pageid;
                            null != t && "undefined" != t ? this.getPage(t) : this.homepage(), c.stopPullDownRefresh();
                        },
                        onShareAppMessage: function () {
                            return {
                                title: that.page.title,
                                desc: "小程序",
                                path: "pages/index/index"
                            };
                        },
                        methods: {
                            homepage: function () {
                                var n = this;
                                c.request({
                                    url: n.$baseurl + "doPagehomepage",
                                    data: {
                                        uniacid: n.$uniacid,
                                        source: n.source
                                    },
                                    header: {
                                        "content-type": "application/json"
                                    },
                                    success: function (t) {
                                        if (-1 == t.data.data) c.showModal({
                                            title: "提醒",
                                            content: "抱歉, 您暂无使用该小程序权限!",
                                            showCancel: !1,
                                            success: function (t) {
                                                return !1;
                                            }
                                        });
                                        else {
                                            var e = t.data.data.pageid;
                                            0 == e ? c.showModal({
                                                title: "提醒",
                                                content: "diy布局没有设置首页，无法进入",
                                                showCancel: !1,
                                                success: function (t) {
                                                    return !1;
                                                }
                                            }) : (n.getPage(e), i.getOpenid(n.fxsid, function () {})), n.pageid = e, n.homepageid = e;
                                        }
                                    },
                                    fail: function (t) {
                                        console.log(t);
                                    }
                                });
                            },
                            getPage: function (t) {
                                var d = this;
                                c.request({
                                    url: d.$baseurl + "dopageGetitems",
                                    data: {
                                        uniacid: d.$uniacid,
                                        pageid: t,
                                        source: c.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        var e = t.data.data;
                                        if (d.list = e.items, 3 == e) return d.homepage(), !1;
                                        for (var n = e.items, i = 0; i < n.length; i++) "multiple" != n[i].id && "mlist" != n[i].id || c.getStorageSync("latitude") && c.getStorageSync("longitude") || c.getLocation({
                                            type: "wgs84",
                                            success: function (t) {
                                                d.latitude = t.latitude, d.longitude = t.longitude, c.setStorageSync("latitude", t.latitude),
                                                    c.setStorageSync("longitude", t.longitude), d.allShopList();
                                            },
                                            fail: function (t) {
                                                d.getlocation(), d.allShopList();
                                            }
                                        });
                                        for (i = 0; i < n.length; i++) "msmk" == n[i].id && d.diyGetMsmk(i, n[i].params.sourceid, n[i].params.goodsnum, n[i].params.con_type, n[i].params.con_key),
                                            "pt" == n[i].id && d.diyGetPt(i, n[i].params.sourceid, n[i].params.goodsnum, n[i].params.con_type, n[i].params.con_key),
                                            "cases" == n[i].id && d.diyGetCases(i, n[i].params.sourceid, n[i].params.casenum, n[i].params.con_type, n[i].params.con_key, n[i].params.showtype),
                                            "listdesc" == n[i].id && d.diyGetListdesc(i, n[i].params.sourceid, n[i].params.newsnum, n[i].params.con_type, n[i].params.con_key, n[i].params.showtype),
                                            "notice" == n[i].id && d.diyGetNotice(i, n[i].params.sourceid, n[i].params.noticenum, n[i].params.noticedata),
                                            "goods" == n[i].id && d.diyGetGoods(i, n[i].params.sourceid, n[i].params.goodsnum, n[i].params.con_type, n[i].params.con_key),
                                            "feedback" == n[i].id && d.diyGetFeedback(n[i].params.sourceid), "ddlb" == n[i].id && d.diyGetDdlb(i),
                                            "reserve" == n[i].id && d.diyGetReserve(i, n[i].params.sourceid, n[i].params.goodsnum, n[i].params.con_type, n[i].params.con_key),
                                            "yhq" == n[i].id && d.diyGetYhq(i, n[i].style.counts), "xnlf" == n[i].id && d.diyGetXnlf(i, n[i].params.fwl, n[i].params.backgroundimg),
                                            "multiple" != n[i].id && "mlist" != n[i].id || ("multiple" == n[i].id && d.diyGetMutiple(i, n[i].style.showtype, n[i].params.counts, n[i].params.content_type),
                                                "mlist" == n[i].id && d.diyGetMlist(i, n[i].style.viewcount, n[i].params.content_type)),
                                            "bargain" == n[i].id && d.diyGetBargain(i, n[i].params.sourceid, n[i].params.goodsnum, n[i].params.con_type, n[i].params.con_key),
                                            "personlist" == n[i].id && d.diyGetPersionlist(i, n[i].params.goodsnum), "supply" == n[i].id && d.diyGetSupply(i, n[i].params.newsnum, n[i].params.data_types, n[i].params.supply),
                                            "video" == n[i].id && d.diyGetVideo(i, n[i].params.videourl), "dt" == n[i].id && (d.markers = [{
                                                iconPath: d.$imgurl + "dtdw1.png",
                                                id: 0,
                                                latitude: n[i].params.zzb,
                                                longitude: n[i].params.hzb,
                                                width: 30,
                                                height: 30,
                                                title: "111",
                                                callout: {
                                                    content: n[i].params.title + n[i].params.wzmc,
                                                    color: "#fff",
                                                    fontSize: 12,
                                                    borderRadius: 5,
                                                    bgColor: "#31DC8F",
                                                    padding: 10,
                                                    display: "ALWAYS",
                                                    textAlign: "center"
                                                }
                                            }]), "richtext" == n[i].id && (n[i].richtext = (0, r.default)(n[i].richtext));
                                        var o = t.data.data.pageset,
                                            a = t.data.data.page;
                                        if (null != o && 1 == o.go_home) {
                                            var s = o.kp_m;
                                            0 < s && (d.sec = s, d.countdown());
                                        }
                                        d.pageset = o, d.page = a, d.audioPlay1(), n = t.data.data.items, c.setNavigationBarTitle({
                                            title: a.title
                                        });
                                        var u = 1 == a.topcolor ? "#000000" : "#ffffff";
                                        c.setNavigationBarColor({
                                            frontColor: u,
                                            backgroundColor: a.topbackground,
                                            animation: {
                                                duration: 400,
                                                timingFunc: "easeIn"
                                            }
                                        }), c.hideLoading();
                                    },
                                    fail: function (t) {
                                        c.hideLoading(), c.showModal({
                                            title: "提示",
                                            content: "加载失败"
                                        });
                                    }
                                });
                            },
                            diyGetMsmk: function (t, e, n, i, o) {
                                var a = this;
                                c.request({
                                    url: a.$host + "/api/Plugin/msmk",
                                    data: {
                                        uniacid: a.$uniacid,
                                        sourceid: e || 0,
                                        count: n,
                                        con_type: i,
                                        con_key: o
                                    },
                                    success: function (t) {
                                        a.msmkdata = t.data.data, a.daojishi();
                                    }
                                });
                            },
                            diyGetPt: function (e, t, n, i, o) {
                                var a = this;
                                c.request({
                                    url: a.$host + "/api/Plugin/pt",
                                    data: {
                                        uniacid: a.$uniacid,
                                        sourceid: t || 0,
                                        count: n,
                                        con_type: i,
                                        con_key: o,
                                        source: c.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        a.list[e].data = t.data.data;
                                    }
                                });
                            },
                            diyGetCases: function (e, t, n, i, o, a) {
                                var s = this;
                                c.request({
                                    url: s.$host + "/api/Plugin/cases",
                                    data: {
                                        uniacid: s.$uniacid,
                                        sourceid: t || 0,
                                        count: n,
                                        con_type: i,
                                        con_key: o,
                                        showtype: a
                                    },
                                    success: function (t) {
                                        s.list[e].data = t.data.data;
                                    }
                                });
                            },
                            diyGetListdesc: function (e, t, n, i, o, a) {
                                var s = this;
                                c.request({
                                    url: s.$host + "/api/Plugin/listdesc",
                                    data: {
                                        uniacid: s.$uniacid,
                                        sourceid: t || 0,
                                        count: n,
                                        con_type: i,
                                        con_key: o,
                                        showtype: a
                                    },
                                    success: function (t) {
                                        s.list[e].data = t.data.data;
                                    }
                                });
                            },
                            diyGetNotice: function (e, t, n, i) {
                                var o = this;
                                c.request({
                                    url: o.$host + "/api/Plugin/notice",
                                    data: {
                                        uniacid: o.$uniacid,
                                        sourceid: t || 0,
                                        count: n,
                                        noticedata: i
                                    },
                                    success: function (t) {
                                        o.list[e].data = t.data.data;
                                    }
                                });
                            },
                            diyGetFeedback: function (t) {
                                c.setStorageSync("form_source", t);
                                var e = this;
                                c.request({
                                    url: e.$host + "/api/Plugin/feedback",
                                    data: {
                                        uniacid: e.$uniacid,
                                        sourceid: t || 0
                                    },
                                    success: function (t) {
                                        e.forminfo = t.data.data.forminfo.tp_text, e.formid = t.data.data.forminfo.id, e.formname = t.data.data.forminfo.formname,
                                            e.formdescs = t.data.data.forminfo.descs;
                                    }
                                });
                            },
                            diyGetDdlb: function (e) {
                                var n = this;
                                c.request({
                                    url: n.$host + "/api/Plugin/ddlb",
                                    data: {
                                        uniacid: n.$uniacid,
                                        source: c.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        n.list[e].count = t.data.data.count, n.counts = t.data.data.count;
                                    }
                                });
                            },
                            diyGetGoods: function (e, t, n, i, o) {
                                var a = this;
                                c.request({
                                    url: a.$host + "/api/Plugin/goods",
                                    data: {
                                        uniacid: a.$uniacid,
                                        sourceid: t || 0,
                                        count: n,
                                        con_type: i,
                                        con_key: o
                                    },
                                    success: function (t) {
                                        a.list[e].data = t.data.data;
                                    }
                                });
                            },
                            diyGetReserve: function (e, t, n, i, o) {
                                var a = this;
                                c.request({
                                    url: a.$host + "/api/Plugin/reserve",
                                    data: {
                                        uniacid: a.$uniacid,
                                        sourceid: t || 0,
                                        goodsnum: n,
                                        con_type: i,
                                        con_key: o
                                    },
                                    success: function (t) {
                                        a.list[e].data = t.data.data;
                                    }
                                });
                            },
                            diyGetYhq: function (e, t) {
                                var n = this;
                                c.request({
                                    url: n.$host + "/api/Plugin/yhq",
                                    data: {
                                        uniacid: n.$uniacid,
                                        counts: t
                                    },
                                    success: function (t) {
                                        n.list[e].coupon = t.data.data, n.yhqCounts = t.data.data;
                                    }
                                });
                            },
                            diyGetXnlf: function (e, t, n) {
                                var i = this;
                                c.request({
                                    url: i.$host + "/api/Plugin/xnlf",
                                    data: {
                                        uniacid: i.$uniacid,
                                        fwl: t,
                                        backgroundimg: n,
                                        souce: c.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        i.list[e].avatars = t.data.data.avatars, i.list[e].params.fwl = t.data.data.fwl;
                                    }
                                });
                            },
                            diyGetMutiple: function (e, t, n, i) {
                                var o = this;
                                c.request({
                                    url: o.$host + "/api/Plugin/multiple",
                                    data: {
                                        uniacid: o.$uniacid,
                                        showtype: t,
                                        counts: n,
                                        content_type: i
                                    },
                                    success: function (t) {
                                        o.list[e].data = t.data.data;
                                    }
                                });
                            },
                            diyGetMlist: function (t, e, n) {
                                var o = this;
                                c.request({
                                    url: o.$host + "/api/Plugin/mlist",
                                    data: {
                                        uniacid: o.$uniacid,
                                        viewcount: e,
                                        content_type: n
                                    },
                                    success: function (t) {
                                        var e = t.data.data.catelist;
                                        if (e)
                                            for (var n = o.menuContent, i = 0; i < e.length; i++) n[0].content.push(e[i].name);
                                        o.menuContent = n, o.allShopList();
                                    }
                                });
                            },
                            diyGetBargain: function (t, e, n, i, o) {
                                var a = this;
                                c.request({
                                    url: a.$host + "/api/Plugin/bargain",
                                    data: {
                                        uniacid: a.$uniacid,
                                        sourceid: e || 0,
                                        goodsnum: n,
                                        con_type: i,
                                        con_key: o
                                    },
                                    success: function (t) {
                                        a.list2 = t.data.data.data2, a.daojishi_bar();
                                    }
                                });
                            },
                            diyGetPersionlist: function (e, t) {
                                var n = this;
                                c.request({
                                    url: n.$host + "/api/Plugin/personlist",
                                    data: {
                                        uniacid: n.$uniacid,
                                        goodsnum: t
                                    },
                                    success: function (t) {
                                        n.list[e].data = t.data.data;
                                    }
                                });
                            },
                            diyGetSupply: function (e, t, n, i) {
                                var o = this;
                                c.request({
                                    url: o.$host + "/api/Plugin/supply",
                                    data: {
                                        uniacid: o.$uniacid,
                                        newsnum: t,
                                        data_types: n,
                                        supply: i
                                    },
                                    success: function (t) {
                                        o.list[e].data = t.data.data;
                                    }
                                });
                            },
                            diyGetVideo: function (e, t) {
                                var n = this;
                                c.request({
                                    url: n.$host + "/api/Plugin/video",
                                    data: {
                                        uniacid: n.$uniacid,
                                        videourl: t
                                    },
                                    success: function (t) {
                                        n.list[e].params.videourl = t.data.data.videourl;
                                    }
                                });
                            },
                            allShopList: function () {
                                var t = this;
                                c.request({
                                    url: t.$baseurl + "doPageselectShopList",
                                    data: {
                                        uniacid: t.$uniacid,
                                        option1: t.menuContent[0].title,
                                        option2: t.menuContent[1].title,
                                        option3: t.menuContent[2].title,
                                        longitude: t.longitude,
                                        latitude: t.latitude
                                    },
                                    success: function (t) {
                                        c.setStorageSync("index_store", t.data.data);
                                    }
                                });
                            },
                            redirectto: function (t) {
                                var e = t.currentTarget.dataset.link,
                                    n = t.currentTarget.dataset.linktype;
                                this._redirectto(e, n);
                            },
                            daojishi: function () {
                                for (var t = this, e = [], n = t.msmkdata, i = 0; i < n.length; i++) {
                                    var o = new Date().getTime();
                                    if (0 == n[i].sale_time && 0 == n[i].sale_end_time);
                                    else if (1e3 * n[i].sale_time > o) n[i].t_flag = 1;
                                    else if (1e3 * n[i].sale_end_time < o) n[i].t_flag = 2;
                                    else if (n[i].sale_end_time <= 0) n[i].endtime = 0;
                                    else {
                                        var a, s, u, d, c = 1e3 * parseInt(n[i].sale_end_time) - o;
                                        if (0 <= c && (a = Math.floor(c / 1e3 / 60 / 60 / 24), s = Math.floor(c / 1e3 / 60 / 60 % 24) < 10 ? "0" + Math.floor(c / 1e3 / 60 / 60 % 24) : Math.floor(c / 1e3 / 60 / 60 % 24),
                                                u = Math.floor(c / 1e3 / 60 % 60) < 10 ? "0" + Math.floor(c / 1e3 / 60 % 60) : Math.floor(c / 1e3 / 60 % 60),
                                                d = Math.floor(c / 1e3 % 60) < 10 ? "0" + Math.floor(c / 1e3 % 60) : Math.floor(c / 1e3 % 60)),
                                            0 < a) {
                                            n[i].endtime = a + "天" + s + ":" + u + ":" + d;
                                            var r = a + "天" + s + ":" + u + ":" + d;
                                        } else n[i].endtime = s + ":" + u + ":" + d, r = s + ":" + u + ":" + d;
                                        e[i] = r, t.mstime = e;
                                    }
                                }
                                t.msmkdata = n, setTimeout(function () {
                                    t.daojishi();
                                }, 1e3);
                            },
                            daojishi_bar: function () {
                                for (var t = this, e = [], n = t.list2, i = 0; i < n.length; i++) {
                                    var o = new Date().getTime();
                                    if (0 == n[i].sale_time && 0 == n[i].sale_end_time);
                                    else if (1e3 * n[i].sale_time > o) n[i].t_flag = 1;
                                    else if (1e3 * n[i].sale_end_time < o) n[i].t_flag = 2;
                                    else if (n[i].sale_end_time <= 0) n[i].endtime = 0;
                                    else {
                                        var a, s, u, d, c = 1e3 * parseInt(n[i].sale_end_time) - o;
                                        if (0 <= c && (a = Math.floor(c / 1e3 / 60 / 60 / 24), s = Math.floor(c / 1e3 / 60 / 60 % 24) < 10 ? "0" + Math.floor(c / 1e3 / 60 / 60 % 24) : Math.floor(c / 1e3 / 60 / 60 % 24),
                                                u = Math.floor(c / 1e3 / 60 % 60) < 10 ? "0" + Math.floor(c / 1e3 / 60 % 60) : Math.floor(c / 1e3 / 60 % 60),
                                                d = Math.floor(c / 1e3 % 60) < 10 ? "0" + Math.floor(c / 1e3 % 60) : Math.floor(c / 1e3 % 60)),
                                            0 < a) {
                                            n[i].endtime = a + "天" + s + ":" + u + ":" + d;
                                            var r = a + "天" + s + ":" + u + ":" + d;
                                        } else n[i].endtime = s + ":" + u + ":" + d, r = s + ":" + u + ":" + d;
                                        e[i] = r, t.bartime = e;
                                    }
                                }
                                t.list2 = n, setTimeout(function () {
                                    t.daojishi_bar();
                                }, 1e3);
                            },
                            audioPlay1: function () {},
                            audioPlay: function (t) {
                                1 == this.yuyin ? (this.audioCtx.play(), this.yuyin = 2) : (this.audioCtx.pause(),
                                    this.yuyin = 1);
                            },
                            getlocation: function () {
                                c.getSetting({
                                    success: function (t) {
                                        t.authSetting["scope.userLocation"] || c.showModal({
                                            title: "请授权获取当前位置",
                                            content: "附近店铺需要授权此功能",
                                            showCancel: !1,
                                            success: function (t) {
                                                t.confirm && c.openSetting({
                                                    success: function (t) {
                                                        !0 === t.authSetting["scope.userLocation"] ? c.showToast({
                                                            title: "授权成功",
                                                            icon: "success",
                                                            duration: 1e3
                                                        }) : c.showToast({
                                                            title: "授权失败",
                                                            icon: "success",
                                                            duration: 1e3,
                                                            success: function () {
                                                                c.reLaunch({
                                                                    url: "/pages/index/index"
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    },
                                    fail: function (t) {
                                        c.showToast({
                                            title: "调用授权窗口失败",
                                            icon: "success",
                                            duration: 1e3
                                        });
                                    }
                                });
                            },
                            tabFun2: function (t) {
                                var e = t.target.dataset.id,
                                    n = {};
                                n.curHdIndex = e, this.tabArr2 = n;
                            },
                            hide_tcgg: function () {
                                var t = this.pageset;
                                t.tc_is = 2, this.show_page_tcgg = 0, this.pageset = t;
                            },
                            countdown: function (t) {
                                var e = this;
                                if (null != t && "undefined" != t) var n = t.currentTarget.dataset.close;
                                else n = 1;
                                var i = e.sec;
                                if (0 == i || 0 == n) {
                                    var o = e.pageset;
                                    return o.kp_is = 2, this.showFad = 0, void(this.pageset = o);
                                }
                                setTimeout(function () {
                                    e.sec = i - 1, e.countdown();
                                }, 1e3);
                            },
                            showvideo_diy: function (t) {
                                this.sv_diy = 1, this.videourl = t.currentTarget.dataset.video;
                            },
                            cvideo_idy: function () {
                                this.sv_diy = 0;
                            },
                            refreshSessionkey: function () {
                                var e = this;
                                c.login({
                                    success: function (t) {
                                        c.request({
                                            url: e.$baseurl + "doPagegetNewSessionkey",
                                            data: {
                                                uniacid: e.$uniacid,
                                                code: t.code
                                            },
                                            success: function (t) {
                                                e.newSessionKey = t.data.data;
                                            }
                                        });
                                    }
                                });
                            },
                            makephonecall: function () {
                                c.makePhoneCall({
                                    phoneNumber: this.baseinfo.tel
                                });
                            }
                        }
                    };
                n.default = e;
            }).call(this, o("543d").default);
        }
    },
    [
        ["5841", "common/runtime", "common/vendor"]
    ]
]);