(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/equity_show/equity_show"], {
        4247: function (t, e, u) {
            (function (n) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var i = u("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                shows: [],
                                bg_img: "",
                                userinfo: ""
                            };
                        },
                        onPullDownRefresh: function () {
                            this.getequity(), n.stopPullDownRefresh();
                        },
                        onLoad: function (t) {
                            var e = this;
                            n.setNavigationBarTitle({
                                title: "开通权益"
                            });
                            t.fxsid && (this.fxsid = t.fxsid), this._baseMin(this), i.getOpenid(0, function () {
                                e.getequity();
                            }, function () {
                                e.needAuth = !0;
                            }, function () {
                                e.needBind = !0;
                            });
                        },
                        methods: {
                            getequity: function () {
                                var e = this;
                                n.request({
                                    url: e.$baseurl + "doPagegetequity",
                                    data: {
                                        uniacid: e.$uniacid,
                                        suid: n.getStorageSync("suid"),
                                        source: n.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        e.shows = t.data.data.equity, e.bg_img = t.data.data.bg_img, e.userinfo = t.data.userinfo;
                                    },
                                    fail: function (t) {}
                                });
                            },
                            toregister: function () {
                                n.redirectTo({
                                    url: "/pages/register/register"
                                });
                            }
                        }
                    };
                e.default = t;
            }).call(this, u("543d").default);
        },
        "6bd3": function (t, e, n) {
            n.r(e);
            var i = n("4247"),
                u = n.n(i);
            for (var a in i) "default" !== a && function (t) {
                n.d(e, t, function () {
                    return i[t];
                });
            }(a);
            e.default = u.a;
        },
        "77b7": function (t, e, n) {},
        c11c: function (t, e, n) {
            var i = n("77b7");
            n.n(i).a;
        },
        c13e: function (t, e, n) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                n("020c"), n("921b"), e(n("66fd")), t(e(n("e538")).default);
            }).call(this, n("543d").createPage);
        },
        d4a2: function (t, e, n) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                u = [];
            n.d(e, "a", function () {
                return i;
            }), n.d(e, "b", function () {
                return u;
            });
        },
        e538: function (t, e, n) {
            n.r(e);
            var i = n("d4a2"),
                u = n("6bd3");
            for (var a in u) "default" !== a && function (t) {
                n.d(e, t, function () {
                    return u[t];
                });
            }(a);
            n("c11c");
            var o = n("2877"),
                r = Object(o.a)(u.default, i.a, i.b, !1, null, null, null);
            e.default = r.exports;
        }
    },
    [
        ["c13e", "common/runtime", "common/vendor"]
    ]
]);