(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/form_list/form_list"], {
        "0069": function (t, e, n) {
            var a = n("9f01");
            n.n(a).a;
        },
        "8a3f": function (t, e, n) {
            n.r(e);
            var a = n("bb66"),
                i = n.n(a);
            for (var o in a) "default" !== o && function (t) {
                n.d(e, t, function () {
                    return a[t];
                });
            }(o);
            e.default = i.a;
        },
        "9a29": function (t, e, n) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            n.d(e, "a", function () {
                return a;
            }), n.d(e, "b", function () {
                return i;
            });
        },
        "9f01": function (t, e, n) {},
        ba93: function (t, e, n) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                n("020c"), n("921b"), e(n("66fd")), t(e(n("e1a2")).default);
            }).call(this, n("543d").createPage);
        },
        bb66: function (t, e, n) {
            (function (a) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var i = n("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                formset: [],
                                formset_length: 0,
                                page: 1,
                                page_signs: "/pages/form_list/form_list",
                                baseinfo: []
                            };
                        },
                        onLoad: function (t) {
                            var e = this;
                            t.title ? a.setNavigationBarTitle({
                                title: t.title
                            }) : a.setNavigationBarTitle({
                                title: "我的表单提交列表"
                            });
                            var n = 0;
                            t.fxsid && (n = t.fxsid, e.fxsid = t.fxsid), this.openid = a.getStorageSync("openid"),
                                i.getOpenid(n, function () {
                                    e.getFormList();
                                }), this._baseMin(this);
                        },
                        onPullDownRefresh: function () {
                            this.page = 1, this.getFormList(), a.stopPullDownRefresh();
                        },
                        onReachBottom: function () {
                            var e = this;
                            if (0 < e.list.length) {
                                var n = e.page + 1;
                                wx.request({
                                    url: e.$baseurl + "doPageGetFormList",
                                    data: {
                                        openid: e.openid,
                                        page: n,
                                        suid: a.getStorageSync("suid")
                                    },
                                    success: function (t) {
                                        0 < t.data.data.length && (e.page = n, e.formset = e.formset.concat(t.data.data));
                                    },
                                    fail: function (t) {}
                                });
                            }
                        },
                        methods: {
                            redirectto: function (t) {
                                var e = t.currentTarget.dataset.link,
                                    n = t.currentTarget.dataset.linktype;
                                this._redirectto(e, n);
                            },
                            getFormList: function () {
                                var e = this;
                                a.request({
                                    url: e.$baseurl + "doPageGetFormList",
                                    data: {
                                        uniacid: e.$uniacid,
                                        openid: e.openid,
                                        page: e.page,
                                        suid: a.getStorageSync("suid")
                                    },
                                    success: function (t) {
                                        e.formset = t.data.data, e.formset_length = t.data.data.length, console.log(e.formset_length);
                                    },
                                    fail: function (t) {}
                                });
                            }
                        }
                    };
                e.default = t;
            }).call(this, n("543d").default);
        },
        e1a2: function (t, e, n) {
            n.r(e);
            var a = n("9a29"),
                i = n("8a3f");
            for (var o in i) "default" !== o && function (t) {
                n.d(e, t, function () {
                    return i[t];
                });
            }(o);
            n("0069");
            var r = n("2877"),
                s = Object(r.a)(i.default, a.a, a.b, !1, null, null, null);
            e.default = s.exports;
        }
    },
    [
        ["ba93", "common/runtime", "common/vendor"]
    ]
]);