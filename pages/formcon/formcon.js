(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/formcon/formcon"], {
        1571: function (t, n, e) {},
        "27d5": function (t, n, e) {
            e.r(n);
            var a = e("f24e"),
                o = e.n(a);
            for (var r in a) "default" !== r && function (t) {
                e.d(n, t, function () {
                    return a[t];
                });
            }(r);
            n.default = o.a;
        },
        "355b": function (t, n, e) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                o = [];
            e.d(n, "a", function () {
                return a;
            }), e.d(n, "b", function () {
                return o;
            });
        },
        7513: function (t, n, e) {
            (function (t) {
                function n(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                e("020c"), e("921b"), n(e("66fd")), t(n(e("9daa")).default);
            }).call(this, e("543d").createPage);
        },
        "99a5": function (t, n, e) {
            var a = e("1571");
            e.n(a).a;
        },
        "9daa": function (t, n, e) {
            e.r(n);
            var a = e("355b"),
                o = e("27d5");
            for (var r in o) "default" !== r && function (t) {
                e.d(n, t, function () {
                    return o[t];
                });
            }(r);
            e("99a5");
            var i = e("2877"),
                u = Object(i.a)(o.default, a.a, a.b, !1, null, null, null);
            n.default = u.exports;
        },
        f24e: function (t, n, e) {
            (function (a) {
                Object.defineProperty(n, "__esModule", {
                    value: !0
                }), n.default = void 0;
                var o = e("f571"),
                    t = {
                        data: function () {
                            return {
                                page: 1,
                                page_signs: "/sudu8_page/formcon/formcon",
                                formcon: [],
                                baseinfo: []
                            };
                        },
                        onPullDownRefresh: function () {
                            this.getFormCon(), a.stopPullDownRefresh();
                        },
                        onLoad: function (t) {
                            var n = this;
                            a.setNavigationBarTitle({
                                title: "我的表单提交详情页"
                            });
                            var e = 0;
                            t.fxsid && (e = t.fxsid, n.fxsid = t.fxsid), t.id && (this.id = t.id), this._baseMin(this),
                                o.getOpenid(e, function () {
                                    n.getFormCon();
                                });
                        },
                        methods: {
                            redirectto: function (t) {
                                var n = t.currentTarget.dataset.link,
                                    e = t.currentTarget.dataset.linktype;
                                this._redirectto(n, e);
                            },
                            imgYu: function (t) {
                                var n = t.currentTarget.dataset.src,
                                    e = t.currentTarget.dataset.list;
                                a.previewImage({
                                    current: n,
                                    urls: e
                                });
                            },
                            getFormCon: function () {
                                var n = this;
                                a.request({
                                    url: n.$baseurl + "doPageGetFormCon",
                                    data: {
                                        uniacid: n.$uniacid,
                                        id: n.id
                                    },
                                    success: function (t) {
                                        n.formcon = t.data.data;
                                    },
                                    fail: function (t) {}
                                });
                            }
                        }
                    };
                n.default = t;
            }).call(this, e("543d").default);
        }
    },
    [
        ["7513", "common/runtime", "common/vendor"]
    ]
]);