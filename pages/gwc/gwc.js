(global.webpackJsonp = global.webpackJsonp || []).push([
  ["pages/gwc/gwc"], {
    "308f": function(t, a, n) {
      var e = n("7ea1");
      n.n(e).a;
    },
    "594a": function(t, a, n) {
      var e = function() {
          this.$createElement;
          this._self._c;
        },
        i = [];
      n.d(a, "a", function() {
        return e;
      }), n.d(a, "b", function() {
        return i;
      });
    },
    6456: function(t, a, n) {
      n.r(a);
      var e = n("594a"),
        i = n("68d6");
      for (var s in i) "default" !== s && function(t) {
        n.d(a, t, function() {
          return i[t];
        });
      }(s);
      n("308f");
      var o = n("2877"),
        c = Object(o.a)(i.default, e.a, e.b, !1, null, null, null);
      a.default = c.exports;
    },
    "68d6": function(t, a, n) {
      n.r(a);
      var e = n("a6fe"),
        i = n.n(e);
      for (var s in e) "default" !== s && function(t) {
        n.d(a, t, function() {
          return e[t];
        });
      }(s);
      a.default = i.a;
    },
    "7ea1": function(t, a, n) {},
    9847: function(t, a, n) {
      (function(t) {
        function a(t) {
          return t && t.__esModule ? t : {
            default: t
          };
        }
        n("020c"), n("921b"), a(n("66fd")), t(a(n("6456")).default);
      }).call(this, n("543d").createPage);
    },
    a6fe: function(t, a, n) {
      (function(c) {
        Object.defineProperty(a, "__esModule", {
          value: !0
        }), a.default = void 0;
        var s = n("f571"),
          t = {
            data: function() {
              return {
                $imgurl: this.$imgurl,
                needAuth: !1,
                needBind: !1,
                page_signs: "/pages/gwc/gwc",
                baseinfo: {},
                take_self: 1,
                nav: 1,
                mygwc: [],
                normal_total: 0,
                invalid_total: 0,
                allxz: 0,
                allprice: 0,
                jiesuan: 0,
                manage_statue: 0,
                gwc_ids: "",
                gwc_nodata: !1,
                system_w: 0,
                suid: "",
                pull: 0,
                isIphoneX: ""
              };
            },
            onLoad: function(t) {
              var a = this;
              c.getSystemInfo({
                success: function(t) {
                  -1 != t.model.search("iPhone X") && (a.isIphoneX = !0);
                }
              }), this._baseMin(this);
              var n = c.getStorageSync("systemInfo");
              0 <= n.system.indexOf("iOS") && (this.is_ios = 1), this.system_w = parseInt(n.windowWidth);
              var e = 0;
              t.fxsid && (e = t.fxsid), this.fxsid = e;
              var i = c.getStorageSync("suid");
              i && (this.suid = i), s.getOpenid(e, function() {
                a.getmygwc();
              }, function() {
                a.needAuth = !0;
              }, function() {
                a.needBind = !0;
              });
            },
            onPullDownRefresh: function() {
              this.pull = 1, this.getmygwc(), c.stopPullDownRefresh();
            },
            methods: {
              cell: function() {
                this.needAuth = !1;
              },
              closeAuth: function() {
                this.needAuth = !1, this.needBind = !0;
              },
              closeBind: function() {
                this.needBind = !1;
              },
              revisenav: function(t) {
                var a = this.nav,
                  n = t.currentTarget.dataset.nav;
                a != n && (1 == n ? this.mygwc = this.express_data : 2 == n ? this.mygwc = this.take_data : 3 == n && (this.mygwc = this.all_support_data),
                  this.normal_total = this.mygwc.normal.length, this.invalid_total = this.mygwc.invalid.length,
                  0 < this.normal_total + this.invalid_total ? this.gwc_nodata = !0 : this.gwc_nodata = !1,
                  this.nav = n, this.allprice = 0, this.manage_statue = 0, this.allxz = 0, this.gwc_nodata ? this.allxuanz() : this.jiesuan = 0);
              },
              getmygwc: function() {
                var a = this;
                c.setNavigationBarTitle({
                  title: "购物车"
                });
                var t = this.suid;
                c.request({
                  url: a.$host + "/api/mainwxapp/doPagegetmygwc",
                  data: {
                    uniacid: a.$uniacid,
                    suid: t
                  },
                  success: function(t) {
                    a.take_self = t.data.data.take_self, a.express_data = t.data.data.express_data,
                      a.take_data = t.data.data.take_data, a.all_support_data = t.data.data.all_support_data,
                      1 == a.nav ? a.mygwc = t.data.data.express_data : 2 == a.nav ? a.mygwc = t.data.data.take_data : 3 == a.nav && (a.mygwc = t.data.data.all_support_data),
                      a.normal_total = a.mygwc.normal.length, a.invalid_total = a.mygwc.invalid.length,
                      0 < a.normal_total + a.invalid_total && (a.gwc_nodata = !0), 1 == a.pull && (a.allxz = 0),
                      a.allxuanz();
                  }
                });
              },
              allxuanz: function() {
                var t = this,
                  a = t.allxz,
                  n = t.mygwc.normal,
                  e = t.allprice;
                if (1 == a) {
                  for (var i = a = 0; i < n.length; i++) e = (1000 * e - n[i].num * n[i].pro_info.discount_price * 1000) / 1000,
                    n[i].ck = 0;
                  this.jiesuan = 0;
                } else {
                  for (a = 1, i = e = 0; i < n.length; i++) e = (100 * e + n[i].num * n[i].pro_info.discount_price * 100) / 100,
                    n[i].ck = 1;
                  this.jiesuan = 1;
                }
                e = (e = 100 * e * .01) < 0 ? 0 : e, t.allxz = a, t.allprice = e.toFixed(2), t.mygwc.normal = n;
              },
              addbtn: function(t) {
                var e = this,
                  i = t.currentTarget.dataset.index,
                  s = e.mygwc.normal,
                  o = s[i].num,
                  a = s[i].pro_info.kc,
                  n = s[i].id;
                if (a < ++o) return c.showModal({
                  title: "提醒",
                  content: "您的购买数量超过了库存！",
                  showCancel: !1
                }), o--, !1;
                s[i].num = o, c.request({
                  url: e.$baseurl + "doPageduogwcchange",
                  data: {
                    uniacid: e.$uniacid,
                    id: n,
                    num: o
                  },
                  success: function(t) {
                    if (0 == t.data.error) {
                      for (var a = 0, n = 0; n < s.length; n++) 1 == s[n].ck && (a = 1 * a + s[n].num * s[n].pro_info.discount_price);
                      e.mygwc.normal = s, a = .01 * parseInt(100 * a), e.allprice = a.toFixed(2);
                    } else c.showModal({
                      title: "提醒",
                      content: t.data.msg,
                      showCancel: !1
                    }), o--, s[i].num = o;
                  }
                });
              },
              delbtn: function(t) {
                var e = this,
                  a = t.currentTarget.dataset.index,
                  i = e.mygwc.normal,
                  n = i[a].num,
                  s = (i[a].pro_info.kc,
                    i[a].id);
                0 == --n && n++, i[a].num = n, c.request({
                  url: e.$baseurl + "doPageduogwcchange",
                  data: {
                    uniacid: e.$uniacid,
                    id: s,
                    num: n
                  },
                  success: function(t) {
                    for (var a = 0, n = 0; n < i.length; n++) 1 == i[n].ck && (a = 1 * a + i[n].num * i[n].pro_info.discount_price);
                    a = parseInt(100 * a) / 100, e.mygwc.normal = i, e.allprice = a.toFixed(2);
                  }
                });
              },
              manage: function() {
                var t = this.manage_statue;
                this.manage_statue = 0 == t ? 1 : 0;
                for (var a = this.mygwc.normal, n = 0; n < a.length; n++) a[n].ck = 0;
                this.jiesuan = 0, this.allxz = 0, this.allprice = 0, this.mygwc.normal = a;
              },
              delgwcs: function(t) {
                var a = this,
                  n = "",
                  e = [];
                if (1 == t.currentTarget.dataset.type) {
                  e = this.mygwc.invalid;
                  for (var i = 0; i < e.length; i++) n += e[i].id + ",";
                } else
                  for (e = this.mygwc.normal, i = 0; i < e.length; i++) 1 == e[i].ck && (n += e[i].id + ",");
                n ? c.showModal({
                  title: "提示",
                  content: "该操作不可逆，确定删除吗？",
                  success: function(t) {
                    if (!t.confirm) return !1;
                    c.request({
                      url: a.$host + "/api/mainwxapp/doPageDelGwcs",
                      data: {
                        uniacid: a.$uniacid,
                        suid: a.suid,
                        gwc_ids: n
                      },
                      success: function(t) {
                        0 == t.data.data.error ? c.showModal({
                          title: "提示",
                          content: "删除成功",
                          showCancel: !1,
                          success: function(t) {
                            a.getmygwc();
                          }
                        }) : c.showModal({
                          title: "提示",
                          content: "删除失败",
                          showCancel: !1
                        });
                      }
                    });
                  }
                }) : c.showModal({
                  title: "提示",
                  content: "请先选择删除商品",
                  showCancel: !1
                });
              },
              deldata: function(t) {
                var e = this,
                  a = t.currentTarget.dataset.id,
                  i = t.currentTarget.dataset.index,
                  s = e.mygwc.normal;
                c.showModal({
                  title: "提醒",
                  content: "您确定要删除该商品？",
                  success: function(t) {
                    t.confirm && c.request({
                      url: e.$baseurl + "doPagedelmygwc",
                      data: {
                        uniacid: e.$uniacid,
                        id: a
                      },
                      success: function(t) {
                        s.splice(i, 1);
                        for (var a = 0, n = 0; n < s.length; n++) a = 1 * a + s[n].num * s[n].pro_info.discount_price;
                        a = parseInt(100 * a) / 100, e.mygwc.normal = s, e.allprice = a.toFixed(2);
                      }
                    });
                  }
                });
              },
              topay: function(t) {
                var a = this.mygwc.normal;
                if (0 == this.allprice) return c.showModal({
                  title: "提醒",
                  content: "请先选择结算的商品！",
                  showCancel: !1
                }), !1;
                for (var n = [], e = 0; e < a.length; e++)
                  if (1 == a[e].ck) {
                    var i;
                    i = a[e].pvid + "|" + a[e].pid + "|" + a[e].num, n.push(i);
                  }
                c.setStorageSync("buydata", n), c.navigateTo({
                  url: "/pages/order_down/order_down?from_gwc=1&nav=" + this.nav
                });
              },
              xuanz: function(t) {
                var a = this,
                  n = t.currentTarget.dataset.index,
                  e = (t.currentTarget.dataset.id,
                    t.currentTarget.dataset.num, t.currentTarget.dataset.price),
                  i = a.mygwc.normal,
                  s = a.allprice;
                0 == i[n].ck ? s = (i[n].ck = 1) * s + 1 * e : ((i[n].ck = 0) < e && (s = .01 * (100 * s - 100 * e)),
                  a.allxz = 0);
                for (var o = 0, c = 0; c < i.length; c++) o = 1 * i[c].ck + o;
                o == i.length && (a.allxz = 1), i[0].uid = i[0].uid + 1, a.mygwc.normal = i, a.allprice = s <= 0 ? 0 : s.toFixed(2),
                  1 == a.manage_statue && 0 < o ? a.jiesuan = 1 : a.jiesuan = s <= 0 ? 0 : 1;
              }
            }
          };
        a.default = t;
      }).call(this, n("543d").default);
    }
  },
  [
    ["9847", "common/runtime", "common/vendor"]
  ]
]);