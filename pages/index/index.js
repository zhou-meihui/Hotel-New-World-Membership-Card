(global.webpackJsonp = global.webpackJsonp || []).push([
  ["pages/index/index"], {
    "2f4f": function(t, e, n) {},
    "454c": function(t, e, n) {
      var i = n("2f4f");
      n.n(i).a;
    },
    "588b": function(t, e, n) {
      (function(t) {
        function e(t) {
          return t && t.__esModule ? t : {
            default: t
          };
        }
        n("020c"), n("921b"), e(n("66fd")), t(e(n("ec7d")).default);
      }).call(this, n("543d").createPage);
    },
    "87a0": function(t, e, n) {
      n.r(e);
      var i = n("9e0d"),
        o = n.n(i);
      for (var a in i) "default" !== a && function(t) {
        n.d(e, t, function() {
          return i[t];
        });
      }(a);
      e.default = o.a;
    },
    "9e0d": function(t, n, o) {
      (function(r) {
        Object.defineProperty(n, "__esModule", {
          value: !0
        }), n.default = void 0, o("2f62");
        var t, l = (t = o("3584")) && t.__esModule ? t : {
          default: t
        };
        var i = o("f571"),
          e = {
            data: function() {
              return {
                $imgurl: this.$imgurl,
                $host: this.$host,
                pageid: "",
                foot_is: 1,
                list: [],
                page_signs: "/pages/index/index",
                miniad: "",
                baseinfo: "",
                showAd: 1,
                showFad: 1,
                searchtitle: "",
                date_c: "",
                time_c: "",
                store: [],
                currentId: 1,
                minHeight: 220,
                heighthave: 0,
                content: [],
                status: 0,
                menuContent: [{
                  title: "全部分类",
                  content: ["全部分类"]
                }, {
                  title: "综合排序",
                  content: ["综合排序", "距离最近"]
                }, {
                  title: "所有商家",
                  content: ["所有商家", "优选商家"]
                }],
                showMenu: !0,
                lastIndex: null,
                times: 1,
                longitude: "",
                latitude: "",
                yuyin: 1,
                tabArr2: {
                  curHdIndex: 0
                },
                formimg: [],
                zhixin: !1,
                show_page_tcgg: 1,
                forminfo: [],
                pd_val: [],
                footmenu: 0,
                footmenuh: 0,
                foottext: 0,
                page_is: 1,
                homepageid: 2,
                shanghu_goods: [],
                page: "",
                source: "",
                sv_diy: 0,
                pageset: {},
                sec: -1,
                formdescs: "",
                newSessionKey: "",
                msmkdata: "",
                formid: "",
                mstime: [],
                list2: [],
                ischecked: !1,
                bartime: [],
                markers: [],
                counts: [],
                yhqCounts: [],
                pageIdNew: '',//页面id
              };
            },
            components: {
              banner: function() {
                return o.e("components/diy_components/banner").then(o.bind(null, "f938"));
              },
              mlist: function() {
                return o.e("components/diy_components/mlist").then(o.bind(null, "36ec"));
              },
              goods: function() {
                return o.e("components/diy_components/goods").then(o.bind(null, "0e6b"));
              },
              listdesc: function() {
                return o.e("components/diy_components/listdesc").then(o.bind(null, "d327"));
              },
              menulist: function() {
                return o.e("components/diy_components/menulist").then(o.bind(null, "9482"));
              },
              title2: function() {
                return o.e("components/diy_components/title2").then(o.bind(null, "8ec3"));
              },
              classfit: function() {
                return o.e("components/diy_components/classfit").then(o.bind(null, "7e35"));
              },
              menu2: function() {
                return o.e("components/diy_components/menu2").then(o.bind(null, "4aee"));
              },
              ddlb: function() {
                return o.e("components/diy_components/ddlb").then(o.bind(null, "ce46"));
              },
              anniu: function() {
                return o.e("components/diy_components/anniu").then(o.bind(null, "7f54"));
              },
              multiple: function() {
                return o.e("components/diy_components/multiple").then(o.bind(null, "78d0"));
              },
              cases: function() {
                return o.e("components/diy_components/cases").then(o.bind(null, "2fbd"));
              },
              dnfw: function() {
                return o.e("components/diy_components/dnfw").then(o.bind(null, "2e94"));
              },
              xnlf: function() {
                return o.e("components/diy_components/xnlf").then(o.bind(null, "48f9"));
              },
              wyrq: function() {
                return o.e("components/diy_components/wyrq").then(o.bind(null, "c1fc"));
              },
              logo: function() {
                return o.e("components/diy_components/logo").then(o.bind(null, "760b"));
              },
              service: function() {
                return o.e("components/diy_components/service").then(o.bind(null, "40b3"));
              },
              listmenu: function() {
                return o.e("components/diy_components/listmenu").then(o.bind(null, "32a9"));
              },
              joblist: function() {
                return o.e("components/diy_components/joblist").then(o.bind(null, "c551"));
              },
              xfk: function() {
                return o.e("components/diy_components/xfk").then(o.bind(null, "bd44"));
              },
              contact: function() {
                return o.e("components/diy_components/contact").then(o.bind(null, "728f"));
              },
              bigimg: function() {
                return o.e("components/diy_components/bigimg").then(o.bind(null, "5222"));
              },
              page_line: function() {
                return o.e("components/diy_components/line").then(o.bind(null, "743c"));
              },
              blank: function() {
                return o.e("components/diy_components/blank").then(o.bind(null, "e4cb"));
              },
              picturew: function() {
                return o.e("components/diy_components/picturew").then(o.bind(null, "6445"));
              },
              notice: function() {
                return o.e("components/diy_components/notice").then(o.bind(null, "0237"));
              },
              ssk: function() {
                return o.e("components/diy_components/ssk").then(o.bind(null, "ac0b"));
              },
              yhq: function() {
                return o.e("components/diy_components/yhq").then(o.bind(null, "5ac4"));
              },
              diyform: function() {
                return o.e("components/diyform/diyform").then(o.bind(null, "b16e"));
              },
              supply: function() {
                return o.e("components/diy_components/supply").then(o.bind(null, "bcf3"));
              },
              msmk: function() {
                return o.e("components/diy_components/msmk").then(o.bind(null, "5397"));
              },
              pt: function() {
                return o.e("components/diy_components/pt").then(o.bind(null, "f4f8"));
              },
              reserve: function() {
                return o.e("components/diy_components/reserve").then(o.bind(null, "15d1"));
              },
              personlist: function() {
                return o.e("components/diy_components/personlist").then(o.bind(null, "dd04"));
              },
              bargain: function() {
                return o.e("components/diy_components/bargain").then(o.bind(null, "b8b7"));
              },
              yhqgoods: function() {
                return o.e("components/diy_components/yhqgoods").then(o.bind(null, "40d4"));
              }
            },
            onReady: function() {
              this.audioCtx = r.createAudioContext("myAudio");
            },
            onLoad: function(t) {
              
              console.log(t,'onLoad')
              this._baseMin(this), this.source = 1, this.refreshSessionkey();
              var e = 0;
              t.fxsid && (e = t.fxsid, r.setStorageSync("fxsid", e)), this.fxsid = e;
              var n = t.pageid;
              this.pageid = n, r.getStorageSync("pageid") && (this.pageid = r.getStorageSync("pageid")),
                null != n && "undefined" != n ? (this.getPage(n), this.page_is = 2, this.pageid = n,
                  this.page_signs = "/pages/index/index?pageid=" + n) : this.homepage();
            },
            onPullDownRefresh: function() {
              this.refreshSessionkey(), this._baseMin(this);
              var t = this.pageid;
              // if(t != null && t != "undefined"){

              // }

              null != t && "undefined" != t ? this.getPage(t) : this.homepage(), r.stopPullDownRefresh();
            },
            onShareAppMessage: function() {
              return {
                title: that.page.title,
                desc: "小程序",
                path: "pages/index/index"
              };
            },
            methods: {
              homepage: function() {
                var n = this;
                r.request({
                  url: n.$baseurl + "doPagehomepage",
                  data: {
                    uniacid: n.$uniacid,
                    source: n.source
                  },
                  header: {
                    "content-type": "application/json"
                  },
                  success: function(t) {
                    if (-1 == t.data.data) return r.showModal({
                      title: "提醒",
                      content: "抱歉, 您暂无使用该小程序权限!",
                      showCancel: !1,
                      success: function(t) {}
                    }), !1;
                    var e = t.data.data.pageid;
                    0 == e ? r.showModal({
                      title: "提醒",
                      content: "diy布局没有设置首页，无法进入",
                      showCancel: !1,
                      success: function(t) {
                        return !1;
                      }
                    }) : (n.getPage(e), i.getOpenid(n.fxsid, function() {})), n.pageid = e, n.homepageid = e;
                  },
                  fail: function(t) {
                    console.log(t);
                  }
                });
              },
              aaa: function() {
                this.ischecked = !1, console.log(this.ischecked);
              },
              getPage: function(t) {
                var d = this;
                r.request({
                  url: d.$baseurl + "dopageGetitems",
                  data: {
                    uniacid: d.$uniacid,
                    pageid: t,
                    source: r.getStorageSync("source")
                  },
                  success: function(t) {
                    var e = t.data.data;
                      //  console.log(e.pageset.kp_is);
                    //判断开屏广告状态码，1:show, 2:hide
                    console.log(e,'diy页面配置');
                    d.pageIdNew = e.id
                    var cach = wx.getStorageSync('hide')
                    if (cach === "") {
                      if (e.pageset.kp_is === 1) {
                        //开屏广告状态码为0时，设置缓存值 为2
                        // console.log(e.pageset.kp_is)
                        wx.setStorage({
                          key: "hide",
                          data: 2
                        })
                      }
                    } else if (cach != "") { //不为空时，取缓存重新赋值
                      e.pageset.kp_is = wx.getStorageSync('hide');
                    }

                    if (d.list = e.items, 3 == e) return d.homepage(), !1;
                    for (var n = e.items, i = !1, o = 0; o < n.length; o++) "multiple" != n[o].id && "mlist" != n[o].id || (i = !0);
                    for (i && (r.getStorageSync("latitude") && r.getStorageSync("longitude") || r.getLocation({
                        type: "wgs84",
                        success: function(t) {
                          d.latitude = t.latitude, d.longitude = t.longitude, r.setStorageSync("latitude", t.latitude),
                            r.setStorageSync("longitude", t.longitude);
                        },
                        fail: function(t) {
                          d.getlocation();
                        }
                      })), o = 0; o < n.length; o++) "msmk" == n[o].id && d.diyGetMsmk(o, n[o].params.sourceid, n[o].params.goodsnum, n[o].params.con_type, n[o].params.con_key),
                      "pt" == n[o].id && d.diyGetPt(o, n[o].params.sourceid, n[o].params.goodsnum, n[o].params.con_type, n[o].params.con_key),
                      "cases" == n[o].id && d.diyGetCases(o, n[o].params.sourceid, n[o].params.casenum, n[o].params.con_type, n[o].params.con_key, n[o].params.showtype),
                      "listdesc" == n[o].id && d.diyGetListdesc(o, n[o].params.sourceid, n[o].params.newsnum, n[o].params.con_type, n[o].params.con_key, n[o].params.showtype),
                      "notice" == n[o].id && 0 == n[o].params.noticedata && d.diyGetNotice(o, n[o].params.sourceid, n[o].params.noticenum, n[o].params.noticedata),
                      "goods" == n[o].id && d.diyGetGoods(o, n[o].params.sourceid, n[o].params.goodsnum, n[o].params.con_type, n[o].params.con_key),
                      "feedback" == n[o].id && d.diyGetFeedback(n[o].params.sourceid), "ddlb" == n[o].id && d.diyGetDdlb(o),
                      "reserve" == n[o].id && d.diyGetReserve(o, n[o].params.sourceid, n[o].params.goodsnum, n[o].params.con_type, n[o].params.con_key),
                      "yhq" == n[o].id && d.diyGetYhq(o, n[o].style.counts), "xnlf" == n[o].id && d.diyGetXnlf(o, n[o].params.fwl, n[o].params.backgroundimg),
                      "multiple" != n[o].id && "mlist" != n[o].id || (i = !0, "multiple" == n[o].id && d.diyGetMutiple(o, n[o].style.showtype, n[o].params.counts, n[o].params.content_type),
                        "mlist" == n[o].id && d.diyGetMlist(o, n[o].style.viewcount, n[o].params.content_type)),
                      "bargain" == n[o].id && d.diyGetBargain(o, n[o].params.sourceid, n[o].params.goodsnum, n[o].params.con_type, n[o].params.con_key),
                      "personlist" == n[o].id && d.diyGetPersionlist(o, n[o].params.goodsnum), "supply" == n[o].id && d.diyGetSupply(o, n[o].params.newsnum, n[o].params.data_types, n[o].params.supply),
                      "video" == n[o].id && d.diyGetVideo(o, n[o].params.videourl), "dt" == n[o].id && (d.markers = [{
                        iconPath: d.$imgurl + "dtdw1.png",
                        id: 0,
                        latitude: n[o].params.zzb,
                        longitude: n[o].params.hzb,
                        width: 30,
                        height: 30,
                        title: "111",
                        callout: {
                          content: n[o].params.title + n[o].params.wzmc,
                          color: "#fff",
                          fontSize: 12,
                          borderRadius: 5,
                          bgColor: "#31DC8F",
                          padding: 10,
                          display: "ALWAYS",
                          textAlign: "center"
                        }
                      }]), "richtext" == n[o].id && (n[o].richtext = (0, l.default)(n[o].richtext));
                    var a = t.data.data.pageset,
                      s = t.data.data.page;
                    if (null != a && 1 == a.go_home) {
                      var u = a.kp_m;
                      0 < u && (d.sec = u, d.countdown());
                    }
                    d.pageset = a, d.page = s, d.audioPlay1(), n = t.data.data.items, r.setNavigationBarTitle({
                      title: s.title
                    });
                    var c = 1 == s.topcolor ? "#000000" : "#ffffff";
                    r.setNavigationBarColor({
                      frontColor: c,
                      backgroundColor: s.topbackground,
                      animation: {
                        duration: 400,
                        timingFunc: "easeIn"
                      }
                    }), r.hideLoading();
                  },
                  fail: function(t) {
                    r.hideLoading(), r.showModal({
                      title: "提示",
                      content: "加载失败"
                    });
                  }
                });
              },
              diyGetMsmk: function(t, e, n, i, o) {
                var a = this;
                r.request({
                  url: a.$host + "/api/Plugin/msmk",
                  data: {
                    uniacid: a.$uniacid,
                    sourceid: e || 0,
                    count: n,
                    con_type: i,
                    con_key: o
                  },
                  success: function(t) {
                    a.msmkdata = t.data.data, a.daojishi();
                  }
                });
              },
              diyGetPt: function(e, t, n, i, o) {
                var a = this;
                r.request({
                  url: a.$host + "/api/Plugin/pt",
                  data: {
                    uniacid: a.$uniacid,
                    sourceid: t || 0,
                    count: n,
                    con_type: i,
                    con_key: o,
                    source: r.getStorageSync("source")
                  },
                  success: function(t) {
                    a.list[e].data = t.data.data;
                  }
                });
              },
              diyGetCases: function(e, t, n, i, o, a) {
                var s = this;
                r.request({
                  url: s.$host + "/api/Plugin/cases",
                  data: {
                    uniacid: s.$uniacid,
                    sourceid: t || 0,
                    count: n,
                    con_type: i,
                    con_key: o,
                    showtype: a
                  },
                  success: function(t) {
                    s.list[e].data = t.data.data;
                  }
                });
              },
              diyGetListdesc: function(e, t, n, i, o, a) {
                var s = this;
                r.request({
                  url: s.$host + "/api/Plugin/listdesc",
                  data: {
                    uniacid: s.$uniacid,
                    sourceid: t || 0,
                    count: n,
                    con_type: i,
                    con_key: o,
                    showtype: a
                  },
                  success: function(t) {
                    s.list[e].data = t.data.data;
                  }
                });
              },
              diyGetNotice: function(e, t, n, i) {
                var o = this;
                r.request({
                  url: o.$host + "/api/Plugin/notice",
                  data: {
                    uniacid: o.$uniacid,
                    sourceid: t || 0,
                    count: n,
                    noticedata: i
                  },
                  success: function(t) {
                    o.list[e].data = t.data.data;
                  }
                });
              },
              diyGetFeedback: function(t) {
                r.setStorageSync("form_source", t);
                var e = this;
                r.request({
                  url: e.$host + "/api/Plugin/feedback",
                  data: {
                    uniacid: e.$uniacid,
                    sourceid: t || 0
                  },
                  success: function(t) {
                    e.forminfo = t.data.data.forminfo.tp_text, e.formid = t.data.data.forminfo.id, e.formname = t.data.data.forminfo.formname,
                      e.formdescs = t.data.data.forminfo.descs;
                  }
                });
              },
              diyGetDdlb: function(e) {
                var n = this;
                r.request({
                  url: n.$host + "/api/Plugin/ddlb",
                  data: {
                    uniacid: n.$uniacid,
                    source: r.getStorageSync("source")
                  },
                  success: function(t) {
                    n.list[e].count = t.data.data.count, n.counts = t.data.data.count;
                  }
                });
              },
              diyGetGoods: function(e, t, n, i, o) {
                var a = this;
                r.request({
                  url: a.$host + "/api/Plugin/goods",
                  data: {
                    uniacid: a.$uniacid,
                    sourceid: t || 0,
                    count: n,
                    con_type: i,
                    con_key: o
                  },
                  success: function(t) {
                    a.list[e].data = t.data.data;
                  }
                });
              },
              diyGetReserve: function(e, t, n, i, o) {
                var a = this;
                r.request({
                  url: a.$host + "/api/Plugin/reserve",
                  data: {
                    uniacid: a.$uniacid,
                    sourceid: t || 0,
                    goodsnum: n,
                    con_type: i,
                    con_key: o
                  },
                  success: function(t) {
                    a.list[e].data = t.data.data;
                  }
                });
              },
              diyGetYhq: function(e, t) {
                var n = this;
                r.request({
                  url: n.$host + "/api/Plugin/yhq",
                  data: {
                    uniacid: n.$uniacid,
                    counts: t
                  },
                  success: function(t) {
                    n.list[e].coupon = t.data.data, n.yhqCounts = t.data.data;
                  }
                });
              },
              diyGetXnlf: function(e, t, n) {
                var i = this;
                r.request({
                  url: i.$host + "/api/Plugin/xnlf",
                  data: {
                    uniacid: i.$uniacid,
                    fwl: t,
                    backgroundimg: n,
                    souce: r.getStorageSync("source")
                  },
                  success: function(t) {
                    i.list[e].avatars = t.data.data.avatars, i.list[e].params.fwl = t.data.data.fwl;
                  }
                });
              },
              diyGetMutiple: function(e, t, n, i) {
                var o = this;
                r.request({
                  url: o.$host + "/api/Plugin/multiple",
                  data: {
                    uniacid: o.$uniacid,
                    showtype: t,
                    counts: n,
                    content_type: i
                  },
                  success: function(t) {
                    o.list[e].data = t.data.data;
                  }
                });
              },
              diyGetMlist: function(t, e, n) {
                var o = this;
                r.request({
                  url: o.$host + "/api/Plugin/mlist",
                  data: {
                    uniacid: o.$uniacid,
                    viewcount: e,
                    content_type: n
                  },
                  success: function(t) {
                    var e = t.data.data.catelist;
                    if (e)
                      for (var n = o.menuContent, i = 0; i < e.length; i++) n[0].content.push(e[i].name);
                    o.menuContent = n, o.allShopList();
                  }
                });
              },
              diyGetBargain: function(t, e, n, i, o) {
                var a = this;
                r.request({
                  url: a.$host + "/api/Plugin/bargain",
                  data: {
                    uniacid: a.$uniacid,
                    sourceid: e || 0,
                    goodsnum: n,
                    con_type: i,
                    con_key: o
                  },
                  success: function(t) {
                    a.list2 = t.data.data.data2, a.daojishi_bar();
                  }
                });
              },
              diyGetPersionlist: function(e, t) {
                var n = this;
                r.request({
                  url: n.$host + "/api/Plugin/personlist",
                  data: {
                    uniacid: n.$uniacid,
                    goodsnum: t
                  },
                  success: function(t) {
                    n.list[e].data = t.data.data;
                  }
                });
              },
              diyGetSupply: function(e, t, n, i) {
                var o = this;
                r.request({
                  url: o.$host + "/api/Plugin/supply",
                  data: {
                    uniacid: o.$uniacid,
                    newsnum: t,
                    data_types: n,
                    supply: i
                  },
                  success: function(t) {
                    o.list[e].data = t.data.data;
                  }
                });
              },
              diyGetVideo: function(e, t) {
                var n = this;
                r.request({
                  url: n.$host + "/api/Plugin/video",
                  data: {
                    uniacid: n.$uniacid,
                    videourl: t
                  },
                  success: function(t) {
                    n.list[e].params.videourl = t.data.data.videourl;
                  }
                });
              },
              allShopList: function() {
                var t = this;
                r.request({
                  url: t.$baseurl + "doPageselectShopList",
                  data: {
                    uniacid: t.$uniacid,
                    option1: t.menuContent[0].title,
                    option2: t.menuContent[1].title,
                    option3: t.menuContent[2].title,
                    longitude: t.longitude,
                    latitude: t.latitude
                  },
                  success: function(t) {}
                });
              },
              redirectto: function(t) {
                var e = t.currentTarget.dataset.link,
                  n = t.currentTarget.dataset.linktype;
                this._redirectto(e, n);
              },
              daojishi: function() {
                for (var t = this, e = [], n = t.msmkdata, i = 0; i < n.length; i++) {
                  var o = new Date().getTime();
                  if (0 == n[i].sale_time && 0 == n[i].sale_end_time);
                  else if (1e3 * n[i].sale_time > o) n[i].t_flag = 1;
                  else if (1e3 * n[i].sale_end_time < o) n[i].t_flag = 2;
                  else if (n[i].sale_end_time <= 0) n[i].endtime = 0;
                  else {
                    var a, s, u, c, d = 1e3 * parseInt(n[i].sale_end_time) - o;
                    if (0 <= d && (a = Math.floor(d / 1e3 / 60 / 60 / 24), s = Math.floor(d / 1e3 / 60 / 60 % 24) < 10 ? "0" + Math.floor(d / 1e3 / 60 / 60 % 24) : Math.floor(d / 1e3 / 60 / 60 % 24),
                        u = Math.floor(d / 1e3 / 60 % 60) < 10 ? "0" + Math.floor(d / 1e3 / 60 % 60) : Math.floor(d / 1e3 / 60 % 60),
                        c = Math.floor(d / 1e3 % 60) < 10 ? "0" + Math.floor(d / 1e3 % 60) : Math.floor(d / 1e3 % 60)),
                      0 < a) {
                      n[i].endtime = a + "天" + s + ":" + u + ":" + c;
                      var r = a + "天" + s + ":" + u + ":" + c;
                    } else n[i].endtime = s + ":" + u + ":" + c, r = s + ":" + u + ":" + c;
                    e[i] = r, t.mstime = e;
                  }
                }
                t.msmkdata = n, setTimeout(function() {
                  t.daojishi();
                }, 1e3);
              },
              daojishi_bar: function() {
                for (var t = this, e = [], n = t.list2, i = 0; i < n.length; i++) {
                  var o = new Date().getTime();
                  if (0 == n[i].sale_time && 0 == n[i].sale_end_time);
                  else if (1e3 * n[i].sale_time > o) n[i].t_flag = 1;
                  else if (1e3 * n[i].sale_end_time < o) n[i].t_flag = 2;
                  else if (n[i].sale_end_time <= 0) n[i].endtime = 0;
                  else {
                    var a, s, u, c, d = 1e3 * parseInt(n[i].sale_end_time) - o;
                    if (0 <= d && (a = Math.floor(d / 1e3 / 60 / 60 / 24), s = Math.floor(d / 1e3 / 60 / 60 % 24) < 10 ? "0" + Math.floor(d / 1e3 / 60 / 60 % 24) : Math.floor(d / 1e3 / 60 / 60 % 24),
                        u = Math.floor(d / 1e3 / 60 % 60) < 10 ? "0" + Math.floor(d / 1e3 / 60 % 60) : Math.floor(d / 1e3 / 60 % 60),
                        c = Math.floor(d / 1e3 % 60) < 10 ? "0" + Math.floor(d / 1e3 % 60) : Math.floor(d / 1e3 % 60)),
                      0 < a) {
                      n[i].endtime = a + "天" + s + ":" + u + ":" + c;
                      var r = a + "天" + s + ":" + u + ":" + c;
                    } else n[i].endtime = s + ":" + u + ":" + c, r = s + ":" + u + ":" + c;
                    e[i] = r, t.bartime = e;
                  }
                }
                t.list2 = n, setTimeout(function() {
                  t.daojishi_bar();
                }, 1e3);
              },
              audioPlay1: function() {},
              audioPlay: function(t) {
                1 == this.yuyin ? (this.audioCtx.play(), this.yuyin = 2) : (this.audioCtx.pause(),
                  this.yuyin = 1);
              },
              getlocation: function() {
                r.getSetting({
                  success: function(t) {
                    t.authSetting["scope.userLocation"] || r.showModal({
                      title: "请授权获取当前位置",
                      content: "附近店铺需要授权此功能",
                      showCancel: !1,
                      success: function(t) {
                        t.confirm && r.openSetting({
                          success: function(t) {
                            !0 === t.authSetting["scope.userLocation"] ? r.showToast({
                              title: "授权成功",
                              icon: "success",
                              duration: 1e3
                            }) : r.showToast({
                              title: "授权失败",
                              icon: "success",
                              duration: 1e3,
                              success: function() {
                                r.reLaunch({
                                  url: "/pages/index/index"
                                });
                              }
                            });
                          }
                        });
                      }
                    });
                  },
                  fail: function(t) {
                    r.showToast({
                      title: "调用授权窗口失败",
                      icon: "success",
                      duration: 1e3
                    });
                  }
                });
              },
              tabFun2: function(t) {
                var e = t.target.dataset.id,
                  n = {};
                n.curHdIndex = e, this.tabArr2 = n;
              },
              hide_tcgg: function() {
                var t = this.pageset;
                t.tc_is = 2, this.show_page_tcgg = 0, this.pageset = t;
              },
              countdown: function(t) {
                var e = this;
                if (null != t && "undefined" != t) var n = t.currentTarget.dataset.close;
                else n = 1;
                var i = e.sec;
                if (0 == i || 0 == n) {
                  var o = e.pageset;
                  return o.kp_is = 2, this.showFad = 0, void(this.pageset = o);
                }
                setTimeout(function() {
                  e.sec = i - 1, e.countdown();
                }, 1e3);
              },
              showvideo_diy: function(t) {
                this.sv_diy = 1, this.videourl = t.currentTarget.dataset.video;
              },
              cvideo_idy: function() {
                this.sv_diy = 0;
              },
              refreshSessionkey: function() {
                var e = this;
                r.login({
                  success: function(t) {
                    r.request({
                      url: e.$baseurl + "doPagegetNewSessionkey",
                      data: {
                        uniacid: e.$uniacid,
                        code: t.code
                      },
                      success: function(t) {
                        e.newSessionKey = t.data.data;
                      }
                    });
                  }
                });
              },
              makephonecall: function() {
                r.makePhoneCall({
                  phoneNumber: this.baseinfo.tel
                });
              }
            }
          };
        n.default = e;
      }).call(this, o("543d").default);
    },
    ec7d: function(t, e, n) {
      n.r(e);
      var i = n("ffe9"),
        o = n("87a0");
      for (var a in o) "default" !== a && function(t) {
        n.d(e, t, function() {
          return o[t];
        });
      }(a);
      n("454c");
      var s = n("2877"),
        u = Object(s.a)(o.default, i.a, i.b, !1, null, null, null);
      e.default = u.exports;
    },
    ffe9: function(t, e, n) {
      var i = function() {
          this.$createElement;
          this._self._c;
        },
        o = [];
      n.d(e, "a", function() {
        return i;
      }), n.d(e, "b", function() {
        return o;
      });
    }
  },
  [
    ["588b", "common/runtime", "common/vendor"]
  ]
]);