(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/listPic/listPic"], {
        3435: function (t, i, e) {
            var o = e("acd5");
            e.n(o).a;
        },
        "4ad9": function (t, i, e) {
            (function (g) {
                Object.defineProperty(i, "__esModule", {
                    value: !0
                }), i.default = void 0;
                var o = e("f571"),
                    t = {
                        components: {
                            list_pic: function () {
                                return e.e("components/plugins/list_pic").then(e.bind(null, "2270"));
                            },
                            list_pic_td0: function () {
                                return e.e("components/plugins/list_pic_td0").then(e.bind(null, "6b0e"));
                            },
                            list_pic_td1: function () {
                                return e.e("components/plugins/list_pic_td1").then(e.bind(null, "6585"));
                            },
                            list_pic_td2: function () {
                                return e.e("components/plugins/list_pic_td2").then(e.bind(null, "a125"));
                            },
                            list_text: function () {
                                return e.e("components/plugins/list_text").then(e.bind(null, "8ea7"));
                            }
                        },
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                page_signs: "",
                                baseinfo: {},
                                cateinfo: {},
                                cid: 0,
                                page: 1,
                                morePro: !1,
                                ProductsList: [],
                                orderOrBusiness: "order",
                                block: !1,
                                logs: [],
                                goodsH: 0,
                                scrollToGoodsView: 0,
                                toView: "",
                                toViewType: "",
                                GOODVIEWID: "catGood_",
                                animation: !0,
                                goodsNumArr: [0],
                                shoppingCart: {},
                                shoppingCartGoodsId: [],
                                goodMap: {},
                                chooseGoodArr: [],
                                totalNum: 0,
                                totalPay: 0,
                                showShopCart: !1,
                                fromClickScroll: !1,
                                timeStart: "",
                                timeEnd: "",
                                hideCount: !0,
                                count: 0,
                                needAni: !1,
                                hide_good_box: !0,
                                url: "",
                                protype: 1,
                                minHeight: 180,
                                heighthave: 0,
                                from_top: 0,
                                cate_list: "",
                                chessRoomDetail: {},
                                catHighLightIndex: 0,
                                cateslide: [],
                                type: ""
                            };
                        },
                        onLoad: function (t) {
                            var i = this;
                            this.page_signs = "/pages/listPic/listPic?type=" + t.type + "&cid=" + t.cid, this.cid = t.cid,
                                this._baseMin(this);
                            var e = 0;
                            t.fxsid && (e = t.fxsid), this.fxsid = e, t.type && (this.type = t.type), g.getStorageSync("suid"),
                                o.getOpenid(e, function () {
                                    i.getList();
                                });
                        },
                        onReachBottom: function () {
                            var i = this,
                                e = i.page + 1,
                                t = i.cid;
                            wx.request({
                                url: i.$baseurl + "doPagelistPic",
                                data: {
                                    cid: t,
                                    page: e,
                                    uniacid: i.$uniacid
                                },
                                success: function (t) {
                                    "" != t.data.data.list ? (i.cate_list = i.cate_list.concat(t.data.data.list), i.page = e) : i.morePro = !1;
                                }
                            });
                        },
                        onPullDownRefresh: function () {
                            this.page = 1, this.getList(), g.stopPullDownRefresh();
                        },
                        onShareAppMessage: function () {
                            return {
                                title: this.cateinfo.name + "-" + this.baseinfo.name
                            };
                        },
                        methods: {
                            handleTap: function (t) {
                                console.log(t);
                                var i = t.currentTarget.id.slice(1);
                                i && (this.cid = i, this.page = 1, this.from_top = 1, this.getList(i));
                            },
                            getList: function (t) {
                                var h = this;
                                null == t && (t = h.cid), g.request({
                                    url: h.$baseurl + "dopagelistPic",
                                    data: {
                                        cid: t,
                                        uniacid: h.$uniacid,
                                        type: h.type
                                    },
                                    success: function (t) {
                                        10 < t.data.data.num.length ? h.morePro = !0 : h.morePro = !1, h.cateinfo = t.data.data,
                                            h.cate_list = t.data.data.list, h.cateslide = t.data.data.cateslide, 0 == h.cateinfo.is_top ? g.setNavigationBarTitle({
                                                title: h.cateinfo.name
                                            }) : g.setNavigationBarTitle({
                                                title: h.cateinfo.this.name
                                            }), g.setStorageSync("isShowLoading", !1), g.hideNavigationBarLoading(), g.stopPullDownRefresh();
                                        var i = h.cateinfo.topcid;
                                        if (2 == h.cateinfo.list_style_more && 0 == i) {
                                            var e = g.getStorageSync("systemInfo");
                                            h.goodsH = e.windowHeight - 55, h.minHeight = 200, h.goodsRh = e.windowHeight - 55 + 200;
                                            var o = {};
                                            o.catList = t.data.data.newlist;
                                            for (var a = [], n = 0; n < t.data.data.length; n++)
                                                for (var s = 0; s < t.data.data[n].goodsList.length; s++) a.push(t.data.data[n].goodsList[s]);
                                            h.chessRoomDetail = o, h.allpro = a, h.toView = h.GOODVIEWID + h.chessRoomDetail.catList[0].id;
                                            for (var r = h.catHighLightIndex = 0; r < h.chessRoomDetail.catList.length; r++) {
                                                h.goodsNumArr.push(h.chessRoomDetail.catList[r].goodsList.length);
                                                var c = h.chessRoomDetail.catList[r].goodsList;
                                                if (0 < c.length)
                                                    for (var d = 0; d < c.length; d++) h.goodMap[c[d].id] = c[d];
                                            }
                                            for (var l = [], u = 0; u < h.goodsNumArr.length; u++) 0 == u ? l.push(0) : l.push(98 * h.goodsNumArr[u] + l[u - 1]);
                                            h.goodsNumArr = l;
                                        }
                                    },
                                    fail: function (t) {
                                        console.log(t);
                                    }
                                });
                            },
                            swiperLoad: function (o) {
                                var a = this;
                                g.getSystemInfo({
                                    success: function (t) {
                                        var i = o.detail.width / o.detail.height,
                                            e = t.windowWidth / i;
                                        a.heighthave || (a.minHeight = e, a.heighthave = 1);
                                    }
                                });
                            },
                            goodsViewScrollFn: function (t) {
                                this.getIndexFromHArr(t.detail.scrollTop);
                            },
                            getIndexFromHArr: function (t) {
                                for (var i = 0; i < this.goodsNumArr.length; i++) {
                                    var e = t - 40 * i;
                                    e >= this.goodsNumArr[i] && e < this.goodsNumArr[i + 1] && (this.fromClickScroll || (this.catHighLightIndex = i));
                                }
                                this.fromClickScroll = !1;
                            },
                            catClickFn: function (t) {
                                var i = t.target.id.split("_")[1],
                                    e = t.target.id.split("_")[2];
                                this.fromClickScroll = !0, this.catHighLightIndex = i, this.toView = this.GOODVIEWID + e;
                            },
                            tabChange: function (t) {
                                var i = t.currentTarget.dataset.id;
                                this.orderOrBusiness = i;
                            },
                            tiaozhuang: function (t) {
                                var i = t.currentTarget.dataset.id,
                                    e = t.currentTarget.dataset.types,
                                    o = t.currentTarget.dataset.ismore,
                                    a = "";
                                "showPro" == e ? (0 == o && (a = "/pages/showPro/showPro?id=" + i), 1 == o && (a = "/pages/showPro_lv/showPro_lv?id=" + i)) : a = "/pages/" + e + "/" + e + "?id=" + i,
                                    g.navigateTo({
                                        url: a
                                    });
                            }
                        }
                    };
                i.default = t;
            }).call(this, e("543d").default);
        },
        7713: function (t, i, e) {
            e.r(i);
            var o = e("4ad9"),
                a = e.n(o);
            for (var n in o) "default" !== n && function (t) {
                e.d(i, t, function () {
                    return o[t];
                });
            }(n);
            i.default = a.a;
        },
        "79c6": function (t, i, e) {
            var o = function () {
                    this.$createElement;
                    this._self._c;
                },
                a = [];
            e.d(i, "a", function () {
                return o;
            }), e.d(i, "b", function () {
                return a;
            });
        },
        acd5: function (t, i, e) {},
        d7c5: function (t, i, e) {
            (function (t) {
                function i(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                e("020c"), e("921b"), i(e("66fd")), t(i(e("eb99")).default);
            }).call(this, e("543d").createPage);
        },
        eb99: function (t, i, e) {
            e.r(i);
            var o = e("79c6"),
                a = e("7713");
            for (var n in a) "default" !== n && function (t) {
                e.d(i, t, function () {
                    return a[t];
                });
            }(n);
            e("3435");
            var s = e("2877"),
                r = Object(s.a)(a.default, o.a, o.b, !1, null, null, null);
            i.default = r.exports;
        }
    },
    [
        ["d7c5", "common/runtime", "common/vendor"]
    ]
]);