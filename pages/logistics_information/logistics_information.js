(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/logistics_information/logistics_information"], {
        "0051": function (t, i, n) {
            n.r(i);
            var a = n("d3a4"),
                e = n.n(a);
            for (var u in a) "default" !== u && function (t) {
                n.d(i, t, function () {
                    return a[t];
                });
            }(u);
            i.default = e.a;
        },
        "368e": function (t, i, n) {},
        "37b3": function (t, i, n) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                e = [];
            n.d(i, "a", function () {
                return a;
            }), n.d(i, "b", function () {
                return e;
            });
        },
        "38d7": function (t, i, n) {
            (function (t) {
                function i(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                n("020c"), n("921b"), i(n("66fd")), t(i(n("ac9d")).default);
            }).call(this, n("543d").createPage);
        },
        "929d": function (t, i, n) {
            var a = n("368e");
            n.n(a).a;
        },
        ac9d: function (t, i, n) {
            n.r(i);
            var a = n("37b3"),
                e = n("0051");
            for (var u in e) "default" !== u && function (t) {
                n.d(i, t, function () {
                    return e[t];
                });
            }(u);
            n("929d");
            var o = n("2877"),
                r = Object(o.a)(e.default, a.a, a.b, !1, null, null, null);
            i.default = r.exports;
        },
        d3a4: function (t, i, n) {
            (function (e) {
                Object.defineProperty(i, "__esModule", {
                    value: !0
                }), i.default = void 0;
                var a = n("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                $host: this.$host,
                                info_list: []
                            };
                        },
                        onLoad: function (t) {
                            var i = this;
                            this._baseMin(this);
                            var n = e.getStorageSync("suid");
                            n && (this.suid = n), t.order_id && (this.order_id = t.order_id), a.getOpenid(0, function () {
                                i.getWuliulist();
                            });
                        },
                        onPullDownRefresh: function () {
                            this.getWuliulist(), e.stopPullDownRefresh();
                        },
                        methods: {
                            getWuliulist: function () {
                                var i = this;
                                e.request({
                                    url: this.$host + "/api/MainWxapp/getLogisticsInforList",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: this.suid,
                                        order_id: this.order_id
                                    },
                                    success: function (t) {
                                        i.info_list = t.data.data.info_list;
                                    }
                                });
                            },
                            toState: function (t) {
                                var i = t.currentTarget.dataset.kuaidi;
                                if ("商家配送" != i) {
                                    var n = t.currentTarget.dataset.order_item_id,
                                        a = t.currentTarget.dataset.kuaidihao;
                                    e.navigateTo({
                                        url: "/pages/logistics_state/logistics_state?order_id=" + n + "&kuaidi=" + i + "&kuaidihao=" + a
                                    });
                                }
                            }
                        }
                    };
                i.default = t;
            }).call(this, n("543d").default);
        }
    },
    [
        ["38d7", "common/runtime", "common/vendor"]
    ]
]);