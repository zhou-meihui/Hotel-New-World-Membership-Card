(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/logistics_state/logistics_state"], {
        "0e81": function (t, i, a) {
            var e = a("667d");
            a.n(e).a;
        },
        "1b2e": function (t, i, u) {
            (function (a) {
                Object.defineProperty(i, "__esModule", {
                    value: !0
                }), i.default = void 0;
                var e = u("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                page_signs: "/pages/orderlist_dan/orderlist_dan",
                                kuaidi: "",
                                kuaidihao: "",
                                flag: "",
                                list: "",
                                type: "",
                                status: "",
                                pro: ""
                            };
                        },
                        onLoad: function (t) {
                            var i = this;
                            this._baseMin(this), a.setNavigationBarTitle({
                                    title: "物流信息"
                                }), t.kuaidi && (this.kuaidi = t.kuaidi), t.kuaidihao && (this.kuaidihao = t.kuaidihao),
                                t.order_id && (this.order_item_id = t.order_id);
                            e.getOpenid(0, function () {
                                i.getWuliu();
                            });
                        },
                        onPullDownRefresh: function () {
                            this.getWuliu(), a.stopPullDownRefresh();
                        },
                        methods: {
                            getWuliu: function () {
                                var i = this;
                                i.kuaidi, a.showLoading({
                                    title: "查询中.."
                                }), a.request({
                                    url: i.$baseurl + "dopageGetWuliu",
                                    data: {
                                        kuaidi: i.kuaidi,
                                        kuaidihao: i.kuaidihao,
                                        uniacid: i.$uniacid,
                                        order_item_id: i.order_item_id
                                    },
                                    success: function (t) {
                                        i.flag = t.data.flag, i.pro = t.data.pro, i.list = t.data.list, i.type = t.data.type,
                                            i.status = t.data.status, console.log(i.pro.pro_thumb), a.hideLoading();
                                    }
                                });
                            },
                            copy: function (t) {
                                var i = this.kuaidihao;
                                a.setClipboardData({
                                    data: i,
                                    success: function (t) {
                                        a.getClipboardData({
                                            success: function (t) {}
                                        }), a.showToast({
                                            title: "复制成功",
                                            duration: 2e3
                                        });
                                    }
                                });
                            }
                        }
                    };
                i.default = t;
            }).call(this, u("543d").default);
        },
        3461: function (t, i, a) {
            var e = function () {
                    this.$createElement;
                    this._self._c;
                },
                u = [];
            a.d(i, "a", function () {
                return e;
            }), a.d(i, "b", function () {
                return u;
            });
        },
        "549c": function (t, i, a) {
            a.r(i);
            var e = a("3461"),
                u = a("6f7c");
            for (var n in u) "default" !== n && function (t) {
                a.d(i, t, function () {
                    return u[t];
                });
            }(n);
            a("0e81");
            var o = a("2877"),
                d = Object(o.a)(u.default, e.a, e.b, !1, null, null, null);
            i.default = d.exports;
        },
        "667d": function (t, i, a) {},
        "6f7c": function (t, i, a) {
            a.r(i);
            var e = a("1b2e"),
                u = a.n(e);
            for (var n in e) "default" !== n && function (t) {
                a.d(i, t, function () {
                    return e[t];
                });
            }(n);
            i.default = u.a;
        },
        "8cba": function (t, i, a) {
            (function (t) {
                function i(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                a("020c"), a("921b"), i(a("66fd")), t(i(a("549c")).default);
            }).call(this, a("543d").createPage);
        }
    },
    [
        ["8cba", "common/runtime", "common/vendor"]
    ]
]);