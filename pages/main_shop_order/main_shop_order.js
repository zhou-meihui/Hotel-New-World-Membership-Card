(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/main_shop_order/main_shop_order"], {
        "040b": function (t, e, a) {
            var s = a("37a5");
            a.n(s).a;
        },
        1638: function (t, e, a) {
            var s = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            a.d(e, "a", function () {
                return s;
            }), a.d(e, "b", function () {
                return i;
            });
        },
        "17dd": function (t, e, a) {
            a.r(e);
            var s = a("1638"),
                i = a("7b87");
            for (var o in i) "default" !== o && function (t) {
                a.d(e, t, function () {
                    return i[t];
                });
            }(o);
            a("040b");
            var r = a("2877"),
                n = Object(r.a)(i.default, s.a, s.b, !1, null, null, null);
            e.default = n.exports;
        },
        "37a5": function (t, e, a) {},
        "474b": function (t, e, a) {
            (function (o) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var i = a("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                $host: this.$host,
                                page_signs: "/pages/main_shop_order/main_shop_order",
                                page: 1,
                                morePro: !1,
                                baseinfo: {},
                                orderinfo: [],
                                orderinfo_length: 0,
                                type: 9,
                                flag: 0,
                                status: 0,
                                showmask: !1,
                                kuaidi: ["选择快递", "圆通", "中通", "申通", "顺丰", "韵达", "天天", "百世", "EMS", "本人到店", "其他"],
                                index: 0,
                                showhx: 0,
                                hxmm: "",
                                hxmm_list: [{
                                    val: "",
                                    fs: !0
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }],
                                is_focus: !1,
                                hx_choose: 0,
                                hx_ewm: "",
                                showPay: 0,
                                choosepayf: 0,
                                mymoney: 0,
                                mymoney_pay: 1,
                                is_submit: 1,
                                order_id: 0,
                                h5_wxpay: 0,
                                h5_alipay: 0,
                                pay_type: 1,
                                pay_money: 0,
                                searchkeyword: "",
                                suid: 0
                            };
                        },
                        onLoad: function (t) {
                            this._baseMin(this);
                            var e = 0;
                            t.fxsid && (e = t.fxsid), this.fxsid = e, t.flag && (this.flag = t.flag);
                            var a = o.getStorageSync("source");
                            this.source = a;
                            var s = o.getStorageSync("suid");
                            s && (this.suid = s), i.getOpenid(e, function () {});
                        },
                        onShow: function () {
                            this.page = 1, this.getLists();
                        },
                        onPullDownRefresh: function () {
                            this.getLists(), o.stopPullDownRefresh();
                        },
                        onReachBottom: function () {
                            var e = this,
                                a = 1 * e.page + 1;
                            o.request({
                                url: this.$host + "/api/MainWxapp/doPageGetMyOrders",
                                data: {
                                    page: a,
                                    uniacid: this.$uniacid,
                                    flag: this.flag,
                                    suid: this.suid
                                },
                                success: function (t) {
                                    e.orderinfo = e.orderinfo.concat(t.data.data.orders), e.page = a;
                                }
                            });
                        },
                        methods: {
                            toapplyall: function (t) {
                                var e = t.currentTarget.dataset.order_id;
                                console.log(t.currentTarget.dataset.order_id);
                                var a = "/pages/applyAfterSales/applyAfterSales?order_id=" + e + "&from_to=1";
                                o.navigateTo({
                                    url: a
                                });
                            },
                            changflag: function (t) {
                                this.searchkeyword = "", this.flag = t.currentTarget.dataset.status, this.getLists();
                            },
                            getLists: function () {
                                var e = this;
                                o.request({
                                    url: this.$host + "/api/MainWxapp/doPageGetMyOrders",
                                    data: {
                                        page: 1,
                                        uniacid: this.$uniacid,
                                        flag: this.flag,
                                        suid: this.suid
                                    },
                                    header: {
                                        "custom-header": "hello"
                                    },
                                    success: function (t) {
                                        e.orderinfo = t.data.data.orders, e.orderinfo_length = e.orderinfo.length, e.mymoney = t.data.data.money;
                                    },
                                    fail: function (t) {
                                        console.log(t);
                                    }
                                });
                            },
                            goevaluate: function (t) {
                                var e = t.currentTarget.dataset.order_id,
                                    a = t.currentTarget.dataset.type;
                                o.navigateTo({
                                    url: "/pagesOther/evaluate_pro/evaluate_pro?order_id=" + e + "&type=" + a
                                });
                            },
                            hxshow: function (t) {
                                this.showhx = 1, this.order_id = t.target.id;
                            },
                            hxhide: function () {
                                this.showhx = 0, this.hxmm = "";
                            },
                            onFocus: function (t) {
                                this.isFocus = !0;
                            },
                            hxmmInput: function (t) {
                                for (var e = t.target.value.length, a = 0; a < this.hxmm_list.length; a++) this.hxmm_list[a].fs = !1,
                                    this.hxmm_list[a].val = t.target.value[a];
                                e && (this.hxmm_list[e - 1].fs = !0), this.hxmm = t.target.value;
                            },
                            hxhide1: function () {
                                this.hx_choose = 0, this.hxmm = "";
                                for (var t = 0; t < this.hxmm_list.length; t++) this.hxmm_list[t].fs = !1, this.hxmm_list[t].val = "";
                            },
                            gethxmima: function () {
                                this.hx_choose = 1;
                            },
                            gethxImg: function () {
                                var e = this;
                                o.request({
                                    url: this.$baseurl + "doPageHxEwm",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: this.suid,
                                        pageUrl: "mainShop",
                                        orderid: this.order_id
                                    },
                                    success: function (t) {
                                        console.log(t.data), e.hx_ewm = t.data.data, e.hx_choose = 2;
                                    }
                                });
                            },
                            hxmmpass: function () {
                                var s = this;
                                this.hxmm ? o.request({
                                    url: this.$baseurl + "hxmm",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: this.suid,
                                        order_id: this.order_id,
                                        is_more: 5,
                                        hxmm: this.hxmm
                                    },
                                    success: function (t) {
                                        var e = t.data.data;
                                        if (0 == e) {
                                            o.showModal({
                                                title: "提示",
                                                content: "核销密码不正确！",
                                                showCancel: !1
                                            }), s.hxmm = "";
                                            for (var a = 0; a < s.hxmm_list.length; a++) s.hxmm_list[a].fs = !1, s.hxmm_list[a].val = "";
                                        } else 1 == e ? (s.showhx = 0, o.showModal({
                                            title: "提示",
                                            content: "核销成功！",
                                            showCancel: !1,
                                            success: function (t) {
                                                o.redirectTo({
                                                    url: "/pages/main_shop_order/main_shop_order"
                                                });
                                            }
                                        })) : 2 == e && o.showModal({
                                            title: "提示",
                                            content: "已核销!",
                                            showCancel: !1,
                                            success: function () {
                                                o.startPullDownRefresh(), this.showhx = 0, this.page = 1, this.getList(), o.stopPullDownRefresh();
                                            }
                                        });
                                    }
                                }) : o.showModal({
                                    title: "提示",
                                    content: "请输入核销密码！",
                                    showCancel: !1
                                });
                            },
                            qrshouh: function (t) {
                                var e = t.target.id,
                                    a = this.suid,
                                    s = this.$host,
                                    i = this.$uniacid;
                                o.showModal({
                                    title: "提示",
                                    content: "确认收货吗？",
                                    success: function (t) {
                                        t.confirm && o.request({
                                            url: s + "/api/mainwxapp/dopageqrshouh",
                                            data: {
                                                uniacid: i,
                                                suid: a,
                                                order_id: e
                                            },
                                            success: function (t) {
                                                o.showToast({
                                                    title: "收货成功！",
                                                    success: function (t) {
                                                        setTimeout(function () {
                                                            o.redirectTo({
                                                                url: "/pages/main_shop_order/main_shop_order"
                                                            });
                                                        }, 1500);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            makeCall: function (t) {
                                var e = t.currentTarget.dataset.sid;
                                if (console.log(e), 0 == e) {
                                    var a = o.getStorageSync("base_tel");
                                    o.makePhoneCall({
                                        phoneNumber: a
                                    });
                                } else o.request({
                                    url: this.$baseurl + "doPageShowstore_W",
                                    data: {
                                        uniacid: this.$uniacid,
                                        id: e
                                    },
                                    success: function (t) {
                                        var e = t.data.data.tel;
                                        o.makePhoneCall({
                                            phoneNumber: e
                                        });
                                    }
                                });
                            },
                            wlinfo: function (t) {
                                var e = t.currentTarget.dataset.kuaidi,
                                    a = t.currentTarget.dataset.kuaidihao,
                                    s = t.currentTarget.dataset.order_id;
                                null != e && null != a ? o.navigateTo({
                                    url: "/pages/logistics_state/logistics_state?kuaidi=" + e + "&kuaidihao=" + a + "&order_id=" + s
                                }) : o.navigateTo({
                                    url: "/pages/logistics_information/logistics_information?order_id=" + s
                                });
                            },
                            serachInput: function (t) {
                                this.searchkeyword = t.detail.value;
                            },
                            search: function () {
                                var e = this,
                                    t = this.searchkeyword,
                                    a = this.flag;
                                t ? o.request({
                                    url: this.$host + "/api/mainwxapp/doPageOrderSearch",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: this.suid,
                                        key: t,
                                        flag: a
                                    },
                                    success: function (t) {
                                        e.orderinfo = t.data.data.orders;
                                    }
                                }) : o.showModal({
                                    title: "提示",
                                    content: "请输入搜索内容！",
                                    showCancel: !1
                                });
                            },
                            paybox: function (t) {
                                var e = 0,
                                    a = this.orderinfo,
                                    s = t.currentTarget.dataset.order_id;
                                this.order_id = s;
                                for (var i = 0; i < a.length; i++) a[i].order_id == s && (e = 1 == a[i].is_change_price ? a[i].change_price : a[i].pay_money);
                                console.log("pay_money=" + e), this.pay_money = e, this.mymoney < e && (this.mymoney_pay = 2,
                                    this.pay_type = 2, this.choosepayf = 1), 0 == this.showPay && (this.showPay = 1);
                            },
                            payboxclose: function () {
                                1 == this.showPay && (this.showPay = 0, this.order_id = 0);
                            },
                            choosepay: function (t) {
                                this.pay_type = t.currentTarget.dataset.pay_type, this.choosepayf = t.currentTarget.dataset.type;
                            },
                            pay: function () {
                                if (2 == this.is_submit) return !1;
                                this.is_submit = 2, this.$uniacid;
                                var t = this.suid,
                                    e = this.source,
                                    a = this.pay_type,
                                    s = this.pay_money,
                                    i = this.order_id;
                                1 == a ? o.request({
                                    url: this.$baseurl + "payCallBackNotify",
                                    data: {
                                        uniacid: this.$uniacid,
                                        order_id: i,
                                        suid: t,
                                        payprice: s,
                                        types: "mainShop",
                                        flag: 0,
                                        fxsid: this.fxsid,
                                        source: e,
                                        pay_to: 0
                                    },
                                    success: function (t) {
                                        0 != t.data.data.error ? o.showToast({
                                            title: t.data.data.msg,
                                            icon: "none",
                                            mask: !0,
                                            success: function () {
                                                setTimeout(function () {
                                                    o.redirectTo({
                                                        url: "/pages/main_shop_order/main_shop_order"
                                                    });
                                                }, 1e3);
                                            }
                                        }) : o.showToast({
                                            title: "购买成功！",
                                            icon: "success",
                                            mask: !0,
                                            success: function () {
                                                setTimeout(function () {
                                                    o.redirectTo({
                                                        url: "/pages/main_shop_order/main_shop_order"
                                                    });
                                                }, 1e3);
                                            }
                                        });
                                    }
                                }) : this._showwxpay(this, s, "mainShop", i, this.form_id, "/pages/main_shop_order/main_shop_order");
                            },
                            cancelOrderNoPay: function (a) {
                                var s = this;
                                o.showModal({
                                    title: "提示",
                                    content: "该操作不可逆，是否确认取消",
                                    success: function (t) {
                                        if (t.confirm) {
                                            var e = a.currentTarget.dataset.order_id;
                                            o.request({
                                                url: s.$host + "/api/mainwxapp/doPageCancelOrderNoPay",
                                                data: {
                                                    uniacid: s.$uniacid,
                                                    suid: s.suid,
                                                    order_id: e
                                                },
                                                success: function (t) {
                                                    0 == t.data.data.error && o.showModal({
                                                        title: "提示",
                                                        content: "取消成功",
                                                        showCancel: !1,
                                                        success: function (t) {
                                                            s.page = 1, s.searchkeyword = "", s.getLists();
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    }
                                });
                            },
                            deleteOrder: function (t) {
                                var e = this,
                                    a = t.currentTarget.dataset.order_id;
                                o.showModal({
                                    title: "提示",
                                    content: "该操作不可逆，是否删除",
                                    success: function (t) {
                                        t.confirm && o.request({
                                            url: e.$host + "/api/mainwxapp/deleteOrder",
                                            data: {
                                                uniacid: e.$uniacid,
                                                suid: e.suid,
                                                order_id: a
                                            },
                                            success: function (t) {
                                                0 == t.data.data.error ? o.showModal({
                                                    title: "提示",
                                                    content: "删除成功",
                                                    showCancel: !1,
                                                    success: function (t) {
                                                        e.page = 1, e.searchkeyword = "", e.getLists();
                                                    }
                                                }) : o.showModal({
                                                    title: "提示",
                                                    content: t.data.data.msg,
                                                    showCancel: !1
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    };
                e.default = t;
            }).call(this, a("543d").default);
        },
        "7b87": function (t, e, a) {
            a.r(e);
            var s = a("474b"),
                i = a.n(s);
            for (var o in s) "default" !== o && function (t) {
                a.d(e, t, function () {
                    return s[t];
                });
            }(o);
            e.default = i.a;
        },
        9155: function (t, e, a) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                a("020c"), a("921b"), e(a("66fd")), t(e(a("17dd")).default);
            }).call(this, a("543d").createPage);
        }
    },
    [
        ["9155", "common/runtime", "common/vendor"]
    ]
]);