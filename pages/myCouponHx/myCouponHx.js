// pages/myCouponHx/myCouponHx.js
var config = require('../../siteinfo');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    lq_code: '', //兑换码
    title: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // console.log(options,'hexiao')
    // let code = JSON.parse(options.info).code
    // this.setData({
    //   lq_code: code
    // })
  },
  scanCode() {
    let that = this;
    wx.scanCode({
      success(res) {
        console.log(res, 'resss')
        that.result1 = res.result
        const info = JSON.parse(that.result1)
        console.log('info', info)
        that.setData({
          lq_code: info.code,
          title: info.coupon.title
        })
      }
    })
  },
  codeInput(e) {
    console.log(e.detail.value, 'codeInput')
    this.setData({
      lq_code: e.detail.value
    })
  },
  exchange() {
    if (this.data.lq_code == '') {
      wx.showToast({
        title: '兑换码不能为空',
        icon: 'none'
      })
      return
    }
    wx.request({
      url: config.siteroot1 + "verifyCode",
      data: {
        uniacid: config.uniacid,
        hx_suid: wx.getStorageSync('suid'),
        code: this.data.lq_code, //兑换码
      },
      header: {
        "custom-header": "hello"
      },
      success: function (e) {

        if (e.data.err) {
          wx.showToast({
            title: e.data.msg,
            icon: 'none'
          })
          return
        }
        console.log(e.data.product_id, 'exchangeClubCard')
        wx.showToast({
          title: '优惠卷'+e.data.title + '核销成功',
          icon: 'none',
          duration: 2000
        })
        setTimeout(() => {
          wx.redirectTo({
            url: '/pages/usercenter/usercenter',
          })
        }, 1000)

      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})