// pages/cardQuan/cardQuan.js
var config = require('../../siteinfo');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    page_signs: "/pages/myCouponNew/myCouponNew",
    cupponList: [],
    items_1: [null, null],
    tabsList: ['未使用', '已使用', '已过期'],
    current: 0,
    current1: 0,
    show: false,
    type: 0, // 0未使用 1已使用 2已过期
    dhmCode: '',
    cateList: [], //分类列表
    categoryId: '',//分类id
    isSubmit: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */

  onLoad(options) {
    this.getConfig()
    this.getList()
    // this.getCate()
  },
  toUseCouppon(e) {
    console.log(e.currentTarget.dataset.items.cid,'eeee')
    let id = e.currentTarget.dataset.items.cid
    let obj = e.currentTarget.dataset.items
    // obj.rules = e.currentTarget.dataset.rules
    // // str = str.replace(/"/g, "'");  
    // obj.title = e.currentTarget.dataset.title
    // var rules = [e.currentTarget.dataset.rules.replace(/"/g, "'")]
    // console.log(obj,'rules',rules[0])
    // let obj1 = JSON.stringify(obj).replace(/"/g, '\'')
    wx.navigateTo({
      url: `/pages/toUseCouponNew/toUseCouponNew?id=` + id + '&obj=' + JSON.stringify(obj),
    })
   
    // wx.navigateTo({
    //   url: `/pages/toUseCouponNew/toUseCouponNew?obj=${encodeURIComponent(obj1)}`,
    // })
  },
  // 修改折叠状态
  changeFlow(e) {
    let index = e.currentTarget.dataset.index
    this.data.cupponList[index].flow = !this.data.cupponList[index].flow
    this.setData({
      cupponList: this.data.cupponList
    })
  },
  // 兑换码输入值
  dhmInput(e) {
    this.setData({
      dhmCode: e.detail.value
    })
  },
  // 兑换礼券
  duihuan() {
    if (this.data.dhmCode == '') {
      wx.showToast({
        title: '兑换码不能为空',
        icon: 'none'
      })
      return
    }
    var _this = this;
    // wx.showToast({
    //   title: '兑换成功，去完善信息',
    //   icon: 'none'
    // })
    // _this.setData({
    //   show: false
    // })
    // wx.navigateTo({
    //   url: '/pages/completeInfo/completeInfo',
    // })
    wx.showLoading({
      title: '兑换中'
    })
    this.setData({
      isSubmit: true
    })
    wx.request({
      url: config.siteroot1 + "exchangeClubCard",
      data: {
        uniacid: config.uniacid,
        redeem_code: this.data.dhmCode,
        uid: wx.getStorageSync('golobeuid'),
        suid: wx.getStorageSync('suid')
      },
      header: {
        "custom-header": "hello"
      },
      success: function (e) {
        
        if (e.data.err) {
          wx.showToast({
            title: e.data.msg,
            icon: 'none'
          })
          _this.setData({
            isSubmit: false
          })
          return
          // wx.hideloading()
        }
        console.log(e.data.product_id,'exchangeClubCard')
        wx.hideLoading();
        _this.setData({
          isSubmit: false
        })
        wx.showToast({
          title: '兑换成功',
          icon: 'none',
          duration: 2000
        })
        _this.setData({
          show: false
        })
        // setTimeout(()=>{
        //   wx.navigateTo({
        //     url: '/pages/completeInfo/completeInfo?product_id=' + e.data.product_id,
        //   })
        // },1000)
       
        // _this.getList()
      }
    })
  },
  // 打开兑换弹窗
  openPopDh() {
    this.setData({
      show: true
    })
  },
  // 关闭弹窗
  closePop() {
    this.setData({
      show: false
    })
  },
  changeCate(e){
    this.setData({
      current1: e.currentTarget.dataset.index,
      categoryId: e.currentTarget.dataset.id
    })
    this.getList()
  },
  changeTab(e) {
    this.setData({
      current: e.currentTarget.dataset.index,
      type: e.currentTarget.dataset.index
    })
    // this.data.type = this.data.current
    // this.setData({

    // })
    this.getList()
  },
  getCate() {
    var _this = this;
    wx.request({
      url: config.siteroot1 + "couponCategory",
      data: {
        uniacid: config.uniacid
      },
      header: {
        "custom-header": "hello"
      },
      success: function (e) {
        console.log(e.data,'ekekekekekekke')
        let arr = e.data
        let newArr = [{id:0,name: '全部'}, ...arr];  
        console.log(newArr,'newArr')
        _this.setData({
          cateList: newArr,
          categoryId: newArr[0].id
        })
        // let id = e.data[0].id
        _this.getList()
      }
    })
  },
  // toList() {
  //   // https://yzhy.cg500.com/api/Wxapps/doPagegetNewSessionkey?uniacid=63
  // },
  // 时间戳转换
  timestampToDateString(value) {
    var timestamp = value.toString()
    // 时间戳为10位需*1000，时间戳为13位不需乘1000
    var date = new Date(timestamp * 1000);
    var Y = date.getFullYear() + ".";
    var M =
      (date.getMonth() + 1 < 10 ?
        "0" + (date.getMonth() + 1) :
        date.getMonth() + 1) + ".";
    var D = (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + " ";
    var h = date.getHours() + ":";
    var m = date.getMinutes() + ":";
    var s = date.getSeconds();
    return Y + M + D;
  },
  getList() {
    var _this = this;
    wx.request({
      url: config.siteroot1 + "userCoupon",
      data: {
        uniacid: config.uniacid,
        suid: wx.getStorageSync('suid'),
        uid: wx.getStorageSync('golobeuid'),
        type: this.data.type,
        category_id: this.data.categoryId
      },
      header: {
        "custom-header": "hello"
      },
      success: function (e) {
        let arr1 = e.data
        console.log(arr1,'11111')
        let arr = arr1.map(item => {
          return {
            ...item,
            flow: false
          }
        });
        _this.setData({
          cupponList: arr
        })
        console.log(_this.data.cupponList,'22222')
      }
    })
  },
  getConfig() {
    var _this = this;
    wx.request({
      url: config.siteroot + "doPageBaseMin",
      data: {
        uniacid: config.uniacid,
        vs1: 1,
        suid: 2
      },
      header: {
        "custom-header": "hello"
      },
      success: function (e) {
        _this.setData({
          baseinfo: e.data.data
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})