// pages/myCupponCode/myCupponCode.js
var config = require('../../siteinfo');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info: {},
    isSubmit: false, //true不能提交 false可以提交
    zc_discount: '', //中餐折扣
    zzh_discount: '', //自助餐折扣
    kf_discount: '', //客房折扣
    dtb_discount: '', //大堂吧折扣
    yyjs_discount: '', //游泳健身折扣
    xf_money: '', //消费金额
    remark: '', //备注
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log('options', options)
    let info = JSON.parse(options.info)
    console.log(info, 'info')
    this.setData({
      info: info
    })
    this.fomatTime(info.end_time)
  },
  // 中餐折扣
  zcDiscInput(e) {
    console.log(e.detail.value, '中餐折扣')
    this.setData({
      zc_discount: e.detail.value
    })
  },
  // 自助折扣
  zzhDiscInput(e) {
    console.log(e.detail.value, '自助折扣')
    this.setData({
      zzh_discount: e.detail.value
    })
  },
  // 客房折扣
  kfDiscInput(e) {
    console.log(e.detail.value, '客房折扣')
    this.setData({
      kf_discount: e.detail.value
    })
  },
  // 大堂吧折扣
  dtbDiscInput(e) {
    console.log(e.detail.value, '大堂吧折扣')
    this.setData({
      dtb_discount: e.detail.value
    })
  },
  // 游泳健身折扣
  yyjsDiscInput(e) {
    console.log(e.detail.value, '游泳健身折扣')
    this.setData({
      yyjs_discount: e.detail.value
    })
  },
  // 消费金额
  xfMoneyInput(e) {
    console.log(e.detail.value, '消费金额')
    this.setData({
      xf_money: e.detail.value
    })
  },
  // 备注
  remarkInput(e) {
    console.log(e.detail.value, '备注')
    this.setData({
      remark: e.detail.value
    })
  },
  fomatTime(value) {
    // 假设你有一个十位时间戳（以秒为单位）  
    var timestampInSeconds = value; // 假设这是某个日期的秒时间戳  

    // 将秒时间戳转换为毫秒时间戳  
    var timestampInMilliseconds = timestampInSeconds * 1000;

    // 使用Date对象将毫秒时间戳转换成日期  
    var date = new Date(timestampInMilliseconds);

    // 提取年、月、日  
    var year = date.getFullYear();
    var month = (date.getMonth() + 1).toString().padStart(2, '0'); // 确保月份为两位数  
    var day = date.getDate().toString().padStart(2, '0'); // 确保日期为两位数  

    // 格式化日期为 "YYYY.MM.DD"  
    var formattedDate = `${year}.${month}.${day}`;

    // 输出格式化后的日期  
    console.log(formattedDate); // 输出类似 "2024.05.06" 的字符串
    this.setData({
      "info.end_time": formattedDate
    })
  },
  // 提交
  submit() {
    this.setData({
      isSubmit: true
    })
    var _this = this;
    wx.showLoading({
      title: '提交中',
    })
    wx.request({
      url: config.siteroot1 + "consumeLog",
      data: {
        uniacid: config.uniacid,
        uid: this.data.info.uid,
        suid: this.data.info.suid,
        card_no: this.data.info.card_no,//卡号
        zczk: this.data.zc_discount, //中餐折扣
        zzczk: this.data.zzh_discount, //自助餐折扣
        kfzk: this.data.kf_discount, //客房折扣
        dtbzk: this.data.dtb_discount, //大堂吧折扣
        yyjszk: this.data.yyjs_discount, //游泳健身折扣
        money: this.data.xf_money, //消费金额
        remark: this.data.remark, //备注
        hx_suid: wx.getStorageSync('suid')
      },
      header: {
        "custom-header": "hello"
      },
      success: function (e) {

        if (e.data.err) {
          wx.showToast({
            title: e.data.msg,
            icon: 'none'
          })
          _this.setData({
            isSubmit: false
          })
          return
        }
        console.log(e.data.product_id, 'exchangeClubCard')
        wx.showToast({
          title: '成功',
          icon: 'none',
          duration: 2000
        })
        wx.hideLoading();
        setTimeout(() => {
          _this.setData({
            isSubmit: false
          })
          wx.redirectTo({
            url: '/pages/usercenter/usercenter',
          })
        }, 1000)

      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})