(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/open1/open1"], {
        "0633": function (e, t, n) {
            (function (d) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var a = n("f571"),
                    e = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                state: 0,
                                currentTab: 0,
                                processData: [],
                                grade: 0,
                                vipid: "",
                                baseinfo: [],
                                needAuth: !1,
                                needBind: !1,
                                pay_type: 1,
                                show_pay_type: 0,
                                alipay: 1,
                                wxpay: 1,
                                zf_money: 0,
                                is_ios: 0,
                                ids: "",
                                vids: ""
                            };
                        },
                        onPullDownRefresh: function () {
                            this.getvipgrade(), d.stopPullDownRefresh();
                        },
                        onLoad: function (e) {
                            var t = this;
                            d.setNavigationBarTitle({
                                title: "会员权益"
                            });
                            e.fxsid && (this.fxsid = e.fxsid), this._baseMin(this), a.getOpenid(0, function () {
                                t.getvipgrade();
                            }, function () {
                                t.needAuth = !0;
                            }, function () {
                                t.needBind = !0;
                            }), 0 <= d.getStorageSync("systemInfo").system.indexOf("iOS") && (this.is_ios = 1);
                        },
                        methods: {
                            swichNav: function (e) {
                                if (this.currentTab === e.currentTarget.dataset.current) return !1;
                                console.log(e), this.currentTab = e.currentTarget.dataset.current;
                            },
                            swiperChange: function (e) {
                                this.currentTab = e.detail.current;
                            },
                            getvipgrade: function () {
                                var s = this,
                                    e = d.getStorageSync("openid");
                                d.request({
                                    url: s.$baseurl + "doPagegetvipgrade",
                                    data: {
                                        uniacid: s.$uniacid,
                                        openid: e,
                                        suid: d.getStorageSync("suid")
                                    },
                                    success: function (e) {
                                        for (var t = e.data.data.vip, a = e.data.data.userinfo.grade, n = 0, i = 0; i < t.length; i++) a == t[i].grade && (n = i,
                                            s.ids = n);
                                        s.currentTab = n, s.processData = t, s.vids = "v" + s.ids, s.grade = a, s.vipid = e.data.data.userinfo.vipid,
                                            s.usermoney = e.data.data.userinfo.money ? e.data.data.userinfo.money : 0;
                                    }
                                });
                            },
                            open_grade: function (e) {
                                var a = this,
                                    t = this.baseinfo.ios;
                                if (1 == this.is_ios && 0 == t) d.showModal({
                                    title: "提示",
                                    content: "iOS系统不支持虚拟支付",
                                    showCancel: !1,
                                    success: function (e) {
                                        d.redirectTo({
                                            url: "/pages/index/index"
                                        });
                                    }
                                });
                                else {
                                    var n = e.currentTarget.dataset.grade,
                                        i = e.currentTarget.dataset.price,
                                        s = a.usermoney,
                                        o = 0,
                                        r = 0;
                                    i <= s ? o = i : r = i - (o = s), d.request({
                                        url: a.$baseurl + "doPagecreateorder",
                                        data: {
                                            uniacid: a.$uniacid,
                                            suid: d.getStorageSync("suid"),
                                            source: d.getStorageSync("source"),
                                            ordermoeny: i,
                                            yuemoney: o,
                                            money: r,
                                            open_grade: n,
                                            types: "vipgrade"
                                        },
                                        header: {
                                            "content-type": "application/json"
                                        },
                                        success: function (e) {
                                            var t = e.data.data;
                                            0 < t ? 0 == r ? d.showModal({
                                                title: "请注意",
                                                content: "您将使用余额支付" + o + "元",
                                                success: function (e) {
                                                    e.confirm && (a.payover_do(t), d.showLoading({
                                                        title: "下单中...",
                                                        mask: !0
                                                    }));
                                                }
                                            }) : a._showwxpay(a, r, "vipgrade", t, "", "/pages/usercenter/usercenter") : d.showModal({
                                                title: "提示",
                                                content: "生成订单失败",
                                                showCancel: !1
                                            });
                                        }
                                    });
                                }
                            },
                            showpay: function () {
                                var t = this;
                                d.request({
                                    url: this.$baseurl + "doPageGetH5payshow",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: d.getStorageSync("suid")
                                    },
                                    success: function (e) {
                                        0 == e.data.data.ali && 0 == e.data.data.wx ? d.showModal({
                                            title: "提示",
                                            content: "请联系管理员设置支付参数",
                                            success: function (e) {
                                                d.redirectTo({
                                                    url: "/pages/index/index"
                                                });
                                            }
                                        }) : (0 == e.data.data.ali ? (t.alipay = 0, t.pay_type = 2) : (t.alipay = 1, t.pay_type = 1),
                                            0 == e.data.data.wx ? t.wxpay = 0 : t.wxpay = 1, t.show_pay_type = 1);
                                    }
                                });
                            },
                            changealipay: function () {
                                this.pay_type = 1;
                            },
                            changewxpay: function () {
                                this.pay_type = 2;
                            },
                            close_pay_type: function () {
                                this.show_pay_type = 0;
                            },
                            h5topay: function () {
                                var e = this.pay_type;
                                1 == e ? this._alih5pay(this, this.zf_money, 16, this.orderid) : 2 == e && this._wxh5pay(this, this.zf_money, "vipgrade", this.orderid),
                                    this.show_pay_type = 0;
                            },
                            payover_do: function (e, t) {
                                var a = this;
                                d.request({
                                    url: a.$baseurl + "dopagepaynotify",
                                    data: {
                                        uniacid: a.$uniacid,
                                        out_trade_no: e,
                                        suid: d.getStorageSync("suid"),
                                        types: "vipgrade",
                                        open_grade: t
                                    },
                                    success: function (e) {
                                        "失败" == e.data.data.message ? d.showToast({
                                            title: "付款失败, 请刷新后重新付款！",
                                            icon: "none",
                                            mask: !0,
                                            success: function () {}
                                        }) : d.showToast({
                                            title: "购买成功！",
                                            icon: "success",
                                            mask: !0,
                                            success: function () {
                                                a.getvipgrade();
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    };
                t.default = e;
            }).call(this, n("543d").default);
        },
        "0d55": function (e, t, a) {},
        5989: function (e, t, a) {
            a.r(t);
            var n = a("7e79"),
                i = a("a5ca");
            for (var s in i) "default" !== s && function (e) {
                a.d(t, e, function () {
                    return i[e];
                });
            }(s);
            a("cd46");
            var o = a("2877"),
                r = Object(o.a)(i.default, n.a, n.b, !1, null, null, null);
            t.default = r.exports;
        },
        "7e79": function (e, t, a) {
            var n = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            a.d(t, "a", function () {
                return n;
            }), a.d(t, "b", function () {
                return i;
            });
        },
        a5ca: function (e, t, a) {
            a.r(t);
            var n = a("0633"),
                i = a.n(n);
            for (var s in n) "default" !== s && function (e) {
                a.d(t, e, function () {
                    return n[e];
                });
            }(s);
            t.default = i.a;
        },
        b083: function (e, t, a) {
            (function (e) {
                function t(e) {
                    return e && e.__esModule ? e : {
                        default: e
                    };
                }
                a("020c"), a("921b"), t(a("66fd")), e(t(a("5989")).default);
            }).call(this, a("543d").createPage);
        },
        cd46: function (e, t, a) {
            var n = a("0d55");
            a.n(n).a;
        }
    },
    [
        ["b083", "common/runtime", "common/vendor"]
    ]
]);