(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/orderDetail/orderDetail"], {
        "26d8": function (e, t, n) {
            n.r(t);
            var a = n("c113"),
                o = n("7ab6");
            for (var i in o) "default" !== i && function (e) {
                n.d(t, e, function () {
                    return o[e];
                });
            }(i);
            n("fba1");
            var r = n("2877"),
                u = Object(r.a)(o.default, a.a, a.b, !1, null, null, null);
            t.default = u.exports;
        },
        "2a2a9": function (e, t, n) {
            (function (e) {
                function t(e) {
                    return e && e.__esModule ? e : {
                        default: e
                    };
                }
                n("020c"), n("921b"), t(n("66fd")), e(t(n("26d8")).default);
            }).call(this, n("543d").createPage);
        },
        3796: function (e, n, o) {
            (function (i) {
                function t(e, t, n) {
                    return t in e ? Object.defineProperty(e, t, {
                        value: n,
                        enumerable: !0,
                        configurable: !0,
                        writable: !0
                    }) : e[t] = n, e;
                }
                Object.defineProperty(n, "__esModule", {
                    value: !0
                }), n.default = void 0;
                var a = o("f571"),
                    e = {
                        data: function () {
                            var e;
                            return {
                                $imgurl: this.$imgurl,
                                baseinfo: "",
                                tabbar: "",
                                orderid: "",
                                state: 1,
                                showmask: 0,
                                datas: (e = {
                                    jsondata: [{
                                        baseinfo: [],
                                        proinfo: []
                                    }],
                                    yhInfo_yhq: [],
                                    yhInfo_score: []
                                }, t(e, "yhInfo_yhq", []), t(e, "yhInfo_mj", []), t(e, "store_info", []), e),
                                orderFormDisable: !0,
                                isChange: "",
                                formchangeBtn: 2,
                                kuaidi: ["选择快递", "圆通", "中通", "申通", "顺丰", "韵达", "天天", "EMS", "百世", "本人到店", "其他"],
                                index: ""
                            };
                        },
                        onPullDownRefresh: function () {
                            this.getOrder(), i.stopPullDownRefresh();
                        },
                        onLoad: function (e) {
                            var t = this;
                            this._baseMin(this), this.orderid = e.orderid;
                            var n = 0;
                            e.fxsid && (n = e.fxsid), this.fxsid = n, a.getOpenid(n, function () {
                                t.getOrder();
                            });
                        },
                        methods: {
                            makePhoneCallC: function (e) {
                                i.makePhoneCall({
                                    phoneNumber: e.currentTarget.dataset.tel
                                });
                            },
                            getOrder: function () {
                                var t = this;
                                i.request({
                                    url: this.$baseurl + "doPagegetduoOrderDetail",
                                    data: {
                                        uniacid: this.$uniacid,
                                        order_id: this.orderid
                                    },
                                    success: function (e) {
                                        t.datas = e.data.data;
                                    }
                                });
                            },
                            copy: function (e) {
                                var t = e.target.id;
                                i.setClipboardData({
                                    data: t,
                                    success: function (e) {
                                        i.showToast({
                                            title: "复制成功"
                                        });
                                    }
                                });
                            },
                            qrshouh: function (e) {
                                var t = e.target.id,
                                    n = i.getStorageSync("openid"),
                                    a = this.$baseurl,
                                    o = this.$uniacid;
                                i.showModal({
                                    title: "提示",
                                    content: "确认收货吗？",
                                    success: function (e) {
                                        e.confirm && i.request({
                                            url: a + "dopagenewquerenxc",
                                            data: {
                                                uniacid: o,
                                                openid: n,
                                                orderid: t
                                            },
                                            success: function (e) {
                                                i.showToast({
                                                    title: "收货成功！",
                                                    success: function (e) {
                                                        setTimeout(function () {
                                                            i.redirectTo({
                                                                url: "/pages/orderDetail/orderDetail?orderid=" + t
                                                            });
                                                        }, 1500);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            tuihuo: function (e) {
                                this.showmask = 1, this.order_tuihuo = e.target.id;
                            },
                            tuikuan: function (e) {
                                var t = this,
                                    n = e.detail.formId,
                                    a = e.currentTarget.dataset.order;
                                i.showModal({
                                    title: "提醒",
                                    content: "确定要退款吗？",
                                    success: function (e) {
                                        e.confirm && i.request({
                                            url: t.$baseurl + "doPageduotk",
                                            data: {
                                                uniacid: t.$uniacid,
                                                formId: n,
                                                order_id: a
                                            },
                                            success: function (t) {
                                                console.log(t), 0 == t.data.data.flag ? i.showModal({
                                                    title: "提示",
                                                    content: t.data.data.message,
                                                    showCancel: !1,
                                                    success: function (e) {
                                                        i.redirectTo({
                                                            url: "/pages/orderDetail/orderDetail?orderid=" + a
                                                        });
                                                    }
                                                }) : i.showModal({
                                                    title: "很抱歉",
                                                    content: t.data.data.message,
                                                    confirmText: "联系客服",
                                                    success: function (e) {
                                                        e.confirm && i.makePhoneCall({
                                                            phoneNumber: t.data.mobile
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            bindPickerChange: function (e) {
                                this.index = e.detail.value;
                            },
                            changekdh: function (e) {
                                this.kdh = e.target.value;
                            },
                            cancelkdinfo: function () {
                                this.showmask = 0;
                            },
                            changekdinfo: function () {
                                var e = this;
                                0 == e.index ? i.showModal({
                                    title: "提交失败",
                                    content: "必须选择快递",
                                    showCancel: !1
                                }) : e.kdh ? i.request({
                                    url: e.$baseurl + "doPagenewtuihuo",
                                    data: {
                                        uniacid: e.$uniacid,
                                        order_id: e.order_tuihuo,
                                        kuaidi: e.kuaidi[e.index],
                                        kuaidihao: e.kdh
                                    },
                                    success: function (e) {
                                        1 == e.data.result && i.showToast({
                                            title: "已申请退货",
                                            icon: "success",
                                            success: function () {
                                                setTimeout(function () {
                                                    wx.redirectTo({
                                                        url: "/pages/order_more_list/order_more_list?flag=10&type1=10"
                                                    });
                                                }, 1500);
                                            }
                                        });
                                    }
                                }) : i.showModal({
                                    title: "提交失败",
                                    content: "快递号/信息必填",
                                    showCancel: !1
                                });
                            },
                            makephonecall: function () {
                                this.datas.seller_tel && i.makePhoneCall({
                                    phoneNumber: this.datas.seller_tel
                                });
                            }
                        }
                    };
                n.default = e;
            }).call(this, o("543d").default);
        },
        "7ab6": function (e, t, n) {
            n.r(t);
            var a = n("3796"),
                o = n.n(a);
            for (var i in a) "default" !== i && function (e) {
                n.d(t, e, function () {
                    return a[e];
                });
            }(i);
            t.default = o.a;
        },
        "7f5a": function (e, t, n) {},
        c113: function (e, t, n) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                o = [];
            n.d(t, "a", function () {
                return a;
            }), n.d(t, "b", function () {
                return o;
            });
        },
        fba1: function (e, t, n) {
            var a = n("7f5a");
            n.n(a).a;
        }
    },
    [
        ["2a2a9", "common/runtime", "common/vendor"]
    ]
]);