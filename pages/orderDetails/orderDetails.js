(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/orderDetails/orderDetails"], {
        "27ab": function (t, e, s) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                a = [];
            s.d(e, "a", function () {
                return i;
            }), s.d(e, "b", function () {
                return a;
            });
        },
        "43bf": function (t, e, s) {
            s.r(e);
            var i = s("b51a"),
                a = s.n(i);
            for (var o in i) "default" !== o && function (t) {
                s.d(e, t, function () {
                    return i[t];
                });
            }(o);
            e.default = a.a;
        },
        "5b36": function (t, e, s) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                s("020c"), s("921b"), e(s("66fd")), t(e(s("d5b8")).default);
            }).call(this, s("543d").createPage);
        },
        "5c86": function (t, e, s) {
            var i = s("6495");
            s.n(i).a;
        },
        6495: function (t, e, s) {},
        b51a: function (t, e, s) {
            (function (r) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var n = s("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                baseinfo: "",
                                orderdetails: "",
                                orderadmin: "",
                                orderstore: "",
                                hxmm_list: [{
                                    val: "",
                                    fs: !0
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }],
                                is_focus: !1,
                                hx_choose: 0,
                                hx_ewm: "",
                                showhx: 0,
                                hxmm: "",
                                showPay: 0,
                                choosepayf: 0,
                                mymoney: 0,
                                mymoney_pay: 1,
                                pay_type: 1,
                                pay_money: 0,
                                is_submit: 1,
                                h5_wxpay: 0,
                                h5_alipay: 0,
                                suid: 0,
                                source: "",
                                order_id: "",
                                can_apply: !1
                            };
                        },
                        onLoad: function (t) {
                            var e = this;
                            this._baseMin(this);
                            var s = 0;
                            t.fxsid && (s = t.fxsid), this.fxsid = s;
                            var i = 0;
                            t.order_id && (i = t.order_id), this.order_id = i;
                            var a = r.getStorageSync("suid");
                            a && (this.suid = a);
                            var o = r.getStorageSync("source");
                            o && (this.source = o), n.getOpenid(s, function () {
                                e.getorderdetails();
                            });
                        },
                        onPullDownRefresh: function () {
                            this.showhx = 0, this.hxmm = "", this.getorderdetails(), r.stopPullDownRefresh();
                        },
                        methods: {
                            orderSale: function (t) {
                                var e = t.currentTarget.dataset.order_service_id;
                                console.log(e), r.navigateTo({
                                    url: "/pages/orderAftersale/orderAftersale?order_service_id=" + e
                                });
                            },
                            makephone: function (t) {
                                var e = t.currentTarget.dataset.tel;
                                r.makePhoneCall({
                                    phoneNumber: e
                                });
                            },
                            getorderdetails: function () {
                                var e = this;
                                r.request({
                                    url: this.$host + "/api/mainwxapp/mainShopOrderDetails",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: this.suid,
                                        order_id: this.order_id
                                    },
                                    success: function (t) {
                                        console.log(t.data.data), e.orderdetails = t.data.data.order, e.orderadmin = t.data.data.order.address_info,
                                            e.orderstore = t.data.data.order.self_taking_info, e.mymoney = t.data.data.money,
                                            e.can_apply = e.orderdetails.can_apply;
                                    },
                                    fail: function (t) {
                                        console.log(t);
                                    }
                                });
                            },
                            copy: function (t) {
                                var e = t.currentTarget.dataset.text;
                                console.log(e), r.setClipboardData({
                                    data: e,
                                    success: function (t) {
                                        r.getClipboardData({
                                            success: function (t) {}
                                        }), r.showToast({
                                            title: "复制成功",
                                            duration: 2e3
                                        });
                                    }
                                });
                            },
                            paybox: function () {
                                var t, e = this.mymoney,
                                    s = this.orderdetails;
                                t = 1 == this.orderdetails.is_change_price ? s.change_price : s.pay_money, e < (this.pay_money = t) && (this.mymoney_pay = 2,
                                    this.pay_type = 2, this.choosepayf = 1), 0 == this.showPay && (this.showPay = 1);
                            },
                            payboxclose: function () {
                                1 == this.showPay && (this.showPay = 0);
                            },
                            choosepay: function (t) {
                                this.pay_type = t.currentTarget.dataset.pay_type, this.choosepayf = t.currentTarget.dataset.type;
                            },
                            pay: function () {
                                if (2 == this.is_submit) return !1;
                                this.is_submit = 2, this.$uniacid;
                                var t = this.suid,
                                    e = this.source,
                                    s = this.pay_type,
                                    i = this.pay_money,
                                    a = this.order_id;
                                1 == s ? r.request({
                                    url: this.$baseurl + "payCallBackNotify",
                                    data: {
                                        uniacid: this.$uniacid,
                                        order_id: a,
                                        suid: t,
                                        payprice: i,
                                        types: "mainShop",
                                        flag: 0,
                                        fxsid: this.fxsid,
                                        source: e,
                                        pay_to: 0
                                    },
                                    success: function (t) {
                                        0 != t.data.data.error ? r.showToast({
                                            title: t.data.data.msg,
                                            icon: "none",
                                            mask: !0,
                                            success: function () {
                                                setTimeout(function () {
                                                    r.redirectTo({
                                                        url: "/pages/main_shop_order/main_shop_order"
                                                    });
                                                }, 1e3);
                                            }
                                        }) : r.showToast({
                                            title: "购买成功！",
                                            icon: "success",
                                            mask: !0,
                                            success: function () {
                                                setTimeout(function () {
                                                    r.redirectTo({
                                                        url: "/pages/main_shop_order/main_shop_order"
                                                    });
                                                }, 1e3);
                                            }
                                        });
                                    }
                                }) : this._showwxpay(this, i, "mainShop", a, this.form_id, "/pages/main_shop_order/main_shop_order");
                            },
                            cancelOrderNoPay: function () {
                                var s = this;
                                r.showModal({
                                    title: "提示",
                                    content: "该操作不可逆，是否确认取消",
                                    success: function (t) {
                                        if (t.confirm) {
                                            var e = s.order_id;
                                            r.request({
                                                url: s.$host + "/api/mainwxapp/doPageCancelOrderNoPay",
                                                data: {
                                                    uniacid: s.$uniacid,
                                                    suid: s.suid,
                                                    order_id: e
                                                },
                                                success: function (t) {
                                                    0 == t.data.data.error && r.showModal({
                                                        title: "提示",
                                                        content: "取消成功",
                                                        showCancel: !1,
                                                        success: function (t) {
                                                            s.getorderdetails();
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    }
                                });
                            },
                            deleteOrder: function () {
                                var e = this,
                                    s = this.order_id;
                                r.showModal({
                                    title: "提示",
                                    content: "该操作不可逆，是否删除",
                                    success: function (t) {
                                        t.confirm && r.request({
                                            url: e.$host + "/api/mainwxapp/deleteOrder",
                                            data: {
                                                uniacid: e.$uniacid,
                                                suid: e.suid,
                                                order_id: s
                                            },
                                            success: function (t) {
                                                0 == t.data.data.error ? r.showModal({
                                                    title: "提示",
                                                    content: "删除成功",
                                                    showCancel: !1,
                                                    success: function (t) {
                                                        r.redirectTo({
                                                            url: "/pages/main_shop_order/main_shop_order"
                                                        });
                                                    }
                                                }) : r.showModal({
                                                    title: "提示",
                                                    content: t.data.data.msg,
                                                    showCancel: !1
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            toapplyall: function (t) {
                                var e = t.currentTarget.dataset.type,
                                    s = "/pages/applyAfterSales/applyAfterSales?";
                                if (1 == e) s = s + "order_id=" + this.order_id + "&from_to=1";
                                else if (2 == e) {
                                    s = s + "order_item_id=" + t.currentTarget.dataset.order_item_id + "&from_to=2";
                                }
                                r.navigateTo({
                                    url: s
                                });
                            },
                            qrshouh: function (t) {
                                var e = this,
                                    s = this.order_id,
                                    i = this.suid,
                                    a = this.$host,
                                    o = this.$uniacid;
                                r.showModal({
                                    title: "提示",
                                    content: "确认收货吗？",
                                    success: function (t) {
                                        t.confirm && r.request({
                                            url: a + "/api/mainwxapp/dopageqrshouh",
                                            data: {
                                                uniacid: o,
                                                suid: i,
                                                order_id: s
                                            },
                                            success: function (t) {
                                                r.showToast({
                                                    title: "收货成功！",
                                                    success: function (t) {
                                                        r.startPullDownRefresh(), e.showhx = 0, e.hxmm = "", e.getorderdetails(), r.stopPullDownRefresh();
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            wlinfo: function () {
                                var t = this.orderdetails,
                                    e = t.express,
                                    s = t.express_no,
                                    i = this.order_id;
                                null != e && null != s ? r.navigateTo({
                                    url: "/pages/logistics_state/logistics_state?kuaidi=" + e + "&kuaidihao=" + s + "&order_id=" + i
                                }) : r.navigateTo({
                                    url: "/pages/logistics_information/logistics_information?order_id=" + i
                                });
                            },
                            hxshow: function (t) {
                                this.showhx = 1;
                            },
                            hxhide: function () {
                                this.showhx = 0, this.hxmm = "";
                            },
                            onFocus: function (t) {
                                this.isFocus = !0;
                            },
                            hxmmInput: function (t) {
                                for (var e = t.target.value.length, s = 0; s < this.hxmm_list.length; s++) this.hxmm_list[s].fs = !1,
                                    this.hxmm_list[s].val = t.target.value[s];
                                e && (this.hxmm_list[e - 1].fs = !0), this.hxmm = t.target.value;
                            },
                            gethxmima: function () {
                                this.hx_choose = 1;
                            },
                            gethxImg: function () {
                                var e = this;
                                r.request({
                                    url: this.$baseurl + "doPageHxEwm",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: this.suid,
                                        pageUrl: "mainShop",
                                        orderid: this.order_id
                                    },
                                    success: function (t) {
                                        console.log(t.data), e.hx_ewm = t.data.data, e.hx_choose = 2;
                                    }
                                });
                            },
                            hxhide1: function () {
                                this.hx_choose = 0, this.hxmm = "";
                                for (var t = 0; t < this.hxmm_list.length; t++) this.hxmm_list[t].fs = !1, this.hxmm_list[t].val = "";
                            },
                            hxmmpass: function () {
                                var i = this;
                                this.hxmm ? r.request({
                                    url: this.$baseurl + "hxmm",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: this.suid,
                                        order_id: this.order_id,
                                        is_more: 5,
                                        hxmm: this.hxmm
                                    },
                                    success: function (t) {
                                        var e = t.data.data;
                                        if (0 == e) {
                                            r.showModal({
                                                title: "提示",
                                                content: "核销密码不正确！",
                                                showCancel: !1
                                            }), i.hxmm = "";
                                            for (var s = 0; s < i.hxmm_list.length; s++) i.hxmm_list[s].fs = !1, i.hxmm_list[s].val = "";
                                            console.log(i.hxmm);
                                        } else 1 == e ? r.showToast({
                                            title: "消费成功",
                                            icon: "success",
                                            duration: 2e3,
                                            success: function (t) {
                                                r.startPullDownRefresh(), i.showhx = 0, i.getorderdetails(), r.stopPullDownRefresh();
                                            }
                                        }) : 2 == e && r.showModal({
                                            title: "提示",
                                            content: "已核销!",
                                            showCancel: !1,
                                            success: function (t) {
                                                r.startPullDownRefresh(), i.showhx = 0, i.getorderdetails(), r.stopPullDownRefresh();
                                            }
                                        });
                                    }
                                }) : r.showModal({
                                    title: "提示",
                                    content: "请输入核销密码！",
                                    showCancel: !1
                                });
                            }
                        }
                    };
                e.default = t;
            }).call(this, s("543d").default);
        },
        d5b8: function (t, e, s) {
            s.r(e);
            var i = s("27ab"),
                a = s("43bf");
            for (var o in a) "default" !== o && function (t) {
                s.d(e, t, function () {
                    return a[t];
                });
            }(o);
            s("5c86");
            var r = s("2877"),
                n = Object(r.a)(a.default, i.a, i.b, !1, null, null, null);
            e.default = n.exports;
        }
    },
    [
        ["5b36", "common/runtime", "common/vendor"]
    ]
]);