var config = require('../../siteinfo');
(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/order_down/order_down"], {
        "23b3": function (t, a, e) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                o = [];
            e.d(a, "a", function () {
                return i;
            }), e.d(a, "b", function () {
                return o;
            });
        },
        "614a": function (t, a, e) {
            var i = e("c1cd");
            e.n(i).a;
        },
        "7ec7": function (t, a, e) {
            e.r(a);
            var i = e("23b3"),
                o = e("d349");
            for (var s in o) "default" !== s && function (t) {
                e.d(a, t, function () {
                    return o[t];
                });
            }(s);
            e("614a");
            var n = e("2877"),
                r = Object(n.a)(o.default, i.a, i.b, !1, null, null, null);
            a.default = r.exports;
        },
        c1cd: function (t, a, e) {},
        c9f9: function (t, a, e) {
            (function (C) {
                Object.defineProperty(a, "__esModule", {
                    value: !0
                }), a.default = void 0, e("2f62");
                var l = e("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                $host: this.$host,
                                baseinfo: "",
                                currentTab: 0,
                                showCouponBox: 0,
                                baoyou: -1,
                                default_address: "",
                                add_id: 0,
                                default_concat: "",
                                self_taking_time: "",
                                shop_info: "",
                                shop_id: 0,
                                take_self: 0,
                                buyPro: [],
                                total_num: 0,
                                total_price: 0,
                                from_gwc: 0,
                                nav: 1,
                                coupon_index: "",
                                coupon_id: 0,
                                coupon_use: 0,
                                coupon_list: [],
                                mymoney: 0,
                                gzscore: 0,
                                score_use: 0,
                                score_money: 0,
                                pay_money: "",
                                discount_money: 0,
                                total_discount_price: 0,
                                user_remark: "",
                                formlist_id: 0,
                                formlist_val: "",
                                suid: "",
                                buydata: "",
                                freight_money: 0,
                                error_msg: "",
                                error_msg_plus: "",
                                start_year: 2019,
                                end_year: 2060,
                                source: "",
                                free_package: 0,
                                openid: "",
                                showPay: 0,
                                choosepayf: 0,
                                mymoney_pay: 1,
                                pay_type: 1,
                                is_submit: 1,
                                use_score: 0,
                                h5_wxpay: 0,
                                h5_alipay: 0,
                                forminfo: "",
                                forminfolist: "",
                                error_num: 0
                            };
                        },
                        components: {
                            datetime: function () {
                                return e.e("components/datetime/datetime").then(e.bind(null, "9dfc"));
                            }
                        },
                        onLoad: function (t) {
                            var a, e = this;
                            a = C.getStorageSync("openid"), this.openid = a;
                            var i = C.getStorageSync("buydata");
                            i && (this.buydata = JSON.stringify(i));
                            var o = C.getStorageSync("suid");
                            o && (this.suid = o);
                            var s = C.getStorageSync("source");
                            this.source = s;
                            var n = t.from_gwc;
                            n && (this.from_gwc = n);
                            var r = t.currentTab;
                            r && (this.currentTab = r);
                            var d = t.nav;
                            d && (this.nav = d);
                            var c = t.add_id;
                            c && (this.add_id = c);
                            var u = t.shop_id;
                            u && (this.shop_id = u), this._baseMin(this);
                            var h = 0;
                            t.fxsid && (h = t.fxsid), this.fxsid = h, l.getOpenid(h, function () {
                                e.myContactInfo(), e.getBuyGoodsInfo(), e.getFormInfo();
                            });
                        },
                        methods: {
                          lingCard: function(){
                            // 领取会员卡
                            console.log('领取会员卡',config.siteroot1,this.buyPro[0].id,this.default_address.mobile,this.default_address.name)
                            var _this = this;
                            // wx.request({
                            //   url: config.siteroot1 + "getClubCard",
                            //   data: {
                            //     uniacid: config.uniacid,
                            //     suid: wx.getStorageSync('suid'),
                            //     uid: wx.getStorageSync('golobeuid'),
                            //     product_id: this.buyPro[0].id,
                            //     name: this.default_address.name,
                            //     mobile: this.default_address.mobile
                            //   },
                            //   header: {
                            //     "custom-header": "hello"
                            //   },
                            //   success: function (e) {
                            //     console.log(e, 'getClubCard')
                            //     if (e.data.err) {
                            //       wx.showToast({
                            //         title: e.data.msg,
                            //         icon: 'none'
                            //       })
                            //       return
                            //     }
                               
                            //   }
                            // })
                          },
                            chooseStore: function () {
                                C.navigateTo({
                                    url: "/pages/chooseStore/chooseStore?from_gwc=" + this.from_gwc + "&nav=" + this.nav + "&address_id=" + this.add_id + "&stid=" + this.shop_id + "&type=mainShop"
                                });
                            },
                            paybox: function () {
                                if (1 == this.currentTab) {
                                    if (!this.shop_id) return C.showModal({
                                        title: "提示",
                                        content: "请先选择门店",
                                        showCancel: !1
                                    }), !1;
                                    if (!this.self_taking_time) return C.showModal({
                                        title: "提示",
                                        content: "请先选择预约取件时间",
                                        showCancel: !1
                                    }), !1;
                                    var t = this.default_concat;
                                    if (!t) return C.showModal({
                                        title: "提示",
                                        content: "请先输入预留电话",
                                        showCancel: !1
                                    }), !1;
                                    if (!/^1[3456789]{1}\d{9}$/.test(t)) return C.showModal({
                                        title: "提醒",
                                        content: "请您输入正确的手机号码",
                                        showCancel: !1
                                    }), !1;
                                } else {
                                    if (!this.add_id) return C.showModal({
                                        title: "提示",
                                        content: "请先选择收件地址",
                                        showCancel: !1
                                    }), !1;
                                }
                                for (var a = this.forminfolist, e = 0; e < a.length; e++)
                                    if (1 == a[e].ismust)
                                        if (5 == a[e].type) {
                                            if ("" == a[e].z_val) return C.showModal({
                                                title: "提醒",
                                                content: a[e].name + "为必填项！",
                                                showCancel: !1
                                            }), !1;
                                        } else {
                                            // if ("" == a[e].val) return C.showModal({
                                            //     title: "提醒",
                                            //     content: a[e].name + "为必填项！",
                                            //     showCancel: !1
                                            // }), !1;
                                            if (0 == a[e].type && 1 == a[e].tp_text[0].yval) {
                                                if (!/^1[3456789]{1}\d{9}$/.test(a[e].val)) return C.showModal({
                                                    title: "提醒",
                                                    content: "请您输入正确的手机号码",
                                                    showCancel: !1
                                                }), !1;
                                            } else if (0 == a[e].type && 7 == a[e].tp_text[0].yval) {
                                                if (!/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test(a[e].val)) return C.showModal({
                                                    title: "提醒",
                                                    content: "请您输入正确的身份证号",
                                                    showCancel: !1
                                                }), !1;
                                            }
                                        }
                                this.mymoney < this.pay_money && (this.mymoney_pay = 2, this.pay_type = 2, this.choosepayf = 1),
                                    0 == this.showPay && (this.showPay = 1);
                            },
                            payboxclose: function () {
                                1 == this.showPay && (this.showPay = 0);
                            },
                            choosepay: function (t) {
                                this.pay_type = t.currentTarget.dataset.pay_type, this.choosepayf = t.currentTarget.dataset.type;
                            },
                            openDatetimePicker: function (t) {
                                this.$refs.myPicker.show();
                            },
                            closeDatetimePicker: function () {
                                this.$refs.myPicker.hide();
                            },
                            handleSubmit: function (t) {
                                this.self_taking_time = "".concat(t.year, "-").concat(t.month, "-").concat(t.day, " ").concat(t.hour, ":").concat(t.minute);
                            },
                            getCouponBox: function () {
                                this.showCouponBox = 1;
                            },
                            closeCouponBox: function () {
                                this.showCouponBox = 0;
                            },
                            changeCoupon: function (t) {
                                this.total_price;
                                var a = this.coupon_id,
                                    e = this.coupon_index,
                                    i = this.coupon_list,
                                    o = this.coupon_use,
                                    s = t.currentTarget.dataset.index;
                                i[s].can_use ? (0 < a ? e == s ? o = e = a = i[s].is_check = 0 : (i[e].is_check = 0,
                                        i[s].is_check = 1, a = i[s].id, o = i[e = s].price) : (i[s].is_check = 1, a = i[s].id,
                                        o = i[e = s].price), this.coupon_list = i, this.coupon_id = a, this.coupon_index = e,
                                    this.coupon_use = o, this.getpay(1), this.showCouponBox = 0) : C.showModal({
                                    title: "提示",
                                    content: i[s].can_use_msg + "，不可使用",
                                    showCancel: !1
                                });
                            },
                            getMyCoupon: function () {
                                var a = this;
                                C.request({
                                    url: this.$host + "/api/MainWxapp/doPageGetMyCoupon",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: this.suid,
                                        buydata: this.buydata,
                                        total_price: this.total_price
                                    },
                                    success: function (t) {
                                        a.coupon_list = t.data.data.coupon_list, a.mymoney = t.data.data.userinfo.money;
                                    }
                                });
                            },
                            toCatelist: function () {
                                C.navigateTo({
                                    url: "/pages/catelist/catelist"
                                });
                            },
                            getFormInfo: function () {
                                var a = this;
                                C.request({
                                    url: this.$host + "/api/MainWxapp/getFormInfo",
                                    data: {
                                        uniacid: this.$uniacid
                                    },
                                    success: function (t) {
                                        a.forminfo = t.data.data, a.forminfo && (a.formlist_id = t.data.data.id, a.formdescs = t.data.data.descs,
                                            a.forminfolist = t.data.data.tp_text);
                                    }
                                });
                            },
                            myContactInfo: function () {
                                var a = this;
                                C.request({
                                    url: a.$host + "/api/MainWxapp/getMyContactInfo",
                                    data: {
                                        uniacid: a.$uniacid,
                                        suid: a.suid,
                                        add_id: a.add_id,
                                        shop_id: a.shop_id
                                    },
                                    success: function (t) {
                                        a.baoyou = t.data.data.baoyou, a.default_address = t.data.data.default_address,
                                            a.default_address && (a.add_id = a.default_address.id, a.getFreight()), t.data.data.default_concat && (a.default_concat = t.data.data.default_concat.mobile),
                                            a.shop_info = t.data.data.shop_info, a.take_self = t.data.data.take_self;
                                    }
                                });
                            },
                            getBuyGoodsInfo: function () {
                                var i = this;
                                C.request({
                                    url: this.$host + "/api/MainWxapp/getBuyGoodsInfo",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: this.suid,
                                        buydata: this.buydata
                                    },
                                    success: function (t) {
                                        if (0 == t.data.data.error) {
                                            var a = t.data.data.buyPro;
                                            if (0 == i.from_gwc) {
                                                var e = 1 * a[0].kuaidi + 1;
                                                2 == e && (i.currentTab = 1), i.nav = e;
                                            } else 2 == i.nav && (i.currentTab = 1);
                                            i.buyPro = a, i.total_num = t.data.data.total_num, i.total_price = t.data.data.total_price,
                                                i.total_discount_price = t.data.data.total_discount_price, 0 < t.data.data.total_price && (i.getMyCoupon(),
                                                    i.getpay());
                                        } else C.showModal({
                                            title: "提示",
                                            content: t.data.data.msg,
                                            showCancel: !1,
                                            success: function (t) {
                                                C.navigateBack({
                                                    delta: 1
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            concat_input: function (t) {
                                this.default_concat = t.detail.value;
                            },
                            clickTab: function (t) {
                                var a = this;
                                if (a.currentTab == t.currentTarget.dataset.current) return !1;
                                a.currentTab = t.currentTarget.dataset.current, 1 == a.currentTab ? (a.error_msg_plus = "",
                                    a.getpay()) : (a.error_msg_plus = a.error_msg, a.getFreight());
                            },
                            add_address: function () {
                                C.navigateTo({
                                    url: "/pages/address/address?shareid=" + this.shareid + "&add_id=" + this.add_id + "&shop_id=" + this.shop_id + "&from_gwc=" + this.from_gwc + "&nav=" + this.nav
                                });
                            },
                            useScore: function () {
                                var a = this,
                                    t = this;
                                0 == t.use_score ? (t.use_score = 1, C.request({
                                    url: t.$host + "/api/mainwxapp/dopageptsetgwcscore",
                                    data: {
                                        uniacid: t.$uniacid,
                                        suid: t.suid,
                                        buydata: t.buydata
                                    },
                                    success: function (t) {
                                        a.score_use = t.data.data.jf, a.score_money = t.data.data.moneycl, a.gzscore = t.data.data.gzscore,
                                            0 < a.score_money && a.getpay(2), a.use_score = 1;
                                    }
                                })) : (t.use_score = 0, this.score_money = 0, this.score_use = 0, this.gzscore = 0,
                                    this.getpay());
                            },
                            getFreight: function () {
                                var e = this,
                                    t = this.default_address.address.split(" "),
                                    a = t[0] + " " + t[1],
                                    i = this.buydata;
                                C.request({
                                    url: this.$host + "/api/mainwxapp/doPageGetFreight",
                                    data: {
                                        uniacid: this.$uniacid,
                                        pro_city: a,
                                        buydata: i,
                                        suid: this.suid
                                    },
                                    success: function (t) {
                                        var a = t.data.data.error;
                                        e.error_num = t.data.data.error, -1 == a ? (e.freight_money = 0, e.free_package = 1) : 0 == a ? (e.freight_money = t.data.data.all_freight_price,
                                            0 < e.freight_money && e.getpay()) : 2 == a && 0 == e.currentTab && (e.error_msg = t.data.data.error_msg,
                                            e.error_msg_plus = t.data.data.error_msg);
                                    }
                                });
                            },
                            remarkInput: function (t) {
                                var a = t.detail.value;
                                this.user_remark = a;
                            },
                            getpay: function (t) {
                                var a = this.total_price,
                                    e = this.coupon_use,
                                    i = this.freight_money,
                                    o = this.baoyou;
                                1 == this.currentTab ? (i = 0, this.freight_money = 0, this.free_package = 0) : o <= a && 0 <= o && (this.free_package = 2,
                                    i = this.freight_money = 0);
                                var s = this.score_money,
                                    n = 0;
                                a - e - s < 0 ? (1 == t ? this.coupon_use = a - s : 0 < (s = Math.ceil(a - e)) ? (this.score_use = parseInt(this.gzscore * s),
                                        this.score_money = s) : (this.score_use = 0, this.score_money = 0), n = i) : n = (n = a - e - s + 1 * i) < 0 ? 0 : n.toFixed(2),
                                    this.pay_money = n;
                            },
                            pay: function (t) {
                                var e = this;
                                if (2 == this.is_submit) return !1;
                                this.is_submit = 2, C.showToast({
                                    title: "下单中",
                                    icon: "loading",
                                    mask: !0
                                });
                                var a = t.detail.formId,
                                    i = this.$uniacid,
                                    o = this.suid,
                                    s = this.source,
                                    n = this.buydata,
                                    r = 1 * this.currentTab + 1,
                                    d = this.pay_type,
                                    c = this.pay_money,
                                    u = (this.coupon_use + this.score_money).toFixed(2),
                                    h = this.freight_money,
                                    l = this.total_num,
                                    f = this.score_use,
                                    _ = this.score_money,
                                    p = this.coupon_id,
                                    m = this.coupon_use,
                                    y = this.from_gwc,
                                    g = this.user_remark,
                                    v = this.formlist_id,
                                    b = this.forminfolist,
                                    w = this.add_id,
                                    x = this.shop_id,
                                    k = this.self_taking_time,
                                    T = this.default_concat;
                                C.request({
                                    url: this.$host + "/api/mainwxapp/createMainGoodsOrder",
                                    data: {
                                        form_id: a,
                                        uniacid: i,
                                        suid: o,
                                        source: s,
                                        buydata: n,
                                        delivery_type: r,
                                        pay_type: d,
                                        pay_money: c,
                                        discount_money: u,
                                        freight_money: h,
                                        total_num: l,
                                        score_use: f,
                                        score_money: _,
                                        coupon_id: p,
                                        coupon_use: m,
                                        from_gwc: y,
                                        user_remark: g,
                                        formlist_id: v,
                                        address_id: w,
                                        self_taking_shop_id: x,
                                        self_taking_contact: T,
                                        self_taking_time: k,
                                        formlist_val: "" == b ? b : JSON.stringify(b)
                                    },
                                    success: function (t) {
                                        var a = t.data.data.order_id;
                                        t.data.data.error ? C.showModal({
                                            title: "提示",
                                            content: t.data.data.msg,
                                            showCancel: !1
                                        }) : 1 == d ? C.request({
                                            url: e.$baseurl + "payCallBackNotify",
                                            data: {
                                                uniacid: e.$uniacid,
                                                order_id: a,
                                                suid: o,
                                                payprice: c,
                                                types: "mainShop",
                                                flag: 0,
                                                fxsid: e.fxsid,
                                                source: s,
                                                pay_to: 0
                                            },
                                            success: function (t) {
                                                
                                                0 != t.data.data.error ? C.showToast({
                                                    title: t.data.data.msg,
                                                    icon: "none",
                                                    mask: !0,
                                                    success: function () {
                                                        setTimeout(function () {
                                                            C.redirectTo({
                                                                url: "/pages/main_shop_order/main_shop_order"
                                                            });
                                                        }, 1e3);
                                                    }
                                                }) : C.showToast({
                                                    title: "购买成功！",
                                                    icon: "success",
                                                    mask: !0,
                                                    success: function () {
                                                      e.lingCard()
                                                        setTimeout(function () {
                                                            C.redirectTo({
                                                                url: "/pages/main_shop_order/main_shop_order"
                                                            });
                                                        }, 1e3);
                                                    }
                                                });
                                            }
                                        }) : e._showwxpay(e, c, "mainShop", a, e.form_id, "/pages/main_shop_order/main_shop_order");
                                    }
                                });
                            },
                            weixinadd: function () {
                                var n = this;
                                C.chooseAddress({
                                    success: function (t) {
                                        for (var a = t.provinceName + " " + t.cityName + " " + t.countyName + " " + t.detailInfo, e = t.userName, i = t.telNumber, o = n.forminfolist, s = 0; s < o.length; s++) 0 == o[s].type && 2 == o[s].tp_text[0].yval && (o[s].val = e),
                                            0 == o[s].type && 3 == o[s].tp_text[0].yval && (o[s].val = i), 0 == o[s].type && 4 == o[s].tp_text[0].yval && (o[s].val = a);
                                        n.myname = e, n.mymobile = i, n.myaddress = a, n.forminfolist = o;
                                    },
                                    fail: function (t) {
                                        C.getSetting({
                                            success: function (t) {
                                                t.authSetting["scope.address"] || C.openSetting({
                                                    success: function (t) {}
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            getPhoneNumber: function (t) {
                                var i = this,
                                    a = t.detail.iv,
                                    e = t.detail.encryptedData;
                                "getPhoneNumber:ok" == t.detail.errMsg ? C.checkSession({
                                    success: function () {
                                        C.request({
                                            url: i.$baseurl + "doPagejiemiNew",
                                            data: {
                                                uniacid: i.$uniacid,
                                                newSessionKey: i.newSessionKey,
                                                iv: a,
                                                encryptedData: e
                                            },
                                            success: function (t) {
                                                if (t.data.data) {
                                                    for (var a = i.forminfolist, e = 0; e < a.length; e++) 0 == a[e].type && 5 == a[e].tp_text[0].yval && (a[e].val = t.data.data);
                                                    i.phoneNumber = t.data.data, i.wxmobile = t.data.data, i.forminfolist = a;
                                                } else C.showModal({
                                                    title: "提示",
                                                    content: "sessionKey已过期，请下拉刷新！"
                                                });
                                            },
                                            fail: function (t) {}
                                        });
                                    },
                                    fail: function () {
                                        C.showModal({
                                            title: "提示",
                                            content: "sessionKey已过期，请下拉刷新！"
                                        });
                                    }
                                }) : C.showModal({
                                    title: "提示",
                                    content: "请先授权获取您的手机号！",
                                    showCancel: !1
                                });
                            },
                            bindInputChange: function (t) {
                                var a = t.detail.value,
                                    e = t.currentTarget.dataset.index,
                                    i = this.forminfolist;
                                i[e].val = a, this.forminfolist = i;
                            },
                            bindPickerChange: function (t) {
                                var a = t.detail.value,
                                    e = t.currentTarget.dataset.index,
                                    i = this.forminfolist,
                                    o = i[e].tp_text[a];
                                i[e].val = o, this.forminfolist = i;
                            },
                            bindDateChange: function (t) {
                                var a = t.detail.value,
                                    e = t.currentTarget.dataset.index,
                                    i = this.forminfolist;
                                i[e].val = a, this.forminfolist = i;
                            },
                            bindTimeChange: function (t) {
                                var a = t.detail.value,
                                    e = t.currentTarget.dataset.index,
                                    i = this.forminfolist;
                                i[e].val = a, this.forminfolist = i;
                            },
                            checkboxChange: function (t) {
                                var a = t.detail.value,
                                    e = t.currentTarget.dataset.index,
                                    i = this.forminfolist;
                                i[e].val = a, this.forminfolist = i;
                            },
                            radioChange: function (t) {
                                var a = t.detail.value,
                                    e = t.currentTarget.dataset.index,
                                    i = this.forminfolist;
                                i[e].val = a, this.forminfolist = i;
                            },
                            choiceimg1111: function (t) {
                                var s = this,
                                    a = 0,
                                    n = s.zhixin,
                                    r = t.currentTarget.dataset.index,
                                    d = s.forminfolist,
                                    e = d[r].val,
                                    i = d[r].tp_text[0];
                                e ? a = e.length : (a = 0, e = []);
                                var o = i - a;
                                d[r].z_val && d[r].z_val, C.chooseImage({
                                    count: o,
                                    sizeType: ["original", "compressed"],
                                    sourceType: ["album", "camera"],
                                    success: function (t) {
                                        n = !0, s.zhixin = n, C.showLoading({
                                            title: "图片上传中"
                                        });
                                        var e = t.tempFilePaths,
                                            i = 0,
                                            o = e.length;
                                        ! function a() {
                                            C.uploadFile({
                                                url: s.$baseurl + "wxupimg",
                                                formData: {
                                                    uniacid: s.$uniacid
                                                },
                                                filePath: e[i],
                                                name: "file",
                                                success: function (t) {
                                                    d[r].z_val.push(t.data), s.forminfolist = d, ++i < o ? a() : (n = !1, s.zhixin = n,
                                                        C.hideLoading());
                                                }
                                            });
                                        }();
                                    }
                                });
                            },
                            delimg: function (t) {
                                var a = t.currentTarget.dataset.index,
                                    e = t.currentTarget.dataset.id,
                                    i = this.forminfolist,
                                    o = i[a].z_val;
                                o.splice(e, 1), 0 == o.length && (o = ""), i[a].z_val = o, this.forminfolist = i;
                            }
                        }
                    };
                a.default = t;
            }).call(this, e("543d").default);
        },
        cfeb: function (t, a, e) {
            (function (t) {
                function a(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                e("020c"), e("921b"), a(e("66fd")), t(a(e("7ec7")).default);
            }).call(this, e("543d").createPage);
        },
        d349: function (t, a, e) {
            e.r(a);
            var i = e("c9f9"),
                o = e.n(i);
            for (var s in i) "default" !== s && function (t) {
                e.d(a, t, function () {
                    return i[t];
                });
            }(s);
            a.default = o.a;
        }
    },
    [
        ["cfeb", "common/runtime", "common/vendor"]
    ]
]);