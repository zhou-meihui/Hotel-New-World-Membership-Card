(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/order_more_list/order_more_list"], {
        "0046": function (t, e, i) {
            (function (o) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var a = i("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                page_signs: "/pages/order_more_list/order_more_list",
                                page: 1,
                                morePro: !1,
                                baseinfo: {},
                                orderinfo: [],
                                type: 9,
                                type1: 10,
                                flag: 10,
                                showmask: !1,
                                kuaidi: ["选择快递", "圆通", "中通", "申通", "顺丰", "韵达", "天天", "百世", "EMS", "本人到店", "其他"],
                                index: 0,
                                showhx: 0,
                                hxmm: "",
                                hxmm_list: [{
                                    val: "",
                                    fs: !0
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }],
                                hx_choose: 0,
                                hx_ewm: ""
                            };
                        },
                        onLoad: function (t) {
                            this._baseMin(this);
                            var e = 0;
                            t.fxsid && (e = t.fxsid), this.fxsid = e, t.flag && (this.flag = t.flag), t.type1 && (this.type1 = t.type1),
                                a.getOpenid(e, function () {});
                        },
                        onShow: function () {
                            this.page = 1, this.getList();
                        },
                        onPullDownRefresh: function () {
                            this.getLists(), o.stopPullDownRefresh();
                        },
                        onReachBottom: function () {
                            var e = this,
                                a = 1 * e.page + 1;
                            o.request({
                                url: e.$baseurl + "doPageduoorderlist",
                                data: {
                                    page: a,
                                    uniacid: this.$uniacid,
                                    flag: this.flag,
                                    type1: this.type1,
                                    suid: o.getStorageSync("suid")
                                },
                                success: function (t) {
                                    e.orderinfo = e.orderinfo.concat(t.data.data), e.page = a;
                                }
                            });
                        },
                        methods: {
                            getLists: function () {
                                var e = this;
                                o.request({
                                    url: this.$baseurl + "doPageduoorderlist",
                                    data: {
                                        page: 1,
                                        uniacid: this.$uniacid,
                                        flag: this.flag,
                                        type1: this.type1,
                                        suid: o.getStorageSync("suid")
                                    },
                                    header: {
                                        "custom-header": "hello"
                                    },
                                    success: function (t) {
                                        e.orderinfo = t.data.data;
                                    }
                                });
                            },
                            getList: function () {
                                var e = this,
                                    t = o.getStorageSync("openid");
                                o.request({
                                    url: this.$baseurl + "doPageduoorderlist",
                                    data: {
                                        page: this.page,
                                        uniacid: this.$uniacid,
                                        flag: this.flag,
                                        type1: this.type1,
                                        openid: t,
                                        suid: o.getStorageSync("suid")
                                    },
                                    header: {
                                        "custom-header": "hello"
                                    },
                                    success: function (t) {
                                        e.orderinfo = 1 == e.page ? t.data.data : e.orderinfo.concat(t.data.data), e.page = e.page + 1;
                                    }
                                });
                            },
                            goevaluate: function (t) {
                                var e = t.currentTarget.dataset.order,
                                    a = t.currentTarget.dataset.type;
                                o.navigateTo({
                                    url: "/pagesOther/evaluate/evaluate?order_id=" + e + "&type=" + a
                                });
                            },
                            changflag: function (t) {
                                var e = this;
                                e.page = 1;
                                var a = t.currentTarget.dataset.flag,
                                    i = t.currentTarget.dataset.nav;
                                null != i && null != a ? (e.type1 = i, e.flag = a) : null == i && (e.flag = a),
                                    e.getList();
                            },
                            hxshow: function (t) {
                                this.showhx = 1, this.order = t.target.id;
                            },
                            hxhide: function () {
                                this.showhx = 0, this.hxmm = "";
                            },
                            hxmmInput: function (t) {
                                for (var e = t.target.value.length, a = 0; a < this.hxmm_list.length; a++) this.hxmm_list[a].fs = !1,
                                    this.hxmm_list[a].val = t.target.value[a];
                                e && (this.hxmm_list[e - 1].fs = !0), this.hxmm = t.target.value;
                            },
                            hxmmpass: function () {
                                var i = this;
                                this.hxmm || o.showModal({
                                    title: "提示",
                                    content: "请输入核销密码！",
                                    showCancel: !1
                                }), o.request({
                                    url: this.$baseurl + "hxmm",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: o.getStorageSync("suid"),
                                        order_id: this.order,
                                        is_more: 2,
                                        hxmm: this.hxmm
                                    },
                                    success: function (t) {
                                        console.log(t.data.data);
                                        var e = t.data.data;
                                        if (0 == e) {
                                            o.showModal({
                                                title: "提示",
                                                content: "核销密码不正确！",
                                                showCancel: !1
                                            }), i.hxmm = "";
                                            for (var a = 0; a < i.hxmm_list.length; a++) i.hxmm_list[a].fs = !1, i.hxmm_list[a].val = "";
                                        } else 1 == e ? o.showToast({
                                            title: "消费成功",
                                            icon: "success",
                                            duration: 2e3,
                                            success: function (t) {
                                                o.redirectTo({
                                                    url: "/pages/order_more_list/order_more_list?flag=10&type1=10"
                                                });
                                            }
                                        }) : 2 == e && o.showModal({
                                            title: "提示",
                                            content: "已核销!",
                                            showCancel: !1,
                                            success: function () {
                                                o.startPullDownRefresh(), that.page = 1, that.getList(), o.stopPullDownRefresh();
                                            }
                                        });
                                    }
                                });
                            },
                            qrshouh: function (t) {
                                var e = t.target.id,
                                    a = o.getStorageSync("suid"),
                                    i = this.$baseurl,
                                    s = this.$uniacid;
                                o.showModal({
                                    title: "提示",
                                    content: "确认收货吗？",
                                    success: function (t) {
                                        t.confirm && o.request({
                                            url: i + "dopagenewquerenxc",
                                            data: {
                                                uniacid: s,
                                                suid: a,
                                                orderid: e
                                            },
                                            success: function (t) {
                                                o.showToast({
                                                    title: "收货成功！",
                                                    success: function (t) {
                                                        setTimeout(function () {
                                                            o.redirectTo({
                                                                url: "/pages/order_more_list/order_more_list?flag=10&type1=10"
                                                            });
                                                        }, 1500);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            makeCall: function (t) {
                                var e = t.currentTarget.dataset.sid;
                                if (console.log(e), 0 == e) {
                                    var a = o.getStorageSync("base_tel");
                                    o.makePhoneCall({
                                        phoneNumber: a
                                    });
                                } else o.request({
                                    url: this.$baseurl + "doPageShowstore_W",
                                    data: {
                                        uniacid: this.$uniacid,
                                        id: e
                                    },
                                    success: function (t) {
                                        var e = t.data.data.tel;
                                        o.makePhoneCall({
                                            phoneNumber: e
                                        });
                                    }
                                });
                            },
                            wlinfo: function (t) {
                                var e = t.currentTarget.dataset.kuaidi,
                                    a = t.currentTarget.dataset.kuaidihao;
                                o.navigateTo({
                                    url: "/pages/logistics_state/logistics_state?kuaidi=" + e + "&kuaidihao=" + a
                                });
                            },
                            gethxmima: function () {
                                this.hx_choose = 1;
                            },
                            gethxImg: function () {
                                var e = this;
                                o.request({
                                    url: this.$baseurl + "doPageHxEwm",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: o.getStorageSync("suid"),
                                        pageUrl: "showProMore",
                                        orderid: this.order
                                    },
                                    success: function (t) {
                                        console.log(t.data), e.hx_ewm = t.data.data, e.hx_choose = 2;
                                    }
                                });
                            },
                            hxhide1: function () {
                                this.hx_choose = 0, this.hxmm = "";
                                for (var t = 0; t < this.hxmm_list.length; t++) this.hxmm_list[t].fs = !1, this.hxmm_list[t].val = "";
                            }
                        }
                    };
                e.default = t;
            }).call(this, i("543d").default);
        },
        5328: function (t, e, a) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                a("020c"), a("921b"), e(a("66fd")), t(e(a("8352")).default);
            }).call(this, a("543d").createPage);
        },
        7809: function (t, e, a) {
            a.r(e);
            var i = a("0046"),
                s = a.n(i);
            for (var o in i) "default" !== o && function (t) {
                a.d(e, t, function () {
                    return i[t];
                });
            }(o);
            e.default = s.a;
        },
        8352: function (t, e, a) {
            a.r(e);
            var i = a("a3ed"),
                s = a("7809");
            for (var o in s) "default" !== o && function (t) {
                a.d(e, t, function () {
                    return s[t];
                });
            }(o);
            a("e7a8");
            var n = a("2877"),
                r = Object(n.a)(s.default, i.a, i.b, !1, null, null, null);
            e.default = r.exports;
        },
        a3ed: function (t, e, a) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                s = [];
            a.d(e, "a", function () {
                return i;
            }), a.d(e, "b", function () {
                return s;
            });
        },
        c1e2: function (t, e, a) {},
        e7a8: function (t, e, a) {
            var i = a("c1e2");
            a.n(i).a;
        }
    },
    [
        ["5328", "common/runtime", "common/vendor"]
    ]
]);