// pages/personalInfo/personalInfo.js
var config = require('../../siteinfo');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // userInfo: {},
    avatar: '',
    nickname: '',
    avatarUrl: '',
    birthday:'',
    truename:'',
    mobile:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let userInfo = wx.getStorageSync('golobeuser')
    this.setData({
      avatar: userInfo.avatar,
      avatarUrl: userInfo.avatar,
      nickname: userInfo.nickname,
      mobile:userInfo.mobile,
      truename:userInfo.realname,
      birthday:userInfo.birthday,
    })
  },
  onChooseAvatar(e) {
    console.log(e.detail.avatarUrl,'onChooseAvatar')
    this.setData({
      avatar: e.detail.avatarUrl
    })
    // avatarUrl
    var _this = this;
    wx.uploadFile({
      url: config.siteroot + 'wxupimg?uniacid=' + config.uniacid, // 后台接口
      filePath: e.detail.avatarUrl, // 上传图片 url
      name: 'file',
      header: {}, // header 值
      success: res => {
        console.log(res.data,'dkdkdkdkkdkd')
        _this.setData({
          avatarUrl: res.data
        })
      },
      fail: e => {

      }
    });
  },
  //生日
  getbirthday(e){
    console.log(e.detail.value,'getbirthday')
    this.setData({
      birthday: e.detail.value
    })
  },
  //电话
  getmobile(e){
    console.log(e.detail.value,'getmobile')
    this.setData({
      mobile: e.detail.value
    })
  },
  //真实姓名
  gettruename(e){
    console.log(e.detail.value,'gettruename')
    this.setData({
      truename: e.detail.value
    })
  },
  // 昵称
  getNickname(e){
    console.log(e.detail.value,'getNickname')
    this.setData({
      nickname: e.detail.value
    })
  },
  updataInfo(){
    console.log(this.data.avatarUrl,this.data.nickname,'nickname')
    var _this = this;
    wx.showLoading({
      title: '上传中',
    })
    wx.request({
      url: config.siteroot1 + "editUser",
      data: {
        uniacid: config.uniacid,
        suid: wx.getStorageSync('suid'),
        uid: wx.getStorageSync('golobeuid'),
        nickname: this.data.nickname,
        avatar: this.data.avatarUrl,

        birthday: this.data.birthday,
        truename: this.data.truename,
        // nickname: this.data.mobile,
      },
      header: {
        "custom-header": "hello"
      },
      success: function (e) {
        console.log(e, 'verifyCode')
        if (e.data.err) {
          wx.showToast({
            title: e.data.msg,
            icon: 'none'
          })
          return
        }
        wx.hideLoading()
        wx.showToast({
          title: '修改成功',
          icon: 'none'
        })
        setTimeout(()=>{
          wx.redirectTo({
            url: '/pages/usercenter/usercenter',
          })
        },1000)
       
        // _this.setData({
        //   show: false
        // })
        // _this.getList()
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})