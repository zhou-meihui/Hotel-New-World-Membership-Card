(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/register/register"], {
        "0c46": function (e, t, a) {
            (function (d) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var e = (a("f571"), {
                    components: {
                        wPicker: function () {
                            return Promise.all([a.e("common/vendor"), a.e("components/w-picker/w-picker")]).then(a.bind(null, "8fa3"));
                        }
                    },
                    data: function () {
                        return {
                            $imgurl: this.$imgurl,
                            a: "1",
                            phoneNumber: "",
                            date: null,
                            region: null,
                            allregion: ["北京市", "北京市", "东城区"],
                            addressDetail: "",
                            name: "",
                            page_signs: "/pages/register/register",
                            formset: 0,
                            pagedata: [],
                            beizhu: "",
                            pagetype: "",
                            formdescs: "",
                            form_status: 0,
                            baseinfo: [],
                            userbg: "",
                            isover: 0,
                            wxmobile: "",
                            myname: "",
                            mymobile: "",
                            myaddress: "",
                            ttcxs: 0,
                            needAuth: !1,
                            needBind: !1,
                            superuser: "",
                            tabList: [{
                                mode: "region",
                                name: "省市区",
                                value: [0, 0, 0]
                            }],
                            userinfo: "",
                            is_sub: !1
                        };
                    },
                    onPullDownRefresh: function () {
                        this.refreshSessionkey(), d.stopPullDownRefresh();
                    },
                    onLoad: function (e) {
                        this.refreshSessionkey(), null != e.type && (this.pagetype = e.type), e.from && (this.from = e.from),
                            e.fxsid && e.fxsid, d.setNavigationBarTitle({
                                title: "开卡"
                            }), this.region = "北京市-北京市-东城区", this._getSuperUserInfo(this), this._baseMin(this),
                            this.checkApply(), this.getUserinfo(), this.registerForm();
                    },
                    computed: {
                        mode: function () {
                            return this.tabList[0].mode;
                        },
                        defaultVal: function () {
                            return this.tabList[0].value;
                        }
                    },
                    methods: {
                        cell: function () {
                            this.needAuth = !1;
                        },
                        closeAuth: function () {
                            this.needAuth = !1, this.needBind = !0;
                        },
                        closeBind: function () {
                            console.log("closeBind"), this.needBind = !1, this.checkApply();
                        },
                        getSuid: function () {
                            if (d.getStorageSync("suid")) return !0;
                            return d.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                !1;
                        },
                        toggleTab: function () {
                            this.$refs.picker.show();
                        },
                        checkApply: function () {
                            var t = this,
                                e = d.getStorageSync("openid");
                            d.request({
                                url: t.$baseurl + "doPagecheckApply",
                                data: {
                                    uniacid: t.$uniacid,
                                    suid: d.getStorageSync("suid"),
                                    openid: e,
                                    source: d.getStorageSync("source")
                                },
                                success: function (e) {
                                    3 == e.data.data.flag ? d.showModal({
                                        title: "提示",
                                        content: "已有申请记录，请等待审核！",
                                        showCancel: !1,
                                        success: function () {
                                            d.redirectTo({
                                                url: "/pages/index/index"
                                            });
                                        }
                                    }) : 1 != e.data.data.flag && 4 != e.data.data.flag || d.showModal({
                                        title: "提示",
                                        content: "您已经是会员！",
                                        showCancel: !1,
                                        success: function () {
                                            d.navigateBack({
                                                delta: 1
                                            });
                                        }
                                    }), t.userinfo = e.data.userinfo;
                                },
                                fail: function (e) {}
                            });
                        },
                        registerForm: function () {
                            var t = this,
                                e = d.getStorageSync("openid");
                            d.request({
                                url: t.$baseurl + "doPageRegisterFrom",
                                data: {
                                    uniacid: t.$uniacid,
                                    suid: d.getStorageSync("suid"),
                                    openid: e
                                },
                                success: function (e) {
                                    1 == e.data.data.flag ? (t.formset = 1, t.form_status = e.data.data.form_status,
                                        t.pagedata = e.data.data.form, t.beizhu = e.data.data.beizhu, t.formdescs = e.data.data.formdescs) : (t.formset = 0,
                                        t.form_status = e.data.data.form_status, t.beizhu = e.data.data.beizhu);
                                },
                                fail: function (e) {}
                            });
                        },
                        refreshSessionkey: function () {
                            var t = this;
                            d.login({
                                success: function (e) {
                                    d.request({
                                        url: t.$baseurl + "doPagegetNewSessionkey",
                                        data: {
                                            uniacid: t.$uniacid,
                                            code: e.code
                                        },
                                        success: function (e) {
                                            t.newSessionKey = e.data.data;
                                        }
                                    });
                                }
                            });
                        },
                        redirectto: function (e) {
                            var t = e.currentTarget.dataset.link,
                                a = e.currentTarget.dataset.linktype;
                            this._redirectto(t, a);
                        },
                        getUserinfo: function () {
                            var t = this;
                            d.request({
                                url: t.$baseurl + "doPageMymoney",
                                data: {
                                    uniacid: t.$uniacid,
                                    suid: d.getStorageSync("suid")
                                },
                                success: function (e) {
                                    t.userbg = e.data.data.userbg, t.cardname = e.data.data.cardname;
                                }
                            });
                        },
                        wxdz1: function () {
                            var t = this;
                            t.a = "2", d.chooseAddress({
                                success: function (e) {
                                    t.name = e.userName, t.region = e.provinceName + "-" + e.countyName + "-" + e.cityName,
                                        t.addressDetail = e.detailInfo;
                                },
                                fail: function () {
                                    d.showModal({
                                        title: "授权失败",
                                        content: "请重新授权",
                                        success: function (e) {
                                            e.confirm && d.openSetting({
                                                success: function () {
                                                    d.getSetting({
                                                        success: function (e) {
                                                            e.authSetting["scope.address"] ? d.chooseAddress({
                                                                success: function (e) {
                                                                    t.name = e.userName, t.region = e.provinceName + "-" + e.countyName + "-" + e.cityName,
                                                                        t.addressDetail = e.detailInfo;
                                                                }
                                                            }) : t.wxdz2();
                                                        }
                                                    });
                                                }
                                            }), e.cancel && t.wxdz2();
                                        }
                                    });
                                }
                            });
                        },
                        wxdz2: function () {
                            this.a = "1";
                        },
                        getPhoneNumber: function (e) {
                            var n = this,
                                t = e.detail.iv,
                                a = e.detail.encryptedData;
                            "getPhoneNumber:ok" == e.detail.errMsg ? d.checkSession({
                                success: function () {
                                    d.request({
                                        url: n.$baseurl + "doPagejiemiNew",
                                        data: {
                                            uniacid: n.$uniacid,
                                            newSessionKey: n.newSessionKey,
                                            iv: t,
                                            encryptedData: a
                                        },
                                        success: function (e) {
                                            if (e.data.data) {
                                                for (var t = n.pagedata, a = 0; a < t.length; a++) 0 == t[a].type && 5 == t[a].tp_text[0].yval && (t[a].val = e.data.data);
                                                n.phoneNumber = e.data.data, n.wxmobile = e.data.data, n.pagedata = t;
                                            } else d.showModal({
                                                title: "提示",
                                                content: "sessionKey已过期，请下拉刷新！"
                                            });
                                        },
                                        fail: function (e) {}
                                    });
                                },
                                fail: function () {
                                    d.showModal({
                                        title: "提示",
                                        content: "sessionKey已过期，请下拉刷新！"
                                    });
                                }
                            }) : d.showModal({
                                title: "提示",
                                content: "请先授权获取您的手机号！",
                                showCancel: !1
                            });
                        },
                        bindInputChange: function (e) {
                            var t = e.detail.value,
                                a = e.currentTarget.dataset.index,
                                n = this.pagedata;
                            n[a].val = t, this.pagedata = n;
                        },
                        bindPickerChange: function (e) {
                            var t = e.detail.value,
                                a = e.currentTarget.dataset.index,
                                n = this.pagedata,
                                i = n[a].tp_text[t];
                            n[a].val = i, this.pagedata = n;
                        },
                        bindDateChange: function (e) {
                            var t = e.detail.value,
                                a = e.currentTarget.dataset.index,
                                n = this.pagedata;
                            n[a].val = t, this.pagedata = n;
                        },
                        bindTimeChange: function (e) {
                            var t = e.detail.value,
                                a = e.currentTarget.dataset.index,
                                n = this.pagedata;
                            n[a].val = t, this.pagedata = n;
                        },
                        checkboxChange: function (e) {
                            var t = e.detail.value,
                                a = e.currentTarget.dataset.index,
                                n = this.pagedata;
                            n[a].val = t, this.pagedata = n;
                        },
                        radioChange: function (e) {
                            var t = e.detail.value,
                                a = e.currentTarget.dataset.index,
                                n = this.pagedata;
                            n[a].val = t, this.pagedata = n;
                        },
                        choiceimg1111: function (e) {
                            var s = this,
                                t = 0,
                                o = s.zhixin,
                                r = e.currentTarget.dataset.index,
                                c = s.pagedata,
                                a = c[r].val,
                                n = c[r].tp_text[0];
                            a ? t = a.length : (t = 0, a = []);
                            var i = n - t;
                            c[r].z_val && c[r].z_val, d.chooseImage({
                                count: i,
                                sizeType: ["original", "compressed"],
                                sourceType: ["album", "camera"],
                                success: function (e) {
                                    o = !0, s.zhixin = o, d.showLoading({
                                        title: "图片上传中"
                                    });
                                    var a = e.tempFilePaths,
                                        n = 0,
                                        i = a.length;
                                    ! function t() {
                                        d.uploadFile({
                                            url: s.$baseurl + "wxupimg",
                                            formData: {
                                                uniacid: s.$uniacid
                                            },
                                            filePath: a[n],
                                            name: "file",
                                            success: function (e) {
                                                c[r].z_val.push(e.data), s.pagedata = c, ++n < i ? t() : (o = !1, s.zhixin = o,
                                                    d.hideLoading());
                                            }
                                        });
                                    }();
                                }
                            });
                        },
                        delimg: function (e) {
                            var t = e.currentTarget.dataset.index,
                                a = e.currentTarget.dataset.id,
                                n = this.pagedata,
                                i = n[t].z_val;
                            i.splice(a, 1), 0 == i.length && (i = ""), n[t].z_val = i, this.pagedata = n;
                        },
                        namexz: function (e) {
                            for (var t = this, a = e.currentTarget.dataset.index, n = t.pagedata[a], i = [], s = 0; s < n.tp_text.length; s++) {
                                var o = {};
                                o.keys = n.tp_text[s], o.val = 1, i.push(o);
                            }
                            t.ttcxs = 1, t.formindex = a, t.xx = i, t.xuanz = 0, t.lixuanz = -1, t.riqi();
                        },
                        riqi: function () {
                            for (var e = this, t = new Date(), a = new Date(t.getTime()), n = a.getFullYear() + "-" + (a.getMonth() + 1) + "-" + a.getDate(), i = e.xx, s = 0; s < i.length; s++) i[s].val = 1;
                            e.xx = i, e.gettoday(n);
                            var o = [],
                                r = [],
                                c = new Date();
                            for (s = 0; s < 5; s++) {
                                var d = new Date(c.getTime() + 24 * s * 3600 * 1e3),
                                    u = d.getFullYear(),
                                    l = d.getMonth() + 1,
                                    f = d.getDate(),
                                    g = l + "月" + f + "日",
                                    h = u + "-" + l + "-" + f;
                                o.push(g), r.push(h);
                            }
                            e.arrs = o, e.fallarrs = r, e.today = n;
                        },
                        xuanzd: function (e) {
                            for (var t = this, a = e.currentTarget.dataset.index, n = t.fallarrs[a], i = t.xx, s = 0; s < i.length; s++) i[s].val = 1;
                            t.xuanz = a, t.today = n, t.lixuanz = -1, t.xx = i, t.gettoday(n);
                        },
                        goux: function (e) {
                            var t = e.currentTarget.dataset.index;
                            this.lixuanz = t;
                        },
                        gettoday: function (e) {
                            var i = this,
                                t = i.id,
                                a = i.formindex,
                                s = i.xx;
                            d.request({
                                url: i.$baseurl + "doPageDuzhan",
                                data: {
                                    uniacid: i.$uniacid,
                                    id: t,
                                    types: "showPro_lv_buy",
                                    days: e,
                                    pagedatekey: a
                                },
                                header: {
                                    "content-type": "application/json"
                                },
                                success: function (e) {
                                    for (var t = e.data.data, a = 0; a < t.length; a++) s[t[a]].val = 2;
                                    var n = 0;
                                    t.length == s.length && (n = 1), i.xx = s, i.isover = n;
                                }
                            });
                        },
                        getPhoneNumber1: function (e) {
                            var n = this,
                                t = e.detail.iv,
                                a = e.detail.encryptedData;
                            "getPhoneNumber:ok" == e.detail.errMsg ? d.checkSession({
                                success: function () {
                                    d.request({
                                        url: "entry/wxapp/jiemiNew",
                                        data: {
                                            newSessionKey: n.newSessionKey,
                                            iv: t,
                                            encryptedData: a
                                        },
                                        success: function (e) {
                                            if (e.data.data) {
                                                for (var t = n.pagedata, a = 0; a < t.length; a++) 0 == t[a].type && 5 == t[a].tp_text[0].yval && (t[a].val = e.data.data);
                                                n.wxmobile = e.data.data, n.pagedata = t;
                                            } else d.showModal({
                                                title: "提示",
                                                content: "sessionKey已过期，请下拉刷新！"
                                            });
                                        },
                                        fail: function (e) {}
                                    });
                                },
                                fail: function () {
                                    d.showModal({
                                        title: "提示",
                                        content: "sessionKey已过期，请下拉刷新！"
                                    });
                                }
                            }) : d.showModal({
                                title: "提示",
                                content: "请先授权获取您的手机号！",
                                showCancel: !1
                            });
                        },
                        weixinadd: function () {
                            var o = this;
                            d.chooseAddress({
                                success: function (e) {
                                    for (var t = e.provinceName + " " + e.cityName + " " + e.countyName + " " + e.detailInfo, a = e.userName, n = e.telNumber, i = o.pagedata, s = 0; s < i.length; s++) 0 == i[s].type && 2 == i[s].tp_text[0].yval && (i[s].val = a),
                                        0 == i[s].type && 3 == i[s].tp_text[0].yval && (i[s].val = n), 0 == i[s].type && 4 == i[s].tp_text[0].yval && (i[s].val = t);
                                    o.myname = a, o.mymobile = n, o.myaddress = t, o.pagedata = i;
                                },
                                fail: function (e) {
                                    d.getSetting({
                                        success: function (e) {
                                            e.authSetting["scope.address"] || d.openSetting({
                                                success: function (e) {}
                                            });
                                        }
                                    });
                                }
                            });
                        },
                        save_nb: function () {
                            var e = this,
                                t = e.today,
                                a = e.xx,
                                n = e.lixuanz;
                            if (-1 == n) return d.showModal({
                                title: "提现",
                                content: "请选择预约的选项",
                                showCancel: !1
                            }), !1;
                            var i = "已选择" + t + "，" + a[n].keys.yval,
                                s = e.pagedata,
                                o = e.formindex;
                            s[o].val = i, s[o].days = t, s[o].indexkey = o, s[o].xuanx = n, e.ttcxs = 0, e.pagedata = s;
                        },
                        quxiao: function () {
                            this.ttcxs = 0;
                        },
                        changeName: function (e) {
                            this.name = e.detail.value;
                        },
                        changeDate: function (e) {
                            this.date = e.detail.value;
                        },
                        changeRegion: function (e) {
                            var t = e.checkArr,
                                a = [t[0], t[1], t[2]];
                            this.allregion = a, this.region = a.join("-"), console.log(this.region);
                        },
                        changeDetail: function (e) {
                            this.addressDetail = e.detail.value;
                        },
                        confirmRegister: function (e) {
                            if (!this.getSuid()) return !1;
                            var t = this,
                                a = (d.getStorageSync("suid"), e.detail.formId),
                                n = t.form_status;
                            if (1 == n) {
                                if (!t.name) return d.showModal({
                                    title: "姓名不可为空！",
                                    content: "请重新输入",
                                    showCancel: !1
                                }), !1;
                                if (!t.phoneNumber) return d.showModal({
                                    title: "手机号不可为空！",
                                    content: "请重新输入",
                                    showCancel: !1
                                }), !1;
                                if (!t.date) return d.showModal({
                                    title: "生日不可为空！",
                                    content: "请重新输入",
                                    showCancel: !1
                                }), !1;
                                if (!t.region) return d.showModal({
                                    title: "地区不可为空！",
                                    content: "请重新输入",
                                    showCancel: !1
                                }), !1;
                                if (!t.addressDetail) return d.showModal({
                                    title: "详细地址不可为空！",
                                    content: "请重新输入",
                                    showCancel: !1
                                }), !1;
                            }
                            for (var i = t.pagedata, s = 0; s < i.length; s++)
                                if (1 == i[s].ismust)
                                    if (5 == i[s].type) {
                                        if ("" == i[s].z_val) return d.showModal({
                                            title: "提醒",
                                            content: i[s].name + "为必填项！",
                                            showCancel: !1
                                        }), !1;
                                    } else {
                                        if ("" == i[s].val) return d.showModal({
                                            title: "提醒",
                                            content: i[s].name + "为必填项！",
                                            showCancel: !1
                                        }), !1;
                                        if (0 == i[s].type && 1 == i[s].tp_text[0].yval) {
                                            if (!/^1[3456789]{1}\d{9}$/.test(i[s].val)) return d.showModal({
                                                title: "提醒",
                                                content: "请您输入正确的手机号码",
                                                showCancel: !1
                                            }), !1;
                                        } else if (0 == i[s].type && 7 == i[s].tp_text[0].yval) {
                                            if (!/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test(i[s].val)) return d.showModal({
                                                title: "提醒",
                                                content: "请您输入正确的身份证号",
                                                showCancel: !1
                                            }), !1;
                                        }
                                    }
                            var o = d.getStorageSync("openid"),
                                r = d.getStorageSync("subscribe");
                            if (0 < r.length && "" != r[7].mid) {
                                var c = new Array();
                                c.push(r[7].mid), d.requestSubscribeMessage({
                                    tmplIds: c,
                                    success: function (e) {
                                        t.is_sub = !0, d.request({
                                            url: t.$baseurl + "doPageregisterVIP",
                                            data: {
                                                uniacid: t.$uniacid,
                                                openid: o,
                                                suid: d.getStorageSync("suid"),
                                                source: d.getStorageSync("source"),
                                                name: t.name,
                                                phoneNumber: t.phoneNumber,
                                                date: t.date,
                                                region: t.region,
                                                addressDetail: t.addressDetail,
                                                formId: a,
                                                cardname: t.cardname,
                                                pagedata: JSON.stringify(i),
                                                form_status: n
                                            },
                                            cachetime: "30",
                                            success: function (e) {
                                                1 == e.data.data ? d.showModal({
                                                    title: "提示",
                                                    content: "申请成功，请等待审核！",
                                                    showCancel: !1,
                                                    success: function (e) {
                                                        d.redirectTo({
                                                            url: "/pages/usercenter/usercenter"
                                                        });
                                                    }
                                                }) : 3 == e.data.data ? d.showToast({
                                                    title: "开通成功！",
                                                    icon: "success",
                                                    success: function () {
                                                        setTimeout(function () {
                                                            d.redirectTo({
                                                                url: "/pages/usercenter/usercenter"
                                                            });
                                                        }, 1500);
                                                    }
                                                }) : d.showModal({
                                                    title: "提示",
                                                    content: "申请失败",
                                                    showCancel: !1
                                                });
                                            },
                                            fail: function (e) {}
                                        });
                                    }
                                });
                            } else t.is_sub = !0, d.request({
                                url: t.$baseurl + "doPageregisterVIP",
                                data: {
                                    uniacid: t.$uniacid,
                                    openid: o,
                                    suid: d.getStorageSync("suid"),
                                    source: d.getStorageSync("source"),
                                    name: t.name,
                                    phoneNumber: t.phoneNumber,
                                    date: t.date,
                                    region: t.region,
                                    addressDetail: t.addressDetail,
                                    formId: a,
                                    cardname: t.cardname,
                                    pagedata: JSON.stringify(i),
                                    form_status: n
                                },
                                cachetime: "30",
                                success: function (e) {
                                    1 == e.data.data ? d.showModal({
                                        title: "提示",
                                        content: "申请成功，请等待审核！",
                                        showCancel: !1,
                                        success: function (e) {
                                            d.redirectTo({
                                                url: "/pages/usercenter/usercenter"
                                            });
                                        }
                                    }) : 3 == e.data.data ? d.showToast({
                                        title: "开通成功！",
                                        icon: "success",
                                        success: function () {
                                            setTimeout(function () {
                                                d.redirectTo({
                                                    url: "/pages/usercenter/usercenter"
                                                });
                                            }, 1500);
                                        }
                                    }) : d.showModal({
                                        title: "提示",
                                        content: "申请失败",
                                        showCancel: !1
                                    });
                                },
                                fail: function (e) {}
                            });
                        }
                    }
                });
                t.default = e;
            }).call(this, a("543d").default);
        },
        "4a91": function (e, t, a) {
            var n = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            a.d(t, "a", function () {
                return n;
            }), a.d(t, "b", function () {
                return i;
            });
        },
        "4b3e": function (e, t, a) {
            a.r(t);
            var n = a("4a91"),
                i = a("50b4");
            for (var s in i) "default" !== s && function (e) {
                a.d(t, e, function () {
                    return i[e];
                });
            }(s);
            a("9bb7");
            var o = a("2877"),
                r = Object(o.a)(i.default, n.a, n.b, !1, null, null, null);
            t.default = r.exports;
        },
        "50b4": function (e, t, a) {
            a.r(t);
            var n = a("0c46"),
                i = a.n(n);
            for (var s in n) "default" !== s && function (e) {
                a.d(t, e, function () {
                    return n[e];
                });
            }(s);
            t.default = i.a;
        },
        "5d76": function (e, t, a) {},
        "9bb7": function (e, t, a) {
            var n = a("5d76");
            a.n(n).a;
        },
        e373: function (e, t, a) {
            (function (e) {
                function t(e) {
                    return e && e.__esModule ? e : {
                        default: e
                    };
                }
                a("020c"), a("921b"), t(a("66fd")), e(t(a("4b3e")).default);
            }).call(this, a("543d").createPage);
        }
    },
    [
        ["e373", "common/runtime", "common/vendor"]
    ]
]);