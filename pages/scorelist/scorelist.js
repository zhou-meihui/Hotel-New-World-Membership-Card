(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/scorelist/scorelist"], {
        6342: function (t, e, a) {
            var i = a("7d4b");
            a.n(i).a;
        },
        "7d4b": function (t, e, a) {},
        "87e5": function (t, e, a) {
            a.r(e);
            var i = a("ac16"),
                n = a("940b");
            for (var s in n) "default" !== s && function (t) {
                a.d(e, t, function () {
                    return n[t];
                });
            }(s);
            a("6342");
            var o = a("2877"),
                u = Object(o.a)(n.default, i.a, i.b, !1, null, null, null);
            e.default = u.exports;
        },
        "940b": function (t, e, a) {
            a.r(e);
            var i = a("c587"),
                n = a.n(i);
            for (var s in i) "default" !== s && function (t) {
                a.d(e, t, function () {
                    return i[t];
                });
            }(s);
            e.default = n.a;
        },
        "989a": function (t, e, a) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                a("020c"), a("921b"), e(a("66fd")), t(e(a("87e5")).default);
            }).call(this, a("543d").createPage);
        },
        ac16: function (t, e, a) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                n = [];
            a.d(e, "a", function () {
                return i;
            }), a.d(e, "b", function () {
                return n;
            });
        },
        c587: function (t, e, a) {
            (function (s) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var o = a("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                baseinfo: [],
                                userInfo: "",
                                searchtitle: "",
                                scopes: !1,
                                money: 0,
                                score: 0,
                                isview: 0,
                                page: 1,
                                xz: 1,
                                scorelists: [],
                                globaluser: [],
                                isover: 1,
                                needAuth: !1,
                                needBind: !1
                            };
                        },
                        onPullDownRefresh: function () {
                            this.getinfo();
                            var t = this.xz;
                            1 == t && this.getscore(), 2 == t && this.getmoney(), s.stopPullDownRefresh();
                        },
                        onLoad: function (t) {
                            var e = this,
                                a = this;
                            s.setNavigationBarTitle({
                                title: "积分明细"
                            });
                            var i = 0;
                            t.fxsid && (i = t.fxsid, a.fxsid = t.fxsid);
                            var n = t.type;
                            null != n && (a.xz = n), this._baseMin(this), s.getStorageSync("suid"), o.getOpenid(i, function () {
                                a.getinfo();
                                var t = a.xz;
                                1 == t && a.getscore(), 2 == t && a.getmoney();
                            }, function () {
                                e.needAuth = !0;
                            }, function () {
                                e.needBind = !0;
                            });
                        },
                        onReachBottom: function () {
                            var e = this,
                                a = e.page + 1;
                            1 == (s.getStorageSync("openid"), e.xz) ? s.request({
                                url: e.$baseurl + "doPagegetmyscorelist",
                                data: {
                                    uniacid: e.$uniacid,
                                    suid: s.getStorageSync("suid"),
                                    page: a
                                },
                                success: function (t) {
                                    1 == t.data.data.isover && (e.scorelists = e.scorelists.concat(t.data.data.lists),
                                        e.page = a);
                                }
                            }) : s.request({
                                url: e.$baseurl + "doPagegetmymoneylist",
                                data: {
                                    uniacid: e.$uniacid,
                                    suid: s.getStorageSync("suid"),
                                    page: a
                                },
                                success: function (t) {
                                    1 == t.data.data.isover && (e.scorelists = e.scorelists.concat(t.data.data.lists),
                                        e.page = a);
                                }
                            });
                        },
                        methods: {
                            redirectto: function (t) {
                                var e = t.currentTarget.dataset.link,
                                    a = t.currentTarget.dataset.linktype;
                                this._redirectto(e, a);
                            },
                            getinfo: function () {
                                var e = this;
                                s.request({
                                    url: e.$baseurl + "dopagealluserinfo",
                                    data: {
                                        uniacid: e.$uniacid,
                                        suid: s.getStorageSync("suid"),
                                        source: s.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        t.data.data && (e.globaluser = t.data.data);
                                    }
                                });
                            },
                            getscore: function () {
                                var e = this;
                                s.request({
                                    url: e.$baseurl + "doPagegetmyscorelist",
                                    data: {
                                        suid: s.getStorageSync("suid"),
                                        page: e.page,
                                        uniacid: e.$uniacid
                                    },
                                    success: function (t) {
                                        e.scorelists = t.data.data.lists;
                                    }
                                });
                            },
                            getmoney: function () {
                                var e = this;
                                s.request({
                                    url: e.$baseurl + "doPagegetmymoneylist",
                                    data: {
                                        suid: s.getStorageSync("suid"),
                                        page: e.page,
                                        uniacid: e.$uniacid
                                    },
                                    success: function (t) {
                                        e.scorelists = t.data.data.lists;
                                    }
                                });
                            },
                            qieh: function (t) {
                                var e = this,
                                    a = t.target.dataset.id;
                                e.xz != a && (e.xz = a, (e.page = 1) == a ? (e.getscore(), s.setNavigationBarTitle({
                                    title: "积分明细"
                                })) : (e.getmoney(), s.setNavigationBarTitle({
                                    title: "消费流水"
                                })));
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1;
                            }
                        }
                    };
                e.default = t;
            }).call(this, a("543d").default);
        }
    },
    [
        ["989a", "common/runtime", "common/vendor"]
    ]
]);