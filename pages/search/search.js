(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/search/search"], {
        "4af4": function (t, e, a) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                a("020c"), a("921b"), e(a("66fd")), t(e(a("5ba8")).default);
            }).call(this, a("543d").createPage);
        },
        "5ba8": function (t, e, a) {
            a.r(e);
            var i = a("6353"),
                n = a("a85a");
            for (var c in n) "default" !== c && function (t) {
                a.d(e, t, function () {
                    return n[t];
                });
            }(c);
            a("5cc5");
            var s = a("2877"),
                u = Object(s.a)(n.default, i.a, i.b, !1, null, null, null);
            e.default = u.exports;
        },
        "5cc5": function (t, e, a) {
            var i = a("7cd0");
            a.n(i).a;
        },
        6353: function (t, e, a) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                n = [];
            a.d(e, "a", function () {
                return i;
            }), a.d(e, "b", function () {
                return n;
            });
        },
        "6c9c": function (t, e, n) {
            (function (a) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var i = n("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                bg: "",
                                page_signs: "/sudu8/pages/search/search",
                                searchlist: [],
                                searchtype: [],
                                datas: "",
                                comment: "",
                                sc: 0,
                                paimai: "",
                                baseinfo: {},
                                needAuth: !1,
                                needBind: !1,
                                sid: 0,
                                type: ""
                            };
                        },
                        onPullDownRefresh: function () {
                            this.getsearch(), a.stopPullDownRefresh();
                        },
                        onLoad: function (t) {
                            var e = this;
                            this._baseMin(this);
                            var a = 0;
                            t.fxsid && (a = t.fxsid), this.fxsid = a, t.sid && (this.sid = t.sid), this.title = t.title,
                                this.paimai = t.paimai, t.type && (this.type = t.type), this.cid = t.cid, i.getOpenid(a, function () {
                                    e.getsearch();
                                });
                        },
                        methods: {
                            getsearch: function () {
                                var e = this;
                                a.request({
                                    url: e.$baseurl + "doPageProductsearch",
                                    data: {
                                        title: e.title,
                                        uniacid: e.$uniacid,
                                        paimai: e.paimai,
                                        sid: e.sid,
                                        cid: e.cid ? e.cid : 0,
                                        type: e.type ? e.type : "",
                                        latitude: a.getStorageSync("latitude"),
                                        longitude: a.getStorageSync("longitude")
                                    },
                                    success: function (t) {
                                        e.searchlist = t.data.data.list, e.searchtype = t.data.data.type, a.setNavigationBarTitle({
                                            title: e.title + "搜索结果"
                                        }), a.setStorageSync("isShowLoading", !1), a.hideNavigationBarLoading(), a.stopPullDownRefresh();
                                    }
                                });
                            }
                        }
                    };
                e.default = t;
            }).call(this, n("543d").default);
        },
        "7cd0": function (t, e, a) {},
        a85a: function (t, e, a) {
            a.r(e);
            var i = a("6c9c"),
                n = a.n(i);
            for (var c in i) "default" !== c && function (t) {
                a.d(e, t, function () {
                    return i[t];
                });
            }(c);
            e.default = n.a;
        }
    },
    [
        ["4af4", "common/runtime", "common/vendor"]
    ]
]);