(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/shoppay/shoppay"], {
        "32ee": function (e, t, a) {
            (function (y) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var n = a("f571"),
                    e = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                page_signs: "/pages/shoppay/shoppay",
                                baseinfo: [],
                                scopes: !1,
                                money: 0,
                                yue: 0,
                                guiz: [],
                                weixpay: 0,
                                paymoney: 0,
                                jifen_u: 0,
                                jfscore: 0,
                                jfmoney: 0,
                                jqdjg: "请选择",
                                yhq_hidden: 0,
                                yhqmoney: 0,
                                yhq_id: 0,
                                sid: 0,
                                heights: 0,
                                yhq_i: 0,
                                showModalStatus: !1,
                                animationData: "",
                                yhq: [],
                                score: 0,
                                shopsbase: [],
                                yhqid: 0,
                                needAuth: !1,
                                needBind: !1,
                                pay_type: 1,
                                show_pay_type: 0,
                                alipay: 1,
                                wxpay: 1,
                                zf_money: 0,
                                ischecked: !1
                            };
                        },
                        onPullDownRefresh: function () {
                            this.getinfos(), y.stopPullDownRefresh();
                        },
                        onLoad: function (e) {
                            var t = this;
                            y.setNavigationBarTitle({
                                title: "店内支付"
                            });
                            var a = this;
                            e.fxsid && (this.fxsid = e.fxsid), e.sid && (this.sid = e.sid), 0 < a.sid && a.getshopsbase(),
                                y.getSystemInfo({
                                    success: function (e) {
                                        t.heights = e.windowHeight;
                                    }
                                }), y.getStorageSync("suid"), this._baseMin(this), n.getOpenid(0, function () {
                                    a.getGuiz();
                                }, function () {
                                    t.needAuth = !0;
                                }, function () {
                                    t.needBind = !0;
                                });
                        },
                        methods: {
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1, that.getshopsbase();
                            },
                            getSuid: function () {
                                if (y.getStorageSync("suid")) return !0;
                                return y.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                    !1;
                            },
                            redirectto: function (e) {
                                var t = e.currentTarget.dataset.link,
                                    a = e.currentTarget.dataset.linktype;
                                this._redirectto(t, a);
                            },
                            getGuiz: function () {
                                var t = this,
                                    e = y.getStorageSync("suid");
                                y.request({
                                    url: t.$baseurl + "doPageGuiz",
                                    data: {
                                        suid: e,
                                        uniacid: t.$uniacid
                                    },
                                    success: function (e) {
                                        t.scoreconf = e.data.data.conf, t.yhq = e.data.data.coupon, t.guiz = e.data.data.user,
                                            t.yue = e.data.data.user.money, t.score = e.data.data.user.score, t.score_shoppay = e.data.data.conf.score_shoppay;
                                    },
                                    fail: function (e) {}
                                });
                            },
                            makePhoneCall: function (e) {
                                var t = this.baseinfo.tel;
                                y.makePhoneCall({
                                    phoneNumber: t
                                });
                            },
                            makePhoneCallB: function (e) {
                                var t = this.baseinfo.tel_b;
                                y.makePhoneCall({
                                    phoneNumber: t
                                });
                            },
                            chongz: function () {
                                y.redirectTo({
                                    url: "/pages/recharge/recharge"
                                });
                            },
                            switchChange: function (e) {
                                var t = this,
                                    a = !t.ischecked,
                                    n = (1e3 * t.paymoney - 1e3 * t.yhqmoney) / 1e3;
                                if (1 == a) {
                                    t.money;
                                    var i = t.weixpay,
                                        o = t.yue,
                                        s = t.score_shoppay,
                                        c = t.score,
                                        d = t.scoreconf;
                                    if (parseInt(s) >= parseInt(d.score) && parseInt(c) >= parseInt(d.score)) {
                                        var r = 0;
                                        if (parseInt(c) >= parseInt(s) ? 0 < d.score && (r = parseInt(s * d.money / d.score * 1)) : 0 < d.score && (r = parseInt(c * d.money / d.score * 1)),
                                            n <= r) t.weixpay = 0, t.money = 0, t.jfmoney = n.toFixed(2), t.jfscore = n * d.score,
                                            t.jifen_u = 1;
                                        else {
                                            var u = 0;
                                            0 < d.score && (u = r * (d.score / d.money)), i = (i = (1e3 * n - 1e3 * o - 1e3 * r) / 1e3) < 0 ? 0 : i,
                                                t.money = ((1e3 * n - 1e3 * i - 1e3 * r) / 1e3).toFixed(2), t.weixpay = i.toFixed(2),
                                                t.jfmoney = r.toFixed(2), t.jfscore = u, t.jifen_u = 1;
                                        }
                                    } else i = (i = (1e3 * n - 1e3 * o) / 1e3) < 0 ? 0 : i, t.money = ((1e3 * n - 1e3 * i) / 1e3).toFixed(2),
                                        t.weixpay = i.toFixed(2), t.jfmoney = 0, t.jfscore = 0, t.jifen_u = 1;
                                    t.ischecked = !0;
                                } else i = (i = (1e3 * n - 1e3 * (o = t.yue)) / 1e3) < 0 ? 0 : i, t.money = ((1e3 * n - 1e3 * i) / 1e3).toFixed(2),
                                    t.weixpay = i.toFixed(2), t.jfscore = 0, t.jfmoney = 0, t.jifen_u = 0, t.ischecked = !1;
                            },
                            setsubmit: function () {
                                if (!this.getSuid()) return !1;
                                var a = this,
                                    t = y.getStorageSync("openid"),
                                    n = a.paymoney,
                                    i = a.weixpay,
                                    o = a.money,
                                    s = a.jfscore,
                                    c = (a.yhqmoney,
                                        a.yhq_id),
                                    e = a.jfmoney;
                                if (!n && 0 == e || n <= 0 && 0 == e) return y.showModal({
                                    title: "提醒",
                                    content: "请输入正确的消费金额！",
                                    showCancel: !1
                                }), !1;
                                0 == i ? y.showModal({
                                    title: "提示",
                                    content: "确认支付,费用将从余额直接扣除!",
                                    cancelText: "取消支付",
                                    confirmText: "确认支付",
                                    success: function (e) {
                                        if (e.cancel) return !1;
                                        y.request({
                                            url: a.$baseurl + "doPageShoppay_duo",
                                            data: {
                                                uniacid: a.$uniacid,
                                                openid: t,
                                                suid: y.getStorageSync("suid"),
                                                ordermoeny: n,
                                                yuemoney: o,
                                                money: 0,
                                                order_id: "",
                                                jfscore: s,
                                                yhq_id: c,
                                                sid: a.sid
                                            },
                                            header: {
                                                "content-type": "application/json"
                                            },
                                            success: function (e) {
                                                y.showToast({
                                                    title: "支付成功",
                                                    icon: "success",
                                                    duration: 3e3,
                                                    success: function () {
                                                        setTimeout(function () {
                                                            y.redirectTo({
                                                                url: "/pages/shoppay/shoppay?sid=" + a.sid
                                                            });
                                                        }, 3e3);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }) : y.request({
                                    url: a.$baseurl + "doPageBalance",
                                    data: {
                                        uniacid: a.$uniacid,
                                        openid: t,
                                        yuemoney: o,
                                        money: i,
                                        yhq_id: c,
                                        jfscore: s,
                                        source: y.getStorageSync("source"),
                                        suid: y.getStorageSync("suid"),
                                        sid: a.sid
                                    },
                                    header: {
                                        "content-type": "application/json"
                                    },
                                    success: function (e) {
                                        var t = e.data.data.order_id;
                                        "success" == e.data.message && a._showwxpay(a, i, "shoppay", t, "", "/pages/shoppay/shoppay?sid=" + a.sid),
                                            "error" == e.data.message && y.showModal({
                                                title: "提醒",
                                                content: e.data.data.message,
                                                showCancel: !1
                                            });
                                    }
                                });
                            },
                            showpay: function () {
                                var t = this;
                                y.request({
                                    url: this.$baseurl + "doPageGetH5payshow",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: y.getStorageSync("suid")
                                    },
                                    success: function (e) {
                                        0 == e.data.data.ali && 0 == e.data.data.wx ? y.showModal({
                                            title: "提示",
                                            content: "请联系管理员设置支付参数",
                                            success: function (e) {
                                                y.redirectTo({
                                                    url: "/pages/index/index"
                                                });
                                            }
                                        }) : (0 == e.data.data.ali ? (t.alipay = 0, t.pay_type = 2) : (t.alipay = 1, t.pay_type = 1),
                                            0 == e.data.data.wx ? t.wxpay = 0 : t.wxpay = 1, t.show_pay_type = 1);
                                    }
                                });
                            },
                            changealipay: function () {
                                this.pay_type = 1;
                            },
                            changewxpay: function () {
                                this.pay_type = 2;
                            },
                            close_pay_type: function () {
                                this.show_pay_type = 0;
                            },
                            h5topay: function () {
                                var e = this.pay_type;
                                1 == e ? this._alih5pay(this, this.zf_money, 13, this.orderid) : 2 == e && this._wxh5pay(this, this.zf_money, "shoppay", this.orderid),
                                    this.show_pay_type = 0;
                            },
                            setmoney: function (e) {
                                var t = this,
                                    a = e.detail.value,
                                    n = e.detail.value;
                                if (t.jfmoney = 0, t.jfscore = 0, t.jifen_u = 0, t.yhq_id = 0, t.yhqmoney = 0, t.yhqid = 0,
                                    t.jqdjg = "请选择", t.ischecked = !1, "" == a || a <= 0) return t.money = 0, t.weixpay = 0,
                                    t.paymoney = 0, !1;
                                var i = (1e3 * a - 1e3 * t.yue) / 1e3;
                                i = i < 0 ? 0 : i, t.weixpay = i, t.money = (1e3 * a - 1e3 * i) / 1e3, t.paymoney = n;
                            },
                            getmoney: function (e) {
                                var t = this,
                                    a = t.paymoney,
                                    n = t.jfmoney,
                                    i = t.yhq,
                                    o = e.currentTarget.dataset.index,
                                    s = (i[o].price,
                                        i[o].pay_money),
                                    c = i[o].id,
                                    d = (1e3 * a - 1e3 * n) / 1e3;
                                if (d < s) return y.showModal({
                                    title: "提示",
                                    content: "需支付金额未满" + s + "元，不可使用该优惠券！",
                                    showCancel: !1
                                }), !1;
                                var r = i[o].price,
                                    u = (1e3 * d - 1e3 * t.yue - 1e3 * r) / 1e3;
                                u = u < 0 ? 0 : u, t.hideModal(), t.money = ((1e3 * d - 1e3 * u - 1e3 * r) / 1e3).toFixed(2) < 0 ? 0 : ((1e3 * d - 1e3 * u - 1e3 * r) / 1e3).toFixed(2),
                                    t.weixpay = u.toFixed(2), t.yhq_id = c, t.yhqmoney = r, t.jqdjg = i[o].title;
                            },
                            qxyh: function () {
                                var e = this,
                                    t = (1e3 * e.paymoney - 1e3 * e.jfmoney) / 1e3,
                                    a = (1e3 * t - 1e3 * e.yue) / 1e3;
                                a = a < 0 ? 0 : a, e.hideModal(), e.money = (1e3 * t - 1e3 * a) / 1e3, e.weixpay = a,
                                    e.yhq_id = 0, e.yhqmoney = 0, e.jqdjg = "请选择";
                            },
                            showModal: function () {
                                if (0 == this.paymoney) return y.showModal({
                                    title: "提醒",
                                    content: "请先输入消费金额",
                                    showCancel: !1
                                }), !1;
                                var e = y.createAnimation({
                                    duration: 200,
                                    timingFunction: "linear",
                                    delay: 0
                                });
                                (this.animation = e).translateY(300).step(), this.animationData = e.export(), this.showModalStatus = !0,
                                    setTimeout(function () {
                                        e.translateY(0).step(), this.animationData = e.export();
                                    }.bind(this), 200);
                            },
                            hideModal: function () {
                                var e = y.createAnimation({
                                    duration: 200,
                                    timingFunction: "linear",
                                    delay: 0
                                });
                                (this.animation = e).translateY(300).step(), this.animationData = e.export(), setTimeout(function () {
                                    e.translateY(0).step(), this.animationData = e.export(), this.showModalStatus = !1;
                                }.bind(this), 200);
                            },
                            getshopsbase: function () {
                                var t = this;
                                y.request({
                                    url: t.$baseurl + "doPagegetshopsbase",
                                    data: {
                                        uniacid: t.$uniacid,
                                        sid: t.sid
                                    },
                                    success: function (e) {
                                        t.shopsbase = e.data.data;
                                    }
                                });
                            }
                        }
                    };
                t.default = e;
            }).call(this, a("543d").default);
        },
        "538c": function (e, t, a) {},
        "6ab0": function (e, t, a) {
            a.r(t);
            var n = a("32ee"),
                i = a.n(n);
            for (var o in n) "default" !== o && function (e) {
                a.d(t, e, function () {
                    return n[e];
                });
            }(o);
            t.default = i.a;
        },
        "74f3": function (e, t, a) {
            var n = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            a.d(t, "a", function () {
                return n;
            }), a.d(t, "b", function () {
                return i;
            });
        },
        "8b80": function (e, t, a) {
            var n = a("538c");
            a.n(n).a;
        },
        "920c": function (e, t, a) {
            (function (e) {
                function t(e) {
                    return e && e.__esModule ? e : {
                        default: e
                    };
                }
                a("020c"), a("921b"), t(a("66fd")), e(t(a("9a46")).default);
            }).call(this, a("543d").createPage);
        },
        "9a46": function (e, t, a) {
            a.r(t);
            var n = a("74f3"),
                i = a("6ab0");
            for (var o in i) "default" !== o && function (e) {
                a.d(t, e, function () {
                    return i[e];
                });
            }(o);
            a("8b80");
            var s = a("2877"),
                c = Object(s.a)(i.default, n.a, n.b, !1, null, null, null);
            t.default = c.exports;
        }
    },
    [
        ["920c", "common/runtime", "common/vendor"]
    ]
]);