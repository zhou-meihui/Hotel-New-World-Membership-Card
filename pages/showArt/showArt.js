(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pages/showArt/showArt" ], {
    5902: function(t, a, e) {
        e.r(a);
        var i = e("7bf2"), s = e("90df");
        for (var n in s) "default" !== n && function(t) {
            e.d(a, t, function() {
                return s[t];
            });
        }(n);
        e("c7c0");
        var o = e("2877"), d = Object(o.a)(s.default, i.a, i.b, !1, null, null, null);
        a.default = d.exports;
    },
    "69a0": function(t, a, e) {
        (function(t) {
            function a(t) {
                return t && t.__esModule ? t : {
                    default: t
                };
            }
            e("020c"), e("921b"), a(e("66fd")), t(a(e("5902")).default);
        }).call(this, e("543d").createPage);
    },
    "7bf2": function(t, a, e) {
        var i = function() {
            this.$createElement;
            this._self._c;
        }, s = [];
        e.d(a, "a", function() {
            return i;
        }), e.d(a, "b", function() {
            return s;
        });
    },
    "90df": function(t, a, e) {
        e.r(a);
        var i = e("c384"), s = e.n(i);
        for (var n in i) "default" !== n && function(t) {
            e.d(a, t, function() {
                return i[t];
            });
        }(n);
        a.default = s.a;
    },
    bf53: function(t, a, e) {},
    c384: function(t, i, s) {
        (function(g) {
            Object.defineProperty(i, "__esModule", {
                value: !0
            }), i.default = void 0;
            var t, p = (t = s("3584")) && t.__esModule ? t : {
                default: t
            };
            function e(t, a, e) {
                return a in t ? Object.defineProperty(t, a, {
                    value: e,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : t[a] = e, t;
            }
            var y = g.createInnerAudioContext(), v = g.getBackgroundAudioManager();
            v.title = "文章音频";
            var n = s("f571"), a = (getApp(), {
                data: function() {
                    return {
                        $imgurl: this.$imgurl,
                        $host: this.$host,
                        id: "",
                        datas: "",
                        forminfo: [],
                        imgcount_xz: 0,
                        pagedata_set: [],
                        zh_all: 0,
                        zh_now: 0,
                        is_comment: 0,
                        comm: 0,
                        commSelf: 0,
                        comments: [],
                        comments_num: 0,
                        commShow: 0,
                        shareShow: 0,
                        shareScore: 0,
                        shareNotice: 0,
                        pd_val: [],
                        videopay: 1,
                        autoplay: !1,
                        poster: "",
                        navtel: "",
                        shareHome: 0,
                        fxsid: 0,
                        showLike: 0,
                        art_price: "",
                        music_price: "",
                        loopPlay: "",
                        music: "",
                        musicAutoPlay: "",
                        musicTitle: "",
                        musicpay: 1,
                        artpay: 1,
                        art_type: 0,
                        audioplay_flag: 1,
                        duration: 1,
                        curTimeVal: "",
                        durationDay: "播放获取",
                        curTimeValDay: "00:00",
                        musictype: "",
                        needAuth: !1,
                        needBind: !1,
                        baseinfo: {},
                        name: "",
                        time: "",
                        pinglun_t: "",
                        video_url: "",
                        myname: "",
                        mymobile: "",
                        myaddress: "",
                        wxmobile: "",
                        ttcxs: 0,
                        xuanz: -1,
                        lixuanz: -1,
                        xx: [],
                        formindex: -1,
                        arrs: [],
                        isover: 0,
                        formdescs: "",
                        newSessionKey: "",
                        zhixin: !1,
                        superuser: "",
                        userid: "",
                        price: 0,
                        pay_type: 0,
                        show_pay_type: 0,
                        wxpay: 0,
                        alipay: 0,
                        paymoney: 0,
                        block: "",
                        share: 0,
                        shareimg: 0,
                        shareimg_url: "",
                        system_w: 0,
                        system_h: 0,
                        img_w: 0,
                        img_h: 0,
                        is_ios: 0,
                        scollect: 0,
                        collects: 0,
                        is_like: 0,
                        likes: 0,
                        suid: 0
                    };
                },
                onLoad: function(t) {
                    var a = this;
                    this._baseMin(this);
                    var e = 0;
                    t.fxsid && (e = t.fxsid, g.setStorageSync("fxsid", e)), this.fxsid = e, this.id = t.id, 
                    t.userid && (this.userid = t.userid);
                    var i = g.getStorageSync("suid");
                    this.suid = i, this.refreshSessionkey(), n.getOpenid(e, function() {
                        a.getArt(), a._getSuperUserInfo(a);
                    });
                    var s = g.getStorageSync("systemInfo");
                    0 <= s.system.indexOf("iOS") && (this.is_ios = 1), this.img_w = parseInt((.65 * s.windowWidth).toFixed(0)), 
                    this.img_h = parseInt((1.875 * this.img_w).toFixed(0)), this.system_w = parseInt(s.windowWidth), 
                    this.system_h = parseInt(s.windowHeight);
                },
                onPullDownRefresh: function() {
                    this.refreshSessionkey(), this.getArt(), this.zhixin = !1, g.stopPullDownRefresh();
                },
                onShareAppMessage: function() {
                    var t, a = g.getStorageSync("suid"), e = this.id;
                    return t = 1 == this.superuser.fxs ? "/pages/showArt/showArt?id=" + e + "&userid=" + a : "/pages/showArt/showArt?id=" + e + "&fxsid=" + a + "&userid=" + a, 
                    {
                        title: this.title,
                        path: t
                    };
                },
                onUnload: function() {},
                methods: {
                    getArt: function() {
                        this.getPoductDetail(this.id);
                    },
                    refreshSessionkey: function() {
                        var a = this;
                        g.login({
                            success: function(t) {
                                g.request({
                                    url: a.$baseurl + "doPagegetNewSessionkey",
                                    data: {
                                        uniacid: a.$uniacid,
                                        code: t.code
                                    },
                                    success: function(t) {
                                        a.newSessionKey = t.data.data;
                                    }
                                });
                            }
                        });
                    },
                    onReady: function() {
                        this.refreshSessionkey(), this.audioCtx = g.createAudioContext("myAudio");
                    },
                    audioPlay: function() {
                        var d = this, t = d.musictype;
                        d.audioplay_flag = 2, 1 == t && (y.play(), y.onPlay(function(t) {
                            y.onTimeUpdate(function(t) {
                                var a = y.duration, e = parseInt(a / 60);
                                e < 10 && (e = "0" + e);
                                var i = parseInt(a - 60 * e);
                                i < 10 && (i = "0" + i);
                                var s = y.currentTime, n = parseInt(s / 60);
                                n < 10 && (n = "0" + n);
                                var o = parseInt(s - 60 * n);
                                o < 10 && (o = "0" + o), d.duration = 100 * y.duration.toFixed(2), d.curTimeVal = 100 * y.currentTime.toFixed(2), 
                                d.durationDay = e + ":" + i, d.curTimeValDay = n + ":" + o;
                            }), y.onEnded(function() {
                                d.setStopState(d);
                            });
                        })), 2 == t && ("" != d.music && (v.src = d.music), v.play());
                    },
                    audioPause: function() {
                        var t = this.musictype;
                        (this.audioplay_flag = 1) == t && y.pause(), 2 == t && v.pause();
                    },
                    slideBar: function(t) {
                        var a = t.detail.value;
                        this.curTimeVal = a / 100, y.seek(this.curTimeVal);
                    },
                    updateTime: function(d) {
                        y.onTimeUpdate(function(t) {
                            var a = y.duration, e = parseInt(a / 60);
                            e < 10 && (e = "0" + e);
                            var i = parseInt(a - 60 * e);
                            i < 10 && (i = "0" + i);
                            var s = y.currentTime, n = parseInt(s / 60);
                            n < 10 && (n = "0" + n);
                            var o = parseInt(s - 60 * n);
                            o < 10 && (o = "0" + o), d.duration = 100 * y.duration.toFixed(2), d.curTimeVal = 100 * y.currentTime.toFixed(2), 
                            d.durationDay = e + ":" + i, d.curTimeValDay = n + ":" + o;
                        }), y.duration.toFixed(2) - y.currentTime.toFixed(2) <= 0 && d.setStopState(d), 
                        y.onEnded(function() {
                            d.setStopState(d);
                        });
                    },
                    setStopState: function(t) {
                        var a = this.musictype;
                        t.curTimeVal = 0, 1 == a && y.stop(), 2 == a && v.stop();
                    },
                    redirectto: function(t) {
                        var a = t.currentTarget.dataset.link, e = t.currentTarget.dataset.linktype;
                        this._redirectto(a, e);
                    },
                    follow: function(t) {
                        var a = this, e = t.currentTarget.dataset.id, i = t.currentTarget.dataset.zan, s = g.getStorageSync("suid"), n = g.getStorageSync("openid");
                        g.request({
                            url: a.$baseurl + "commentFollow",
                            cachetime: "0",
                            data: {
                                uniacid: a.$uniacid,
                                id: e,
                                zan: i,
                                openid: n,
                                suid: s
                            },
                            success: function(t) {
                                1 == t.data.data.result ? (g.showToast({
                                    title: "点赞成功",
                                    icon: "success",
                                    duration: 1e3
                                }), a.getPoductDetail(a.id)) : 2 == t.data.data.result && (g.showToast({
                                    title: "取消赞成功",
                                    icon: "success",
                                    duration: 1e3
                                }), a.getPoductDetail(a.id));
                            }
                        });
                    },
                    pinglun: function(t) {
                        this.pinglun_t = t.detail.value;
                    },
                    pinglun_sub: function() {
                        var a = this, e = a.pinglun_t, i = a.id, s = g.getStorageSync("openid"), n = g.getStorageSync("suid");
                        return !!this.getSuid() && ("" == e || null == e ? (g.showModal({
                            content: "评论不能为空"
                        }), !1) : void g.request({
                            url: a.$baseurl + "doPageCheckContents",
                            data: {
                                uniacid: a.$uniacid,
                                content: e,
                                source: g.getStorageSync("source")
                            },
                            success: function(t) {
                                return 1 == t.data.data ? (g.showModal({
                                    title: "提示",
                                    content: "小程序相关信息错误，无法发布",
                                    showCancel: !1
                                }), !1) : 2 == t.data.data ? (g.showModal({
                                    title: "提示",
                                    content: "提交内容涉嫌违法违规，请修改后再提交",
                                    showCancel: !1
                                }), !1) : void (0 == t.data.data && g.request({
                                    url: a.$baseurl + "comment",
                                    data: {
                                        uniacid: a.$uniacid,
                                        suid: n,
                                        pinglun_t: e,
                                        id: i,
                                        openid: s
                                    },
                                    success: function(t) {
                                        1 == t.data.data.result && (g.showToast({
                                            title: "评价提交成功",
                                            icon: "success",
                                            duration: 2e3
                                        }), setTimeout(function() {
                                            g.redirectTo({
                                                url: "/pages/showArt/showArt?id=" + i
                                            });
                                        }, 2e3));
                                    }
                                }));
                            }
                        }));
                    },
                    getPoductDetail: function(h) {
                        var f = this, m = g.getStorageSync("suid");
                        console.log(111111), m && g.request({
                            url: f.$baseurl + "doPageMymoneyD",
                            data: {
                                uniacid: f.$uniacid,
                                suid: g.getStorageSync("suid")
                            },
                            success: function(t) {
                                var a = t.data.data;
                                f.mymoney = a.money, f.globaluser = a;
                            }
                        }), g.request({
                            url: f.$baseurl + "doPageproductsDetail",
                            data: {
                                id: h,
                                uniacid: f.$uniacid,
                                suid: m
                            },
                            cachetime: "0",
                            success: function(t) {
                                if (null != t.data.data.share_score && "null" != t.data.data.share_score) {
                                    if (-1 != t.data.data.share_score.indexOf("http")) {
                                        var a = encodeURIComponent(t.data.data.share_score);
                                        g.redirectTo({
                                            url: "/pages/webpage/webpage?url=" + a
                                        });
                                    }
                                    -1 != t.data.data.share_score.indexOf("pages") && g.redirectTo({
                                        url: t.data.data.share_score
                                    });
                                }
                                if (t.data.data.text && (t.data.data.text = t.data.data.text.replace(/\<img/gi, '<img style="width:100%;height:auto" '), 
                                t.data.data.text = (0, p.default)(t.data.data.text)), 0 != t.data.data.edittime) var e = t.data.data.edittime; else e = "" == t.data.data.etime ? t.data.data.ctime : t.data.data.etime;
                                if ("" == t.data.data.labels) var i = t.data.data.thumb; else i = t.data.data.labels;
                                if ("false" == t.data.data.market_price) var s = !1; else s = !0;
                                if (0 < parseInt(t.data.data.pro_flag)) {
                                    if (t.data.data.navlistnum) {
                                        var n = (100 / t.data.data.navlistnum).toFixed(2);
                                        f.pro_flag = t.data.data.pro_flag, f.navlist = t.data.data.navlist, f.navwidth = n + "%", 
                                        f.likeArtList = t.data.data.likeArtList;
                                    }
                                } else f.pro_flag = t.data.data.pro_flag;
                                var o = t.data.data.likeArtList;
                                o && 0 < o.length && (f.showLike = 1), f.collects = t.data.data.collects, f.scollect = t.data.data.is_collect, 
                                f.likes = t.data.data.likes, f.is_like = t.data.data.is_like;
                                var d = t.data.data.music_art_info.music, r = t.data.data.musicpay, u = t.data.data.music_art_info.autoPlay, c = t.data.data.music_art_info.loopPlay, l = 1;
                                t.data.data.music_art_info.musictype && (l = t.data.data.music_art_info.musictype), 
                                2 == l ? 1 == r && 1 == u && ("" != d && (f.audioplay_flag = 2, v.title = "文章音频", 
                                v.src = d), v.play(), Math.ceil(v.duration) - Math.ceil(v.currentTime) <= 0 && (c ? v.onEnded(function() {
                                    f.audioPlay();
                                }) : (v.onEnded(function() {
                                    f.setStopState(f);
                                }), f.audioplay_flag = 2))) : (f.audioSrc = d) && (y.src = d, 1 == r && 1 == u && (f.audioplay_flag = 2, 
                                y.autoplay = !0, y.onPlay(function() {
                                    y.onTimeUpdate(function(t) {
                                        var a = y.duration, e = parseInt(a / 60);
                                        e < 10 && (e = "0" + e);
                                        var i = parseInt(a - 60 * e);
                                        i < 10 && (i = "0" + i);
                                        var s = y.currentTime, n = parseInt(s / 60);
                                        n < 10 && (n = "0" + n);
                                        var o = parseInt(s - 60 * n);
                                        o < 10 && (o = "0" + o), f.duration = 100 * y.duration.toFixed(2), f.curTimeVal = 100 * y.currentTime.toFixed(2), 
                                        f.durationDay = e + ":" + i, f.curTimeValDay = n + ":" + o;
                                    }), y.onEnded(function() {
                                        f.setStopState(f);
                                    });
                                }))), 1 == c && (y.loop = !0), f.id = t.data.data.id, f.title = t.data.data.title, 
                                f.time = e, f.video_url = t.data.data.video, f.videopay = t.data.data.videopay, 
                                f.autoplay = s, f.poster = i, f.price = t.data.data.price, f.block = t.data.data.btn.pic_page_btn, 
                                f.pmarb = t.data.data.pmarb, f.datas = t.data.data, f.formdescs = t.data.data.formdescs, 
                                f.forminfo = t.data.data.forms, f.commSelf = t.data.data.comment, f.art_price = t.data.data.music_art_info.art_price, 
                                f.music_price = t.data.data.music_art_info.music_price, f.loopPlay = c, f.music = d, 
                                f.musicTitle = t.data.data.music_art_info.musicTitle, f.musicpay = r, f.artpay = t.data.data.artpay, 
                                f.art_type = t.data.data.art_type, f.musictype = l, f.descs = t.data.data.desc, 
                                g.setNavigationBarTitle({
                                    title: f.title
                                }), g.setStorageSync("isShowLoading", !1), g.hideNavigationBarLoading(), g.stopPullDownRefresh(), 
                                setTimeout(function() {
                                    if (1 == f.baseinfo.commA && 0 != f.commSelf || 1 == f.commSelf) {
                                        f.commShow = 1;
                                        var t = f.baseinfo.commAs;
                                        g.request({
                                            url: f.$baseurl + "getComment",
                                            cachetime: "0",
                                            data: {
                                                id: h,
                                                uniacid: f.$uniacid,
                                                comms: t,
                                                source: g.getStorageSync("source"),
                                                suid: g.getStorageSync("suid")
                                            },
                                            success: function(t) {
                                                "" != t.data.data && (f.comments = t.data.data, f.comments_num = t.data.data.length, 
                                                f.is_comment = 1);
                                            }
                                        });
                                    }
                                }, 500), f._givepscore(f, h, "showArt", f.userid, m);
                            }
                        });
                    },
                    shareClo: function() {
                        this.shareShow = 0;
                    },
                    sendMail_form: function(a, t) {
                        g.request({
                            url: this.$baseurl + "doPagesendMail_form",
                            data: {
                                id: a,
                                cid: t,
                                uniacid: this.$uniacid
                            },
                            success: function(t) {
                                g.redirectTo({
                                    url: "/pages/showArt/showArt?id=" + a
                                });
                            },
                            fail: function(t) {
                                console.log("sendMail_order_fail");
                            }
                        });
                    },
                    onPreviewImage: function(t) {
                        g.showImage(t);
                    },
                    ffgk: function(t) {
                        if (g.getStorageSync("suid"), !this.getSuid()) return !1;
                        var a = this, e = a.id, i = a.mymoney, s = t.currentTarget.dataset.pay, n = t.currentTarget.dataset.type;
                        a.artType = n;
                        var o = g.getStorageSync("openid");
                        if (1 * s <= 1 * i) g.showModal({
                            title: "提醒",
                            content: "您的余额为" + i + "元，本次将扣除" + s + "元",
                            success: function(t) {
                                t.confirm && g.request({
                                    url: a.$baseurl + "doPagevideozhifu",
                                    data: {
                                        uniacid: a.$uniacid,
                                        suid: g.getStorageSync("suid"),
                                        openid: o,
                                        mykoumoney: s,
                                        types: 1,
                                        id: e,
                                        artType: n
                                    },
                                    header: {
                                        "content-type": "application/json"
                                    },
                                    success: function(t) {
                                        a.getPoductDetail(e);
                                    }
                                });
                            }
                        }); else {
                            var d = s - i;
                            a.paymoney = d, "微信", 0 == i ? g.showModal({
                                title: "提醒",
                                content: "您将微信支付" + d + "元",
                                success: function(t) {
                                    t.confirm && a.zhifu(d, i, e, n);
                                }
                            }) : g.showModal({
                                title: "提醒",
                                content: "您将余额支付" + i + "元，微信支付" + d + "元",
                                success: function(t) {
                                    t.confirm && a.zhifu(d, i, e, n);
                                }
                            });
                        }
                    },
                    zhifu: function(e, i, s, n) {
                        var o = this, t = g.getStorageSync("openid");
                        g.request({
                            url: o.$baseurl + "doPagevideozhifu",
                            data: {
                                uniacid: o.$uniacid,
                                openid: t,
                                suid: g.getStorageSync("suid"),
                                money: e,
                                mykoumoney: i,
                                types: 2,
                                id: s,
                                pay_type: "weixin",
                                artType: o.artType
                            },
                            header: {
                                "content-type": "application/json"
                            },
                            success: function(t) {
                                var a = t.data.data.order_id;
                                "success" == t.data.message && g.requestPayment({
                                    timeStamp: t.data.data.timeStamp,
                                    nonceStr: t.data.data.nonceStr,
                                    package: t.data.data.package,
                                    signType: "MD5",
                                    paySign: t.data.data.paySign,
                                    success: function(t) {
                                        g.showToast({
                                            title: "支付成功",
                                            icon: "success",
                                            duration: 3e3,
                                            success: function(t) {
                                                o.dosetmoney(e, i, s, a, n);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    },
                    dosetmoney: function(t, a, e, i, s) {
                        var n = this, o = g.getStorageSync("openid");
                        e = n.id, g.request({
                            url: n.$baseurl + "doPagevideogeng",
                            data: {
                                uniacid: n.$uniacid,
                                openid: o,
                                suid: g.getStorageSync("suid"),
                                money: t,
                                mykoumoney: a,
                                orderid: i,
                                id: e,
                                artType: s
                            },
                            header: {
                                "content-type": "application/json"
                            },
                            success: function(t) {
                                n.getPoductDetail(e);
                            }
                        });
                    },
                    cell: function() {
                        this.needAuth = !1;
                    },
                    closeAuth: function() {
                        this.needAuth = !1, this.needBind = !0;
                    },
                    closeBind: function() {
                        this.needBind = !1, this.getArt();
                    },
                    bindInputChange: function(t) {
                        var a = t.detail.value, e = t.currentTarget.dataset.index, i = this.forminfo;
                        i[e].val = a, this.forminfo = i;
                    },
                    weixinadd: function() {
                        var o = this;
                        g.chooseAddress({
                            success: function(t) {
                                for (var a = t.provinceName + " " + t.cityName + " " + t.countyName + " " + t.detailInfo, e = t.userName, i = t.telNumber, s = o.forminfo, n = 0; n < s.length; n++) 0 == s[n].type && 2 == s[n].tp_text[0].yval && (s[n].val = e), 
                                0 == s[n].type && 3 == s[n].tp_text[0].yval && (s[n].val = i), 0 == s[n].type && 4 == s[n].tp_text[0].yval && (s[n].val = a);
                                o.myname = e, o.mymobile = i, o.myaddress = a, o.forminfo = s;
                            }
                        });
                    },
                    getPhoneNumber: function(t) {
                        var i = this, a = t.detail.iv, e = t.detail.encryptedData;
                        "getPhoneNumber:ok" == t.detail.errMsg ? g.checkSession({
                            success: function() {
                                g.request({
                                    url: i.$baseurl + "doPagejiemiNew",
                                    data: {
                                        uniacid: i.$uniacid,
                                        newSessionKey: i.newSessionKey,
                                        iv: a,
                                        encryptedData: e
                                    },
                                    success: function(t) {
                                        if (t.data.data) {
                                            for (var a = i.forminfo, e = 0; e < a.length; e++) 0 == a[e].type && 5 == a[e].tp_text[0] && (a[e].val = t.data.data);
                                            i.wxmobile = t.data.data, i.forminfo = a;
                                        } else g.showModal({
                                            title: "提示",
                                            content: "sessionKey已过期，请下拉刷新！"
                                        });
                                    },
                                    fail: function(t) {
                                        console.log(t);
                                    }
                                });
                            },
                            fail: function() {
                                g.showModal({
                                    title: "提示",
                                    content: "sessionKey已过期，请下拉刷新！"
                                });
                            }
                        }) : g.showModal({
                            title: "提示",
                            content: "请先授权获取您的手机号！",
                            showCancel: !1
                        });
                    },
                    checkboxChange: function(t) {
                        var a = t.detail.value, e = t.currentTarget.dataset.index, i = this.forminfo;
                        i[e].val = a, this.forminfo = i;
                    },
                    radioChange: function(t) {
                        var a = t.detail.value, e = t.currentTarget.dataset.index, i = this.forminfo;
                        i[e].val = a, this.forminfo = i;
                    },
                    delimg: function(t) {
                        var a = t.currentTarget.dataset.index, e = t.currentTarget.dataset.id, i = this.forminfo, s = i[a].z_val;
                        s.splice(e, 1), 0 == s.length && (s = ""), i[a].z_val = s, this.forminfo = i;
                    },
                    choiceimg: function(t) {
                        var n = this, a = 0, o = n.zhixin, d = t.currentTarget.dataset.index, r = n.forminfo, e = r[d].val, i = r[d].tp_text[0];
                        e ? a = e.length : (a = 0, e = []);
                        var s = i - a;
                        n.pd_val, g.chooseImage({
                            count: s,
                            sizeType: [ "original", "compressed" ],
                            sourceType: [ "album", "camera" ],
                            success: function(t) {
                                o = !0, n.zhixin = o, g.showLoading({
                                    title: "图片上传中"
                                });
                                var e = t.tempFilePaths, i = 0, s = e.length;
                                !function a() {
                                    g.uploadFile({
                                        url: n.$baseurl + "wxupimg",
                                        formData: {
                                            uniacid: n.$uniacid
                                        },
                                        filePath: e[i],
                                        name: "file",
                                        success: function(t) {
                                            r[d].z_val.push(t.data), n.forminfo = r, ++i < s ? a() : (o = !1, n.zhixin = o, 
                                            g.hideLoading());
                                        }
                                    });
                                }();
                            }
                        });
                    },
                    bindPickerChange: function(t) {
                        var a = t.detail.value, e = t.currentTarget.dataset.index, i = this.forminfo, s = i[e].tp_text[a];
                        i[e].val = s, this.forminfo = i;
                    },
                    bindDateChange: function(t) {
                        var a = t.detail.value, e = t.currentTarget.dataset.index, i = this.forminfo;
                        i[e].val = a, this.forminfo = i;
                    },
                    bindTimeChange: function(t) {
                        var a = t.detail.value, e = t.currentTarget.dataset.index, i = this.forminfo;
                        i[e].val = a, this.forminfo = i;
                    },
                    namexz: function(t) {
                        for (var a = this, e = t.currentTarget.dataset.index, i = a.forminfo[e], s = [], n = 0; n < i.tp_text.length; n++) {
                            var o = {};
                            o.keys = i.tp_text[n], o.val = 1, s.push(o);
                        }
                        a.ttcxs = 1, a.formindex = e, a.xx = s, a.xuanz = 0, a.lixuanz = -1, a.riqi();
                    },
                    riqi: function() {
                        for (var t = this, a = new Date(), e = new Date(a.getTime()), i = e.getFullYear() + "-" + (e.getMonth() + 1) + "-" + e.getDate(), s = t.xx, n = 0; n < s.length; n++) s[n].val = 1;
                        t.xx = s, t.gettoday(i);
                        for (var o = [], d = [], r = new Date(), u = 0; u < 5; u++) {
                            var c = new Date(r.getTime() + 24 * u * 3600 * 1e3), l = c.getFullYear(), h = c.getMonth() + 1, f = c.getDate(), m = h + "月" + f + "日", g = l + "-" + h + "-" + f;
                            o.push(m), d.push(g);
                        }
                        t.arrs = o, t.fallarrs = d, t.today = i;
                    },
                    gettoday: function(t) {
                        var s = this, a = s.id, e = s.formindex, n = s.xx;
                        g.request({
                            url: s.$baseurl + "dopageDuzhan",
                            data: {
                                uniacid: s.$uniacid,
                                id: a,
                                types: "showArt",
                                days: t,
                                pagedatekey: e
                            },
                            success: function(t) {
                                for (var a = t.data.data, e = 0; e < a.length; e++) n[a[e]].val = 2;
                                var i = 0;
                                a.length == n.length && (i = 1), s.xx = n, s.isover = i;
                            }
                        });
                    },
                    goux: function(t) {
                        this.lixuanz = t.currentTarget.dataset.index;
                    },
                    xuanzd: function(t) {
                        for (var a = t.currentTarget.dataset.index, e = this.fallarrs[a], i = this.xx, s = 0; s < i.length; s++) i[s].val = 1;
                        this.xuanz = a, this.today = e, this.lixuanz = -1, this.xx = i, this.gettoday(e);
                    },
                    quxiao: function() {
                        this.ttcxs = 0;
                    },
                    save: function() {
                        var t = this.today, a = this.xx, e = this.lixuanz;
                        if (-1 == e) return g.showModal({
                            title: "提现",
                            content: "请选择预约的选项",
                            showCancel: !1
                        }), !1;
                        var i = "已选择" + t + "，" + a[e].keys, s = this.forminfo, n = this.formindex;
                        s[n].val = i, s[n].days = t, s[n].indexkey = n, s[n].xuanx = e, this.ttcxs = 0, 
                        this.forminfo = s;
                    },
                    formSubmit: function(t) {
                        var e = this, a = g.getStorageSync("suid");
                        if (!this.getSuid()) return !1;
                        for (var i = e.forminfo, s = 0; s < i.length; s++) {
                            if (1 == i[s].ismust) if (5 == i[s].type) {
                                if (!i[s].z_val) return g.showModal({
                                    title: "提醒",
                                    content: i[s].name + "为必填项！",
                                    showCancel: !1
                                }), !1;
                            } else {
                                if ("" == i[s].val) return g.showModal({
                                    title: "提醒",
                                    content: i[s].name + "为必填项！",
                                    showCancel: !1
                                }), !1;
                                if (0 == i[s].type && 1 == i[s].tp_text[0]) {
                                    if (!/^1[3456789]{1}\d{9}$/.test(i[s].val)) return g.showModal({
                                        title: "提醒",
                                        content: "请您输入正确的手机号码",
                                        showCancel: !1
                                    }), !1;
                                } else if (0 == i[s].type && 7 == i[s].tp_text[0]) {
                                    if (!/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test(i[s].val)) return g.showModal({
                                        title: "提醒",
                                        content: "请您输入正确的身份证号",
                                        showCancel: !1
                                    }), !1;
                                }
                            }
                            if (5 == i[s].type && i[s].z_val && 0 < i[s].z_val.length) for (var n = 0; n < i[s].z_val.length; n++) i[s].z_val[n] = i[s].z_val[n].substr(i[s].z_val[n].indexOf("/upimages"));
                        }
                        var o = g.getStorageSync("openid");
                        g.request({
                            url: e.$baseurl + "doPageFormval",
                            data: {
                                uniacid: e.$uniacid,
                                suid: a,
                                id: e.id,
                                pagedata: JSON.stringify(i),
                                fid: e.datas.formset,
                                types: "showArt",
                                openid: o,
                                source: g.getStorageSync("source"),
                                form_id: t.detail.formId ? t.detail.formId : ""
                            },
                            success: function(t) {
                                var a = t.data.data.id;
                                g.showModal({
                                    title: "提示",
                                    content: t.data.data.con,
                                    showCancel: !1,
                                    success: function() {
                                        e.sendMail_form(e.id, a);
                                    }
                                });
                            }
                        });
                    },
                    changealipay: function() {
                        this.pay_type = 1;
                    },
                    changewxpay: function() {
                        this.pay_type = 2;
                    },
                    close_pay_type: function() {
                        this.show_pay_type = 0, g.navigateTo({
                            url: "/pagesReserve/orderList/orderList"
                        });
                    },
                    h5topay: function() {
                        this.pay_type, this.zhifu(this.paymoney, this.mymoney, this.id, this.artType), this.show_pay_type = 0;
                    },
                    pay3: function(a) {
                        var t;
                        g.request({
                            url: this.$baseurl + "doPageHfivepay",
                            data: (t = {
                                uniacid: this.$uniacid,
                                order_id: a,
                                suid: g.getStorageSync("suid"),
                                types: 11
                            }, e(t, "suid", g.getStorageSync("suid")), e(t, "money", this.paymoney), t),
                            success: function(t) {
                                1 == t.data.flag && g.navigateTo({
                                    url: "/pages/aliH5pay/aliH5pay?id=" + a + "&type=11"
                                });
                            }
                        });
                    },
                    pay5: function(a) {
                        g.request({
                            url: this.$baseurl + "doPageHfiveWxpay",
                            data: {
                                uniacid: this.$uniacid,
                                order_id: a,
                                types: "art",
                                suid: g.getStorageSync("suid"),
                                money: this.paymoney
                            },
                            success: function(t) {
                                t.data && (g.setStorageSync(a, t.data.mweb_url), g.navigateTo({
                                    url: "/pages/wxH5pay/wxH5pay?order_id=" + a
                                }));
                            }
                        });
                    },
                    showpay: function() {
                        this.money = this.paymoney;
                        var a = this;
                        g.request({
                            url: this.$baseurl + "doPageGetH5payshow",
                            data: {
                                uniacid: this.$uniacid,
                                suid: g.getStorageSync("suid")
                            },
                            success: function(t) {
                                0 == t.data.data.ali && 0 == t.data.data.wx ? g.showModal({
                                    title: "提示",
                                    content: "请联系管理员设置支付参数",
                                    showCancel: !1,
                                    success: function(t) {
                                        return !1;
                                    }
                                }) : (0 == t.data.data.ali ? (a.alipay = 0, a.pay_type = 2) : (a.alipay = 1, a.pay_type = 1), 
                                0 == t.data.data.wx ? a.wxpay = 0 : a.wxpay = 1, a.show_pay_type = 1);
                            }
                        });
                    },
                    open_share: function() {
                        this.share = 1;
                    },
                    h5ShareAppMessage: function() {
                        var a = this, t = g.getStorageSync("suid");
                        g.showModal({
                            title: "长按复制链接后分享",
                            content: this.$host + "/h5/index.html?id=" + this.$uniacid + "#/pages/showArt/showArt?id=" + this.id + "&fxsid=" + t + "&userid=" + t,
                            showCancel: !1,
                            success: function(t) {
                                a.share = 0;
                            }
                        });
                    },
                    getShareImg: function() {
                        g.showLoading({
                            title: "海报生成中"
                        });
                        var a = this;
                        g.request({
                            url: a.$baseurl + "dopageshareewm",
                            data: {
                                uniacid: a.$uniacid,
                                suid: g.getStorageSync("suid"),
                                gid: a.id,
                                types: "art",
                                source: g.getStorageSync("source"),
                                pageUrl: "showArt"
                            },
                            success: function(t) {
                                g.hideLoading(), 0 == t.data.data.error ? (a.shareimg = 1, a.shareimg_url = t.data.data.url) : g.showToast({
                                    title: t.data.data.msg,
                                    icon: "none"
                                });
                            }
                        });
                    },
                    share_close: function() {
                        this.share = 0, this.shareimg = 0;
                    },
                    closeShare: function() {
                        this.shareimg = 0;
                    },
                    saveImg: function() {
                        var a = this;
                        g.getImageInfo({
                            src: a.shareimg_url,
                            success: function(t) {
                                g.saveImageToPhotosAlbum({
                                    filePath: t.path,
                                    success: function() {
                                        g.showToast({
                                            title: "保存成功！",
                                            icon: "none"
                                        }), a.shareimg = 0, a.share = 0;
                                    }
                                });
                            }
                        });
                    },
                    aliSaveImg: function() {
                        var a = this;
                        g.getImageInfo({
                            src: a.shareimg_url,
                            success: function(t) {
                                my.saveImage({
                                    url: t.path,
                                    showActionSheet: !0,
                                    success: function() {
                                        my.alert({
                                            title: "保存成功"
                                        }), a.shareimg = 0, a.share = 0;
                                    }
                                });
                            }
                        });
                    },
                    getSuid: function() {
                        if (g.getStorageSync("suid")) return !0;
                        return g.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0, 
                        !1;
                    },
                    getlikes: function() {
                        var a = this, t = this.is_like, e = g.getStorageSync("suid");
                        0 == t ? g.request({
                            url: this.$host + "/api/MainWxapp/doPageLikes",
                            data: {
                                uniacid: this.$uniacid,
                                suid: e,
                                id: this.id,
                                type: "showArt"
                            },
                            success: function(t) {
                                "收藏成功" == t.data.data.msg && (a.is_like = 1, a.likes = a.likes + 1, g.showToast({
                                    title: "点赞成功",
                                    icon: "success",
                                    duration: 1e3
                                }));
                            }
                        }) : 1 == t && g.request({
                            url: this.$host + "/api/MainWxapp/doPageLikes",
                            data: {
                                uniacid: this.$uniacid,
                                suid: e,
                                id: this.id,
                                type: "showArt"
                            },
                            success: function(t) {
                                "取消收藏成功" == t.data.data.msg && (a.is_like = 0, a.likes = a.likes - 1, g.showToast({
                                    title: "取消赞成功",
                                    icon: "success",
                                    duration: 1e3
                                }));
                            }
                        });
                    },
                    collect: function(t) {
                        if (this.getSuid()) {
                            var e = this, a = e.scollect, i = e.id, s = g.getStorageSync("suid");
                            0 == a ? (g.showLoading({
                                title: "收藏中"
                            }), g.request({
                                url: e.$baseurl + "doPageCollect",
                                data: {
                                    uniacid: e.$uniacid,
                                    suid: s,
                                    types: "showArt",
                                    id: i
                                },
                                success: function(t) {
                                    var a = t.data.data;
                                    "收藏成功" == a ? (e.scollect = 1, e.collects = e.collects + 1) : e.scollect = 0, g.showToast({
                                        title: a,
                                        icon: "succes",
                                        duration: 1e3,
                                        mask: !0
                                    });
                                }
                            })) : 1 == a && (g.showLoading({
                                title: "取消收藏中"
                            }), g.request({
                                url: e.$baseurl + "doPageCollect",
                                data: {
                                    uniacid: e.$uniacid,
                                    suid: s,
                                    types: "showArt",
                                    id: i
                                },
                                success: function(t) {
                                    var a = t.data.data;
                                    "取消收藏成功" == a ? (e.scollect = 0, e.collects = e.collects - 1) : e.scollect = 1, 
                                    g.showToast({
                                        title: a,
                                        icon: "succes",
                                        duration: 1e3,
                                        mask: !0
                                    });
                                }
                            })), setTimeout(function() {
                                g.hideLoading();
                            }, 1e3);
                        }
                    }
                }
            });
            i.default = a;
        }).call(this, s("543d").default);
    },
    c7c0: function(t, a, e) {
        var i = e("bf53");
        e.n(i).a;
    }
}, [ [ "69a0", "common/runtime", "common/vendor" ] ] ]);