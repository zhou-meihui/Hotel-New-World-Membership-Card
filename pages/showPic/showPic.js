(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/showPic/showPic"], {
        1219: function (t, e, i) {
            var s = i("920b");
            i.n(s).a;
        },
        "32c5": function (t, e, i) {
            var s = function () {
                    this.$createElement;
                    this._self._c;
                },
                a = [];
            i.d(e, "a", function () {
                return s;
            }), i.d(e, "b", function () {
                return a;
            });
        },
        "569a": function (t, e, i) {
            (function (a) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var n = i("f571"),
                    t = (getApp(), {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                picList: [],
                                piclist_num: 0,
                                thumb: "",
                                shareShow: 0,
                                shareScore: 0,
                                shareNotice: 0,
                                fxsid: 0,
                                shareHome: 0,
                                datas: "",
                                baseinfo: {},
                                sharesuid: "",
                                get_share_gz: 2,
                                share: 0,
                                id: 0,
                                shareimg: 0,
                                shareimg_url: "",
                                system_w: 0,
                                system_h: 0,
                                img_w: 0,
                                img_h: 0,
                                title: ""
                            };
                        },
                        onLoad: function (t) {
                            var e = this;
                            this._baseMin(this);
                            var i = 0;
                            t.fxsid && (i = t.fxsid), this.fxsid = i, this.id = t.id, t.userid && (this.sharesuid = t.userid);
                            var s = a.getStorageSync("systemInfo");
                            this.img_w = parseInt((.65 * s.windowWidth).toFixed(0)), this.img_h = parseInt((1.875 * this.img_w).toFixed(0)),
                                this.system_w = parseInt(s.windowWidth), this.system_h = parseInt(s.windowHeight),
                                n.getOpenid(this.fxsid, function () {
                                    e.getPic();
                                });
                        },
                        onShareAppMessage: function () {
                            var t, e = a.getStorageSync("suid");
                            return t = "/pages/showPic/showPic?id=" + this.id + "&fxsid=" + e + "&userid=" + e, {
                                title: this.title,
                                path: t,
                                success: function (t) {}
                            };
                        },
                        onPullDownRefresh: function () {
                            var t = this.id;
                            this.getShowPic(t), a.stopPullDownRefresh();
                        },
                        methods: {
                            navback: function () {
                                a.navigateBack();
                            },
                            getPic: function () {
                                this.getShowPic(this.id);
                            },
                            redirectto: function (t) {
                                var e = t.currentTarget.dataset.link,
                                    i = t.currentTarget.dataset.linktype;
                                this._redirectto(e, i);
                            },
                            getShowPic: function (t) {
                                var e = this;
                                a.request({
                                    url: e.$baseurl + "dopageshowPic",
                                    data: {
                                        id: t,
                                        uniacid: e.$uniacid
                                    },
                                    cachetime: "30",
                                    success: function (t) {
                                        e.picList = t.data.data.text, e.piclist_num = t.data.data.text.length, e.thumb = t.data.data.thumb,
                                            e.title = t.data.data.title, e.desc = t.data.data.desc, e.get_share_gz = t.data.data.get_share_gz,
                                            a.setNavigationBarTitle({
                                                title: e.title
                                            }), a.setStorageSync("isShowLoading", !1), a.hideNavigationBarLoading(), a.stopPullDownRefresh(),
                                            e.givepscore();
                                    }
                                });
                            },
                            shareClo: function () {
                                this.shareShow = 0;
                            },
                            share111: function () {
                                this.share = 1;
                            },
                            share_close: function () {
                                this.share = 0;
                            },
                            h5ShareAppMessage: function () {
                                var e = this,
                                    t = a.getStorageSync("suid");
                                a.showModal({
                                    title: "长按复制链接后分享",
                                    content: this.$host + "/h5/index.html?id=" + this.$uniacid + "#/pages/showPic/showPic?id=" + this.id + "&fxsid=" + t + "&userid=" + t,
                                    showCancel: !1,
                                    success: function (t) {
                                        e.share = 0;
                                    }
                                });
                            },
                            getShareImg: function () {
                                a.showLoading({
                                    title: "海报生成中"
                                });
                                var e = this;
                                a.request({
                                    url: e.$baseurl + "dopageshareewm",
                                    data: {
                                        uniacid: e.$uniacid,
                                        suid: a.getStorageSync("suid"),
                                        gid: e.id,
                                        types: "pic",
                                        source: a.getStorageSync("source"),
                                        pageUrl: "showPic"
                                    },
                                    success: function (t) {
                                        a.hideLoading(), 0 == t.data.data.error ? (e.shareimg = 1, e.shareimg_url = t.data.data.url) : a.showToast({
                                            title: t.data.data.msg,
                                            icon: "none"
                                        });
                                    }
                                });
                            },
                            closeShare: function () {
                                this.shareimg = 0;
                            },
                            saveImg: function () {
                                var e = this;
                                a.getImageInfo({
                                    src: e.shareimg_url,
                                    success: function (t) {
                                        a.saveImageToPhotosAlbum({
                                            filePath: t.path,
                                            success: function () {
                                                a.showToast({
                                                    title: "保存成功！",
                                                    icon: "none"
                                                }), e.shareimg = 0, e.share = 0;
                                            }
                                        });
                                    }
                                });
                            },
                            aliSaveImg: function () {
                                var e = this;
                                a.getImageInfo({
                                    src: e.shareimg_url,
                                    success: function (t) {
                                        my.saveImage({
                                            url: t.path,
                                            showActionSheet: !0,
                                            success: function () {
                                                my.alert({
                                                    title: "保存成功"
                                                }), e.shareimg = 0, e.share = 0;
                                            }
                                        });
                                    }
                                });
                            },
                            givepscore: function () {
                                var t = this.id,
                                    e = this.sharesuid,
                                    i = a.getStorageSync("suid");
                                e != i && 0 != e && "" != e && null != e && a.request({
                                    url: this.$baseurl + "doPagegiveposcore",
                                    data: {
                                        id: t,
                                        types: "showPic",
                                        suid: i,
                                        fxsid: e,
                                        uniacid: this.$uniacid,
                                        source: a.getStorageSync("source")
                                    },
                                    success: function (t) {}
                                });
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this._checkBindPhone(this);
                            },
                            closeBind: function () {
                                this.needBind = !1, this.getPic();
                            }
                        }
                    });
                e.default = t;
            }).call(this, i("543d").default);
        },
        "8a09": function (t, e, i) {
            i.r(e);
            var s = i("32c5"),
                a = i("ebb2");
            for (var n in a) "default" !== n && function (t) {
                i.d(e, t, function () {
                    return a[t];
                });
            }(n);
            i("1219");
            var o = i("2877"),
                c = Object(o.a)(a.default, s.a, s.b, !1, null, null, null);
            e.default = c.exports;
        },
        "920b": function (t, e, i) {},
        e35a: function (t, e, i) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                i("020c"), i("921b"), e(i("66fd")), t(e(i("8a09")).default);
            }).call(this, i("543d").createPage);
        },
        ebb2: function (t, e, i) {
            i.r(e);
            var s = i("569a"),
                a = i.n(s);
            for (var n in s) "default" !== n && function (t) {
                i.d(e, t, function () {
                    return s[t];
                });
            }(n);
            e.default = a.a;
        }
    },
    [
        ["e35a", "common/runtime", "common/vendor"]
    ]
]);