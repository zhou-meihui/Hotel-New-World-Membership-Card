(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/showPiclist/showPiclist"], {
        "280c": function (t, i, e) {},
        "3cb1": function (t, i, e) {
            var n = function () {
                    this.$createElement;
                    this._self._c;
                },
                a = [];
            e.d(i, "a", function () {
                return n;
            }), e.d(i, "b", function () {
                return a;
            });
        },
        "3d11": function (t, i, e) {
            (function (a) {
                Object.defineProperty(i, "__esModule", {
                    value: !0
                }), i.default = void 0;
                var r = e("f571"),
                    t = (getApp(), {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                baseinfo: {},
                                picList: [],
                                piclist_num: 0,
                                fxsid: 0,
                                id: 0,
                                title: "",
                                currentSwiper: 0,
                                system_h: 0,
                                key: 0
                            };
                        },
                        onLoad: function (t) {
                            var i = this;
                            this._baseMin(this);
                            var e = 0;
                            t.fxsid && (e = t.fxsid), this.fxsid = e, this.id = t.id, this.key = t.key, this.currentSwiper = parseInt(this.key),
                                r.getOpenid(this.fxsid, function () {
                                    i.getPic();
                                });
                            var n = a.getStorageSync("systemInfo");
                            this.system_h = parseInt(n.windowHeight);
                        },
                        onPullDownRefresh: function () {
                            var t = this.id;
                            this.getShowPic(t), a.stopPullDownRefresh();
                        },
                        methods: {
                            swiperChange: function (t) {
                                this.currentSwiper = t.detail.current;
                            },
                            getPic: function () {
                                this.getShowPic(this.id);
                            },
                            redirectto: function (t) {
                                var i = t.currentTarget.dataset.link,
                                    e = t.currentTarget.dataset.linktype;
                                this._redirectto(i, e);
                            },
                            getShowPic: function (t) {
                                var i = this;
                                a.request({
                                    url: i.$baseurl + "dopageshowPic",
                                    data: {
                                        id: t,
                                        uniacid: i.$uniacid
                                    },
                                    cachetime: "30",
                                    success: function (t) {
                                        i.picList = t.data.data.text, i.piclist_num = t.data.data.text.length, i.title = t.data.data.title,
                                            a.setNavigationBarTitle({
                                                title: i.title
                                            }), a.setStorageSync("isShowLoading", !1), a.hideNavigationBarLoading(), a.stopPullDownRefresh();
                                    }
                                });
                            }
                        }
                    });
                i.default = t;
            }).call(this, e("543d").default);
        },
        a111: function (t, i, e) {
            e.r(i);
            var n = e("3d11"),
                a = e.n(n);
            for (var r in n) "default" !== r && function (t) {
                e.d(i, t, function () {
                    return n[t];
                });
            }(r);
            i.default = a.a;
        },
        a313: function (t, i, e) {
            (function (t) {
                function i(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                e("020c"), e("921b"), i(e("66fd")), t(i(e("b855")).default);
            }).call(this, e("543d").createPage);
        },
        b855: function (t, i, e) {
            e.r(i);
            var n = e("3cb1"),
                a = e("a111");
            for (var r in a) "default" !== r && function (t) {
                e.d(i, t, function () {
                    return a[t];
                });
            }(r);
            e("d298");
            var s = e("2877"),
                c = Object(s.a)(a.default, n.a, n.b, !1, null, null, null);
            i.default = c.exports;
        },
        d298: function (t, i, e) {
            var n = e("280c");
            e.n(n).a;
        }
    },
    [
        ["a313", "common/runtime", "common/vendor"]
    ]
]);