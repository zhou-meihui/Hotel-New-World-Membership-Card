(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/showProMore/showProMore"], {
        "271a": function (t, e, i) {},
        "2a2a": function (t, e, i) {
            var s = function () {
                    this.$createElement;
                    this._self._c;
                },
                o = [];
            i.d(e, "a", function () {
                return s;
            }), i.d(e, "b", function () {
                return o;
            });
        },
        3488: function (t, e, i) {
            var s = i("271a");
            i.n(s).a;
        },
        5171: function (t, e, i) {
            i.r(e);
            var s = i("fc41"),
                o = i.n(s);
            for (var a in s) "default" !== a && function (t) {
                i.d(e, t, function () {
                    return s[t];
                });
            }(a);
            e.default = o.a;
        },
        "55f1": function (t, e, i) {
            i.r(e);
            var s = i("2a2a"),
                o = i("5171");
            for (var a in o) "default" !== a && function (t) {
                i.d(e, t, function () {
                    return o[t];
                });
            }(a);
            i("3488");
            var n = i("2877"),
                r = Object(n.a)(o.default, s.a, s.b, !1, null, null, null);
            e.default = r.exports;
        },
        "978b": function (t, e, i) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                i("020c"), i("921b"), e(i("66fd")), t(e(i("55f1")).default);
            }).call(this, i("543d").createPage);
        },
        fc41: function (t, i, s) {
            (function (m) {
                Object.defineProperty(i, "__esModule", {
                    value: !0
                }), i.default = void 0;
                var t, v = (t = s("3584")) && t.__esModule ? t : {
                    default: t
                };
                var o = s("f571"),
                    e = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                $host: this.$host,
                                gid: "",
                                imgUrls: [],
                                num: 1,
                                indicatorDots: !0,
                                autoplay: !0,
                                interval: 3e3,
                                duration: 1e3,
                                circular: !0,
                                sc: 0,
                                guige: 0,
                                protab: 1,
                                nowcon: "con",
                                is_comment: 0,
                                comments: 2,
                                share: 0,
                                xzarr: [],
                                gwccount: 0,
                                products: [{
                                    xsl: 0
                                }],
                                fxsid: 0,
                                heighthave: 0,
                                minHeight: 220,
                                shareHome: 0,
                                vip_config: 0,
                                base_color2: "",
                                proinfo: "",
                                gzjson: "",
                                datas: "",
                                baseinfo: [],
                                gwc: "",
                                gm: "",
                                baseproinfo: "",
                                superuser: "",
                                userid: "",
                                pic_video: "",
                                isplay: !1,
                                currentSwiper: 0,
                                discounts: 0,
                                shareimg: 0,
                                shareimg_url: "",
                                system_w: 0,
                                system_h: 0,
                                img_w: 0,
                                img_h: 0,
                                dlength: 0,
                                strgrouparr: "",
                                buy_type: "",
                                stores: "",
                                needAuth: !1,
                                needBind: !1,
                                newstr: "",
                                showCouponBox: 0,
                                toscrollTop: 0,
                                fiexdBoxs: 0,
                                buyuser_length: 0,
                                coupon_length: 0,
                                buy_person: [{
                                    avatar: ""
                                }, {
                                    avatar: ""
                                }, {
                                    avatar: ""
                                }, {
                                    avatar: ""
                                }, {
                                    avatar: ""
                                }, {
                                    avatar: ""
                                }],
                                vipid: ""
                            };
                        },
                        onLoad: function (t) {
                          console.log(t,'showProMore')
                            var e = this;
                            t.id && (this.gid = t.id), this._baseMin(this);
                            var i = 0;
                            t.fxsid && (i = t.fxsid, m.setStorageSync("fxsid", i)), this.fxsid = i, t.userid && (this.userid = t.userid),
                                m.getStorageSync("suid"), o.getOpenid(i, function () {
                                    e.getPro(), e._getSuperUserInfo(e);
                                });
                            var s = m.getStorageSync("systemInfo");
                            this.img_w = parseInt((.65 * s.windowWidth).toFixed(0)), this.img_h = parseInt((1.875 * this.img_w).toFixed(0)),
                                this.system_w = parseInt(s.windowWidth), this.system_h = parseInt(s.windowHeight);
                        },
                        onPageScroll: function (t) {
                            200 < t.scrollTop ? this.fiexdBoxs = 1 : this.fiexdBoxs = 0;
                        },
                        onPullDownRefresh: function () {
                            this.getPro(), m.stopPullDownRefresh();
                        },
                        onShareAppMessage: function () {
                            var t, e = m.getStorageSync("suid"),
                                i = this.products,
                                s = this.gid;
                            return t = 1 == this.superuser.fxs ? "/pages/showProMore/showProMore?id=" + s + "&userid=" + e : "/pages/showProMore/showProMore?id=" + s + "&userid=" + e + "&fxsid=" + e,
                                console.log(t), {
                                    title: i.title,
                                    path: t,
                                    imageUrl: i.shareimg
                                };
                        },
                        methods: {
                            toTop: function () {
                                m.pageScrollTo({
                                    scrollTop: 0
                                });
                            },
                            navback: function () {
                                m.navigateBack();
                            },
                            toBeVip: function () {
                                m.navigateTo({
                                    url: "/pages/equity_show/equity_show"
                                });
                            },
                            checkvip: function (u) {
                                var c = this,
                                    t = (m.getStorageSync("openid"), m.getStorageSync("suid"));
                                m.request({
                                    url: c.$baseurl + "doPagecheckvip",
                                    data: {
                                        uniacid: c.$uniacid,
                                        kwd: "duo",
                                        suid: t,
                                        id: c.gid,
                                        gz: 1
                                    },
                                    success: function (t) {
                                        if (!1 === t.data.data) return c.needvip = !0, m.showModal({
                                            title: "进入失败",
                                            content: "使用本功能需先开通vip!",
                                            showCancel: !1,
                                            success: function (t) {
                                                t.confirm && m.redirectTo({
                                                    url: "/pages/register/register"
                                                });
                                            }
                                        }), !1;
                                        if (0 < t.data.data.needgrade)
                                            if (c.needvip = !0, 0 < t.data.data.grade)
                                                if (t.data.data.grade < t.data.data.needgrade) m.showModal({
                                                    title: "进入失败",
                                                    content: "使用本功能需成为" + t.data.data.vipname + "(" + t.data.data.needgrade + "级)以上等级会员,请先升级!",
                                                    showCancel: !1,
                                                    success: function (t) {
                                                        t.confirm && m.redirectTo({
                                                            url: "/pages/open1/open1"
                                                        });
                                                    }
                                                });
                                                else {
                                                    var e = u.currentTarget.dataset.type,
                                                        i = m.getStorageSync("subscribe");
                                                    if (0 < i.length) {
                                                        var s = new Array();
                                                        "" != i[0].mid && s.push(i[0].mid), "" != i[1].mid && s.push(i[1].mid), "" != i[2].mid && s.push(i[2].mid),
                                                            0 < s.length ? m.requestSubscribeMessage({
                                                                tmplIds: s,
                                                                success: function (t) {
                                                                    "gwc" == e ? c.gwcget() : "gm" == e && c.gmget();
                                                                }
                                                            }) : "gwc" == e ? c.gwcget() : "gm" == e && c.gmget();
                                                    } else "gwc" == e ? c.gwcget() : "gm" == e && c.gmget();
                                                }
                                        else if (t.data.data.grade < t.data.data.needgrade) m.showModal({
                                            title: "进入失败",
                                            content: "使用本功能需成为" + t.data.data.vipname + "(" + t.data.data.needgrade + "级)以上等级会员,请先开通会员后再升级会员等级!",
                                            showCancel: !1,
                                            success: function (t) {
                                                t.confirm && m.redirectTo({
                                                    url: "/pages/register/register"
                                                });
                                            }
                                        });
                                        else {
                                            e = u.currentTarget.dataset.type;
                                            var o = m.getStorageSync("subscribe");
                                            if (0 < o.length) {
                                                var a = new Array();
                                                "" != o[0].mid && a.push(o[0].mid), "" != o[1].mid && a.push(o[1].mid), "" != o[2].mid && a.push(o[2].mid),
                                                    0 < a.length ? m.requestSubscribeMessage({
                                                        tmplIds: a,
                                                        success: function (t) {
                                                            "gwc" == e ? c.gwcget() : "gm" == e && c.gmget();
                                                        }
                                                    }) : "gwc" == e ? c.gwcget() : "gm" == e && c.gmget();
                                            } else "gwc" == e ? c.gwcget() : "gm" == e && c.gmget();
                                        } else {
                                            e = u.currentTarget.dataset.type;
                                            var n = m.getStorageSync("subscribe");
                                            if (0 < n.length) {
                                                var r = new Array();
                                                "" != n[0].mid && r.push(n[0].mid), "" != n[1].mid && r.push(n[1].mid), "" != n[2].mid && r.push(n[2].mid),
                                                    0 < r.length ? m.requestSubscribeMessage({
                                                        tmplIds: r,
                                                        success: function (t) {
                                                            "gwc" == e ? c.gwcget() : "gm" == e && c.gmget();
                                                        }
                                                    }) : "gwc" == e ? c.gwcget() : "gm" == e && c.gmget();
                                            } else "gwc" == e ? c.gwcget() : "gm" == e && c.gmget();
                                        }
                                    },
                                    fail: function (t) {}
                                });
                            },
                            goevaluate: function () {
                                var t = this.gid;
                                m.navigateTo({
                                    url: "/pagesOther/evaluate_list/evaluate_list?id=" + t + "&protype=mainshop"
                                });
                            },
                            getPro: function () {
                                var p = this,
                                    f = this,
                                    w = m.getStorageSync("suid"),
                                    t = m.getStorageSync("openid");
                                m.request({
                                    url: this.$host + "/api/MainWxapp/dopageduoproducts",
                                    data: {
                                        uniacid: f.$uniacid,
                                        fxsid: f.fxsid,
                                        openid: t,
                                        suid: w,
                                        id: f.gid,
                                        source: m.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        var e = t.data.data,
                                            i = t.data.data.products.grade;
                                        p.vipid = t.data.data.products.vipid;
                                        var s = t.data.data.products.discount_status,
                                            o = t.data.data.products.stores,
                                            a = 0;
                                        if (0 < i && 0 < s) {
                                            var n = t.data.data.products.discount;
                                            if (2 == s) {
                                                if (0 < n.length)
                                                    for (var r = 0; r < n.length; r++)
                                                        if (i == n[r].grade) {
                                                            a = n[r].discount;
                                                            break;
                                                        }
                                            } else a = n;
                                            e.products.discount_price = (e.products.price * a * .1).toFixed(2);
                                        }
                                        var u = e.products;
                                        f.buy_person = t.data.data.products.detail_buyuser, m.setNavigationBarTitle({
                                                title: u.title
                                            }), 1 == u.is_sale && m.showModal({
                                                title: "提示",
                                                content: "该商品已下架,请选择其他商品",
                                                showCancel: !1,
                                                success: function () {
                                                    m.redirectTo({
                                                        url: "/pages/index/index"
                                                    });
                                                }
                                            }), u.texts && (u.texts = u.texts.replace(/\<img/gi, '<img style="width:100%;height:auto;display:block" '),
                                                u.texts = (0, v.default)(u.texts)), p.vip_config = e.vip_config, p.products = u,
                                            p.discounts = a, p.stores = o, p.imgUrls = u.imgtext, p.guiz = e.guiz, f.pic_video = p.products.video,
                                            f.dlength = p.imgUrls.length, f.buyuser_length = u.detail_buyuser.length, f.coupon_length = u.detail_coupon.length;
                                        var c = e.grouparr,
                                            d = "";
                                        for (r = 0; r < c.length; r++) d += c[r] + "、";
                                        var g = d.substring(0, d.length - 1);
                                        p.strgrouparr = g, p.grouparr = c;
                                        var h = e.grouparr_val;
                                        p.gzjson = h;
                                        var l = e.shouc;
                                        p.sc = 1 == l ? 0 : 1, 1 == u.use_more ? p.getproinfo() : p.proinfo = u, w && p.gwcdata();
                                    }
                                });
                            },
                            getproinfo: function () {
                                for (var i = this, s = this, t = s.gzjson, e = s.grouparr, o = "", a = "", n = s.gid, r = 0; r < e.length; r++) {
                                    var u = t[e[r]].val,
                                        c = t[e[r]].ck;
                                    o += u[c] + "######", a += u[c] + ";";
                                }
                                var d = o.substring(0, o.length - 6);
                                a = a.substring(0, a.length - 1), m.request({
                                    url: s.$baseurl + "dopageduoproductsinfo",
                                    data: {
                                        uniacid: s.$uniacid,
                                        str: d,
                                        id: n
                                    },
                                    success: function (t) {
                                        var e = t.data.data;
                                        (e.proinfo.discount_price = 0) < i.discounts && (e.proinfo.discount_price = (e.proinfo.price * i.discounts * .1).toFixed(2) < .01 ? .01 : (e.proinfo.price * i.discounts * .1).toFixed(2)),
                                            s.proinfo = e.proinfo, s.baseproinfo = e.baseinfo, s.newstr = a, 1 == e.baseinfo.is_sale && m.showModal({
                                                title: "提示",
                                                content: "该商品已下架,请选择其他商品",
                                                showCancel: !1,
                                                success: function () {
                                                    m.redirectTo({
                                                        url: "/pages/index/index"
                                                    });
                                                }
                                            }), s._givepscore(s, n, "showProMore", s.userid, m.getStorageSync("suid"));
                                    }
                                });
                            },
                            gwcdata: function () {
                                var e = this;
                                m.getStorageSync("openid"), m.request({
                                    url: this.$baseurl + "doPagegwcdata",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: m.getStorageSync("suid")
                                    },
                                    success: function (t) {
                                        e.gwccount = t.data.data;
                                    }
                                });
                            },
                            guige_block: function () {
                                this.guige = 1, this.gwc = 1, this.gm = 0;
                            },
                            guige_hidden: function () {
                                this.guige = 0, this.num = 1, this.getPro();
                            },
                            changepro: function (t) {
                                var e = t.target.dataset.id,
                                    i = (this.grouparr, t.target.dataset.index),
                                    s = this.gzjson;
                                s[e].ck = i, this.gzjson = s, this.getproinfo();
                            },
                            num_add: function () {
                                var t = this.proinfo.kc,
                                    e = this.num;
                                t < (e += 1) && (m.showModal({
                                    title: "提醒",
                                    content: "您的购买数量超过了库存！",
                                    showCancel: !1
                                }), e--), this.num = e;
                            },
                            num_jian: function () {
                                var t = this.num;
                                1 == t ? this.num = 1 : (t -= 1, this.num = t);
                            },
                            gmget: function () {
                                if (this.getSuid()) {
                                    var t = this,
                                        e = t.proinfo,
                                        i = t.num,
                                        s = t.products,
                                        o = (t.baseproinfo, t.guiz,
                                            t.vip_config);
                                    if (1 == t.baseinfo.is_sale && m.showModal({
                                            title: "提示",
                                            content: "该商品已下架,请选择其他商品",
                                            showCancel: !1,
                                            success: function () {
                                                m.redirectTo({
                                                    url: "/pages/index/index"
                                                });
                                            }
                                        }), 1 == o) return m.showModal({
                                        title: "提醒",
                                        content: "该商品必须开通会员卡购买！",
                                        showCancel: !1,
                                        success: function () {
                                            m.navigateTo({
                                                url: "/pages/register/register?type=duo"
                                            });
                                        }
                                    }), !1;
                                    if (0 == e.kc || 0 == e.pro_kc) return m.showModal({
                                        title: "提醒",
                                        content: "您来晚了，已经卖完了！",
                                        showCancel: !1
                                    }), !1;
                                    if (1 == s.use_more) var a = t.gid + "|" + e.id + "|" + i;
                                    else a = t.gid + "|-1|" + i;
                                    var n = [];
                                    n.push(a), m.setStorage({
                                        key: "buydata",
                                        data: n
                                    }), m.navigateTo({
                                        url: "/pages/order_down/order_down"
                                    });
                                }
                            },
                            gwcget: function () {
                                if (this.getSuid()) {
                                    var e = this,
                                        t = e.proinfo,
                                        i = e.num,
                                        s = (m.getStorageSync("openid"), m.getStorageSync("suid"),
                                            e.vip_config);
                                    if (1 == e.baseinfo.is_sale && m.showModal({
                                            title: "提示",
                                            content: "该商品已下架,请选择其他商品",
                                            showCancel: !1,
                                            success: function () {
                                                m.redirectTo({
                                                    url: "/pages/index/index"
                                                });
                                            }
                                        }), 1 == s) return m.showModal({
                                        title: "提醒",
                                        content: "该商品必须开通会员卡购买！",
                                        showCancel: !1,
                                        success: function () {
                                            m.navigateTo({
                                                url: "/pages/register/register?type=duo"
                                            });
                                        }
                                    }), !1;
                                    if (0 == t.kc) return m.showModal({
                                        title: "提醒",
                                        content: "您来晚了，已经卖完了！",
                                        showCancel: !1
                                    }), !1;
                                    m.request({
                                        url: this.$host + "/api/MainWxapp/dopagegwcadd",
                                        data: {
                                            uniacid: this.$uniacid,
                                            suid: m.getStorageSync("suid"),
                                            id: t.id,
                                            pid: e.products.id,
                                            prokc: i
                                        },
                                        success: function (t) {
                                            m.showToast({
                                                title: "加入成功",
                                                icon: "success",
                                                duration: 2e3,
                                                success: function () {
                                                    e.guige_hidden(), e.gwcdata();
                                                }
                                            });
                                        }
                                    });
                                }
                            },
                            gwc_100: function () {
                                var t = this.products;
                                1 == t.is_sale && m.showModal({
                                    title: "提示",
                                    content: "该商品已下架,请选择其他商品",
                                    showCancel: !1,
                                    success: function () {
                                        m.redirectTo({
                                            url: "/pages/index/index"
                                        });
                                    }
                                }), t.use_more, this.gwc = 1, this.gm = 0, this.guige = 1;
                            },
                            gm_100: function () {
                                var t = this.products;
                                1 == t.is_sale && m.showModal({
                                    title: "提示",
                                    content: "该商品已下架,请选择其他商品",
                                    showCancel: !1,
                                    success: function () {
                                        m.redirectTo({
                                            url: "/pages/index/index"
                                        });
                                    }
                                }), t.use_more, this.gwc = 0, this.gm = 1, this.guige = 1;
                            },
                            collect: function (t) {
                                if (this.getSuid()) {
                                    var i = this,
                                        e = i.sc,
                                        s = i.gid,
                                        o = m.getStorageSync("openid"),
                                        a = m.getStorageSync("suid");
                                    1 == i.baseinfo.is_sale && m.showModal({
                                        title: "提示",
                                        content: "该商品已下架,请选择其他商品",
                                        showCancel: !1,
                                        success: function () {
                                            m.redirectTo({
                                                url: "/pages/index/index"
                                            });
                                        }
                                    }), 0 == e ? (m.showLoading({
                                        title: "收藏中"
                                    }), m.request({
                                        url: i.$baseurl + "doPageCollect",
                                        data: {
                                            uniacid: i.$uniacid,
                                            suid: a,
                                            openid: o,
                                            types: "showProMore",
                                            id: s
                                        },
                                        success: function (t) {
                                            var e = t.data.data;
                                            i.sc = "收藏成功" == e ? 1 : 0, m.showToast({
                                                title: e,
                                                icon: "succes",
                                                duration: 1e3,
                                                mask: !0
                                            });
                                        }
                                    })) : 1 == e && (m.showLoading({
                                        title: "取消收藏中"
                                    }), m.request({
                                        url: i.$baseurl + "doPageCollect",
                                        data: {
                                            uniacid: i.$uniacid,
                                            suid: a,
                                            openid: o,
                                            types: "showProMore",
                                            id: s
                                        },
                                        success: function (t) {
                                            var e = t.data.data;
                                            i.sc = "取消收藏成功" == e ? 0 : 1, m.showToast({
                                                title: e,
                                                icon: "succes",
                                                duration: 1e3,
                                                mask: !0
                                            });
                                        }
                                    })), setTimeout(function () {
                                        m.hideLoading();
                                    }, 1e3);
                                }
                            },
                            gogwc: function () {
                                this.getSuid() && m.navigateTo({
                                    url: "/pages/gwc/gwc"
                                });
                            },
                            getSuid: function () {
                                if (m.getStorageSync("suid")) return !0;
                                return m.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                    !1;
                            },
                            swiperLoad: function (s) {
                                var o = this;
                                m.getSystemInfo({
                                    success: function (t) {
                                        var e = s.detail.width / s.detail.height,
                                            i = t.windowWidth / e;
                                        o.heighthave || (o.minHeight = i, o.heighthave = 1);
                                    }
                                });
                            },
                            swiperChange: function (t) {
                                this.autoplay = !0, this.currentSwiper = t.detail.current, this.isplay = !1, this.autoplay = this.autoplay,
                                    this.endvideo();
                            },
                            playvideo: function () {
                                this.autoplay = !1, this.isplay = !0, this.autoplay = this.autoplay;
                            },
                            endvideo: function () {
                                this.autoplay = !0, this.isplay = !1, this.autoplay = this.autoplay;
                            },
                            open_share: function () {
                                this.share = 1;
                            },
                            share_close: function () {
                                this.share = 0, this.shareimg = 0;
                            },
                            h5ShareAppMessage: function () {
                                var e = this,
                                    t = m.getStorageSync("suid");
                                m.showModal({
                                    title: "长按复制链接后分享",
                                    content: this.$host + "/h5/index.html?id=" + this.$uniacid + "#/pages/showProMore/showProMore?id=" + this.gid + "&fxsid=" + t + "&userid=" + t,
                                    showCancel: !1,
                                    success: function (t) {
                                        e.share = 0;
                                    }
                                });
                            },
                            getShareImg: function () {
                                m.showLoading({
                                    title: "海报生成中"
                                });
                                var e = this;
                                m.request({
                                    url: e.$baseurl + "dopageshareewm",
                                    data: {
                                        uniacid: e.$uniacid,
                                        suid: m.getStorageSync("suid"),
                                        gid: e.gid,
                                        types: "duo",
                                        source: m.getStorageSync("source"),
                                        pageUrl: "showProMore"
                                    },
                                    success: function (t) {
                                        m.hideLoading(), 0 == t.data.data.error ? (e.shareimg = 1, e.shareimg_url = t.data.data.url) : m.showToast({
                                            title: t.data.data.msg,
                                            icon: "none"
                                        });
                                    }
                                });
                            },
                            closeShare: function () {
                                this.shareimg = 0;
                            },
                            saveImg: function () {
                                var e = this;
                                m.getImageInfo({
                                    src: e.shareimg_url,
                                    success: function (t) {
                                        m.saveImageToPhotosAlbum({
                                            filePath: t.path,
                                            success: function () {
                                                m.showToast({
                                                    title: "保存成功！",
                                                    icon: "none"
                                                }), e.shareimg = 0, e.share = 0;
                                            }
                                        });
                                    }
                                });
                            },
                            aliSaveImg: function () {
                                var e = this;
                                m.getImageInfo({
                                    src: e.shareimg_url,
                                    success: function (t) {
                                        my.saveImage({
                                            url: t.path,
                                            showActionSheet: !0,
                                            success: function () {
                                                my.alert({
                                                    title: "保存成功"
                                                }), e.shareimg = 0, e.share = 0;
                                            }
                                        });
                                    }
                                });
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1, this.gwcdata();
                            },
                            getCouponBox: function () {
                                this.showCouponBox = 1;
                            },
                            closeCouponBox: function () {
                                this.showCouponBox = 0;
                            },
                            getList: function () {
                                var e = this;
                                m.request({
                                    url: this.$baseurl + "doPageCoupon",
                                    data: {
                                        suid: m.getStorageSync("suid"),
                                        uniacid: this.$uniacid
                                    },
                                    success: function (t) {
                                        e.couponlist = t.data.data, m.hideNavigationBarLoading(), m.stopPullDownRefresh();
                                    }
                                });
                            },
                            getmoney: function (t) {
                                if (!this.getSuid()) return !1;
                                var i = this,
                                    s = t.currentTarget.dataset.id,
                                    e = t.currentTarget.dataset.cid;
                                (0 < i.products.detail_coupon[e].nowCount || "无限" == i.products.detail_coupon[e].nowCount) && (0 < i.products.detail_coupon[e].counts || -1 == i.products.detail_coupon[e].counts) ? m.getStorage({
                                    key: "suid",
                                    success: function (t) {
                                        var e = t.data;
                                        e && m.request({
                                            url: i.$baseurl + "doPagegetcoupon",
                                            data: {
                                                id: s,
                                                suid: e,
                                                uniacid: i.$uniacid
                                            },
                                            success: function (t) {
                                                console.log(t), 1 == t.data.data && wx.showToast({
                                                    title: "领取成功",
                                                    icon: "success",
                                                    duration: 2e3,
                                                    success: function () {
                                                        setTimeout(function () {
                                                            i.getList(), i.getPro();
                                                        }, 500);
                                                    }
                                                }), 2 == t.data.data && wx.showToast({
                                                    title: "领取失败",
                                                    icon: "loading",
                                                    duration: 2e3,
                                                    success: function () {
                                                        setTimeout(function () {
                                                            i.getList(), i.getPro();
                                                        }, 500);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }): m.showToast({
                                    title: "不能再领啦",
                                    icon: "none",
                                    duration: 1500
                                });
                            },
                            makephonecall: function () {
                                m.makePhoneCall({
                                    phoneNumber: m.getStorageSync("base_tel")
                                });
                            }
                        }
                    };
                i.default = e;
            }).call(this, s("543d").default);
        }
    },
    [
        ["978b", "common/runtime", "common/vendor"]
    ]
]);