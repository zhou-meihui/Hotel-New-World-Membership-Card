(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/store/store"], {
        "0eab": function (t, e, a) {
            var n = a("6a37");
            a.n(n).a;
        },
        "5e7a": function (t, e, a) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                a("020c"), a("921b"), e(a("66fd")), t(e(a("a949")).default);
            }).call(this, a("543d").createPage);
        },
        6190: function (t, e, a) {
            var n = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            a.d(e, "a", function () {
                return n;
            }), a.d(e, "b", function () {
                return i;
            });
        },
        "6a37": function (t, e, a) {},
        "85a3": function (t, e, n) {
            (function (r) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var i = n("f571"),
                    a = n("0f48"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                page_signs: "/pages/store/store",
                                storelist: [],
                                baseinfo: [],
                                footer: {
                                    i_tel: this.$imgurl + "i_tel.png",
                                    i_add: this.$imgurl + "i_add.png",
                                    i_time: this.$imgurl + "i_time.png",
                                    i_view: this.$imgurl + "i_view.png",
                                    close: this.$imgurl + "c.png",
                                    v_ico: this.$imgurl + "p.png"
                                },
                                currentCity: "",
                                searchtitle: "",
                                title: "",
                                city: "",
                                storeShow: 0,
                                storenum: 0,
                                search: 0
                            };
                        },
                        onPullDownRefresh: function (t) {
                            this.getStoreConf(), this.searchtitle = "", r.stopPullDownRefresh();
                        },
                        onLoad: function (t) {
                            var e = this,
                                a = t.city;
                            a && (this.city = a);
                            var n = 0;
                            t.fxsid && (n = t.fxsid, this.fxsid = t.fxsid), this._baseMin(this), i.getOpenid(n, function () {
                                e.getStoreConf();
                            }, function () {
                                e.needAuth = !0;
                            }, function () {
                                e.needBind = !0;
                            });
                        },
                        onShareAppMessage: function () {
                            return {
                                title: this.title + "展示 -" + this.baseinfo.name
                            };
                        },
                        methods: {
                            openMap: function (t) {
                                r.openLocation({
                                    latitude: t.currentTarget.dataset.lat,
                                    longitude: t.currentTarget.dataset.lon,
                                    name: t.currentTarget.dataset.title,
                                    address: t.currentTarget.dataset.address,
                                    scale: 22
                                });
                            },
                            redirectto: function (t) {
                                var e = t.currentTarget.dataset.link,
                                    a = t.currentTarget.dataset.linktype;
                                this._redirectto(e, a);
                            },
                            getStoreConf: function () {
                                var o = this;
                                r.request({
                                    url: o.$baseurl + "doPagestoreConf",
                                    data: {
                                        uniacid: o.$uniacid
                                    },
                                    success: function (t) {
                                        if (t.data.data.mapkey) var e = t.data.data.mapkey;
                                        else e = "6DYBZ-GN6W3-45I3C-Y2A4Q-YRIAS-YFBP3";
                                        o.search = t.data.data.search, o.title = t.data.data.title, o.storeShow = t.data.data.flag,
                                            r.setNavigationBarTitle({
                                                title: o.title + "展示"
                                            });
                                        var i = new a({
                                            key: e
                                        });
                                        0 == t.data.data.flag ? r.getLocation({
                                            success: function (t) {
                                                var e = t.latitude,
                                                    a = t.longitude;
                                                i.reverseGeocoder({
                                                    location: {
                                                        latitude: e,
                                                        longitude: a
                                                    },
                                                    success: function (t) {
                                                        o.currentCity = t.result.address_component.city;
                                                    }
                                                }), o.getListAll(a, e);
                                            },
                                            fail: function () {
                                                o.getListAll();
                                            }
                                        }) : r.getLocation({
                                            success: function (t) {
                                                var a = t.latitude,
                                                    n = t.longitude;
                                                i.reverseGeocoder({
                                                    location: {
                                                        latitude: a,
                                                        longitude: n
                                                    },
                                                    success: function (t) {
                                                        var e = o.city;
                                                        e ? (o.currentCity = e, o.getList(n, a, e)) : (o.currentCity = t.result.address_component.city,
                                                            o.getList(n, a, t.result.address_component.city));
                                                    }
                                                });
                                            }
                                        }), r.hideNavigationBarLoading(), r.stopPullDownRefresh();
                                    },
                                    fail: function (t) {}
                                });
                            },
                            getList: function (t, e, a) {
                                var n = this;
                                r.request({
                                    url: n.$baseurl + "doPagestoreNew",
                                    data: {
                                        uniacid: n.$uniacid,
                                        lon: t,
                                        lat: e,
                                        currentCity: a
                                    },
                                    success: function (t) {
                                        n.storelist = t.data.data, n.storenum = t.data.data.length, r.hideNavigationBarLoading(),
                                            r.stopPullDownRefresh();
                                    },
                                    fail: function (t) {}
                                });
                            },
                            getListAll: function (t, e) {
                                var a = this;
                                r.request({
                                    url: a.$baseurl + "doPagestore",
                                    data: {
                                        lon: t,
                                        lat: e,
                                        uniacid: a.$uniacid
                                    },
                                    success: function (t) {
                                        a.storelist = t.data.data.list, a.storenum = t.data.data.num, r.hideNavigationBarLoading(),
                                            r.stopPullDownRefresh();
                                    },
                                    fail: function (t) {}
                                });
                            },
                            dianPhoneCall: function (t) {
                                var e = t.currentTarget.dataset.index;
                                r.makePhoneCall({
                                    phoneNumber: e
                                });
                            },
                            makePhoneCall: function (t) {
                                var e = this.baseinfo.tel;
                                r.makePhoneCall({
                                    phoneNumber: e
                                });
                            },
                            makePhoneCallB: function (t) {
                                var e = this.baseinfo.tel_b;
                                r.makePhoneCall({
                                    phoneNumber: e
                                });
                            },
                            mycoupp: function () {
                                r.redirectTo({
                                    url: "/pages/mycoupon/mycoupon"
                                });
                            },
                            serachInput: function (t) {
                                this.searchtitle = t.detail.value;
                            },
                            searchR: function () {
                                var e = this,
                                    t = t,
                                    a = a,
                                    n = e.searchtitle;
                                n ? r.request({
                                    url: e.$baseurl + "dopagestore",
                                    data: {
                                        uniacid: e.$uniacid,
                                        lon: a,
                                        lat: t,
                                        keyword: n
                                    },
                                    success: function (t) {
                                        e.storelist = t.data.data.list, e.storenum = t.data.data.num, r.hideNavigationBarLoading(),
                                            r.stopPullDownRefresh();
                                    },
                                    fail: function (t) {}
                                }) : r.showModal({
                                    title: "提醒",
                                    content: "请输入搜索关键字！",
                                    showCancel: !1
                                });
                            },
                            gesinco: function () {
                                this.diplay = res.data.data.flag;
                                var t = 0;
                                for (1 == res.flag ? t++ : t--, t = 0; t < res.data.length; t++) res.data[t].cmd;
                            }
                        }
                    };
                e.default = t;
            }).call(this, n("543d").default);
        },
        a949: function (t, e, a) {
            a.r(e);
            var n = a("6190"),
                i = a("fcf3");
            for (var o in i) "default" !== o && function (t) {
                a.d(e, t, function () {
                    return i[t];
                });
            }(o);
            a("0eab");
            var r = a("2877"),
                s = Object(r.a)(i.default, n.a, n.b, !1, null, null, null);
            e.default = s.exports;
        },
        fcf3: function (t, e, a) {
            a.r(e);
            var n = a("85a3"),
                i = a.n(n);
            for (var o in n) "default" !== o && function (t) {
                a.d(e, t, function () {
                    return n[t];
                });
            }(o);
            e.default = i.a;
        }
    },
    [
        ["5e7a", "common/runtime", "common/vendor"]
    ]
]);