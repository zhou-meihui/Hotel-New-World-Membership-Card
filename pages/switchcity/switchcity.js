(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/switchcity/switchcity"], {
        "0514": function (t, e, i) {
            var c = function () {
                    this.$createElement;
                    this._self._c;
                },
                n = [];
            i.d(e, "a", function () {
                return c;
            }), i.d(e, "b", function () {
                return n;
            });
        },
        "17d8": function (t, e, i) {
            var c = i("54dd");
            i.n(c).a;
        },
        "54dd": function (t, e, i) {},
        "641f": function (t, e, i) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                i("020c"), i("921b"), e(i("66fd")), t(e(i("6869")).default);
            }).call(this, i("543d").createPage);
        },
        6869: function (t, e, i) {
            i.r(e);
            var c = i("0514"),
                n = i("f1b7");
            for (var r in n) "default" !== r && function (t) {
                i.d(e, t, function () {
                    return n[t];
                });
            }(r);
            i("17d8");
            var o = i("2877"),
                a = Object(o.a)(n.default, c.a, c.b, !1, null, null, null);
            e.default = a.exports;
        },
        "8e7b": function (t, e, i) {
            (function (f) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var y = i("f571"),
                    l = i("8921"),
                    t = {
                        data: function () {
                            return {
                                searchLetter: [],
                                showLetter: "",
                                winHeight: 0,
                                cityList: [],
                                isShowLetter: !1,
                                scrollTop: 0,
                                scrollTopId: "",
                                city: "上海市",
                                hotcityList: [{
                                    cityCode: 11e4,
                                    city: "北京市"
                                }, {
                                    cityCode: 31e4,
                                    city: "上海市"
                                }, {
                                    cityCode: 440100,
                                    city: "广州市"
                                }, {
                                    cityCode: 440300,
                                    city: "深圳市"
                                }, {
                                    cityCode: 330100,
                                    city: "杭州市"
                                }, {
                                    cityCode: 320100,
                                    city: "南京市"
                                }, {
                                    cityCode: 420100,
                                    city: "武汉市"
                                }, {
                                    cityCode: 410100,
                                    city: "郑州市"
                                }, {
                                    cityCode: 12e4,
                                    city: "天津市"
                                }, {
                                    cityCode: 610100,
                                    city: "西安市"
                                }, {
                                    cityCode: 510100,
                                    city: "成都市"
                                }, {
                                    cityCode: 5e5,
                                    city: "重庆市"
                                }],
                                baseinfo: []
                            };
                        },
                        onLoad: function (t) {
                            for (var e = this, i = this, c = l.searchLetter, n = t.c, r = l.cityList(), o = f.getSystemInfoSync().windowHeight, a = o / c.length, d = [], s = 0; s < c.length; s++) {
                                var u = {};
                                u.name = c[s], u.tHeight = s * a, u.bHeight = (s + 1) * a, d.push(u);
                            }
                            i.winHeight = o, i.itemH = a, i.searchLetter = d, i.cityList = r, i.city = n;
                            t.fxsid && (i.fxsid = t.fxsid), this._baseMin(this), y.getOpenid(0, function () {}, function () {
                                e.needAuth = !0;
                            }, function () {
                                e.needBind = !0;
                            });
                        },
                        methods: {
                            redirectto: function (t) {
                                var e = t.currentTarget.dataset.link,
                                    i = t.currentTarget.dataset.linktype;
                                this._redirectto(e, i);
                            },
                            clickLetter: function (t) {
                                var e = t.currentTarget.dataset.letter;
                                this.showLetter = e, this.isShowLetter = !0, this.scrollTopId = e;
                                var i = this;
                                setTimeout(function () {
                                    i.isShowLetter = !1;
                                }, 1e3);
                            },
                            bindCity: function (t) {
                                this.city = t.currentTarget.dataset.city, f.redirectTo({
                                    url: "/pages/store/store?city=" + t.currentTarget.dataset.city
                                });
                            },
                            bindHotCity: function (t) {
                                this.city = t.currentTarget.dataset.city, f.redirectTo({
                                    url: "/pages/store/store?city=" + t.currentTarget.dataset.city
                                });
                            },
                            hotCity: function () {
                                this.scrollTop = 0;
                            }
                        }
                    };
                e.default = t;
            }).call(this, i("543d").default);
        },
        f1b7: function (t, e, i) {
            i.r(e);
            var c = i("8e7b"),
                n = i.n(c);
            for (var r in c) "default" !== r && function (t) {
                i.d(e, t, function () {
                    return c[t];
                });
            }(r);
            e.default = n.a;
        }
    },
    [
        ["641f", "common/runtime", "common/vendor"]
    ]
]);