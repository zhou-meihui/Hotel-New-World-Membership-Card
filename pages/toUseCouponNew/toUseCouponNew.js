// pages/toUseCouponNew/toUseCouponNew.js
var config = require('../../siteinfo');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    codeObj: {},
    rules: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log('options',options.obj,options)
    this.getRules(options.id)
    // let code = 'https://yzhy.cg500.com'+ JSON.parse(options.obj).qrcode
    
    // this.setData({
    //   codeObj: decodeURIComponent(options.obj)
    // })
    this.setData({
      codeObj:JSON.parse(options.obj)
    })
    console.log(this.data.codeObj,'codeObj')
    // this.data.codeObj.qrcode = 'https://yzhy.cg500.com/'+ this.data.codeObj.qrcode
    // console.log(this.data.codeObj.qrcode,'this.data.codeObj.qrcode')
    // this.setData({
    //   codeObj: this.data.codeObj
    // })
  },
  getRules(id){
    var _this = this;
    wx.request({
      url: config.siteroot1 + "couponInfo",
      data: {
        id: id
      },
      header: {
        "custom-header": "hello"
      },
      success: function (e) {
        
        if (e.data.err) {
          wx.showToast({
            title: e.data.msg,
            icon: 'none'
          })
          return
        }
        console.log(e.data.coupon_txt,'couponInfo')
        _this.setData({
          rules: e.data.coupon_txt
        })
      }
    })
  },
  useRule(){
    wx.navigateTo({
      url: '/pages/index/index?pageid=423',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})