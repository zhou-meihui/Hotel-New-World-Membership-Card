var config = require('../../siteinfo.js');
(global.webpackJsonp = global.webpackJsonp || []).push([
  ["pages/usercenter/usercenter"], {
    "4a54": function (e, a, t) {
      (function (o) {
        Object.defineProperty(a, "__esModule", {
          value: !0
        }), a.default = void 0, t("2f62");
        var i = t("f571"),
          e = {
            data: function () {
              return {
                $imgurl: this.$imgurl,
                globaluser: "",
                vipinfodata: "",
                menuidata: "",
                isvip: "",
                needVip: "",
                money: 0,
                score: 0,
                coupon: 0,
                vipset: 1,
                viptext: "6688",
                baseinfo: {},
                vipflag: 1,
                fxzzd: "",
                myorder: "",
                mysign: "",
                mycoupons: "",
                recharge: 0,
                cardname: "",
                userbg: "",
                needAuth: !1,
                needBind: !1,
                ali_userinfo: "",
                baidu_userinfo: "",
                toutiao_userinfo: "",
                qq_userinfo: "",
                superuser: "",
                page_signs: "/pages/usercenter/usercenter",
                member_card: 1,
                grade: 0,
                vipname: "",
                isview: 0,
                equity_flag: 1,
                is: {
                  coupon_arr: []
                },
                is_ios: 0,
                re_money: "",
                re_score: 0,
                re_give_money: 0,
                coupon_arrs: [],
                recharge_receive_is: 0,
                counts1: 0,
                counts2: 0,
                counts3: 0,
                counts4: 0,
                counts5: 0,
                has_suid: 0,
                cardList: [],
                isPopupVisible: false,
                codeImg: '',
                isHx: false, //true 核销员  false 不是核销员
              };
            },
            computed: {},
            onShow: function () {
              this.vipinfo();
              this.isHxyuan(); // 核销员验证

            },
            onLoad: function (e) {
              this.getSuid()
              this.getopenCardStatus()
              var a = this;
              this.getMainOrderNum(), this._baseMin(this), 0 <= o.getStorageSync("systemInfo").system.indexOf("iOS") && (this.is_ios = 1);
              var t = 0;
              e.fxsid && (t = e.fxsid), this.fxsid = t;
              var n = o.getStorageSync("suid");
              this.has_suid = n, console.log("weixin"), i.getOpenid(t, function () {
                a.globaluserinfo(), a.menuinfo();
              }, function () {
                a.needAuth = !1;
              }, function () {
                a.needBind = !0;
              });
            },
            onPullDownRefresh: function () {
              this.vipinfo(), this.menuinfo(), this.getMainOrderNum(), this.globaluserinfo(),
                o.getStorageSync("golobeuser") && (console.log(11111111), this._checkBindPhone(this)),
                o.stopPullDownRefresh();
            },
            onShareAppMessage: function () {},
            methods: {
              // 扫码核销
              scanCode() {
                let that = this;
                wx.scanCode({
                  success(res) {
                    console.log(res, 'resss')
                    that.result1 = res.result
                    const info = JSON.parse(that.result1)
                    console.log('info', info)
                    wx.navigateTo({
                      url: '/pages/myCupponCode/myCupponCode?info=' + res.result,
                    })
                    // if (info.type == 1) {
                    //   wx.navigateTo({
                    //     url: '/pages/myCupponCode/myCupponCode?info=' + res.result,
                    //   })
                    // }else if(info.type == 2){
                    //   wx.navigateTo({
                    //     url: '/pages/myCouponHx/myCouponHx?info=' + res.result,
                    //   })
                    // }
                    // wx.navigateTo({
                    //   url: '/pages/myCouponHx/myCouponHx?info=' + res.result,
                    // })

                  }
                })
              },
              viewErCode(e) {
                console.log(e, 'viewErCode')
                // this.codeImg = 'https://yzhy.cg500.com/' + e
                // this.isPopupVisible = true
                let codeImg = 'https://yzhy.cg500.com/' + e
                wx.previewImage({
                  urls: [codeImg],
                  current: 0
                });
              },
              viewCardInfo(e) {
                console.log(e, 'viewCardInfo')
                let id = e
                if (id == 1) {
                  wx.navigateTo({
                    url: '/pages/index/index?pageid=420',
                  })
                } else if (id == 2) {
                  wx.navigateTo({
                    url: '/pages/index/index?pageid=424',
                  })
                }
              },
              // 了解更多
              getMore() {
                wx.navigateTo({
                  url: '/pages/cardQuan/cardQuan',
                })
              },
              onPopupHide() {
                this.isPopupVisible = false
              },
              toPerson: function () {
                wx.navigateTo({
                  url: '/pages/personalInfo/personalInfo',
                })
              },
              toBuyCardList: function () {
                wx.navigateTo({
                  url: '/pages/buyCardList/buyCardList',
                })
              },
              // 核销员验证
              isHxyuan: function () {
                var _this = this;
                wx.request({
                  url: config.siteroot1 + "/isHx",
                  data: {
                    uniacid: this.$uniacid,
                    suid: o.getStorageSync("suid")
                  },
                  success: function (e) {
                    console.log(e.data, '核销员验证')
                    if (e.data == "success") {
                      _this.isHx = true
                    }
                  }
                })
              },
              getopenCardStatus: function () {
                console.log('走了么', config.siteroot1)
                var _this = this;
                wx.request({
                  url: config.siteroot1 + "/userClubCard",
                  data: {
                    uniacid: this.$uniacid,
                    suid: o.getStorageSync("suid"),
                    uid: o.getStorageSync("golobeuid")
                  },
                  success: function (e) {
                    console.log(e.data, 'userClubCard')
                    _this.cardList = e.data
                  }
                })
              },
              getMainOrderNum: function () {
                var a = this;
                o.request({
                  url: this.$host + "/api/MainWxapp/getMainOrderStatus",
                  data: {
                    uniacid: this.$uniacid,
                    suid: o.getStorageSync("suid")
                  },
                  success: function (e) {
                    a.counts1 = e.data.data.order_types.not_payment, a.counts2 = e.data.data.order_types.not_delivery,
                      a.counts3 = e.data.data.order_types.not_receiver, a.counts4 = e.data.data.order_types.not_evaluate,
                      a.counts5 = e.data.data.order_types.not_after_sale;
                  }
                });
              },
              getPhone: function () {
                console.log('走了么')
                // if (!o.getStorageSync("suid")) {
                //   o.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0;
                // }
                this.needBind = !0
              },
              getSuid: function () {
                if (!o.getStorageSync("suid")) {
                  o.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0;
                }
                // this.needBind = !0
                // if (!o.getStorageSync("suid1")) {
                //   o.getStorageSync("golobeuser1") ? this.needBind = !0 : this.needAuth = !0;
                // }
              },
              h5DoLogin: function () {
                this.needBind = !0;
              },
              cell: function () {
                this.needAuth = !1, this.vipinfo(), this._getSuperUserInfo(this);
              },
              closeAuth: function () {
                this.needAuth = !1, this.globaluserinfo(), this.isHxyuan(); this.getopenCardStatus();console.log("closeAuth888"), o.getStorageSync("golobeuser") && this._checkBindPhone(this);
              },
              closeBind: function () {
                console.log("closeBind"), this.needBind = !1, this.vipflag = 1, this.vipinfo(),
                  this._getSuperUserInfo(this), this.has_suid = o.getStorageSync("suid");
                  this.globaluserinfo();
                  this.isHxyuan();
                  this.getopenCardStatus();
              },
              globaluserinfo: function () {
                // if (o.getStorageSync("suid1")) {
                  var a = this,
                    e = o.getStorageSync("openid");
                  o.request({
                    url: this.$baseurl + "dopageglobaluserinfo",
                    data: {
                      uniacid: this.$uniacid,
                      openid: e,
                      source: o.getStorageSync("source")
                    },
                    header: {
                      "custom-header": "hello"
                    },
                    success: function (e) {

                      // wx.setStorageSync('golobeuser1', e.data.data)
                      // a.globaluser = wx.getStorageSync('golobeuser1')
                      a.globaluser = e.data.data;
                    }
                  });
                // }

              },
              aliUserinfo: function () {
                this.ali_userinfo = o.getStorageSync("ali_userinfo");
              },
              qqUserinfo: function () {
                this.qq_userinfo = o.getStorageSync("qq_userinfo");
              },
              toutiaoUserinfo: function () {
                this.toutiao_userinfo = o.getStorageSync("toutiao_userinfo");
              },
              getBaiduUserInfo: function () {
                this.baidu_userinfo = o.getStorageSync("baidu_userinfo"), this.vipinfo();
              },
              vipinfo: function () {
                var a = this;
                o.request({
                  url: this.$baseurl + "doPageMymoneyD",
                  data: {
                    uniacid: this.$uniacid,
                    suid: o.getStorageSync("suid")
                  },
                  success: function (e) {
                    a.vipinfodata = e.data.data, "2" != e.data.data.vipset || e.data.data.vipid || e.data.data.vipflag ? e.data.data.vipid ? (a.viptext = e.data.data.vipid.substr(12, 4),
                        a.isvip = !0) : (3 == e.data.data.vipflag ? a.vipflag = 3 : 2 == e.data.data.vipflag ? a.vipflag = 2 : 4 == e.data.data.vipflag && (a.vipflag = 4),
                        a.isvip = !1) : a.needVip = !0, a.userbg = e.data.data.userbg, a.money = e.data.data.money,
                      a.score = e.data.data.score, a.coupon = e.data.data.couponNum, a.vipset = e.data.data.vipset,
                      a.cardname = e.data.data.cardname, a.grade = e.data.data.grade, a.vipname = e.data.data.vipname,
                      a.is = e.data.data.is, a.equity_flag = e.data.data.is.flag, a.recharge_receive_is = e.data.data.recharge_receive_is,
                      e.data.data.recharge_receive_is && (a.moneydata = e.data.data.recharge_receive,
                        a.re_money = a.moneydata.money, a.re_score = a.moneydata.score, a.re_give_money = a.moneydata.give_money,
                        a.coupon_arrs = a.moneydata.coupon_con);
                  }
                });
              },
              menuinfo: function () {
                var a = this;
                o.request({
                  url: this.$baseurl + "doPageupdatausersetnew",
                  data: {
                    uniacid: this.$uniacid,
                    source: o.getStorageSync("source"),
                    suid: o.getStorageSync("suid")
                  },
                  header: {
                    "custom-header": "hello"
                  },
                  success: function (e) {
                    a.menuidata = e.data.data, a.fxzzd = e.data.data.arrs, a.myorder = e.data.data.myorder,
                      a.mysign = e.data.data.mysign, a.mycoupons = e.data.data.mycoupons;
                  }
                });
              },
              dianPhoneCall: function (e) {
                console.log(2222);
                var a = e.currentTarget.dataset.index;
                o.makePhoneCall({
                  phoneNumber: a
                });
              },
              toSign: function () {
                o.navigateTo({
                  url: "/pagesSign/index/index"
                });
              },
              cancel: function () {
                var a = this;
                o.request({
                  url: a.$baseurl + "doPagechangeReceive",
                  data: {
                    uniacid: a.$uniacid,
                    id: a.is.id
                  },
                  success: function (e) {
                    a.equity_flag = 1;
                  }
                });
              },
              con_cancel: function () {
                var a = this;
                o.request({
                  url: a.$baseurl + "doPagechangeRechargeReceive",
                  data: {
                    uniacid: a.$uniacid,
                    id: a.moneydata.id
                  },
                  success: function (e) {
                    a.recharge_receive_is = 0;
                  }
                });
              },
              scancode: function () {
                var i = this;
                o.scanCode({
                  success: function (e) {
                    var a = e.result.split("&"),
                      t = a[0].split("=")[1],
                      n = a[1].split("=")[1]; -
                    1 < t && n ? o.showModal({
                      title: "提示",
                      content: "确认核销",
                      success: function (e) {
                        e.confirm && o.request({
                          url: i.$baseurl + "hxmm",
                          data: {
                            uniacid: i.$uniacid,
                            order_id: n,
                            suid: o.getStorageSync("suid"),
                            hxuser_is: 1,
                            is_more: t
                          },
                          success: function (e) {
                            var a = e.data.data;
                            3 == a ? o.showModal({
                              title: "提示",
                              content: "不是核销员，无权限核销！",
                              showCancel: !1,
                              success: function (e) {
                                o.redirectTo({
                                  url: "/pages/usercenter/usercenter"
                                });
                              }
                            }) : 1 == a ? o.showToast({
                              title: "核销成功",
                              icon: "success",
                              duration: 2e3
                            }) : 2 == a ? o.showModal({
                              title: "提示",
                              content: "核销失败，该订单是已核销订单!",
                              showCancel: !1
                            }) : 4 == a && o.showModal({
                              title: "提示",
                              content: "该订单是多商户订单，请进入店铺管理用管理员核销!",
                              showCancel: !1
                            });
                          }
                        });
                      }
                    }) : o.showModal({
                      title: "提示",
                      content: "二维码错误",
                      showCancel: !1
                    });
                  }
                });
              }
            }
          };
        a.default = e;
      }).call(this, t("543d").default);
    },
    "5eea": function (e, a, t) {
      t.r(a);
      var n = t("d10d"),
        i = t("f590");
      for (var o in i) "default" !== o && function (e) {
        t.d(a, e, function () {
          return i[e];
        });
      }(o);
      t("fecf");
      var s = t("2877"),
        u = Object(s.a)(i.default, n.a, n.b, !1, null, null, null);
      a.default = u.exports;
    },
    a018: function (e, a, t) {},
    d10d: function (e, a, t) {
      var n = function () {
          this.$createElement;
          this._self._c;
        },
        i = [];
      t.d(a, "a", function () {
        return n;
      }), t.d(a, "b", function () {
        return i;
      });
    },
    ede9: function (e, a, t) {
      (function (e) {
        function a(e) {
          return e && e.__esModule ? e : {
            default: e
          };
        }
        t("020c"), t("921b"), a(t("66fd")), e(a(t("5eea")).default);
      }).call(this, t("543d").createPage);
    },
    f590: function (e, a, t) {
      t.r(a);
      var n = t("4a54"),
        i = t.n(n);
      for (var o in n) "default" !== o && function (e) {
        t.d(a, e, function () {
          return n[e];
        });
      }(o);
      a.default = i.a;
    },
    fecf: function (e, a, t) {
      var n = t("a018");
      t.n(n).a;
    }
  },
  [
    ["ede9", "common/runtime", "common/vendor"]
  ]
]);