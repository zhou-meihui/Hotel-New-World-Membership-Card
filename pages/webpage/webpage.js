(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pages/webpage/webpage"], {
        "3abb": function (n, e, t) {
            (function (n) {
                function e(n) {
                    return n && n.__esModule ? n : {
                        default: n
                    };
                }
                t("020c"), t("921b"), e(t("66fd")), n(e(t("9b11")).default);
            }).call(this, t("543d").createPage);
        },
        "4fd1": function (n, e, t) {
            t.r(e);
            var o = t("a317"),
                u = t.n(o);
            for (var a in o) "default" !== a && function (n) {
                t.d(e, n, function () {
                    return o[n];
                });
            }(a);
            e.default = u.a;
        },
        "96cc": function (n, e, t) {
            var o = function () {
                    this.$createElement;
                    this._self._c;
                },
                u = [];
            t.d(e, "a", function () {
                return o;
            }), t.d(e, "b", function () {
                return u;
            });
        },
        "9b11": function (n, e, t) {
            t.r(e);
            var o = t("96cc"),
                u = t("4fd1");
            for (var a in u) "default" !== a && function (n) {
                t.d(e, n, function () {
                    return u[n];
                });
            }(a);
            var f = t("2877"),
                r = Object(f.a)(u.default, o.a, o.b, !1, null, null, null);
            e.default = r.exports;
        },
        a317: function (n, t, e) {
            (function (n) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var e = {
                    data: function () {
                        return {
                            webviewStyles: {
                                progress: {
                                    color: "#FF3333"
                                }
                            },
                            url: "",
                            baseinfo: ""
                        };
                    },
                    onPullDownRefresh: function () {
                        this.getinfos(), n.stopPullDownRefresh();
                    },
                    onLoad: function (n) {
                        var e = this;
                        e._baseMin(this), e.url = decodeURIComponent(n.url), n.fxsid && (e.fxsid = n.fxsid);
                    },
                    onShareAppMessage: function () {
                        return {};
                    }
                };
                t.default = e;
            }).call(this, e("543d").default);
        }
    },
    [
        ["3abb", "common/runtime", "common/vendor"]
    ]
]);