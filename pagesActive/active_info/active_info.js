(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesActive/active_info/active_info"], {
        "140a": function (t, e, i) {
            i.r(e);
            var a = i("7b0a"),
                n = i.n(a);
            for (var s in a) "default" !== s && function (t) {
                i.d(e, t, function () {
                    return a[t];
                });
            }(s);
            e.default = n.a;
        },
        "1c29": function (t, e, i) {
            var a = i("dcbf");
            i.n(a).a;
        },
        "62b6": function (t, e, i) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                n = [];
            i.d(e, "a", function () {
                return a;
            }), i.d(e, "b", function () {
                return n;
            });
        },
        "75b0": function (t, e, i) {
            i.r(e);
            var a = i("62b6"),
                n = i("140a");
            for (var s in n) "default" !== s && function (t) {
                i.d(e, t, function () {
                    return n[t];
                });
            }(s);
            i("1c29");
            var o = i("2877"),
                c = Object(o.a)(n.default, a.a, a.b, !1, null, null, null);
            e.default = c.exports;
        },
        "7b0a": function (t, i, n) {
            (function (s) {
                Object.defineProperty(i, "__esModule", {
                    value: !0
                }), i.default = void 0;
                var t, a = (t = n("3584")) && t.__esModule ? t : {
                    default: t
                };
                var o = n("f571"),
                    e = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                $host: this.$host,
                                aid: 0,
                                autoplay: !0,
                                minHeight: 220,
                                sc: 0,
                                currentSwiper: 0,
                                baseinfo: "",
                                activeinfo: {
                                    pics: []
                                },
                                share: 0,
                                texts: "",
                                active_endtime: "",
                                active_enddate: "",
                                active_endh: "",
                                active_endm: "",
                                active_ends: "",
                                shareimg: "",
                                shareimg_url: "",
                                img_w: "",
                                img_h: "",
                                system_w: "",
                                system_h: "",
                                dlength: 0,
                                needAuth: !1,
                                needBind: !1,
                                thumb: "",
                                applylist_count: 0
                            };
                        },
                        onLoad: function (t) {
                            var e = this,
                                i = 0;
                            t && (i = t.aid, this.aid = i);
                            var a = 0;
                            t.fxsid && (a = t.fxsid, s.setStorageSync("fxsid", a), this.fxsid = t.fxsid), this._baseMin(this),
                                s.getStorageSync("suid"), o.getOpenid(a, function () {
                                    e.activeInfo();
                                });
                            var n = s.getStorageSync("systemInfo");
                            this.img_w = parseInt((.65 * n.windowWidth).toFixed(0)), this.img_h = parseInt((1.875 * this.img_w).toFixed(0)),
                                this.system_w = parseInt(n.windowWidth), this.system_h = parseInt(n.windowHeight);
                        },
                        onPullDownRefresh: function () {
                            this.activeInfo(), s.stopPullDownRefresh();
                        },
                        onShareAppMessage: function () {
                            var t = s.getStorageSync("suid"),
                                e = "/pagesActive/active_info/active_info?aid=" + this.aid + "&fxsid=" + t + "&userid=" + t;
                            return {
                                title: this.title,
                                path: e
                            };
                        },
                        methods: {
                            daojishi: function () {
                                var t = this,
                                    e = t.activeinfo,
                                    i = new Date().getTime();
                                if (0 == e.starttime && 0 == e.endtime);
                                else if (1e3 * e.starttime > i) e.t_flag = 1;
                                else if (1e3 * e.endtime < i) e.t_flag = 2;
                                else if (e.endtime < 0) e.active_endtime = 0;
                                else {
                                    var a, n, s, o, c = 1e3 * parseInt(e.endtime) - i;
                                    0 <= c && (a = Math.floor(c / 1e3 / 60 / 60 / 24), n = Math.floor(c / 1e3 / 60 / 60 % 24) < 10 ? "0" + Math.floor(c / 1e3 / 60 / 60 % 24) : Math.floor(c / 1e3 / 60 / 60 % 24),
                                            s = Math.floor(c / 1e3 / 60 % 60) < 10 ? "0" + Math.floor(c / 1e3 / 60 % 60) : Math.floor(c / 1e3 / 60 % 60),
                                            o = Math.floor(c / 1e3 % 60) < 10 ? "0" + Math.floor(c / 1e3 % 60) : Math.floor(c / 1e3 % 60)),
                                        0 < a ? (e.active_endtime = a + "天" + n + ":" + s + ":" + o, e.active_enddate = a + "天") : e.active_endtime = n + ":" + s + ":" + o,
                                        e.active_endh = n, e.active_endm = s, e.active_ends = o;
                                }
                                t.active_endtime = e.active_endtime, t.active_enddate = e.active_enddate, t.active_endh = e.active_endh,
                                    t.active_endm = e.active_endm, t.active_ends = e.active_ends, setTimeout(function () {
                                        t.daojishi();
                                    }, 1e3);
                            },
                            getShareImg: function () {
                                s.showLoading({
                                    title: "海报生成中"
                                });
                                var e = this;
                                s.request({
                                    url: e.$baseurl + "dopageshareewm",
                                    data: {
                                        uniacid: e.$uniacid,
                                        suid: s.getStorageSync("suid"),
                                        gid: e.aid,
                                        types: "active",
                                        source: s.getStorageSync("source"),
                                        pageUrl: "active"
                                    },
                                    success: function (t) {
                                        s.hideLoading(), 0 == t.data.data.error ? (e.shareimg = 1, e.shareimg_url = t.data.data.url) : s.showToast({
                                            title: t.data.data.msg,
                                            icon: "none"
                                        });
                                    }
                                });
                            },
                            closeShare: function () {
                                this.shareimg = 0, this.share = 0;
                            },
                            saveImg: function () {
                                var e = this;
                                s.getImageInfo({
                                    src: e.shareimg_url,
                                    success: function (t) {
                                        s.saveImageToPhotosAlbum({
                                            filePath: t.path,
                                            success: function () {
                                                s.showToast({
                                                    title: "保存成功！",
                                                    icon: "none"
                                                }), e.shareimg = 0, e.share = 0;
                                            }
                                        });
                                    }
                                });
                            },
                            aliSaveImg: function () {
                                var e = this;
                                s.getImageInfo({
                                    src: e.shareimg_url,
                                    success: function (t) {
                                        my.saveImage({
                                            url: t.path,
                                            showActionSheet: !0,
                                            success: function () {
                                                my.alert({
                                                    title: "保存成功"
                                                }), e.shareimg = 0, e.share = 0;
                                            }
                                        });
                                    }
                                });
                            },
                            h5ShareAppMessage: function () {
                                s.showToast({
                                    title: "请使用浏览器自带分享功能",
                                    icon: "none"
                                });
                            },
                            share111: function () {
                                this.share = 1;
                            },
                            share_close: function () {
                                this.share = 0, this.shareimg = 0;
                            },
                            goapplylist: function () {
                                s.navigateTo({
                                    url: "/pagesActive/apply_list/apply_list?aid=" + this.aid
                                });
                            },
                            swiperLoad: function (a) {
                                var n = this;
                                s.getSystemInfo({
                                    success: function (t) {
                                        var e = a.detail.width / a.detail.height,
                                            i = t.windowWidth / e;
                                        n.heighthave || (n.minHeight = i, n.heighthave = 1);
                                    }
                                });
                            },
                            swiperChange: function (t) {
                                this.autoplay = !0, this.currentSwiper = t.detail.current, this.isplay = !1, this.autoplay = this.autoplay;
                            },
                            activeInfo: function () {
                                var i = this,
                                    t = this.aid;
                                s.request({
                                    url: this.$baseurl + "doPageActiveInfo",
                                    data: {
                                        uniacid: this.$uniacid,
                                        aid: t,
                                        suid: s.getStorageSync("suid"),
                                        source: s.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        var e = t.data.data;
                                        i.thumb = t.data.data.thumb, s.setNavigationBarTitle({
                                                title: e.name
                                            }), e.contents && (e.contents = e.contents.replace(/\<img/gi, '<img style="width:100%;height:auto;display:block" '),
                                                e.contents = (0, a.default)(e.contents)), i.texts = e.contents, i.activeinfo = e,
                                            i.applylist_count = e.applylist.length, i.sc = e.collectcount, i.dlength = e.pics.length,
                                            i.daojishi();
                                    }
                                });
                            },
                            goApply: function () {
                                if (!this.getSuid()) return !1;
                                s.navigateTo({
                                    url: "/pagesActive/apply/apply?aid=" + this.aid + "&thumb=" + this.thumb
                                });
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1;
                            },
                            makePhoneCall: function () {
                                var t = this.activeinfo.tel,
                                    e = this.baseinfo.tel;
                                s.makePhoneCall({
                                    phoneNumber: t || e
                                });
                            },
                            collect: function (t) {
                                var i = this;
                                if (!this.getSuid()) return !1;
                                var a = t.currentTarget.dataset.name;
                                s.getStorage({
                                    key: "suid",
                                    success: function (t) {
                                        var e = s.getStorageSync("suid");
                                        s.request({
                                            url: i.$baseurl + "doPageCollect",
                                            data: {
                                                suid: e,
                                                types: "active",
                                                id: a,
                                                uniacid: i.$uniacid
                                            },
                                            header: {
                                                "content-type": "application/json"
                                            },
                                            success: function (t) {
                                                var e = t.data.data;
                                                i.sc = "收藏成功" == e ? 1 : 0, s.showToast({
                                                    title: e,
                                                    icon: "succes",
                                                    duration: 1e3,
                                                    mask: !0
                                                });
                                            }
                                        });
                                    },
                                    fail: function () {
                                        var t = s.getStorageSync("appcode");
                                        s.request({
                                            url: i.$baseurl + "doPageAppbase",
                                            data: {
                                                code: t,
                                                uniacid: i.$uniacid
                                            },
                                            header: {
                                                "content-type": "application/json"
                                            },
                                            success: function (t) {
                                                var e = t.data.data.openid;
                                                s.setStorage({
                                                    key: "openid",
                                                    data: t.data.data.openid,
                                                    success: function () {
                                                        s.request({
                                                            url: i.$baseurl + "doPageCollect",
                                                            data: {
                                                                openid: e,
                                                                types: "active",
                                                                id: a,
                                                                uniacid: i.$uniacid
                                                            },
                                                            header: {
                                                                "content-type": "application/json"
                                                            },
                                                            success: function (t) {
                                                                var e = t.data.data;
                                                                i.sc = 1, s.showToast({
                                                                    title: e,
                                                                    icon: "succes",
                                                                    duration: 1e3,
                                                                    mask: !0
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            getSuid: function () {
                                if (s.getStorageSync("suid")) return !0;
                                return s.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                    !1;
                            }
                        }
                    };
                i.default = e;
            }).call(this, n("543d").default);
        },
        d498: function (t, e, i) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                i("020c"), i("921b"), e(i("66fd")), t(e(i("75b0")).default);
            }).call(this, i("543d").createPage);
        },
        dcbf: function (t, e, i) {}
    },
    [
        ["d498", "common/runtime", "common/vendor"]
    ]
]);