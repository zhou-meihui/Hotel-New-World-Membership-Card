(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesActive/apply/apply"], {
        "256c": function (t, e, a) {
            (function (l) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var i = a("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                aid: 0,
                                forminfo: [],
                                disabled: !1,
                                username: "",
                                tel: "",
                                pd_val: [],
                                ttcxs: "",
                                arrs: [],
                                pagedata: [],
                                formset: 0,
                                xuanz: 0,
                                isover: 0,
                                formdescs: "",
                                wxmobile: "",
                                baseinfo: "",
                                pro_tel: "",
                                myname: "",
                                mymobile: "",
                                myaddress: "",
                                scan_res: "",
                                thumb: "",
                                source: ""
                            };
                        },
                        onPullDownRefresh: function () {
                            this.getForms(), l.stopPullDownRefresh();
                        },
                        onLoad: function (t) {
                            var e = this;
                            this.source = l.getStorageSync("source"), this.refreshSessionkey(), l.setNavigationBarTitle({
                                title: "活动报名"
                            }), this._baseMin(this);
                            var a = 0;
                            t && (a = t.aid, this.aid = a), t.thumb && (t.thumb, this.thumb = t.thumb);
                            var n = 0;
                            t.fxsid && (n = t.fxsid, this.fxsid = t.fxsid), i.getOpenid(n, function () {
                                e.getForms();
                            }, function () {
                                e.needAuth = !0;
                            }, function () {
                                e.needBind = !0;
                            });
                        },
                        methods: {
                            getForms: function () {
                                var e = this,
                                    t = this.aid;
                                0 == t && l.showModal({
                                    title: "提示",
                                    content: "请先选择报名活动",
                                    showCancel: !1,
                                    success: function (t) {
                                        l.request({
                                            url: "/pagesActive/index/index"
                                        });
                                    }
                                }), l.request({
                                    url: this.$baseurl + "doPageGetForms",
                                    data: {
                                        uniacid: this.$uniacid,
                                        aid: t
                                    },
                                    success: function (t) {
                                        null != t.data.data && (e.pagedata = t.data.data.cons, e.formset = t.data.data.id);
                                    }
                                });
                            },
                            userNameInput: function (t) {
                                this.username = t.detail.value;
                            },
                            userTelInput: function (t) {
                                this.tel = t.detail.value;
                            },
                            bindInputChange: function (t) {
                                var e = t.detail.value,
                                    a = t.currentTarget.dataset.index,
                                    n = this.pagedata;
                                n[a].val = e, this.pagedata = n;
                            },
                            bindPickerChange: function (t) {
                                var e = t.detail.value,
                                    a = t.currentTarget.dataset.index,
                                    n = this.pagedata,
                                    i = n[a].tp_text[e];
                                n[a].val = i, this.pagedata = n;
                            },
                            bindDateChange: function (t) {
                                var e = t.detail.value,
                                    a = t.currentTarget.dataset.index,
                                    n = this.pagedata;
                                n[a].val = e, this.pagedata = n;
                            },
                            bindTimeChange: function (t) {
                                var e = t.detail.value,
                                    a = t.currentTarget.dataset.index,
                                    n = this.pagedata;
                                n[a].val = e, this.pagedata = n;
                            },
                            checkboxChange: function (t) {
                                var e = t.detail.value,
                                    a = t.currentTarget.dataset.index,
                                    n = this.pagedata;
                                n[a].val = e, this.pagedata = n;
                            },
                            radioChange: function (t) {
                                var e = t.detail.value,
                                    a = t.currentTarget.dataset.index,
                                    n = this.pagedata;
                                n[a].val = e, this.pagedata = n;
                            },
                            weixinadd: function () {
                                var o = this;
                                l.chooseAddress({
                                    success: function (t) {
                                        for (var e = t.provinceName + " " + t.cityName + " " + t.countyName + " " + t.detailInfo, a = t.userName, n = t.telNumber, i = o.pagedata, s = 0; s < i.length; s++) 0 == i[s].type && 2 == i[s].tp_text[0].yval && (i[s].val = a),
                                            0 == i[s].type && 3 == i[s].tp_text[0].yval && (i[s].val = n), 0 == i[s].type && 4 == i[s].tp_text[0].yval && (i[s].val = e);
                                        o.myname = a, o.mymobile = n, o.myaddress = e, o.pagedata = i;
                                    },
                                    fail: function (t) {
                                        l.getSetting({
                                            success: function (t) {
                                                t.authSetting["scope.address"] || l.openSetting({
                                                    success: function (t) {}
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            choiceimg1111: function (t) {
                                var s = this,
                                    e = 0,
                                    o = s.zhixin,
                                    r = t.currentTarget.dataset.index,
                                    u = s.pagedata,
                                    c = u[r].val,
                                    a = u[r].tp_text[0];
                                c ? e = c.length : (e = 0, c = []);
                                var n = a - e,
                                    d = s.$baseurl + "wxupimg";
                                s.pd_val, l.chooseImage({
                                    count: n,
                                    sizeType: ["original", "compressed"],
                                    sourceType: ["album", "camera"],
                                    success: function (t) {
                                        o = !0, s.zhixin = o, l.showLoading({
                                            title: "图片上传中"
                                        });
                                        var a = t.tempFilePaths;
                                        c = c.concat(a), u[r].val = c, s.pagedata = u;
                                        var n = 0,
                                            i = a.length;
                                        ! function e() {
                                            l.uploadFile({
                                                url: d,
                                                filePath: a[n],
                                                name: "file",
                                                success: function (t) {
                                                    u[r].z_val.push(t.data), s.pagedata = u, ++n < i ? e() : (o = !1, s.zhixin = o,
                                                        l.hideLoading());
                                                }
                                            });
                                        }();
                                    }
                                });
                            },
                            delimg: function (t) {
                                var e = t.currentTarget.dataset.index,
                                    a = t.currentTarget.dataset.id,
                                    n = this.pagedata,
                                    i = n[e].val;
                                i.splice(a, 1), 0 == i.length && (i = ""), n[e].val = i, this.pagedata = n;
                            },
                            onPreviewImage: function (t) {
                                app.util.showImage(t);
                            },
                            namexz: function (t) {
                                for (var e = this, a = t.currentTarget.dataset.index, n = e.pagedata[a], i = [], s = 0; s < n.tp_text.length; s++) {
                                    var o = {};
                                    o.keys = n.tp_text[s], o.val = 1, i.push(o);
                                }
                                e.ttcxs = 1, e.formindex = a, e.xx = i, e.xuanz = 0, e.lixuanz = -1, e.riqi();
                            },
                            riqi: function () {
                                for (var t = this, e = new Date(), a = new Date(e.getTime()), n = a.getFullYear() + "-" + (a.getMonth() + 1) + "-" + a.getDate(), i = t.xx, s = 0; s < i.length; s++) i[s].val = 1;
                                t.xx = i, t.gettoday(n);
                                var o = [],
                                    r = [],
                                    u = new Date();
                                for (s = 0; s < 5; s++) {
                                    var c = new Date(u.getTime() + 24 * s * 3600 * 1e3),
                                        d = c.getFullYear(),
                                        l = c.getMonth() + 1,
                                        f = c.getDate(),
                                        h = l + "月" + f + "日",
                                        g = d + "-" + l + "-" + f;
                                    o.push(h), r.push(g);
                                }
                                t.arrs = o, t.fallarrs = r, t.today = n;
                            },
                            xuanzd: function (t) {
                                for (var e = this, a = t.currentTarget.dataset.index, n = e.fallarrs[a], i = e.xx, s = 0; s < i.length; s++) i[s].val = 1;
                                e.xuanz = a, e.today = n, e.lixuanz = -1, e.xx = i, e.gettoday(n);
                            },
                            goux: function (t) {
                                var e = t.currentTarget.dataset.index;
                                this.lixuanz = e;
                            },
                            gettoday: function (t) {
                                var i = this,
                                    e = i.id,
                                    a = i.formindex,
                                    s = i.xx;
                                l.request({
                                    url: i.$baseurl + "doPageDuzhan",
                                    data: {
                                        id: e,
                                        types: "showArt",
                                        days: t,
                                        pagedatekey: a,
                                        uniacid: i.$uniacid
                                    },
                                    header: {
                                        "content-type": "application/json"
                                    },
                                    success: function (t) {
                                        for (var e = t.data.data, a = 0; a < e.length; a++) s[e[a]].val = 2;
                                        var n = 0;
                                        e.length == s.length && (n = 1), i.xx = s, i.isover = n;
                                    }
                                });
                            },
                            save_nb: function () {
                                var t = this,
                                    e = t.today,
                                    a = t.xx,
                                    n = t.lixuanz;
                                if (-1 == n) return l.showModal({
                                    title: "提现",
                                    content: "请选择预约的选项",
                                    showCancel: !1
                                }), !1;
                                var i = "已选择" + e + "，" + a[n].keys.yval,
                                    s = t.pagedata,
                                    o = t.formindex;
                                s[o].val = i, s[o].days = e, s[o].indexkey = o, s[o].xuanx = n, t.ttcxs = 0, t.pagedata = s;
                            },
                            quxiao: function () {
                                this.ttcxs = 0;
                            },
                            scans: function () {
                                var n = this,
                                    i = n.pagedata;
                                l.scanCode({
                                    success: function (t) {
                                        for (var e = t.result, a = 0; a < i.length; a++) 0 == i[a].type && 6 == i[a].tp_text[0].yval && (i[a].val = e);
                                        n.scan_res = e, n.pagedata = i;
                                    }
                                });
                            },
                            scan_input: function (t) {
                                for (var e = this.pagedata, a = t.detail.value, n = 0; n < e.length; n++) 0 == e[n].type && 6 == e[n].tp_text[0].yval && (e[n].val = a);
                                this.scan_res = a, this.pagedata = e;
                            },
                            formSubmit: function (t) {
                                var e = this,
                                    a = e.aid;
                                if ("" == e.username) return l.showModal({
                                    title: "提示",
                                    content: "姓名为必填项！",
                                    showCancel: !1
                                }), !1;
                                var n = e.tel;
                                if ("" == n) return l.showModal({
                                    title: "提示",
                                    content: "手机号为必填项！",
                                    showCancel: !1
                                }), !1;
                                if (!/^1[3456789]{1}\d{9}$/.test(n)) return l.showModal({
                                    title: "提醒",
                                    content: "请您输入正确的手机号码",
                                    showCancel: !1
                                }), !1;
                                for (var i = !0, s = e.pagedata, o = 0; o < s.length; o++)
                                    if (1 == s[o].ismust)
                                        if (5 == s[o].type) {
                                            if ("" == s[o].z_val) return i = !1, l.showModal({
                                                title: "提醒",
                                                content: s[o].name + "为必填项！",
                                                showCancel: !1
                                            }), !1;
                                        } else if ("" == s[o].val) return i = !1, l.showModal({
                                    title: "提醒",
                                    content: s[o].name + "为必填项！",
                                    showCancel: !1
                                }), !1;
                                i && l.request({
                                    url: e.$baseurl + "doPageActiveApplySub",
                                    data: {
                                        id: a,
                                        pagedata: JSON.stringify(s),
                                        uniacid: e.$uniacid,
                                        aid: e.aid,
                                        suid: l.getStorageSync("suid"),
                                        source: e.source,
                                        formset: e.formset,
                                        username: e.username,
                                        tel: e.tel
                                    },
                                    cachetime: "30",
                                    success: function (t) {
                                        1 == t.data ? l.showModal({
                                            title: "提示",
                                            content: "已提交成功",
                                            showCancel: !1,
                                            success: function (t) {
                                                l.redirectTo({
                                                    url: "/pagesActive/apply_collect/apply_collect"
                                                });
                                            }
                                        }) : l.showModal({
                                            title: "提示",
                                            content: "提交失败，请重新提交",
                                            showCancel: !1,
                                            success: function (t) {}
                                        });
                                    }
                                });
                            },
                            refreshSessionkey: function () {
                                var e = this;
                                l.login({
                                    success: function (t) {
                                        l.request({
                                            url: e.$baseurl + "doPagegetNewSessionkey",
                                            data: {
                                                uniacid: e.$uniacid,
                                                code: t.code
                                            },
                                            success: function (t) {
                                                e.newSessionKey = t.data.data;
                                            }
                                        });
                                    }
                                });
                            },
                            getPhoneNumber: function (t) {
                                var n = this,
                                    e = t.detail.iv,
                                    a = t.detail.encryptedData;
                                "getPhoneNumber:ok" == t.detail.errMsg ? l.checkSession({
                                    success: function () {
                                        l.request({
                                            url: n.$baseurl + "doPagejiemiNew",
                                            data: {
                                                uniacid: n.$uniacid,
                                                newSessionKey: n.newSessionKey,
                                                iv: e,
                                                encryptedData: a
                                            },
                                            success: function (t) {
                                                if (t.data.data) {
                                                    for (var e = n.pagedata, a = 0; a < e.length; a++) 0 == e[a].type && 5 == e[a].tp_text[0].yval && (e[a].val = t.data.data);
                                                    n.wxmobile = t.data.data, n.pagedata = e;
                                                } else l.showModal({
                                                    title: "提示",
                                                    content: "sessionKey已过期，请下拉刷新！"
                                                });
                                            },
                                            fail: function (t) {}
                                        });
                                    },
                                    fail: function () {
                                        l.showModal({
                                            title: "提示",
                                            content: "sessionKey已过期，请下拉刷新！"
                                        });
                                    }
                                }) : l.showModal({
                                    title: "提示",
                                    content: "请先授权获取您的手机号！",
                                    showCancel: !1
                                });
                            }
                        }
                    };
                e.default = t;
            }).call(this, a("543d").default);
        },
        "35e6": function (t, e, a) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                a("020c"), a("921b"), e(a("66fd")), t(e(a("ce53")).default);
            }).call(this, a("543d").createPage);
        },
        "7fc8": function (t, e, a) {
            a.r(e);
            var n = a("256c"),
                i = a.n(n);
            for (var s in n) "default" !== s && function (t) {
                a.d(e, t, function () {
                    return n[t];
                });
            }(s);
            e.default = i.a;
        },
        "95e1": function (t, e, a) {
            var n = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            a.d(e, "a", function () {
                return n;
            }), a.d(e, "b", function () {
                return i;
            });
        },
        c2f8: function (t, e, a) {},
        ce53: function (t, e, a) {
            a.r(e);
            var n = a("95e1"),
                i = a("7fc8");
            for (var s in i) "default" !== s && function (t) {
                a.d(e, t, function () {
                    return i[t];
                });
            }(s);
            a("d71f");
            var o = a("2877"),
                r = Object(o.a)(i.default, n.a, n.b, !1, null, null, null);
            e.default = r.exports;
        },
        d71f: function (t, e, a) {
            var n = a("c2f8");
            a.n(n).a;
        }
    },
    [
        ["35e6", "common/runtime", "common/vendor"]
    ]
]);