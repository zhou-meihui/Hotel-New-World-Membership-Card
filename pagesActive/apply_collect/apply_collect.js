(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesActive/apply_collect/apply_collect"], {
        "03ab": function (t, e, n) {},
        1666: function (t, e, n) {
            n.r(e);
            var i = n("76bd"),
                a = n.n(i);
            for (var l in i) "default" !== l && function (t) {
                n.d(e, t, function () {
                    return i[t];
                });
            }(l);
            e.default = a.a;
        },
        "220e": function (t, e, n) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                n("020c"), n("921b"), e(n("66fd")), t(e(n("79c1")).default);
            }).call(this, n("543d").createPage);
        },
        "70e2": function (t, e, n) {
            var i = n("03ab");
            n.n(i).a;
        },
        "76bd": function (t, e, a) {
            (function (n) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var i = a("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                baseinfo: "",
                                list: [],
                                type: 1,
                                list_length: 0
                            };
                        },
                        onPullDownRefresh: function () {
                            this.myActiveApplyCollect(), this.getTFlag(), n.stopPullDownRefresh();
                        },
                        onLoad: function (t) {
                            var e = this;
                            this._baseMin(this);
                            var n = 0;
                            t.fxsid && (n = t.fxsid, this.fxsid = t.fxsid), i.getOpenid(n, function () {
                                e.myActiveApplyCollect(), e.getTFlag();
                            }, function () {
                                e.needAuth = !0;
                            }, function () {
                                e.needBind = !0;
                            });
                        },
                        methods: {
                            changeTab: function (t) {
                                var e = this,
                                    n = t.currentTarget.dataset.id;
                                e.type = n, e.myActiveApplyCollect(), e.getTFlag();
                            },
                            myActiveApplyCollect: function () {
                                var e = this;
                                n.request({
                                    url: e.$baseurl + "doPageMyApplyCollect",
                                    data: {
                                        uniacid: e.$uniacid,
                                        suid: n.getStorageSync("suid"),
                                        type: e.type
                                    },
                                    success: function (t) {
                                        e.list = t.data.data, e.list_length = e.list.length;
                                    }
                                });
                            },
                            getTFlag: function () {
                                for (var t = this, e = t.list, n = 0; n < e.length; n++) {
                                    var i = new Date().getTime();
                                    4 == e[n].t_flag || (0 == e[n].endtime ? e[n].t_flag = 3 : 1e3 * e[n].starttime > i ? e[n].t_flag = 1 : 1e3 * e[n].endtime < i && (e[n].t_flag = 2));
                                }
                                t.list = e, setTimeout(function () {
                                    t.getTFlag();
                                }, 1e3);
                            },
                            goactiveinfo: function (t) {
                                var e = t.currentTarget.dataset.id;
                                n.navigateTo({
                                    url: "/pagesActive/active_info/active_info?aid=" + e
                                });
                            }
                        }
                    };
                e.default = t;
            }).call(this, a("543d").default);
        },
        "79c1": function (t, e, n) {
            n.r(e);
            var i = n("abf6"),
                a = n("1666");
            for (var l in a) "default" !== l && function (t) {
                n.d(e, t, function () {
                    return a[t];
                });
            }(l);
            n("70e2");
            var u = n("2877"),
                c = Object(u.a)(a.default, i.a, i.b, !1, null, null, null);
            e.default = c.exports;
        },
        abf6: function (t, e, n) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                a = [];
            n.d(e, "a", function () {
                return i;
            }), n.d(e, "b", function () {
                return a;
            });
        }
    },
    [
        ["220e", "common/runtime", "common/vendor"]
    ]
]);