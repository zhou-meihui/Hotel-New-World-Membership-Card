(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesActive/index/index"], {
        "203b": function (t, i, c) {
            (function (a) {
                function e(t, e, i) {
                    return e in t ? Object.defineProperty(t, e, {
                        value: i,
                        enumerable: !0,
                        configurable: !0,
                        writable: !0
                    }) : t[e] = i, t;
                }
                Object.defineProperty(i, "__esModule", {
                    value: !0
                }), i.default = void 0;
                var n = c("f571"),
                    t = {
                        data: function () {
                            var t;
                            return e(t = {
                                    $imgurl: this.$imgurl,
                                    $host: this.$host,
                                    searchtitle: "",
                                    page: 1,
                                    cateid: 0,
                                    catelist: [],
                                    activeList: ""
                                }, "searchtitle", ""), e(t, "baseinfo", ""), e(t, "page_signs", "pagesActive/index/index"),
                                e(t, "arr", []), e(t, "activeList_length", 0), t;
                        },
                        onPullDownRefresh: function () {
                            this.page = 1, this.activeLists(), a.stopPullDownRefresh();
                        },
                        onLoad: function (t) {
                            var e = this,
                                i = 0;
                            t.fxsid && (i = t.fxsid, this.fxsid = t.fxsid), this._baseMin(this), a.getStorageSync("suid"),
                                n.getOpenid(i, function () {
                                    e.activeLists();
                                }, function () {
                                    e.needAuth = !0;
                                }, function () {
                                    e.needBind = !0;
                                });
                        },
                        onReachBottom: function () {
                            var e = this,
                                t = e.cateid,
                                i = e.page + 1;
                            a.request({
                                url: e.$baseurl + "doPageActiveList",
                                data: {
                                    uniacid: e.$uniacid,
                                    page: i,
                                    cateid: t,
                                    searchtitle: e.searchtitle
                                },
                                success: function (t) {
                                    0 < t.data.data.list.length && (e.catelist = t.data.data.catelist, e.activeList = e.activeList.concat(t.data.data.list),
                                        e.page = i), e.daojishi();
                                }
                            });
                        },
                        methods: {
                            changeTab: function (t) {
                                var e = t.currentTarget.dataset.id;
                                this.cateid = e, this.activeLists();
                            },
                            redirectto: function (t) {
                                var e = t.currentTarget.dataset.link,
                                    i = t.currentTarget.dataset.linktype;
                                app.redirectto(e, i);
                            },
                            activeLists: function (t) {
                                var e = this,
                                    i = e.cateid;
                                a.request({
                                    url: e.$baseurl + "doPageActiveList",
                                    data: {
                                        uniacid: e.$uniacid,
                                        page: e.page,
                                        cateid: i,
                                        searchtitle: e.searchtitle
                                    },
                                    success: function (t) {
                                        e.catelist = t.data.data.catelist, e.activeList = t.data.data.list, e.activeList_length = e.activeList.length,
                                            e.daojishi();
                                    }
                                });
                            },
                            daojishi: function () {
                                for (var t = this, e = t.activeList, i = [], a = 0; a < e.length; a++) {
                                    var n = new Date().getTime();
                                    if (4 == e[a].t_flag);
                                    else if (0 == e[a].endtime) e[a].t_flag = 3;
                                    else if (1e3 * e[a].starttime > n) e[a].t_flag = 1;
                                    else if (1e3 * e[a].endtime < n) e[a].t_flag = 2;
                                    else if (e[a].endtime <= 0) e[a].active_endtime = 0;
                                    else {
                                        var c, s, o, r, d = 1e3 * parseInt(e[a].endtime) - n;
                                        0 <= d && (c = Math.floor(d / 1e3 / 60 / 60 / 24), s = Math.floor(d / 1e3 / 60 / 60 % 24) < 10 ? "0" + Math.floor(d / 1e3 / 60 / 60 % 24) : Math.floor(d / 1e3 / 60 / 60 % 24),
                                                o = Math.floor(d / 1e3 / 60 % 60) < 10 ? "0" + Math.floor(d / 1e3 / 60 % 60) : Math.floor(d / 1e3 / 60 % 60),
                                                r = Math.floor(d / 1e3 % 60) < 10 ? "0" + Math.floor(d / 1e3 % 60) : Math.floor(d / 1e3 % 60)),
                                            0 < c ? (e[a].active_endtime = s + ":" + o + ":" + r, e[a].active_enddate = c + "天",
                                                e[a].active_endh = s, e[a].active_endm = o, e[a].active_ends = r, i[a] = c + "天" + s + ":" + o + ":" + r) : (e[a].active_endtime = s + ":" + o + ":" + r,
                                                e[a].active_endh = s, e[a].active_endm = o, e[a].active_ends = r, i[a] = s + ":" + o + ":" + r);
                                    }
                                }
                                t.arr = i, t.activeList = e, setTimeout(function () {
                                    t.daojishi();
                                }, 1e3);
                            },
                            serachInput: function (t) {
                                this.searchtitle = t.detail.value;
                            },
                            search: function () {
                                this.searchtitle ? this.activeLists() : a.showModal({
                                    title: "提示",
                                    content: "请输入搜索内容！",
                                    showCancel: !1
                                });
                            },
                            goactiveinfo: function (t) {
                                var e = t.currentTarget.dataset.id;
                                a.navigateTo({
                                    url: "/pagesActive/active_info/active_info?aid=" + e
                                });
                            }
                        }
                    };
                i.default = t;
            }).call(this, c("543d").default);
        },
        2143: function (t, e, i) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                i("020c"), i("921b"), e(i("66fd")), t(e(i("30b9")).default);
            }).call(this, i("543d").createPage);
        },
        "21c3": function (t, e, i) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                n = [];
            i.d(e, "a", function () {
                return a;
            }), i.d(e, "b", function () {
                return n;
            });
        },
        "30b9": function (t, e, i) {
            i.r(e);
            var a = i("21c3"),
                n = i("82f5");
            for (var c in n) "default" !== c && function (t) {
                i.d(e, t, function () {
                    return n[t];
                });
            }(c);
            i("b5b9");
            var s = i("2877"),
                o = Object(s.a)(n.default, a.a, a.b, !1, null, null, null);
            e.default = o.exports;
        },
        "82f5": function (t, e, i) {
            i.r(e);
            var a = i("203b"),
                n = i.n(a);
            for (var c in a) "default" !== c && function (t) {
                i.d(e, t, function () {
                    return a[t];
                });
            }(c);
            e.default = n.a;
        },
        b5b9: function (t, e, i) {
            var a = i("c387");
            i.n(a).a;
        },
        c387: function (t, e, i) {}
    },
    [
        ["2143", "common/runtime", "common/vendor"]
    ]
]);