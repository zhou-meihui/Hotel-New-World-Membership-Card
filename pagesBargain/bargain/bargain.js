(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesBargain/bargain/bargain"], {
        3577: function (a, e, n) {
            (function (i) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var t = n("f571"),
                    a = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                swiperCurrent: 0,
                                indicatorDots: !0,
                                autoplay: !0,
                                interval: 3e3,
                                duration: 800,
                                circular: !0,
                                cates: "",
                                cid: "",
                                list: [],
                                searchKey: "",
                                page: 1,
                                addmore: 1,
                                baseinfo: [],
                                banners: [],
                                bannerClose: 0
                            };
                        },
                        onPullDownRefresh: function () {
                            var a = this;
                            a.page = 1, a.searchKey = "", a.bargainList(), i.stopPullDownRefresh();
                        },
                        onLoad: function (a) {
                            var e = this;
                            i.setNavigationBarTitle({
                                title: "砍价会场"
                            });
                            var n = 0;
                            a.fxsid && (n = a.fxsid, this.fxsid = a.fxsid), this._baseMin(this), t.getOpenid(n, function () {
                                e.bargainList();
                            }, function () {
                                e.needAuth = !0;
                            }, function () {
                                e.needBind = !0;
                            });
                        },
                        methods: {
                            onPageScroll: function (a) {
                                8 < a.scrollTop ? this.bannerClose = 1 : this.bannerClose = 0;
                            },
                            bargainList: function () {
                                var e = this,
                                    a = e.searchKey;
                                i.request({
                                    url: e.$baseurl + "doPageBargainList",
                                    data: {
                                        uniacid: e.$uniacid,
                                        openid: e.openid,
                                        searchKey: a,
                                        cate_id: e.cid,
                                        page: e.page,
                                        addmore: 1,
                                        source: i.getStorageSync("source")
                                    },
                                    success: function (a) {
                                        e.list = a.data.data.list, e.banners = a.data.data.banners, e.cates = a.data.data.cates;
                                    }
                                });
                            },
                            addmores: function () {
                                var e = this,
                                    n = e.page + 1,
                                    a = e.searchKey;
                                i.request({
                                    url: e.$baseurl + "doPageBargainList",
                                    data: {
                                        uniacid: e.$uniacid,
                                        openid: e.openid,
                                        searchKey: a,
                                        cate_id: e.cid,
                                        page: n,
                                        source: i.getStorageSync("source")
                                    },
                                    success: function (a) {
                                        a.data.data.list.length < 6 && (e.addmore = 0), e.page = n, e.banners = a.data.data.banners,
                                            e.list = e.list.concat(a.data.data.list);
                                    }
                                });
                            },
                            getSearchKey: function (a) {
                                var e = this,
                                    n = a.detail.value;
                                e.cid, e.page = 1, e.searchKey = n, e.bargainList();
                            },
                            goMybargain: function () {
                                i.navigateTo({
                                    url: "/pagesBargain/my_bargain/my_bargain"
                                });
                            },
                            goPage: function (a) {
                                var e = a.currentTarget.dataset.id;
                                i.navigateTo({
                                    url: "/pagesBargain/bargain_pro/bargain_pro?id=" + e
                                });
                            },
                            handleTap: function (a) {
                                var n = a.currentTarget.dataset.id,
                                    t = this;
                                i.request({
                                    url: t.$baseurl + "doPageBargainList",
                                    data: {
                                        uniacid: t.$uniacid,
                                        cate_id: n
                                    },
                                    success: function (a) {
                                        var e = a.data.data;
                                        t.list = e.list, t.cates = e.cates, t.cid = n;
                                    }
                                });
                            }
                        }
                    };
                e.default = a;
            }).call(this, n("543d").default);
        },
        4388: function (a, e, n) {
            n.r(e);
            var t = n("4b62"),
                i = n("e20e");
            for (var r in i) "default" !== r && function (a) {
                n.d(e, a, function () {
                    return i[a];
                });
            }(r);
            n("ae31");
            var s = n("2877"),
                c = Object(s.a)(i.default, t.a, t.b, !1, null, null, null);
            e.default = c.exports;
        },
        "4b62": function (a, e, n) {
            var t = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            n.d(e, "a", function () {
                return t;
            }), n.d(e, "b", function () {
                return i;
            });
        },
        "66ea": function (a, e, n) {},
        9715: function (a, e, n) {
            (function (a) {
                function e(a) {
                    return a && a.__esModule ? a : {
                        default: a
                    };
                }
                n("020c"), n("921b"), e(n("66fd")), a(e(n("4388")).default);
            }).call(this, n("543d").createPage);
        },
        ae31: function (a, e, n) {
            var t = n("66ea");
            n.n(t).a;
        },
        e20e: function (a, e, n) {
            n.r(e);
            var t = n("3577"),
                i = n.n(t);
            for (var r in t) "default" !== r && function (a) {
                n.d(e, a, function () {
                    return t[a];
                });
            }(r);
            e.default = i.a;
        }
    },
    [
        ["9715", "common/runtime", "common/vendor"]
    ]
]);