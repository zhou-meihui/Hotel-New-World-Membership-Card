(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesBargain/bargain_activity/bargain_activity"], {
        1901: function (n, e, i) {
            var t = i("b678");
            i.n(t).a;
        },
        "2f03": function (n, e, i) {
            var t = function () {
                    this.$createElement;
                    this._self._c;
                },
                a = [];
            i.d(e, "a", function () {
                return t;
            }), i.d(e, "b", function () {
                return a;
            });
        },
        4646: function (n, e, i) {
            i.r(e);
            var t = i("c75b"),
                a = i.n(t);
            for (var r in t) "default" !== r && function (n) {
                i.d(e, n, function () {
                    return t[n];
                });
            }(r);
            e.default = a.a;
        },
        "815d": function (n, e, i) {
            i.r(e);
            var t = i("2f03"),
                a = i("4646");
            for (var r in a) "default" !== r && function (n) {
                i.d(e, n, function () {
                    return a[n];
                });
            }(r);
            i("1901");
            var o = i("2877"),
                s = Object(o.a)(a.default, t.a, t.b, !1, null, null, null);
            e.default = s.exports;
        },
        b2c2: function (n, e, i) {
            (function (n) {
                function e(n) {
                    return n && n.__esModule ? n : {
                        default: n
                    };
                }
                i("020c"), i("921b"), e(i("66fd")), n(e(i("815d")).default);
            }).call(this, i("543d").createPage);
        },
        b678: function (n, e, i) {},
        c75b: function (n, i, a) {
            (function (r) {
                Object.defineProperty(i, "__esModule", {
                    value: !0
                }), i.default = void 0;
                var n, o = (n = a("3584")) && n.__esModule ? n : {
                    default: n
                };
                var t = a("f571"),
                    e = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                close: 0,
                                bargain_id: 0,
                                my_receive_flag: 1,
                                userid: "",
                                proInfo: [],
                                countDown: "",
                                countDown_d: "",
                                countDown_h: "",
                                countDown_m: "",
                                countDown_s: "",
                                baseinfo: [],
                                suid: 0,
                                rules: "",
                                needAuth: !1,
                                needBind: !1,
                                windowH: 0,
                                alreadyPrice: 0,
                                restPrice: 0
                            };
                        },
                        onPullDownRefresh: function () {
                            this.proId, this.bargain_id, this.bargainInfo(), r.stopPullDownRefresh();
                        },
                        onLoad: function (n) {
                            var e = this;
                            r.setNavigationBarTitle({
                                title: "砍价活动页"
                            }), this.suid = r.getStorageSync("suid"), this.suid, n.id && (this.proId = n.id);
                            var i = 0;
                            n.fxsid && (i = n.fxsid, r.setStorageSync("fxsid", i), this.fxsid = n.fxsid), n.userid && (this.userid = n.userid),
                                n.bargain_id && (this.bargain_id = n.bargain_id), this._baseMin(this), t.getOpenid(i, function () {
                                    e.bargainInfo();
                                }, function () {
                                    e.needAuth = !0;
                                }, function () {
                                    e.needBind = !0;
                                });
                        },
                        onShareAppMessage: function () {
                            var n, e = (r.getStorageSync("openid"), this.suid),
                                i = this.proInfo;
                            return n = "/pagesBargain/bargain_activity/bargain_activity?id=" + this.proId + "&fxsid=" + e + "&userid=" + e + "&bargain_id=" + this.bargain_id, {
                                title: i.shareTitle ? i.shareTitle : i.title,
                                path: n
                            };
                        },
                        methods: {
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                console.log("closeBind"), this.needBind = !1, this.bargainInfo();
                            },
                            closes: function () {
                                this.close = 0;
                            },
                            look_regular: function () {
                                this.close = 1;
                            },
                            bargainInfo: function () {
                                var a = this;
                                r.request({
                                    url: a.$baseurl + "doPageBargainInfo",
                                    data: {
                                        uniacid: a.$uniacid,
                                        proId: a.proId,
                                        openid: a.openid,
                                        suid: r.getStorageSync("suid"),
                                        userid: a.userid,
                                        bargain_id: a.bargain_id,
                                        source: r.getStorageSync("source")
                                    },
                                    success: function (n) {
                                        var e = r.getStorageSync("systemInfo");
                                        a.windowH = e.windowHeight, 1 == n.data.data.errcode ? r.showModal({
                                            title: "提示",
                                            content: "商品已下架、不存在、已删除或库存不足",
                                            showCancel: !1,
                                            success: function () {
                                                return r.reLaunch({
                                                    url: "/pages/index/index"
                                                }), !1;
                                            }
                                        }) : 2 == n.data.data.errcode ? r.showModal({
                                            title: "提示",
                                            content: "商品砍价管理规则未设置，无法砍价",
                                            showCancel: !1,
                                            success: function () {
                                                return r.reLaunch({
                                                    url: "/pages/index/index"
                                                }), !1;
                                            }
                                        }) : 3 == n.data.data.errcode ? a.userid == a.openid ? r.showModal({
                                            title: "提示",
                                            content: "该商品已下单，点击跳转订单列表页",
                                            showCancel: !1,
                                            success: function () {
                                                return r.navigateBack({
                                                    delta: 9
                                                }), r.navigateTo({
                                                    url: "/pagesBargain/orderlist/orderlist"
                                                }), !1;
                                            }
                                        }) : r.showModal({
                                            title: "提示",
                                            content: "该商品已下单，发起我的砍价",
                                            showCancel: !1,
                                            success: function () {
                                                return r.navigateBack({
                                                    delta: 9
                                                }), r.navigateTo({
                                                    url: "/pagesBargain/bargain/bargain"
                                                }), !1;
                                            }
                                        }) : 4 == n.data.data.errcode && r.showModal({
                                            title: "提示",
                                            content: n.data.data.errmsg,
                                            showCancel: !1,
                                            success: function () {}
                                        });
                                        var i = n.data.data;
                                        i.nowPrice = parseFloat(i.nowPrice).toFixed(2);
                                        var t = 1e3 * i.overtime - Date.parse(new Date());
                                        a.my_receive_flag = i.my_receive.flag, a.proInfo = i, a.alreadyPrice = (a.proInfo.price - a.proInfo.nowPrice).toFixed(2),
                                            a.restPrice = (a.proInfo.price - a.proInfo.miniPrice - a.alreadyPrice).toFixed(2),
                                            a.bargain_id = n.data.data.id, a.rules = i.rules, a.rules && (a.rules = a.rules.replace(/\<img/gi, '<img style="width:100%;height:auto;display:block" '),
                                                a.rules = (0, o.default)(a.rules)), a.activeEndTime = t, setInterval(function () {
                                                a.countDowns();
                                            }, 1e3);
                                    }
                                });
                            },
                            countDowns: function () {
                                var n = this.activeEndTime;
                                n <= 0 && (n = 0);
                                var e = this.getFormat(n);
                                n -= 1e3;
                                var i = "".concat(e.dd, ":").concat(e.hh, ":").concat(e.mm, ":").concat(e.ss),
                                    t = "".concat(e.dd),
                                    a = "".concat(e.hh),
                                    r = "".concat(e.mm),
                                    o = "".concat(e.ss);
                                this.activeEndTime = n, this.countDown = i, this.countDown_d = t, this.countDown_h = a,
                                    this.countDown_m = r, this.countDown_s = o;
                            },
                            getFormat: function (n) {
                                var e = parseInt(n / 1e3),
                                    i = 0,
                                    t = 0,
                                    a = 0;
                                return 60 < e && (i = parseInt(e / 60), e = parseInt(e % 60), 60 < i && (t = parseInt(i / 60),
                                    i = parseInt(i % 60), 24 < t && (a = parseInt(t / 24), t = parseInt(t % 24)))), {
                                    ss: e = 9 < e ? e : "0".concat(e),
                                    mm: i = 9 < i ? i : "0".concat(i),
                                    hh: t = 9 < t ? t : "0".concat(t),
                                    dd: a = 9 < a ? a : "0".concat(a)
                                };
                            },
                            goOrder: function () {
                                r.redirectTo({
                                    url: "/pagesBargain/bargain_order/bargain_order?id=" + this.proId + "&bargain_id=" + this.bargain_id
                                });
                            },
                            changeReceiveFlag: function (n) {
                                var e = this,
                                    i = n.currentTarget.dataset.receive_id;
                                r.request({
                                    url: e.$baseurl + "doPagechangeReceiveFlag",
                                    data: {
                                        uniacid: e.$uniacid,
                                        receive_id: i
                                    },
                                    success: function (n) {
                                        e.my_receive_flag = 1;
                                    }
                                });
                            },
                            golist: function () {
                                r.navigateTo({
                                    url: "/pagesBargain/bargain/bargain"
                                });
                            },
                            h5OnShare: function () {
                                var e = this;
                                r.showModal({
                                    title: "长按复制链接后分享",
                                    content: this.$host + "/h5/index.html?id=" + this.$uniacid + "#/pagesBargain/bargain_activity/bargain_activity?id=" + this.proId + "&fxsid=" + this.suid + "&userid=" + this.suid,
                                    showCancel: !1,
                                    success: function (n) {
                                        e.share = 0;
                                    }
                                });
                            }
                        }
                    };
                i.default = e;
            }).call(this, a("543d").default);
        }
    },
    [
        ["b2c2", "common/runtime", "common/vendor"]
    ]
]);