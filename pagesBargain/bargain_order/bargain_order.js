(global.webpackJsonp = global.webpackJsonp || []).push([ [ "pagesBargain/bargain_order/bargain_order" ], {
    "437a": function(a, e, t) {},
    "4a89": function(a, e, t) {
        t.r(e);
        var i = t("69a5"), n = t("b3b6");
        for (var s in n) "default" !== s && function(a) {
            t.d(e, a, function() {
                return n[a];
            });
        }(s);
        t("7a06");
        var d = t("2877"), o = Object(d.a)(n.default, i.a, i.b, !1, null, null, null);
        e.default = o.exports;
    },
    "69a5": function(a, e, t) {
        var i = function() {
            this.$createElement;
            this._self._c;
        }, n = [];
        t.d(e, "a", function() {
            return i;
        }), t.d(e, "b", function() {
            return n;
        });
    },
    "75c6": function(a, e, t) {
        (function(b) {
            function n(a, e, t) {
                return e in a ? Object.defineProperty(a, e, {
                    value: t,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : a[e] = t, a;
            }
            Object.defineProperty(e, "__esModule", {
                value: !0
            }), e.default = void 0;
            var i = t("f571"), a = {
                data: function() {
                    return {
                        $imgurl: this.$imgurl,
                        again: 0,
                        nav: 1,
                        addressid: 0,
                        pro_city: "",
                        yunfei_price: 0,
                        buyerMsg: "",
                        orderid: "",
                        yfjian: 0,
                        bargain_id: 0,
                        pd_val: [],
                        address: [],
                        m_address: [],
                        m_address_l: 0,
                        baseinfo: [],
                        proInfo: [],
                        pagedata: {
                            tp_text: [],
                            val: []
                        },
                        sfje: 0,
                        zf_money: 0,
                        xx: [],
                        myname: "",
                        mymobile: "",
                        myaddress: "",
                        wxmobile: "",
                        mymoney: 0,
                        yue_order: 0,
                        formdescs: "",
                        ttcxs: 0,
                        arrs: [],
                        xuanz: 0,
                        lixuanz: 0,
                        isover: 0,
                        zf_type: 0,
                        disabled: !1,
                        pay_type: 1,
                        show_pay_type: 0,
                        alipay: 1,
                        wxpay: 1,
                        needAuth: !1,
                        needBind: !1,
                        kuaidi: "",
                        stores: "",
                        current: 0,
                        storeall: [],
                        storeid: "",
                        is_submit: 1,
                        start_year: 2019,
                        end_year: 2060,
                        showPay: 0,
                        choosepayf: 0,
                        shop_id: 0,
                        shop_info: "",
                        self_taking_time: "",
                        default_concat: "",
                        mymoney_pay: 1,
                        h5_wxpay: 0,
                        h5_alipay: 0
                    };
                },
                components: {
                    datetime: function() {
                        return t.e("components/datetime/datetime").then(t.bind(null, "9dfc"));
                    }
                },
                onLoad: function(a) {
                    var e = this, t = this;
                    a.id && (t.id = a.id), a.bargain_id && (t.bargain_id = a.bargain_id), a.nav && (t.nav = a.nav, 
                    t.addressid = 0), a.shop_id && (t.shop_id = a.shop_id), a.addressid && (t.addressid = a.addressid), 
                    "" != a.orderid && "undefined" != a.orderid && (t.orderid = a.orderid);
                    a.fxsid && (t.fxsid = a.fxsid), t.refreshSessionkey(), this._baseMin(this), i.getOpenid(0, function() {
                        t.checkFreePackage();
                    }, function() {
                        e.needAuth = !0;
                    }, function() {
                        e.needBind = !0;
                    });
                },
                methods: {
                    chooseStore: function() {
                        b.navigateTo({
                            url: "/pages/chooseStore/chooseStore?gid=" + this.id + "&addressid=" + this.addressid + "&orderid=" + this.orderid + "&shop_id=" + this.shop_id + "&type=bargain"
                        });
                    },
                    concat_input: function(a) {
                        this.default_concat = a.detail.value;
                    },
                    openDatetimePicker: function(a) {
                        this.$refs.myPicker.show();
                    },
                    closeDatetimePicker: function() {
                        this.$refs.myPicker.hide();
                    },
                    handleSubmit: function(a) {
                        this.self_taking_time = "".concat(a.year, "-").concat(a.month, "-").concat(a.day, " ").concat(a.hour, ":").concat(a.minute);
                    },
                    paybox: function() {
                        var a = this.nav;
                        if (console.log("nav=" + a), 2 == a) {
                            if (!this.shop_info) return b.showModal({
                                title: "提示",
                                content: "请先选择门店",
                                showCancel: !1
                            }), !1;
                            if (!this.self_taking_time) return b.showModal({
                                title: "提示",
                                content: "请先选择预约取件时间",
                                showCancel: !1
                            }), !1;
                            var e = this.default_concat;
                            if (!e) return b.showModal({
                                title: "提示",
                                content: "请先输入预留电话",
                                showCancel: !1
                            }), !1;
                            if (!/^1[3456789]{1}\d{9}$/.test(e)) return b.showModal({
                                title: "提醒",
                                content: "请您输入正确的手机号码",
                                showCancel: !1
                            }), !1;
                        } else {
                            if (!this.addressid) return b.showModal({
                                title: "提示",
                                content: "请先选择收件地址",
                                showCancel: !1
                            }), !1;
                        }
                        this.mymoney < this.zf_money && (this.mymoney_pay = 2, this.pay_type = 2, this.zf_type = 1, 
                        this.choosepayf = 1), 0 == this.showPay && (this.showPay = 1);
                    },
                    payboxclose: function() {
                        1 == this.showPay && (this.showPay = 0);
                    },
                    choosepay: function(a) {
                        this.zf_type = 1 == a.currentTarget.dataset.pay_type ? 0 : 1, this.choosepayf = a.currentTarget.dataset.type;
                    },
                    checkFreePackage: function() {
                        var e = this;
                        0 == e.again && b.request({
                            url: e.$baseurl + "doPagecheckFreePackage",
                            data: {
                                uniacid: e.$uniacid,
                                suid: b.getStorageSync("suid")
                            },
                            success: function(a) {
                                e.free_package = a.data.data;
                            }
                        }), e.bargainProInfo();
                    },
                    closeAuth: function() {
                        console.log("closeAuth"), this.needAuth = !1, this._checkBindPhone(this), this.globaluserinfo();
                    },
                    closeBind: function() {
                        console.log("closeBind"), this.needBind = !1, this.checkFreePackage();
                    },
                    bargainProInfo: function() {
                        var t = this;
                        b.request({
                            url: t.$baseurl + "doPageBargainOrder",
                            data: {
                                uniacid: t.$uniacid,
                                id: t.id,
                                openid: t.openid,
                                suid: b.getStorageSync("suid")
                            },
                            success: function(a) {
                                if (console.log(a.data.data.is_bargained), 0 < a.data.data.is_bargained) return b.showModal({
                                    title: "提示",
                                    content: "该商品还有下单未支付订单，请前往支付",
                                    showCancel: !1,
                                    success: function(a) {
                                        b.redirectTo({
                                            url: "/pagesBargain/orderlist/orderlist"
                                        });
                                    }
                                }), !1;
                                if (b.setNavigationBarTitle({
                                    title: a.data.data.title
                                }), 0 < a.data.data.form_id && t.getFormInfo(a.data.data.form_id), console.log(a.data.data), 
                                t.proInfo = a.data.data, t.mymoney = parseFloat(a.data.data.mymoney), t.kuaidi = a.data.data.kuaidi, 
                                2 != t.nav && (1 == t.kuaidi ? t.nav = 1 : 2 == t.kuaidi ? t.nav = 2 : 3 == t.kuaidi && (t.nav = 1)), 
                                1 == t.nav) {
                                    var e = t.addressid;
                                    e ? t.getmraddresszd(e) : t.getmraddress();
                                } else t.myContactInfo();
                            }
                        });
                    },
                    myContactInfo: function() {
                        var e = this;
                        b.request({
                            url: e.$host + "/api/MainWxapp/getMyContactInfo",
                            data: {
                                uniacid: e.$uniacid,
                                suid: e.suid,
                                shop_id: e.shop_id,
                                type: "bargain"
                            },
                            success: function(a) {
                                e.shop_info = a.data.data.shop_info, e.getmyinfo();
                            }
                        });
                    },
                    navs: function(a) {
                        var e = this, t = parseInt(a.currentTarget.dataset.value), i = parseInt(a.currentTarget.dataset.value), n = 0, s = e.yunfei_price;
                        1 == i ? n -= s : n = s;
                        var d = Math.round(100 * (e.sfje - n)) / 100;
                        e.nav = i, e.yfjian = 1 == i ? 0 : s, e.sfje = d, e.zf_type = e.mymoney >= d ? 0 : 1, 
                        e.zf_money = e.mymoney >= d ? d : Math.round(100 * (d - e.mymoney)) / 100, e.nav = t, 
                        console.log(a), console.log(e.nav);
                    },
                    getFormInfo: function(a) {
                        var e = this;
                        b.request({
                            url: e.$baseurl + "doPagegetFormInfo",
                            data: {
                                uniacid: e.$uniacid,
                                form_id: a
                            },
                            success: function(a) {
                                e.pagedata = a.data.data.forms;
                            }
                        });
                    },
                    add_address: function() {
                        0 == this.again && b.navigateTo({
                            url: "/pages/address/address?pid=" + this.id + "&addressid=" + this.addressid + "&orderid=" + this.orderid
                        });
                    },
                    getmraddress: function() {
                        var i = this, a = b.getStorageSync("openid"), e = b.getStorageSync("suid");
                        b.request({
                            url: i.$baseurl + "doPagegetmraddress",
                            data: n({
                                uniacid: i.$uniacid,
                                openid: a,
                                suid: b.getStorageSync("suid")
                            }, "suid", e),
                            success: function(a) {
                                var e = a.data.data;
                                if ("" != e && null != e) i.address = e, i.addressid = e.id, i.pro_city = e.pro_city; else {
                                    var t = i.again;
                                    0 == t ? i.address = "" : 1 == t && b.showModal({
                                        title: "提示",
                                        content: "该订单数据错误，请重新下单",
                                        showCancel: !1,
                                        success: function() {
                                            b.redirectTo({
                                                url: "/pages/index/index"
                                            });
                                        }
                                    });
                                }
                                i.getmyinfo();
                            }
                        });
                    },
                    getmraddresszd: function(a) {
                        var i = this, e = b.getStorageSync("openid"), t = b.getStorageSync("suid");
                        b.request({
                            url: i.$baseurl + "doPagegetmraddresszd",
                            data: n({
                                uniacid: i.$uniacid,
                                openid: e,
                                suid: b.getStorageSync("suid"),
                                id: a
                            }, "suid", t),
                            success: function(a) {
                                var e = a.data.data;
                                if ("" != e && null != e) i.address = e, i.pro_city = e.pro_city; else {
                                    var t = i.again;
                                    0 == t ? i.address = "" : 1 == t && 0 == i.m_address_l ? i.address = "" : 1 == t && b.showModal({
                                        title: "提示",
                                        content: "该订单数据错误，请重新下单",
                                        showCancel: !1,
                                        success: function() {
                                            b.redirectTo({
                                                url: "/pages/index/index"
                                            });
                                        }
                                    });
                                }
                                i.getmyinfo();
                            }
                        });
                    },
                    getmyinfo: function() {
                        var i = this, n = (b.getStorageSync("openid"), i.nav), a = i.again;
                        if (0 == a) {
                            var e = i.pro_city, t = i.free_package, s = i.proInfo.nowPrice;
                            "" != e && 2 != i.nav && 0 == t ? b.request({
                                url: i.$baseurl + "doPageyunfeigetnew",
                                data: {
                                    uniacid: i.$uniacid,
                                    id: i.id,
                                    type: "bargain",
                                    hjjg: s,
                                    num: 1,
                                    pro_city: e
                                },
                                success: function(a) {
                                    var e, t = a.data.data;
                                    e = parseFloat(s) >= parseFloat(t.byou) && "" != t.byou ? 0 : t.yfei, s = 2 == n ? Math.round(1 * s * 100) / 100 : Math.round(100 * (1 * s + 1 * e)) / 100, 
                                    i.yunfei_price = e, i.sfje = s, i.zf_type = i.mymoney >= s ? 0 : 1, i.zf_money = i.mymoney >= s ? s : Math.round(100 * (s - i.mymoney)) / 100, 
                                    i.get_yf = t.yfei, i.baoyou = t.byou;
                                }
                            }) : (i.sfje = s, i.zf_type = i.mymoney >= s ? 0 : 1, i.zf_money = i.mymoney >= s ? s : Math.round(100 * (s - i.mymoney)) / 100);
                        } else 1 == a && (s = i.true_price_order, i.sfje = s, i.zf_type = i.mymoney >= s ? 0 : 1, 
                        i.zf_money = i.mymoney >= s ? s : Math.round(100 * (s - i.mymoney)) / 100);
                    },
                    buyerMsgs: function(a) {
                        var e = a.detail.value;
                        this.buyerMsg = e;
                    },
                    submit: i.throttle(function(a) {
                        if (2 == this.is_submit) return !1;
                        var e = this;
                        console.log(a);
                        var t = a.detail.formId, i = e.again;
                        e.formId = t;
                        var n = e.proInfo, s = e.address;
                        if (0 < n.form_id) {
                            if (2 != e.nav && (null == s || !s) && 0 == i) return b.showModal({
                                title: "提示",
                                content: "请先选择/设置收货地址！",
                                showCancel: !0,
                                success: function(a) {
                                    if (!a.confirm) return e.disabled = !1;
                                    b.navigateTo({
                                        url: "/pages/address/address?shareid=" + e.shareid + "&pid=" + e.id + "&orderid=" + e.orderid
                                    });
                                }
                            }), !1;
                            e.formSubmit();
                        } else {
                            if (2 != e.nav && (null == s || !s) && 0 == i) return b.showModal({
                                title: "提示",
                                content: "请先选择/设置收货地址！",
                                showCancel: !0,
                                success: function(a) {
                                    if (a.confirm) b.navigateTo({
                                        url: "/pages/address/address?shareid=" + e.shareid + "&pid=" + e.id + "&orderid=" + e.orderid
                                    }); else if (a.cancel) return e.disabled = !1;
                                }
                            }), !1;
                            e.doshend(0);
                        }
                    }, 2e3),
                    doshend: function(a) {
                        var t = this, e = b.getStorageSync("openid"), i = t.zf_money;
                        if (0 == t.zf_type) var n = i, s = 0; else n = 0, s = i;
                        var d = t.nav, o = t.yunfei_price;
                        o -= t.yfjian;
                        var r = t.address, c = t.buyerMsg, u = t.orderid, l = t.again;
                        if (o = 1 == d ? o : 0, !(2 == d || null != r && r || 0 != l)) return b.showModal({
                            title: "提示",
                            content: "请先选择/设置收货地址！",
                            showCancel: !0,
                            success: function(a) {
                                if (!a.confirm) return t.disabled = !1;
                                b.navigateTo({
                                    url: "/pages/address/address?shareid=" + t.shareid + "&pid=" + t.id + "&orderid=" + t.orderid
                                });
                            }
                        }), !1;
                        var f = r.id;
                        if (0 == t.again) {
                            var h = "", g = "", p = "", y = "";
                            if (2 == t.nav) if ("" == t.storeall) h = t.baseinfo.name, g = t.baseinfo.tel, p = t.baseinfo.address; else for (var m = t.storeid, v = t.storeall, _ = 0; _ < v.length; _++) if (m == v[_].id) {
                                h = v[_].title, g = v[_].tel, p = v[_].province + v[_].city + v[_].country, y = v[_].times;
                                break;
                            }
                        }
                        this.is_submit = 2, b.request({
                            url: t.$baseurl + "doPagecreateorder",
                            header: {
                                "content-type": "application/json"
                            },
                            data: {
                                uniacid: t.$uniacid,
                                pid: t.id,
                                num: 1,
                                types: "bargain",
                                openid: e,
                                suid: b.getStorageSync("suid"),
                                wx_price: s,
                                yue_price: n,
                                true_price: i,
                                addressId: f,
                                buyerMsg: c,
                                nav: d,
                                form_id: a,
                                formId: t.formId,
                                yunfei: o,
                                orderid: u || "",
                                again: t.again,
                                bargain_id: t.bargain_id,
                                source: b.getStorageSync("source"),
                                store_hours: y,
                                store_name: h,
                                store_address: p,
                                store_tel: g,
                                shop_id: t.shop_id,
                                self_taking_time: t.self_taking_time,
                                default_concat: t.default_concat
                            },
                            success: function(a) {
                                if ("1" == a.data.data.errcode) return b.showModal({
                                    title: a.data.data.err,
                                    content: "请重新下单",
                                    showCancel: !1
                                }), t.disabled = !1;
                                if ("2" == a.data.data.errcode) return b.showModal({
                                    title: a.data.data.err,
                                    content: a.data.data.can_buy <= 0 ? "去逛逛其他商品吧~" : "您还可购买" + a.data.data.can_buy + "件",
                                    showCancel: !1
                                }), t.disabled = !1;
                                if ("3" == a.data.data.errcode) return b.showModal({
                                    title: a.data.data.err,
                                    content: "当前库存为" + a.data.data.kc + "件",
                                    showCancel: !1
                                }), t.disabled = !1;
                                if ("4" == a.data.data.errcode) return b.showModal({
                                    title: a.data.data.err,
                                    content: "商品已经下架或库存不足",
                                    showCancel: !1,
                                    success: function() {
                                        b.redirectTo({
                                            url: "/pages/index/index"
                                        });
                                    }
                                }), !1;
                                var e = a.data.data;
                                t.orderid = e, i <= t.mymoney && 0 == t.zf_type ? t.pay1(e) : t.pay2(e);
                            }
                        });
                    },
                    showpay: function() {
                        var e = this;
                        b.request({
                            url: this.$baseurl + "doPageGetH5payshow",
                            data: {
                                uniacid: this.$uniacid,
                                suid: b.getStorageSync("suid")
                            },
                            success: function(a) {
                                0 == a.data.data.ali && 0 == a.data.data.wx ? b.showModal({
                                    title: "提示",
                                    content: "请联系管理员设置支付参数",
                                    success: function(a) {
                                        b.redirectTo({
                                            url: "/pages/index/index"
                                        });
                                    }
                                }) : (0 == a.data.data.ali ? (e.alipay = 0, e.pay_type = 2) : (e.alipay = 1, e.pay_type = 1), 
                                0 == a.data.data.wx ? e.wxpay = 0 : e.wxpay = 1, e.show_pay_type = 1);
                            }
                        });
                    },
                    changealipay: function() {
                        this.pay_type = 1;
                    },
                    changewxpay: function() {
                        this.pay_type = 2;
                    },
                    close_pay_type: function() {
                        this.show_pay_type = 0;
                    },
                    h5topay: function() {
                        var a = this.pay_type;
                        1 == a ? this._alih5pay(this, this.zf_money, 14, this.orderid) : 2 == a && this._wxh5pay(this, this.zf_money, "bargain", this.orderid), 
                        this.show_pay_type = 0;
                    },
                    pay1: function(e) {
                        var t = this;
                        b.showModal({
                            title: "请注意",
                            content: "您将使用余额支付" + t.sfje + "元",
                            success: function(a) {
                                a.confirm && (t.payover_do(e), b.showLoading({
                                    title: "下单中...",
                                    mask: !0
                                })), b.redirectTo({
                                    url: "/pagesBargain/orderlist/orderlist"
                                });
                            }
                        });
                    },
                    pay2: function(e) {
                        var t = this, a = b.getStorageSync("openid");
                        b.request({
                            url: t.$baseurl + "doPagebeforepay",
                            data: {
                                uniacid: t.$uniacid,
                                openid: a,
                                suid: b.getStorageSync("suid"),
                                order_id: e,
                                types: "bargain",
                                formId: t.formId
                            },
                            header: {
                                "content-type": "application/json"
                            },
                            success: function(a) {
                                1 == a.data.data.errs && (b.showModal({
                                    title: "支付失败",
                                    content: a.data.data.return_msg,
                                    showCancel: !1
                                }), t.disabled = !1);
                                -1 != [ 1, 2, 3, 4 ].indexOf(a.data.data.err) && (b.showModal({
                                    title: "支付失败",
                                    content: a.data.data.message,
                                    showCancel: !1
                                }), t.disabled = !1), 0 == a.data.data.err && (b.request({
                                    url: t.$baseurl + "doPagesavePrepayid",
                                    data: {
                                        uniacid: t.$uniacid,
                                        types: "bargain",
                                        order_id: e,
                                        prepayid: a.data.data.package
                                    },
                                    success: function(a) {},
                                    fail: function(a) {}
                                }), b.requestPayment({
                                    timeStamp: a.data.data.timeStamp,
                                    nonceStr: a.data.data.nonceStr,
                                    package: a.data.data.package,
                                    signType: "MD5",
                                    paySign: a.data.data.paySign,
                                    success: function(a) {
                                        b.showToast({
                                            title: "支付成功",
                                            icon: "success",
                                            mask: !0,
                                            duration: 3e3,
                                            success: function(a) {
                                                b.showToast({
                                                    title: "购买成功！",
                                                    icon: "success",
                                                    mask: !0,
                                                    success: function() {
                                                        b.navigateTo({
                                                            url: "/pagesBargain/orderlist/orderlist?type=9"
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    },
                                    fail: function(a) {
                                        b.navigateTo({
                                            url: "/pagesBargain/orderlist/orderlist?type=9"
                                        });
                                    },
                                    complete: function(a) {}
                                }));
                            }
                        });
                    },
                    payover_do: function(a) {
                        var e, t = (e = b.getStorageSync("openid"), this.sfje);
                        b.request({
                            url: this.$baseurl + "doPagepaynotify",
                            data: {
                                uniacid: this.$uniacid,
                                out_trade_no: a,
                                openid: e,
                                suid: b.getStorageSync("suid"),
                                source: b.getStorageSync("source"),
                                payprice: t,
                                types: "bargain",
                                flag: 0,
                                formId: this.formId
                            },
                            success: function(a) {
                                "失败" == a.data.data.message ? b.showToast({
                                    title: "付款失败, 请刷新后重新付款！",
                                    icon: "none",
                                    mask: !0,
                                    success: function() {
                                        b.navigateTo({
                                            url: "/pagesBargain/orderlist/orderlist?type=9"
                                        });
                                    }
                                }) : b.showToast({
                                    title: "购买成功！",
                                    icon: "success",
                                    mask: !0,
                                    success: function() {
                                        b.navigateTo({
                                            url: "/pagesBargain/orderlist/orderlist?type=9"
                                        }), b.hideLoading();
                                    }
                                });
                            }
                        });
                    },
                    makePhoneCallC: function(a) {
                        var e = a.currentTarget.dataset.tel;
                        b.makePhoneCall({
                            phoneNumber: e
                        });
                    },
                    bindInputChange: function(a) {
                        var e = a.detail.value, t = a.currentTarget.dataset.index, i = this.pagedata;
                        i[t].val = e, this.pagedata = i;
                    },
                    bindPickerChange: function(a) {
                        var e = a.detail.value, t = a.currentTarget.dataset.index, i = this.pagedata, n = i[t].tp_text[e];
                        i[t].val = n, this.pagedata = i;
                    },
                    bindDateChange: function(a) {
                        var e = a.detail.value, t = a.currentTarget.dataset.index, i = this.pagedata;
                        i[t].val = e, this.pagedata = i;
                    },
                    bindTimeChange: function(a) {
                        var e = a.detail.value, t = a.currentTarget.dataset.index, i = this.pagedata;
                        i[t].val = e, this.pagedata = i;
                    },
                    checkboxChange: function(a) {
                        var e = a.detail.value, t = a.currentTarget.dataset.index, i = this.pagedata;
                        i[t].val = e, this.pagedata = i;
                    },
                    radioChange: function(a) {
                        var e = a.detail.value, t = a.currentTarget.dataset.index, i = this.pagedata;
                        i[t].val = e, this.pagedata = i;
                    },
                    choiceimg1111: function(a) {
                        var s = this, e = 0, d = s.zhixin, o = a.currentTarget.dataset.index, r = s.pagedata, c = r[o].val, t = r[o].tp_text[0];
                        c ? e = c.length : (e = 0, c = []);
                        var i = t - e, u = s.$baseurl + "wxupimg";
                        s.pd_val, b.chooseImage({
                            count: i,
                            sizeType: [ "original", "compressed" ],
                            sourceType: [ "album", "camera" ],
                            success: function(a) {
                                d = !0, s.zhixin = d, b.showLoading({
                                    title: "图片上传中"
                                });
                                var t = a.tempFilePaths;
                                c = c.concat(t), r[o].val = c, s.pagedata = r;
                                var i = 0, n = t.length;
                                !function e() {
                                    b.uploadFile({
                                        url: u,
                                        formData: {
                                            uniacid: s.$uniacid
                                        },
                                        filePath: t[i],
                                        name: "file",
                                        success: function(a) {
                                            r[o].z_val.push(a.data), s.pagedata = r, ++i < n ? e() : (d = !1, s.zhixin = d, 
                                            b.hideLoading());
                                        }
                                    });
                                }();
                            }
                        });
                    },
                    delimg: function(a) {
                        var e = a.currentTarget.dataset.index, t = a.currentTarget.dataset.id, i = this.pagedata, n = i[e].val;
                        n.splice(t, 1), 0 == n.length && (n = ""), i[e].val = n, this.pagedata = i;
                    },
                    namexz: function(a) {
                        for (var e = this, t = a.currentTarget.dataset.index, i = e.pagedata[t], n = [], s = 0; s < i.tp_text.length; s++) {
                            var d = {};
                            d.keys = i.tp_text[s], d.val = 1, n.push(d);
                        }
                        e.ttcxs = 1, e.formindex = t, e.xx = n, e.xuanz = 0, e.lixuanz = -1, e.riqi();
                    },
                    riqi: function() {
                        for (var a = this, e = new Date(), t = new Date(e.getTime()), i = t.getFullYear() + "-" + (t.getMonth() + 1) + "-" + t.getDate(), n = a.xx, s = 0; s < n.length; s++) n[s].val = 1;
                        a.xx = n, a.gettoday(i);
                        var d = [], o = [], r = new Date();
                        for (s = 0; s < 5; s++) {
                            var c = new Date(r.getTime() + 24 * s * 3600 * 1e3), u = c.getFullYear(), l = c.getMonth() + 1, f = c.getDate(), h = l + "月" + f + "日", g = u + "-" + l + "-" + f;
                            d.push(h), o.push(g);
                        }
                        a.arrs = d, a.fallarrs = o, a.today = i;
                    },
                    xuanzd: function(a) {
                        for (var e = this, t = a.currentTarget.dataset.index, i = e.fallarrs[t], n = e.xx, s = 0; s < n.length; s++) n[s].val = 1;
                        e.xuanz = t, e.today = i, e.lixuanz = -1, e.xx = n, e.gettoday(i);
                    },
                    goux: function(a) {
                        var e = a.currentTarget.dataset.index;
                        this.lixuanz = e;
                    },
                    gettoday: function(a) {
                        var n = this, e = n.id, t = n.formindex, s = n.xx;
                        b.request({
                            url: n.$baseurl + "doPageDuzhan",
                            data: {
                                uniacid: n.$uniacid,
                                id: e,
                                types: "showArt",
                                days: a,
                                pagedatekey: t
                            },
                            header: {
                                "content-type": "application/json"
                            },
                            success: function(a) {
                                for (var e = a.data.data, t = 0; t < e.length; t++) s[e[t]].val = 2;
                                var i = 0;
                                e.length == s.length && (i = 1), n.xx = s, n.isover = i;
                            }
                        });
                    },
                    save_nb: function() {
                        var a = this, e = a.today, t = a.xx, i = a.lixuanz;
                        if (-1 == i) return b.showModal({
                            title: "提现",
                            content: "请选择预约的选项",
                            showCancel: !1
                        }), !1;
                        var n = "已选择" + e + "，" + t[i].keys.yval, s = a.pagedata, d = a.formindex;
                        s[d].val = n, s[d].days = e, s[d].indexkey = d, s[d].xuanx = i, a.ttcxs = 0, a.pagedata = s;
                    },
                    refreshSessionkey: function() {
                        var e = this;
                        b.login({
                            success: function(a) {
                                b.request({
                                    url: e.$baseurl + "doPagegetNewSessionkey",
                                    data: {
                                        uniacid: e.$uniacid,
                                        code: a.code
                                    },
                                    success: function(a) {
                                        e.newSessionKey = a.data.data;
                                    }
                                });
                            }
                        });
                    },
                    getPhoneNumber1: function(a) {
                        var i = this, e = a.detail.iv, t = a.detail.encryptedData;
                        "getPhoneNumber:ok" == a.detail.errMsg ? b.checkSession({
                            success: function() {
                                b.request({
                                    url: i.$baseurl + "doPagejiemiNew",
                                    data: {
                                        uniacid: i.$uniacid,
                                        newSessionKey: i.newSessionKey,
                                        iv: e,
                                        encryptedData: t
                                    },
                                    success: function(a) {
                                        if (a.data.data) {
                                            for (var e = i.pagedata, t = 0; t < e.length; t++) 0 == e[t].type && 5 == e[t].tp_text[0].yval && (e[t].val = a.data.data);
                                            i.wxmobile = a.data.data, i.pagedata = e;
                                        } else b.showModal({
                                            title: "提示",
                                            content: "sessionKey已过期，请下拉刷新！"
                                        });
                                    },
                                    fail: function(a) {}
                                });
                            },
                            fail: function() {
                                b.showModal({
                                    title: "提示",
                                    content: "sessionKey已过期，请下拉刷新！"
                                });
                            }
                        }) : b.showModal({
                            title: "提示",
                            content: "请先授权获取您的手机号！",
                            showCancel: !1
                        });
                    },
                    quxiao: function() {
                        this.ttcxs = 0;
                    },
                    weixinadd: function() {
                        var d = this;
                        b.chooseAddress({
                            success: function(a) {
                                for (var e = a.provinceName + " " + a.cityName + " " + a.countyName + " " + a.detailInfo, t = a.userName, i = a.telNumber, n = d.pagedata, s = 0; s < n.length; s++) 0 == n[s].type && 2 == n[s].tp_text[0].yval && (n[s].val = t), 
                                0 == n[s].type && 3 == n[s].tp_text[0].yval && (n[s].val = i), 0 == n[s].type && 4 == n[s].tp_text[0].yval && (n[s].val = e);
                                d.myname = t, d.mymobile = i, d.myaddress = e, d.pagedata = n;
                            },
                            fail: function(a) {
                                b.getSetting({
                                    success: function(a) {
                                        a.authSetting["scope.address"] || b.openSetting({
                                            success: function(a) {}
                                        });
                                    }
                                });
                            }
                        });
                    },
                    formSubmit: function(a) {
                        for (var t = this, e = t.id, i = !0, n = t.pagedata, s = 0; s < n.length; s++) if (1 == n[s].ismust) if (5 == n[s].type) {
                            if ("" == n[s].z_val) return i = !1, b.showModal({
                                title: "提醒",
                                content: n[s].name + "为必填项！",
                                showCancel: !1
                            }), t.disabled = !1;
                        } else {
                            if ("" == n[s].val) return i = !1, b.showModal({
                                title: "提醒",
                                content: n[s].name + "为必填项！",
                                showCancel: !1
                            }), t.disabled = !1;
                            if (0 == n[s].type && 1 == n[s].tp_text[0].yval) {
                                if (!/^1[3456789]{1}\d{9}$/.test(n[s].val)) return b.showModal({
                                    title: "提醒",
                                    content: "请您输入正确的手机号码",
                                    showCancel: !1
                                }), !1;
                            } else if (0 == n[s].type && 7 == n[s].tp_text[0].yval) {
                                if (!/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test(n[s].val)) return b.showModal({
                                    title: "提醒",
                                    content: "请您输入正确的身份证号",
                                    showCancel: !1
                                }), !1;
                            }
                        }
                        if (this.is_submit = 2, i) {
                            var d = b.getStorageSync("openid");
                            b.request({
                                url: t.$baseurl + "doPageFormval",
                                data: {
                                    uniacid: t.$uniacid,
                                    id: e,
                                    form_id: t.formId,
                                    pagedata: JSON.stringify(n),
                                    fid: t.proInfo.form_id,
                                    openid: d,
                                    suid: b.getStorageSync("suid"),
                                    types: "bargain",
                                    source: b.getStorageSync("source")
                                },
                                success: function(a) {
                                    var e = a.data.data.id;
                                    t.doshend(e);
                                }
                            });
                        }
                    }
                }
            };
            e.default = a;
        }).call(this, t("543d").default);
    },
    "7a06": function(a, e, t) {
        var i = t("437a");
        t.n(i).a;
    },
    b084: function(a, e, t) {
        (function(a) {
            function e(a) {
                return a && a.__esModule ? a : {
                    default: a
                };
            }
            t("020c"), t("921b"), e(t("66fd")), a(e(t("4a89")).default);
        }).call(this, t("543d").createPage);
    },
    b3b6: function(a, e, t) {
        t.r(e);
        var i = t("75c6"), n = t.n(i);
        for (var s in i) "default" !== s && function(a) {
            t.d(e, a, function() {
                return i[a];
            });
        }(s);
        e.default = n.a;
    }
}, [ [ "b084", "common/runtime", "common/vendor" ] ] ]);