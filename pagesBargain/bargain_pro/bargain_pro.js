(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesBargain/bargain_pro/bargain_pro"], {
        "0dbd": function (a, i, e) {
            e.r(i);
            var t = e("eb10"),
                n = e.n(t);
            for (var r in t) "default" !== r && function (a) {
                e.d(i, a, function () {
                    return t[a];
                });
            }(r);
            i.default = n.a;
        },
        "272d": function (a, i, e) {
            (function (a) {
                function i(a) {
                    return a && a.__esModule ? a : {
                        default: a
                    };
                }
                e("020c"), e("921b"), i(e("66fd")), a(i(e("9345")).default);
            }).call(this, e("543d").createPage);
        },
        "7aaf": function (a, i, e) {
            var t = e("d41c");
            e.n(t).a;
        },
        9345: function (a, i, e) {
            e.r(i);
            var t = e("9bbe"),
                n = e("0dbd");
            for (var r in n) "default" !== r && function (a) {
                e.d(i, a, function () {
                    return n[a];
                });
            }(r);
            e("7aaf");
            var s = e("2877"),
                o = Object(s.a)(n.default, t.a, t.b, !1, null, null, null);
            i.default = o.exports;
        },
        "9bbe": function (a, i, e) {
            var t = function () {
                    this.$createElement;
                    this._self._c;
                },
                n = [];
            e.d(i, "a", function () {
                return t;
            }), e.d(i, "b", function () {
                return n;
            });
        },
        d41c: function (a, i, e) {},
        eb10: function (a, i, e) {
            (function (r) {
                Object.defineProperty(i, "__esModule", {
                    value: !0
                }), i.default = void 0;
                var n = e("f571"),
                    a = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                swiperCurrent: 0,
                                indicatorDots: !0,
                                autoplay: !0,
                                interval: 3e3,
                                duration: 800,
                                circular: !0,
                                countDownList: [],
                                id: 0,
                                fxsid: 0,
                                proInfo: {
                                    masterThumb: [],
                                    labels: []
                                },
                                bargain_id: 0,
                                isplay: !1,
                                currentSwiper: 0,
                                minHeight: 0,
                                baseinfo: [],
                                pic_video: "",
                                state: 0,
                                texts: "",
                                bargainCt: [],
                                shareimg: 0,
                                share: 0,
                                shareimg_url: "",
                                system_w: 0,
                                system_h: 0,
                                img_w: 0,
                                img_h: 0,
                                dlength: 0,
                                needAuth: !1,
                                needBind: !1,
                                alreadyPrice: 0,
                                restPrice: 0,
                                fiexdBoxs: 0
                            };
                        },
                        onPullDownRefresh: function () {
                            this.id, this.bargainProInfo(), r.stopPullDownRefresh();
                        },
                        onLoad: function (a) {
                            var i = this;
                            a.id && (i.id = a.id);
                            var e = 0;
                            a.fxsid && (e = a.fxsid, r.setStorageSync("fxsid", e), i.fxsid = a.fxsid), i.checkvip(),
                                this._baseMin(this), r.getStorageSync("suid"), n.getOpenid(e, function () {
                                    i.bargainProInfo();
                                }), i.checkvip();
                            var t = r.getStorageSync("systemInfo");
                            this.img_w = parseInt((.65 * t.windowWidth).toFixed(0)), this.img_h = parseInt((1.875 * this.img_w).toFixed(0)),
                                this.system_w = parseInt(t.windowWidth), this.system_h = parseInt(t.windowHeight);
                        },
                        onShareAppMessage: function () {
                            var a, i = r.getStorageSync("suid"),
                                e = this.proInfo;
                            return a = "/pagesBargain/bargain_pro/bargain_pro?id=" + this.id + "&userid=" + i, {
                                title: e.title,
                                path: a,
                                imageUrl: e.shareThumb
                            };
                        },
                        methods: {
                            navback: function () {
                                r.navigateBack();
                            },
                            toTop: function () {
                                r.pageScrollTo({
                                    scrollTop: 0
                                });
                            },
                            onPageScroll: function (a) {
                                200 < a.scrollTop ? this.fiexdBoxs = 1 : this.fiexdBoxs = 0;
                            },
                            makePhoneCall: function (a) {
                                var i = this.baseinfo.tel;
                                r.makePhoneCall({
                                    phoneNumber: i
                                });
                            },
                            checkvip: function (a) {
                                var i = this,
                                    e = r.getStorageSync("suid");
                                r.request({
                                    url: i.$baseurl + "doPageCheckvip",
                                    data: {
                                        uniacid: i.$uniacid,
                                        kwd: "bargain",
                                        suid: e,
                                        id: i.id,
                                        gz: 1
                                    },
                                    success: function (a) {
                                        if (!1 === a.data.data) return i.needvip = !0, r.showModal({
                                            title: "进入失败",
                                            content: "使用本功能需先开通vip!",
                                            showCancel: !1,
                                            success: function (a) {
                                                a.confirm && r.redirectTo({
                                                    url: "/pages/register/register"
                                                });
                                            }
                                        }), !1;
                                        0 < a.data.data.needgrade && (i.needvip = !0, 0 < a.data.data.grade ? a.data.data.grade < a.data.data.needgrade && r.showModal({
                                            title: "进入失败",
                                            content: "使用本功能需成为" + a.data.data.vipname + "(" + a.data.data.needgrade + "级)以上等级会员,请先升级!",
                                            showCancel: !1,
                                            success: function (a) {
                                                a.confirm && r.redirectTo({
                                                    url: "/pages/open1/open1"
                                                });
                                            }
                                        }) : a.data.data.grade < a.data.data.needgrade && r.showModal({
                                            title: "进入失败",
                                            content: "使用本功能需成为" + a.data.data.vipname + "(" + a.data.data.needgrade + "级)以上等级会员,请先开通会员后再升级会员等级!",
                                            showCancel: !1,
                                            success: function (a) {
                                                a.confirm && r.redirectTo({
                                                    url: "/pages/register/register"
                                                });
                                            }
                                        }));
                                    },
                                    fail: function (a) {}
                                });
                            },
                            bargainProInfo: function () {
                                var t = this;
                                r.request({
                                    url: t.$baseurl + "doPageBargainProInfo",
                                    data: {
                                        uniacid: t.$uniacid,
                                        id: t.id,
                                        openid: t.openid,
                                        suid: r.getStorageSync("suid"),
                                        source: r.getStorageSync("source")
                                    },
                                    success: function (a) {
                                        1 == a.data.data.active_flag && 0 == a.data.data.overtime && r.showModal({
                                            title: "提示",
                                            content: "该商品已下架，请选择其他商品进行砍价",
                                            showCancel: !1,
                                            success: function () {
                                                r.navigateBack({
                                                    deltal: 9
                                                }), r.navigateTo({
                                                    url: "/pagesBargain/bargain/bargain"
                                                });
                                            }
                                        });
                                        var i = a.data.data.activeEndTime,
                                            e = a.data.data.overtime;
                                        t.pic_video = a.data.data.video, t.proInfo = a.data.data, t.texts = a.data.data.texts,
                                            t.texts && (t.texts = t.texts.replace(/\<img/gi, '<img style="width:100%;height:auto;display:block" ')),
                                            t.activeEndTime = i, t.bargain_id = a.data.data.bargain_id, t.bargainEndTime = e,
                                            t.dlength = t.proInfo.masterThumb.length, t.alreadyPrice = (t.proInfo.price - t.proInfo.nowPrice).toFixed(2),
                                            t.restPrice = (t.proInfo.price - t.proInfo.miniPrice - t.alreadyPrice).toFixed(2),
                                            t.countDown(), t.countDown1();
                                    }
                                });
                            },
                            timeFormat: function (a) {
                                return a < 10 ? "0" + a : a;
                            },
                            countDown: function () {
                                var a = new Date().getTime(),
                                    i = 1e3 * this.activeEndTime,
                                    e = [],
                                    t = null;
                                if (0 < i - a) {
                                    var n = (i - a) / 1e3,
                                        r = parseInt(n / 86400),
                                        s = parseInt(n % 86400 / 3600),
                                        o = parseInt(n % 86400 % 3600 / 60),
                                        d = parseInt(n % 86400 % 3600 % 60);
                                    t = {
                                        day: this.timeFormat(r),
                                        hou: this.timeFormat(s),
                                        min: this.timeFormat(o),
                                        sec: this.timeFormat(d)
                                    };
                                } else t = {
                                    day: "00",
                                    hou: "00",
                                    min: "00",
                                    sec: "00"
                                };
                                e.push(t), this.countDownList = e, setTimeout(this.countDown, 1e3);
                            },
                            countDown1: function () {
                                var a = new Date().getTime(),
                                    i = 1e3 * this.bargainEndTime,
                                    e = [],
                                    t = null;
                                if (0 < i - a) {
                                    var n = (i - a) / 1e3,
                                        r = parseInt(n / 86400),
                                        s = parseInt(n % 86400 / 3600),
                                        o = parseInt(n % 86400 % 3600 / 60),
                                        d = parseInt(n % 86400 % 3600 % 60);
                                    t = {
                                        day: this.timeFormat(r),
                                        hou: this.timeFormat(s),
                                        min: this.timeFormat(o),
                                        sec: this.timeFormat(d)
                                    };
                                } else t = {
                                    day: "00",
                                    hou: "00",
                                    min: "00",
                                    sec: "00"
                                };
                                e.push(t), this.bargainCt = e, setTimeout(this.countDown1, 1e3);
                            },
                            goActivity: function () {
                                if (!this.getSuid()) return !1;
                                var i = this,
                                    a = r.getStorageSync("subscribe");
                                if (0 < a.length) {
                                    var e = new Array();
                                    "" != a[0].mid && e.push(a[0].mid), "" != a[1].mid && e.push(a[1].mid), "" != a[2].mid && e.push(a[2].mid),
                                        0 < e.length ? r.requestSubscribeMessage({
                                            tmplIds: e,
                                            success: function (a) {
                                                r.navigateTo({
                                                    url: "/pagesBargain/bargain_activity/bargain_activity?bargain_id=" + i.bargain_id + "&id=" + i.id
                                                });
                                            }
                                        }) : r.navigateTo({
                                            url: "/pagesBargain/bargain_activity/bargain_activity?bargain_id=" + i.bargain_id + "&id=" + i.id
                                        });
                                } else r.navigateTo({
                                    url: "/pagesBargain/bargain_activity/bargain_activity?bargain_id=" + i.bargain_id + "&id=" + i.id
                                });
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1;
                            },
                            getSuid: function () {
                                if (r.getStorageSync("suid")) return !0;
                                return r.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                    !1;
                            },
                            createOrder: function (a) {
                                var i = this;
                                if (!this.getSuid()) return !1;
                                var e = r.getStorageSync("subscribe");
                                if (0 < e.length) {
                                    var t = new Array();
                                    "" != e[0].mid && t.push(e[0].mid), "" != e[1].mid && t.push(e[1].mid), "" != e[2].mid && t.push(e[2].mid),
                                        0 < t.length ? r.requestSubscribeMessage({
                                            tmplIds: t,
                                            success: function (a) {
                                                r.redirectTo({
                                                    url: "/pagesBargain/bargain_order/bargain_order?id=" + i.id
                                                });
                                            }
                                        }) : r.redirectTo({
                                            url: "/pagesBargain/bargain_order/bargain_order?id=" + this.id
                                        });
                                } else r.redirectTo({
                                    url: "/pagesBargain/bargain_order/bargain_order?id=" + this.id
                                });
                            },
                            swiperLoad: function (t) {
                                var n = this;
                                r.getSystemInfo({
                                    success: function (a) {
                                        var i = t.detail.width / t.detail.height,
                                            e = a.windowWidth / i;
                                        n.heighthave || (n.minHeight = e, n.heighthave = 1);
                                    }
                                });
                            },
                            swiperChange: function (a) {
                                this.currentSwiper = a.detail.current, this.isplay = !1, this.autoplay = !0;
                            },
                            playvideo: function () {
                                this.isplay = !0, this.autoplay = !1;
                            },
                            endvideo: function () {
                                this.autoplay = !0, this.isplay = !1;
                            },
                            open_share: function () {
                                this.share = 1;
                            },
                            share_close: function () {
                                this.share = 0, this.shareimg = 0;
                            },
                            h5ShareAppMessage: function () {
                                var i = this,
                                    a = r.getStorageSync("suid");
                                r.showModal({
                                    title: "长按复制链接后分享",
                                    content: this.$host + "/h5/index.html?id=" + this.$uniacid + "#/pagesBargain/bargain_pro/bargain_pro?id=" + this.id + "&fxsid=" + a + "&userid=" + a,
                                    showCancel: !1,
                                    success: function (a) {
                                        i.share = 0;
                                    }
                                });
                            },
                            getShareImg: function () {
                                r.showLoading({
                                    title: "海报生成中"
                                });
                                var i = this;
                                r.request({
                                    url: i.$baseurl + "dopageshareewm",
                                    data: {
                                        uniacid: i.$uniacid,
                                        suid: r.getStorageSync("suid"),
                                        gid: i.id,
                                        types: "bargain",
                                        source: r.getStorageSync("source"),
                                        pageUrl: "bargain_pro"
                                    },
                                    success: function (a) {
                                        r.hideLoading(), 0 == a.data.data.error ? (i.shareimg = 1, i.shareimg_url = a.data.data.url) : r.showToast({
                                            title: a.data.data.msg,
                                            icon: "none"
                                        });
                                    }
                                });
                            },
                            closeShare: function () {
                                this.shareimg = 0;
                            },
                            saveImg: function () {
                                var i = this;
                                r.getImageInfo({
                                    src: i.shareimg_url,
                                    success: function (a) {
                                        r.saveImageToPhotosAlbum({
                                            filePath: a.path,
                                            success: function () {
                                                r.showToast({
                                                    title: "保存成功！",
                                                    icon: "none"
                                                }), i.shareimg = 0, i.share = 0;
                                            }
                                        });
                                    }
                                });
                            },
                            aliSaveImg: function () {
                                var i = this;
                                r.getImageInfo({
                                    src: i.shareimg_url,
                                    success: function (a) {
                                        my.saveImage({
                                            url: a.path,
                                            showActionSheet: !0,
                                            success: function () {
                                                my.alert({
                                                    title: "保存成功"
                                                }), i.shareimg = 0, i.share = 0;
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    };
                i.default = a;
            }).call(this, e("543d").default);
        }
    },
    [
        ["272d", "common/runtime", "common/vendor"]
    ]
]);