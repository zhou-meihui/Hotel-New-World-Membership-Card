(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesBargain/my_bargain/my_bargain"], {
        3412: function (a, t, n) {
            var e = n("ed09");
            n.n(e).a;
        },
        "5c1a": function (a, t, n) {
            n.r(t);
            var e = n("7353"),
                i = n("9a21");
            for (var r in i) "default" !== r && function (a) {
                n.d(t, a, function () {
                    return i[a];
                });
            }(r);
            n("3412");
            var o = n("2877"),
                s = Object(o.a)(i.default, e.a, e.b, !1, null, null, null);
            t.default = s.exports;
        },
        "5e66": function (a, t, n) {
            (function (e) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var i = n("f571"),
                    a = {
                        data: function () {
                            return {
                                list: [],
                                page: 1,
                                addmore: 1,
                                flag: 1,
                                baseinfo: []
                            };
                        },
                        onPullDownRefresh: function () {
                            this.page = 1, this.myBargainList(), e.stopPullDownRefresh();
                        },
                        onLoad: function (a) {
                            var t = this,
                                n = this;
                            e.setNavigationBarTitle({
                                title: "我的砍价"
                            });
                            a.fxsid && (n.fxsid = a.fxsid), this._baseMin(this), i.getOpenid(0, function () {
                                n.myBargainList();
                            }, function () {
                                t.needAuth = !0;
                            }, function () {
                                t.needBind = !0;
                            });
                        },
                        methods: {
                            changeNav: function (a) {
                                var t = this,
                                    n = a.currentTarget.dataset.flag;
                                t.flag = n, t.page = 1, t.list = [], t.myBargainList();
                            },
                            myBargainList: function () {
                                var t = this;
                                e.request({
                                    url: t.$baseurl + "doPagemyBargainList",
                                    data: {
                                        uniacid: t.$uniacid,
                                        openid: t.openid,
                                        suid: e.getStorageSync("suid"),
                                        page: t.page,
                                        flag: t.flag
                                    },
                                    success: function (a) {
                                        t.list = a.data.data, t.setCountDowns();
                                    }
                                });
                            },
                            addmores: function () {
                                var t = this,
                                    n = t.page + 1;
                                e.request({
                                    url: t.$baseurl + "doPagemyBargainList",
                                    data: {
                                        uniacid: t.$uniacid,
                                        openid: t.openid,
                                        suid: e.getStorageSync("suid"),
                                        page: n,
                                        flag: t.flag
                                    },
                                    success: function (a) {
                                        a.data.data.length < 6 && (t.addmore = 0), t.page = n, t.list = t.list.concat(a.data.data),
                                            t.setCountDowns();
                                    }
                                });
                            },
                            setCountDowns: function () {
                                for (var a = this, t = a.list, n = new Date().getTime(), e = 0; e < t.length; e++) {
                                    var i = 1e3 * t[e].overtime - n;
                                    i <= 0 && (i = 0);
                                    var r = this.getFormat(i);
                                    t[e].djs.day = "".concat(r.dd), t[e].djs.hour = "".concat(r.hh), t[e].djs.min = "".concat(r.mm),
                                        t[e].djs.second = "".concat(r.ss);
                                }
                                a.list = t, setTimeout(a.setCountDowns, 1e3);
                            },
                            getFormat: function (a) {
                                var t = parseInt(a / 1e3),
                                    n = 0,
                                    e = 0,
                                    i = 0;
                                return 60 < t && (n = parseInt(t / 60), t = parseInt(t % 60), 60 < n && (e = parseInt(n / 60),
                                    n = parseInt(n % 60), 24 < e && (i = parseInt(e / 24), e = parseInt(e % 24)))), {
                                    ss: t = 9 < t ? t : "0".concat(t),
                                    mm: n = 9 < n ? n : "0".concat(n),
                                    hh: e = 9 < e ? e : "0".concat(e),
                                    dd: i = 9 < i ? i : "0".concat(i)
                                };
                            },
                            goActivity: function (a) {
                                var t = a.currentTarget.dataset.pid,
                                    n = a.currentTarget.dataset.bargain_id;
                                e.navigateTo({
                                    url: "/pagesBargain/bargain_activity/bargain_activity?bargain_id=" + n + "&id=" + t
                                });
                            },
                            createOrder: function (a) {
                                var t = a.currentTarget.dataset.pid,
                                    n = a.currentTarget.dataset.bargain_id;
                                e.navigateTo({
                                    url: "/pagesBargain/bargain_order/bargain_order?id=" + t + "&bargain_id=" + n
                                });
                            },
                            golist: function () {
                                e.navigateTo({
                                    url: "/pagesBargain/bargain/bargain"
                                });
                            }
                        }
                    };
                t.default = a;
            }).call(this, n("543d").default);
        },
        7353: function (a, t, n) {
            var e = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            n.d(t, "a", function () {
                return e;
            }), n.d(t, "b", function () {
                return i;
            });
        },
        "9a21": function (a, t, n) {
            n.r(t);
            var e = n("5e66"),
                i = n.n(e);
            for (var r in e) "default" !== r && function (a) {
                n.d(t, a, function () {
                    return e[a];
                });
            }(r);
            t.default = i.a;
        },
        b4a1: function (a, t, n) {
            (function (a) {
                function t(a) {
                    return a && a.__esModule ? a : {
                        default: a
                    };
                }
                n("020c"), n("921b"), t(n("66fd")), a(t(n("5c1a")).default);
            }).call(this, n("543d").createPage);
        },
        ed09: function (a, t, n) {}
    },
    [
        ["b4a1", "common/runtime", "common/vendor"]
    ]
]);