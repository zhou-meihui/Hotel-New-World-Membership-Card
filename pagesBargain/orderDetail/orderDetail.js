(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesBargain/orderDetail/orderDetail"], {
        "00c1": function (t, a, e) {
            e.r(a);
            var n = e("e873"),
                o = e.n(n);
            for (var i in n) "default" !== i && function (t) {
                e.d(a, t, function () {
                    return n[t];
                });
            }(i);
            a.default = o.a;
        },
        "1ad4": function (t, a, e) {},
        "3a3c": function (t, a, e) {
            var n = e("1ad4");
            e.n(n).a;
        },
        "78aa": function (t, a, e) {
            var n = function () {
                    this.$createElement;
                    this._self._c;
                },
                o = [];
            e.d(a, "a", function () {
                return n;
            }), e.d(a, "b", function () {
                return o;
            });
        },
        "8fd0": function (t, a, e) {
            (function (t) {
                function a(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                e("020c"), e("921b"), a(e("66fd")), t(a(e("fa1b")).default);
            }).call(this, e("543d").createPage);
        },
        e873: function (t, a, e) {
            (function (i) {
                Object.defineProperty(a, "__esModule", {
                    value: !0
                }), a.default = void 0;
                var n = e("f571"),
                    t = {
                        data: function () {
                            return {
                                state: 1,
                                orderFormDisable: !0,
                                isChange: "",
                                formchangeBtn: 2,
                                datas: {
                                    store_info: []
                                },
                                $imgurl: this.$imgurl,
                                baseinfo: [],
                                showPay: 0,
                                choosepayf: 0,
                                mymoney: 0,
                                mymoney_pay: 1,
                                is_submit: 1,
                                order_id: 0,
                                h5_wxpay: 0,
                                h5_alipay: 0,
                                pay_type: 1,
                                pay_money: 0,
                                paytype: ""
                            };
                        },
                        onPullDownRefresh: function () {
                            i.stopPullDownRefresh();
                        },
                        onLoad: function (t) {
                            var a = this,
                                e = this;
                            t.orderid && (e.orderid = t.orderid);
                            t.fxsid && (e.fxsid = t.fxsid), this._baseMin(this), n.getOpenid(0, function () {
                                e.getOrder();
                            }, function () {
                                a.needAuth = !0;
                            }, function () {
                                a.needBind = !0;
                            });
                        },
                        methods: {
                            paybox: function (t) {
                                this.orderinfo;
                                var a = this.datas.order_id;
                                this.order_id = a;
                                var e = this.datas.true_price;
                                this.pay_money = e, this.mymoney < e && (this.mymoney_pay = 2, this.pay_type = 2,
                                    this.choosepayf = 1), 0 == this.showPay && (this.showPay = 1);
                            },
                            payboxclose: function () {
                                1 == this.showPay && (this.showPay = 0);
                            },
                            choosepay: function (t) {
                                this.pay_type = t.currentTarget.dataset.pay_type, this.choosepayf = t.currentTarget.dataset.type;
                            },
                            pay: function (t) {
                                if (2 == this.is_submit) return !1;
                                this.is_submit = 2, this.$uniacid, this.suid, this.source;
                                var a = this.pay_type,
                                    e = this.pay_money;
                                console.log(e);
                                var n = this.order_id,
                                    o = t.detail.formId;
                                1 == a ? this.pay1(n) : this._showwxpay(this, e, "bargain", n, o, "/pagesBargain/orderlist/orderlist");
                            },
                            pay1: function (a) {
                                var e = this;
                                i.showModal({
                                    title: "请注意",
                                    content: "您将使用余额支付" + e.pay_money + "元",
                                    success: function (t) {
                                        t.confirm ? (e.payover_do(a), i.showLoading({
                                            title: "下单中...",
                                            mask: !0
                                        })) : i.redirectTo({
                                            url: "/pagesBargain/orderlist/orderlist"
                                        });
                                    }
                                });
                            },
                            payover_do: function (t) {
                                var a = this,
                                    e = a.pay_money,
                                    n = (a.mymoney, i.getStorageSync("suid")),
                                    o = i.getStorageSync("openid");
                                i.request({
                                    url: a.$baseurl + "doPagepaynotify",
                                    data: {
                                        out_trade_no: t,
                                        suid: n,
                                        payprice: e,
                                        types: "bargain",
                                        flag: 0,
                                        formId: a.formId,
                                        uniacid: a.$uniacid,
                                        openid: o,
                                        source: i.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        "失败" == t.data.data.message ? i.showToast({
                                            title: "付款失败, 请刷新后重新付款！",
                                            icon: "none",
                                            mask: !0,
                                            success: function () {
                                                i.navigateTo({
                                                    url: "/pagesBargain/orderlist/orderlist"
                                                }), i.hideLoading();
                                            }
                                        }) : i.showToast({
                                            title: "购买成功！",
                                            icon: "success",
                                            mask: !0,
                                            success: function () {
                                                i.navigateTo({
                                                    url: "/pagesBargain/orderlist/orderlist"
                                                }), i.hideLoading();
                                            }
                                        });
                                    }
                                });
                            },
                            makePhoneCallC: function (t) {
                                i.makePhoneCall({
                                    phoneNumber: t.currentTarget.dataset.tel
                                });
                            },
                            ContactMerchant: function () {
                                var e = this;
                                i.showModal({
                                    title: "提示",
                                    content: "请联系商家咨询具体信息！",
                                    confirmText: "联系商家",
                                    success: function (t) {
                                        if (t.confirm) {
                                            var a = e.baseinfo.tel;
                                            i.makePhoneCall({
                                                phoneNumber: a
                                            });
                                        }
                                    }
                                });
                            },
                            bindDateChange2: function (t) {
                                this.chuydate = t.detail.value;
                            },
                            getOrder: function () {
                                var e = this,
                                    t = e.orderid;
                                i.request({
                                    url: e.$baseurl + "doPagegetBargainOrderDetail",
                                    data: {
                                        uniacid: e.$uniacid,
                                        order_id: t,
                                        suid: i.getStorageSync("suid")
                                    },
                                    success: function (t) {
                                        e.datas = t.data.data.info, e.mymoney = t.data.data.mymoney;
                                        var a = t.data.data.info.paytype;
                                        0 == a ? e.paytype = "余额支付" : 1 == a ? e.paytype = "微信支付" : 2 == a ? e.paytype = "支付宝支付" : 3 == a ? e.paytype = "百度支付" : 4 == a && (e.paytype = "QQ支付");
                                    }
                                });
                            },
                            copy: function (t) {
                                i.setClipboardData({
                                    data: t.currentTarget.dataset.ddh,
                                    success: function (t) {
                                        i.showToast({
                                            title: "复制成功"
                                        });
                                    }
                                });
                            },
                            makephonecall: function () {
                                "" != this.datas.seller_tel && "null" != this.datas.seller_tel && i.makePhoneCall({
                                    phoneNumber: this.datas.seller_tel
                                });
                            },
                            tuikuan: function (t) {
                                var a = this,
                                    e = t.detail.formId,
                                    n = t.currentTarget.dataset.order;
                                i.showModal({
                                    title: "提醒",
                                    content: "确定要退款吗？",
                                    success: function (t) {
                                        t.confirm && i.request({
                                            url: a.$baseurl + "doPagetk",
                                            data: {
                                                uniacid: a.$uniacid,
                                                formId: e,
                                                order_id: n
                                            },
                                            success: function (t) {
                                                0 == t.data.data.flag ? i.showModal({
                                                    title: "提示",
                                                    content: t.data.data.message,
                                                    showCancel: !1,
                                                    success: function (t) {
                                                        i.redirectTo({
                                                            url: "/pagesBargain/orderDetail/orderDetail?orderid=" + n
                                                        });
                                                    }
                                                }) : i.showModal({
                                                    title: "很抱歉",
                                                    content: t.data.data.message,
                                                    confirmText: "联系客服",
                                                    success: function (t) {
                                                        t.confirm && i.makePhoneCall({
                                                            phoneNumber: a.datas.mobile
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    };
                a.default = t;
            }).call(this, e("543d").default);
        },
        fa1b: function (t, a, e) {
            e.r(a);
            var n = e("78aa"),
                o = e("00c1");
            for (var i in o) "default" !== i && function (t) {
                e.d(a, t, function () {
                    return o[t];
                });
            }(i);
            e("3a3c");
            var s = e("2877"),
                r = Object(s.a)(o.default, n.a, n.b, !1, null, null, null);
            a.default = r.exports;
        }
    },
    [
        ["8fd0", "common/runtime", "common/vendor"]
    ]
]);