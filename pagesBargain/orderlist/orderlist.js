(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesBargain/orderlist/orderlist"], {
        1701: function (t, e, i) {
            (function (o) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var a = i("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                page: 1,
                                morePro: !1,
                                baseinfo: [],
                                orderinfo: [],
                                orderinfo_length: 0,
                                type: 10,
                                nav: [{
                                    id: 10,
                                    text: "全部"
                                }, {
                                    id: 0,
                                    text: "待付款"
                                }, {
                                    id: 1,
                                    text: "待核销",
                                    nav: 2
                                }, {
                                    id: 11,
                                    text: "待发货",
                                    nav: 1
                                }, {
                                    id: 4,
                                    text: "已发货"
                                }, {
                                    id: 2,
                                    text: "已完成"
                                }, {
                                    id: -1,
                                    text: "已关闭"
                                }, {
                                    id: 3,
                                    text: "已过期"
                                }, {
                                    id: 5,
                                    text: "商家已取消"
                                }, {
                                    id: 6,
                                    text: "售后"
                                }],
                                page_signs: "/pagesBargain/orderlist/orderlist",
                                showhx: 0,
                                hxmm: "",
                                hxmm_list: [{
                                    val: "",
                                    fs: !0
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }],
                                hx_choose: 0,
                                hx_ewm: "",
                                showPay: 0,
                                choosepayf: 0,
                                mymoney: 0,
                                mymoney_pay: 1,
                                is_submit: 1,
                                order_id: 0,
                                h5_wxpay: 0,
                                h5_alipay: 0,
                                pay_type: 1,
                                pay_money: 0
                            };
                        },
                        onLoad: function (t) {
                            var e = this;
                            o.setNavigationBarTitle({
                                title: "砍价订单"
                            }), t.type && (this.type = t.type);
                            t.fxsid && (this.fxsid = t.fxsid), this._baseMin(this), a.getOpenid(0, function () {}, function () {
                                e.needAuth = !0;
                            }, function () {
                                e.needBind = !0;
                            });
                        },
                        onShow: function () {
                            this.page = 1, this.getList();
                        },
                        onPullDownRefresh: function () {
                            this.page = 1, this.getList(), o.stopPullDownRefresh();
                        },
                        onReachBottom: function () {
                            var e = this,
                                t = e.type,
                                a = e.page + 1,
                                i = o.getStorageSync("openid");
                            o.request({
                                url: e.$baseurl + "doPageMyBargainorder",
                                data: {
                                    uniacid: e.$uniacid,
                                    openid: i,
                                    suid: o.getStorageSync("suid"),
                                    page: a,
                                    type: t,
                                    is_more: 0
                                },
                                success: function (t) {
                                    "" != t.data.data.list && (e.orderinfo = e.orderinfo.concat(t.data.data.list), e.page = a,
                                        e.mymoney = t.data.data.mymoney);
                                }
                            });
                        },
                        methods: {
                            paybox: function (t) {
                                var e = 0,
                                    a = this.orderinfo,
                                    i = t.currentTarget.dataset.order;
                                this.order_id = i;
                                for (var n = 0; n < a.length; n++) a[n].order_id == i && (e = a[n].true_price);
                                this.pay_money = e, this.mymoney < e && (this.mymoney_pay = 2, this.pay_type = 2,
                                    this.choosepayf = 1), 0 == this.showPay && (this.showPay = 1);
                            },
                            payboxclose: function () {
                                1 == this.showPay && (this.showPay = 0);
                            },
                            choosepay: function (t) {
                                this.pay_type = t.currentTarget.dataset.pay_type, this.choosepayf = t.currentTarget.dataset.type;
                            },
                            pay: function (t) {
                                if (2 == this.is_submit) return !1;
                                this.is_submit = 2, this.$uniacid, this.suid, this.source;
                                var e = this.pay_type,
                                    a = this.pay_money,
                                    i = this.order_id,
                                    n = t.detail.formId;
                                1 == e ? this.pay1(i) : this._showwxpay(this, a, "bargain", i, n, "/pagesBargain/orderlist/orderlist");
                            },
                            pay1: function (e) {
                                var a = this;
                                o.showModal({
                                    title: "请注意",
                                    content: "您将使用余额支付" + a.pay_money + "元",
                                    success: function (t) {
                                        t.confirm ? (a.payover_do(e), o.showLoading({
                                            title: "下单中...",
                                            mask: !0
                                        })) : o.redirectTo({
                                            url: "/pagesBargain/orderlist/orderlist"
                                        });
                                    }
                                });
                            },
                            payover_do: function (t) {
                                var e = this,
                                    a = e.pay_money,
                                    i = (e.mymoney, o.getStorageSync("suid")),
                                    n = o.getStorageSync("openid");
                                o.request({
                                    url: e.$baseurl + "doPagepaynotify",
                                    data: {
                                        out_trade_no: t,
                                        suid: i,
                                        payprice: a,
                                        types: "bargain",
                                        flag: 0,
                                        formId: e.formId,
                                        uniacid: e.$uniacid,
                                        openid: n,
                                        source: o.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        "失败" == t.data.data.message ? o.showToast({
                                            title: "付款失败, 请刷新后重新付款！",
                                            icon: "none",
                                            mask: !0,
                                            success: function () {
                                                o.navigateTo({
                                                    url: "/pagesBargain/orderlist/orderlist"
                                                }), o.hideLoading();
                                            }
                                        }) : o.showToast({
                                            title: "购买成功！",
                                            icon: "success",
                                            mask: !0,
                                            success: function () {
                                                o.navigateTo({
                                                    url: "/pagesBargain/orderlist/orderlist"
                                                }), o.hideLoading();
                                            }
                                        });
                                    }
                                });
                            },
                            wlinfo: function (t) {
                                var e = t.currentTarget.dataset.kuaidi,
                                    a = t.currentTarget.dataset.kuaidihao;
                                o.navigateTo({
                                    url: "/pages/logistics_state/logistics_state?kuaidi=" + e + "&kuaidihao=" + a
                                });
                            },
                            getList: function (t) {
                                var e = this,
                                    a = o.getStorageSync("openid");
                                o.request({
                                    url: e.$baseurl + "doPageMyBargainorder",
                                    data: {
                                        uniacid: e.$uniacid,
                                        openid: a,
                                        suid: o.getStorageSync("suid"),
                                        nav: e.navs,
                                        type: e.type
                                    },
                                    success: function (t) {
                                        e.orderinfo = t.data.data.list, e.orderinfo_length = t.data.data.list.length, e.mymoney = t.data.data.mymoney;
                                    },
                                    fail: function (t) {}
                                });
                            },
                            chonxhq: function (t) {
                                var e = this,
                                    a = t.currentTarget.dataset.id,
                                    i = t.currentTarget.dataset.nav || "";
                                e.type = a, e.page = 1, e.navs = i;
                                var n = o.getStorageSync("openid");
                                o.request({
                                    url: e.$baseurl + "doPageMyBargainorder",
                                    data: {
                                        uniacid: e.$uniacid,
                                        openid: n,
                                        suid: o.getStorageSync("suid"),
                                        type: a,
                                        nav: i
                                    },
                                    success: function (t) {
                                        e.orderinfo = t.data.data.list, e.orderinfo_length = t.data.data.list.length, e.mymoney = t.data.data.mymoney;
                                    },
                                    fail: function (t) {}
                                });
                            },
                            choose_nav: function (t) {
                                var e = t.currentTarget.dataset.id;
                                this.a = e;
                            },
                            makePhoneCallB: function (t) {
                                var e = this.baseinfo.tel_b;
                                o.makePhoneCall({
                                    phoneNumber: e
                                });
                            },
                            hxshow: function (t) {
                                this.showhx = 1, this.order = t.currentTarget.dataset.order;
                            },
                            hxhide: function () {
                                this.showhx = 0, this.hxmm = "";
                            },
                            hxmmInput: function (t) {
                                for (var e = t.target.value.length, a = 0; a < this.hxmm_list.length; a++) this.hxmm_list[a].fs = !1,
                                    this.hxmm_list[a].val = t.target.value[a];
                                e && (this.hxmm_list[e - 1].fs = !0), this.hxmm = t.target.value;
                            },
                            hxmmpass: function () {
                                var a = this,
                                    t = a.hxmm,
                                    e = a.order,
                                    i = o.getStorageSync("openid");
                                t ? o.request({
                                    url: a.$baseurl + "hxmm",
                                    data: {
                                        uniacid: a.$uniacid,
                                        hxmm: t,
                                        order_id: e,
                                        is_more: 4,
                                        openid: i,
                                        suid: o.getStorageSync("suid")
                                    },
                                    header: {
                                        "content-type": "application/json"
                                    },
                                    success: function (t) {
                                        if (0 == t.data.data) {
                                            o.showModal({
                                                title: "提示",
                                                content: "核销密码不正确！",
                                                showCancel: !1
                                            }), a.hxmm = "";
                                            for (var e = 0; e < a.hxmm_list.length; e++) a.hxmm_list[e].fs = !1, a.hxmm_list[e].val = "";
                                        } else o.showToast({
                                            title: "核销成功",
                                            icon: "success",
                                            duration: 2e3,
                                            success: function (t) {
                                                a.showhx = 0, a.hxmm = "", o.startPullDownRefresh(), a.page = 1, a.getList(), o.stopPullDownRefresh();
                                            }
                                        });
                                    }
                                }) : o.showModal({
                                    title: "提示",
                                    content: "请输入核销密码！",
                                    showCancel: !1
                                });
                            },
                            orderagain: function (t) {
                                var e = t.currentTarget.dataset.pid;
                                o.navigateTo({
                                    url: "/pagesBargain/bargain_pro/bargain_pro?id=" + e
                                });
                            },
                            qrshouh: function (t) {
                                var e = this,
                                    a = t.currentTarget.dataset.order,
                                    i = o.getStorageSync("openid");
                                o.showModal({
                                    title: "提示",
                                    content: "确认收货吗？",
                                    success: function (t) {
                                        t.confirm && o.request({
                                            url: e.$baseurl + "doPageshouhuo",
                                            data: {
                                                uniacid: e.$uniacid,
                                                openid: i,
                                                suid: o.getStorageSync("suid"),
                                                orderid: a
                                            },
                                            success: function (t) {
                                                o.showToast({
                                                    title: "收货成功！",
                                                    success: function (t) {
                                                        setTimeout(function () {
                                                            e.page = 1, e.getList();
                                                        }, 1500);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            redirectto: function (t) {
                                var e = t.currentTarget.dataset.link,
                                    a = t.currentTarget.dataset.linktype;
                                this._redirectto(e, a);
                            },
                            gethxmima: function () {
                                this.hx_choose = 1;
                            },
                            gethxImg: function () {
                                var e = this;
                                o.request({
                                    url: this.$baseurl + "doPageHxEwm",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: o.getStorageSync("suid"),
                                        pageUrl: "bargain_pro",
                                        orderid: this.order
                                    },
                                    success: function (t) {
                                        e.hx_ewm = t.data.data, e.hx_choose = 2;
                                    }
                                });
                            },
                            hxhide1: function () {
                                this.hx_choose = 0, this.hxmm = "";
                                for (var t = 0; t < this.hxmm_list.length; t++) this.hxmm_list[t].fs = !1, this.hxmm_list[t].val = "";
                            }
                        }
                    };
                e.default = t;
            }).call(this, i("543d").default);
        },
        "6fc3": function (t, e, a) {
            a.r(e);
            var i = a("1701"),
                n = a.n(i);
            for (var o in i) "default" !== o && function (t) {
                a.d(e, t, function () {
                    return i[t];
                });
            }(o);
            e.default = n.a;
        },
        7840: function (t, e, a) {
            var i = a("b553");
            a.n(i).a;
        },
        acad: function (t, e, a) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                a("020c"), a("921b"), e(a("66fd")), t(e(a("f7ab")).default);
            }).call(this, a("543d").createPage);
        },
        b553: function (t, e, a) {},
        eeb4: function (t, e, a) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                n = [];
            a.d(e, "a", function () {
                return i;
            }), a.d(e, "b", function () {
                return n;
            });
        },
        f7ab: function (t, e, a) {
            a.r(e);
            var i = a("eeb4"),
                n = a("6fc3");
            for (var o in n) "default" !== o && function (t) {
                a.d(e, t, function () {
                    return n[t];
                });
            }(o);
            a("7840");
            var s = a("2877"),
                r = Object(s.a)(n.default, i.a, i.b, !1, null, null, null);
            e.default = r.exports;
        }
    },
    [
        ["acad", "common/runtime", "common/vendor"]
    ]
]);