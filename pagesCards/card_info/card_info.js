(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesCards/card_info/card_info"], {
        "04e4": function (t, a, e) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                n = [];
            e.d(a, "a", function () {
                return i;
            }), e.d(a, "b", function () {
                return n;
            });
        },
        "1c0f": function (t, a, e) {
            var i = e("6ec0");
            e.n(i).a;
        },
        "2b25": function (t, a, e) {
            (function (t) {
                function a(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                e("020c"), e("921b"), a(e("66fd")), t(a(e("ebf7")).default);
            }).call(this, e("543d").createPage);
        },
        "500f": function (t, a, e) {
            e.r(a);
            var i = e("6957"),
                n = e.n(i);
            for (var s in i) "default" !== s && function (t) {
                e.d(a, t, function () {
                    return i[t];
                });
            }(s);
            a.default = n.a;
        },
        6957: function (t, e, i) {
            (function (u) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var t, s = (t = i("3584")) && t.__esModule ? t : {
                    default: t
                };
                var n = i("f571"),
                    c = u.createInnerAudioContext(),
                    a = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                page_signs: "staff_cards",
                                staffinfo: {
                                    expand: []
                                },
                                style: 0,
                                hide_info: 0,
                                id: 0,
                                isview: 0,
                                voice: "",
                                autovoice: 0,
                                music: "",
                                musicAutoPlay: "",
                                musicTitle: "",
                                musicpay: 0,
                                audioplay: 1,
                                duration: "",
                                curTimeVal: "",
                                durationDay: "播放获取",
                                curTimeValDay: "00:00",
                                st: "",
                                sy: "",
                                status: 0,
                                tabbar_t: 0,
                                iszan: 0,
                                zans: "",
                                zan: 0,
                                basefoot: {
                                    tabbar: []
                                },
                                baseinfo: "",
                                share: 0,
                                staffset: "",
                                pic: "",
                                share_open: "",
                                descp: "",
                                shareimg: 0,
                                shareimg_url: "",
                                system_w: 0,
                                system_h: 0,
                                img_w: 0,
                                img_h: 0,
                                base_color: "",
                                needBind: !1,
                                needAuth: !1,
                                visit_num: 0,
                                forward_num: 0
                            };
                        },
                        onHide: function () {
                            this.getStaffInfo(), this.getStaffset();
                        },
                        onUnload: function () {
                            this.audioPause(), this.getStaffset();
                        },
                        onPullDownRefresh: function () {
                            this.getStaffInfo(), this.getStaffset(), u.stopPullDownRefresh();
                        },
                        onShareAppMessage: function () {
                            var t = u.getStorageSync("suid");
                            return "微信小程序名片", console.log("/pagesCards/card_info/card_info?id=" + this.id + "&sharesuid=" + t), {
                                title: "微信小程序名片",
                                path: "/pagesCards/card_info/card_info?id=" + this.id + "&sharesuid=" + t
                            };
                        },
                        onLoad: function (t) {
                            var a = this;
                            t.scene ? a.id = t.scene : a.id = t.id;
                            var e = 0;
                            t.fxsid && (e = t.fxsid, u.setStorageSync("fxsid", e), a.fxsid = e), 1 == t.share && a.share111(),
                                this._baseMin(this), u.getStorageSync("suid"), n.getOpenid(e, function () {});
                            var i = u.getStorageSync("systemInfo");
                            this.img_w = parseInt((.65 * i.windowWidth).toFixed(0)), this.img_h = parseInt((1.875 * this.img_w).toFixed(0)),
                                this.system_w = parseInt(i.windowWidth), this.system_h = parseInt(i.windowHeight),
                                t.sharesuid && t.sharesuid != u.getStorageSync("suid") ? u.request({
                                    url: a.$baseurl + "dopagesharecardSuccess",
                                    data: {
                                        id: a.id,
                                        sharesuid: t.sharesuid,
                                        suid: u.getStorageSync("suid"),
                                        uniacid: a.$uniacid
                                    },
                                    success: function (t) {
                                        a.addvisit();
                                    }
                                }) : a.addvisit();
                        },
                        methods: {
                            h5ShareAppMessage: function () {
                                var a = this,
                                    t = u.getStorageSync("suid");
                                u.showModal({
                                    title: "长按复制链接后分享",
                                    content: this.$host + "/h5/index.html?id=" + this.$uniacid + "#/pagesCards/card_info/card_info?id=" + this.id + "&fxsid=" + t + "&userid=" + t,
                                    showCancel: !1,
                                    success: function (t) {
                                        a.share = 0;
                                    }
                                });
                            },
                            getShareImg: function () {
                                u.showLoading({
                                    title: "海报生成中"
                                });
                                var a = this;
                                u.request({
                                    url: a.$baseurl + "doPageStaffShareewm",
                                    data: {
                                        uniacid: a.$uniacid,
                                        suid: u.getStorageSync("suid"),
                                        gid: a.id,
                                        source: u.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        u.hideLoading(), 0 == t.data.data.error ? (a.shareimg = 1, a.shareimg_url = t.data.data.url) : u.showToast({
                                            title: t.data.data.msg,
                                            icon: "none"
                                        });
                                    }
                                });
                            },
                            addvisit: function () {
                                var a = this;
                                u.request({
                                    url: a.$baseurl + "dopageaddVisit",
                                    data: {
                                        id: a.id,
                                        suid: u.getStorageSync("suid"),
                                        uniacid: a.$uniacid
                                    },
                                    success: function (t) {
                                        a.getStaffInfo(), a.getStaffset();
                                    }
                                });
                            },
                            getStaffInfo: function () {
                                var n = this;
                                u.request({
                                    url: n.$baseurl + "dopagegetStaffInfo",
                                    data: {
                                        id: n.id,
                                        suid: u.getStorageSync("suid"),
                                        uniacid: n.$uniacid,
                                        source: u.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        if (t.data.data.staffinfo) {
                                            if ("" == t.data.data.userinfo.nickname && (n.isview = 1), n.staffinfo = t.data.data.staffinfo,
                                                u.setNavigationBarTitle({
                                                    title: n.staffinfo.realname ? n.staffinfo.realname : "名片"
                                                }), n.zan = t.data.data.staffinfo.zan, n.visit_num = t.data.data.staffinfo.visit,
                                                n.forward_num = t.data.data.staffinfo.forward, 9999 < n.zan) {
                                                var a = (n.zan / 1e4).toFixed(1) + "W";
                                                n.zan = a;
                                            }
                                            if (9999 < n.visit_num) {
                                                var e = (n.visit_num / 1e4).toFixed(1) + "W";
                                                n.visit_num = e;
                                            }
                                            if (9999 < n.forward_num) {
                                                var i = (n.forward_num / 1e4).toFixed(1) + "W";
                                                n.forward_num = i;
                                            }
                                            n.voice = t.data.data.staffinfo.voice, "" != n.voice && (n.autovoice = t.data.data.staffinfo.autovoice),
                                                "" == t.data.data.staffinfo.pic ? n.pic = "/pages/resource/img/default_pic.png" : n.pic = t.data.data.staffinfo.pic,
                                                1 == t.data.data.haszan ? (n.zans = 1, n.iszan = 1) : (n.zans = "", n.iszan = 0),
                                                n.descp = t.data.data.staffinfo.descp, n.descp && (n.descp = n.descp.replace(/\<img/gi, '<img style="width:100%;height:auto;display:block" '),
                                                    n.descp = (0, s.default)(n.descp)), 0 == n.autovoice ? (n.st = 10, n.sy = "", n.status = 0) : (n.st = 2,
                                                    n.sy = "music-on", n.status = 1, n.audioPlay());
                                        } else u.showModal({
                                            title: "提示",
                                            content: "该员工信息已被删除!",
                                            success: function (t) {
                                                t.confirm ? u.navigateBack({
                                                    delta: 1
                                                }) : t.cancel && u.navigateBack({
                                                    delta: 1
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            getStaffset: function () {
                                this.baseinfo.base_color2;
                                var a = this;
                                u.request({
                                    url: a.$baseurl + "dopagegetStaffset",
                                    data: {
                                        uniacid: a.$uniacid
                                    },
                                    success: function (t) {
                                        a.style = t.data.data.card_style, a.tabbar_t = t.data.data.tabbar_t, a.basefoot = t.data.data.baseInfo,
                                            a.share_open = t.data.data.is_share, a.staffset = t.data.data, a.$uniacid = a.$uniacid;
                                    }
                                });
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1;
                            },
                            getSuid: function () {
                                if (u.getStorageSync("suid")) return !0;
                                return u.getStorageSync("golobeuser") ? this.needBind = !0 : (this.needAuth = !0,
                                    console.log()), !1;
                            },
                            redirectto: function (t) {
                                var a = t.currentTarget.dataset.link,
                                    e = t.currentTarget.dataset.linktype;
                                this._redirectto(a, e);
                            },
                            callphone: function (t) {
                                var a = t.currentTarget.dataset.text;
                                u.makePhoneCall({
                                    phoneNumber: a
                                });
                            },
                            copy: function (t) {
                                var a = t.currentTarget.dataset.text;
                                u.setClipboardData({
                                    data: a,
                                    success: function (t) {
                                        u.getClipboardData({
                                            success: function (t) {}
                                        }), u.showToast({
                                            title: "复制成功",
                                            duration: 2e3
                                        });
                                    }
                                });
                            },
                            addPhoneContact: function (t) {
                                var e = this,
                                    a = t.currentTarget.dataset.text,
                                    i = t.currentTarget.dataset.mobile,
                                    n = t.currentTarget.dataset.wxnumber;
                                null != n && "" != n || (n = i);
                                var s = t.currentTarget.dataset.email,
                                    o = t.currentTarget.dataset.company,
                                    r = t.currentTarget.dataset.province,
                                    c = t.currentTarget.dataset.city,
                                    d = t.currentTarget.dataset.address;
                                u.addPhoneContact({
                                    firstName: a,
                                    mobilePhoneNumber: i,
                                    weChatNumber: n,
                                    email: s,
                                    organization: o,
                                    addressState: r,
                                    addressCity: c,
                                    addressStreet: d,
                                    success: function () {
                                        u.request({
                                            url: e.$baseurl + "dopagesavecard",
                                            data: {
                                                id: e.id,
                                                suid: u.getStorageSync("suid"),
                                                uniacid: e.$uniacid
                                            },
                                            success: function (t) {
                                                var a = t.data.data;
                                                0 == a ? u.showModal({
                                                    title: "提示",
                                                    content: "保存成功, 增加积分与抽奖次数!",
                                                    confirmText: "去抽奖",
                                                    success: function (t) {
                                                        t.confirm ? e.prizes() : t.cancel;
                                                    }
                                                }) : 1 == a ? u.showModal({
                                                    title: "提示",
                                                    content: "保存成功, 但积分增加与抽奖次数都已达到达每日上限!",
                                                    confirmText: "去抽奖",
                                                    success: function (t) {
                                                        t.confirm ? e.prizes() : t.cancel;
                                                    }
                                                }) : 2 == a ? u.showModal({
                                                    title: "提示",
                                                    content: "保存成功, 积分增加已达到每日上限,继续保存可增加抽奖次数!",
                                                    confirmText: "去抽奖",
                                                    success: function (t) {
                                                        t.confirm ? e.prizes() : t.cancel;
                                                    }
                                                }) : 3 == a ? u.showModal({
                                                    title: "提示",
                                                    content: "保存成功, 抽奖次数已达到每日上限,继续保存可增加积分!!",
                                                    confirmText: "去抽奖",
                                                    success: function (t) {
                                                        t.confirm ? e.prizes() : t.cancel;
                                                    }
                                                }) : 10 == a ? u.showModal({
                                                    title: "提示",
                                                    content: "该员工已保存过了!",
                                                    confirmText: "去抽奖",
                                                    success: function (t) {
                                                        t.confirm ? e.prizes() : t.cancel;
                                                    }
                                                }) : 11 == a ? u.showModal({
                                                    title: "提示",
                                                    content: "您已成功保存该员工!",
                                                    confirmText: "去抽奖",
                                                    success: function (t) {
                                                        t.confirm ? e.prizes() : t.cancel;
                                                    }
                                                }) : 12 == a && u.showModal({
                                                    title: "提示",
                                                    content: "保存成功!",
                                                    success: function (t) {
                                                        t.confirm || t.cancel;
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            playvoice: function (t) {
                                var a = this,
                                    e = t.currentTarget.dataset.text;
                                "" == a.voice ? u.showModal({
                                    title: "提示",
                                    content: "该员工还没有语音简介"
                                }) : 0 == e ? (a.audioPlay(), a.status = 1, a.st = 2, a.sy = "music-on", a.autovoice = 1) : 1 == e && (a.audioPause(),
                                    a.status = 0, a.st = 10, a.sy = "", a.autovoice = 0);
                            },
                            prizes: function () {
                                u.request({
                                    url: this.$baseurl + "doPagetoPrizes",
                                    data: {
                                        uniacid: this.$uniacid
                                    },
                                    success: function (t) {
                                        var a = t.data.data; -
                                        1 != a ? u.navigateTo({
                                            url: "/pagesShake/index/index?id=" + a
                                        }) : u.showModal({
                                            title: "提示",
                                            content: "当前小程序没有抽奖活动!"
                                        });
                                    }
                                });
                            },
                            addzan: function (t) {
                                if (!this.getSuid()) return !1;
                                var i = this,
                                    a = t.currentTarget.dataset.text;
                                u.request({
                                    url: i.$baseurl + "doPagestaffzan",
                                    data: {
                                        id: i.id,
                                        iszan: a,
                                        suid: u.getStorageSync("suid"),
                                        uniacid: i.$uniacid
                                    },
                                    success: function (t) {
                                        if (1 == t.data.data.result) {
                                            if (i.iszan = 1, i.zans = 1, i.zan = t.data.data.zan, 9999 < i.zan) {
                                                var a = (i.zan / 1e4).toFixed(1) + "W";
                                                i.zan = a;
                                            }
                                            u.showToast({
                                                title: "点赞成功"
                                            });
                                        } else {
                                            if (i.iszan = 0, i.zans = "", i.zan = t.data.data.zan, 9999 < i.zan) {
                                                var e = (i.zan / 1e4).toFixed(1) + "W";
                                                i.zan = e;
                                            }
                                            u.showToast({
                                                title: "取消赞成功"
                                            });
                                        }
                                    }
                                });
                            },
                            share111: function () {
                                this.share = 1;
                            },
                            share_close: function () {
                                this.share = 0, this.shareimg = 0;
                            },
                            audioPlay: function () {
                                var r = this;
                                "" == r.voice ? u.showModal({
                                    title: "提示",
                                    content: "该员工还没有语音简介"
                                }) : (r.autovoice = 1, r.st = 2, r.sy = "music-on", r.status = 1, c.src = r.voice,
                                    c.play(), c.onPlay(function (t) {
                                        c.onTimeUpdate(function (t) {
                                            var a = c.duration,
                                                e = parseInt(a / 60);
                                            e < 10 && (e = "0" + e);
                                            var i = parseInt(a - 60 * e);
                                            i < 10 && (i = "0" + i);
                                            var n = c.currentTime,
                                                s = parseInt(n / 60);
                                            s < 10 && (s = "0" + s);
                                            var o = parseInt(n - 60 * s);
                                            o < 10 && (o = "0" + o), r.duration = 100 * c.duration.toFixed(2), r.curTimeVal = 100 * c.currentTime.toFixed(2),
                                                r.durationDay = e + ":" + i, r.curTimeValDay = s + ":" + o;
                                        }), c.onEnded(function () {
                                            r.setStopState(r), r.autovoice = 0, r.st = 10, r.sy = "", r.status = 0;
                                        });
                                    }));
                            },
                            audioPause: function () {
                                this.autovoice = 0, this.st = 10, this.sy = "", this.status = 0, c.pause();
                            },
                            slideBar: function (t) {
                                var a = t.detail.value;
                                this.curTimeVal = a / 100, c.seek(this.curTimeVal);
                            },
                            updateTime: function (r) {
                                c.onTimeUpdate(function (t) {
                                        var a = c.duration,
                                            e = parseInt(a / 60);
                                        e < 10 && (e = "0" + e);
                                        var i = parseInt(a - 60 * e);
                                        i < 10 && (i = "0" + i);
                                        var n = c.currentTime,
                                            s = parseInt(n / 60);
                                        s < 10 && (s = "0" + s);
                                        var o = parseInt(n - 60 * s);
                                        o < 10 && (o = "0" + o), r.duration = 100 * c.duration.toFixed(2), r.curTimeVal = 100 * c.currentTime.toFixed(2),
                                            r.durationDay = e + ":" + i, r.curTimeValDay = s + ":" + o;
                                    }), c.duration.toFixed(2) - c.currentTime.toFixed(2) <= 0 && r.setStopState(r),
                                    c.onEnded(function () {
                                        r.setStopState(r);
                                    });
                            },
                            setStopState: function (t) {
                                t.curTimeVal = 0, c.stop();
                            },
                            hide_info_block: function () {
                                var t = this.hide_info;
                                t = 0 == t ? 1 : 0, this.hide_info = t;
                            },
                            closeShare: function () {
                                this.shareimg = 0;
                            },
                            saveImg: function () {
                                var a = this;
                                u.getImageInfo({
                                    src: a.shareimg_url,
                                    success: function (t) {
                                        u.saveImageToPhotosAlbum({
                                            filePath: t.path,
                                            success: function () {
                                                u.showToast({
                                                    title: "保存成功！",
                                                    icon: "none"
                                                }), a.shareimg = 0, a.share = 0;
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    };
                e.default = a;
            }).call(this, i("543d").default);
        },
        "6ec0": function (t, a, e) {},
        ebf7: function (t, a, e) {
            e.r(a);
            var i = e("04e4"),
                n = e("500f");
            for (var s in n) "default" !== s && function (t) {
                e.d(a, t, function () {
                    return n[t];
                });
            }(s);
            e("1c0f");
            var o = e("2877"),
                r = Object(o.a)(n.default, i.a, i.b, !1, null, null, null);
            a.default = r.exports;
        }
    },
    [
        ["2b25", "common/runtime", "common/vendor"]
    ]
]);