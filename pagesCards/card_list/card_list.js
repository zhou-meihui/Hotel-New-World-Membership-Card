(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesCards/card_list/card_list"], {
        "46b3": function (t, a, n) {
            var e = function () {
                    this.$createElement;
                    this._self._c;
                },
                r = [];
            n.d(a, "a", function () {
                return e;
            }), n.d(a, "b", function () {
                return r;
            });
        },
        "53c3": function (t, a, n) {
            (function (e) {
                Object.defineProperty(a, "__esModule", {
                    value: !0
                }), a.default = void 0;
                var r = n("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                staffs: "",
                                list_style: 1,
                                baseinfo: ""
                            };
                        },
                        onLoad: function (t) {
                            var a = this;
                            e.setNavigationBarTitle({
                                title: "名片列表"
                            });
                            var n = 0;
                            t.fxsid && (n = t.fxsid, a.fxsid = t.fxsid), this._baseMin(this), e.getStorageSync("suid"),
                                r.getOpenid(n, function () {
                                    a.getstaffs();
                                });
                        },
                        methods: {
                            redirectto: function (t) {
                                var a = t.currentTarget.dataset.link,
                                    n = t.currentTarget.dataset.linktype;
                                this._redirectto(a, n);
                            },
                            getstaffs: function () {
                                var a = this;
                                e.request({
                                    url: a.$baseurl + "doPagegetStaffs",
                                    data: {
                                        uniacid: a.$uniacid
                                    },
                                    success: function (t) {
                                        a.staffs = t.data.data;
                                    }
                                });
                            },
                            staffcard: function (t) {
                                var a = t.currentTarget.dataset.text;
                                e.navigateTo({
                                    url: "/pagesCards/card_info/card_info?id=" + a
                                });
                            },
                            sharestaffcard: function (t) {
                                var a = t.currentTarget.dataset.text;
                                e.navigateTo({
                                    url: "/pagesCards/card_info/card_info?id=" + a + "&share=1"
                                });
                            }
                        }
                    };
                a.default = t;
            }).call(this, n("543d").default);
        },
        5916: function (t, a, n) {
            var e = n("cc2f");
            n.n(e).a;
        },
        "92e7": function (t, a, n) {
            (function (t) {
                function a(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                n("020c"), n("921b"), a(n("66fd")), t(a(n("d5cb")).default);
            }).call(this, n("543d").createPage);
        },
        a590: function (t, a, n) {
            n.r(a);
            var e = n("53c3"),
                r = n.n(e);
            for (var i in e) "default" !== i && function (t) {
                n.d(a, t, function () {
                    return e[t];
                });
            }(i);
            a.default = r.a;
        },
        cc2f: function (t, a, n) {},
        d5cb: function (t, a, n) {
            n.r(a);
            var e = n("46b3"),
                r = n("a590");
            for (var i in r) "default" !== i && function (t) {
                n.d(a, t, function () {
                    return r[t];
                });
            }(i);
            n("5916");
            var f = n("2877"),
                c = Object(f.a)(r.default, e.a, e.b, !1, null, null, null);
            a.default = c.exports;
        }
    },
    [
        ["92e7", "common/runtime", "common/vendor"]
    ]
]);