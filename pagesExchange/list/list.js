(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesExchange/list/list"], {
        "19c5": function (t, e, a) {
            var n = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            a.d(e, "a", function () {
                return n;
            }), a.d(e, "b", function () {
                return i;
            });
        },
        3571: function (t, e, a) {},
        "374f": function (t, e, a) {
            a.r(e);
            var n = a("19c5"),
                i = a("5a1d");
            for (var o in i) "default" !== o && function (t) {
                a.d(e, t, function () {
                    return i[t];
                });
            }(o);
            a("f7f4");
            var s = a("2877"),
                c = Object(s.a)(i.default, n.a, n.b, !1, null, null, null);
            e.default = c.exports;
        },
        "5a1d": function (t, e, a) {
            a.r(e);
            var n = a("bca7"),
                i = a.n(n);
            for (var o in n) "default" !== o && function (t) {
                a.d(e, t, function () {
                    return n[t];
                });
            }(o);
            e.default = i.a;
        },
        "788a": function (t, e, a) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                a("020c"), a("921b"), e(a("66fd")), t(e(a("374f")).default);
            }).call(this, a("543d").createPage);
        },
        bca7: function (t, e, a) {
            (function (i) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var o = a("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                page_signs: "/pagesExchange/list/list",
                                tongji: [],
                                globaluser: [],
                                cate_list: [],
                                cate_list_length: 0,
                                c: "",
                                page: 1,
                                morePro: !1,
                                ProductsList: [],
                                baseinfo: [],
                                footer: {},
                                cid: 0,
                                cate: "",
                                needBind: !1,
                                needAuth: !1
                            };
                        },
                        onShareAppMessage: function () {
                            return {
                                title: this.cateinfo.name + "-" + this.baseinfo.name
                            };
                        },
                        onPullDownRefresh: function () {
                            this.getList(), this.getCate(), i.stopPullDownRefresh();
                        },
                        onLoad: function (t) {
                            var e = this,
                                a = this;
                            a.cid = t.cid;
                            var n = 0;
                            t.fxsid && (n = t.fxsid, a.fxsid = t.fxsid), this._baseMin(this), i.getStorageSync("suid"),
                                o.getOpenid(n, function () {
                                    a.getList(), a.getCate();
                                }, function () {
                                    e.needAuth = !0;
                                }, function () {
                                    e.needBind = !0;
                                });
                        },
                        onReachBottom: function () {
                            var e = this,
                                a = e.page + 1,
                                t = e.cid;
                            i.request({
                                url: e.$baseurl + "doPageScorepro",
                                data: {
                                    uniacid: e.$uniacid,
                                    cid: t,
                                    page: a
                                },
                                success: function (t) {
                                    "" != t.data.data ? (e.cate_list = e.cate_list.concat(t.data.data), e.page = a) : e.morePro = !1;
                                }
                            });
                        },
                        methods: {
                            closeAuth: function () {
                                this.needAuth = !1, i.getStorageSync("golobeuser") && this._checkBindPhone(this);
                            },
                            closeBind: function () {
                                this.needBind = !1, this.getList(), this.getCate();
                            },
                            redirectto: function (t) {
                                var e = t.currentTarget.dataset.link,
                                    a = t.currentTarget.dataset.linktype;
                                this._redirectto(e, a);
                            },
                            handleTap: function (t) {
                                var e = t.currentTarget.id.slice(1);
                                this.cid, e && (this.cid = e, this.page = 1, this.getList(e));
                            },
                            getCate: function () {
                                var e = this;
                                i.request({
                                    url: e.$baseurl + "doPageScorecate",
                                    data: {
                                        uniacid: e.$uniacid,
                                        source: i.getStorageSync("source"),
                                        suid: i.getStorageSync("suid")
                                    },
                                    success: function (t) {
                                        e.cate = t.data.data.cate, e.globaluser = t.data.data.userinfo;
                                    },
                                    fail: function (t) {}
                                });
                            },
                            getList: function (e) {
                                var a = this;
                                null == e && (e = 0), i.request({
                                    url: a.$baseurl + "doPageScorepro",
                                    cachetime: "30",
                                    data: {
                                        uniacid: a.$uniacid,
                                        cid: e
                                    },
                                    success: function (t) {
                                        a.cate_list = t.data.data, a.cate_list_length = t.data.data.length, a.cid = e, i.setNavigationBarTitle({
                                            title: "积分商城"
                                        }), i.setStorageSync("isShowLoading", !1), i.hideNavigationBarLoading(), i.stopPullDownRefresh();
                                    },
                                    fail: function (t) {}
                                });
                            },
                            makePhoneCall: function (t) {
                                var e = this.baseinfo.tel;
                                i.makePhoneCall({
                                    phoneNumber: e
                                });
                            },
                            makePhoneCallB: function (t) {
                                var e = this.baseinfo.tel_b;
                                i.makePhoneCall({
                                    phoneNumber: e
                                });
                            },
                            openMap: function (t) {
                                i.openLocation({
                                    latitude: parseFloat(this.baseinfo.latitude),
                                    longitude: parseFloat(this.baseinfo.longitude),
                                    name: this.baseinfo.name,
                                    address: this.baseinfo.address,
                                    scale: 22
                                });
                            },
                            chenggfh: function () {
                                var t = i.getStorageSync("golobeuser");
                                this.isview = 0, this.globaluser = t;
                            }
                        }
                    };
                e.default = t;
            }).call(this, a("543d").default);
        },
        f7f4: function (t, e, a) {
            var n = a("3571");
            a.n(n).a;
        }
    },
    [
        ["788a", "common/runtime", "common/vendor"]
    ]
]);