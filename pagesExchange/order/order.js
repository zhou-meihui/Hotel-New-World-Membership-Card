(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesExchange/order/order"], {
        "0390": function (e, t, n) {
            (function (i) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var o = n("f571"),
                    e = {
                        data: function () {
                            return {
                                page_signs: "/pagesExchange/order/order",
                                $imgurl: this.$imgurl,
                                baseinfo: [],
                                orderlist: [],
                                orderlist_length: 0
                            };
                        },
                        onPullDownRefresh: function () {
                            this.getList(), i.stopPullDownRefresh();
                        },
                        onLoad: function (e) {
                            var t = this,
                                n = this;
                            i.setNavigationBarTitle({
                                title: "我的积分订单"
                            }), e.type && (n.type = e.type);
                            var a = 0;
                            e.fxsid && (a = e.fxsid, n.fxsid = e.fxsid), this._baseMin(this), i.getStorageSync("suid"),
                                o.getOpenid(a, function () {
                                    n.getList();
                                }, function () {
                                    t.needAuth = !0;
                                }, function () {
                                    t.needBind = !0;
                                });
                        },
                        methods: {
                            redirectto: function (e) {
                                var t = e.currentTarget.dataset.link,
                                    n = e.currentTarget.dataset.linktype;
                                this._redirectto(t, n);
                            },
                            getList: function (e) {
                                var t = this;
                                i.request({
                                    url: t.$baseurl + "doPagemyscoreorder",
                                    data: {
                                        uniacid: t.$uniacid,
                                        suid: i.getStorageSync("suid")
                                    },
                                    success: function (e) {
                                        t.orderlist = e.data.data, t.orderlist_length = e.data.data.length;
                                    },
                                    fail: function (e) {}
                                });
                            },
                            makePhoneCall: function (e) {
                                var t = this.baseinfo.tel;
                                i.makePhoneCall({
                                    phoneNumber: t
                                });
                            },
                            makePhoneCallB: function (e) {
                                var t = this.baseinfo.tel_b;
                                i.makePhoneCall({
                                    phoneNumber: t
                                });
                            },
                            openMap: function (e) {
                                var t = this;
                                i.openLocation({
                                    latitude: parseFloat(t.baseinfo.latitude),
                                    longitude: parseFloat(t.baseinfo.longitude),
                                    name: t.baseinfo.name,
                                    address: t.baseinfo.address,
                                    scale: 22
                                });
                            }
                        }
                    };
                t.default = e;
            }).call(this, n("543d").default);
        },
        "3f79": function (e, t, n) {
            (function (e) {
                function t(e) {
                    return e && e.__esModule ? e : {
                        default: e
                    };
                }
                n("020c"), n("921b"), t(n("66fd")), e(t(n("e401")).default);
            }).call(this, n("543d").createPage);
        },
        "5b06": function (e, t, n) {},
        "97f3": function (e, t, n) {
            var a = n("5b06");
            n.n(a).a;
        },
        e401: function (e, t, n) {
            n.r(t);
            var a = n("fe1c"),
                i = n("fa55");
            for (var o in i) "default" !== o && function (e) {
                n.d(t, e, function () {
                    return i[e];
                });
            }(o);
            n("97f3");
            var r = n("2877"),
                u = Object(r.a)(i.default, a.a, a.b, !1, null, null, null);
            t.default = u.exports;
        },
        fa55: function (e, t, n) {
            n.r(t);
            var a = n("0390"),
                i = n.n(a);
            for (var o in a) "default" !== o && function (e) {
                n.d(t, e, function () {
                    return a[e];
                });
            }(o);
            t.default = i.a;
        },
        fe1c: function (e, t, n) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            n.d(t, "a", function () {
                return a;
            }), n.d(t, "b", function () {
                return i;
            });
        }
    },
    [
        ["3f79", "common/runtime", "common/vendor"]
    ]
]);