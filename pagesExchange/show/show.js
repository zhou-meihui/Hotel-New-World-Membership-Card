(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesExchange/show/show"], {
        "020e": function (t, e, a) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                a("020c"), a("921b"), e(a("66fd")), t(e(a("7d07")).default);
            }).call(this, a("543d").createPage);
        },
        "0907": function (t, e, a) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                n = [];
            a.d(e, "a", function () {
                return i;
            }), a.d(e, "b", function () {
                return n;
            });
        },
        "3a2e": function (t, a, o) {
            (function (u) {
                Object.defineProperty(a, "__esModule", {
                    value: !0
                }), a.default = void 0;
                var t, i = (t = o("3584")) && t.__esModule ? t : {
                    default: t
                };
                var n = o("f571"),
                    e = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                baseinfo: [],
                                orderlist: [],
                                id: "",
                                sc: 0,
                                bg: "",
                                datas: {
                                    labels: [],
                                    slide: []
                                },
                                content: "",
                                jhsl: 1,
                                dprice: "",
                                yhje: 0,
                                hjjg: "",
                                sfje: "",
                                order: "",
                                my_num: "",
                                xg_num: "",
                                shengyu: "",
                                userInfo: "",
                                num: [],
                                xz_num: [],
                                proinfo: "",
                                pic_video: "",
                                isplay: !1,
                                currentSwiper: 0,
                                minHeight: 220,
                                heighthave: 0,
                                autoplay: !0,
                                dlength: 0,
                                needAuth: !1,
                                needBind: !1
                            };
                        },
                        onShareAppMessage: function () {
                            return {
                                title: this.title
                            };
                        },
                        onPullDownRefresh: function () {
                            u.stopPullDownRefresh();
                        },
                        onLoad: function (t) {
                            var e = this,
                                a = t.id;
                            e.id = a;
                            var i = 0;
                            t.fxsid && (i = t.fxsid, u.setStorageSync("fxsid", i), e.fxsid = t.fxsid), this._baseMin(this),
                                u.getStorageSync("suid"), n.getOpenid(i, function () {
                                    var t = e.id;
                                    e.getShowPic(t);
                                });
                        },
                        methods: {
                            makePhoneCall: function (t) {
                                var e = this.baseinfo.tel;
                                u.makePhoneCall({
                                    phoneNumber: e
                                });
                            },
                            collect: function (t) {
                                var a = this;
                                if (!this.getSuid()) return !1;
                                t.currentTarget.dataset.name, u.request({
                                    url: a.$baseurl + "doPageCollect",
                                    data: {
                                        uniacid: a.$uniacid,
                                        suid: wx.getStorageSync("suid"),
                                        types: "exchange",
                                        id: a.id
                                    },
                                    header: {
                                        "content-type": "application/json"
                                    },
                                    success: function (t) {
                                        var e = t.data.data;
                                        a.sc = "收藏成功" == e ? 1 : 0, u.showToast({
                                            title: e,
                                            icon: "succes",
                                            duration: 1e3,
                                            mask: !0
                                        });
                                    }
                                });
                            },
                            redirectto: function (t) {
                                var e = t.currentTarget.dataset.link,
                                    a = t.currentTarget.dataset.linktype;
                                this._redirectto(e, a);
                            },
                            getShowPic: function (t) {
                                var e = this,
                                    a = u.getStorageSync("suid");
                                u.request({
                                    url: e.$baseurl + "doPageScoreinfo",
                                    data: {
                                        uniacid: e.$uniacid,
                                        id: t,
                                        suid: a
                                    },
                                    success: function (t) {
                                        e.datas = t.data.data, e.dlength = e.datas.slide.length, e.datas.product_txt && (e.datas.product_txt = e.datas.product_txt.replace(/\<img/gi, '<img style="width:100%;height:auto;display:block" '),
                                                e.datas.product_txt = (0, i.default)(e.datas.product_txt)), e.pic_video = t.data.data.video,
                                            e.sc = t.data.data.sc, u.setNavigationBarTitle({
                                                title: t.data.data.title
                                            }), u.hideNavigationBarLoading(), u.stopPullDownRefresh();
                                    }
                                });
                            },
                            save: n.throttle(function (i) {
                                var n = this;
                                if (!this.getSuid()) return !1;
                                n.jhsl;
                                var o = u.getStorageSync("suid"),
                                    s = n.id;
                                u.request({
                                    url: n.$baseurl + "doPagecheckvip",
                                    data: {
                                        uniacid: n.$uniacid,
                                        kwd: "exchange",
                                        suid: o
                                    },
                                    success: function (t) {
                                        if (t.data.data) {
                                            var e = u.getStorageSync("subscribe");
                                            if (0 < e.length && "" != e[9].mid) {
                                                var a = new Array();
                                                a.push(e[9].mid), u.requestSubscribeMessage({
                                                    tmplIds: a,
                                                    success: function (t) {
                                                        u.showModal({
                                                            title: "提示",
                                                            content: "确定兑换此商品吗？",
                                                            success: function (t) {
                                                                t.confirm ? u.request({
                                                                    url: n.$baseurl + "doPageScoreorder",
                                                                    data: {
                                                                        uniacid: n.$uniacid,
                                                                        suid: o,
                                                                        id: s,
                                                                        formId: i.detail.formId,
                                                                        openid: u.getStorageSync("openid"),
                                                                        source: u.getStorageSync("source")
                                                                    },
                                                                    header: {
                                                                        "content-type": "application/json"
                                                                    },
                                                                    success: function (t) {
                                                                        var e = t.data.data;
                                                                        0 == e.flag ? u.showModal({
                                                                            title: "提醒",
                                                                            content: e.msg,
                                                                            showCancel: !1
                                                                        }) : u.showToast({
                                                                            title: "兑换成功",
                                                                            icon: "success",
                                                                            duration: 1e3,
                                                                            success: function () {
                                                                                setTimeout(function () {
                                                                                    u.redirectTo({
                                                                                        url: "/pagesExchange/order/order"
                                                                                    });
                                                                                }, 1e3);
                                                                            }
                                                                        });
                                                                    }
                                                                }) : t.cancel;
                                                            }
                                                        });
                                                    }
                                                });
                                            } else u.showModal({
                                                title: "提示",
                                                content: "确定兑换此商品吗？",
                                                success: function (t) {
                                                    t.confirm ? u.request({
                                                        url: n.$baseurl + "doPageScoreorder",
                                                        data: {
                                                            uniacid: n.$uniacid,
                                                            suid: o,
                                                            id: s,
                                                            formId: i.detail.formId,
                                                            openid: u.getStorageSync("openid"),
                                                            source: u.getStorageSync("source")
                                                        },
                                                        header: {
                                                            "content-type": "application/json"
                                                        },
                                                        success: function (t) {
                                                            var e = t.data.data;
                                                            0 == e.flag ? u.showModal({
                                                                title: "提醒",
                                                                content: e.msg,
                                                                showCancel: !1
                                                            }) : u.showToast({
                                                                title: "兑换成功",
                                                                icon: "success",
                                                                duration: 1e3,
                                                                success: function () {
                                                                    setTimeout(function () {
                                                                        u.redirectTo({
                                                                            url: "/pagesExchange/order/order"
                                                                        });
                                                                    }, 1e3);
                                                                }
                                                            });
                                                        }
                                                    }) : t.cancel;
                                                }
                                            });
                                        } else u.showModal({
                                            title: "进入失败",
                                            content: "使用本功能需先开通vip!",
                                            showCancel: !1,
                                            success: function (t) {
                                                t.confirm && u.navigateTo({
                                                    url: "/pages/register/register?type=jifen"
                                                });
                                            }
                                        });
                                    },
                                    fail: function (t) {}
                                });
                            }, 2e3),
                            tabChange: function (t) {
                                var e = t.currentTarget.dataset.id;
                                this.nowcon = e;
                            },
                            swiperLoad: function (i) {
                                var n = this;
                                u.getSystemInfo({
                                    success: function (t) {
                                        var e = i.detail.width / i.detail.height,
                                            a = t.windowWidth / e;
                                        n.heighthave || (n.minHeight = a, n.heighthave = 1);
                                    }
                                });
                            },
                            swiperChange: function (t) {
                                this.autoplay = !0, this.currentSwiper = t.detail.current, this.isplay = !1, this.autoplay = this.autoplay;
                            },
                            playvideo: function () {
                                this.autoplay = !1, this.isplay = !0, this.autoplay = this.autoplay;
                            },
                            endvideo: function () {
                                this.autoplay = !0, this.isplay = !1, this.autoplay = this.autoplay;
                            },
                            getSuid: function () {
                                if (u.getStorageSync("suid")) return !0;
                                return u.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                    !1;
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1;
                            }
                        }
                    };
                a.default = e;
            }).call(this, o("543d").default);
        },
        "7d07": function (t, e, a) {
            a.r(e);
            var i = a("0907"),
                n = a("c9e9");
            for (var o in n) "default" !== o && function (t) {
                a.d(e, t, function () {
                    return n[t];
                });
            }(o);
            a("fa88");
            var s = a("2877"),
                u = Object(s.a)(n.default, i.a, i.b, !1, null, null, null);
            e.default = u.exports;
        },
        c9e9: function (t, e, a) {
            a.r(e);
            var i = a("3a2e"),
                n = a.n(i);
            for (var o in i) "default" !== o && function (t) {
                a.d(e, t, function () {
                    return i[t];
                });
            }(o);
            e.default = n.a;
        },
        de2f: function (t, e, a) {},
        fa88: function (t, e, a) {
            var i = a("de2f");
            a.n(i).a;
        }
    },
    [
        ["020e", "common/runtime", "common/vendor"]
    ]
]);