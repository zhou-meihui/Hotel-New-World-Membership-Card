(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesExternal/jdGoods/jdGoods"], {
        1958: function (t, a, e) {
            var n = function () {
                    this.$createElement;
                    this._self._c;
                },
                o = [];
            e.d(a, "a", function () {
                return n;
            }), e.d(a, "b", function () {
                return o;
            });
        },
        b066: function (t, a, e) {
            (function (t) {
                function a(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                e("020c"), e("921b"), a(e("66fd")), t(a(e("e449")).default);
            }).call(this, e("543d").createPage);
        },
        d023: function (t, a, e) {
            e.r(a);
            var n = e("f2ff"),
                o = e.n(n);
            for (var i in n) "default" !== i && function (t) {
                e.d(a, t, function () {
                    return n[t];
                });
            }(i);
            a.default = o.a;
        },
        dfb5: function (t, a, e) {},
        e39c: function (t, a, e) {
            var n = e("dfb5");
            e.n(n).a;
        },
        e449: function (t, a, e) {
            e.r(a);
            var n = e("1958"),
                o = e("d023");
            for (var i in o) "default" !== i && function (t) {
                e.d(a, t, function () {
                    return o[t];
                });
            }(i);
            e("e39c");
            var s = e("2877"),
                r = Object(s.a)(o.default, n.a, n.b, !1, null, null, null);
            a.default = r.exports;
        },
        f2ff: function (t, a, o) {
            (function (e) {
                Object.defineProperty(a, "__esModule", {
                    value: !0
                }), a.default = void 0;
                var n = o("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                baseinfo: "",
                                page: 1,
                                minHeight: 180,
                                morePro: !1,
                                goods: [],
                                flag: 0,
                                sort: 1
                            };
                        },
                        onLoad: function (t) {
                            var a = this;
                            this.page_signs = "/pagesExternal/jdGoods/jdGoods", this._baseMin(this);
                            var e = 0;
                            t.fxsid && (e = t.fxsid), this.fxsid = e, n.getOpenid(e, function () {
                                a.getList();
                            });
                        },
                        onReachBottom: function () {
                            var a = this;
                            this.page = this.page + 1, e.request({
                                url: a.$baseurl + "doPageGetExternalGoods",
                                data: {
                                    page: a.page,
                                    type: "jd",
                                    flag: a.flag,
                                    sort: a.sort,
                                    uniacid: a.$uniacid
                                },
                                success: function (t) {
                                    0 < t.data.data.length ? a.goods = a.goods.concat(t.data.data) : e.showToast({
                                        title: "已经到底了",
                                        icon: "none"
                                    });
                                }
                            });
                        },
                        methods: {
                            getList: function () {
                                var a = this;
                                e.request({
                                    url: a.$baseurl + "doPageGetExternalGoods",
                                    data: {
                                        page: a.page,
                                        type: "jd",
                                        flag: a.flag,
                                        sort: a.sort,
                                        uniacid: a.$uniacid
                                    },
                                    success: function (t) {
                                        a.goods = t.data.data;
                                    }
                                });
                            },
                            changflag: function (t) {
                                this.flag = t.currentTarget.dataset.flag, 2 == this.flag && (1 == this.sort ? this.sort = 2 : this.sort = 1),
                                    this.page = 1, this.getList();
                            },
                            redirectto: function (t) {
                                var a = t.currentTarget.dataset.link,
                                    e = t.currentTarget.dataset.linktype;
                                this._redirectto(a, e);
                            }
                        }
                    };
                a.default = t;
            }).call(this, o("543d").default);
        }
    },
    [
        ["b066", "common/runtime", "common/vendor"]
    ]
]);