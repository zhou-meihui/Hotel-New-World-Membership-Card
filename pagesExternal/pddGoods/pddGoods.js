(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesExternal/pddGoods/pddGoods"], {
        "037a": function (t, a, n) {
            n.r(a);
            var e = n("4bfd"),
                o = n("ad10");
            for (var i in o) "default" !== i && function (t) {
                n.d(a, t, function () {
                    return o[t];
                });
            }(i);
            n("b48d");
            var s = n("2877"),
                d = Object(s.a)(o.default, e.a, e.b, !1, null, null, null);
            a.default = d.exports;
        },
        "222c": function (t, a, n) {},
        3040: function (t, a, o) {
            (function (n) {
                Object.defineProperty(a, "__esModule", {
                    value: !0
                }), a.default = void 0;
                var e = o("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                baseinfo: "",
                                page: 1,
                                minHeight: 180,
                                morePro: !1,
                                goods: [],
                                flag: 0,
                                sort: 1
                            };
                        },
                        onLoad: function (t) {
                            var a = this;
                            this.page_signs = "/pagesExternal/pddGoods/pddGoods", this._baseMin(this);
                            var n = 0;
                            t.fxsid && (n = t.fxsid), this.fxsid = n, e.getOpenid(n, function () {
                                a.getList();
                            });
                        },
                        onReachBottom: function () {
                            var a = this;
                            this.page = this.page + 1, n.request({
                                url: a.$baseurl + "doPageGetExternalGoods",
                                data: {
                                    page: a.page,
                                    type: "pdd",
                                    flag: a.flag,
                                    sort: a.sort,
                                    uniacid: a.$uniacid
                                },
                                success: function (t) {
                                    0 < t.data.data.length ? a.goods = a.goods.concat(t.data.data) : n.showToast({
                                        title: "已经到底了",
                                        icon: "none"
                                    });
                                }
                            });
                        },
                        methods: {
                            getList: function () {
                                var a = this;
                                n.request({
                                    url: a.$baseurl + "doPageGetExternalGoods",
                                    data: {
                                        page: a.page,
                                        type: "pdd",
                                        flag: a.flag,
                                        sort: a.sort,
                                        uniacid: a.$uniacid
                                    },
                                    success: function (t) {
                                        a.goods = t.data.data;
                                    }
                                });
                            },
                            redirectto: function (t) {
                                var a = t.currentTarget.dataset.link,
                                    n = t.currentTarget.dataset.linktype;
                                this._redirectto(a, n);
                            },
                            changflag: function (t) {
                                this.flag = t.currentTarget.dataset.flag, 2 == this.flag && (1 == this.sort ? this.sort = 2 : this.sort = 1),
                                    this.page = 1, this.getList();
                            }
                        }
                    };
                a.default = t;
            }).call(this, o("543d").default);
        },
        "4bfd": function (t, a, n) {
            var e = function () {
                    this.$createElement;
                    this._self._c;
                },
                o = [];
            n.d(a, "a", function () {
                return e;
            }), n.d(a, "b", function () {
                return o;
            });
        },
        a06c: function (t, a, n) {
            (function (t) {
                function a(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                n("020c"), n("921b"), a(n("66fd")), t(a(n("037a")).default);
            }).call(this, n("543d").createPage);
        },
        ad10: function (t, a, n) {
            n.r(a);
            var e = n("3040"),
                o = n.n(e);
            for (var i in e) "default" !== i && function (t) {
                n.d(a, t, function () {
                    return e[t];
                });
            }(i);
            a.default = o.a;
        },
        b48d: function (t, a, n) {
            var e = n("222c");
            n.n(e).a;
        }
    },
    [
        ["a06c", "common/runtime", "common/vendor"]
    ]
]);