(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesFenxiao/fenxiao_apply/fenxiao_apply"], {
        3248: function (e, t, n) {
            n.r(t);
            var a = n("5bf1"),
                i = n.n(a);
            for (var o in a) "default" !== o && function (e) {
                n.d(t, e, function () {
                    return a[e];
                });
            }(o);
            t.default = i.a;
        },
        3249: function (e, t, n) {},
        "45de": function (e, t, n) {
            (function (e) {
                function t(e) {
                    return e && e.__esModule ? e : {
                        default: e
                    };
                }
                n("020c"), n("921b"), t(n("66fd")), e(t(n("93f9")).default);
            }).call(this, n("543d").createPage);
        },
        "4c1b": function (e, t, n) {
            var a = n("3249");
            n.n(a).a;
        },
        "5bf1": function (e, n, a) {
            (function (f) {
                Object.defineProperty(n, "__esModule", {
                    value: !0
                }), n.default = void 0;
                var e, u = (e = a("3584")) && e.__esModule ? e : {
                    default: e
                };
                var i = a("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                banner: "",
                                page_signs: "/pagesFenxiao/fenxiao_apply/fenxiao_apply",
                                indicatorDots: !1,
                                autoplay: !1,
                                interval: 5e3,
                                duration: 1e3,
                                check: 0,
                                xieyi_block: 0,
                                input_name: "",
                                input_tel: "",
                                fxs_name: "",
                                fxs: "",
                                item: "",
                                needAuth: !1,
                                needBind: !1,
                                baseinfo: {},
                                content: "",
                                fx_msg: ""
                            };
                        },
                        onLoad: function (e) {
                            var t = this,
                                n = this;
                            n._baseMin(n);
                            var a = 0;
                            e.fxsid && (a = e.fxsid), this.fxsid = a, f.getStorageSync("suid"), i.getOpenid(a, function () {
                                n.fxssq();
                            }, function () {
                                t.needAuth = !0;
                            }, function () {
                                t.needBind = !0;
                            });
                        },
                        onPullDownRefresh: function () {
                            this.fxssq(), f.stopPullDownRefresh();
                        },
                        methods: {
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1, this.fxssq();
                            },
                            getSuid: function () {
                                if (f.getStorageSync("suid")) return !0;
                                return f.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                    !1;
                            },
                            fxssq: function () {
                                var s = this,
                                    e = f.getStorageSync("suid");
                                f.request({
                                    url: s.$baseurl + "dopagesqcwfxsbase",
                                    data: {
                                        uniacid: s.$uniacid,
                                        suid: e,
                                        source: f.getStorageSync("source")
                                    },
                                    success: function (e) {
                                        console.log(e.data.data);
                                        var t = e.data.data.sq,
                                            n = e.data.data.userinfo,
                                            a = e.data.data.gz,
                                            i = a.sq_thumb;
                                        s.item = i, s.fxs_name = a.fxs_name, s.fx_msg = a.fx_msg, f.setNavigationBarTitle({
                                            title: "申请成为" + a.fxs_name + "的分销商"
                                        }), 2 == n.fxs && 2 != n.fxsstop ? f.redirectTo({
                                            url: "/pagesFenxiao/fenxiao_center/fenxiao_center"
                                        }) : (2 == a.fxs_sz && t && 1 == t.flag && f.redirectTo({
                                            url: "/pagesFenxiao/fenxiao_s/fenxiao_s?type=1"
                                        }), 4 == a.fxs_sz && f.redirectTo({
                                            url: "/pagesFenxiao/fenxiao_s/fenxiao_s?type=3"
                                        })), s.input_tel = n.phone, s.fxs = e.data.data.fxs;
                                        var o = e.data.data.gz.fxs_xy.replace(/\<img/gi, '<img style="max-width:100%;height:auto" ');
                                        o = (0, u.default)(o), s.content = o, console.log(s.content);
                                    }
                                });
                            },
                            subs: function (e) {
                                if (!this.getSuid()) return !1;
                                var t = this,
                                    n = t.input_name,
                                    a = t.input_tel,
                                    i = t.check,
                                    o = e.detail.formId;
                                if (0 == i) return f.showToast({
                                    title: "请先阅读协议",
                                    icon: "none"
                                }), !1;
                                if ("" == n) return f.showToast({
                                    title: "请先输入姓名",
                                    icon: "none"
                                }), !1;
                                if (a.length < 11) return f.showToast({
                                    title: "请输入正确手机号",
                                    icon: "none"
                                }), !1;
                                if ("" == a) return f.showToast({
                                    title: "请先输入手机号",
                                    icon: "none"
                                }), !1;
                                var s = f.getStorageSync("suid"),
                                    u = f.getStorageSync("openid"),
                                    c = f.getStorageSync("source");
                                if (s) {
                                    var r = f.getStorageSync("subscribe");
                                    if (0 < r.length && r[6].mid) {
                                        var d = new Array();
                                        d.push(r[6].mid), f.requestSubscribeMessage({
                                            tmplIds: d,
                                            success: function (e) {
                                                f.request({
                                                    url: t.$baseurl + "dopagesqcwfxs",
                                                    data: {
                                                        uniacid: t.$uniacid,
                                                        truename: n,
                                                        truetel: a,
                                                        suid: s,
                                                        openid: u,
                                                        source: c,
                                                        formid: o
                                                    },
                                                    success: function (e) {
                                                        if (e.data.data < 3) {
                                                            var t = ""; -
                                                            1 == e.data.data && (t = "恭喜您申请成功！"), 1 == e.data.data && (t = "您的申请正在审核中"), 2 == e.data.data && (t = "您已经是分销商了"),
                                                                f.showModal({
                                                                    title: "提醒",
                                                                    content: t,
                                                                    showCancel: !1,
                                                                    success: function () {
                                                                        f.redirectTo({
                                                                            url: "/pagesFenxiao/fenxiao_center/fenxiao_center"
                                                                        });
                                                                    }
                                                                });
                                                        } else f.redirectTo({
                                                            url: "/pagesFenxiao/fenxiao_s/fenxiao_s?type=2"
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    } else f.request({
                                        url: t.$baseurl + "dopagesqcwfxs",
                                        data: {
                                            uniacid: t.$uniacid,
                                            truename: n,
                                            truetel: a,
                                            suid: s,
                                            openid: u,
                                            source: c,
                                            formid: o
                                        },
                                        success: function (e) {
                                            if (e.data.data < 3) {
                                                var t = ""; -
                                                1 == e.data.data && (t = "恭喜您申请成功！"), 1 == e.data.data && (t = "您的申请正在审核中"), 2 == e.data.data && (t = "您已经是分销商了"),
                                                    f.showModal({
                                                        title: "提醒",
                                                        content: t,
                                                        showCancel: !1,
                                                        success: function () {
                                                            f.redirectTo({
                                                                url: "/pagesFenxiao/fenxiao_center/fenxiao_center"
                                                            });
                                                        }
                                                    });
                                            } else f.redirectTo({
                                                url: "/pagesFenxiao/fenxiao_s/fenxiao_s?type=2"
                                            });
                                        }
                                    });
                                }
                            },
                            xieyi_close: function () {
                                this.xieyi_block = 0;
                            },
                            xieyi_hidden: function () {
                                this.xieyi_block = 1, this.check = 1;
                            },
                            check_change: function () {
                                var e = this.check;
                                this.check = 0 == e ? 1 : 0;
                            },
                            change_name: function (e) {
                                this.input_name = e.detail.value;
                            },
                            change_tel: function (e) {
                                this.input_tel = e.detail.value;
                            }
                        }
                    };
                n.default = t;
            }).call(this, a("543d").default);
        },
        "93f9": function (e, t, n) {
            n.r(t);
            var a = n("c277"),
                i = n("3248");
            for (var o in i) "default" !== o && function (e) {
                n.d(t, e, function () {
                    return i[e];
                });
            }(o);
            n("4c1b");
            var s = n("2877"),
                u = Object(s.a)(i.default, a.a, a.b, !1, null, null, null);
            t.default = u.exports;
        },
        c277: function (e, t, n) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            n.d(t, "a", function () {
                return a;
            }), n.d(t, "b", function () {
                return i;
            });
        }
    },
    [
        ["45de", "common/runtime", "common/vendor"]
    ]
]);