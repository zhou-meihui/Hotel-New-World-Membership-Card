(global.webpackJsonp = global.webpackJsonp || []).push([
  ["pagesFenxiao/fenxiao_center/fenxiao_center"], {
    aac3: function (e, a, n) {
      (function (e) {
        function a(e) {
          return e && e.__esModule ? e : {
            default: e
          };
        }
        n("020c"), n("921b"), a(n("66fd")), e(a(n("c2e0")).default);
      }).call(this, n("543d").createPage);
    },
    bc55: function (e, a, n) {
      var t = n("e319");
      n.n(t).a;
    },
    c2e0: function (e, a, n) {
      n.r(a);
      var t = n("e3b4"),
        i = n("e990");
      for (var o in i) "default" !== o && function (e) {
        n.d(a, e, function () {
          return i[e];
        });
      }(o);
      n("bc55");
      var s = n("2877"),
        r = Object(s.a)(i.default, t.a, t.b, !1, null, null, null);
      a.default = r.exports;
    },
    e319: function (e, a, n) { },
    e3b4: function (e, a, n) {
      var t = function () {
        this.$createElement;
        this._self._c;
      },
        i = [];
      n.d(a, "a", function () {
        return t;
      }), n.d(a, "b", function () {
        return i;
      });
    },
    e969: function (e, a, n) {
      (function (o) {
        Object.defineProperty(a, "__esModule", {
          value: !0
        }), a.default = void 0;
        var i = n("f571"),
          e = {
            data: function () {
              return {
                $imgurl: this.$imgurl,
                baseinfo: {},
                superuser: {},
                usercenter: {},
                page_signs: "/pagesFenxiao/fenxiao_center/fenxiao_center",
                needAuth: !1,
                needBind: !1,
                shareimg: 0,
                shareimg_url: 0,
                img_w: "",
                img_h: "",
                system_w: 0,
                system_h: 0
              };
            },
            onLoad: function (e) {
              var a = this;
              this._baseMin(this);
              var n = 0;
              e.fxsid && (n = e.fxsid), this.fxsid = n, o.getStorageSync("suid"), i.getOpenid(n, function () {
                a._getSuperUserInfo(a);
              }, function () {
                a.needAuth = !0;
              }, function () {
                a.needBind = !0;
              });
              var t = o.getStorageSync("systemInfo");
              this.img_w = parseInt((.65 * t.windowWidth).toFixed(0)), this.img_h = parseInt((1.875 * this.img_w).toFixed(0)),
                this.system_w = parseInt(t.windowWidth), this.system_h = parseInt(t.windowHeight);
            },
            onPullDownRefresh: function () {
              this._getSuperUserInfo(this), this.fxzxdata(), o.stopPullDownRefresh();
            },
            onShow: function () {
              this.fxzxdata();
            },
            onShareAppMessage: function () {
              var e = o.getStorageSync("suid");
              return {
                title: "分销中心",
                path: 1 == this.superuser.fxs ? "/pagesFenxiao/fenxiao_center/fenxiao_center?userid=" + e : "/pagesFenxiao/fenxiao_center/fenxiao_center?userid=" + e + "&fxsid=" + e
              };
            },
            methods: {
              closeAuth: function () {
                this.needAuth = !1, this.fxzxdata(), o.getStorageSync("golobeuser") && this._checkBindPhone(this);
              },
              closeBind: function () {
                this.needBind = !1, this._getSuperUserInfo(this), this.fxzxdata();
              },
              fxzxdata: function () {
                var i = this,
                  e = o.getStorageSync("suid");
                e || o.showModal({
                  title: "提示",
                  content: "请申请成为分销商！",
                  showCancel: !1,
                  success: function (e) {
                    return o.redirectTo({
                      url: "/pagesFenxiao/fenxiao_apply/fenxiao_apply"
                    }), !1;
                  }
                }), o.request({
                  url: this.$baseurl + "dopagefxszhongx",
                  data: {
                    uniacid: this.$uniacid,
                    suid: e
                  },
                  success: function (e) {
                    var a = e.data.data.sq,
                      n = e.data.data.user,
                      t = e.data.data.guiz;
                    a ? (1 == a.flag && o.redirectTo({
                      url: "/pagesFenxiao/fenxiao_s/fenxiao_s?type=1"
                    }), 3 == a.flag && o.redirectTo({
                      url: "/pagesFenxiao/fenxiao_apply/fenxiao_apply"
                    })) : (1 == n.fxs && 1 == t.fxs_sz && o.redirectTo({
                      url: "/pagesFenxiao/fenxiao_apply/fenxiao_apply"
                    }), 1 == n.fxs && 2 == t.fxs_sz && o.redirectTo({
                      url: "/pagesFenxiao/fenxiao_apply/fenxiao_apply"
                    }), 1 == n.fxs && 3 == t.fxs_sz && o.redirectTo({
                      url: "/pagesFenxiao/fenxiao_s/fenxiao_s?type=4"
                    }), 1 == n.fxs && 4 == t.fxs_sz && o.redirectTo({
                      url: "/pagesFenxiao/fenxiao_s/fenxiao_s?type=3"
                    })), 2 == n.fxsstop && o.showModal({
                      title: "提示",
                      content: "您的分销商身份已被禁用，点击确定重新申请",
                      showCancel: !1,
                      success: function (e) {
                        return o.redirectTo({
                          url: "/pagesFenxiao/fenxiao_apply/fenxiao_apply"
                        }), !1;
                      }
                    }), console.log(e.data.data), i.usercenter = e.data.data;
                  }
                });
              },
              goshare: function () {
                var a = this;
                o.showLoading({
                  title: "二维码生成中"
                });
                var e = o.getStorageSync("suid");
                o.request({
                  url: a.$baseurl + "dopagefxshareewm",
                  data: {
                    uniacid: a.$uniacid,
                    suid: e,
                    source: o.getStorageSync("source"),
                    pageUrl: "fenxiao"
                  },
                  success: function (e) {
                    0 == e.data.data.error ? (a.shareimg = 1, a.shareimg_url = e.data.data.url, o.hideLoading()) : (o.hideLoading(),
                      o.showModal({
                        title: "提示",
                        showCancel: !1,
                        content: "二维码生成失败，请稍后再试",
                        success: function (e) { }
                      }));
                  }
                });
              },
              closeShare: function () {
                this.shareimg = 0;
              },
              saveImg: function () {
                var a = this;
                o.getImageInfo({
                  src: a.shareimg_url,
                  success: function (e) {
                    o.saveImageToPhotosAlbum({
                      filePath: e.path,
                      success: function () {
                        o.showToast({
                          title: "保存成功！",
                          icon: "none"
                        }), a.shareimg = 0, a.share = 0;
                      }
                    });
                  }
                });
              },
              aliSaveImg: function () {
                var a = this;
                o.getImageInfo({
                  src: a.shareimg_url,
                  success: function (e) {
                    my.saveImage({
                      url: e.path,
                      showActionSheet: !0,
                      success: function () {
                        my.alert({
                          title: "保存成功"
                        }), a.shareimg = 0, a.share = 0;
                      }
                    });
                  }
                });
              },
              my_team: function () {
                o.navigateTo({
                  url: "/pagesFenxiao/fenxiao_team/fenxiao_team"
                });
              },
              fenxiao_account: function () {
                o.navigateTo({
                  url: "/pagesFenxiao/fenxiao_account/fenxiao_account"
                });
              },
              account_tixian: function () {
                o.navigateTo({
                  url: "/pagesFenxiao/fenxiao_tixian/fenxiao_tixian"
                });
              },
              fenxiao_order: function () {
                o.navigateTo({
                  url: "/pagesFenxiao/fenxiao_order/fenxiao_order"
                });
              },
              tixian_record: function () {
                o.navigateTo({
                  url: "/pagesFenxiao/fenxiao_tixian_do/fenxiao_tixian_do"
                });
              }
            }
          };
        a.default = e;
      }).call(this, n("543d").default);
    },
    e990: function (e, a, n) {
      n.r(a);
      var t = n("e969"),
        i = n.n(t);
      for (var o in t) "default" !== o && function (e) {
        n.d(a, e, function () {
          return t[e];
        });
      }(o);
      a.default = i.a;
    }
  },
  [
    ["aac3", "common/runtime", "common/vendor"]
  ]
]);