(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesFenxiao/fenxiao_order/fenxiao_order"], {
        2643: function (n, e, t) {
            (function (n) {
                function e(n) {
                    return n && n.__esModule ? n : {
                        default: n
                    };
                }
                t("020c"), t("921b"), e(t("66fd")), n(e(t("554f")).default);
            }).call(this, t("543d").createPage);
        },
        "554f": function (n, e, t) {
            t.r(e);
            var i = t("d5ae"),
                a = t("9801");
            for (var o in a) "default" !== o && function (n) {
                t.d(e, n, function () {
                    return a[n];
                });
            }(o);
            t("7afd");
            var u = t("2877"),
                s = Object(u.a)(a.default, i.a, i.b, !1, null, null, null);
            e.default = s.exports;
        },
        "61a7": function (n, e, t) {},
        "7afd": function (n, e, t) {
            var i = t("61a7");
            t.n(i).a;
        },
        9801: function (n, e, t) {
            t.r(e);
            var i = t("e0c9"),
                a = t.n(i);
            for (var o in i) "default" !== o && function (n) {
                t.d(e, n, function () {
                    return i[n];
                });
            }(o);
            e.default = a.a;
        },
        d5ae: function (n, e, t) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                a = [];
            t.d(e, "a", function () {
                return i;
            }), t.d(e, "b", function () {
                return a;
            });
        },
        e0c9: function (n, e, a) {
            (function (t) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var i = a("f571"),
                    n = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                nav: 1,
                                page_signs: "/pagesFenxiao/fenxiao_order/fenxiao_order",
                                needAuth: !1,
                                needBind: !1,
                                baseinfo: {},
                                orderscount: {},
                                orders: []
                            };
                        },
                        onLoad: function (n) {
                            var e = this;
                            this._baseMin(this);
                            var t = 0;
                            n.fxsid && (t = n.fxsid), this.fxsid = t, i.getOpenid(t, function () {
                                e.getbase(), e.getlistqf(1);
                            }, function () {
                                e.needAuth = !0;
                            }, function () {
                                e.needBind = !0;
                            });
                        },
                        onPullDownRefresh: function () {
                            this.getbase(), this.getlistqf(1), t.stopPullDownRefresh();
                        },
                        methods: {
                            closeAuth: function () {
                                this.needAuth = !1, this._checkBindPhone(this);
                            },
                            closeBind: function () {
                                this.needBind = !1, this.getbase(), this.getlistqf(1);
                            },
                            getbase: function () {
                                var e = this;
                                t.request({
                                    url: this.$baseurl + "doPagefxcount",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: t.getStorageSync("suid")
                                    },
                                    success: function (n) {
                                        e.orderscount = n.data.data;
                                    }
                                });
                            },
                            getlistqf: function (n) {
                                var e = this;
                                t.request({
                                    url: this.$baseurl + "doPagefxdingd",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: t.getStorageSync("suid"),
                                        types: n
                                    },
                                    success: function (n) {
                                        console.log(n), e.orders = n.data;
                                    }
                                });
                            },
                            goshop: function () {
                                t.redirectTo({
                                    url: "/pages/index/index"
                                });
                            },
                            navchange: function (n) {
                                var e = n.currentTarget.dataset.id;
                                this.nav = e, this.getlistqf(e);
                            }
                        }
                    };
                e.default = n;
            }).call(this, a("543d").default);
        }
    },
    [
        ["2643", "common/runtime", "common/vendor"]
    ]
]);