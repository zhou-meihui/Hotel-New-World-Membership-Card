(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesFlashSale/orderDetail_dan/orderDetail_dan"], {
        "0402": function (t, a, e) {
            var s = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            e.d(a, "a", function () {
                return s;
            }), e.d(a, "b", function () {
                return i;
            });
        },
        "1b75": function (t, a, e) {
            (function (t) {
                function a(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                e("020c"), e("921b"), a(e("66fd")), t(a(e("ed5f")).default);
            }).call(this, e("543d").createPage);
        },
        "6de4": function (t, a, e) {
            var s = e("ac70");
            e.n(s).a;
        },
        ac70: function (t, a, e) {},
        eaf7: function (t, e, i) {
            (function (o) {
                function a(t, a, e) {
                    return a in t ? Object.defineProperty(t, a, {
                        value: e,
                        enumerable: !0,
                        configurable: !0,
                        writable: !0
                    }) : t[a] = e, t;
                }
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var s = i("f571"),
                    t = {
                        data: function () {
                            var t;
                            return {
                                $imgurl: this.$imgurl,
                                baseinfo: "",
                                tabbar: "",
                                orderid: "",
                                state: 1,
                                showmask: 0,
                                datas: (t = {
                                    jsondata: [{
                                        baseinfo: []
                                    }],
                                    yhInfo_yhq: [],
                                    yhInfo_score: []
                                }, a(t, "yhInfo_yhq", []), a(t, "yhInfo_mj", []), a(t, "store_info", []), t),
                                orderFormDisable: !0,
                                isChange: "",
                                formchangeBtn: 2,
                                kuaidi: ["选择快递", "圆通", "中通", "申通", "顺丰", "韵达", "天天", "EMS", "百世", "本人到店", "其他"],
                                index: "",
                                showhx: 0,
                                hxmm: "",
                                hxmm_list: [{
                                    val: "",
                                    fs: !0
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }],
                                hx_choose: 0,
                                hx_ewm: "",
                                showPay: 0,
                                choosepayf: 0,
                                mymoney: 0,
                                mymoney_pay: 1,
                                is_submit: 1,
                                order_id: 0,
                                h5_wxpay: 0,
                                h5_alipay: 0,
                                pay_type: 1,
                                pay_money: 0,
                                paytype: ""
                            };
                        },
                        onLoad: function (t) {
                            var a = this;
                            this._baseMin(this), this.orderid = t.orderid;
                            var e = 0;
                            t.fxsid && (e = t.fxsid), this.fxsid = e, s.getOpenid(e, function () {
                                a.getOrder();
                            });
                        },
                        methods: {
                            makePhoneCallC: function (t) {
                                o.makePhoneCall({
                                    phoneNumber: t.currentTarget.dataset.tel
                                });
                            },
                            getOrder: function () {
                                var e = this,
                                    t = e.orderid;
                                o.request({
                                    url: e.$baseurl + "doPagegetOrderDetail",
                                    data: {
                                        uniacid: e.$uniacid,
                                        order_id: t,
                                        suid: o.getStorageSync("suid")
                                    },
                                    success: function (t) {
                                        e.datas = t.data.data.detail;
                                        var a = t.data.data.detail.paytype;
                                        null == a ? e.paytype = "余额支付" : 1 == a ? e.paytype = "微信支付" : 2 == a ? e.paytype = "支付宝支付" : 3 == a ? e.paytype = "百度支付" : 4 == a && (e.paytype = "QQ支付"),
                                            e.mymoney = t.data.data.mymoney;
                                    }
                                });
                            },
                            makephonecall: function () {
                                this.datas.seller_tel && o.makePhoneCall({
                                    phoneNumber: this.datas.seller_tel
                                });
                            },
                            copy: function (t) {
                                var a = t.currentTarget.dataset.id;
                                o.setClipboardData({
                                    data: a,
                                    success: function (t) {
                                        o.showToast({
                                            title: "复制成功"
                                        });
                                    }
                                });
                            },
                            hxshow: function () {
                                this.showhx = 1;
                            },
                            hxhide: function () {
                                this.showhx = 0, this.hxmm = "";
                            },
                            hxmmInput: function (t) {
                                for (var a = t.target.value.length, e = 0; e < this.hxmm_list.length; e++) this.hxmm_list[e].fs = !1,
                                    this.hxmm_list[e].val = t.target.value[e];
                                a && (this.hxmm_list[a - 1].fs = !0), this.hxmm = t.target.value;
                            },
                            hxmmpass: function () {
                                var e = this,
                                    t = e.hxmm,
                                    a = e.datas;
                                t ? o.request({
                                    url: e.$baseurl + "hxmm",
                                    data: {
                                        hxmm: t,
                                        order_id: a.order_id,
                                        uniacid: e.$uniacid,
                                        is_more: 0,
                                        suid: o.getStorageSync("suid")
                                    },
                                    success: function (t) {
                                        if (0 == t.data.data) {
                                            o.showModal({
                                                title: "提示",
                                                content: "核销密码不正确！",
                                                showCancel: !1
                                            }), e.hxmm = "";
                                            for (var a = 0; a < e.hxmm_list.length; a++) e.hxmm_list[a].fs = !1, e.hxmm_list[a].val = "";
                                        } else o.showToast({
                                            title: "消费成功",
                                            icon: "success",
                                            duration: 2e3,
                                            success: function (t) {
                                                e.showhx = 0, e.hxmm = "";
                                                var a = e.order;
                                                e.getOrder(a);
                                            }
                                        });
                                    }
                                }) : o.showModal({
                                    title: "提示",
                                    content: "请输入核销密码！",
                                    showCancel: !1
                                });
                            },
                            tuikuan: function (t) {
                                var a = this,
                                    e = t.detail.formId,
                                    s = t.currentTarget.dataset.order;
                                o.showModal({
                                    title: "提醒",
                                    content: "确定要退款吗？",
                                    success: function (t) {
                                        t.confirm && o.request({
                                            url: a.$baseurl + "doPagemiaoshatk",
                                            data: {
                                                uniacid: a.$uniacid,
                                                formId: e,
                                                order_id: s
                                            },
                                            success: function (a) {
                                                0 == a.data.data.flag ? o.showModal({
                                                    title: "提示",
                                                    content: a.data.data.message,
                                                    showCancel: !1,
                                                    success: function (t) {
                                                        o.redirectTo({
                                                            url: "/pagesFlashSale/orderDetail_dan/orderDetail_dan?orderid=" + s
                                                        });
                                                    }
                                                }) : o.showModal({
                                                    title: "很抱歉",
                                                    content: a.data.data.message,
                                                    confirmText: "联系客服",
                                                    success: function (t) {
                                                        t.confirm && o.makePhoneCall({
                                                            phoneNumber: a.data.data.mobile
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            gethxmima: function () {
                                this.hx_choose = 1;
                            },
                            gethxImg: function () {
                                var a = this;
                                o.request({
                                    url: this.$baseurl + "doPageHxEwm",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: o.getStorageSync("suid"),
                                        pageUrl: "showPro",
                                        orderid: this.orderid
                                    },
                                    success: function (t) {
                                        a.hx_ewm = t.data.data, a.hx_choose = 2;
                                    }
                                });
                            },
                            hxhide1: function () {
                                this.hx_choose = 0, this.hxmm = "";
                                for (var t = 0; t < this.hxmm_list.length; t++) this.hxmm_list[t].fs = !1, this.hxmm_list[t].val = "";
                            },
                            paybox: function (t) {
                                this.orderinfo;
                                var a = this.datas.order_id;
                                this.order_id = a;
                                var e = this.datas.true_price;
                                this.pay_money = e, this.mymoney < e && (this.mymoney_pay = 2, this.pay_type = 2,
                                    this.choosepayf = 1), 0 == this.showPay && (this.showPay = 1);
                            },
                            payboxclose: function () {
                                1 == this.showPay && (this.showPay = 0);
                            },
                            choosepay: function (t) {
                                this.pay_type = t.currentTarget.dataset.pay_type, this.choosepayf = t.currentTarget.dataset.type;
                            },
                            pay: function (t) {
                                if (2 == this.is_submit) return !1;
                                this.is_submit = 2, this.$uniacid, this.suid, this.source;
                                var a = this.pay_type,
                                    e = this.pay_money,
                                    s = this.order_id,
                                    i = t.detail.formId;
                                1 == a ? this.pay1(s) : this._showwxpay(this, e, "miaosha", s, i, "/pagesFlashSale/orderlist_dan/orderlist_dan?type=9");
                            },
                            pay1: function (a) {
                                var e = this;
                                o.showModal({
                                    title: "请注意",
                                    content: "您将使用余额支付" + e.pay_money + "元",
                                    success: function (t) {
                                        t.confirm ? (e.payover_do(a), o.showLoading({
                                            title: "下单中...",
                                            mask: !0
                                        })) : o.redirectTo({
                                            url: "/pagesFlashSale/orderlist_dan/orderlist_dan"
                                        });
                                    }
                                });
                            },
                            payover_do: function (t) {
                                var a = this,
                                    e = a.pay_money,
                                    s = (a.mymoney, o.getStorageSync("suid")),
                                    i = o.getStorageSync("openid");
                                o.request({
                                    url: a.$baseurl + "doPagepaynotify",
                                    data: {
                                        out_trade_no: t,
                                        suid: s,
                                        payprice: e,
                                        types: "miaosha",
                                        flag: 0,
                                        formId: a.formId,
                                        uniacid: a.$uniacid,
                                        openid: i,
                                        source: o.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        "失败" == t.data.data.message ? o.showToast({
                                            title: "付款失败, 请刷新后重新付款！",
                                            icon: "none",
                                            mask: !0,
                                            success: function () {
                                                o.navigateTo({
                                                    url: "/pagesFlashSale/orderlist_dan/orderlist_dan?type=9"
                                                }), o.hideLoading();
                                            }
                                        }) : o.showToast({
                                            title: "购买成功！",
                                            icon: "success",
                                            mask: !0,
                                            success: function () {
                                                o.navigateTo({
                                                    url: "/pagesFlashSale/orderlist_dan/orderlist_dan?type=9"
                                                }), o.hideLoading();
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    };
                e.default = t;
            }).call(this, i("543d").default);
        },
        ec93: function (t, a, e) {
            e.r(a);
            var s = e("eaf7"),
                i = e.n(s);
            for (var o in s) "default" !== o && function (t) {
                e.d(a, t, function () {
                    return s[t];
                });
            }(o);
            a.default = i.a;
        },
        ed5f: function (t, a, e) {
            e.r(a);
            var s = e("0402"),
                i = e("ec93");
            for (var o in i) "default" !== o && function (t) {
                e.d(a, t, function () {
                    return i[t];
                });
            }(o);
            e("6de4");
            var n = e("2877"),
                r = Object(n.a)(i.default, s.a, s.b, !1, null, null, null);
            a.default = r.exports;
        }
    },
    [
        ["1b75", "common/runtime", "common/vendor"]
    ]
]);