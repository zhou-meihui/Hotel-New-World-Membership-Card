(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesFlashSale/order_dan/order_dan"], {
        "48c2": function (a, t, e) {
            e.r(t);
            var i = e("633d"),
                n = e.n(i);
            for (var s in i) "default" !== s && function (a) {
                e.d(t, a, function () {
                    return i[a];
                });
            }(s);
            t.default = n.a;
        },
        "633d": function (a, t, e) {
            (function (x) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var d = e("f571"),
                    a = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                $host: this.$host,
                                baseinfo: "",
                                needAuth: !1,
                                needBind: !1,
                                nav: 1,
                                num: 1,
                                jifen_u: 0,
                                yunfei: 0,
                                yfjian: 0,
                                jqdjg: "请选择",
                                yhqid: 0,
                                yhdq: 0,
                                dkmoney: 0,
                                dkscore: 0,
                                zf_type: null,
                                zf_money: "",
                                pagedata: {},
                                imgcount_xz: 0,
                                pagedata_set: [],
                                xuanz: 0,
                                lixuanz: -1,
                                ttcxs: 0,
                                get_yf: 0,
                                baoyou: 0,
                                has_yf: 1,
                                kuaidi: "",
                                fid: "",
                                chooseNum: 0,
                                thumb: "",
                                id: 0,
                                mymoney: 0,
                                showhx: 0,
                                isview: 0,
                                pd_val: [],
                                again: 0,
                                pro_city: "",
                                mj_order: "",
                                yhq_order: "",
                                score_order: "",
                                score_money_order: 0,
                                yhq_money_order: 0,
                                yunfei_order: 0,
                                mj_money_order: 0,
                                m_address: [],
                                m_address_l: 0,
                                beizhu_val: "",
                                sw: !1,
                                free_package: 0,
                                disabled: !1,
                                gid: 0,
                                mraddress: "",
                                title: "",
                                dprice: "",
                                hjjg: "",
                                myscore: 0,
                                ischecked: !1,
                                h5pay: 0,
                                dopay: "",
                                pay_type: 1,
                                show_pay_type: 0,
                                alipay: 1,
                                wxpay: 1,
                                datas: "",
                                myname: "",
                                mymobile: "",
                                myaddress: "",
                                wxmobile: "",
                                xx: [],
                                formindex: -1,
                                arrs: [],
                                isover: 0,
                                formdescs: "",
                                newSessionKey: "",
                                showModalStatus: !1,
                                couponlist: "",
                                again_nav: 1,
                                animationData: "",
                                stores: "",
                                current: 0,
                                storeall: [],
                                storeid: "",
                                is_submit: 1,
                                start_year: 2019,
                                end_year: 2060,
                                showPay: 0,
                                choosepayf: 0,
                                mjly: "",
                                shop_id: 0,
                                shop_info: "",
                                self_taking_time: "",
                                default_concat: "",
                                mymoney_pay: 1,
                                h5_wxpay: 0,
                                h5_alipay: 0
                            };
                        },
                        components: {
                            datetime: function () {
                                return e.e("components/datetime/datetime").then(e.bind(null, "9dfc"));
                            }
                        },
                        onLoad: function (a) {
                            var t = this,
                                e = 0;
                            a.fxsid && (e = a.fxsid), this.fxsid = e, a.again && (this.again = 1), a.id && (this.gid = a.id),
                                this.refreshSessionkey();
                            var i = a.shop_id;
                            i && (this.shop_id = i);
                            var n = a.addressid;
                            n && (this.addressid = n);
                            var s = a.nav;
                            s && (this.nav = s), this._baseMin(this), d.getOpenid(e, function () {
                                t.getorder();
                            }, function () {
                                t.needAuth = !0;
                            }, function () {
                                t.needBind = !0;
                            });
                        },
                        onPullDownRefresh: function () {
                            this.refreshSessionkey(), x.stopPullDownRefresh();
                        },
                        methods: {
                            clickTab: function (a) {
                                var t = this;
                                if (t.nav == a.currentTarget.dataset.current) return !1;
                                t.nav = a.currentTarget.dataset.current, t.getmyinfo();
                            },
                            chooseStore: function () {
                                x.navigateTo({
                                    url: "/pages/chooseStore/chooseStore?address_id=" + this.addressid + "&stid=" + this.shop_id + "&type=flashSale&gid=" + this.gid
                                });
                            },
                            concat_input: function (a) {
                                this.default_concat = a.detail.value;
                            },
                            openDatetimePicker: function (a) {
                                this.$refs.myPicker.show();
                            },
                            closeDatetimePicker: function () {
                                this.$refs.myPicker.hide();
                            },
                            handleSubmit: function (a) {
                                this.self_taking_time = "".concat(a.year, "-").concat(a.month, "-").concat(a.day, " ").concat(a.hour, ":").concat(a.minute);
                            },
                            paybox: function () {
                                if (2 == this.nav) {
                                    if (!this.shop_info) return x.showModal({
                                        title: "提示",
                                        content: "请先选择门店",
                                        showCancel: !1
                                    }), !1;
                                    if (!this.self_taking_time) return x.showModal({
                                        title: "提示",
                                        content: "请先选择预约取件时间",
                                        showCancel: !1
                                    }), !1;
                                    var a = this.default_concat;
                                    if (!a) return x.showModal({
                                        title: "提示",
                                        content: "请先输入预留电话",
                                        showCancel: !1
                                    }), !1;
                                    if (!/^1[3456789]{1}\d{9}$/.test(a)) return x.showModal({
                                        title: "提醒",
                                        content: "请您输入正确的手机号码",
                                        showCancel: !1
                                    }), !1;
                                } else {
                                    if (!this.addressid) return x.showModal({
                                        title: "提示",
                                        content: "请先选择收件地址",
                                        showCancel: !1
                                    }), !1;
                                }
                                this.mymoney < this.zf_money && (this.mymoney_pay = 2, this.pay_type = 2, this.choosepayf = 1),
                                    0 == this.showPay && (this.showPay = 1);
                            },
                            payboxclose: function () {
                                1 == this.showPay && (this.showPay = 0);
                            },
                            choosepay: function (a) {
                                this.zf_type = 1 == a.currentTarget.dataset.pay_type ? 0 : 1, this.choosepayf = a.currentTarget.dataset.type;
                            },
                            closeAuth: function () {
                                console.log("closeAuth"), this.needAuth = !1, this._checkBindPhone(this), this.globaluserinfo();
                            },
                            closeBind: function () {
                                console.log("closeBind"), this.needBind = !1, this.getorder();
                            },
                            getorder: function () {
                                var t = this;
                                x.getStorageSync("suid"), 0 == this.again && x.request({
                                    url: t.$baseurl + "doPagecheckFreePackage",
                                    data: {
                                        uniacid: t.$uniacid,
                                        suid: x.getStorageSync("suid")
                                    },
                                    success: function (a) {
                                        t.free_package = a.data.data;
                                    }
                                }), t.getGoodsInfo();
                            },
                            getGoodsInfo: function () {
                                var i = this,
                                    a = x.getStorageSync("suid");
                                x.request({
                                    url: i.$baseurl + "doPageshowPro11",
                                    data: {
                                        uniacid: i.$uniacid,
                                        suid: a,
                                        id: i.gid
                                    },
                                    success: function (a) {
                                        if (0 == a.data.data.pro_xz) var t = 1;
                                        else t = a.data.data.pro_xz - a.data.data.my_num;
                                        if (i.mymoney = parseFloat(a.data.data.userinfo.money), i.myscore = parseFloat(a.data.data.userinfo.score),
                                            i.thumb = a.data.data.thumb, i.title = a.data.data.title, i.datas = a.data.data,
                                            i.dprice = a.data.data.price, i.hjjg = a.data.data.order_num ? a.data.data.price * a.data.data.order_num : a.data.data.price,
                                            i.my_num = a.data.data.my_num, i.xg_num = a.data.data.pro_xz, i.shengyu = a.data.data.pro_kc,
                                            i.xg_buy = t, i.num = a.data.data.order_num ? a.data.data.order_num : 1, i.fid = a.data.data.formset,
                                            i.pagedata = a.data.data.forms, i.formdescs = a.data.data.formdescs, i.kuaidi = a.data.data.kuaidi,
                                            2 != i.nav && (0 == i.kuaidi ? i.nav = 1 : 1 == i.kuaidi ? i.nav = 2 : 2 == i.kuaidi && (i.nav = 1)),
                                            x.setNavigationBarTitle({
                                                title: i.title
                                            }), x.setStorageSync("isShowLoading", !1), x.hideNavigationBarLoading(), x.stopPullDownRefresh(),
                                            1 == i.nav) {
                                            var e = i.addressid;
                                            e ? i.getmraddresszd(e) : i.getmraddress();
                                        } else i.myContactInfo();
                                    }
                                });
                            },
                            myContactInfo: function () {
                                var t = this;
                                x.request({
                                    url: t.$host + "/api/MainWxapp/getMyContactInfo",
                                    data: {
                                        uniacid: t.$uniacid,
                                        suid: t.suid,
                                        shop_id: t.shop_id,
                                        type: "flashSale"
                                    },
                                    success: function (a) {
                                        t.shop_info = a.data.data.shop_info, t.getmyinfo();
                                    }
                                });
                            },
                            getmraddresszd: function (a) {
                                var e = this,
                                    t = x.getStorageSync("suid");
                                x.request({
                                    url: e.$baseurl + "doPagegetmraddresszd",
                                    data: {
                                        suid: t,
                                        id: a,
                                        uniacid: e.$uniacid
                                    },
                                    success: function (a) {
                                        var t = a.data.data;
                                        "" != t ? (e.mraddress = t, e.pro_city = t.pro_city) : e.mraddress = "", e.getmyinfo();
                                    }
                                });
                            },
                            getmraddress: function () {
                                var e = this,
                                    a = x.getStorageSync("suid");
                                x.request({
                                    url: e.$baseurl + "doPagegetmraddress",
                                    data: {
                                        suid: a,
                                        uniacid: e.$uniacid
                                    },
                                    success: function (a) {
                                        var t = a.data.data;
                                        t ? (e.mraddress = t, e.pro_city = t.pro_city, e.addressid = t.id) : (e.again, e.mraddress = ""),
                                            e.getmyinfo();
                                    }
                                });
                            },
                            getmyinfo: function () {
                                var i = this,
                                    n = (x.getStorageSync("suid"), i.hjjg),
                                    s = i.nav,
                                    a = (i.again, i.pro_city),
                                    t = i.free_package;
                                "" != a && 2 != i.nav && 0 == t ? x.request({
                                    url: i.$baseurl + "doPageyunfeigetnew",
                                    data: {
                                        uniacid: i.$uniacid,
                                        id: i.gid,
                                        type: "miaosha",
                                        hjjg: i.hjjg,
                                        num: i.num,
                                        pro_city: a
                                    },
                                    success: function (a) {
                                        var t, e = a.data.data;
                                        t = parseFloat(n) >= parseFloat(e.byou) && "" != e.byou ? 0 : e.yfei, n = 2 == s ? Math.round(1 * n * 100) / 100 : Math.round(100 * (1 * n + 1 * t)) / 100,
                                            i.yunfei = t, i.sfje = n, i.zf_type = i.mymoney >= n ? 0 : 1, i.zf_money = n, i.get_yf = e.yfei,
                                            i.baoyou = e.byou;
                                    }
                                }) : (i.sfje = n, i.zf_type = i.mymoney >= n ? 0 : 1, i.zf_money = n);
                            },
                            add_address: function () {
                                0 == this.again && x.navigateTo({
                                    url: "/pages/address/address?shareid=" + this.shareid + "&pid=" + this.gid + "&orderid=" + this.orderid
                                });
                            },
                            jian: function () {
                                var a = this,
                                    t = a.num;
                                --t <= 0 && (t = 1);
                                var e = 100 * a.dprice * t / 100;
                                a.num = t, a.hjjg = e, a.ischecked = !1, a.getmyinfo();
                            },
                            jia: function () {
                                var a = this,
                                    t = a.num,
                                    e = a.my_num,
                                    i = a.xg_num,
                                    n = a.shengyu,
                                    s = a.dprice;
                                n < ++t && -1 != n && (t--, x.showModal({
                                    title: "提醒",
                                    content: "库存量不足！",
                                    showCancel: !1
                                })), i < t + e && 0 != i && (1 < t && t--, x.showModal({
                                    title: "提醒",
                                    content: "该商品为限购产品，您总购买数已超过限额！",
                                    showCancel: !1
                                }));
                                var d = 100 * s * t / 100;
                                a.num = t, a.hjjg = d, a.ischecked = !1, a.getmyinfo();
                            },
                            refreshSessionkey: function () {
                                var t = this;
                                x.login({
                                    success: function (a) {
                                        x.request({
                                            url: t.$baseurl + "doPagegetNewSessionkey",
                                            data: {
                                                uniacid: t.$uniacid,
                                                code: a.code
                                            },
                                            success: function (a) {
                                                t.newSessionKey = a.data.data;
                                            }
                                        });
                                    }
                                });
                            },
                            submit: d.throttle(function (a) {
                                var t = this;
                                if (2 == this.is_submit) return !1;
                                var e = a.detail.formId,
                                    i = t.again;
                                t.formId = e, t.disabled = !0, t.datas;
                                var n = t.mraddress;
                                if (0 < t.fid) {
                                    if (2 != t.nav && (null == n || !n) && 0 == i) return x.showModal({
                                        title: "提示",
                                        content: "请先选择/设置收货地址！",
                                        showCancel: !0,
                                        success: function (a) {
                                            if (!a.confirm) return t.disabled = !1;
                                            x.navigateTo({
                                                url: "/pages/address/address?shareid=" + t.shareid + "&pid=" + t.gid + "&orderid=" + t.orderid
                                            });
                                        }
                                    }), !1;
                                    t.formSubmit();
                                } else {
                                    if (2 != t.nav && (null == n || !n) && 0 == i) return x.showModal({
                                        title: "提示",
                                        content: "请先选择/设置收货地址！",
                                        showCancel: !0,
                                        success: function (a) {
                                            if (a.confirm) x.navigateTo({
                                                url: "/pages/address/address?shareid=" + t.shareid + "&pid=" + t.gid + "&orderid=" + t.orderid
                                            });
                                            else if (a.cancel) return t.disabled = !1;
                                        }
                                    }), !1;
                                    t.doshend(0);
                                }
                            }, 2e3),
                            doshend: function (a) {
                                var e = this,
                                    t = wx.getStorageSync("suid"),
                                    i = e.yhqid,
                                    n = e.sfje,
                                    s = e.nav,
                                    d = e.yunfei,
                                    o = e.yfjian,
                                    r = e.dkscore,
                                    c = (e.dkmoney,
                                        e.mraddress),
                                    u = e.mjly,
                                    l = e.orderid,
                                    h = e.again;
                                if (1 == s ? d -= o : d = 0, !(2 == s || null != c && c || 0 != h)) return x.showModal({
                                    title: "提示",
                                    content: "请先选择/设置收货地址！",
                                    showCancel: !0,
                                    success: function (a) {
                                        if (!a.confirm) return e.disabled = !1;
                                        x.navigateTo({
                                            url: "/pages/address/address?shareid=" + e.shareid + "&pid=" + e.gid + "&orderid=" + e.orderid
                                        });
                                    }
                                }), !1;
                                var f = c.id;
                                if (0 == e.again) {
                                    var g = "",
                                        y = "",
                                        p = "",
                                        m = "";
                                    if (2 == e.nav)
                                        if ("" == e.storeall) g = e.baseinfo.name, y = e.baseinfo.tel, p = e.baseinfo.address;
                                        else
                                            for (var _ = e.storeid, v = e.storeall, w = 0; w < v.length; w++)
                                                if (_ == v[w].id) {
                                                    g = v[w].title, y = v[w].tel, p = v[w].province + v[w].city + v[w].country, m = v[w].times;
                                                    break;
                                                }
                                }
                                e.is_submit = 2, x.request({
                                    url: e.$baseurl + "doPagecreateorder",
                                    data: {
                                        pid: e.gid,
                                        num: e.num,
                                        types: "miaosha",
                                        suid: t,
                                        couponid: i,
                                        price: n,
                                        dkscore: r,
                                        address: f,
                                        mjly: u,
                                        nav: s,
                                        formid: a,
                                        yunfei: d,
                                        orderid: l || "",
                                        uniacid: e.$uniacid,
                                        again: e.again,
                                        source: x.getStorageSync("source"),
                                        store_hours: m,
                                        store_name: g,
                                        store_address: p,
                                        store_tel: y,
                                        zf_type: e.zf_type,
                                        shop_id: e.shop_id,
                                        self_taking_time: e.self_taking_time,
                                        default_concat: e.default_concat
                                    },
                                    success: function (a) {
                                        if ("1" == a.data.data.errcode) x.showModal({
                                            title: a.data.data.err,
                                            content: "请重新下单",
                                            showCancel: !1
                                        }), e.disabled = !1;
                                        else if ("2" == a.data.data.errcode) x.showModal({
                                            title: a.data.data.err,
                                            content: a.data.data.can_buy <= 0 ? "去逛逛其他商品吧~" : "您还可购买" + a.data.data.can_buy + "件",
                                            showCancel: !1
                                        }), e.disabled = !1;
                                        else if ("3" == a.data.data.errcode) x.showModal({
                                            title: a.data.data.err,
                                            content: "当前库存为" + a.data.data.kc + "件",
                                            showCancel: !1
                                        }), e.disabled = !1;
                                        else if ("4" == a.data.data.errcode) x.showModal({
                                            title: "提示",
                                            content: "该商品已下架",
                                            showCancel: !1,
                                            success: function () {
                                                x.redirectTo({
                                                    url: "/pages/index/index"
                                                });
                                            }
                                        });
                                        else if ("-1" == a.data.data.errcode) {
                                            var t = a.data.data.orderid;
                                            e.orderid = t, 0 == e.zf_type ? e.pay1(t) : e.pay2(t);
                                        }
                                    }
                                });
                            },
                            showpay: function () {
                                var t = this;
                                x.request({
                                    url: this.$baseurl + "doPageGetH5payshow",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: x.getStorageSync("suid")
                                    },
                                    success: function (a) {
                                        0 == a.data.data.ali && 0 == a.data.data.wx ? x.showModal({
                                            title: "提示",
                                            content: "请联系管理员设置支付参数",
                                            success: function (a) {
                                                x.navigateTo({
                                                    url: "/pagesFlashSale/order_more_list/order_more_list"
                                                });
                                            }
                                        }) : (0 == a.data.data.ali ? (t.alipay = 0, t.pay_type = 2) : (t.alipay = 1, t.pay_type = 1),
                                            0 == a.data.data.wx ? t.wxpay = 0 : t.wxpay = 1, t.show_pay_type = 1);
                                    }
                                });
                            },
                            changealipay: function () {
                                this.pay_type = 1;
                            },
                            changewxpay: function () {
                                this.pay_type = 2;
                            },
                            close_pay_type: function () {
                                this.show_pay_type = 0;
                            },
                            h5topay: function () {
                                var a = this.pay_type;
                                1 == a ? this._alih5pay(this, this.zf_money, 7, this.orderid) : 2 == a && this._wxh5pay(this, this.zf_money, "miaosha", this.orderid),
                                    this.show_pay_type = 0;
                            },
                            pay1: function (t) {
                                var e = this;
                                x.showModal({
                                    title: "请注意",
                                    content: "您将使用余额支付" + e.sfje + "元",
                                    success: function (a) {
                                        a.confirm ? (e.payover_do(t), x.showLoading({
                                            title: "下单中...",
                                            mask: !0
                                        })) : x.redirectTo({
                                            url: "/pagesFlashSale/orderlist_dan/orderlist_dan"
                                        });
                                    }
                                });
                            },
                            pay2: function (t) {
                                var e = this,
                                    a = x.getStorageSync("openid"),
                                    i = e.sfje;
                                x.request({
                                    url: e.$baseurl + "doPagebeforepay",
                                    data: {
                                        openid: a,
                                        price: i,
                                        order_id: t,
                                        types: "miaosha",
                                        formId: e.formId,
                                        uniacid: e.$uniacid,
                                        suid: x.getStorageSync("suid")
                                    },
                                    success: function (a) {
                                        1 == a.data.data.errs && (x.showModal({
                                            title: "支付失败",
                                            content: a.data.data.return_msg,
                                            showCancel: !1
                                        }), e.disabled = !1); -
                                        1 != [1, 2, 3, 4].indexOf(a.data.data.err) && (x.showModal({
                                            title: "支付失败",
                                            content: a.data.data.message,
                                            showCancel: !1
                                        }), e.disabled = !1), 0 == a.data.data.err && (x.request({
                                            url: e.$baseurl + "doPagesavePrepayid",
                                            data: {
                                                types: "miaosha",
                                                order_id: t,
                                                prepayid: a.data.data.package,
                                                uniacid: e.$uniacid
                                            },
                                            success: function (a) {},
                                            fail: function (a) {}
                                        }), x.requestPayment({
                                            timeStamp: a.data.data.timeStamp,
                                            nonceStr: a.data.data.nonceStr,
                                            package: a.data.data.package,
                                            signType: "MD5",
                                            paySign: a.data.data.paySign,
                                            success: function (a) {
                                                x.showToast({
                                                    title: "支付成功",
                                                    icon: "success",
                                                    mask: !0,
                                                    duration: 3e3,
                                                    success: function (a) {
                                                        x.showToast({
                                                            title: "购买成功！",
                                                            icon: "success",
                                                            mask: !0,
                                                            success: function () {
                                                                x.navigateTo({
                                                                    url: "/pagesFlashSale/orderlist_dan/orderlist_dan?type=9"
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            },
                                            fail: function (a) {
                                                x.navigateTo({
                                                    url: "/pagesFlashSale/orderlist_dan/orderlist_dan?type=9"
                                                });
                                            },
                                            complete: function (a) {}
                                        }));
                                    }
                                });
                            },
                            pay4: function (a) {
                                x.request({
                                    url: this.$baseurl + "doPageAlipay",
                                    data: {
                                        uniacid: this.$uniacid,
                                        order_id: a,
                                        suid: x.getStorageSync("suid"),
                                        types: "miaosha"
                                    },
                                    success: function (a) {
                                        a && x.tradePay({
                                            tradeNO: a.data.trade_no,
                                            success: function (a) {
                                                x.navigateTo({
                                                    url: "/pagesFlashSale/orderlist_dan/orderlist_dan"
                                                });
                                            },
                                            fail: function (a) {
                                                x.navigateTo({
                                                    url: "/pagesFlashSale/orderlist_dan/orderlist_dan"
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            payover_do: function (a) {
                                var t = this,
                                    e = t.sfje,
                                    i = (t.mymoney, x.getStorageSync("suid")),
                                    n = x.getStorageSync("openid");
                                x.request({
                                    url: t.$baseurl + "doPagepaynotify",
                                    data: {
                                        out_trade_no: a,
                                        suid: i,
                                        payprice: e,
                                        types: "miaosha",
                                        flag: 0,
                                        formId: t.formId,
                                        uniacid: t.$uniacid,
                                        openid: n,
                                        source: x.getStorageSync("source")
                                    },
                                    success: function (a) {
                                        "失败" == a.data.data.message ? x.showToast({
                                            title: "付款失败, 请刷新后重新付款！",
                                            icon: "none",
                                            mask: !0,
                                            success: function () {
                                                x.navigateTo({
                                                    url: "/pagesFlashSale/orderlist_dan/orderlist_dan?type=9"
                                                }), x.hideLoading();
                                            }
                                        }) : x.showToast({
                                            title: "购买成功！",
                                            icon: "success",
                                            mask: !0,
                                            success: function () {
                                                x.navigateTo({
                                                    url: "/pagesFlashSale/orderlist_dan/orderlist_dan?type=9"
                                                }), x.hideLoading();
                                            }
                                        });
                                    }
                                });
                            },
                            showModal: function () {
                                this.ischecked = !1, this.sw = !1, this.showModalStatus = !0;
                                var a = x.createAnimation({
                                    duration: 200,
                                    timingFunction: "linear",
                                    delay: 0
                                });
                                (this.animation = a).translateY(300).step(), this.animationData = a.export(), setTimeout(function () {
                                    a.translateY(0).step(), this.animationData = a.export();
                                }.bind(this), 200);
                            },
                            hideModal: function () {
                                var a = wx.createAnimation({
                                    duration: 200,
                                    timingFunction: "linear",
                                    delay: 0
                                });
                                (this.animation = a).translateY(300).step(), this.animationData = a.export(), setTimeout(function () {
                                    a.translateY(0).step(), this.animationData = a.export(), this.showModalStatus = !1;
                                }.bind(this), 200);
                            },
                            mjlySet: function (a) {
                                var t = a.detail.value;
                                this.mjly = t;
                            },
                            bindInputChange: function (a) {
                                var t = a.detail.value,
                                    e = a.currentTarget.dataset.index,
                                    i = this.pagedata;
                                i[e].val = t, this.pagedata = i;
                            },
                            weixinadd: function () {
                                var d = this;
                                x.chooseAddress({
                                    success: function (a) {
                                        for (var t = a.provinceName + " " + a.cityName + " " + a.countyName + " " + a.detailInfo, e = a.userName, i = a.telNumber, n = d.pagedata, s = 0; s < n.length; s++) 0 == n[s].type && 2 == n[s].tp_text[0].yval && (n[s].val = e),
                                            0 == n[s].type && 3 == n[s].tp_text[0].yval && (n[s].val = i), 0 == n[s].type && 4 == n[s].tp_text[0].yval && (n[s].val = t);
                                        d.myname = e, d.mymobile = i, d.myaddress = t, d.pagedata = n;
                                    }
                                });
                            },
                            getPhoneNumber: function (a) {
                                var i = this,
                                    t = a.detail.iv,
                                    e = a.detail.encryptedData;
                                "getPhoneNumber:ok" == a.detail.errMsg ? x.checkSession({
                                    success: function () {
                                        x.request({
                                            url: i.$baseurl + "doPagejiemiNew",
                                            data: {
                                                uniacid: i.$uniacid,
                                                newSessionKey: i.newSessionKey,
                                                iv: t,
                                                encryptedData: e
                                            },
                                            success: function (a) {
                                                if (a.data.data) {
                                                    for (var t = i.pagedata, e = 0; e < t.length; e++) 0 == t[e].type && 5 == t[e].tp_text[0] && (t[e].val = a.data.data);
                                                    i.wxmobile = a.data.data, i.pagedata = t;
                                                } else x.showModal({
                                                    title: "提示",
                                                    content: "sessionKey已过期，请下拉刷新！"
                                                });
                                            },
                                            fail: function (a) {
                                                console.log(a);
                                            }
                                        });
                                    },
                                    fail: function () {
                                        x.showModal({
                                            title: "提示",
                                            content: "sessionKey已过期，请下拉刷新！"
                                        });
                                    }
                                }) : x.showModal({
                                    title: "提示",
                                    content: "请先授权获取您的手机号！",
                                    showCancel: !1
                                });
                            },
                            checkboxChange: function (a) {
                                var t = a.detail.value,
                                    e = a.currentTarget.dataset.index,
                                    i = this.pagedata;
                                i[e].val = t, this.pagedata = i;
                            },
                            radioChange: function (a) {
                                var t = a.detail.value,
                                    e = a.currentTarget.dataset.index,
                                    i = this.pagedata;
                                i[e].val = t, this.pagedata = i;
                            },
                            delimg: function (a) {
                                var t = a.currentTarget.dataset.index,
                                    e = a.currentTarget.dataset.id,
                                    i = this.pagedata,
                                    n = i[t].z_val;
                                n.splice(e, 1), 0 == n.length && (n = ""), i[t].z_val = n, this.pagedata = i;
                            },
                            choiceimg: function (a) {
                                var s = this,
                                    t = 0,
                                    d = s.zhixin,
                                    o = a.currentTarget.dataset.index,
                                    r = s.pagedata,
                                    e = r[o].val,
                                    i = r[o].tp_text[0];
                                e ? t = e.length : (t = 0, e = []);
                                var n = i - t;
                                s.pd_val, x.chooseImage({
                                    count: n,
                                    sizeType: ["original", "compressed"],
                                    sourceType: ["album", "camera"],
                                    success: function (a) {
                                        d = !0, s.zhixin = d, x.showLoading({
                                            title: "图片上传中"
                                        });
                                        var e = a.tempFilePaths,
                                            i = 0,
                                            n = e.length;
                                        ! function t() {
                                            x.uploadFile({
                                                url: s.$baseurl + "wxupimg",
                                                formData: {
                                                    uniacid: s.$uniacid
                                                },
                                                filePath: e[i],
                                                name: "file",
                                                success: function (a) {
                                                    r[o].z_val.push(a.data), s.pagedata = r, ++i < n ? t() : (d = !1, s.zhixin = d,
                                                        x.hideLoading());
                                                }
                                            });
                                        }();
                                    }
                                });
                            },
                            bindPickerChange: function (a) {
                                var t = a.detail.value,
                                    e = a.currentTarget.dataset.index,
                                    i = this.pagedata,
                                    n = i[e].tp_text[t];
                                i[e].val = n, this.pagedata = i;
                            },
                            bindDateChange: function (a) {
                                var t = a.detail.value,
                                    e = a.currentTarget.dataset.index,
                                    i = this.pagedata;
                                i[e].val = t, this.pagedata = i;
                            },
                            bindTimeChange: function (a) {
                                var t = a.detail.value,
                                    e = a.currentTarget.dataset.index,
                                    i = this.pagedata;
                                i[e].val = t, this.pagedata = i;
                            },
                            namexz: function (a) {
                                for (var t = this, e = a.currentTarget.dataset.index, i = t.pagedata[e], n = [], s = 0; s < i.tp_text.length; s++) {
                                    var d = {};
                                    d.keys = i.tp_text[s].yval, d.val = 1, n.push(d);
                                }
                                t.ttcxs = 1, t.formindex = e, t.xx = n, t.xuanz = 0, t.lixuanz = -1, t.riqi();
                            },
                            riqi: function () {
                                for (var a = this, t = new Date(), e = new Date(t.getTime()), i = e.getFullYear() + "-" + (e.getMonth() + 1) + "-" + e.getDate(), n = a.xx, s = 0; s < n.length; s++) n[s].val = 1;
                                a.xx = n, a.gettoday(i);
                                for (var d = [], o = [], r = new Date(), c = 0; c < 5; c++) {
                                    var u = new Date(r.getTime() + 24 * c * 3600 * 1e3),
                                        l = u.getFullYear(),
                                        h = u.getMonth() + 1,
                                        f = u.getDate(),
                                        g = h + "月" + f + "日",
                                        y = l + "-" + h + "-" + f;
                                    d.push(g), o.push(y);
                                }
                                a.arrs = d, a.fallarrs = o, a.today = i;
                            },
                            gettoday: function (a) {
                                var n = this,
                                    t = n.formindex,
                                    s = n.xx;
                                x.request({
                                    url: n.$baseurl + "dopageDuzhan",
                                    data: {
                                        uniacid: n.$uniacid,
                                        id: 0,
                                        types: "showOrder",
                                        days: a,
                                        pagedatekey: t
                                    },
                                    success: function (a) {
                                        for (var t = a.data.data, e = 0; e < t.length; e++) s[t[e]].val = 2;
                                        var i = 0;
                                        t.length == s.length && (i = 1), n.xx = s, n.isover = i;
                                    }
                                });
                            },
                            goux: function (a) {
                                this.lixuanz = a.currentTarget.dataset.index;
                            },
                            xuanzd: function (a) {
                                for (var t = a.currentTarget.dataset.index, e = this.fallarrs[t], i = this.xx, n = 0; n < i.length; n++) i[n].val = 1;
                                this.xuanz = t, this.today = e, this.lixuanz = -1, this.xx = i, this.gettoday(e);
                            },
                            quxiao: function () {
                                this.ttcxs = 0;
                            },
                            save: function () {
                                var a = this.today,
                                    t = this.xx,
                                    e = this.lixuanz;
                                if (-1 == e) return x.showModal({
                                    title: "提现",
                                    content: "请选择预约的选项",
                                    showCancel: !1
                                }), !1;
                                var i = "已选择" + a + "，" + t[e].keys,
                                    n = this.pagedata,
                                    s = this.formindex;
                                n[s].val = i, n[s].days = a, n[s].indexkey = s, n[s].xuanx = e, this.ttcxs = 0,
                                    this.pagedata = n;
                            },
                            formSubmit: function () {
                                var e = this,
                                    a = x.getStorageSync("suid");
                                if (!a) return x.showModal({
                                    title: "提醒",
                                    content: "您尚未登陆/注册，请先登陆/注册",
                                    success: function (a) {
                                        a.confirm && x.navigateTo({
                                            url: "/pages/usercenter/usercenter"
                                        });
                                    }
                                }), !1;
                                for (var t = e.pagedata, i = 0; i < t.length; i++) {
                                    if (1 == t[i].ismust)
                                        if (5 == t[i].type) {
                                            if (!t[i].z_val) return x.showModal({
                                                title: "提醒",
                                                content: t[i].name + "为必填项！",
                                                showCancel: !1
                                            }), !1;
                                        } else if ("" == t[i].val) return x.showModal({
                                        title: "提醒",
                                        content: t[i].name + "为必填项！",
                                        showCancel: !1
                                    }), !1;
                                    if (5 == t[i].type && t[i].z_val) {
                                        if (0 < t[i].z_val.length)
                                            for (var n = 0; n < t[i].z_val.length; n++) t[i].z_val[n] = t[i].z_val[n].substr(t[i].z_val[n].indexOf("/upimages"));
                                    } else if (0 == t[i].type && 1 == t[i].tp_text[0].yval) {
                                        if (!/^1[3456789]{1}\d{9}$/.test(t[i].val)) return x.showModal({
                                            title: "提醒",
                                            content: "请您输入正确的手机号码",
                                            showCancel: !1
                                        }), !1;
                                    } else if (0 == t[i].type && 7 == t[i].tp_text[0].yval) {
                                        if (!/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test(t[i].val)) return x.showModal({
                                            title: "提醒",
                                            content: "请您输入正确的身份证号",
                                            showCancel: !1
                                        }), !1;
                                    }
                                }
                                e.is_submit = 2;
                                var s;
                                s = x.getStorageSync("openid"), x.request({
                                    url: e.$baseurl + "doPageFormval",
                                    data: {
                                        uniacid: e.$uniacid,
                                        suid: a,
                                        id: e.gid,
                                        pagedata: JSON.stringify(t),
                                        fid: e.datas.formset,
                                        types: "miaosha",
                                        openid: s,
                                        source: x.getStorageSync("source"),
                                        form_id: e.formId ? e.formId : ""
                                    },
                                    success: function (a) {
                                        var t = a.data.data.id;
                                        x.showModal({
                                            title: "提醒",
                                            content: a.data.data.con,
                                            showCancel: !1,
                                            success: function () {
                                                e.doshend(t);
                                            }
                                        });
                                    }
                                });
                            },
                            makePhoneCallC: function (a) {
                                var t = a.currentTarget.dataset.tel;
                                x.makePhoneCall({
                                    phoneNumber: t
                                });
                            }
                        }
                    };
                t.default = a;
            }).call(this, e("543d").default);
        },
        "6f8c": function (a, t, e) {
            var i = e("de99");
            e.n(i).a;
        },
        "8aaf": function (a, t, e) {
            (function (a) {
                function t(a) {
                    return a && a.__esModule ? a : {
                        default: a
                    };
                }
                e("020c"), e("921b"), t(e("66fd")), a(t(e("8d87")).default);
            }).call(this, e("543d").createPage);
        },
        "8d87": function (a, t, e) {
            e.r(t);
            var i = e("b9d3"),
                n = e("48c2");
            for (var s in n) "default" !== s && function (a) {
                e.d(t, a, function () {
                    return n[a];
                });
            }(s);
            e("6f8c");
            var d = e("2877"),
                o = Object(d.a)(n.default, i.a, i.b, !1, null, null, null);
            t.default = o.exports;
        },
        b9d3: function (a, t, e) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                n = [];
            e.d(t, "a", function () {
                return i;
            }), e.d(t, "b", function () {
                return n;
            });
        },
        de99: function (a, t, e) {}
    },
    [
        ["8aaf", "common/runtime", "common/vendor"]
    ]
]);