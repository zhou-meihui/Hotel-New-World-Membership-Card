(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesFlashSale/orderlist_dan/orderlist_dan"], {
        "12a3": function (t, e, a) {},
        1342: function (t, e, a) {
            a.r(e);
            var i = a("7edd"),
                n = a("3fbe");
            for (var o in n) "default" !== o && function (t) {
                a.d(e, t, function () {
                    return n[t];
                });
            }(o);
            a("fe7b");
            var s = a("2877"),
                r = Object(s.a)(n.default, i.a, i.b, !1, null, null, null);
            e.default = r.exports;
        },
        1641: function (t, e, a) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                a("020c"), a("921b"), e(a("66fd")), t(e(a("1342")).default);
            }).call(this, a("543d").createPage);
        },
        "3fbe": function (t, e, a) {
            a.r(e);
            var i = a("fbc5"),
                n = a.n(i);
            for (var o in i) "default" !== o && function (t) {
                a.d(e, t, function () {
                    return i[t];
                });
            }(o);
            e.default = n.a;
        },
        "7edd": function (t, e, a) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                n = [];
            a.d(e, "a", function () {
                return i;
            }), a.d(e, "b", function () {
                return n;
            });
        },
        fbc5: function (t, e, i) {
            (function (o) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var a = i("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                page_signs: "/pagesFlashSale/orderlist_dan/orderlist_dan",
                                page: 1,
                                morePro: !1,
                                baseinfo: {},
                                orderinfo: [],
                                orderinfo_length: 0,
                                type: 9,
                                nav: [{
                                    id: 9,
                                    text: "全部"
                                }, {
                                    id: 0,
                                    text: "待付款"
                                }, {
                                    id: 1,
                                    text: "待消费",
                                    nav: 2
                                }, {
                                    id: 11,
                                    text: "待发货",
                                    nav: 1
                                }, {
                                    id: 4,
                                    text: "已发货"
                                }, {
                                    id: 2,
                                    text: "已完成"
                                }, {
                                    id: -1,
                                    text: "已过期"
                                }, {
                                    id: 6,
                                    text: "售后"
                                }, {
                                    id: 5,
                                    text: "商家已取消"
                                }],
                                showPay: 0,
                                choosepayf: 0,
                                mymoney: 0,
                                mymoney_pay: 1,
                                is_submit: 1,
                                order_id: 0,
                                h5_wxpay: 0,
                                h5_alipay: 0,
                                pay_type: 1,
                                pay_money: 0
                            };
                        },
                        onLoad: function (t) {
                            this._baseMin(this);
                            var e = 0;
                            t.fxsid && (e = t.fxsid), this.fxsid = e, t.flag && (this.flag = t.flag), t.type1 && (this.type = t.type),
                                a.getOpenid(e, function () {});
                        },
                        onShow: function () {
                            this.page = 1, this.getList();
                        },
                        onPullDownRefresh: function () {
                            this.page = 1, this.getList(), o.stopPullDownRefresh();
                        },
                        onReachBottom: function () {
                            var e = this,
                                a = 1 * e.page + 1;
                            o.request({
                                url: e.$baseurl + "doPageMyorder",
                                data: {
                                    page: a,
                                    uniacid: this.$uniacid,
                                    type: this.type,
                                    suid: o.getStorageSync("suid"),
                                    is_more: 0
                                },
                                success: function (t) {
                                    e.orderinfo = e.orderinfo.concat(t.data.data.list), e.page = a;
                                }
                            });
                        },
                        methods: {
                            getList: function () {
                                var e = this,
                                    t = o.getStorageSync("suid");
                                o.request({
                                    url: e.$baseurl + "doPageMyorder",
                                    data: {
                                        uniacid: e.$uniacid,
                                        suid: t,
                                        type: e.type,
                                        is_more: 0
                                    },
                                    success: function (t) {
                                        e.allnum = t.data.data.allnum, e.orderinfo = t.data.data.list, e.orderinfo_length = t.data.data.list.length,
                                            e.mymoney = t.data.data.mymoney;
                                    },
                                    fail: function (t) {}
                                });
                            },
                            goevaluate: function (t) {
                                var e = t.currentTarget.dataset.order,
                                    a = t.currentTarget.dataset.type;
                                o.navigateTo({
                                    url: "/pagesOther/evaluate/evaluate?order_id=" + e + "&type=" + a
                                });
                            },
                            chonxhq: function (t) {
                                var e = this,
                                    a = t.currentTarget.dataset.id,
                                    i = t.currentTarget.dataset.nav || "";
                                this.type = a, this.morePro = !1, this.page = 1, 11 == a && (a = 1), o.request({
                                    url: e.$baseurl + "doPageMyorder",
                                    data: {
                                        uniacid: e.$uniacid,
                                        suid: o.getStorageSync("suid"),
                                        type: a,
                                        nav: i,
                                        is_more: 0
                                    },
                                    success: function (t) {
                                        e.orderinfo = t.data.data.list, e.orderinfo_length = t.data.data.list.length;
                                    },
                                    fail: function (t) {}
                                });
                            },
                            orderinfoGo: function (t) {
                                var e = t.currentTarget.dataset.order;
                                o.navigateTo({
                                    url: "/pagesFlashSale/orderDetail_dan/orderDetail_dan?orderid=" + e
                                });
                            },
                            makePhoneCallB: function (t) {
                                var e = this.baseinfo.tel_b;
                                o.makePhoneCall({
                                    phoneNumber: e
                                });
                            },
                            wlinfo: function (t) {
                                var e = t.currentTarget.dataset.kuaidi,
                                    a = t.currentTarget.dataset.kuaidihao;
                                o.navigateTo({
                                    url: "/pages/logistics_state/logistics_state?kuaidi=" + e + "&kuaidihao=" + a
                                });
                            },
                            qrshouh: function (t) {
                                var e = this,
                                    a = t.currentTarget.dataset.order,
                                    i = o.getStorageSync("suid");
                                o.showModal({
                                    title: "提示",
                                    content: "确认收货吗？",
                                    success: function (t) {
                                        t.confirm && o.request({
                                            url: e.$baseurl + "doPagedanshouhuo",
                                            data: {
                                                uniacid: e.$uniacid,
                                                suid: i,
                                                orderid: a
                                            },
                                            success: function (t) {
                                                o.showToast({
                                                    title: "收货成功！",
                                                    success: function (t) {
                                                        setTimeout(function () {
                                                            e.page = 1, e.getList();
                                                        }, 1500);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            paybox: function (t) {
                                var e = 0,
                                    a = this.orderinfo,
                                    i = t.currentTarget.dataset.order;
                                this.order_id = i;
                                for (var n = 0; n < a.length; n++) a[n].order_id == i && (e = a[n].price);
                                this.pay_money = e, this.mymoney < e && (this.mymoney_pay = 2, this.pay_type = 2,
                                    this.choosepayf = 1), 0 == this.showPay && (this.showPay = 1);
                            },
                            payboxclose: function () {
                                1 == this.showPay && (this.showPay = 0);
                            },
                            choosepay: function (t) {
                                this.pay_type = t.currentTarget.dataset.pay_type, this.choosepayf = t.currentTarget.dataset.type;
                            },
                            pay: function (t) {
                                if (2 == this.is_submit) return !1;
                                this.is_submit = 2, this.$uniacid, this.suid, this.source;
                                var e = this.pay_type,
                                    a = this.pay_money,
                                    i = this.order_id,
                                    n = t.detail.formId;
                                1 == e ? this.pay1(i) : this._showwxpay(this, a, "miaosha", i, n, "/pagesFlashSale/orderlist_dan/orderlist_dan?type=9");
                            },
                            pay1: function (e) {
                                var a = this;
                                o.showModal({
                                    title: "请注意",
                                    content: "您将使用余额支付" + a.pay_money + "元",
                                    success: function (t) {
                                        t.confirm ? (a.payover_do(e), o.showLoading({
                                            title: "下单中...",
                                            mask: !0
                                        })) : o.redirectTo({
                                            url: "/pagesFlashSale/orderlist_dan/orderlist_dan"
                                        });
                                    }
                                });
                            },
                            payover_do: function (t) {
                                var e = this,
                                    a = e.pay_money,
                                    i = (e.mymoney, o.getStorageSync("suid")),
                                    n = o.getStorageSync("openid");
                                o.request({
                                    url: e.$baseurl + "doPagepaynotify",
                                    data: {
                                        out_trade_no: t,
                                        suid: i,
                                        payprice: a,
                                        types: "miaosha",
                                        flag: 0,
                                        formId: e.formId,
                                        uniacid: e.$uniacid,
                                        openid: n,
                                        source: o.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        "失败" == t.data.data.message ? o.showToast({
                                            title: "付款失败, 请刷新后重新付款！",
                                            icon: "none",
                                            mask: !0,
                                            success: function () {
                                                o.navigateTo({
                                                    url: "/pagesFlashSale/orderlist_dan/orderlist_dan?type=9"
                                                }), o.hideLoading();
                                            }
                                        }) : o.showToast({
                                            title: "购买成功！",
                                            icon: "success",
                                            mask: !0,
                                            success: function () {
                                                o.navigateTo({
                                                    url: "/pagesFlashSale/orderlist_dan/orderlist_dan?type=9"
                                                }), o.hideLoading();
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    };
                e.default = t;
            }).call(this, i("543d").default);
        },
        fe7b: function (t, e, a) {
            var i = a("12a3");
            a.n(i).a;
        }
    },
    [
        ["1641", "common/runtime", "common/vendor"]
    ]
]);