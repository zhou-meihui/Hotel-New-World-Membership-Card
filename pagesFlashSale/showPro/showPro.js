var _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (t) {
    return typeof t;
} : function (t) {
    return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
};

(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesFlashSale/showPro/showPro"], {
        "088a": function (t, a, e) {
            (function (t) {
                function a(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                e("020c"), e("921b"), a(e("66fd")), t(a(e("4187")).default);
            }).call(this, e("543d").createPage);
        },
        4187: function (t, a, e) {
            e.r(a);
            var i = e("dccd"),
                o = e("a47a");
            for (var s in o) "default" !== s && function (t) {
                e.d(a, t, function () {
                    return o[t];
                });
            }(s);
            e("d623");
            var n = e("2877"),
                d = Object(n.a)(o.default, i.a, i.b, !1, null, null, null);
            a.default = d.exports;
        },
        4473: function (t, a, e) {},
        a47a: function (t, a, e) {
            e.r(a);
            var i = e("bae7"),
                o = e.n(i);
            for (var s in i) "default" !== s && function (t) {
                e.d(a, t, function () {
                    return i[t];
                });
            }(s);
            a.default = o.a;
        },
        bae7: function (t, e, n) {
            (function (s) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var t, i = (t = n("3584")) && t.__esModule ? t : {
                    default: t
                };
                var o = n("f571"),
                    a = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                baseinfo: "",
                                gid: "",
                                fxsid: "",
                                userid: "",
                                picList: [],
                                infinite: "",
                                start: "",
                                sale_end_time: "",
                                title: "",
                                datas: {
                                    detail_evaluate: []
                                },
                                my_num: "",
                                xg_num: "",
                                commSelf: "",
                                sc: "",
                                imgheights: [],
                                clock_d: "",
                                clock_h: "",
                                clock_m: "",
                                clock_s: "",
                                sto_id: "",
                                a: 1,
                                needAuth: !1,
                                needBind: !1,
                                clocktime: "",
                                pic_video: "",
                                isplay: !1,
                                currentSwiper: 0,
                                minHeight: 220,
                                heighthave: 0,
                                autoplay: !0,
                                shareimg: 0,
                                share: 0,
                                shareimg_url: "",
                                system_w: 0,
                                system_h: 0,
                                img_w: 0,
                                img_h: 0,
                                dlength: 0,
                                fiexdBoxs: 0,
                                buy_person: [{
                                    avatar: ""
                                }, {
                                    avatar: ""
                                }, {
                                    avatar: ""
                                }, {
                                    avatar: ""
                                }, {
                                    avatar: ""
                                }, {
                                    avatar: ""
                                }]
                            };
                        },
                        onLoad: function (t) {
                            var a = this;
                            t.id && (this.gid = t.id), this._baseMin(this);
                            var e = 0;
                            t.fxsid && (e = t.fxsid, s.setStorageSync("fxsid", e)), this.fxsid = e, console.log("获取分销商" + this.fxsid),
                                s.getStorageSync("suid"), t.userid && (this.userid = t.userid), o.getOpenid(e, function () {
                                    a.getPro(), a._getSuperUserInfo(a);
                                });
                            var i = s.getStorageSync("systemInfo");
                            this.img_w = parseInt((.65 * i.windowWidth).toFixed(0)), this.img_h = parseInt((1.875 * this.img_w).toFixed(0)),
                                this.system_w = parseInt(i.windowWidth), this.system_h = parseInt(i.windowHeight);
                        },
                        onPullDownRefresh: function () {
                            this.getPro(), s.stopPullDownRefresh();
                        },
                        onShareAppMessage: function () {
                            var t, a = this,
                                e = s.getStorageSync("suid"),
                                i = a.gid;
                            return t = 1 == a.baseinfo.fxs ? "/pagesFlashSale/showPro/showPro?id=" + i + "&userid=" + e : "/pagesFlashSale/showPro/showPro?id=" + i + "&userid=" + e + "&fxsid=" + e, {
                                title: a.title,
                                path: t,
                                success: function (t) {}
                            };
                        },
                        methods: {
                            navback: function () {
                                s.navigateBack();
                            },
                            toTop: function () {
                                s.pageScrollTo({
                                    scrollTop: 0
                                });
                            },
                            onPageScroll: function (t) {
                                200 < t.scrollTop ? this.fiexdBoxs = 1 : this.fiexdBoxs = 0;
                            },
                            tobuy_before: function () {
                                var a = this;
                                if (!this.getSuid()) return !1;
                                var t = s.getStorageSync("subscribe");
                                if (0 < t.length) {
                                    var e = new Array();
                                    "" != t[2].mid && e.push(t[2].mid), 0 < e.length ? s.requestSubscribeMessage({
                                        tmplIds: e,
                                        success: function (t) {
                                            a.checkvip();
                                        }
                                    }) : a.checkvip();
                                } else a.checkvip();
                            },
                            tobuy: function () {
                                return console.log(6464646464), 1 == this.vip_config ? (s.showModal({
                                    title: "提醒",
                                    content: "该商品必须开通会员卡购买！",
                                    showCancel: !1,
                                    success: function () {
                                        s.navigateTo({
                                            url: "/pages/register/register?type=miaosha"
                                        });
                                    }
                                }), !1) : -1 != this.datas.pro_kc && this.datas.pro_kc < 1 ? (s.showModal({
                                    title: "提醒",
                                    content: "库存不足！",
                                    showCancel: !1,
                                    success: function () {}
                                }), !1) : void s.navigateTo({
                                    url: "/pagesFlashSale/order_dan/order_dan?id=" + this.datas.id
                                });
                            },
                            checkvip: function () {
                                var a = this;
                                s.getStorageSync("openid"), s.request({
                                    url: a.$baseurl + "doPagecheckvip",
                                    data: {
                                        uniacid: a.$uniacid,
                                        kwd: "miaosha",
                                        suid: s.getStorageSync("suid"),
                                        id: a.gid,
                                        gz: 1
                                    },
                                    success: function (t) {
                                        return !0 === t.data.data ? (a.tobuy(), !1) : !1 === t.data.data ? (a.needvip = !0,
                                            s.showModal({
                                                title: "进入失败",
                                                content: "使用本功能需先开通vip!",
                                                showCancel: !1,
                                                success: function (t) {
                                                    t.confirm && s.redirectTo({
                                                        url: "/pages/register/register"
                                                    });
                                                }
                                            }), !1) : void(0 < t.data.data.needgrade ? (a.needvip = !0, 0 < t.data.data.grade ? t.data.data.grade < t.data.data.needgrade ? s.showModal({
                                            title: "进入失败",
                                            content: "使用本功能需成为" + t.data.data.vipname + "(" + t.data.data.needgrade + ")以上等级会员,请先升级!",
                                            showCancel: !1,
                                            success: function (t) {
                                                t.confirm && s.redirectTo({
                                                    url: "/pages/open1/open1"
                                                });
                                            }
                                        }) : a.tobuy() : t.data.data.grade < t.data.data.needgrade ? s.showModal({
                                            title: "进入失败",
                                            content: "使用本功能需成为" + t.data.data.vipname + "(" + t.data.data.needgrade + ")以上等级会员,请先开通会员后再升级会员等级!",
                                            showCancel: !1,
                                            success: function (t) {
                                                t.confirm && s.redirectTo({
                                                    url: "/pages/register/register"
                                                });
                                            }
                                        }) : a.tobuy()) : a.tobuy());
                                    },
                                    fail: function (t) {}
                                });
                            },
                            goevaluate: function () {
                                var t = this.gid;
                                s.navigateTo({
                                    url: "/pagesOther/evaluate_list/evaluate_list?id=" + t + "&protype=miaosha"
                                });
                            },
                            getPro: function (t) {
                                var a = this,
                                    e = s.getStorageSync("suid");
                                s.request({
                                    url: a.$baseurl + "doPageshowPro11",
                                    data: {
                                        types: "flashsale",
                                        id: a.gid,
                                        suid: e,
                                        uniacid: a.$uniacid,
                                        source: s.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        1 == t.data.data.is_saletwo && s.showModal({
                                                title: "提示",
                                                content: "该商品已下架",
                                                showCancel: !1,
                                                success: function () {
                                                    s.redirectTo({
                                                        url: "/pages/index/index"
                                                    });
                                                }
                                            }), 0 == t.data.data.is_flag && s.showModal({
                                                title: "提示",
                                                content: "该商品已不存在,请选择其他商品",
                                                showCancel: !1,
                                                success: function () {
                                                    return s.navigateBack({
                                                        delta: 1
                                                    }), !1;
                                                }
                                            }), 0 == t.data.data.timetobegin && "0" == t.data.data.sale_end_time_copy ? a.infinite = !0 : 0 == t.data.data.timetobegin && 0 == t.data.data.sale_end_time ? a.start = 2 : 0 < t.data.data.timetobegin ? (a.start = 0,
                                                a.daojishi(1e3 * t.data.data.sale_time)) : 0 == t.data.data.timetobegin && 0 < t.data.data.sale_end_time && (a.start = 1,
                                                a.daojishi(1e3 * t.data.data.sale_end_time_copy)), t.data.data.product_txt && (t.data.data.product_txt = t.data.data.product_txt.replace(/\<img/gi, '<img style="width:100%;height:auto;display:block" '),
                                                t.data.data.product_txt = (0, i.default)(t.data.data.product_txt)), t.data.data.con2 && (t.data.data.con2 = t.data.data.con2.replace(/\<img/gi, '<img style="width:100%;height:auto;display:block" '),
                                                t.data.data.con2 = (0, i.default)(t.data.data.con2)), t.data.data.con3 && (t.data.data.con3 = t.data.data.con3.replace(/\<img/gi, '<img style="width:100%;height:auto;display:block" '),
                                                t.data.data.con3 = (0, i.default)(t.data.data.con3)), a.sale_end_time = t.data.data.sale_end_time_copy,
                                            a.picList = t.data.data.text, a.dlength = a.picList.length, a.title = t.data.data.title,
                                            a.datas = t.data.data, a.my_num = t.data.data.my_num, a.xg_num = t.data.data.pro_xz,
                                            a.sc = t.data.data.collectcount, a.commSelf = t.data.data.comment, a.vip_config = t.data.data.vip_config,
                                            a.pic_video = t.data.data.video, a.buy_person = t.data.data.detail_buyuser, s.setStorageSync("isShowLoading", !1),
                                            s.hideNavigationBarLoading(), s.stopPullDownRefresh();
                                    }
                                }), a._givepscore(a, a.gid, "showPro", a.userid, e);
                            },
                            daojishi: function (t) {
                                var a, e, i, o, s = this,
                                    n = new Date().getTime(),
                                    d = t - n;
                                if (0 <= d && (a = Math.floor(d / 1e3 / 60 / 60 / 24), e = Math.floor(d / 1e3 / 60 / 60 % 24) < 10 ? "0" + Math.floor(d / 1e3 / 60 / 60 % 24) : Math.floor(d / 1e3 / 60 / 60 % 24),
                                        i = Math.floor(d / 1e3 / 60 % 60) < 10 ? "0" + Math.floor(d / 1e3 / 60 % 60) : Math.floor(d / 1e3 / 60 % 60),
                                        o = Math.floor(d / 1e3 % 60) < 10 ? "0" + Math.floor(d / 1e3 % 60) : Math.floor(d / 1e3 % 60)),
                                    0 < a) {
                                    if (9 < a) var r = a;
                                    else r = "0" + a;
                                    var c = e,
                                        u = i,
                                        h = o;
                                } else r = "00", c = e, u = i, h = o;
                                s.clock_d = r, s.clock_h = c, s.clock_m = u, s.clock_s = h, setTimeout(function () {
                                    s.daojishi(t);
                                }, 1e3);
                            },
                            countDown: function (a, e) {
                                var i = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : 0,
                                    o = this;
                                "object" != (void 0 === e ? "undefined" : _typeof(e)) && (e = [e]), setTimeout(function () {
                                    for (var t = 0; t < e.length; t++) e[t] -= 1e3;
                                    o.countDown(a, e, i);
                                }, 1e3);
                            },
                            imageLoad: function (t) {
                                var a = t.detail.width,
                                    e = t.detail.height,
                                    i = (e = 750 / (a / e), this.imgheights);
                                i[t.currentTarget.dataset.id] = e, this.imgheights = i, console.log(this.imgheights);
                            },
                            output: function (t) {
                                var a;
                                if (this.sto_id = t.sto_id, a = 0 < t.day ? t.day + "天" + t.hour + "小时" + t.min + "分钟" + t.sec + "秒" : t.hour + "小时" + t.min + "分钟" + t.sec + "秒",
                                    0 == t.day && 0 == t.hour && 0 == t.min && 0 == t.sec)
                                    if (0 == this.start) {
                                        this.start = 1;
                                        var e = new Date().getTime();
                                        that.countDown(this.output, 1e3 * this.sale_end_time - e);
                                    } else 1 == this.start && (this.start = 2);
                                else this.clock = a;
                            },
                            change: function (t) {
                                var a = t.currentTarget.dataset.id;
                                this.a = a;
                            },
                            makePhoneCall: function (t) {
                                var a = this.baseinfo.tel;
                                s.makePhoneCall({
                                    phoneNumber: a
                                });
                            },
                            collect: function (t) {
                                if (!this.getSuid()) return !1;
                                var e = this,
                                    a = (t.currentTarget.dataset.name, s.getStorageSync("suid"));
                                s.request({
                                    url: e.$baseurl + "doPageCollect",
                                    data: {
                                        suid: a,
                                        types: "flashsale",
                                        id: e.gid,
                                        uniacid: e.$uniacid
                                    },
                                    success: function (t) {
                                        var a = t.data.data;
                                        e.sc = "收藏成功" == a ? 1 : 0, s.showToast({
                                            title: a,
                                            icon: "succes",
                                            duration: 1e3,
                                            mask: !0
                                        });
                                    }
                                });
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1;
                            },
                            getSuid: function () {
                                if (s.getStorageSync("suid")) return !0;
                                return s.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                    !1;
                            },
                            swiperLoad: function (i) {
                                var o = this;
                                s.getSystemInfo({
                                    success: function (t) {
                                        var a = i.detail.width / i.detail.height,
                                            e = t.windowWidth / a;
                                        o.heighthave || (o.minHeight = e, o.heighthave = 1);
                                    }
                                });
                            },
                            swiperChange: function (t) {
                                this.autoplay = !0, this.currentSwiper = t.detail.current, this.isplay = !1, this.autoplay = this.autoplay,
                                    this.endvideo();
                            },
                            playvideo: function () {
                                this.autoplay = !1, this.isplay = !0, this.autoplay = this.autoplay;
                            },
                            endvideo: function () {
                                this.autoplay = !0, this.isplay = !1, this.autoplay = this.autoplay;
                            },
                            open_share: function () {
                                this.share = 1;
                            },
                            share_close: function () {
                                this.share = 0, this.shareimg = 0;
                            },
                            h5ShareAppMessage: function () {
                                var a = this,
                                    t = s.getStorageSync("suid");
                                s.showModal({
                                    title: "长按复制链接后分享",
                                    content: this.$host + "/h5/index.html?id=" + this.$uniacid + "#/pagesFlashSale/showPro/showPro?id=" + this.gid + "&fxsid=" + t + "&userid=" + t,
                                    showCancel: !1,
                                    success: function (t) {
                                        a.share = 0;
                                    }
                                });
                            },
                            getShareImg: function () {
                                s.showLoading({
                                    title: "海报生成中"
                                });
                                var a = this;
                                s.request({
                                    url: a.$baseurl + "dopageshareewm",
                                    data: {
                                        uniacid: a.$uniacid,
                                        suid: s.getStorageSync("suid"),
                                        gid: a.gid,
                                        types: "miaosha",
                                        source: s.getStorageSync("source"),
                                        pageUrl: "showPro"
                                    },
                                    success: function (t) {
                                        s.hideLoading(), 0 == t.data.data.error ? (a.shareimg = 1, a.shareimg_url = t.data.data.url) : s.showToast({
                                            title: t.data.data.msg,
                                            icon: "none"
                                        });
                                    }
                                });
                            },
                            closeShare: function () {
                                this.shareimg = 0;
                            },
                            saveImg: function () {
                                var a = this;
                                s.getImageInfo({
                                    src: a.shareimg_url,
                                    success: function (t) {
                                        s.saveImageToPhotosAlbum({
                                            filePath: t.path,
                                            success: function () {
                                                s.showToast({
                                                    title: "保存成功！",
                                                    icon: "none"
                                                }), a.shareimg = 0, a.share = 0;
                                            }
                                        });
                                    }
                                });
                            },
                            aliSaveImg: function () {
                                var a = this;
                                s.getImageInfo({
                                    src: a.shareimg_url,
                                    success: function (t) {
                                        my.saveImage({
                                            url: t.path,
                                            showActionSheet: !0,
                                            success: function () {
                                                my.alert({
                                                    title: "保存成功"
                                                }), a.shareimg = 0, a.share = 0;
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    };
                e.default = a;
            }).call(this, n("543d").default);
        },
        d623: function (t, a, e) {
            var i = e("4473");
            e.n(i).a;
        },
        dccd: function (t, a, e) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                o = [];
            e.d(a, "a", function () {
                return i;
            }), e.d(a, "b", function () {
                return o;
            });
        }
    },
    [
        ["088a", "common/runtime", "common/vendor"]
    ]
]);