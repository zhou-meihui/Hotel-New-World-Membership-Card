(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesFood/food/food"], {
        "24cf": function (t, o, e) {
            e.r(o);
            var i = e("ebc9"),
                a = e.n(i);
            for (var s in i) "default" !== s && function (t) {
                e.d(o, t, function () {
                    return i[t];
                });
            }(s);
            o.default = a.a;
        },
        4064: function (t, o, e) {},
        6820: function (t, o, e) {
            (function (t) {
                function o(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                e("020c"), e("921b"), o(e("66fd")), t(o(e("da29")).default);
            }).call(this, e("543d").createPage);
        },
        "80df": function (t, o, e) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                a = [];
            e.d(o, "a", function () {
                return i;
            }), e.d(o, "b", function () {
                return a;
            });
        },
        be81: function (t, o, e) {
            var i = e("4064");
            e.n(i).a;
        },
        da29: function (t, o, e) {
            e.r(o);
            var i = e("80df"),
                a = e("24cf");
            for (var s in a) "default" !== s && function (t) {
                e.d(o, t, function () {
                    return a[t];
                });
            }(s);
            e("be81");
            var n = e("2877"),
                r = Object(n.a)(a.default, i.a, i.b, !1, null, null, null);
            o.default = r.exports;
        },
        ebc9: function (t, o, i) {
            (function (f) {
                Object.defineProperty(o, "__esModule", {
                    value: !0
                }), o.default = void 0;
                var e = i("f571"),
                    t = {
                        data: function () {
                            return {
                                orderOrBusiness: "order",
                                block: !1,
                                logs: [],
                                goodsH: 0,
                                scrollToGoodsView: 0,
                                toView: "",
                                toViewType: "",
                                GOODVIEWID: "catGood_",
                                animation: !0,
                                goodsNumArr: [0],
                                shoppingCart: {},
                                shoppingCartGoodsId: [],
                                goodMap: {},
                                totalNum: 0,
                                totalPay: 0,
                                showShopCart: !1,
                                fromClickScroll: !1,
                                timeStart: "",
                                timeEnd: "",
                                hideCount: !0,
                                count: 0,
                                needAni: !1,
                                hide_good_box: !0,
                                url: "",
                                protype: 1,
                                baseinfo: [],
                                shangjbs: [],
                                zhm: "",
                                tnum: 0,
                                chessRoomDetail: {
                                    catList: {
                                        goodsList: []
                                    }
                                },
                                gwcdata: [],
                                $imgurl: this.$imgurl,
                                bus_x: 0,
                                bus_y: 0,
                                type_arr: {
                                    val: []
                                },
                                type_title: "",
                                catHighLightIndex: 0,
                                guige_show: 0,
                                guige_chooseed: "",
                                foodInfo: [],
                                realPay: 0,
                                gwcHidden: 2,
                                guige_arr: [],
                                foodid: 0,
                                chooseGoodArr: [],
                                needAuth: !1,
                                needBind: !1
                            };
                        },
                        onShow: function () {
                            this.chooseGoodArr = f.getStorageSync("foodGwc") ? f.getStorageSync("foodGwc") : [],
                                this.totalPay = f.getStorageSync("foodTotalPay") ? f.getStorageSync("foodTotalPay") : 0,
                                this.totalNum = f.getStorageSync("foodTotalNum") ? f.getStorageSync("foodTotalNum") : 0;
                        },
                        onPullDownRefresh: function () {
                            f.stopPullDownRefresh();
                        },
                        onLoad: function (t) {
                            var o = (this.options = t).id;
                            o || (o = 0, f.setStorage({
                                    key: "zid",
                                    data: o
                                })), 0 < o && (this.mid = o, this.getzh(o)), this._baseMin(this), f.getStorageSync("suid"),
                                e.getOpenid(0, function () {}), this.getSjbase(t);
                        },
                        methods: {
                            redirectto: function (t) {
                                var o = t.currentTarget.dataset.link,
                                    e = t.currentTarget.dataset.linktype;
                                this._redirectto(o, e);
                            },
                            getzh: function (t) {
                                var e = this;
                                f.request({
                                    url: e.$baseurl + "doPageGetzh",
                                    data: {
                                        uniacid: e.$uniacid,
                                        zid: t
                                    },
                                    success: function (t) {
                                        e.tnum = t.data.data.zh.tnum, e.zhm = t.data.data.zh.title + t.data.data.zh.tnum;
                                        var o = t.data.data.keys;
                                        f.setStorageSync("arrkey", o), f.setStorageSync("zid", t.data.data.zh.tnum);
                                    },
                                    fail: function (t) {}
                                });
                            },
                            goodsViewScrollFn: function (t) {
                                this.getIndexFromHArr(t.detail.scrollTop);
                            },
                            getIndexFromHArr: function (t) {
                                for (var o = 0; o < this.goodsNumArr.length; o++) {
                                    var e = t - 40 * o;
                                    e >= this.goodsNumArr[o] && e < this.goodsNumArr[o + 1] && (this.fromClickScroll || (this.catHighLightIndex = o));
                                }
                                this.fromClickScroll = !1;
                            },
                            catClickFn: function (t) {
                                var o = t.target.id.split("_")[1],
                                    e = t.target.id.split("_")[2];
                                this.fromClickScroll = !0, this.catHighLightIndex = o, this.toView = this.GOODVIEWID + e;
                            },
                            touchOnGoods: function (t) {
                                clearInterval(this.timer), this.finger = {};
                                var o = {};
                                this.finger.x = t.touches[0].clientX, this.finger.y = t.touches[0].clientY, this.finger.y < this.busPos.y ? o.y = this.finger.y - 150 : o.y = this.busPos.y - 150,
                                    o.x = Math.abs(this.finger.x - this.busPos.x) / 2, this.finger.x > this.busPos.x ? o.x = (this.finger.x - this.busPos.x) / 2 + this.busPos.x : o.x = (this.busPos.x - this.finger.x) / 2 + this.finger.x,
                                    this.linePos = e.bezier([this.busPos, o, this.finger], 30), this.startAnimation(t);
                            },
                            startAnimation: function (t) {
                                var o = 0,
                                    e = this,
                                    i = e.linePos.bezier_points;
                                this.hide_good_box = !1, this.bus_x = e.finger.x, this.bus_y = e.finger.y;
                                var a = i.length;
                                o = a, this.timer = setInterval(function () {
                                    o--, e.bus_x = i[o].x, e.bus_y = i[o].y, o < 1 && (clearInterval(e.timer), e.addGoodToCartFn(t),
                                        e.hide_good_box = !0);
                                }, 22);
                            },
                            tabChange: function (t) {
                                var o = t.currentTarget.dataset.id;
                                this.orderOrBusiness = o;
                            },
                            getSjbase: function (g) {
                                var l = this;
                                f.request({
                                    url: l.$baseurl + "doPageShangjbs",
                                    data: {
                                        uniacid: l.$uniacid
                                    },
                                    success: function (t) {
                                        l.shangjbs = t.data.data, f.setNavigationBarTitle({
                                            title: t.data.data.names
                                        });
                                    },
                                    fail: function (t) {}
                                }), f.request({
                                    url: l.$baseurl + "doPageDingcai",
                                    data: {
                                        uniacid: l.$uniacid
                                    },
                                    success: function (t) {
                                        var o = f.getStorageSync("systemInfo"),
                                            e = g;
                                        l.busPos = {}, l.busPos.x = 45, f.getSystemInfo({
                                                success: function (t) {
                                                    t.windowHeight;
                                                }
                                            }), l.busPos.y = -56, l.mechine = e, l.systemInfo = o, l.goodsH = o.windowHeight - 153 - 48,
                                            console.log(l.systemInfo);
                                        var i = {};
                                        i.catList = t.data.data;
                                        for (var a = [], s = 0; s < t.data.data.length; s++)
                                            for (var n = 0; n < t.data.data[s].goodsList.length; n++) a.push(t.data.data[s].goodsList[n]);
                                        l.chessRoomDetail = i, l.allpro = a, l.toView = l.GOODVIEWID + l.chessRoomDetail.catList[0].id;
                                        for (var r = l.catHighLightIndex = 0; r < l.chessRoomDetail.catList.length; r++) {
                                            l.goodsNumArr.push(l.chessRoomDetail.catList[r].goodsList.length);
                                            var d = l.chessRoomDetail.catList[r].goodsList;
                                            if (0 < d.length)
                                                for (var u = 0; u < d.length; u++) l.goodMap[d[u].id] = d[u];
                                        }
                                        for (var c = [], h = 0; h < l.goodsNumArr.length; h++) 0 == h ? c.push(0) : c.push(98 * l.goodsNumArr[h] + c[h - 1]);
                                        l.goodsNumArr = c;
                                    },
                                    fail: function (t) {}
                                });
                            },
                            makephone: function (t) {
                                f.makePhoneCall({
                                    phoneNumber: t.currentTarget.dataset.tel
                                });
                            },
                            showShopCartFn: function (t) {
                                0 < this.totalPay && (this.showShopCart = !this.showShopCart);
                            },
                            showGuige: function (t) {
                                var a = this;
                                this.foodid = t.currentTarget.id, f.request({
                                    url: this.$baseurl + "doPageGetFoodInfo",
                                    data: {
                                        uniacid: this.$uniacid,
                                        id: t.currentTarget.id
                                    },
                                    success: function (t) {
                                        a.foodInfo = t.data.data.foodInfo, a.realPay = t.data.data.foodInfo.guige_price;
                                        var o, e = "";
                                        for (var i in o = t.data.data.guige_arr, new Array(1, 2, 3), o) e += o[i].val[0] + ",";
                                        a.guige_chooseed = e.substring(0, e.length - 1), a.guige_arr = o, console.log("guige_arr"),
                                            console.log(a.guige_arr), a.guige_show = 1;
                                    }
                                });
                            },
                            closeGuige: function () {
                                this.guige_arr = [], this.guige_show = 0;
                            },
                            chooseed: function (t) {
                                var e = this;
                                this.gwcHidden = 1;
                                var o = t.currentTarget.dataset.topindex,
                                    i = t.currentTarget.dataset.sindex,
                                    a = this.guige_arr,
                                    s = "";
                                for (var n in a)
                                    if (n == o) s += a[n].val[i] + ",";
                                    else {
                                        var r = a[n].ck;
                                        s += a[n].val[r] + ",";
                                    }
                                s = s.substring(0, s.length - 1), f.request({
                                    url: this.$baseurl + "doPageGetFoodKcPrice",
                                    data: {
                                        id: this.foodid,
                                        guige_chooseed: s
                                    },
                                    success: function (t) {
                                        var o = t.data.data;
                                        1 == o.flag ? o.kc <= 0 ? f.showModal({
                                            title: "提示",
                                            content: "该规格库存不足，请重新选择规格",
                                            showCancel: !1,
                                            success: function (t) {}
                                        }) : (e.realPay = o.price, e.gwcHidden = 2) : f.showModal({
                                            title: "提示",
                                            content: "该规格商品已不存在，点击刷新",
                                            showCancel: !1,
                                            success: function (t) {
                                                e.gwcHidden = 2, e.getFoodInfo();
                                            }
                                        });
                                    }
                                }), this.guige_chooseed = s, a[o].ck = i, this.guige_arr = a;
                            },
                            addGwc: function () {
                                for (var t = this.chooseGoodArr, o = this.realPay, e = this.guige_chooseed, i = this.foodInfo.title, a = this.foodInfo.id, s = this.foodInfo.val_id, n = this.totalPay, r = this.totalNum, d = 0, u = 0; u < t.length; u++)
                                    if (a == t[u][0] && t[u][2] == i) {
                                        t[u][3] += 1, d = 1;
                                        break;
                                    }
                                var c = new Array();
                                c[0] = a, c[1] = o, c[2] = i, c[3] = 1, c[4] = s, c[5] = e, n = 1 * o + 1 * n, r = 1 * r + 1,
                                    0 == d && t.push(c), this.chooseGoodArr = t, this.totalPay = n.toFixed(2), this.totalNum = r,
                                    this.setStorage();
                            },
                            addGoodToCartFn: function (t) {
                                var o = t.currentTarget.dataset.index,
                                    e = this.chooseGoodArr,
                                    i = this.totalPay,
                                    a = this.totalNum;
                                e[o][3]++, i = 1 * i + 1 * e[o][1], this.totalPay = i.toFixed(2), this.totalNum = 1 * a + 1,
                                    this.chooseGoodArr = e, this.setStorage();
                            },
                            decreaseGoodToCartFn: function (t) {
                                var o = t.currentTarget.dataset.index,
                                    e = this.chooseGoodArr,
                                    i = this.totalPay,
                                    a = this.totalNum;
                                i = 0 == (a = 1 * a - 1) ? 0 : 1 * i - 1 * e[o][1], e[o][3]--, 0 == e[o][3] && e.splice(o, 1),
                                    this.totalPay = i.toFixed(2), this.totalNum = a, this.chooseGoodArr = e, this.setStorage();
                            },
                            clearGwc: function () {
                                var o = this;
                                f.showModal({
                                    title: "提示",
                                    content: "确定要清空购物车",
                                    success: function (t) {
                                        t.confirm ? (o.totalPay = 0, o.totalNum = 0, o.chooseGoodArr = [], o.showShopCart = !1,
                                            o.setStorage()) : t.cancel;
                                    }
                                });
                            },
                            setStorage: function () {
                                f.setStorage({
                                    key: "foodGwc",
                                    data: this.chooseGoodArr
                                }), f.setStorage({
                                    key: "foodTotalPay",
                                    data: this.totalPay
                                }), f.setStorage({
                                    key: "foodTotalNum",
                                    data: this.totalNum
                                });
                            },
                            goPayFn: function (t) {
                                if (!this.getSuid()) return !1;
                                f.navigateTo({
                                    url: "/pagesFood/food_order/food_order"
                                });
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1;
                            },
                            getSuid: function () {
                                if (f.getStorageSync("suid")) return !0;
                                return f.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                    !1;
                            },
                            goDetail: function (t) {
                                var o = t.currentTarget.id;
                                f.navigateTo({
                                    url: "/pagesFood/food_detail/food_detail?id=" + o
                                });
                            }
                        }
                    };
                o.default = t;
            }).call(this, i("543d").default);
        }
    },
    [
        ["6820", "common/runtime", "common/vendor"]
    ]
]);