(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesFood/food_detail/food_detail"], {
        "4cfa": function (t, a, i) {
            (function (r) {
                Object.defineProperty(a, "__esModule", {
                    value: !0
                }), a.default = void 0;
                var t, s = (t = i("3584")) && t.__esModule ? t : {
                    default: t
                };
                var e = i("f571"),
                    o = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                baseinfo: [],
                                bus_x: 0,
                                bus_y: 0,
                                showShopCart: !1,
                                totalNum: 0,
                                totalPay: 0,
                                chooseGoodArr: [],
                                guige_arr: [],
                                guige_show: 0,
                                foodInfo: [],
                                realPay: 0,
                                guige_chooseed: "",
                                gwcHidden: 2,
                                needAuth: !1,
                                needBind: !1
                            };
                        },
                        onPullDownRefresh: function () {
                            this.getFoodInfo(), r.stopPullDownRefresh();
                        },
                        onLoad: function (t) {
                            var o = this;
                            this._baseMin(this), this.chooseGoodArr = r.getStorageSync("foodGwc") ? r.getStorageSync("foodGwc") : [],
                                this.totalPay = r.getStorageSync("foodTotalPay") ? r.getStorageSync("foodTotalPay") : 0,
                                this.totalNum = r.getStorageSync("foodTotalNum") ? r.getStorageSync("foodTotalNum") : 0,
                                t.id && (this.id = t.id);
                            t.fxsid && (this.fxsid = t.fxsid), r.getStorageSync("suid"), e.getOpenid(0, function () {
                                o.getFoodInfo();
                            });
                        },
                        methods: {
                            getFoodInfo: function () {
                                var n = this;
                                r.request({
                                    url: this.$baseurl + "doPageGetFoodInfo",
                                    data: {
                                        uniacid: this.$uniacid,
                                        id: this.id
                                    },
                                    success: function (t) {
                                        n.foodInfo = t.data.data.foodInfo, n.realPay = t.data.data.foodInfo.guige_price;
                                        var o = n.foodInfo;
                                        o.product_txt && (o.product_txt = o.product_txt.replace(/\<img/gi, '<img style="width:100%;height:auto;display:block" '),
                                            o.product_txt = (0, s.default)(o.product_txt));
                                        var e = "",
                                            a = t.data.data.guige_arr;
                                        for (var i in new Array(1, 2, 3), a) e += a[i].val[0] + ",";
                                        n.guige_chooseed = e.substring(0, e.length - 1), n.guige_arr = a, r.setNavigationBarTitle({
                                            title: n.foodInfo.title
                                        });
                                    }
                                });
                            },
                            showShopCartFn: function (t) {
                                0 < this.totalPay && (this.showShopCart = !this.showShopCart);
                            },
                            showGuige: function () {
                                this.guige_show = 1;
                            },
                            closeGuige: function () {
                                this.guige_show = 0;
                            },
                            chooseed: function (t) {
                                var e = this;
                                this.gwcHidden = 1;
                                var o = t.currentTarget.dataset.topindex,
                                    a = t.currentTarget.dataset.subindex,
                                    i = this.guige_arr,
                                    n = "";
                                for (var s in i)
                                    if (s == o) n += i[s].val[a] + ",";
                                    else {
                                        var d = i[s].ck;
                                        n += i[s].val[d] + ",";
                                    }
                                n = n.substring(0, n.length - 1), r.request({
                                    url: this.$baseurl + "doPageGetFoodKcPrice",
                                    data: {
                                        id: this.id,
                                        guige_chooseed: n
                                    },
                                    success: function (t) {
                                        var o = t.data.data;
                                        1 == o.flag ? o.kc <= 0 ? r.showModal({
                                            title: "提示",
                                            content: "该规格库存不足，请重新选择规格",
                                            showCancel: !1,
                                            success: function (t) {}
                                        }) : (e.realPay = o.price, e.gwcHidden = 2) : r.showModal({
                                            title: "提示",
                                            content: "该规格商品已不存在，点击刷新",
                                            showCancel: !1,
                                            success: function (t) {
                                                e.gwcHidden = 2, e.getFoodInfo();
                                            }
                                        });
                                    }
                                }), this.guige_chooseed = n, i[o].ck = a, this.guige_arr = i;
                            },
                            addGwc: function () {
                                for (var t = this.chooseGoodArr, o = this.realPay, e = this.guige_chooseed, a = this.foodInfo.title, i = this.foodInfo.id, n = this.totalPay, s = this.totalNum, d = 0, r = 0; r < t.length; r++)
                                    if (i == t[r][0] && t[r][2] == a) {
                                        t[r][3] += 1, d = 1;
                                        break;
                                    }
                                var u = new Array();
                                u[0] = i, u[1] = o, u[2] = a, u[3] = 1, u[5] = e, n = 1 * o + 1 * n, s = 1 * s + 1,
                                    0 == d && t.push(u), this.chooseGoodArr = t, this.totalPay = n.toFixed(2), this.totalNum = s,
                                    this.setStorage();
                            },
                            addGoodToCartFn: function (t) {
                                var o = t.currentTarget.dataset.index,
                                    e = this.chooseGoodArr,
                                    a = this.totalPay,
                                    i = this.totalNum;
                                e[o][3]++, a = 1 * a + 1 * e[o][1], this.totalPay = a.toFixed(2), this.totalNum = 1 * i + 1,
                                    this.chooseGoodArr = e, this.setStorage();
                            },
                            decreaseGoodToCartFn: function (t) {
                                var o = t.currentTarget.dataset.index,
                                    e = this.chooseGoodArr,
                                    a = this.totalPay,
                                    i = this.totalNum;
                                a = 0 == (i = 1 * i - 1) ? 0 : 1 * a - 1 * e[o][1], e[o][3]--, 0 == e[o][3] && e.splice(o, 1),
                                    this.totalPay = a.toFixed(2), this.totalNum = i, this.chooseGoodArr = e, this.setStorage();
                            },
                            clearGwc: function () {
                                var o = this;
                                r.showModal({
                                    title: "提示",
                                    content: "确定要清空购物车",
                                    success: function (t) {
                                        t.confirm ? (o.totalPay = 0, o.totalNum = 0, o.chooseGoodArr = [], o.showShopCart = !1,
                                            o.setStorage()) : t.cancel;
                                    }
                                });
                            },
                            setStorage: function () {
                                r.setStorage({
                                    key: "foodGwc",
                                    data: this.chooseGoodArr
                                }), r.setStorage({
                                    key: "foodTotalPay",
                                    data: this.totalPay
                                }), r.setStorage({
                                    key: "foodTotalNum",
                                    data: this.totalNum
                                });
                            },
                            goPayFn: function (t) {
                                if (!this.getSuid()) return !1;
                                0 < this.totalPay && (r.setStorage({
                                    key: "foodGwc",
                                    data: this.chooseGoodArr
                                }), r.setStorage({
                                    key: "foodTotalPayGwc",
                                    data: this.totalPay
                                }), r.navigateTo({
                                    url: "/pagesFood/food_order/food_order"
                                }));
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1;
                            },
                            getSuid: function () {
                                if (r.getStorageSync("suid")) return !0;
                                return r.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                    !1;
                            }
                        }
                    };
                a.default = o;
            }).call(this, i("543d").default);
        },
        "4d42": function (t, o, e) {
            (function (t) {
                function o(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                e("020c"), e("921b"), o(e("66fd")), t(o(e("f8e8")).default);
            }).call(this, e("543d").createPage);
        },
        "76ba": function (t, o, e) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            e.d(o, "a", function () {
                return a;
            }), e.d(o, "b", function () {
                return i;
            });
        },
        "82bf": function (t, o, e) {},
        b025: function (t, o, e) {
            var a = e("82bf");
            e.n(a).a;
        },
        dceb: function (t, o, e) {
            e.r(o);
            var a = e("4cfa"),
                i = e.n(a);
            for (var n in a) "default" !== n && function (t) {
                e.d(o, t, function () {
                    return a[t];
                });
            }(n);
            o.default = i.a;
        },
        f8e8: function (t, o, e) {
            e.r(o);
            var a = e("76ba"),
                i = e("dceb");
            for (var n in i) "default" !== n && function (t) {
                e.d(o, t, function () {
                    return i[t];
                });
            }(n);
            e("b025");
            var s = e("2877"),
                d = Object(s.a)(i.default, a.a, a.b, !1, null, null, null);
            o.default = d.exports;
        }
    },
    [
        ["4d42", "common/runtime", "common/vendor"]
    ]
]);