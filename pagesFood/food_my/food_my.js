(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesFood/food_my/food_my"], {
        "0ad1": function (n, t, e) {
            (function (n) {
                function t(n) {
                    return n && n.__esModule ? n : {
                        default: n
                    };
                }
                e("020c"), e("921b"), t(e("66fd")), n(t(e("aa98")).default);
            }).call(this, e("543d").createPage);
        },
        "1c76": function (n, t, e) {
            e.r(t);
            var a = e("703d"),
                i = e.n(a);
            for (var o in a) "default" !== o && function (n) {
                e.d(t, n, function () {
                    return a[n];
                });
            }(o);
            t.default = i.a;
        },
        "2a92": function (n, t, e) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            e.d(t, "a", function () {
                return a;
            }), e.d(t, "b", function () {
                return i;
            });
        },
        6351: function (n, t, e) {},
        "6b32": function (n, t, e) {
            var a = e("6351");
            e.n(a).a;
        },
        "703d": function (n, t, a) {
            (function (e) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var i = a("f571"),
                    n = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                gwc: "",
                                allprice: "",
                                xiansz: !1,
                                mydingd: [],
                                mydingd_lenght: 0,
                                baseinfo: [],
                                page_signs: "food"
                            };
                        },
                        onPullDownRefresh: function () {
                            this.getmy(), e.stopPullDownRefresh();
                        },
                        onLoad: function (n) {
                            var t = this,
                                e = this,
                                a = 0;
                            n.fxsid && (a = n.fxsid, e.fxsid = n.fxsid), this._baseMin(this), i.getOpenid(a, function () {
                                e.getmy();
                            }, function () {
                                t.needAuth = !0;
                            }, function () {
                                t.needBind = !0;
                            });
                        },
                        methods: {
                            redirectto: function (n) {
                                var t = n.currentTarget.dataset.link,
                                    e = n.currentTarget.dataset.linktype;
                                this._redirectto(t, e);
                            },
                            getmy: function () {
                                var t = this,
                                    n = e.getStorageSync("suid");
                                t.allprice, t.xiansz = !0, e.request({
                                    url: t.$baseurl + "doPageMycai",
                                    data: {
                                        suid: n,
                                        uniacid: t.$uniacid
                                    },
                                    header: {
                                        "content-type": "application/x-www-form-urlencoded"
                                    },
                                    success: function (n) {
                                        t.mydingd = n.data.data, t.mydingd_lenght = n.data.data.length;
                                    }
                                });
                            },
                            pay_again: function (n) {
                                var t = n.currentTarget.dataset.id;
                                e.navigateTo({
                                    url: "/pagesFood/food_order/food_order?id=" + t
                                });
                            }
                        }
                    };
                t.default = n;
            }).call(this, a("543d").default);
        },
        aa98: function (n, t, e) {
            e.r(t);
            var a = e("2a92"),
                i = e("1c76");
            for (var o in i) "default" !== o && function (n) {
                e.d(t, n, function () {
                    return i[n];
                });
            }(o);
            e("6b32");
            var d = e("2877"),
                u = Object(d.a)(i.default, a.a, a.b, !1, null, null, null);
            t.default = u.exports;
        }
    },
    [
        ["0ad1", "common/runtime", "common/vendor"]
    ]
]);