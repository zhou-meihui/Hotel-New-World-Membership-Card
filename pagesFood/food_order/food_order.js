(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesFood/food_order/food_order"], {
        "0878": function (e, t, a) {},
        4420: function (e, t, a) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                n = [];
            a.d(t, "a", function () {
                return i;
            }), a.d(t, "b", function () {
                return n;
            });
        },
        "45f9": function (e, t, a) {
            a.r(t);
            var i = a("9b16"),
                n = a.n(i);
            for (var o in i) "default" !== o && function (e) {
                a.d(t, e, function () {
                    return i[e];
                });
            }(o);
            t.default = n.a;
        },
        "63a2": function (e, t, a) {
            (function (e) {
                function t(e) {
                    return e && e.__esModule ? e : {
                        default: e
                    };
                }
                a("020c"), a("921b"), t(a("66fd")), e(t(a("9b34")).default);
            }).call(this, a("543d").createPage);
        },
        "719b": function (e, t, a) {
            var i = a("0878");
            a.n(i).a;
        },
        "9b16": function (e, t, a) {
            (function (w) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var i = a("f571"),
                    e = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                needAuth: !1,
                                needBind: !1,
                                gwc: [],
                                allprice: "",
                                xiansz: !1,
                                chuydate: "请选择日期",
                                chuytime: "请选择时间",
                                address: "",
                                xinxi: {
                                    username: "",
                                    usertel: "",
                                    address: "",
                                    userdate: "",
                                    usertime: "",
                                    userbeiz: ""
                                },
                                usname: 0,
                                ustel: 0,
                                usadd: 0,
                                usdate: 0,
                                ustime: 0,
                                dikou_score: 0,
                                dikou_money: 0,
                                zf_type: 0,
                                zbiao: 0,
                                index: -1,
                                zhs: [],
                                shangjbs: [],
                                pro_txt: "",
                                pay_type: 1,
                                show_pay_type: 0,
                                alipay: 1,
                                wxpay: 1,
                                zf_money: 0,
                                id: 0,
                                yue_money: 0,
                                wx_money: 0
                            };
                        },
                        onPullDownRefresh: function () {
                            this.getinfos(), w.stopPullDownRefresh();
                        },
                        onLoad: function (e) {
                            var t = this,
                                a = this;
                            this._baseMin(this), w.setNavigationBarTitle({
                                title: "餐饮下单"
                            }), e.id ? (a.id = e.id, a.getOrderInfo()) : (w.getStorage({
                                key: "arrkey",
                                success: function (e) {
                                    e.data && (a.index = e.data);
                                }
                            }), w.getStorage({
                                key: "foodGwc",
                                success: function (e) {
                                    a.gwc = e.data;
                                }
                            }), w.getStorage({
                                key: "foodTotalPay",
                                success: function (e) {
                                    a.allprice = e.data;
                                }
                            }), w.getStorage({
                                key: "mp_address",
                                success: function (e) {
                                    a.address = e.data;
                                }
                            }));
                            e.fxsid && (a.fxsid = e.fxsid), i.getOpenid(0, function () {
                                a.zhchange(), a.getSjbase();
                            }, function () {
                                t.needAuth = !0;
                            }, function () {
                                t.needBind = !0;
                            });
                        },
                        methods: {
                            closeAuth: function () {
                                console.log("closeAuth"), this.needAuth = !1, this._checkBindPhone(this);
                            },
                            closeBind: function () {
                                console.log("closeBind"), this.needBind = !1, that.zhchange(), that.getSjbase();
                            },
                            redirectto: function (e) {
                                var t = e.currentTarget.dataset.link,
                                    a = e.currentTarget.dataset.linktype;
                                this._redirectto(t, a);
                            },
                            zhchange: function () {
                                var a = this,
                                    i = a.index;
                                w.request({
                                    url: a.$baseurl + "doPageZhchange",
                                    data: {
                                        uniacid: a.$uniacid
                                    },
                                    success: function (e) {
                                        -1 == i && (i = 0), console.log(e.data.data.zhs);
                                        var t = e.data.data.zhs[i];
                                        a.zho = e.data.data, a.zhs = e.data.data.zhs, a.zbiao = t;
                                    },
                                    fail: function (e) {}
                                });
                            },
                            bindPickerChange: function (e) {
                                var t = e.detail.value;
                                if (0 == t) this.index = e.detail.value, this.zbiao = "打包/拼桌";
                                else {
                                    var a = this.zhs;
                                    this.index = e.detail.value, zbiao = a[t];
                                }
                            },
                            getSjbase: function () {
                                var l = this,
                                    e = w.getStorageSync("suid");
                                w.request({
                                    url: l.$baseurl + "doPageShangjbs",
                                    data: {
                                        suid: e,
                                        uniacid: l.$uniacid
                                    },
                                    success: function (e) {
                                        var t = e.data.data.dk_money,
                                            a = e.data.data.dk_score,
                                            i = l.allprice,
                                            n = e.data.data.jf_gz,
                                            o = e.data.data.user_money,
                                            s = 0,
                                            d = 0,
                                            u = 0,
                                            r = 0,
                                            c = 0;
                                        t <= i ? (d = a, o < (u = i - (s = t)) ? u = c = (u - (r = o)).toFixed(2) : (c = 0,
                                                u = r = u)) : (d = 1 * (s = i) / (1 * n.money) * (1 * n.score), u = r = c = 0),
                                            l.shangjbs = e.data.data, l.usname = e.data.data.usname, l.ustel = e.data.data.ustel,
                                            l.usadd = e.data.data.usadd, l.usdate = e.data.data.usdate, l.ustime = e.data.data.ustime,
                                            l.dk_money = t, l.dk_score = a, l.dikou_money = s, l.dikou_score = d, l.yue_money = r,
                                            l.wx_money = c, l.zf_money = u;
                                    },
                                    fail: function (e) {}
                                });
                            },
                            pay: i.throttle(function (e) {
                                var t = this,
                                    a = w.getStorageSync("suid"),
                                    i = t.gwc,
                                    n = t.address,
                                    o = t.xinxi,
                                    s = t.zbiao,
                                    d = e.detail.formId;
                                if (null == s) return w.showModal({
                                    content: "请先选择桌号"
                                }), !1;
                                var u = t.usname,
                                    r = t.ustel,
                                    c = t.usadd,
                                    l = t.usdate,
                                    h = t.ustime;
                                if (2 == u && "" == o.username) return w.showModal({
                                    title: "提醒",
                                    content: "姓名为必填！",
                                    showCancel: !1
                                }), !1;
                                if (2 == r && "" == o.usertel) return w.showModal({
                                    title: "提醒",
                                    content: "手机号为必填！",
                                    showCancel: !1
                                }), !1;
                                if (!/^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(16[0-9]{1})|(17[0-9]{1})|(18[0-9]{1})|(19[0-9]{1}))+\d{8})$/.test(o.usertel) && 2 == r) return w.showModal({
                                    title: "提醒",
                                    content: "请输入有效的手机号码！",
                                    showCancel: !1
                                }), !1;
                                if (2 == c && "" == o.address) return w.showModal({
                                    title: "提醒",
                                    content: "地址为必填！",
                                    showCancel: !1
                                }), !1;
                                if (2 == l && "" == o.userdate) return w.showModal({
                                    title: "提醒",
                                    content: "日期为必填！",
                                    showCancel: !1
                                }), !1;
                                if (2 == h && "" == o.usertime) return w.showModal({
                                    title: "提醒",
                                    content: "时间为必填！",
                                    showCancel: !1
                                }), !1;
                                t.xiansz = !0;
                                var f = t.allprice,
                                    y = t.dikou_money,
                                    _ = t.dikou_score,
                                    g = t.yue_money,
                                    p = t.wx_money;
                                if (0 < this.id) var m = this.orderinfo.order_id;
                                else m = "";
                                if (0 < p) a = w.getStorageSync("suid"), w.request({
                                    url: t.$baseurl + "doPageZhifdingd",
                                    data: {
                                        suid: a,
                                        gwc: JSON.stringify(i),
                                        address: n,
                                        xinxi: JSON.stringify(o),
                                        uniacid: t.$uniacid,
                                        zh: s,
                                        formId: d,
                                        price: f,
                                        dikou_score: _,
                                        dikou_money: y,
                                        wx_money: p,
                                        yue_money: g,
                                        order_id: m,
                                        source: w.getStorageSync("source")
                                    },
                                    header: {
                                        "content-type": "application/json"
                                    },
                                    success: function (e) {
                                        t.clearStorage(), t._showwxpay(t, p, "food", e.data.data, d, "/pagesFood/food_my/food_my");
                                    }
                                });
                                else {
                                    var x = w.getStorageSync("openid");
                                    w.showModal({
                                        title: "提示",
                                        content: "确认支付,费用将从余额直接扣除!",
                                        cancelText: "取消支付",
                                        confirmText: "确认支付",
                                        success: function (e) {
                                            if (e.cancel) return !1;
                                            w.request({
                                                url: t.$baseurl + "doPageZjkk",
                                                data: {
                                                    suid: a,
                                                    price: f,
                                                    dikou_score: _,
                                                    dikou_money: y,
                                                    wx_money: p,
                                                    yue_money: g,
                                                    gwc: JSON.stringify(i),
                                                    address: n,
                                                    xinxi: JSON.stringify(o),
                                                    uniacid: t.$uniacid,
                                                    zh: s,
                                                    formId: d,
                                                    source: w.getStorageSync("source"),
                                                    openid: x,
                                                    order_id: m
                                                },
                                                header: {
                                                    "content-type": "application/json"
                                                },
                                                success: function (e) {
                                                    w.showToast({
                                                        title: "支付成功",
                                                        icon: "success",
                                                        duration: 2e3,
                                                        success: function () {
                                                            t.sendMail_order(e.data.data), t.clearStorage(), w.redirectTo({
                                                                url: "/pagesFood/food_my/food_my"
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            }, 2e3),
                            showpay: function () {
                                var t = this;
                                w.request({
                                    url: this.$baseurl + "doPageGetH5payshow",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: w.getStorageSync("suid")
                                    },
                                    success: function (e) {
                                        0 == e.data.data.ali && 0 == e.data.data.wx ? w.showModal({
                                            title: "提示",
                                            content: "请联系管理员设置支付参数",
                                            success: function (e) {
                                                w.redirectTo({
                                                    url: "/pages/index/index"
                                                });
                                            }
                                        }) : (0 == e.data.data.ali ? (t.alipay = 0, t.pay_type = 2) : (t.alipay = 1, t.pay_type = 1),
                                            0 == e.data.data.wx ? t.wxpay = 0 : t.wxpay = 1, t.show_pay_type = 1);
                                    }
                                });
                            },
                            changealipay: function () {
                                this.pay_type = 1;
                            },
                            changewxpay: function () {
                                this.pay_type = 2;
                            },
                            close_pay_type: function () {
                                this.show_pay_type = 0;
                            },
                            h5topay: function () {
                                var e = this.pay_type;
                                1 == e ? this._alih5pay(this, this.zf_money, 12, this.orderid) : 2 == e && this._wxh5pay(this, this.zf_money, "food", this.orderid),
                                    this.show_pay_type = 0;
                            },
                            clearStorage: function () {
                                this.totalPay = 0, this.totalNum = 0, this.chooseGoodArr = [], w.setStorage({
                                    key: "foodGwc",
                                    data: this.chooseGoodArr
                                }), w.setStorage({
                                    key: "foodTotalPay",
                                    data: this.totalPay
                                }), w.setStorage({
                                    key: "foodTotalNum",
                                    data: this.totalNum
                                });
                            },
                            sendMail_order: function (e) {
                                w.request({
                                    url: this.$baseurl + "doPagesendMail_foodorder",
                                    data: {
                                        order_id: e,
                                        uniacid: this.$uniacid
                                    },
                                    success: function (e) {},
                                    fail: function (e) {}
                                });
                            },
                            userNameInput: function (e) {
                                var t = this.xinxi,
                                    a = e.detail.value;
                                t.username = a, this.xinxi = t;
                            },
                            userTelInput: function (e) {
                                var t = this.xinxi,
                                    a = e.detail.value;
                                t.usertel = a, this.xinxi = t;
                            },
                            userAddInput: function (e) {
                                var t = this.xinxi,
                                    a = e.detail.value;
                                t.address = a, this.xinxi = t;
                            },
                            bindDateChange: function (e) {
                                var t = this.xinxi,
                                    a = e.detail.value;
                                t.userdate = a, this.xinxi = t, this.chuydate = a;
                            },
                            bindTimeChange: function (e) {
                                var t = this.xinxi,
                                    a = e.detail.value;
                                t.usertime = a, this.xinxi = t, this.chuytime = a;
                            },
                            userTextInput: function (e) {
                                var t = this.xinxi,
                                    a = e.detail.value;
                                t.userbeiz = a, this.xinxi = t;
                            },
                            switch1Change: function (e) {
                                var t = this,
                                    a = t.dk_score,
                                    i = t.dk_money,
                                    n = 0,
                                    o = 0,
                                    s = 0,
                                    d = 0,
                                    u = 0,
                                    r = t.shangjbs.user_money,
                                    c = t.allprice;
                                e.detail.value ? (r < (u = c - i) ? (s = u = (u - r).toFixed(2), d = r) : (s = 0,
                                        d = u = u.toFixed(2)), n = i, o = a) : (r <= c ? (u = s = (c - r).toFixed(2), d = r) : (s = 0,
                                        d = u = c), t.dikou_score = 0, t.dikou_money = 0), t.dikou_money = n, t.dikou_score = o,
                                    t.yue_money = d, t.wx_money = s, t.zf_money = u;
                            },
                            getOrderInfo: function () {
                                var t = this,
                                    e = this.id;
                                w.request({
                                    url: this.$baseurl + "doPageGetOrderInfo",
                                    data: {
                                        id: e
                                    },
                                    success: function (e) {
                                        t.orderinfo = e.data.data, t.gwc = e.data.data.val, t.allprice = e.data.data.price;
                                    }
                                });
                            }
                        }
                    };
                t.default = e;
            }).call(this, a("543d").default);
        },
        "9b34": function (e, t, a) {
            a.r(t);
            var i = a("4420"),
                n = a("45f9");
            for (var o in n) "default" !== o && function (e) {
                a.d(t, e, function () {
                    return n[e];
                });
            }(o);
            a("719b");
            var s = a("2877"),
                d = Object(s.a)(n.default, i.a, i.b, !1, null, null, null);
            t.default = d.exports;
        }
    },
    [
        ["63a2", "common/runtime", "common/vendor"]
    ]
]);