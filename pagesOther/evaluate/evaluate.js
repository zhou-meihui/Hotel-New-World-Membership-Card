(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesOther/evaluate/evaluate"], {
        "1ab6": function (t, a, e) {
            var n = e("f479");
            e.n(n).a;
        },
        "3aed": function (t, a, e) {
            (function (r) {
                Object.defineProperty(a, "__esModule", {
                    value: !0
                }), a.default = void 0;
                var o = e("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                assess: 1,
                                assess_duo: [],
                                assessList: [{
                                    id: 1,
                                    icon: "icon-c-haoping",
                                    name: "好评"
                                }, {
                                    id: 2,
                                    icon: "icon-c-zhongchaping",
                                    name: "中评"
                                }, {
                                    id: 3,
                                    icon: "icon-c-zhongchaping",
                                    name: "差评"
                                }],
                                anonymous: 0,
                                order_id: "",
                                count_sy: 5,
                                imgs: [],
                                nowcount: 0,
                                evaluatecon: "",
                                type: "",
                                add: 0,
                                thumb: "",
                                duoPro: "",
                                count: 1,
                                evaluate_duo: [],
                                imgs_duo: [],
                                anonymous_duo: [],
                                pid: 0
                            };
                        },
                        onLoad: function (t) {
                            var a = this,
                                e = this;
                            t.fxsid && (e.fxsid = t.fxsid), t.type && (e.type = t.type);
                            var n = 0;
                            t.add && (n = t.add, e.add = t.add), t.id && (e.pid = t.id), t.order_id && (e.order_id = t.order_id),
                                0 == n ? r.setNavigationBarTitle({
                                    title: "产品评价"
                                }) : r.setNavigationBarTitle({
                                    title: "产品追评"
                                }), this._baseMin(this), e.proinfo(), o.getOpenid(0, function () {}, function () {
                                    a.needAuth = !0;
                                }, function () {
                                    a.needBind = !0;
                                });
                        },
                        onPullDownRefresh: function () {
                            this.proinfo(), r.stopPullDownRefresh();
                        },
                        methods: {
                            proinfo: function () {
                                var n = this;
                                r.request({
                                    url: n.$baseurl + "doPageassesspro",
                                    data: {
                                        uniacid: n.$uniacid,
                                        order_id: n.order_id,
                                        type: n.type,
                                        pid: n.pid
                                    },
                                    success: function (t) {
                                        n.thumb = t.data.data.thumb, n.pid = t.data.data.pid, n.duoPro = t.data.data, n.count = t.data.count;
                                        for (var a = 0; a < n.duoPro.length; a++) {
                                            var e = {
                                                id: 1
                                            };
                                            console.log(e), n.assess_duo.push(e);
                                            n.evaluate_duo.push({
                                                evaluatecon: "",
                                                nowcount: 0
                                            });
                                            n.imgs_duo.push([]);
                                            n.anonymous_duo.push({
                                                anonymous: 0
                                            });
                                        }
                                    }
                                });
                            },
                            chooseAssess: function (t) {
                                var a = t.currentTarget.dataset.id;
                                this.assess = a;
                            },
                            chooseAssess_duo: function (t) {
                                var a = t.currentTarget.dataset.id,
                                    e = t.currentTarget.dataset.dkey;
                                this.assess_duo[e].id = a, console.log(this.assess_duo);
                            },
                            chooseAnonymous: function (t) {
                                var a = this.anonymous;
                                a = 0 == a ? 1 : 0, this.anonymous = a;
                            },
                            chooseAnonymous_duo: function (t) {
                                var a = t.currentTarget.dataset.dkey,
                                    e = this.anonymous_duo[a].anonymous;
                                e = 0 == e ? 1 : 0, this.anonymous_duo[a].anonymous = e;
                            },
                            chooseimg: function () {
                                var s = this,
                                    t = s.count_sy,
                                    u = s.imgs;
                                t -= u.length;
                                var d = s.zhixin;
                                r.chooseImage({
                                    count: t,
                                    sizeType: ["original", "compressed"],
                                    sourceType: ["album", "camera"],
                                    success: function (t) {
                                        d = !0, s.zhixin = d, r.showLoading({
                                            title: "图片上传中"
                                        });
                                        var a = t.tempFilePaths,
                                            n = 0,
                                            o = a.length,
                                            i = s.$baseurl + "wxupimg";
                                        ! function e() {
                                            r.uploadFile({
                                                url: i,
                                                formData: {
                                                    uniacid: s.$uniacid
                                                },
                                                filePath: a[n],
                                                name: "file",
                                                success: function (t) {
                                                    var a = t.data;
                                                    u.push(a), s.imgs = u, ++n < o ? e() : (d = !1, s.zhixin = d, r.hideLoading());
                                                }
                                            });
                                        }();
                                    }
                                });
                            },
                            chooseimg_duo: function (t) {
                                var s = this,
                                    a = s.count_sy,
                                    u = t.currentTarget.dataset.dkey,
                                    d = s.imgs_duo[u];
                                a -= d.length;
                                var c = s.zhixin;
                                r.chooseImage({
                                    count: a,
                                    sizeType: ["original", "compressed"],
                                    sourceType: ["album", "camera"],
                                    success: function (t) {
                                        c = !0, s.zhixin = c, r.showLoading({
                                            title: "图片上传中"
                                        });
                                        var a = t.tempFilePaths,
                                            n = 0,
                                            o = a.length,
                                            i = s.$baseurl + "wxupimg";
                                        ! function e() {
                                            r.uploadFile({
                                                url: i,
                                                formData: {
                                                    uniacid: s.$uniacid
                                                },
                                                filePath: a[n],
                                                name: "file",
                                                success: function (t) {
                                                    var a = t.data;
                                                    d.push(a), s.imgs_duo[u] = d, ++n < o ? e() : (c = !1, s.zhixin = c, r.hideLoading());
                                                }
                                            });
                                        }();
                                    }
                                });
                            },
                            delimg_duo: function (t) {
                                var a = t.currentTarget.dataset.dkey,
                                    e = t.currentTarget.dataset.index;
                                this.imgs_duo[a].splice(e, 1);
                            },
                            delimg: function (t) {
                                var a = t.currentTarget.dataset.index,
                                    e = this.imgs;
                                e.splice(a, 1), this.imgs = e;
                            },
                            evaluate: function (t) {
                                var a = t.detail.value,
                                    e = t.detail.cursor;
                                this.evaluatecon = a, this.nowcount = e;
                            },
                            evaluate_duo_bt: function (t) {
                                var a = t.detail.value,
                                    e = t.detail.cursor,
                                    n = t.currentTarget.dataset.dkey;
                                this.evaluate_duo[n].evaluatecon = a, this.evaluate_duo[n].nowcount = e;
                            },
                            submit: function () {
                                var a = this,
                                    e = a.assess,
                                    n = a.evaluatecon,
                                    o = a.imgs,
                                    i = a.anonymous;
                                if ("" == n) return r.showModal({
                                    title: "提示",
                                    content: "评价不能为空",
                                    showCancel: !1
                                }), !1;
                                r.request({
                                    url: a.$baseurl + "doPageCheckContents",
                                    data: {
                                        uniacid: a.$uniacid,
                                        content: n,
                                        source: r.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        return 1 == t.data.data ? (r.showModal({
                                            title: "提示",
                                            content: "小程序相关信息错误，无法发布",
                                            showCancel: !1
                                        }), !1) : 2 == t.data.data ? (r.showModal({
                                            title: "提示",
                                            content: "提交内容涉嫌违法违规，请修改后再提交",
                                            showCancel: !1
                                        }), !1) : void(0 == t.data.data && r.request({
                                            url: a.$baseurl + "doPageEvaluateSub",
                                            cachetime: "30",
                                            data: {
                                                uniacid: a.$uniacid,
                                                assess: e,
                                                evaluatecon: n,
                                                imgs: JSON.stringify(o),
                                                anonymous: i,
                                                suid: r.getStorageSync("suid"),
                                                order_id: a.order_id,
                                                pid: a.pid,
                                                type: a.type,
                                                add: a.add
                                            },
                                            success: function (t) {
                                                1 == t.data ? r.showModal({
                                                    title: "提示",
                                                    content: "评价提交成功",
                                                    showCancel: !1,
                                                    success: function (t) {
                                                        r.navigateBack({
                                                            delta: 1
                                                        });
                                                    }
                                                }) : r.showModal({
                                                    title: "提示",
                                                    content: "评价提交失败"
                                                });
                                            },
                                            fail: function (t) {}
                                        }));
                                    }
                                });
                            },
                            submit_duo: function () {
                                for (var a = this, e = a.assess_duo, n = a.evaluate_duo, o = a.imgs_duo, i = a.anonymous_duo, t = "", s = 0; s < n.length; s++) {
                                    if ("" == n[s].evaluatecon) return r.showModal({
                                        title: "提示",
                                        content: "评价不能为空",
                                        showCancel: !1
                                    }), !1;
                                    t += n[s].evaluatecon;
                                }
                                r.request({
                                    url: a.$baseurl + "doPageCheckContents",
                                    data: {
                                        uniacid: a.$uniacid,
                                        content: t,
                                        source: r.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        return 1 == t.data.data ? (r.showModal({
                                            title: "提示",
                                            content: "小程序相关信息错误，无法发布",
                                            showCancel: !1
                                        }), !1) : 2 == t.data.data ? (r.showModal({
                                            title: "提示",
                                            content: "提交内容涉嫌违法违规，请修改后再提交",
                                            showCancel: !1
                                        }), !1) : void(0 == t.data.data && r.request({
                                            url: a.$baseurl + "doPageDuoEvaluateSub",
                                            cachetime: "30",
                                            data: {
                                                uniacid: a.$uniacid,
                                                assess: JSON.stringify(e),
                                                evaluatecon: JSON.stringify(n),
                                                imgs: JSON.stringify(o),
                                                anonymous: JSON.stringify(i),
                                                suid: r.getStorageSync("suid"),
                                                order_id: a.order_id,
                                                duoPro: JSON.stringify(a.duoPro),
                                                add: a.add
                                            },
                                            success: function (t) {
                                                t.data ? r.showModal({
                                                    title: "提示",
                                                    content: "评价提交成功",
                                                    showCancel: !1,
                                                    success: function (t) {
                                                        r.navigateBack({
                                                            delta: 1
                                                        });
                                                    }
                                                }) : r.showModal({
                                                    title: "提示",
                                                    content: "评价提交失败"
                                                });
                                            },
                                            fail: function (t) {}
                                        }));
                                    }
                                });
                            }
                        }
                    };
                a.default = t;
            }).call(this, e("543d").default);
        },
        5517: function (t, a, e) {
            (function (t) {
                function a(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                e("020c"), e("921b"), a(e("66fd")), t(a(e("af64")).default);
            }).call(this, e("543d").createPage);
        },
        "6b04": function (t, a, e) {
            var n = function () {
                    this.$createElement;
                    this._self._c;
                },
                o = [];
            e.d(a, "a", function () {
                return n;
            }), e.d(a, "b", function () {
                return o;
            });
        },
        "7f12": function (t, a, e) {
            e.r(a);
            var n = e("3aed"),
                o = e.n(n);
            for (var i in n) "default" !== i && function (t) {
                e.d(a, t, function () {
                    return n[t];
                });
            }(i);
            a.default = o.a;
        },
        af64: function (t, a, e) {
            e.r(a);
            var n = e("6b04"),
                o = e("7f12");
            for (var i in o) "default" !== i && function (t) {
                e.d(a, t, function () {
                    return o[t];
                });
            }(i);
            e("1ab6");
            var s = e("2877"),
                u = Object(s.a)(o.default, n.a, n.b, !1, null, null, null);
            a.default = u.exports;
        },
        f479: function (t, a, e) {}
    },
    [
        ["5517", "common/runtime", "common/vendor"]
    ]
]);