(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesOther/evaluate_list/evaluate_list"], {
        "0db3": function (t, e, a) {
            (function (i) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var u = a("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                flag: 0,
                                id: 0,
                                protype: "",
                                list: {
                                    imgs: "",
                                    append_imgs: []
                                },
                                list_length: 0,
                                zan: 0,
                                page: 1,
                                user: "",
                                baseinfo: ""
                            };
                        },
                        onLoad: function (t) {
                            var e = this,
                                a = this;
                            a.list = [], i.setNavigationBarTitle({
                                title: "商品评价列表"
                            }), t.user && (a.user = t.user), t.id && (a.id = t.id), t.protype && (a.protype = t.protype);
                            var n = 0;
                            t.fxsid && (n = t.fxsid, a.fxsid = t.fxsid), this._baseMin(this), u.getOpenid(n, function () {
                                e.getlist();
                            }, function () {
                                e.needAuth = !0;
                            }, function () {
                                e.needBind = !0;
                            });
                        },
                        onPullDownRefresh: function () {
                            this.page = 1, this.getlist(), i.stopPullDownRefresh();
                        },
                        onReachBottom: function () {
                            var e = this,
                                t = e.flag;
                            e.flag = t;
                            var a = e.page;
                            i.request({
                                url: e.$baseurl + "doPageEvaluateList",
                                data: {
                                    uniacid: e.$uniacid,
                                    flag: t,
                                    id: e.id,
                                    protype: e.protype,
                                    page: a,
                                    suid: i.getStorageSync("suid"),
                                    source: i.getStorageSync("source"),
                                    user: e.user
                                },
                                success: function (t) {
                                    e.page = a + 1, e.list = 1 == a ? t.data : e.list.concat(t.data);
                                }
                            });
                        },
                        methods: {
                            goevaluatecon: function (t) {
                                var e = t.currentTarget.dataset.id;
                                i.navigateTo({
                                    url: "/pagesOther/evaluate_detail/evaluate_detail?id=" + e + "&protype=" + this.protype
                                });
                            },
                            getlist: function (t) {
                                var e = this;
                                if (null == t) var a = e.flag;
                                else a = t.currentTarget.dataset.flag;
                                e.flag = a;
                                var n = e.page;
                                i.request({
                                    url: e.$baseurl + "doPageEvaluateList",
                                    data: {
                                        uniacid: e.$uniacid,
                                        flag: a,
                                        id: e.id,
                                        protype: e.protype,
                                        page: n,
                                        suid: i.getStorageSync("suid"),
                                        source: i.getStorageSync("source"),
                                        user: e.user
                                    },
                                    success: function (t) {
                                        e.list = t.data, e.list_length = e.list.length;
                                    }
                                });
                            },
                            addLikes: function (t) {
                                var e = this,
                                    a = t.currentTarget.dataset.id;
                                i.request({
                                    url: e.$baseurl + "doPageEvaluatelikes",
                                    data: {
                                        uniacid: e.$uniacid,
                                        id: a,
                                        suid: i.getStorageSync("suid")
                                    },
                                    success: function (t) {
                                        1 == t.data.flag ? (1 == t.data.likes && (i.showToast({
                                            title: "点赞成功"
                                        }), i.startPullDownRefresh(), e.page = 1, e.zan = 1, i.stopPullDownRefresh()), 2 == t.data.likes && (i.showToast({
                                            title: "取赞成功"
                                        }), i.startPullDownRefresh(), e.page = 1, e.zan = 0, i.stopPullDownRefresh())) : i.showModal({
                                            title: "提示",
                                            content: "点赞失败",
                                            showCancel: !1
                                        });
                                    }
                                });
                            }
                        }
                    };
                e.default = t;
            }).call(this, a("543d").default);
        },
        "396e": function (t, e, a) {
            a.r(e);
            var n = a("b81d"),
                i = a("b997");
            for (var u in i) "default" !== u && function (t) {
                a.d(e, t, function () {
                    return i[t];
                });
            }(u);
            a("7bd3");
            var s = a("2877"),
                r = Object(s.a)(i.default, n.a, n.b, !1, null, null, null);
            e.default = r.exports;
        },
        "75e5": function (t, e, a) {},
        "7bd3": function (t, e, a) {
            var n = a("75e5");
            a.n(n).a;
        },
        a3f6: function (t, e, a) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                a("020c"), a("921b"), e(a("66fd")), t(e(a("396e")).default);
            }).call(this, a("543d").createPage);
        },
        b81d: function (t, e, a) {
            var n = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            a.d(e, "a", function () {
                return n;
            }), a.d(e, "b", function () {
                return i;
            });
        },
        b997: function (t, e, a) {
            a.r(e);
            var n = a("0db3"),
                i = a.n(n);
            for (var u in n) "default" !== u && function (t) {
                a.d(e, t, function () {
                    return n[t];
                });
            }(u);
            e.default = i.a;
        }
    },
    [
        ["a3f6", "common/runtime", "common/vendor"]
    ]
]);