(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesPluginForum/collect/collect"], {
        "12ef": function (e, t, n) {
            (function (e) {
                function t(e) {
                    return e && e.__esModule ? e : {
                        default: e
                    };
                }
                n("020c"), n("921b"), t(n("66fd")), e(t(n("c276")).default);
            }).call(this, n("543d").createPage);
        },
        a92a: function (e, t, n) {},
        c1dd: function (e, t, n) {
            var a = n("a92a");
            n.n(a).a;
        },
        c276: function (e, t, n) {
            n.r(t);
            var a = n("f3b3"),
                i = n("ff50");
            for (var o in i) "default" !== o && function (e) {
                n.d(t, e, function () {
                    return i[e];
                });
            }(o);
            n("c1dd");
            var u = n("2877"),
                c = Object(u.a)(i.default, a.a, a.b, !1, null, null, null);
            t.default = c.exports;
        },
        e5d0: function (e, t, n) {
            (function (a) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var i = n("f571"),
                    e = {
                        data: function () {
                            return {
                                list: [],
                                release: [],
                                page: 1,
                                baseinfo: "",
                                needAuth: !1,
                                needBind: !1,
                                isActive: 1
                            };
                        },
                        onLoad: function (e) {
                            var t = this;
                            a.setNavigationBarTitle({
                                title: "收藏页"
                            });
                            var n = e.id;
                            this.id = n, this._baseMin(this);
                            a.getStorageSync("suid"), i.getOpenid(0, function () {
                                t.getlist();
                            }, function () {
                                t.needAuth = !0;
                            }, function () {
                                t.needBind = !0;
                            });
                        },
                        onReachBottom: function () {
                            var t = this,
                                n = 1 * t.page + 1;
                            a.request({
                                url: t.$baseurl + "doPageGetForumCollect",
                                data: {
                                    uniacid: t.$uniacid,
                                    suid: a.getStorageSync("suid"),
                                    page: n
                                },
                                success: function (e) {
                                    t.list = t.list.concat(e.data.list), t.release = t.release.concat(e.data.release),
                                        t.page = n;
                                }
                            });
                        },
                        onPullDownRefresh: function () {
                            this.getlist(), a.stopPullDownRefresh();
                        },
                        methods: {
                            getlist: function () {
                                var t = this,
                                    e = t.page;
                                wx.request({
                                    url: t.$baseurl + "doPageGetForumCollect",
                                    data: {
                                        uniacid: t.$uniacid,
                                        suid: a.getStorageSync("suid"),
                                        page: e
                                    },
                                    success: function (e) {
                                        t.list = e.data.list, t.release = e.data.release, console.log(t.list);
                                    }
                                });
                            },
                            redirectto: function (e) {
                                var t = e.currentTarget.dataset.link,
                                    n = e.currentTarget.dataset.linktype;
                                this._redirectto(t, n);
                            },
                            releaseInfo: function (e) {
                                var t = e.currentTarget.dataset.id,
                                    n = e.currentTarget.dataset.shenhe;
                                0 == n ? a.showToast({
                                    title: "审核中，请耐心等待",
                                    icon: "none"
                                }) : 2 == n ? a.showToast({
                                    title: "审核失败，请重新发布",
                                    icon: "none"
                                }) : 1 == n && a.navigateTo({
                                    url: "/pagesPluginForum/forum_page/forum_page?rid=" + t
                                });
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1;
                            },
                            change: function (e) {
                                var t = e.currentTarget.dataset.type;
                                console.log("type=" + t), this.isActive = t;
                            }
                        }
                    };
                t.default = e;
            }).call(this, n("543d").default);
        },
        f3b3: function (e, t, n) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            n.d(t, "a", function () {
                return a;
            }), n.d(t, "b", function () {
                return i;
            });
        },
        ff50: function (e, t, n) {
            n.r(t);
            var a = n("e5d0"),
                i = n.n(a);
            for (var o in a) "default" !== o && function (e) {
                n.d(t, e, function () {
                    return a[e];
                });
            }(o);
            t.default = i.a;
        }
    },
    [
        ["12ef", "common/runtime", "common/vendor"]
    ]
]);