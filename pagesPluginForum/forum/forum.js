(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesPluginForum/forum/forum"], {
        "09ba": function (e, t, a) {
            var n = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            a.d(t, "a", function () {
                return n;
            }), a.d(t, "b", function () {
                return i;
            });
        },
        "28fb": function (e, t, n) {
            (function (i) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var a = n("f571"),
                    e = (getApp(), {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                page_signs: "/pagesPluginForum/forum/forum",
                                baseinfo: "",
                                needAuth: !1,
                                needBind: !1,
                                funcAll: [],
                                releaseAll: {
                                    likesAll: [],
                                    commentList: []
                                },
                                pageType: "",
                                fid: "",
                                fids: "",
                                page: 1
                            };
                        },
                        onLoad: function (e) {
                            this._baseMin(this), i.setNavigationBarTitle({
                                title: "发布列表页"
                            });
                            var t = e.fid;
                            0 < e.fid && (this.fid = t), i.getStorageSync("suid"), a.getOpenid(0, function () {});
                        },
                        onShow: function () {
                            this.getlist();
                        },
                        onPullDownRefresh: function () {
                            this.getlist(), i.stopPullDownRefresh();
                        },
                        methods: {
                            getlist: function (e) {
                                var t = this;
                                if (null == e) var a = t.fid;
                                else a = e.currentTarget.dataset.id, t.fid = a;
                                i.request({
                                    url: t.$baseurl + "doPageReleaseAll",
                                    data: {
                                        uniacid: t.$uniacid,
                                        fid: a,
                                        page: t.page,
                                        suid: i.getStorageSync("suid")
                                    },
                                    success: function (e) {
                                        "" != e.data && (2 == e.data.data.is ? i.showModal({
                                            title: "提示",
                                            content: "该分类不存在或不启用",
                                            showCancel: !1,
                                            success: function (e) {
                                                i.navigateBack({
                                                    delta: 1
                                                });
                                            }
                                        }) : (t.funcAll = e.data.data.funcAll, t.fids = "f" + t.fid, t.releaseAll = e.data.data.releaseAll,
                                            t.pageType = e.data.data.pageType));
                                    }
                                });
                            },
                            changeLikes: function (e) {
                                if (!this.getSuid()) return !1;
                                var a = this,
                                    n = e.currentTarget.dataset.index,
                                    t = e.currentTarget.dataset.rid;
                                i.request({
                                    url: a.$baseurl + "doPageForumLikes",
                                    data: {
                                        uniacid: a.$uniacid,
                                        source: i.getStorageSync("source"),
                                        suid: i.getStorageSync("suid"),
                                        rid: t,
                                        vs: 1
                                    },
                                    success: function (e) {
                                        var t = a.releaseAll;
                                        1 == e.data.data.is_like ? (i.showToast({
                                                title: "点赞成功"
                                            }), t[n].is_like = 1) : 2 == e.data.data.is_like && (i.showToast({
                                                title: "取赞成功"
                                            }), t[n].is_like = 2), t[n].likes = e.data.data.num, t[n].likesAll = e.data.data.likesAll,
                                            a.releaseAll = t;
                                    }
                                });
                            },
                            changeCollection: function (e) {
                                if (!this.getSuid()) return !1;
                                var a = this,
                                    n = e.currentTarget.dataset.index,
                                    t = e.currentTarget.dataset.rid;
                                i.request({
                                    url: a.$baseurl + "doPageForumCollection",
                                    data: {
                                        uniacid: a.$uniacid,
                                        suid: i.getStorageSync("suid"),
                                        rid: t,
                                        vs: 1
                                    },
                                    success: function (e) {
                                        var t = a.releaseAll;
                                        1 == e.data.data.is_collect ? (i.showToast({
                                            title: "收藏成功"
                                        }), t[n].is_collect = 1) : 2 == e.data.data.is_collect && (i.showToast({
                                            title: "取收成功"
                                        }), t[n].is_collect = 2), t[n].collection = e.data.data.num, a.releaseAll = t;
                                    }
                                });
                            },
                            goRelease: function () {
                                i.navigateTo({
                                    url: "/pagesPluginForum/release/release?fid=" + this.fid
                                });
                            },
                            goCollect: function () {
                                i.navigateTo({
                                    url: "/pagesPluginForum/collect/collect"
                                });
                            },
                            goContent: function (e) {
                                var t = e.currentTarget.dataset.rid;
                                i.navigateTo({
                                    url: "/pagesPluginForum/forum_page/forum_page?rid=" + t
                                });
                            },
                            getSuid: function () {
                                if (i.getStorageSync("suid")) return !0;
                                return i.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                    !1;
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1;
                            },
                            makephone: function (e) {
                                var t = e.currentTarget.dataset.tel;
                                i.makePhoneCall({
                                    phoneNumber: t
                                });
                            }
                        }
                    });
                t.default = e;
            }).call(this, n("543d").default);
        },
        "33cf": function (e, t, a) {
            (function (e) {
                function t(e) {
                    return e && e.__esModule ? e : {
                        default: e
                    };
                }
                a("020c"), a("921b"), t(a("66fd")), e(t(a("e6e4")).default);
            }).call(this, a("543d").createPage);
        },
        "38f3": function (e, t, a) {
            var n = a("4886");
            a.n(n).a;
        },
        3990: function (e, t, a) {
            a.r(t);
            var n = a("28fb"),
                i = a.n(n);
            for (var u in n) "default" !== u && function (e) {
                a.d(t, e, function () {
                    return n[e];
                });
            }(u);
            t.default = i.a;
        },
        4886: function (e, t, a) {},
        e6e4: function (e, t, a) {
            a.r(t);
            var n = a("09ba"),
                i = a("3990");
            for (var u in i) "default" !== u && function (e) {
                a.d(t, e, function () {
                    return i[e];
                });
            }(u);
            a("38f3");
            var l = a("2877"),
                o = Object(l.a)(i.default, n.a, n.b, !1, null, null, null);
            t.default = o.exports;
        }
    },
    [
        ["33cf", "common/runtime", "common/vendor"]
    ]
]);