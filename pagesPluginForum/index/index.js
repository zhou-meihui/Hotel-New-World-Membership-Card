(global.webpackJsonp = global.webpackJsonp || []).push([
    
    ["pagesPluginForum/index/index"], {
        "2a12": function (n, t, e) {},
        "34e3": function (n, t, e) {
            (function (a) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var u = e("f571"),
                    n = {
                        data: function () {
                            return {
                                data: [],
                                page_signs: "/pagesPluginForum/index/index",
                                baseinfo: "",
                                needAuth: !1,
                                needBind: !1,
                                background: [],

                                $imgurl: this.$imgurl,
                                funcAll: [],
                                releaseAll: {
                                    likesAll: [],
                                    commentList: []
                                },
                                pageType: "",
                                fid: "",
                                fids: "",
                                page: 1
                            };
                        },
                        onLoad: function (n) {
                            var t = this,
                                e = n.id;
                            this.id = e, this._baseMin(this);
                            var i = 0;
                            n.fxsid && (i = n.fxsid), this.fxsid = i, a.getStorageSync("suid"), u.getOpenid(i, function () {
                                t.getMain(e);
                            });
                        },
                        onPullDownRefresh: function () {
                            this.getfunc(), a.stopPullDownRefresh();
                        },
                        onShow: function () {
                            this.getfunc();
                        },
                        methods: {
                            getMain: function () {
                                a.setNavigationBarTitle({
                                    title: "同城首页"
                                });
                            },
                            redirectto: function (n) {
                                var t = n.currentTarget.dataset.link,
                                    e = n.currentTarget.dataset.linktype;
                                this._redirectto(t, e);
                            },
                            getfunc: function () {
                                var t = this;
                                a.request({
                                    url: t.$baseurl + "doPageForumfunc",
                                    data: {
                                        uniacid: t.$uniacid
                                    },
                                    success: function (n) {
                                        t.data = n.data.data;
                                        t.background = n.data.carousel;

                                        var fid = t.data[0]['id'];
                                        0 < fid && (t.fid = fid), a.getStorageSync("suid"), u.getOpenid(0, function () {});
                                        t.getlist();
                                    },
                                    fail: function (n) {}
                                });
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this._checkBindPhone(this);
                            },
                            closeBind: function () {
                                this.needBind = !1, this.getMain();
                            },
                            getlist: function (e) {
                                var t = this;
                                if (null == e) var f = t.fid;
                                else f = e.currentTarget.dataset.id, t.fid = f;

                                a.request({
                                    url: t.$baseurl + "doPageReleaseAll",
                                    data: {
                                        uniacid: t.$uniacid,
                                        fid: f,
                                        page: t.page,
                                        suid: a.getStorageSync("suid")
                                    },
                                    success: function (e) {
                                        "" != e.data && (2 == e.data.data.is ? a.showModal({
                                            title: "提示",
                                            content: "该分类不存在或不启用",
                                            showCancel: !1,
                                            success: function (e) {
                                                a.navigateBack({
                                                    delta: 1
                                                });
                                            }
                                        }) : (t.funcAll = e.data.data.funcAll, t.fids = "f" + t.fid, t.releaseAll = e.data.data.releaseAll,
                                            t.pageType = e.data.data.pageType));
                                    }
                                });
                            },
                            changeLikes: function (e) {
                                if (!this.getSuid()) return !1;
                                var $this = this,
                                    n = e.currentTarget.dataset.index,
                                    t = e.currentTarget.dataset.rid;
                                a.request({
                                    url: $this.$baseurl + "doPageForumLikes",
                                    data: {
                                        uniacid: $this.$uniacid,
                                        source: a.getStorageSync("source"),
                                        suid: a.getStorageSync("suid"),
                                        rid: t,
                                        vs: 1
                                    },
                                    success: function (e) {
                                        var t = $this.releaseAll;
                                        1 == e.data.data.is_like ? (a.showToast({
                                            title: "点赞成功"
                                        }), t[n].is_like = 1) : 2 == e.data.data.is_like && (a.showToast({
                                            title: "取赞成功"
                                        }), t[n].is_like = 2), t[n].likes = e.data.data.num, t[n].likesAll = e.data.data.likesAll,
                                            a.releaseAll = t;
                                    }
                                });
                            },
                            changeCollection: function (e) {
                                if (!this.getSuid()) return !1;
                                var $this = this,
                                    n = e.currentTarget.dataset.index,
                                    t = e.currentTarget.dataset.rid;
                                a.request({
                                    url: $this.$baseurl + "doPageForumCollection",
                                    data: {
                                        uniacid: $this.$uniacid,
                                        suid: a.getStorageSync("suid"),
                                        rid: t,
                                        vs: 1
                                    },
                                    success: function (e) {
                                        var t = $this.releaseAll;
                                        1 == e.data.data.is_collect ? (a.showToast({
                                            title: "收藏成功"
                                        }), t[n].is_collect = 1) : 2 == e.data.data.is_collect && (a.showToast({
                                            title: "取收成功"
                                        }), t[n].is_collect = 2), t[n].collection = e.data.data.num, a.releaseAll = t;
                                    }
                                });
                            },
                            goRelease: function () {
                                a.navigateTo({
                                    url: "/pagesPluginForum/release/release?fid=" + this.fid
                                });
                            },
                            goCollect: function () {
                                a.navigateTo({
                                    url: "/pagesPluginForum/collect/collect"
                                });
                            },
                            goContent: function (e) {
                                var t = e.currentTarget.dataset.rid;
                                a.navigateTo({
                                    url: "/pagesPluginForum/forum_page/forum_page?rid=" + t
                                });
                            },
                            getSuid: function () {
                                if (a.getStorageSync("suid")) return !0;
                                return a.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                    !1;
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            makephone: function (e) {
                                var t = e.currentTarget.dataset.tel;
                                a.makePhoneCall({
                                    phoneNumber: t
                                });
                            }
                        }
                    };
                t.default = n;
            }).call(this, e("543d").default);
        },
        "760a": function (n, t, e) {
            e.r(t);
            var i = e("9a7b"),
                a = e("7fca");
            for (var u in a) "default" !== u && function (n) {
                e.d(t, n, function () {
                    return a[n];
                });
            }(u);
            e("949d");
            var o = e("2877"),
                c = Object(o.a)(a.default, i.a, i.b, !1, null, null, null);
            t.default = c.exports;
        },
        "7fca": function (n, t, e) {
            e.r(t);
            var i = e("34e3"),
                a = e.n(i);
            for (var u in i) "default" !== u && function (n) {
                e.d(t, n, function () {
                    return i[n];
                });
            }(u);
            t.default = a.a;
        },
        "949d": function (n, t, e) {
            var i = e("2a12");
            e.n(i).a;
        },
        "9a7b": function (n, t, e) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                a = [];
            e.d(t, "a", function () {
                return i;
            }), e.d(t, "b", function () {
                return a;
            });
        },
        "b551": function (n, t, e) {
            (function (n) {
                function t(n) {
                    return n && n.__esModule ? n : {
                        default: n
                    };
                }
                e("020c"), e("921b"), t(e("66fd")), n(t(e("760a")).default);
            }).call(this, e("543d").createPage);
        },
        
    },
    
    [
        ["b551", "common/runtime", "common/vendor"]
    ],
    
]);


  