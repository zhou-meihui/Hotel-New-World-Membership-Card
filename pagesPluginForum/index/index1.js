(global.webpackJsonp = global.webpackJsonp || []).push([
    
    ["pagesPluginForum/index/index"], {
        "2a12": function (n, t, e) {},
        "34e3": function (n, t, e) {
            (function (a) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var u = e("f571"),
                    n = {
                        data: function () {
                            return {
                                data: [],
                                page_signs: "/pagesPluginForum/index/index",
                                baseinfo: "",
                                needAuth: !1,
                                needBind: !1,
                                background: []
                            };
                        },
                        onLoad: function (n) {
                            var t = this,
                                e = n.id;
                            this.id = e, this._baseMin(this);
                            var i = 0;
                            n.fxsid && (i = n.fxsid), this.fxsid = i, a.getStorageSync("suid"), u.getOpenid(i, function () {
                                t.getMain(e);
                            });
                        },
                        onPullDownRefresh: function () {
                            this.getfunc(), a.stopPullDownRefresh();
                        },
                        onShow: function () {
                            this.getfunc();
                        },
                        methods: {
                            getMain: function () {
                                a.setNavigationBarTitle({
                                    title: "同城首页"
                                });
                            },
                            redirectto: function (n) {
                                var t = n.currentTarget.dataset.link,
                                    e = n.currentTarget.dataset.linktype;
                                this._redirectto(t, e);
                            },
                            getfunc: function () {
                                var t = this;
                                a.request({
                                    url: t.$baseurl + "doPageForumfunc",
                                    data: {
                                        uniacid: t.$uniacid
                                    },
                                    success: function (n) {
                                        t.data = n.data.data;
                                        t.background = n.data.carousel;
                                    },
                                    fail: function (n) {}
                                });
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this._checkBindPhone(this);
                            },
                            closeBind: function () {
                                this.needBind = !1, this.getMain();
                            }
                        }
                    };
                t.default = n;
            }).call(this, e("543d").default);
        },
        "760a": function (n, t, e) {
            e.r(t);
            var i = e("9a7b"),
                a = e("7fca");
            for (var u in a) "default" !== u && function (n) {
                e.d(t, n, function () {
                    return a[n];
                });
            }(u);
            e("949d");
            var o = e("2877"),
                c = Object(o.a)(a.default, i.a, i.b, !1, null, null, null);
            t.default = c.exports;
        },
        "7fca": function (n, t, e) {
            e.r(t);
            var i = e("34e3"),
                a = e.n(i);
            for (var u in i) "default" !== u && function (n) {
                e.d(t, n, function () {
                    return i[n];
                });
            }(u);
            t.default = a.a;
        },
        "949d": function (n, t, e) {
            var i = e("2a12");
            e.n(i).a;
        },
        "9a7b": function (n, t, e) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                a = [];
            e.d(t, "a", function () {
                return i;
            }), e.d(t, "b", function () {
                return a;
            });
        },
        "b551": function (n, t, e) {
            (function (n) {
                function t(n) {
                    return n && n.__esModule ? n : {
                        default: n
                    };
                }
                e("020c"), e("921b"), t(e("66fd")), n(t(e("760a")).default);
            }).call(this, e("543d").createPage);
        },
        
    },
    
    [
        ["b551", "common/runtime", "common/vendor"]
    ],
    
]);


  