(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesPluginForum/set_top/set_top"], {
        "26c6": function (t, e, i) {
            i.r(e);
            var n = i("e632"),
                a = i("8609");
            for (var s in a) "default" !== s && function (t) {
                i.d(e, t, function () {
                    return a[t];
                });
            }(s);
            i("7fe5");
            var o = i("2877"),
                c = Object(o.a)(a.default, n.a, n.b, !1, null, null, null);
            e.default = c.exports;
        },
        "4c4c": function (t, e, i) {},
        "5bda": function (t, e, i) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                i("020c"), i("921b"), e(i("66fd")), t(e(i("26c6")).default);
            }).call(this, i("543d").createPage);
        },
        "7fe5": function (t, e, i) {
            var n = i("4c4c");
            i.n(n).a;
        },
        8609: function (t, e, i) {
            i.r(e);
            var n = i("ff28"),
                a = i.n(n);
            for (var s in n) "default" !== s && function (t) {
                i.d(e, t, function () {
                    return n[t];
                });
            }(s);
            e.default = a.a;
        },
        e632: function (t, e, i) {
            var n = function () {
                    this.$createElement;
                    this._self._c;
                },
                a = [];
            i.d(e, "a", function () {
                return n;
            }), i.d(e, "b", function () {
                return a;
            });
        },
        ff28: function (t, e, i) {
            (function (o) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var a = i("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                fid: 0,
                                rid: 0,
                                stick_money: 0,
                                stick_days: 0,
                                userMoney: 0,
                                returnpage: 2,
                                allmoney: 0,
                                baseinfo: "",
                                needAuth: !1,
                                needBind: !1,
                                pay_type: 1,
                                show_pay_type: 0,
                                alipay: 1,
                                wxpay: 1,
                                money: 0
                            };
                        },
                        onLoad: function (t) {
                            var e = this,
                                i = t.id;
                            this.id = i, this._baseMin(this), this.fid = t.fid, this.rid = t.rid, this.returnpage = t.returnpage;
                            var n = 0;
                            t.fxsid && (n = t.fxsid), this.fxsid = n, a.getOpenid(n, function () {
                                e.getSet(i);
                            }, function () {
                                e.needAuth = !0;
                            }, function () {
                                e.needBind = !0;
                            });
                        },
                        methods: {
                            getSet: function () {
                                o.setNavigationBarTitle({
                                    title: "置顶"
                                }), this.getForumSet();
                            },
                            getForumSet: function () {
                                var e = this;
                                o.request({
                                    url: e.$baseurl + "doPageForumSet",
                                    data: {
                                        uniacid: e.$uniacid
                                    },
                                    success: function (t) {
                                        e.stick_money = t.data.data.stick_money, e.getUserMoney();
                                    }
                                });
                            },
                            getStickDays: function (t) {
                                var e = t.detail.value,
                                    i = this.stick_money * e;
                                i = i.toFixed(2), this.stick_days = e, this.allmoney = i;
                            },
                            getUserMoney: function () {
                                var e = this;
                                o.request({
                                    url: e.$baseurl + "doPageGetUserMoney",
                                    data: {
                                        uniacid: e.$uniacid,
                                        suid: o.getStorageSync("suid")
                                    },
                                    success: function (t) {
                                        e.userMoney = t.data.data;
                                    }
                                });
                            },
                            formSubmit: function (t) {
                                var e = this,
                                    i = e.stick_days;
                                if (0 == i) return o.showModal({
                                    title: "提示",
                                    content: "请输入置顶天数",
                                    showCancel: !1
                                }), !1;
                                var n = e.stick_money,
                                    a = t.detail.formId,
                                    s = e.userMoney;
                                0 < n ? o.request({
                                    url: e.$baseurl + "doPageForumOrder",
                                    data: {
                                        uniacid: e.$uniacid,
                                        release_money: 0,
                                        stick_days: i,
                                        stick_money: n,
                                        suid: o.getStorageSync("suid"),
                                        formId: a
                                    },
                                    success: function (t) {
                                        0 != t.data && (e.orderid = t.data.orderid, i * n <= 1 * s ? o.showModal({
                                            title: "请注意",
                                            content: "您的余额为" + s + "元，本次将扣除" + i * n + "元",
                                            success: function (t) {
                                                t.confirm && e.setStick(1);
                                            }
                                        }) : (e.setStick(2), 0 < s ? o.showModal({
                                            title: "请注意",
                                            content: "您将余额支付" + s + "元，微信支付" + (i * n - s) + "元",
                                            success: function (t) {
                                                t.confirm && e._showwxpay(e, i * n - s, "settop", e.orderid, a, 1);
                                            }
                                        }) : o.showModal({
                                            title: "请注意",
                                            content: "您将微信支付" + i * n + "元",
                                            success: function (t) {
                                                t.confirm && e._showwxpay(e, i * n, "settop", e.orderid, a, 1);
                                            }
                                        })));
                                    },
                                    fail: function (t) {}
                                }) : e.setStick(1);
                            },
                            setStick: function (t) {
                                var e = this;
                                o.request({
                                    url: e.$baseurl + "doPageSetStick",
                                    data: {
                                        uniacid: e.$uniacid,
                                        rid: e.rid,
                                        stick_money: e.stick_money,
                                        stick_days: e.stick_days,
                                        orderid: e.orderid,
                                        haspay: t,
                                        suid: o.getStorageSync("suid")
                                    },
                                    success: function (t) {
                                        console.log(t), 1 == t.data.data ? o.showModal({
                                            title: "提示",
                                            content: "置顶成功",
                                            showCancel: !1,
                                            success: function (t) {
                                                o.navigateBack({
                                                    delta: e.returnpage
                                                });
                                            }
                                        }) : 2 == t.data.data && o.showModal({
                                            title: "提示",
                                            content: "置顶失败",
                                            showCancel: !1
                                        });
                                    }
                                });
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this._checkBindPhone(this);
                            },
                            closeBind: function () {
                                this.needBind = !1, this.getPic();
                            },
                            changealipay: function () {
                                this.pay_type = 1;
                            },
                            showpay: function () {
                                this.money = this.stick_days * this.stick_money - this.userMoney;
                                var e = this;
                                o.request({
                                    url: this.$baseurl + "doPageGetH5payshow",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: o.getStorageSync("suid")
                                    },
                                    success: function (t) {
                                        0 == t.data.data.ali && 0 == t.data.data.wx ? o.showModal({
                                            title: "提示",
                                            content: "请联系管理员设置支付参数",
                                            showCancel: !1,
                                            success: function (t) {
                                                return !1;
                                            }
                                        }) : (0 == t.data.data.ali ? (e.alipay = 0, e.pay_type = 2) : (e.alipay = 1, e.pay_type = 1),
                                            0 == t.data.data.wx ? e.wxpay = 0 : e.wxpay = 1, e.show_pay_type = 1);
                                    }
                                });
                            },
                            changewxpay: function () {
                                this.pay_type = 2;
                            },
                            close_pay_type: function () {
                                this.show_pay_type = 0;
                            },
                            h5topay: function () {
                                var t = this.pay_type;
                                1 == t ? this._alih5pay(this, this.money, 4, this.orderid) : 2 == t && this._wxh5pay(this, this.money, "settop", this.orderid),
                                    this.show_pay_type = 0;
                            }
                        }
                    };
                e.default = t;
            }).call(this, i("543d").default);
        }
    },
    [
        ["5bda", "common/runtime", "common/vendor"]
    ]
]);