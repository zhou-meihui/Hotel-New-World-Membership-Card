(global.webpackJsonp = global.webpackJsonp || []).push([
  ["pagesPluginShop/goods_buy/goods_buy"], {
    "307d": function (e, a, t) {
      (function (e) {
        function a(e) {
          return e && e.__esModule ? e : {
            default: e
          }
        }
        t("020c"), t("921b"), a(t("66fd")), e(a(t("3f72")).default)
      }).call(this, t("543d").createPage)
    },
    "3f72": function (e, a, t) {
      t.r(a);
      var i = t("74e6"),
        n = t("71fc");
      for (var s in n) "default" !== s && function (e) {
        t.d(a, e, function () {
          return n[e]
        })
      }(s);
      t("d8d7");
      var o = t("2877"),
        d = Object(o.a)(n.default, i.a, i.b, !1, null, null, null);
      a.default = d.exports
    },
    "582d": function (e, a, t) {},
    "71fc": function (e, a, t) {
      t.r(a);
      var i = t("9b1b"),
        n = t.n(i);
      for (var s in i) "default" !== s && function (e) {
        t.d(a, e, function () {
          return i[e]
        })
      }(s);
      a.default = n.a
    },
    "74e6": function (e, a, t) {
      var i = function () {
          this.$createElement;
          this._self._c
        },
        n = [];
      t.d(a, "a", function () {
        return i
      }), t.d(a, "b", function () {
        return n
      })
    },
    "9b1b": function (e, a, t) {
      (function (m) {
        Object.defineProperty(a, "__esModule", {
          value: !0
        }), a.default = void 0, t("2f62");
        var n = t("f571"),
          e = {
            data: function () {
              return {
                $imgurl: this.$imgurl,
                baseinfo: "",
                needAuth: !1,
                needBind: !1,
                id: "",
                bg: "",
                couponlist: [],
                picList: [],
                datas: "",
                autoplay: !1,
                comment: "",
                jhsl: 1,
                dprice: "",
                yhje: 0,
                hjjg: "",
                sfje: "",
                zfje: "",
                order: "",
                pro_name: "",
                pro_tel: "",
                pro_address: "",
                pro_txt: "",
                my_num: "",
                xg_num: "",
                shengyu: "",
                userInfo: "",
                chuydate: "",
                chuytime: "",
                couponprice: 0,
                jqdjg: "请选择",
                yhqid: "0",
                kuaidi: 0,
                nav: 0,
                globaluser: "",
                money: 0,
                showModalStatus: "",
                suid: "",
                pay_type: 1,
                show_pay_type: 0,
                alipay: 1,
                wxpay: 1,
                pagedata: [],
                formdescs: "",
                again: 0,
                orderid: "",
                buy_name: "",
                buy_tel: "",
                buy_address: "",
                payprice: 0,
                order_id: "",
                pd_val: [],
                is_submit: 1
              }
            },
            onLoad: function (e) {
              var a = this,
                t = e.id;
              this.id = t, this._baseMin(this);
              var i = 0;
              e.fxsid && (i = e.fxsid), this.fxsid = i, e.again && (this.again = e.again), e.orderid && (this.order_id = e.orderid), this.refreshSessionkey(), n.getOpenid(i, function () {
                a.getShowPic(t)
              }, function () {
                a.needAuth = !0
              }, function () {
                a.needBind = !0
              })
            },
            methods: {
              closeAuth: function () {
                this.needAuth = !1, this._checkBindPhone(this), this.getShowPic(this.id)
              },
              closeBind: function () {
                console.log("closeBind"), this.needBind = !1, this.getShowPic(this.id)
              },
              refreshSessionkey: function () {
                var a = this;
                m.login({
                  success: function (e) {
                    m.request({
                      url: a.$baseurl + "doPagegetNewSessionkey",
                      data: {
                        uniacid: a.$uniacid,
                        code: e.code
                      },
                      success: function (e) {
                        a.newSessionKey = e.data.data
                      }
                    })
                  }
                })
              },
              getShowPic: function (e) {
                var t = this;
                m.request({
                  url: t.$baseurl + "doPageMymoneyD",
                  data: {
                    uniacid: t.$uniacid,
                    suid: m.getStorageSync("suid")
                  },
                  success: function (e) {
                    t.money = parseFloat(e.data.data.money), t.order_id ? m.request({
                      url: t.$baseurl + "doPageGetShopOrderInfo",
                      data: {
                        uniacid: t.$uniacid,
                        orderid: t.order_id
                      },
                      success: function (e) {
                        0 == e.data.error ? (t.datas = e.data.data.datas, t.hjjg = t.datas.hjjg, t.jqdjg = 0 == e.data.data.coupon ? "未使用优惠券" : "减" + t.datas.cou_price, t.sfje = e.data.data.price, t.nav = e.data.data.nav, t.buy_name = e.data.data.m_address.name, t.buy_tel = e.data.data.m_address.mobile, t.buy_address = e.data.data.m_address.address, t.pro_txt = e.data.data.liuyan, t.zfje = t.money >= t.sfje ? t.sfje : ((100 * t.sfje - 100 * t.money) / 100).toFixed(2), t.payprice = e.data.data.payprice) : (m.showModal({
                          title: "提醒",
                          content: e.data.msg,
                          showCancel: !1
                        }), m.redirectTo({
                          url: "/pages/order_more_list/order_more_list"
                        }))
                      }
                    }) : m.getStorage({
                      key: "suid",
                      success: function (e) {
                        t.suid = e.data, m.request({
                          url: t.$baseurl + "doPagemycoupon",
                          data: {
                            uniacid: t.$uniacid,
                            suid: t.suid,
                            flag: 0,
                            id: t.id,
                            type: "duoshop"
                          },
                          success: function (e) {
                            t.couponlist = e.data.data
                          },
                          fail: function (e) {
                            console.log(e)
                          }
                        }), m.request({
                          url: t.$baseurl + "doPageshowPro_W",
                          data: {
                            uniacid: t.$uniacid,
                            suid: t.suid,
                            id: t.id
                          },
                          success: function (e) {
                            var a = t.yhje;
                            0 == e.data.data.kuaidi ? t.nav = 1 : t.nav = 2, t.picList = e.data.data.images, t.title = e.data.data.title, t.datas = e.data.data, t.hjjg = e.data.data.sellprice, t.dprice = e.data.data.sellprice, t.kuaidi = e.data.data.kuaidi, t.sfje = e.data.data.sellprice - a, t.shengyu = e.data.data.storage, t.pagedata = e.data.data.forms, t.formdescs = e.data.data.formdescs, m.setNavigationBarTitle({
                              title: t.title
                            }), m.setStorageSync("isShowLoading", !1), m.hideNavigationBarLoading(), m.stopPullDownRefresh(), t.zfje = t.money >= t.sfje ? t.sfje : ((100 * t.sfje - 100 * t.money) / 100).toFixed(2)
                          }
                        })
                      }
                    })
                  }
                })
              },
              jian: function () {
                var e = this,
                  a = e.jhsl,
                  t = e.dprice,
                  i = e.yhje;
                --a <= 0 && (a = 1);
                var n = 100 * t * a / 100,
                  s = (n = n.toFixed(2)) - i;
                e.jhsl = a, e.hjjg = n, e.sfje = s, e.zfje = e.money >= e.sfje ? e.sfje : ((100 * e.sfje - 100 * e.money) / 100).toFixed(2), e.jqdjg = "请选择", e.yhqid = 0
              },
              jia: function () {
                var e = this,
                  a = e.jhsl,
                  t = e.my_num,
                  i = e.xg_num,
                  n = e.shengyu,
                  s = e.dprice,
                  o = e.yhje;
                n < ++a && -1 != n && (a--, m.showModal({
                  title: "提醒",
                  content: "库存量不足！",
                  showCancel: !1
                })), i < a + t && 0 != i && (1 == a ? a = 1 : a -= 1, m.showModal({
                  title: "提醒",
                  content: "该商品为限购产品，您总购买数已超过限额！",
                  showCancel: !1
                }));
                var d = 100 * s * a / 100,
                  r = (d = d.toFixed(2)) - o;
                e.jhsl = a, e.hjjg = d, e.sfje = r, e.zfje = e.money >= e.sfje ? e.sfje : ((100 * e.sfje - 100 * e.money) / 100).toFixed(2), e.jqdjg = "请选择", e.yhqid = 0
              },
              showModal: function () {
                this.showModalStatus = !0;
                var e = m.createAnimation({
                  duration: 200,
                  timingFunction: "linear",
                  delay: 0
                });
                (this.animation = e).translateY(300).step(), this.animationData = e.export(), setTimeout(function () {
                  e.translateY(0).step(), this.animationData = e.export()
                }.bind(this), 200)
              },
              hideModal: function () {
                this.showModalStatus = !1;
                var e = m.createAnimation({
                  duration: 200,
                  timingFunction: "linear",
                  delay: 0
                });
                (this.animation = e).translateY(300).step(), this.animationData = e.export(), setTimeout(function () {
                  e.translateY(0).step(), this.animationData = e.export()
                }.bind(this), 200)
              },
              makePhoneCallC: function (e) {
                var a = e.currentTarget.dataset.tel;
                m.makePhoneCall({
                  phoneNumber: a
                })
              },
              qxyh: function () {
                var e = this,
                  a = e.jqdjg;
                "请选择" == a && (a = 0);
                var t = (100 * e.sfje + 100 * a) / 100;
                e.hideModal(), e.jqdjg = 0, e.yhqid = 0, e.sfje = t, e.zfje = e.money >= e.sfje ? e.sfje : ((100 * e.sfje - 100 * e.money) / 100).toFixed(2), e.jqdjg = "请选择"
              },
              getmoney: function (e) {
                var a = this,
                  t = e.currentTarget.id,
                  i = e.currentTarget.dataset.index,
                  n = i.pay_money,
                  s = a.hjjg;
                if (1 * s < 1 * n) m.showModal({
                  title: "提示",
                  content: "价格未满" + n + "元，不可使用该优惠券！",
                  showCancel: !1
                });
                else {
                  var o = parseFloat(((100 * s - 100 * t) / 100).toPrecision(12));
                  o < 0 && (o = 0), a.jqdjg = t, a.yhqid = i.id, a.sfje = o, a.zfje = a.money >= a.sfje ? a.sfje : (a.sfje - a.money).toFixed(2), a.hideModal()
                }
              },
              nav_f: function (e) {
                var a = parseInt(e.detail.value);
                this.nav = a
              },
              userNameInput: function (e) {
                this.pro_name = e.detail.value
              },
              userTelInput: function (e) {
                this.pro_tel = e.detail.value
              },
              userAddInput: function (e) {
                this.pro_address = e.detail.value
              },
              userTextInput: function (e) {
                this.pro_txt = e.detail.value
              },
              save: function (e) {
                var a = this;
                if (2 == this.is_submit) return !1;
                var t = a.pro_name,
                  i = a.pro_tel,
                  n = a.pro_address,
                  s = (a.pro_txt, a.nav),
                  o = e.detail.formId;
                if (a.form_id = o, !t && 2 != s) return r = !1, m.showModal({
                  title: "提醒",
                  content: "姓名为必填！",
                  showCancel: !1
                }), !1;
                if (!i && 2 != s) return r = !1, m.showModal({
                  title: "提醒",
                  content: "手机号为必填！",
                  showCancel: !1
                }), !1;
                if (!/^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/.test(i) && 2 != s) return m.showModal({
                  title: "提醒",
                  content: "请输入有效的手机号码！",
                  showCancel: !1
                }), !1;
                if (!n && 2 != s) return r = !1, m.showModal({
                  title: "提醒",
                  content: "地址为必填！",
                  showCancel: !1
                }), !1;
                if (0 < a.datas.formset) {
                  (a = this).datas;
                  for (var d = a.id, r = !0, u = a.pagedata, c = 0; c < u.length; c++)
                    if (1 == u[c].ismust)
                      if (5 == u[c].type) {
                        if ("" == u[c].z_val) return r = !1, m.showModal({
                          title: "提醒",
                          content: u[c].name + "为必填项！",
                          showCancel: !1
                        }), !1
                      } else {
                        if ("" == u[c].val) return r = !1, m.showModal({
                          title: "提醒",
                          content: u[c].name + "为必填项！",
                          showCancel: !1
                        }), !1;
                        if (0 == u[c].type && 1 == u[c].tp_text[0].yval) {
                          if (!/^1[3456789]{1}\d{9}$/.test(u[c].val)) return m.showModal({
                            title: "提醒",
                            content: "请您输入正确的手机号码",
                            showCancel: !1
                          }), !1
                        } else if (0 == u[c].type && 7 == u[c].tp_text[0].yval) {
                          if (!/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test(u[c].val)) return m.showModal({
                            title: "提醒",
                            content: "请您输入正确的身份证号",
                            showCancel: !1
                          }), !1
                        }
                      } if (r) {
                    this.is_submit = 2;
                    var l = m.getStorageSync("openid");
                    m.request({
                      url: a.$baseurl + "doPageFormval",
                      data: {
                        id: d,
                        pagedata: JSON.stringify(u),
                        types: "duoShop",
                        uniacid: a.$uniacid,
                        fid: a.fid ? a.fid : 0,
                        openid: l,
                        suid: m.getStorageSync("suid"),
                        source: m.getStorageSync("source")
                      },
                      cachetime: "30",
                      success: function (e) {
                        a.formid = e.data.data.id, m.showModal({
                          title: "提示",
                          content: e.data.data.con,
                          showCancel: !1,
                          success: function (e) {
                            a.save_order()
                          }
                        })
                      }
                    })
                  }
                } else a.save_order()
              },
              save_order: function (e) {
                var i = this,
                  a = i.jhsl,
                  t = i.shengyu;
                if (t < a && -1 != t) return a--, m.showModal({
                  title: "提醒",
                  content: "库存量不足！",
                  showCancel: !1
                }), !1;
                var n = i.sfje,
                  s = m.getStorageSync("openid"),
                  o = m.getStorageSync("suid"),
                  d = m.getStorageSync("source"),
                  r = i.jhsl,
                  u = i.dprice,
                  c = i.yhje,
                  l = i.id,
                  h = i.order,
                  f = i.pro_name,
                  g = i.pro_tel,
                  p = i.pro_address,
                  y = i.pro_txt,
                  _ = (i.id, i.yhqid);
                i.nav, this.is_submit = 2, m.request({
                  url: i.$baseurl + "doPageDingd_W",
                  data: {
                    suid: o,
                    source: d,
                    uniacid: i.$uniacid,
                    openid: s,
                    id: l,
                    price: u,
                    count: r,
                    youhui: c,
                    zhifu: n,
                    order: h,
                    pro_name: f,
                    pro_tel: g,
                    pro_address: p,
                    pro_txt: y,
                    yhqid: _,
                    nav: i.nav,
                    formid: i.formid
                  },
                  success: function (e) {
                    var a = e.data.data.order_id;
                    if (0 == e.data.data.success && 0 == e.data.data.syl) return m.showModal({
                      title: "提醒",
                      content: "很遗憾！商品售完了！",
                      showCancel: !1,
                      success: function () {
                        m.reLaunch({
                          url: "../goods_detail/goods_detail?id=" + l
                        })
                      }
                    }), !1;
                    if (0 == e.data.data.success && 0 < e.data.data.syl) return m.showModal({
                      title: "提醒",
                      content: "很遗憾！商品只剩下" + e.data.data.syl + "个",
                      showCancel: !1,
                      success: function () {
                        m.reLaunch({
                          url: "../goods_detail/goods_detail?id=" + l
                        })
                      }
                    }), !1;
                    if (1 == e.data.data.success)
                      if (i.order_id = e.data.data.order_id, n <= i.money) m.showModal({
                        title: "提醒",
                        content: "您将使用余额支付" + n + "元",
                        success: function (e) {
                          e.confirm ? m.request({
                            url: i.$baseurl + "doPageyuzhifu_w",
                            data: {
                              uniacid: i.$uniacid,
                              suid: o,
                              openid: s,
                              source: d,
                              money: n,
                              order_id: a,
                              form_id: i.form_id
                            },
                            success: function (e) {
                              1 == e.data.data && m.showToast({
                                icon: "success",
                                title: "支付成功！",
                                duration: 3e3,
                                success: function (e) {
                                  m.redirectTo({
                                    url: "/pages/order_more_list/order_more_list"
                                  })
                                }
                              })
                            },
                            fail: function (e) {
                              m.redirectTo({
                                url: "/pages/order_more_list/order_more_list"
                              })
                            }
                          }) : e.cancel && m.redirectTo({
                            url: "/pages/order_more_list/order_more_list"
                          })
                        }
                      });
                      else {
                        var t;
                        t = 0 < i.money ? (100 * n - 100 * i.money) / 100 : n, i.pay1(t, a)
                      }
                  }
                })
              },
              pay1: function (a, t) {
                var i = this,
                  n = m.getStorageSync("openid"),
                  s = m.getStorageSync("suid"),
                  o = m.getStorageSync("source");
                m.showModal({
                  title: "提醒",
                  content: "您将微信支付" + a + "元",
                  success: function (e) {
                    e.confirm && m.request({
                      url: i.$baseurl + "doPagezhifu_W",
                      data: {
                        uniacid: i.$uniacid,
                        suid: s,
                        openid: n,
                        source: o,
                        money: a,
                        types: 1,
                        order_id: t,
                        form_id: i.form_id
                      },
                      success: function (e) {
                        e.data.data.order_id && m.requestPayment({
                          timeStamp: e.data.data.timeStamp,
                          nonceStr: e.data.data.nonceStr,
                          package: e.data.data.package,
                          signType: "MD5",
                          paySign: e.data.data.paySign,
                          success: function (e) {
                            m.showToast({
                              title: "支付成功",
                              icon: "success",
                              duration: 3e3,
                              success: function (e) {
                                m.redirectTo({
                                  url: "/pages/order_more_list/order_more_list"
                                })
                              },
                              fail: function (e) {
                                m.showToast({
                                  icon: "loading",
                                  title: "支付失败！",
                                  duration: 2e3
                                })
                              },
                              complete: function (e) {
                                console.log(888444), m.navigateBack({
                                  delta: 9
                                }), m.navigateTo({
                                  url: "/pages/order_more_list/order_more_list"
                                })
                              }
                            })
                          }
                        })
                      }
                    })
                  }
                })
              },
              pay2: function (e, a) {
                m.request({
                  url: this.$baseurl + "doPageAlipay",
                  data: {
                    uniacid: this.$uniacid,
                    order_id: a,
                    suid: m.getStorageSync("suid"),
                    types: "duo"
                  },
                  success: function (e) {
                    e && m.tradePay({
                      tradeNO: e.data.trade_no,
                      success: function (e) {
                        m.navigateTo({
                          url: "/pages/order_more_list/order_more_list"
                        })
                      },
                      fail: function (e) {
                        m.navigateTo({
                          url: "/pages/order_more_list/order_more_list"
                        })
                      }
                    })
                  }
                })
              },
              showpay: function () {
                var a = this;
                m.request({
                  url: this.$baseurl + "doPageGetH5payshow",
                  data: {
                    uniacid: this.$uniacid,
                    suid: m.getStorageSync("suid")
                  },
                  success: function (e) {
                    0 == e.data.data.ali && 0 == e.data.data.wx ? m.showModal({
                      title: "提示",
                      content: "请联系管理员设置支付参数",
                      showCancel: !1,
                      success: function (e) {
                        return !1
                      }
                    }) : (0 == e.data.data.ali ? (a.alipay = 0, a.pay_type = 2) : (a.alipay = 1, a.pay_type = 1), 0 == e.data.data.wx ? a.wxpay = 0 : a.wxpay = 1, a.show_pay_type = 1)
                  }
                })
              },
              changealipay: function () {
                this.pay_type = 1
              },
              changewxpay: function () {
                this.pay_type = 2
              },
              close_pay_type: function () {
                this.show_pay_type = 0
              },
              h5topay: function () {
                var e = this.pay_type;
                1 == e ? this._alih5pay(this, this.zfje, 1, this.order_id) : 2 == e && this._wxh5pay(this, this.zfje, "duo", this.order_id), this.show_pay_type = 0
              },
              againPay: function (e) {
                console.log("再次支付");
                var a = this,
                  t = e.detail.formId;
                a.form_id = t;
                var i = a.order_id;
                if (i)
                  if (0 == a.payprice) m.showModal({
                    title: "提醒",
                    content: "您将使用余额支付" + a.sfje + "元",
                    success: function (e) {
                      e.confirm ? m.request({
                        url: a.$baseurl + "doPageyuzhifu_w",
                        data: {
                          uniacid: a.$uniacid,
                          suid: m.getStorageSync("suid"),
                          openid: m.getStorageSync("openid"),
                          source: m.getStorageSync("source"),
                          money: a.sfje,
                          order_id: i,
                          form_id: a.form_id
                        },
                        success: function (e) {
                          1 == e.data.data && m.showToast({
                            icon: "success",
                            title: "支付成功！",
                            duration: 3e3,
                            success: function (e) {
                              m.redirectTo({
                                url: "/pages/order_more_list/order_more_list"
                              })
                            }
                          })
                        },
                        fail: function (e) {
                          m.redirectTo({
                            url: "/pages/order_more_list/order_more_list"
                          })
                        }
                      }) : e.cancel && m.redirectTo({
                        url: "/pages/order_more_list/order_more_list"
                      })
                    }
                  });
                  else {
                    var n;
                    n = 0 < a.money ? (100 * a.sfje - 100 * a.money) / 100 : a.sfje, a.pay1(n, i)
                  }
              },
              bindInputChange: function (e) {
                var a = e.detail.value,
                  t = e.currentTarget.dataset.index,
                  i = this.pagedata;
                i[t].val = a, this.pagedata = i
              },
              weixinadd: function () {
                var o = this;
                m.chooseAddress({
                  success: function (e) {
                    for (var a = e.provinceName + " " + e.cityName + " " + e.countyName + " " + e.detailInfo, t = e.userName, i = e.telNumber, n = o.pagedata, s = 0; s < n.length; s++) 0 == n[s].type && 2 == n[s].tp_text[0].yval && (n[s].val = t), 0 == n[s].type && 3 == n[s].tp_text[0].yval && (n[s].val = i), 0 == n[s].type && 4 == n[s].tp_text[0].yval && (n[s].val = a);
                    o.myname = t, o.mymobile = i, o.myaddress = a, o.pagedata = n
                  }
                })
              },
              getPhoneNumber: function (e) {
                var i = this,
                  a = e.detail.iv,
                  t = e.detail.encryptedData;
                "getPhoneNumber:ok" == e.detail.errMsg ? m.checkSession({
                  success: function () {
                    m.request({
                      url: i.$baseurl + "doPagejiemiNew",
                      data: {
                        uniacid: i.$uniacid,
                        newSessionKey: i.newSessionKey,
                        iv: a,
                        encryptedData: t
                      },
                      success: function (e) {
                        if (e.data.data) {
                          for (var a = i.pagedata, t = 0; t < a.length; t++) 0 == a[t].type && 5 == a[t].tp_text[0] && (a[t].val = e.data.data);
                          i.wxmobile = e.data.data, i.pagedata = a
                        } else m.showModal({
                          title: "提示",
                          content: "sessionKey已过期，请下拉刷新！"
                        })
                      },
                      fail: function (e) {
                        console.log(e)
                      }
                    })
                  },
                  fail: function () {
                    m.showModal({
                      title: "提示",
                      content: "sessionKey已过期，请下拉刷新！"
                    })
                  }
                }) : m.showModal({
                  title: "提示",
                  content: "请先授权获取您的手机号！",
                  showCancel: !1
                })
              },
              checkboxChange: function (e) {
                var a = e.detail.value,
                  t = e.currentTarget.dataset.index,
                  i = this.pagedata;
                i[t].val = a, this.pagedata = i
              },
              radioChange: function (e) {
                var a = e.detail.value,
                  t = e.currentTarget.dataset.index,
                  i = this.pagedata;
                i[t].val = a, this.pagedata = i
              },
              delimg: function (e) {
                var a = e.currentTarget.dataset.index,
                  t = e.currentTarget.dataset.id,
                  i = this.pagedata,
                  n = i[a].z_val;
                n.splice(t, 1), 0 == n.length && (n = ""), i[a].z_val = n, this.pagedata = i
              },
              choiceimg: function (e) {
                var s = this,
                  a = 0,
                  o = s.zhixin,
                  d = e.currentTarget.dataset.index,
                  r = s.pagedata,
                  t = r[d].val,
                  i = r[d].tp_text[0];
                t ? a = t.length : (a = 0, t = []);
                var n = i - a;
                s.pd_val, m.chooseImage({
                  count: n,
                  sizeType: ["original", "compressed"],
                  sourceType: ["album", "camera"],
                  success: function (e) {
                    o = !0, s.zhixin = o, m.showLoading({
                      title: "图片上传中"
                    });
                    var t = e.tempFilePaths,
                      i = 0,
                      n = t.length;
                    ! function a() {
                      m.uploadFile({
                        url: s.$baseurl + "wxupimg",
                        formData: {
                          uniacid: s.$uniacid
                        },
                        filePath: t[i],
                        name: "file",
                        success: function (e) {
                          r[d].z_val.push(e.data), s.pagedata = r, ++i < n ? a() : (o = !1, s.zhixin = o, m.hideLoading())
                        }
                      })
                    }()
                  }
                })
              },
              bindPickerChange: function (e) {
                var a = e.detail.value,
                  t = e.currentTarget.dataset.index,
                  i = this.pagedata,
                  n = i[t].tp_text[a];
                i[t].val = n, this.pagedata = i
              },
              bindDateChange: function (e) {
                var a = e.detail.value,
                  t = e.currentTarget.dataset.index,
                  i = this.pagedata;
                i[t].val = a, this.pagedata = i
              },
              bindTimeChange: function (e) {
                var a = e.detail.value,
                  t = e.currentTarget.dataset.index,
                  i = this.pagedata;
                i[t].val = a, this.pagedata = i
              },
              namexz: function (e) {
                for (var a = this, t = e.currentTarget.dataset.index, i = a.pagedata[t], n = [], s = 0; s < i.tp_text.length; s++) {
                  var o = {};
                  o.keys = i.tp_text[s].yval, o.val = 1, n.push(o)
                }
                a.ttcxs = 1, a.formindex = t, a.xx = n, a.xuanz = 0, a.lixuanz = -1, a.riqi()
              },
              riqi: function () {
                for (var e = this, a = new Date, t = new Date(a.getTime()), i = t.getFullYear() + "-" + (t.getMonth() + 1) + "-" + t.getDate(), n = e.xx, s = 0; s < n.length; s++) n[s].val = 1;
                e.xx = n, e.gettoday(i);
                for (var o = [], d = [], r = new Date, u = 0; u < 5; u++) {
                  var c = new Date(r.getTime() + 24 * u * 3600 * 1e3),
                    l = c.getFullYear(),
                    h = c.getMonth() + 1,
                    f = c.getDate(),
                    g = h + "月" + f + "日",
                    p = l + "-" + h + "-" + f;
                  o.push(g), d.push(p)
                }
                e.arrs = o, e.fallarrs = d, e.today = i
              },
              gettoday: function (e) {
                var n = this,
                  a = n.formindex,
                  s = n.xx;
                m.request({
                  url: n.$baseurl + "dopageDuzhan",
                  data: {
                    uniacid: n.$uniacid,
                    id: 0,
                    types: "showOrder",
                    days: e,
                    pagedatekey: a
                  },
                  success: function (e) {
                    for (var a = e.data.data, t = 0; t < a.length; t++) s[a[t]].val = 2;
                    var i = 0;
                    a.length == s.length && (i = 1), n.xx = s, n.isover = i
                  }
                })
              },
              goux: function (e) {
                this.lixuanz = e.currentTarget.dataset.index
              },
              xuanzd: function (e) {
                for (var a = e.currentTarget.dataset.index, t = this.fallarrs[a], i = this.xx, n = 0; n < i.length; n++) i[n].val = 1;
                this.xuanz = a, this.today = t, this.lixuanz = -1, this.xx = i, this.gettoday(t)
              },
              quxiao: function () {
                this.ttcxs = 0
              },
              qdsave: function () {
                var e = this.today,
                  a = this.xx,
                  t = this.lixuanz;
                if (-1 == t) return m.showModal({
                  title: "提现",
                  content: "请选择预约的选项",
                  showCancel: !1
                }), !1;
                var i = "已选择" + e + "，" + a[t].keys,
                  n = this.pagedata,
                  s = this.formindex;
                n[s].val = i, n[s].days = e, n[s].indexkey = s, n[s].xuanx = t, this.ttcxs = 0, this.pagedata = n
              }
            }
          };
        a.default = e
      }).call(this, t("543d").default)
    },
    d8d7: function (e, a, t) {
      var i = t("582d");
      t.n(i).a
    }
  },
  [
    ["307d", "common/runtime", "common/vendor"]
  ]
]);