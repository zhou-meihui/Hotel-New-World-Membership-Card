(global.webpackJsonp = global.webpackJsonp || []).push([
  ["pagesPluginShop/goods_detail/goods_detail"], {
    1612: function (t, e, i) {},
    "659c": function (t, a, s) {
      (function (u) {
        Object.defineProperty(a, "__esModule", {
          value: !0
        }), a.default = void 0, s("2f62");
        var t, i = (t = s("3584")) && t.__esModule ? t : {
          default: t
        };
        var d = s("f571"),
          e = {
            data: function () {
              return {
                $imgurl: this.$imgurl,
                isplay: !1,
                bg: "",
                picList: [],
                datas: "",
                sc: 0,
                nowcon: "con",
                is_comment: 0,
                comm: 0,
                commSelf: 0,
                comments: [],
                commShow: 0,
                shareShow: 0,
                shareScore: 0,
                shareNotice: 0,
                content: "",
                con2: "",
                con3: "",
                fxsid: 0,
                shareHome: 0,
                tel: "",
                suid: "",
                baseinfo: "",
                pic_video: "",
                currentSwiper: 0,
                minHeight: 220,
                heighthave: 0,
                autoplay: !0,
                shareimg: 0,
                share: 0,
                shareimg_url: "",
                system_w: 0,
                system_h: 0,
                img_w: 0,
                img_h: 0,
                dlength: 0,
                needAuth: !1,
                needBind: !1
              }
            },
            onLoad: function (t) {
              var e = this,
                i = u.getStorageSync("suid");
              this.suid = i;
              var a = t.id;
              this.id = a;
              var s = 0;
              t.fxsid && (s = t.fxsid, u.setStorageSync("fxsid", s)), this.fxsid = s;
              var n = "";
              t.tel && (n = t.tel, this.tel = n), i = u.getStorageSync("suid"), this._baseMin(this), d.getOpenid(s, function () {
                e.getShowPic(a)
              });
              var o = u.getStorageSync("systemInfo");
              this.img_w = parseInt((.65 * o.windowWidth).toFixed(0)), this.img_h = parseInt((1.875 * this.img_w).toFixed(0)), this.system_w = parseInt(o.windowWidth), this.system_h = parseInt(o.windowHeight)
            },
            onShow: function () {
              this.suid = u.getStorageSync("suid"), this._baseMin(this), this.getShowPic()
            },
            onShareAppMessage: function () {
              var t = u.getStorageSync("suid"),
                e = "/pagePluginShop/goods_detail/goods_detail?id=" + this.id + "&fxsid=" + t + "&userid=" + t;
              return {
                title: this.title,
                path: e,
                success: function (t) {}
              }
            },
            methods: {
              getShowPic: function (t) {
                var e = this;
                u.request({
                  url: e.$baseurl + "doPageshowPro_W",
                  data: {
                    uniacid: e.$uniacid,
                    id: e.id,
                    suid: e.suid
                  },
                  success: function (t) {
                    e.picList = t.data.data.images, e.dlength = e.picList.length, e.title = t.data.data.title, e.datas = t.data.data, e.pic_video = t.data.data.video, e.sc = t.data.data.collectcount, u.setNavigationBarTitle({
                      title: e.title
                    }), u.setStorageSync("isShowLoading", !1), u.hideNavigationBarLoading(), u.stopPullDownRefresh(), e.datas.descp && (e.datas.descp = e.datas.descp.replace(/\<img/gi, '<img style="width:100%;height:auto;display:block" '), e.datas.descp = (0, i.default)(e.datas.descp))
                  }
                }), setTimeout(function () {
                  ("1" == e.comm && "0" != e.commSelf || "1" == e.commSelf) && (e.comms, e.commShow = 1, u.request({
                    url: e.$baseurl + "doPagegetComment",
                    data: {
                      uniacid: e.$uniacid,
                      id: e.$id,
                      comms: e.$comms
                    },
                    success: function (t) {
                      "" != t.data && (e.comments = t.data.data, e.is_comment = 1)
                    }
                  }))
                }, 500)
              },
              collect: function (t) {
                if (!this.getSuid()) return !1;
                var i = this;
                t.currentTarget.dataset.name, u.request({
                  url: i.$baseurl + "doPageCollect",
                  data: {
                    uniacid: i.$uniacid,
                    suid: i.suid,
                    types: "shopsPro",
                    id: i.id
                  },
                  success: function (t) {
                    var e = t.data.data;
                    i.sc = "收藏成功" == e ? 1 : 0, u.showToast({
                      title: e,
                      icon: "succes",
                      duration: 1e3,
                      mask: !0
                    })
                  }
                })
              },
              cell: function () {
                this.needAuth = !1
              },
              closeAuth: function () {
                this.needAuth = !1, this.needBind = !0
              },
              closeBind: function () {
                this.needBind = !1
              },
              getSuid: function () {
                if (u.getStorageSync("suid")) return !0;
                return u.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0, !1
              },
              makePhoneCall: function (t) {
                var e = t.currentTarget.dataset.tel;
                u.makePhoneCall({
                  phoneNumber: e
                })
              },
              swiperLoad: function (a) {
                var s = this;
                u.getSystemInfo({
                  success: function (t) {
                    var e = a.detail.width / a.detail.height,
                      i = t.windowWidth / e;
                    s.heighthave || (s.minHeight = i, s.heighthave = 1)
                  }
                })
              },
              swiperChange: function (t) {
                this.autoplay = !0, this.currentSwiper = t.detail.current, this.isplay = !1, this.autoplay = this.autoplay
              },
              playvideo: function () {
                this.autoplay = !1, this.isplay = !0, this.autoplay = this.autoplay
              },
              endvideo: function () {
                this.autoplay = !0, this.isplay = !1, this.autoplay = this.autoplay
              },
              goBuy: function (t) {
                if (!this.getSuid()) return !1;
                var e = t.currentTarget.dataset.gid,
                  i = u.getStorageSync("subscribe");
                if (0 < i.length) {
                  var a = new Array;
                  "" != i[0].mid && a.push(i[0].mid), "" != i[1].mid && a.push(i[1].mid), "" != i[2].mid && a.push(i[2].mid), 0 < a.length ? u.requestSubscribeMessage({
                    tmplIds: a,
                    success: function (t) {
                      u.navigateTo({
                        url: "/pagesPluginShop/goods_buy/goods_buy?id=" + e
                      })
                    }
                  }) : u.navigateTo({
                    url: "/pagesPluginShop/goods_buy/goods_buy?id=" + e
                  })
                } else u.navigateTo({
                  url: "/pagesPluginShop/goods_buy/goods_buy?id=" + e
                })
              },
              open_share: function () {
                this.share = 1
              },
              share_close: function () {
                this.share = 0, this.shareimg = 0
              },
              h5ShareAppMessage: function () {
                var e = this,
                  t = u.getStorageSync("suid");
                u.showModal({
                  title: "长按复制链接后分享",
                  content: this.$host + "/h5/index.html?id=" + this.$uniacid + "#/pagesPluginShop/goods_detail/goods_detail?id=" + this.id + "&fxsid=" + t + "&userid=" + t,
                  showCancel: !1,
                  success: function (t) {
                    e.share = 0
                  }
                })
              },
              getShareImg: function () {
                u.showLoading({
                  title: "海报生成中"
                });
                var e = this;
                u.request({
                  url: e.$baseurl + "dopageshareewm",
                  data: {
                    uniacid: e.$uniacid,
                    suid: u.getStorageSync("suid"),
                    gid: e.id,
                    types: "shop",
                    source: u.getStorageSync("source"),
                    pageUrl: "goods_detail"
                  },
                  success: function (t) {
                    u.hideLoading(), 0 == t.data.data.error ? (e.shareimg = 1, e.shareimg_url = t.data.data.url) : u.showToast({
                      title: t.data.data.msg,
                      icon: "none"
                    })
                  }
                })
              },
              closeShare: function () {
                this.shareimg = 0
              },
              saveImg: function () {
                var e = this;
                u.getImageInfo({
                  src: e.shareimg_url,
                  success: function (t) {
                    u.saveImageToPhotosAlbum({
                      filePath: t.path,
                      success: function () {
                        u.showToast({
                          title: "保存成功！",
                          icon: "none"
                        }), e.shareimg = 0, e.share = 0
                      }
                    })
                  }
                })
              },
              aliSaveImg: function () {
                var e = this;
                u.getImageInfo({
                  src: e.shareimg_url,
                  success: function (t) {
                    my.saveImage({
                      url: t.path,
                      showActionSheet: !0,
                      success: function () {
                        my.alert({
                          title: "保存成功"
                        }), e.shareimg = 0, e.share = 0
                      }
                    })
                  }
                })
              }
            }
          };
        a.default = e
      }).call(this, s("543d").default)
    },
    8441: function (t, e, i) {
      i.r(e);
      var a = i("aa80"),
        s = i("b10b");
      for (var n in s) "default" !== n && function (t) {
        i.d(e, t, function () {
          return s[t]
        })
      }(n);
      i("c048");
      var o = i("2877"),
        u = Object(o.a)(s.default, a.a, a.b, !1, null, null, null);
      e.default = u.exports
    },
    a282: function (t, e, i) {
      (function (t) {
        function e(t) {
          return t && t.__esModule ? t : {
            default: t
          }
        }
        i("020c"), i("921b"), e(i("66fd")), t(e(i("8441")).default)
      }).call(this, i("543d").createPage)
    },
    aa80: function (t, e, i) {
      var a = function () {
          this.$createElement;
          this._self._c
        },
        s = [];
      i.d(e, "a", function () {
        return a
      }), i.d(e, "b", function () {
        return s
      })
    },
    b10b: function (t, e, i) {
      i.r(e);
      var a = i("659c"),
        s = i.n(a);
      for (var n in a) "default" !== n && function (t) {
        i.d(e, t, function () {
          return a[t]
        })
      }(n);
      e.default = s.a
    },
    c048: function (t, e, i) {
      var a = i("1612");
      i.n(a).a
    }
  },
  [
    ["a282", "common/runtime", "common/vendor"]
  ]
]);