(global.webpackJsonp = global.webpackJsonp || []).push([
  ["pagesPluginShop/goods_list/goods_list"], {
    "0e34": function (t, a, e) {
      (function (t) {
        function a(t) {
          return t && t.__esModule ? t : {
            default: t
          }
        }
        e("020c"), e("921b"), a(e("66fd")), t(a(e("44a0")).default)
      }).call(this, e("543d").createPage)
    },
    "44a0": function (t, a, e) {
      e.r(a);
      var i = e("6a8d"),
        n = e("8a06");
      for (var o in n) "default" !== o && function (t) {
        e.d(a, t, function () {
          return n[t]
        })
      }(o);
      e("f1af");
      var s = e("2877"),
        u = Object(s.a)(n.default, i.a, i.b, !1, null, null, null);
      a.default = u.exports
    },
    "5f58": function (t, a, e) {
      (function (n) {
        Object.defineProperty(a, "__esModule", {
          value: !0
        }), a.default = void 0;
        var o = e("f571"),
          t = {
            data: function () {
              return {
                cid: 0,
                $imgurl: this.$imgurl,
                cates: [],
                cname: "",
                goodsL: [],
                tel: ""
              }
            },
            onLoad: function (t) {
              var a = this;
              n.setNavigationBarTitle({
                title: "商品列表"
              }), this._baseMin(this);
              var e = 0;
              t.sid && (e = t.sid), this.sid = e;
              var i = 0;
              t.fxsid && (i = t.fxsid), t.tel && (this.tel = t.tel), o.getOpenid(i, function () {
                a.getStoreData()
              })
            },
            onPullDownRefresh: function () {
              this.cid = 0, this.getStoreData(), n.stopPullDownRefresh()
            },
            methods: {
              goDetail: function (t) {
                n.navigateTo({
                  url: "/pagesPluginShop/goods_detail/goods_detail?id=" + t.currentTarget.dataset.id + "&tel=" + this.tel
                })
              },
              searchInput: function (t) {
                this.searchKey = t.detail.value
              },
              searchR: function () {
                n.navigateTo({
                  url: "/pages/search/search?sid=" + this.sid + "&title=" + this.searchKey
                })
              },
              changeData: function (t) {
                this.cid = t.currentTarget.dataset.cid, this.getStoreData()
              },
              getStoreData: function () {
                var a = this;
                n.request({
                  url: this.$baseurl + "doPageGetStoreData",
                  data: {
                    sid: this.sid,
                    cid: this.cid
                  },
                  success: function (t) {
                    a.cates = t.data.data.cates, a.cid = t.data.data.cid, a.goodsL = t.data.data.goodsL
                  }
                })
              }
            }
          };
        a.default = t
      }).call(this, e("543d").default)
    },
    "6a8d": function (t, a, e) {
      var i = function () {
          this.$createElement;
          this._self._c
        },
        n = [];
      e.d(a, "a", function () {
        return i
      }), e.d(a, "b", function () {
        return n
      })
    },
    "8a06": function (t, a, e) {
      e.r(a);
      var i = e("5f58"),
        n = e.n(i);
      for (var o in i) "default" !== o && function (t) {
        e.d(a, t, function () {
          return i[t]
        })
      }(o);
      a.default = n.a
    },
    "8f79": function (t, a, e) {},
    f1af: function (t, a, e) {
      var i = e("8f79");
      e.n(i).a
    }
  },
  [
    ["0e34", "common/runtime", "common/vendor"]
  ]
]);