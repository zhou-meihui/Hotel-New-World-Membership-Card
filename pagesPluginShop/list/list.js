(global.webpackJsonp = global.webpackJsonp || []).push([
  ["pagesPluginShop/list/list"], {
    "0115": function (t, e, n) {
      n.r(e);
      var i = n("326e"),
        a = n.n(i);
      for (var o in i) "default" !== o && function (t) {
        n.d(e, t, function () {
          return i[t]
        })
      }(o);
      e.default = a.a
    },
    "326e": function (t, n, i) {
      (function (a) {
        Object.defineProperty(n, "__esModule", {
          value: !0
        }), n.default = void 0;
        var o = i("f571");
        var t = [
            ["", "", "", "", ""],
            ["", "", "", "", ""],
            ["", "", "", "", ""],
            ["", "", "", "", ""],
            ["", "", "", "", ""]
          ],
          e = {
            data: function () {
              return {
                $imgurl: this.$imgurl,
                page_sign: "shopsList",
                page: 1,
                morePro: !1,
                baseinfo: [],
                cid: 0,
                cate: [],
                showMenu: !1,
                subMenuDisplay: ["hidden", "hidden", "hidden", "hidden"],
                subMenuHighLight: t,
                cover: "",
                menuCss: ["ordinary", "ordinary", "ordinary", "ordinary"],
                rotateRight: ["", "", "", "", ""],
                menuContent: [{
                  title: "全部分类",
                  content: ["全部分类"]
                }, {
                  title: "综合排序",
                  content: ["综合排序", "距离最近"]
                }, {
                  title: "所有商家",
                  content: ["所有商家", "优选商家"]
                }],
                content: [],
                indexListHouse: [],
                lastIndex: null,
                times: 1,
                longitude: "",
                latitude: "",
                shopList: ""
              }
            },
            onLoad: function (t) {
              var e = this;
              a.setNavigationBarTitle({
                title: "同城优选"
              });
              var n = t.id;
              this.id = n, t.cid && (this.cid = t.cid), this._baseMin(this);
              var i = 0;
              t.fxsid && (i = t.fxsid), this.fxsid = i, o.getOpenid(i, function () {
                e.getShopList(e.cid), e.getCate(e.cid)
              }, function () {
                e.needAuth = !0
              }, function () {
                e.needBind = !0
              })
            },
            onShow: function () {
              this.getloaction()
            },
            onReachBottom: function () {
              var e = this;
              e.page++, a.request({
                url: e.$baseurl + "doPageselectShopList",
                data: {
                  uniacid: e.$uniacid,
                  option1: e.menuContent[0].title,
                  option2: e.menuContent[1].title,
                  option3: e.menuContent[2].title,
                  longitude: e.longitude,
                  latitude: e.latitude,
                  page: e.page
                },
                cachetime: "30",
                success: function (t) {
                  t && (e.shopList = e.shopList.concat(t.data.data))
                }
              })
            },
            onShareAppMessage: function () {
              return {
                title: this.cateinfo.name + "-" + this.baseinfo.name
              }
            },
            methods: {
              tapMainMenu: function (t) {
                var e = this,
                  n = t.currentTarget.dataset.index;
                console.log(n), e.lastIndex == n ? (e.times++, 2 == e.times ? e.showMenu = !1 : (e.showMenu = !0, e.content = e.menuContent[n].content, e.times = 1)) : (e.showMenu = !0, e.content = e.menuContent[n].content, e.times = 1), e.lastIndex = n
              },
              tapSubMenu: function (t) {
                var e = this,
                  n = e.menuContent,
                  i = t.currentTarget.dataset.index;
                n[e.lastIndex].title = e.content[i], e.menuContent = n, e.showMenu = !1, e.times = 2, e.page = 1, a.request({
                  url: e.$baseurl + "doPageselectShopList",
                  data: {
                    uniacid: e.$uniacid,
                    option1: e.menuContent[0].title,
                    option2: e.menuContent[1].title,
                    option3: e.menuContent[2].title,
                    longitude: a.getStorageSync("longitude"),
                    latitude: a.getStorageSync("latitude"),
                    page: 1
                  },
                  cachetime: "30",
                  success: function (t) {
                    console.log(t), e.shopList = t.data.data
                  }
                })
              },
              switchCate: function (t) {
                var e = this,
                  n = t.currentTarget.dataset.cid,
                  i = t.currentTarget.dataset.name;
                e.getShopList(n), e.nowCate = i, e.changeShow1 = "hide", e.changeShow2 = "hide", e.changeShow3 = "show"
              },
              getShopList: function (t) {
                var e = this;
                a.request({
                  url: e.$baseurl + "doPageselectShopList",
                  data: {
                    uniacid: e.$uniacid,
                    cid: e.cid,
                    option1: e.cid,
                    longitude: a.getStorageSync("longitude"),
                    latitude: a.getStorageSync("latitude")
                  },
                  success: function (t) {
                    console.log(t), e.shopList = t.data.data
                  }
                })
              },
              getCate: function (u) {
                var c = this;
                a.request({
                  url: c.$baseurl + "doPagegetcate",
                  data: {
                    uniacid: c.$uniacid,
                    cid: c.cid
                  },
                  success: function (t) {
                    for (var e = c.menuContent, n = 0; n < t.data.data.length; n++) e[0].content.push(t.data.data[n].name);
                    if (u)
                      for (var i = c.menuContent, a = t.data.data, o = 0; o < a.length; o++) a[o].id == u && (c.menuContent = i);
                    c.cate = t.data.data
                  }
                })
              },
              makePhoneCall: function (t) {
                var e = t.currentTarget.dataset.tel;
                a.makePhoneCall({
                  phoneNumber: e
                })
              },
              getloaction: function () {
                var e = this;
                a.getLocation({
                  type: "wgs84",
                  success: function (t) {
                    e.latitude = t.latitude, e.longitude = t.longitude, a.setStorageSync("latitude", t.latitude), a.setStorageSync("longitude", t.longitude)
                  }
                })
              }
            }
          };
        n.default = e
      }).call(this, i("543d").default)
    },
    5482: function (t, e, n) {},
    7078: function (t, e, n) {
      var i = function () {
          this.$createElement;
          this._self._c
        },
        a = [];
      n.d(e, "a", function () {
        return i
      }), n.d(e, "b", function () {
        return a
      })
    },
    "87e3": function (t, e, n) {
      n.r(e);
      var i = n("7078"),
        a = n("0115");
      for (var o in a) "default" !== o && function (t) {
        n.d(e, t, function () {
          return a[t]
        })
      }(o);
      n("8c4e");
      var u = n("2877"),
        c = Object(u.a)(a.default, i.a, i.b, !1, null, null, null);
      e.default = c.exports
    },
    "8c4e": function (t, e, n) {
      var i = n("5482");
      n.n(i).a
    },
    ac54: function (t, e, n) {
      (function (t) {
        function e(t) {
          return t && t.__esModule ? t : {
            default: t
          }
        }
        n("020c"), n("921b"), e(n("66fd")), t(e(n("87e3")).default)
      }).call(this, n("543d").createPage)
    }
  },
  [
    ["ac54", "common/runtime", "common/vendor"]
  ]
]);