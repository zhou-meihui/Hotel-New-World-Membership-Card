(global.webpackJsonp = global.webpackJsonp || []).push([
  ["pagesPluginShop/manage_order/manage_order"], {
    "356d": function (n, e, t) {
      var a = t("9f62");
      t.n(a).a
    },
    "4cfe": function (n, e, t) {
      (function (i) {
        Object.defineProperty(e, "__esModule", {
          value: !0
        }), e.default = void 0, t("2f62");
        var a = t("f571"),
          n = {
            data: function () {
              return {
                $imgurl: this.$imgurl,
                needAuth: !1,
                needBind: !1,
                page_signs: "/pagesPluginShop/manage_order/manage_order",
                tabs: ["全部", "待付款", "待核销", "待发货", "已发货", "已完成"],
                activeIndex: 0,
                sliderOffset: 0,
                sliderLeft: 0,
                isshow: "hide",
                nav: 10,
                flag: 10,
                type1: 10,
                kd: "",
                kuaidi: ["顺丰", "韵达", "天天", "申通", "圆通", "中通", "国通", "百世", "EMS"],
                showhx: 0,
                hxmm: "",
                order_id: "",
                id: "",
                orderlist: [],
                baseinfo: ""
              }
            },
            onLoad: function (n) {
              var e = this;
              this._baseMin(this), i.setNavigationBarTitle({
                title: "商铺订单"
              });
              var t = n.id;
              this.id = t;
              a.getOpenid(0, function () {
                e.orderList(10, 10)
              })
            },
            onPullDownRefresh: function () {
              this.orderList(this.nav, this.flag), i.stopPullDownRefresh()
            },
            methods: {
              orderList: function (n, e) {
                var t = this;
                i.request({
                  url: t.$baseurl + "dopageShanghuOrderList",
                  data: {
                    nav: n,
                    flag: e,
                    id: i.getStorageSync("mlogin"),
                    uniacid: t.$uniacid
                  },
                  success: function (n) {
                    t.orderlist = n.data.data
                  }
                })
              },
              changflag: function (n) {
                var e = this,
                  t = n.currentTarget.dataset.flag,
                  a = n.currentTarget.dataset.nav;
                null != a && null != t ? (e.type1 = a, e.flag = t) : null == a && (e.flag = t), e.nav = a, e.orderList(a, t)
              },
              gosend: function (n) {
                var e = n.currentTarget.dataset.id;
                this.isshow = "show", this.proid = e
              },
              close: function () {
                this.isshow = "hide"
              },
              bindPickerChange: function (n) {
                var e = n.detail.value;
                this.kd = this.kuaidi[e]
              },
              doSend: function (n) {
                var e = n.detail.value,
                  t = this.kd,
                  a = this;
                "" == e.kd || "" == e.kdnum ? i.showToast({
                  title: "请填写完整发货信息",
                  icon: "none"
                }) : i.request({
                  url: a.$baseurl + "dopagedoSend",
                  data: {
                    id: a.proid,
                    sid: i.getStorageSync("mlogin"),
                    kdname: t,
                    kdnum: e.kdnum,
                    uniacid: a.$uniacid
                  },
                  success: function (n) {
                    1 == n.data.data ? i.showToast({
                      title: "发货成功",
                      icon: "success"
                    }) : i.showToast({
                      title: "发货失败",
                      icon: "none"
                    }), setTimeout(function () {
                      i.redirectTo({
                        url: "/pagesPluginShop/manage_order/manage_order"
                      })
                    }, 1e3)
                  }
                })
              },
              hexiao: function (n) {
                var e = this,
                  t = n.currentTarget.dataset.id;
                i.showModal({
                  title: "提示",
                  content: "确定核销该订单？",
                  success: function (n) {
                    n.confirm ? i.request({
                      url: e.$baseurl + "dopageduoshophx",
                      data: {
                        sid: i.getStorageSync("mlogin"),
                        uniacid: e.$uniacid,
                        order_id: t
                      },
                      success: function (n) {
                        1 == n.data.data ? i.showToast({
                          title: "核销完成",
                          icon: "success",
                          duration: 2e3,
                          success: function (n) {
                            i.redirectTo({
                              url: "/pagesPluginShop/manage_order/manage_order"
                            })
                          }
                        }) : i.showModal({
                          title: "提示",
                          content: "发生未知错误, 核销失败!",
                          showCancel: !1
                        })
                      }
                    }) : n.cancel && console.log("取消")
                  }
                })
              },
              quxiao: function () {
                i.showToast({
                  title: "请至电脑端操作！",
                  icon: "none"
                })
              }
            }
          };
        e.default = n
      }).call(this, t("543d").default)
    },
    "735b": function (n, e, t) {
      (function (n) {
        function e(n) {
          return n && n.__esModule ? n : {
            default: n
          }
        }
        t("020c"), t("921b"), e(t("66fd")), n(e(t("fd0f")).default)
      }).call(this, t("543d").createPage)
    },
    "9f62": function (n, e, t) {},
    cd7c: function (n, e, t) {
      t.r(e);
      var a = t("4cfe"),
        i = t.n(a);
      for (var o in a) "default" !== o && function (n) {
        t.d(e, n, function () {
          return a[n]
        })
      }(o);
      e.default = i.a
    },
    f67f: function (n, e, t) {
      var a = function () {
          this.$createElement;
          this._self._c
        },
        i = [];
      t.d(e, "a", function () {
        return a
      }), t.d(e, "b", function () {
        return i
      })
    },
    fd0f: function (n, e, t) {
      t.r(e);
      var a = t("f67f"),
        i = t("cd7c");
      for (var o in i) "default" !== o && function (n) {
        t.d(e, n, function () {
          return i[n]
        })
      }(o);
      t("356d");
      var d = t("2877"),
        r = Object(d.a)(i.default, a.a, a.b, !1, null, null, null);
      e.default = r.exports
    }
  },
  [
    ["735b", "common/runtime", "common/vendor"]
  ]
]);