(global.webpackJsonp = global.webpackJsonp || []).push([
  ["pagesPluginShop/manage_pro/manage_pro"], {
    "0c81": function (t, e, a) {
      (function (t) {
        function e(t) {
          return t && t.__esModule ? t : {
            default: t
          }
        }
        a("020c"), a("921b"), e(a("66fd")), t(e(a("7b1c")).default)
      }).call(this, a("543d").createPage)
    },
    "13b5": function (t, e, a) {
      var i = a("eaa5");
      a.n(i).a
    },
    "6a4e": function (t, e, a) {
      var i = function () {
          this.$createElement;
          this._self._c
        },
        n = [];
      a.d(e, "a", function () {
        return i
      }), a.d(e, "b", function () {
        return n
      })
    },
    "7b1c": function (t, e, a) {
      a.r(e);
      var i = a("6a4e"),
        n = a("e1b1");
      for (var o in n) "default" !== o && function (t) {
        a.d(e, t, function () {
          return n[t]
        })
      }(o);
      a("13b5");
      var s = a("2877"),
        l = Object(s.a)(n.default, i.a, i.b, !1, null, null, null);
      e.default = l.exports
    },
    "9f92": function (t, e, a) {
      (function (n) {
        Object.defineProperty(e, "__esModule", {
          value: !0
        }), e.default = void 0, a("2f62");
        var i = a("f571"),
          t = {
            data: function () {
              return {
                $imgurl: this.$imgurl,
                needAuth: !1,
                needBind: !1,
                page_signs: "/pagesPluginShop/manage_pro/manage_pro",
                index: 0,
                thumb: "",
                thumb_ok: "",
                id: "",
                hideAdd1: 1,
                pagedata: [],
                host: "",
                video: "",
                cid: 0,
                cateinfo: [],
                catelist: "",
                proInfo: "",
                imgs: []
              }
            },
            onLoad: function (t) {
              var e = this;
              this._baseMin(this), n.setNavigationBarTitle({
                title: "商品详情"
              });
              var a = t.id ? t.id : "";
              this.id = a;
              i.getOpenid(0, function () {
                0 < a ? e._getProInfo(a) : e.getcate()
              })
            },
            methods: {
              _getProInfo: function (t) {
                var e = this;
                n.request({
                  url: e.$baseurl + "dopageshowShopPro",
                  data: {
                    id: t,
                    uniacid: e.$uniacid,
                    suid: n.getStorageSync("suid")
                  },
                  success: function (t) {
                    t.data.data.images && (e.pagedata = t.data.data.images), e.thumb_ok = t.data.data.thumb, e.thumb = t.data.data.thumb, e.proInfo = t.data.data, e.imgs = e.pagedata, e.pagedata = e.pagedata, e.cid = e.proInfo.cid, e.getcate(), console.log(e.thumb)
                  }
                })
              },
              getcate: function () {
                var i = this;
                n.request({
                  url: i.$baseurl + "doPageGetGoodsCate",
                  data: {
                    uniacid: i.$uniacid,
                    id: i.id
                  },
                  success: function (t) {
                    i.id;
                    var e = t.data.data.arr,
                      a = t.data.data.index;
                    i.cateinfo = e, i.index = a, i.catelist = t.data.data.list
                  }
                })
              },
              bindPickerChange: function (t) {
                var e = t.detail.value;
                if (0 < e) var a = e - 1;
                for (var i = this.catelist, n = 0, o = 0, s = 0; s < i.length; s++)
                  if (0 == s) a == s ? o = i[s].id : a < i[s].sub.length + 1 && (o = i[s].sub[a - 1].id);
                  else if (a < (n += i[s].sub.length + 1) + i[0].sub.length + 1)
                  for (var l = 0, c = n + i[0].sub.length + 1 - i[s].sub.length - 1; c < n + i[0].sub.length + 1; c++) c == a && (o = 0 == l ? i[s].id : i[s].sub[l - 1].id), l += 1;
                this.index = e, this.cid = o
              },
              choosethumb: function () {
                var a = this,
                  i = a.$baseurl + "wxupimg";
                n.chooseImage({
                  count: 1,
                  sizeType: ["original", "compressed"],
                  sourceType: ["album", "camera"],
                  success: function (t) {
                    n.showLoading({
                      title: "图片上传中"
                    });
                    var e = t.tempFilePaths;
                    n.uploadFile({
                      url: i,
                      formData: {
                        uniacid: a.$uniacid
                      },
                      filePath: e[0],
                      name: "file",
                      success: function (t) {
                        var e = t.data;
                        a.thumb = e, a.thumb_ok = e, a.hideAdd1 = 0, n.hideLoading()
                      }
                    })
                  }
                })
              },
              chooseZutu: function () {
                var i = this;
                n.chooseImage({
                  count: 3,
                  sizeType: ["original"],
                  sourceType: ["album", "camera"],
                  success: function (t) {
                    0 < t.tempFilePaths.length && i._uploadImg(t.tempFilePaths, 1, function (t) {
                      console.log(t);
                      var e = i.host + t,
                        a = i.pagedata;
                      a.push(e), console.log(a), 2 < i.pagedata.length ? (i.hideAdd = 0, i.imgs = i.pagedata) : (i.hideAdd = 1, i.imgs = a), i.pagedata = a, console.log(i.pagedata)
                    })
                  }
                })
              },
              _uploadImg: function (t, e, a) {
                if (2 == e) n.uploadFile({
                  url: this.$baseurl + "wxupimg",
                  formData: {
                    uniacid: this.$uniacid
                  },
                  filePath: t,
                  name: "file",
                  success: function (t) {
                    console.log("上传成功"), console.log(t), "function" == typeof a && a(t.data)
                  },
                  fail: function (t) {
                    n.showModal({
                      title: "错误提示",
                      content: "上传失败",
                      showCancel: !1
                    })
                  }
                });
                else
                  for (var i = 0; i < t.length; i++) n.uploadFile({
                    url: this.$baseurl + "wxupimg",
                    formData: {
                      uniacid: this.$uniacid
                    },
                    filePath: t[i],
                    name: "file",
                    success: function (t) {
                      console.log("上传成功"), "function" == typeof a && a(t.data)
                    },
                    fail: function (t) {
                      n.showModal({
                        title: "错误提示",
                        content: "上传失败",
                        showCancel: !1
                      })
                    }
                  })
              },
              getBLen: function (t) {
                return null == t ? 0 : ("string" != typeof t && (t += ""), t.replace(/[^\x00-\xff]/g, "01").length)
              },
              delimg: function (t) {
                var e = t.currentTarget.dataset.id,
                  a = this.pagedata;
                a.splice(e, 1), this.imgs = a, this.pagedata = a
              },
              isInt: function (t) {
                return !!/^\d+(?=\.{0,1}\d+$|$)/.test(t)
              },
              formSubmit: function (t) {
                var e = this,
                  a = t.detail.value;
                return 0 == e.cid ? (n.showModal({
                  title: "提示",
                  content: "请选择所属栏目",
                  showCancel: !1
                }), !1) : "" == a.title ? (n.showModal({
                  title: "提示",
                  content: "请输入商品标题",
                  showCancel: !1
                }), !1) : 100 < e.getBLen(a.title) ? (n.showModal({
                  title: "提示",
                  content: "商品标题最多输入50个汉字",
                  showCancel: !1
                }), !1) : "" == e.thumb_ok ? (n.showModal({
                  title: "提示",
                  content: "请上传商品缩略图",
                  showCancel: !1
                }), !1) : e.pagedata.length < 1 ? (n.showModal({
                  title: "提示",
                  content: "请至少上传一张商品组图",
                  showCancel: !1
                }), !1) : "" == a.sellprice ? (n.showModal({
                  title: "提示",
                  content: "请输入商品售价",
                  showCancel: !1
                }), !1) : e.isInt(a.sellprice) ? "" == a.storage ? (n.showModal({
                  title: "提示",
                  content: "请输入商品库存",
                  showCancel: !1
                }), !1) : (console.log(989898989), console.log(e.pagedata), console.log(JSON.stringify(e.pagedata)), void n.request({
                  url: e.$baseurl + "dopageaddPro",
                  data: {
                    title: a.title,
                    num: a.num,
                    pageview: a.pageview,
                    buy_type: 0,
                    flag: 0 == a.flag ? 1 : a.flag,
                    kuaidi: 0,
                    rsales: a.rsales,
                    sellprice: a.sellprice,
                    marketprice: a.marketprice,
                    storage: a.storage,
                    descp: a.descp,
                    images: JSON.stringify(e.pagedata),
                    thumb: e.thumb,
                    sid: n.getStorageSync("mlogin"),
                    id: e.id,
                    uniacid: e.$uniacid,
                    cid: e.cid
                  },
                  success: function (t) {
                    0 == t.data.data ? n.showModal({
                      title: "提示",
                      content: "添加失败",
                      showCancel: !1
                    }) : (n.showToast({
                      title: "添加/修改成功",
                      icon: "success"
                    }), setTimeout(function () {
                      n.redirectTo({
                        url: "/pagesPluginShop/manage_prolist/manage_prolist"
                      })
                    }, 1e3))
                  }
                })) : (n.showModal({
                  title: "提示",
                  content: "商品售价应为正数",
                  showCancel: !1
                }), !1)
              }
            }
          };
        e.default = t
      }).call(this, a("543d").default)
    },
    e1b1: function (t, e, a) {
      a.r(e);
      var i = a("9f92"),
        n = a.n(i);
      for (var o in i) "default" !== o && function (t) {
        a.d(e, t, function () {
          return i[t]
        })
      }(o);
      e.default = n.a
    },
    eaa5: function (t, e, a) {}
  },
  [
    ["0c81", "common/runtime", "common/vendor"]
  ]
]);