(global.webpackJsonp = global.webpackJsonp || []).push([
  ["pagesPluginShop/manage_prolist/manage_prolist"], {
    "1d17": function (t, n, e) {},
    3372: function (t, n, e) {
      e.r(n);
      var a = e("470d"),
        i = e("b71c");
      for (var o in i) "default" !== o && function (t) {
        e.d(n, t, function () {
          return i[t]
        })
      }(o);
      e("eec1");
      var r = e("2877"),
        u = Object(r.a)(i.default, a.a, a.b, !1, null, null, null);
      n.default = u.exports
    },
    "470d": function (t, n, e) {
      var a = function () {
          this.$createElement;
          this._self._c
        },
        i = [];
      e.d(n, "a", function () {
        return a
      }), e.d(n, "b", function () {
        return i
      })
    },
    "97af": function (t, n, e) {
      (function (t) {
        function n(t) {
          return t && t.__esModule ? t : {
            default: t
          }
        }
        e("020c"), e("921b"), n(e("66fd")), t(n(e("3372")).default)
      }).call(this, e("543d").createPage)
    },
    b71c: function (t, n, e) {
      e.r(n);
      var a = e("dd59"),
        i = e.n(a);
      for (var o in a) "default" !== o && function (t) {
        e.d(n, t, function () {
          return a[t]
        })
      }(o);
      n.default = i.a
    },
    dd59: function (t, n, i) {
      (function (a) {
        Object.defineProperty(n, "__esModule", {
          value: !0
        }), n.default = void 0, i("2f62");
        var e = i("f571"),
          t = {
            data: function () {
              return {
                $imgurl: this.$imgurl,
                needAuth: !1,
                needBind: !1,
                page_signs: "/pagesPluginShop/manage_prolist/manage_prolist",
                dataindex: [],
                prolist: [],
                flag: 1
              }
            },
            onLoad: function (t) {
              var n = this;
              this._baseMin(this), a.setNavigationBarTitle({
                title: "商品列表"
              });
              e.getOpenid(0, function () {
                n.getprolist(1)
              })
            },
            onPullDownRefresh: function () {
              this.getprolist(this.flag), a.stopPullDownRefresh()
            },
            methods: {
              getprolist: function (t) {
                var n = this;
                a.request({
                  url: n.$baseurl + "dopageprolist",
                  data: {
                    status: t,
                    suid: a.getStorageSync("suid"),
                    uniacid: n.$uniacid
                  },
                  success: function (t) {
                    n.prolist = t.data.data
                  }
                })
              },
              changflag: function (t) {
                var n = t.currentTarget.dataset.flag;
                null != n && (this.flag = n), this.getprolist(n)
              },
              proedit: function (t) {
                var n = t.currentTarget.dataset.pid;
                a.navigateTo({
                  url: "/pagesPluginShop/manage_pro/manage_pro?id=" + n
                })
              },
              prodel: function (t) {
                var n = this,
                  e = t.currentTarget.dataset.pid;
                a.showModal({
                  title: "提示",
                  content: "确定要删除这个商品吗？",
                  showCancel: !0,
                  cancelText: "取消",
                  cancelColor: "#ccc",
                  confirmText: "删除",
                  confirmColor: "#ff0000",
                  success: function (t) {
                    t.confirm && a.request({
                      url: n.$baseurl + "dopageprodel",
                      data: {
                        pid: e,
                        id: a.getStorageSync("mlogin"),
                        uniacid: n.$uniacid
                      },
                      cachetime: "30",
                      success: function (t) {
                        1 == t.data.data && a.showModal({
                          title: "提示",
                          content: "删除成功！",
                          showCancel: !1,
                          success: function (t) {
                            a.redirectTo({
                              url: "/pagesPluginShop/manage_prolist/manage_prolist"
                            })
                          }
                        })
                      }
                    })
                  }
                })
              },
              goodsDetail: function (t) {
                var n = t.currentTarget.dataset.id;
                2 == t.currentTarget.dataset.flag ? a.showToast({
                  title: "商品未上架",
                  icon: "none"
                }) : a.navigateTo({
                  url: "/pagesPluginShop/goods_detail/goods_detail?id=" + n
                })
              }
            }
          };
        n.default = t
      }).call(this, i("543d").default)
    },
    eec1: function (t, n, e) {
      var a = e("1d17");
      e.n(a).a
    }
  },
  [
    ["97af", "common/runtime", "common/vendor"]
  ]
]);