(global.webpackJsonp = global.webpackJsonp || []).push([
  ["pagesPluginShop/manage_shop/manage_shop"], {
    "5e4f": function (a, e, t) {
      (function (c) {
        function u(a, e, t) {
          return e in a ? Object.defineProperty(a, e, {
            value: t,
            enumerable: !0,
            configurable: !0,
            writable: !0
          }) : a[e] = t, a
        }
        Object.defineProperty(e, "__esModule", {
          value: !0
        }), e.default = void 0, t("2f62");
        var i = t("f571"),
          a = {
            data: function () {
              return {
                $imgurl: this.$imgurl,
                needAuth: !1,
                needBind: !1,
                page_signs: "/pagesPluginShop/manage_index/manage_index",
                dataindex: [],
                logo_ok: "",
                bg_ok: "",
                address: "",
                pagedata: "",
                cateList: [],
                cateName: "",
                proInfo: [],
                logo: "",
                bg: "",
                cate: "",
                cateId: "",
                id: ""
              }
            },
            onLoad: function (a) {
              var e = this;
              this._baseMin(this), c.setNavigationBarTitle({
                title: "商铺信息"
              });
              var t = c.getStorageSync("mlogin");
              this.id = t, i.getOpenid(0, function () {
                e._getShopInfo(t)
              })
            },
            methods: {
              _getShopInfo: function (a) {
                var u = this;
                c.request({
                  url: u.$baseurl + "dopageshopInfo",
                  data: {
                    id: a,
                    uniacid: u.$uniacid
                  },
                  success: function (a) {
                    u.logo_ok = a.data.data[1].logo, u.bg_ok = a.data.data[1].bg, u.address = a.data.data[1].address;
                    for (var e = [], t = a.data.data[1].images, i = 0; i < t.length; i++) {
                      var o = u.host + t[i];
                      e.push(o)
                    }
                    u.pagedata = e;
                    var n = a.data.data[0];
                    u.cateList = n;
                    var d = [];
                    for (i = 0; i < n.length; i++)
                      if (d.push(n[i].name), n[i].id == a.data.data[1].cid) var s = i;
                    u.proInfo = a.data.data[1], u.imgs = u.pagedata, u.logo = u.logo_ok, u.bg = u.host + u.bg_ok, u.catelist = n, u.cateName = d, u.cate = s, u.cateId = a.data.data[1].cid
                  }
                })
              },
              formSubmit: function (a) {
                var e;
                console.log(1111);
                for (var t = this, i = a.detail.value, o = [], n = t.pagedata, d = 0; d < n.length; d++) {
                  var s = n[d].replace(t.host, "");
                  o.push(s)
                }
                c.request({
                  url: t.$baseurl + "dopageeditShop",
                  data: (e = {
                    username: i.username,
                    password: i.password,
                    title: i.title,
                    descp: i.descp,
                    cid: t.cateId,
                    intro: i.intro,
                    worktime: i.worktime,
                    name: i.name,
                    tel: i.tel,
                    address: t.address,
                    latitude: t.latitude,
                    longitude: t.longitude
                  }, u(e, "title", i.title), u(e, "images", o), u(e, "logo", t.logo_ok), u(e, "bg", t.bg_ok), u(e, "sid", c.getStorageSync("mlogin")), u(e, "id", t.id), u(e, "uniacid", t.$uniacid), e),
                  success: function (a) {
                    0 == a.data.data ? c.showModal({
                      title: "提示",
                      content: "修改失败",
                      showCancel: !1
                    }) : c.redirectTo({
                      url: "/pagesPluginShop/manage_index/manage_index"
                    })
                  }
                })
              },
              chooseimage: function () {
                var t = this,
                  i = t.$baseurl + "wxupimg";
                c.chooseImage({
                  count: 1,
                  sizeType: ["original", "compressed"],
                  sourceType: ["album", "camera"],
                  success: function (a) {
                    c.showLoading({
                      title: "图片上传中"
                    });
                    var e = a.tempFilePaths;
                    c.uploadFile({
                      url: i,
                      formData: {
                        uniacid: t.$uniacid
                      },
                      filePath: e[0],
                      name: "file",
                      success: function (a) {
                        var e = a.data;
                        t.imgs = e, t.pagedata.push(e), c.hideLoading()
                      }
                    })
                  }
                })
              },
              chooselogo: function () {
                var t = this,
                  i = t.$baseurl + "wxupimg";
                c.chooseImage({
                  count: 1,
                  sizeType: ["original", "compressed"],
                  sourceType: ["album", "camera"],
                  success: function (a) {
                    c.showLoading({
                      title: "图片上传中"
                    });
                    var e = a.tempFilePaths;
                    c.uploadFile({
                      url: i,
                      formData: {
                        uniacid: t.$uniacid
                      },
                      filePath: e[0],
                      name: "file",
                      success: function (a) {
                        a.data && (t.logo_ok = a.data);
                        var e = a.data;
                        t.logo = e, t.pagedata.push(e), c.hideLoading()
                      }
                    })
                  }
                })
              },
              changePicker: function (a) {
                var e = a.detail.value,
                  t = this.cateList;
                this.cateId = t[e].id, this.cate = e
              },
              getlocation: function () {
                var e = this;
                c.chooseLocation({
                  success: function (a) {
                    e.address = a.address, e.latitude = a.latitude, e.longitude = a.longitude
                  }
                })
              }
            }
          };
        e.default = a
      }).call(this, t("543d").default)
    },
    "6d58": function (a, e, t) {
      t.r(e);
      var i = t("5e4f"),
        o = t.n(i);
      for (var n in i) "default" !== n && function (a) {
        t.d(e, a, function () {
          return i[a]
        })
      }(n);
      e.default = o.a
    },
    8110: function (a, e, t) {
      t.r(e);
      var i = t("ac24"),
        o = t("6d58");
      for (var n in o) "default" !== n && function (a) {
        t.d(e, a, function () {
          return o[a]
        })
      }(n);
      t("c865");
      var d = t("2877"),
        s = Object(d.a)(o.default, i.a, i.b, !1, null, null, null);
      e.default = s.exports
    },
    ac24: function (a, e, t) {
      var i = function () {
          this.$createElement;
          this._self._c
        },
        o = [];
      t.d(e, "a", function () {
        return i
      }), t.d(e, "b", function () {
        return o
      })
    },
    ae72: function (a, e, t) {},
    bc3b: function (a, e, t) {
      (function (a) {
        function e(a) {
          return a && a.__esModule ? a : {
            default: a
          }
        }
        t("020c"), t("921b"), e(t("66fd")), a(e(t("8110")).default)
      }).call(this, t("543d").createPage)
    },
    c865: function (a, e, t) {
      var i = t("ae72");
      t.n(i).a
    }
  },
  [
    ["bc3b", "common/runtime", "common/vendor"]
  ]
]);