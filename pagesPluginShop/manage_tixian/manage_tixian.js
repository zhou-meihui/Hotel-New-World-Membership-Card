(global.webpackJsonp = global.webpackJsonp || []).push([
  ["pagesPluginShop/manage_tixian/manage_tixian"], {
    "0695": function (n, a, t) {
      (function (n) {
        function a(n) {
          return n && n.__esModule ? n : {
            default: n
          }
        }
        t("020c"), t("921b"), a(t("66fd")), n(a(t("f686")).default)
      }).call(this, t("543d").createPage)
    },
    "0d0a": function (n, a, t) {
      (function (i) {
        Object.defineProperty(a, "__esModule", {
          value: !0
        }), a.default = void 0, t("2f62");
        var e = t("f571"),
          n = {
            data: function () {
              return {
                $imgurl: this.$imgurl,
                needAuth: !1,
                needBind: !1,
                page_signs: "/pagesPluginShop/manage_tixian/manage_tixian",
                miniNum: null,
                allMoney: null,
                tixianType: "",
                id: "",
                money: 0
              }
            },
            onLoad: function (n) {
              var a = this;
              this._baseMin(this), i.setNavigationBarTitle({
                title: "提现管理"
              });
              var t = i.getStorageSync("mlogin");
              this.id = t;
              e.getOpenid(0, function () {
                a._getMyzh(t)
              })
            },
            onShow: function () {
              this._getMyzh(this.id)
            },
            onPullDownRefresh: function () {
              this._getMyzh(this.id), i.stopPullDownRefresh()
            },
            methods: {
              _getMyzh: function (n) {
                var a = this;
                i.request({
                  url: a.$baseurl + "dopagegetMoney",
                  data: {
                    id: n,
                    uniacid: a.$uniacid
                  },
                  success: function (n) {
                    a.money = n.data.data.allMoney, a.allMoney = n.data.data.allMoney, a.miniNum = n.data.data.miniNum, a.tixianType = n.data.data.tixiantype
                  }
                })
              },
              goSzjl: function (n) {
                i.navigateTo({
                  url: "/pagesPluginShop/manage_szjl/manage_szjl?" + this.id
                })
              },
              goTxjl: function (n) {
                i.navigateTo({
                  url: "/pagesPluginShop/manage_txjl/manage_txjl?" + this.id
                })
              },
              goTx: function (n) {
                i.navigateTo({
                  url: "/pagesPluginShop/manage_gotixian/manage_gotixian?allMoney=" + this.allMoney + "&miniNum=" + this.miniNum + "&tixianType=" + this.tixianType
                })
              }
            }
          };
        a.default = n
      }).call(this, t("543d").default)
    },
    "0f36": function (n, a, t) {},
    1852: function (n, a, t) {
      var i = t("0f36");
      t.n(i).a
    },
    "907a": function (n, a, t) {
      t.r(a);
      var i = t("0d0a"),
        e = t.n(i);
      for (var o in i) "default" !== o && function (n) {
        t.d(a, n, function () {
          return i[n]
        })
      }(o);
      a.default = e.a
    },
    d4a1: function (n, a, t) {
      var i = function () {
          this.$createElement;
          this._self._c
        },
        e = [];
      t.d(a, "a", function () {
        return i
      }), t.d(a, "b", function () {
        return e
      })
    },
    f686: function (n, a, t) {
      t.r(a);
      var i = t("d4a1"),
        e = t("907a");
      for (var o in e) "default" !== o && function (n) {
        t.d(a, n, function () {
          return e[n]
        })
      }(o);
      t("1852");
      var u = t("2877"),
        l = Object(u.a)(e.default, i.a, i.b, !1, null, null, null);
      a.default = l.exports
    }
  },
  [
    ["0695", "common/runtime", "common/vendor"]
  ]
]);