(global.webpackJsonp = global.webpackJsonp || []).push([
  ["pagesPluginShop/shop/shop"], {
    "2c67": function (t, e, n) {
      n.r(e);
      var o = n("3b29"),
        i = n("4751");
      for (var a in i) "default" !== a && function (t) {
        n.d(e, t, function () {
          return i[t]
        })
      }(a);
      n("b2b2");
      var u = n("2877"),
        s = Object(u.a)(i.default, o.a, o.b, !1, null, null, null);
      e.default = s.exports
    },
    "3b29": function (t, e, n) {
      var o = function () {
          this.$createElement;
          this._self._c
        },
        i = [];
      n.d(e, "a", function () {
        return o
      }), n.d(e, "b", function () {
        return i
      })
    },
    4751: function (t, e, n) {
      n.r(e);
      var o = n("e404"),
        i = n.n(o);
      for (var a in o) "default" !== a && function (t) {
        n.d(e, t, function () {
          return o[t]
        })
      }(a);
      e.default = i.a
    },
    b2b2: function (t, e, n) {
      var o = n("bd18");
      n.n(o).a
    },
    bd18: function (t, e, n) {},
    e0fc: function (t, e, n) {
      (function (t) {
        function e(t) {
          return t && t.__esModule ? t : {
            default: t
          }
        }
        n("020c"), n("921b"), e(n("66fd")), t(e(n("2c67")).default)
      }).call(this, n("543d").createPage)
    },
    e404: function (t, o, i) {
      (function (a) {
        Object.defineProperty(o, "__esModule", {
          value: !0
        }), o.default = void 0, i("2f62");
        var t, n = (t = i("3584")) && t.__esModule ? t : {
          default: t
        };
        var u = i("f571"),
          e = {
            data: function () {
              return {
                $imgurl: this.$imgurl,
                needAuth: !1,
                needBind: !1,
                page_signs: "/pagesPluginShop/shop/shop",
                aboutinfo: "",
                baseinfo: [],
                minHeight: 220,
                lat: "",
                lon: "",
                address: "",
                content: "",
                id: 0
              }
            },
            onPullDownRefresh: function () {
              this.getAbout(this.id), a.stopPullDownRefresh()
            },
            onShow: function () {
              this.addHits()
            },
            onLoad: function (t) {
              var e = this,
                n = t.id,
                o = t.cid;
              console.log(n), console.log(o), this.id = n || o, this._baseMin(this);
              var i = 0;
              t.fxsid && (i = t.fxsid), this.fxsid = i, a.getStorageSync("suid"), u.getOpenid(i, function () {
                e.getAbout(n)
              })
            },
            methods: {
              addHits: function () {
                a.request({
                  url: this.$baseurl + "doPageAddShopHits",
                  data: {
                    sid: this.id
                  },
                  success: function (t) {}
                })
              },
              getAbout: function (t) {
                var e = this;
                a.request({
                  url: e.$baseurl + "doPageShowstore_W",
                  data: {
                    uniacid: e.$uniacid,
                    id: e.id
                  },
                  success: function (t) {
                    e.aboutinfo = t.data.data, e.lat = t.data.data.latitude, e.lon = t.data.data.longitude, e.address = t.data.data.address, a.setNavigationBarTitle({
                      title: e.aboutinfo.name
                    }), e.aboutinfo.descp && (e.aboutinfo.descp = e.aboutinfo.descp.replace(/\<img/gi, '<img style="width:100%;height:auto;display:block" '), e.aboutinfo.descp = (0, n.default)(e.aboutinfo.descp)), console.log(1111), console.log(e.aboutinfo)
                  },
                  fail: function (t) {
                    console.log(t)
                  }
                })
              },
              goshoppay: function () {
                if (console.log(1111111), !this.getSuid()) return !1;
                a.navigateTo({
                  url: "/pages/shoppay/shoppay?sid=" + this.id
                })
              },
              dianPhoneCall: function (t) {
                var e = t.currentTarget.dataset.index;
                a.makePhoneCall({
                  phoneNumber: e
                })
              },
              openMap: function () {
                a.openLocation({
                  latitude: parseFloat(this.lat),
                  longitude: parseFloat(this.lon),
                  name: this.aboutinfo.name,
                  address: this.address,
                  scale: 22
                })
              },
              imgYu: function (t) {
                var e = t.currentTarget.dataset.src,
                  n = t.currentTarget.dataset.list;
                a.previewImage({
                  current: e,
                  urls: n
                })
              },
              cell: function () {
                this.needAuth = !1
              },
              closeAuth: function () {
                this.needAuth = !1, this.needBind = !0
              },
              closeBind: function () {
                this.needBind = !1
              },
              getSuid: function () {
                if (a.getStorageSync("suid")) return !0;
                a.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0
              }
            }
          };
        o.default = e
      }).call(this, i("543d").default)
    }
  },
  [
    ["e0fc", "common/runtime", "common/vendor"]
  ]
]);