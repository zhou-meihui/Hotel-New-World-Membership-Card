(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesPluginSupply/collect/collect"], {
        "15a2": function (t, e, n) {
            n.r(e);
            var a = n("7a9c"),
                i = n.n(a);
            for (var u in a) "default" !== u && function (t) {
                n.d(e, t, function () {
                    return a[t];
                });
            }(u);
            e.default = i.a;
        },
        "238a": function (t, e, n) {},
        3018: function (t, e, n) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                n("020c"), n("921b"), e(n("66fd")), t(e(n("44cf")).default);
            }).call(this, n("543d").createPage);
        },
        "44cf": function (t, e, n) {
            n.r(e);
            var a = n("7d06"),
                i = n("15a2");
            for (var u in i) "default" !== u && function (t) {
                n.d(e, t, function () {
                    return i[t];
                });
            }(u);
            n("7c56");
            var c = n("2877"),
                o = Object(c.a)(i.default, a.a, a.b, !1, null, null, null);
            e.default = o.exports;
        },
        "7a9c": function (t, e, n) {
            (function (a) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var i = n("f571"),
                    t = {
                        data: function () {
                            return {
                                page: 1,
                                baseinfo: "",
                                needAuth: !1,
                                needBind: !1,
                                type: 1,
                                myAll: [],
                                release_audit: 0
                            };
                        },
                        onLoad: function (t) {
                            var e = this;
                            this._baseMin(this), t.fid && (this.type = t.fid);
                            var n = 0;
                            t.fxsid && (n = t.fxsid), this.fxsid = n, a.getStorageSync("suid"), i.getOpenid(n, function () {
                                e.getmycollect(1);
                            }, function () {
                                e.needAuth = !0;
                            }, function () {
                                e.needBind = !0;
                            });
                        },
                        onReachBottom: function () {
                            var e = this,
                                n = 1 * e.page + 1;
                            a.request({
                                url: e.$baseurl + "dopageGetmycollect",
                                data: {
                                    uniacid: e.$uniacid,
                                    suid: a.getStorageSync("suid"),
                                    type: e.type,
                                    page: n
                                },
                                success: function (t) {
                                    e.myAll = e.myAll.concat(t.data.all), e.page = n;
                                }
                            });
                        },
                        onPullDownRefresh: function () {
                            this.getmycollect(this.type), a.stopPullDownRefresh();
                        },
                        methods: {
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1;
                            },
                            change: function (t) {
                                var e = t.currentTarget.dataset.type;
                                this.type = e, this.getmycollect(this.type);
                            },
                            getmycollect: function (t) {
                                var e = this;
                                a.request({
                                    url: e.$baseurl + "dopageGetmycollect",
                                    data: {
                                        uniacid: e.$uniacid,
                                        suid: a.getStorageSync("suid"),
                                        type: t,
                                        page: this.page
                                    },
                                    success: function (t) {
                                        e.myAll = t.data.all, e.release_audit = t.data.release_audit;
                                    }
                                });
                            },
                            releaseInfo: function (t) {
                                var e = t.currentTarget.dataset.id,
                                    n = t.currentTarget.dataset.shenhe;
                                0 == n ? a.showToast({
                                    title: "审核中，请耐心等待",
                                    icon: "none"
                                }) : 2 == n ? a.showToast({
                                    title: "审核失败，请重新发布",
                                    icon: "none"
                                }) : 1 == n && a.navigateTo({
                                    url: "/pagesPluginSupply/page/page?rid=" + e
                                });
                            }
                        }
                    };
                e.default = t;
            }).call(this, n("543d").default);
        },
        "7c56": function (t, e, n) {
            var a = n("238a");
            n.n(a).a;
        },
        "7d06": function (t, e, n) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            n.d(e, "a", function () {
                return a;
            }), n.d(e, "b", function () {
                return i;
            });
        }
    },
    [
        ["3018", "common/runtime", "common/vendor"]
    ]
]);