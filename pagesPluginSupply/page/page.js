(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesPluginSupply/page/page"], {
        "1c54": function (t, e, n) {},
        "2bf9": function (t, e, n) {
            (function (u) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var a = n("f571"),
                    t = (getApp(), {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                page_signs: "/pagesPluginSupply/page/page",
                                baseinfo: "",
                                needAuth: !1,
                                needBind: !1,
                                article_con: "",
                                suid: "",
                                nickname: "",
                                rid: "",
                                page: 1,
                                count: 0,
                                moreComment: !1,
                                commentList: [],
                                commentCon: "",
                                repto_suid: "",
                                commentId: "",
                                alert_del: 0,
                                types: "",
                                delid: "",
                                settop: "",
                                title: ""
                            };
                        },
                        onLoad: function (t) {
                            var e = this;
                            this._baseMin(this);
                            var n = t.rid;
                            this.rid = n;
                            this.suid = u.getStorageSync("suid"), a.getOpenid(0, function () {
                                e.getcontent();
                            });
                        },
                        onPullDownRefresh: function () {
                            this.getcontent(), u.stopPullDownRefresh();
                        },
                        onShow: function () {
                            this.getcontent();
                        },
                        methods: {
                            getcontent: function () {
                                var e = this;
                                u.request({
                                    url: e.$baseurl + "doPageGetSupplyCon",
                                    data: {
                                        uniacid: e.$uniacid,
                                        rid: e.rid,
                                        suid: u.getStorageSync("suid")
                                    },
                                    success: function (t) {
                                        u.setNavigationBarTitle({
                                                title: t.data.data.title
                                            }), e.article_con = t.data.data, e.fid = t.data.data.fid, e.settop = t.data.data.settop,
                                            e.title = t.data.data.title, e.getcomment();
                                    },
                                    fail: function (t) {}
                                });
                            },
                            getcomment: function () {
                                var e = this,
                                    t = e.page,
                                    n = e.rid;
                                u.request({
                                    url: e.$baseurl + "doPageGetSupplyComment",
                                    data: {
                                        rid: n,
                                        page: t,
                                        uniacid: e.$uniacid,
                                        suid: u.getStorageSync("suid")
                                    },
                                    success: function (t) {
                                        "" != t.data.data.list ? (10 < t.data.data.count.length ? e.moreComment = !0 : e.moreComment = !1,
                                            e.count = t.data.data.count, e.commentList = t.data.data.list) : (e.count = t.data.data.count,
                                            e.moreComment = !1);
                                    }
                                });
                            },
                            toReply: function (t) {
                                var e = this;
                                console.log(t), e.nickname = t.currentTarget.dataset.nickname, e.repto_suid = t.currentTarget.dataset.uid,
                                    e.commentId = t.currentTarget.dataset.commentid;
                            },
                            getInputCon: function (t) {
                                var e = t.detail.value;
                                100 < e.length ? u.showModal({
                                    title: "提醒",
                                    content: "字数不能大于100",
                                    showCancel: !1
                                }) : this.commentCon = e;
                            },
                            commentSub: function (t) {
                                if (!this.getSuid()) return !1;
                                var e = this,
                                    n = e.commentCon;
                                if ("" == n) return u.showModal({
                                    title: "提示",
                                    content: "评论不能为空",
                                    showCancel: !1
                                }), !1;
                                if (100 < n.length) return u.showModal({
                                    title: "提示",
                                    content: "评论内容超出允许范围!",
                                    showCancel: !1
                                }), !1;
                                var a = e.rid;
                                u.request({
                                    url: e.$baseurl + "doPageSupplyCommentSub",
                                    data: {
                                        uniacid: e.$uniacid,
                                        rid: a,
                                        suid: e.suid,
                                        content: n,
                                        repto_suid: e.repto_suid,
                                        commentId: e.commentId,
                                        source: u.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        1 == t.data.data ? u.showModal({
                                            title: "提示",
                                            content: "发布成功",
                                            showCancel: !1,
                                            success: function (t) {
                                                u.redirectTo({
                                                    url: "/pagesPluginSupply/page/page?rid=" + a
                                                });
                                            }
                                        }) : u.showModal({
                                            title: "提示",
                                            content: "发布失败,请重新发布",
                                            showCancel: !1
                                        });
                                    }
                                });
                            },
                            setChange: function () {
                                u.navigateTo({
                                    url: "/pagesPluginSupply/release/release?rid=" + this.rid + "&fid=" + this.fid
                                });
                            },
                            pagedel: function (e) {
                                var n = this,
                                    a = n.fid,
                                    i = e.currentTarget.dataset.types;
                                u.showModal({
                                    title: "提示",
                                    content: "谨慎操作，删除后数据无法恢复!",
                                    cancelText: "取消删除",
                                    confirmText: "确认删除",
                                    success: function (t) {
                                        if (!t.confirm) return !1;
                                        u.request({
                                            url: n.$baseurl + "doPageSupplyPageDel",
                                            data: {
                                                uniacid: n.$uniacid,
                                                id: e.currentTarget.dataset.id,
                                                types: i
                                            },
                                            success: function (t) {
                                                1 == t.data.data ? u.showToast({
                                                    title: "删除成功",
                                                    success: function () {
                                                        setTimeout(function () {
                                                            1 == i ? u.redirectTo({
                                                                url: "/pagesPluginSupply/supply/supply?fid=" + a
                                                            }) : 2 != i && 3 != i || u.redirectTo({
                                                                url: "/pagesPluginSupply/page/page?rid=" + n.rid
                                                            });
                                                        }, 2e3);
                                                    }
                                                }) : u.showToast({
                                                    title: "删除失败"
                                                });
                                            },
                                            fail: function (t) {}
                                        });
                                    }
                                });
                            },
                            alertdel: function (t) {
                                this.alert_del = 1, this.types = t.currentTarget.dataset.types, this.delid = t.currentTarget.dataset.id;
                            },
                            hide_alert_del: function () {
                                this.alert_del = 0;
                            },
                            commentAddLikes: function (t) {
                                if (!this.getSuid()) return !1;
                                var n = this,
                                    e = t.currentTarget.dataset.type,
                                    a = t.currentTarget.dataset.commentid,
                                    i = t.currentTarget.dataset.index;
                                if (2 == e) var o = t.currentTarget.dataset.topindex;
                                var s = u.getStorageSync("openid");
                                u.request({
                                    url: n.$baseurl + "doPageCommentChangeLikes",
                                    data: {
                                        uniacid: n.$uniacid,
                                        commentType: e,
                                        commentid: a,
                                        openid: s,
                                        vs: 1
                                    },
                                    success: function (e) {
                                        var t = e.data.data.is_like;
                                        1 == t ? u.showToast({
                                            title: "点赞成功",
                                            success: function () {
                                                var t = n.commentList;
                                                t[i].likesNum = e.data.data.num, t[i].is_like = 1, n.commentList = t;
                                            }
                                        }) : 2 == t ? u.showToast({
                                            title: "取赞成功",
                                            success: function () {
                                                var t = n.commentList;
                                                console.log(t), t[i].likesNum = e.data.data.num, t[i].is_like = 2, n.commentList = t;
                                            }
                                        }) : 3 == t ? u.showToast({
                                            title: "点赞成功",
                                            success: function () {
                                                var t = n.commentList;
                                                console.log(t), t[o].reply[i].likesNum = e.data.data.num, t[o].reply[i].is_like = 1,
                                                    n.commentList = t;
                                            }
                                        }) : 4 == t && u.showToast({
                                            title: "取赞成功",
                                            success: function () {
                                                var t = n.commentList;
                                                t[o].reply[i].likesNum = e.data.data.num, t[o].reply[i].is_like = 2, n.commentList = t;
                                            }
                                        });
                                    },
                                    fail: function (t) {}
                                });
                            },
                            changeLikes: function () {
                                if (!this.getSuid()) return !1;
                                var n = this;
                                u.request({
                                    url: n.$baseurl + "doPageSupplyLikes",
                                    data: {
                                        uniacid: n.$uniacid,
                                        suid: u.getStorageSync("suid"),
                                        rid: n.rid,
                                        vs: 1,
                                        source: u.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        var e = n.article_con;
                                        1 == t.data.data.is_like ? (u.showToast({
                                            title: "点赞成功"
                                        }), e.is_like = 1) : 2 == t.data.data.is_like && (u.showToast({
                                            title: "取赞成功"
                                        }), e.is_like = 2), e.likes = t.data.data.num, n.article_con = e;
                                    }
                                });
                            },
                            changeCollection: function () {
                                if (!this.getSuid()) return !1;
                                var n = this;
                                u.request({
                                    url: n.$baseurl + "doPageSupplyCollection",
                                    data: {
                                        uniacid: n.$uniacid,
                                        suid: u.getStorageSync("suid"),
                                        rid: n.rid,
                                        vs: 1
                                    },
                                    success: function (t) {
                                        var e = n.article_con;
                                        1 == t.data.data.is_collect ? (u.showToast({
                                            title: "收藏成功"
                                        }), e.is_collect = 1) : 2 == t.data.data.is_collect && (u.showToast({
                                            title: "取收成功"
                                        }), e.is_collect = 2), e.collection = t.data.data.num, n.article_con = e;
                                    }
                                });
                            },
                            makephone: function (t) {
                                var e = t.currentTarget.dataset.tel;
                                u.makePhoneCall({
                                    phoneNumber: e
                                });
                            },
                            setStick: function () {
                                u.navigateTo({
                                    url: "/pagesPluginSupply/set_top/set_top?fid=" + this.fid + "&rid=" + this.rid + "&returnpage=1"
                                });
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1;
                            },
                            getSuid: function () {
                                if (u.getStorageSync("suid")) return !0;
                                return u.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                    !1;
                            }
                        }
                    });
                e.default = t;
            }).call(this, n("543d").default);
        },
        "382f": function (t, e, n) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            n.d(e, "a", function () {
                return a;
            }), n.d(e, "b", function () {
                return i;
            });
        },
        "53d5": function (t, e, n) {
            var a = n("1c54");
            n.n(a).a;
        },
        b0c3: function (t, e, n) {
            n.r(e);
            var a = n("382f"),
                i = n("d78b");
            for (var o in i) "default" !== o && function (t) {
                n.d(e, t, function () {
                    return i[t];
                });
            }(o);
            n("53d5");
            var s = n("2877"),
                u = Object(s.a)(i.default, a.a, a.b, !1, null, null, null);
            e.default = u.exports;
        },
        d78b: function (t, e, n) {
            n.r(e);
            var a = n("2bf9"),
                i = n.n(a);
            for (var o in a) "default" !== o && function (t) {
                n.d(e, t, function () {
                    return a[t];
                });
            }(o);
            e.default = i.a;
        },
        e953: function (t, e, n) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                n("020c"), n("921b"), e(n("66fd")), t(e(n("b0c3")).default);
            }).call(this, n("543d").createPage);
        }
    },
    [
        ["e953", "common/runtime", "common/vendor"]
    ]
]);