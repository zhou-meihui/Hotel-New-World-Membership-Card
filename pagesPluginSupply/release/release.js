(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesPluginSupply/release/release"], {
        "40c1": function (e, t, a) {
            a.r(t);
            var n = a("a353"),
                i = a.n(n);
            for (var s in n) "default" !== s && function (e) {
                a.d(t, e, function () {
                    return n[e];
                });
            }(s);
            t.default = i.a;
        },
        "51dc": function (e, t, a) {
            (function (e) {
                function t(e) {
                    return e && e.__esModule ? e : {
                        default: e
                    };
                }
                a("020c"), a("921b"), t(a("66fd")), e(t(a("e249")).default);
            }).call(this, a("543d").createPage);
        },
        "9cee": function (e, t, a) {
            var n = a("eee9");
            a.n(n).a;
        },
        a353: function (e, t, a) {
            (function (d) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var o = a("f571"),
                    e = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                release_money: 0,
                                stick_money: 0,
                                stick_days: 7,
                                release_img: [],
                                cons: "",
                                type: 1,
                                rid: 0,
                                funcAll: [{}],
                                funcTitleArr: ["供应", "求购"],
                                index: 0,
                                userMoney: 0,
                                success_rid: 0,
                                address: "",
                                tel: "",
                                stick: 2,
                                update: 0,
                                cons_len: 0,
                                baseinfo: "",
                                needAuth: !1,
                                needBind: !1,
                                orderid: "",
                                pay_type: 1,
                                show_pay_type: 0,
                                alipay: 1,
                                wxpay: 1,
                                money: 0,
                                title: "",
                                isiPhone: 0
                            };
                        },
                        onLoad: function (e) {
                            var t = this,
                                a = e.id;
                            this.id = a, this._baseMin(this), 2 == e.type && (this.index = 1, this.type = 1),
                                "ios" == d.getStorageSync("systemInfo").platform && this.isiPhone;
                            var n = 0;
                            e.fxsid && (n = e.fxsid), this.fxsid = n;
                            var i = e.fid;
                            0 < i && (this.fid = i);
                            var s = e.rid;
                            0 < s && (this.rid = s), d.getStorageSync("suid"), o.getOpenid(n, function () {
                                t.getRelease(a);
                            }, function () {
                                t.needAuth = !0;
                            }, function () {
                                t.needBind = !0;
                            });
                        },
                        methods: {
                            getRelease: function () {
                                var e = this;
                                d.setNavigationBarTitle({
                                    title: "发布信息"
                                }), e.getforumset(), 0 < e.rid && e.getReleaseInfo();
                            },
                            getInputTel: function (e) {
                                var t = e.detail.value;
                                this.telphone = t;
                            },
                            getInputTitle: function (e) {
                                var t = e.detail.value;
                                this.title = t;
                            },
                            getReleaseInfo: function () {
                                var t = this;
                                d.request({
                                    url: t.$baseurl + "doPageGetSupplyCon",
                                    data: {
                                        rid: t.rid,
                                        uniacid: t.$uniacid,
                                        types: 1
                                    },
                                    success: function (e) {
                                        t.cons = e.data.data.content, t.release_img = e.data.data.img, t.release_money = 0,
                                            t.fid = e.data.data.fid, t.type = e.data.data.fid, t.telphone = e.data.data.telphone,
                                            t.address = e.data.data.address, t.stick = e.data.data.stick, t.update = 1, t.title = e.data.data.title,
                                            t.cons_len = t.cons.length;
                                    }
                                });
                            },
                            getlocation: function () {
                                var t = this;
                                d.chooseLocation({
                                    success: function (e) {
                                        console.log(e), t.address = e.name;
                                    },
                                    fail: function () {
                                        d.getSetting({
                                            success: function (e) {
                                                e.authSetting["scope.userLocation"] || d.showModal({
                                                    title: "请授权获取当前位置",
                                                    content: "获取位置需要授权此功能",
                                                    showCancel: !1,
                                                    success: function (e) {
                                                        e.confirm && d.openSetting({
                                                            success: function (e) {
                                                                !0 === e.authSetting["scope.userLocation"] ? d.showToast({
                                                                    title: "授权成功",
                                                                    icon: "success",
                                                                    duration: 1e3
                                                                }) : d.showToast({
                                                                    title: "授权失败",
                                                                    icon: "success",
                                                                    duration: 1e3,
                                                                    success: function () {
                                                                        t.getlocation();
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            },
                                            fail: function (e) {
                                                d.showToast({
                                                    title: "调用授权窗口失败",
                                                    icon: "success",
                                                    duration: 1e3
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            bindPickerChange: function (e) {
                                console.log(e);
                                var t = e.detail.value;
                                this.funcAll, this.index = t, this.type = 1 * t + 1, console.log(this.type);
                            },
                            releasePay: function (e) {
                                var t = this;
                                if (!this.getSuid()) return !1;
                                if ("" == t.title) return d.showModal({
                                    title: "提示",
                                    showCancel: !1,
                                    content: "标题不能为空"
                                }), !1;
                                if (10 < t.title.length) return d.showModal({
                                    title: "提示",
                                    showCancel: !1,
                                    content: "标题不能超过10个字符"
                                }), !1;
                                if ("" == t.cons) return d.showModal({
                                    title: "提示",
                                    showCancel: !1,
                                    content: "发布信息不能为空"
                                }), !1;
                                if (!/^1[3456789]\d{9}$/.test(t.telphone)) return d.showModal({
                                    title: "提示",
                                    showCancel: !1,
                                    content: "手机格式不正确"
                                }), !1;
                                var a = e.detail.formId;
                                t.formId = a, t.checkcontents();
                            },
                            checkcontents: function () {
                                var t = this;
                                d.request({
                                    url: t.$baseurl + "doPageCheckContents",
                                    data: {
                                        uniacid: t.$uniacid,
                                        content: t.title + t.cons,
                                        source: d.getStorageSync("source")
                                    },
                                    success: function (e) {
                                        return 1 == e.data.data ? (d.showModal({
                                            title: "提示",
                                            content: "小程序相关信息错误，无法发布",
                                            showCancel: !1
                                        }), !1) : 2 == e.data.data ? (d.showModal({
                                            title: "提示",
                                            content: "提交内容涉嫌违法违规，请修改后再提交",
                                            showCancel: !1
                                        }), !1) : void(0 == e.data.data && t.releasePays());
                                    }
                                });
                            },
                            releasePays: function () {
                                var t = this,
                                    a = t.formId,
                                    n = t.release_money,
                                    i = t.userMoney,
                                    e = t.update;
                                0 < n && 0 == e ? d.request({
                                    url: t.$baseurl + "doPageSupplyOrder",
                                    data: {
                                        uniacid: t.$uniacid,
                                        release_money: n,
                                        suid: d.getStorageSync("suid"),
                                        formId: a
                                    },
                                    success: function (e) {
                                        0 != e.data && (t.orderid = e.data.orderid, n <= i ? d.showModal({
                                            title: "请注意",
                                            content: "您的余额为" + i + "元，本次将扣除" + n + "元",
                                            success: function (e) {
                                                e.confirm && d.request({
                                                    url: t.$baseurl + "doPageSupplyChangemymoney",
                                                    data: {
                                                        uniacid: t.$uniacid,
                                                        orderid: t.orderid,
                                                        release_money: n,
                                                        suid: d.getStorageSync("suid")
                                                    },
                                                    success: function (e) {
                                                        if (1 != e.data.data) return !1;
                                                        t.releaseSub(1);
                                                    }
                                                });
                                            }
                                        }) : (t.releaseSub(2), 0 < i ? d.showModal({
                                            title: "请注意",
                                            content: "您将余额支付" + i + "元，微信支付" + (n - i) + "元",
                                            success: function (e) {
                                                e.confirm && t._showwxpay(t, n - i, "supfabu", t.orderid, a, 1);
                                            }
                                        }) : d.showModal({
                                            title: "请注意",
                                            content: "您将微信支付" + n + "元",
                                            success: function (e) {
                                                e.confirm && t._showwxpay(t, n, "supfabu", t.orderid, a, 1);
                                            }
                                        })));
                                    },
                                    fail: function (e) {}
                                }) : t.releaseSub(1);
                            },
                            releaseSub: function (e) {
                                var a = this,
                                    t = a.cons,
                                    n = a.release_money,
                                    i = a.release_img;
                                0 == i.length && (i = ""), d.request({
                                    url: a.$baseurl + "doPageSupplyReleaseSub",
                                    data: {
                                        uniacid: a.$uniacid,
                                        fid: a.type,
                                        rid: a.rid,
                                        cons: t,
                                        release_money: n,
                                        title: a.title,
                                        release_img: JSON.stringify(i),
                                        address: a.address,
                                        telphone: a.telphone,
                                        suid: d.getStorageSync("suid"),
                                        source: d.getStorageSync("source"),
                                        haspay: e,
                                        orderid: a.orderid
                                    },
                                    success: function (e) {
                                        var t = e.data.data;
                                        0 < t ? d.showModal({
                                            title: "提示",
                                            showCancel: !1,
                                            content: "发布成功",
                                            success: function () {
                                                d.navigateBack({
                                                    delta: 1
                                                }), 1 == a.stick ? (a.fid, d.navigateBack({
                                                    delta: 1
                                                })) : a.success_rid = t;
                                            }
                                        }) : 0 == t && d.showToast({
                                            title: "发布失败"
                                        });
                                    },
                                    fail: function (e) {}
                                });
                            },
                            goReleaseLists: function () {
                                this.fid, d.navigateBack({
                                    delta: 1
                                });
                            },
                            go_set_top: function () {
                                var e = this.fid,
                                    t = this.success_rid;
                                d.redirectTo({
                                    url: "/pagesPluginSupply/set_top/set_top?fid=" + e + "&rid=" + t + "&returnpage=2"
                                });
                            },
                            delimg: function (e) {
                                console.log(e);
                                var t = e.currentTarget.dataset.index,
                                    a = this.release_img;
                                a.splice(t, 1), this.release_img = a;
                            },
                            getforumset: function () {
                                var t = this;
                                d.request({
                                    url: t.$baseurl + "doPageSupplySet",
                                    data: {
                                        uniacid: t.$uniacid,
                                        suid: d.getStorageSync("suid")
                                    },
                                    success: function (e) {
                                        t.release_money = e.data.data.release_money, t.stick_money = e.data.data.stick_money,
                                            t.userMoney = e.data.data.mymoney;
                                    },
                                    fail: function (e) {
                                        console.log(e);
                                    }
                                });
                            },
                            getcons: function (e) {
                                var t = e.detail.value,
                                    a = t.length;
                                this.cons = t, this.cons_len = a;
                            },
                            chooseImg: function () {
                                var s = this,
                                    o = s.zhixin,
                                    c = s.release_img;
                                c || (c = []);
                                var u = s.$baseurl + "wxupimg";
                                d.chooseImage({
                                    count: 9,
                                    sizeType: ["original", "compressed"],
                                    sourceType: ["album", "camera"],
                                    success: function (e) {
                                        console.log(e), o = !0, s.zhixin = o, d.showLoading({
                                            title: "图片上传中"
                                        });
                                        var t = e.tempFilePaths,
                                            n = 0,
                                            i = t.length;
                                        (function a() {
                                            d.uploadFile({
                                                url: u,
                                                formData: {
                                                    uniacid: s.$uniacid
                                                },
                                                filePath: t[n],
                                                name: "file",
                                                success: function (e) {
                                                    var t = e.data;
                                                    c.push(t), s.release_img = c, ++n < i ? a() : (o = !1, s.zhixin = o, d.hideLoading());
                                                },
                                                fail: function (e) {
                                                    console.log(e);
                                                }
                                            });
                                        })(), console.log(s.release_img);
                                    }
                                });
                            },
                            select: function () {
                                var e = this.select;
                                e = 2 == e ? 1 : 2, this.select = e;
                            },
                            bindManual: function (e) {
                                var t = e.detail.value;
                                this.stick_days = t;
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1, this.getRelease();
                            },
                            getSuid: function () {
                                if (d.getStorageSync("suid")) return !0;
                                return d.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                    !1;
                            },
                            showpay: function () {
                                this.money = this.release_money - this.userMoney;
                                var t = this;
                                d.request({
                                    url: this.$baseurl + "doPageGetH5payshow",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: d.getStorageSync("suid")
                                    },
                                    success: function (e) {
                                        0 == e.data.data.ali && 0 == e.data.data.wx ? d.showModal({
                                            title: "提示",
                                            content: "请联系管理员设置支付参数",
                                            showCancel: !1,
                                            success: function (e) {
                                                return !1;
                                            }
                                        }) : (0 == e.data.data.ali ? (t.alipay = 0, t.pay_type = 2) : (t.alipay = 1, t.pay_type = 1),
                                            0 == e.data.data.wx ? t.wxpay = 0 : t.wxpay = 1, t.show_pay_type = 1);
                                    }
                                });
                            },
                            changealipay: function () {
                                this.pay_type = 1;
                            },
                            changewxpay: function () {
                                this.pay_type = 2;
                            },
                            close_pay_type: function () {
                                this.show_pay_type = 0;
                            },
                            h5topay: function () {
                                var e = this.pay_type;
                                1 == e ? this._alih5pay(this, this.release_money - this.userMoney, 5, this.orderid) : 2 == e && this._wxh5pay(this, this.release_money - this.userMoney, "supfabu", this.orderid),
                                    this.show_pay_type = 0;
                            }
                        }
                    };
                t.default = e;
            }).call(this, a("543d").default);
        },
        cbcc: function (e, t, a) {
            var n = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            a.d(t, "a", function () {
                return n;
            }), a.d(t, "b", function () {
                return i;
            });
        },
        e249: function (e, t, a) {
            a.r(t);
            var n = a("cbcc"),
                i = a("40c1");
            for (var s in i) "default" !== s && function (e) {
                a.d(t, e, function () {
                    return i[e];
                });
            }(s);
            a("9cee");
            var o = a("2877"),
                c = Object(o.a)(i.default, n.a, n.b, !1, null, null, null);
            t.default = c.exports;
        },
        eee9: function (e, t, a) {}
    },
    [
        ["51dc", "common/runtime", "common/vendor"]
    ]
]);