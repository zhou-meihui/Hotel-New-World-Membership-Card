(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesPluginSupply/supply/supply"], {
        "0b05": function (t, e, n) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                u = [];
            n.d(e, "a", function () {
                return a;
            }), n.d(e, "b", function () {
                return u;
            });
        },
        "3a5d": function (t, e, n) {
            var a = n("95e7");
            n.n(a).a;
        },
        "95e7": function (t, e, n) {},
        "9e3f": function (t, i, l) {
            (function (u) {
                var t;

                function e(t, e, n) {
                    return e in t ? Object.defineProperty(t, e, {
                        value: n,
                        enumerable: !0,
                        configurable: !0,
                        writable: !0
                    }) : t[e] = n, t;
                }
                Object.defineProperty(i, "__esModule", {
                    value: !0
                }), i.default = void 0;
                var a = l("f571"),
                    n = (e(t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                needAuth: !1,
                                needBind: !1,
                                type: 1,
                                supplyAll: {},
                                baseinfo: [],
                                page: 1
                            };
                        },
                        onLoad: function (t) {
                            var e = this;
                            this._baseMin(this), t.fid && (this.type = t.fid);
                            var n = 0;
                            t.fxsid && (n = t.fxsid), this.fxsid = n, u.getStorageSync("suid"), a.getOpenid(n, function () {
                                e.getsupply(1);
                            });
                        },
                        onPullDownRefresh: function () {},
                        onReachBottom: function () {
                            var e = this,
                                n = 1 * e.page + 1;
                            u.request({
                                url: e.$baseurl + "dopageGetsupply",
                                data: {
                                    uniacid: e.$uniacid,
                                    suid: u.getStorageSync("suid"),
                                    type: e.type,
                                    page: n
                                },
                                success: function (t) {
                                    e.supplyAll = e.supplyAll.concat(t.data.data), e.page = n;
                                }
                            });
                        },
                        onShow: function () {
                            this.getsupply(this.type);
                        }
                    }, "onPullDownRefresh", function () {
                        this.getsupply(this.type), u.stopPullDownRefresh();
                    }), e(t, "methods", {
                        changeType: function (t) {
                            var e = t.currentTarget.dataset.type;
                            this.type = e, this.getsupply(this.type);
                        },
                        getsupply: function (t) {
                            var e = this;
                            u.request({
                                url: e.$baseurl + "dopageGetsupply",
                                data: {
                                    uniacid: e.$uniacid,
                                    suid: u.getStorageSync("suid"),
                                    type: t
                                },
                                success: function (t) {
                                    e.supplyAll = t.data.data;
                                }
                            });
                        },
                        goRelease: function () {
                            u.navigateTo({
                                url: "/pagesPluginSupply/release/release?type=" + this.type
                            });
                        },
                        gomydata: function () {
                            u.navigateTo({
                                url: "/pagesPluginSupply/collect/collect"
                            });
                        },
                        changeCollection: function (t) {
                            var n = this;
                            if (!this.getSuid()) return !1;
                            var a = t.currentTarget.dataset.index,
                                e = t.currentTarget.dataset.rid;
                            u.request({
                                url: n.$baseurl + "doPageSupplyCollection",
                                data: {
                                    uniacid: n.$uniacid,
                                    suid: u.getStorageSync("suid"),
                                    rid: e,
                                    vs: 1
                                },
                                success: function (t) {
                                    var e = n.supplyAll;
                                    1 == t.data.data.is_collect ? (u.showToast({
                                        title: "收藏成功"
                                    }), e[a].is_collect = 1) : 2 == t.data.data.is_collect && (u.showToast({
                                        title: "取收成功"
                                    }), e[a].is_collect = 2), e[a].collection = t.data.data.num, n.supplyAll = e;
                                }
                            });
                        },
                        goContent: function (t) {
                            var e = t.currentTarget.dataset.rid;
                            u.navigateTo({
                                url: "/pagesPluginSupply/page/page?rid=" + e
                            });
                        },
                        makephone: function (t) {
                            var e = t.currentTarget.dataset.tel;
                            u.makePhoneCall({
                                phoneNumber: e
                            });
                        },
                        cell: function () {
                            this.needAuth = !1;
                        },
                        closeAuth: function () {
                            this.needAuth = !1, this.needBind = !0;
                        },
                        closeBind: function () {
                            this.needBind = !1;
                        },
                        getSuid: function () {
                            if (u.getStorageSync("suid")) return !0;
                            return u.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                !1;
                        }
                    }), t);
                i.default = n;
            }).call(this, l("543d").default);
        },
        cc56: function (t, e, n) {
            n.r(e);
            var a = n("0b05"),
                u = n("f959");
            for (var i in u) "default" !== i && function (t) {
                n.d(e, t, function () {
                    return u[t];
                });
            }(i);
            n("3a5d");
            var l = n("2877"),
                o = Object(l.a)(u.default, a.a, a.b, !1, null, null, null);
            e.default = o.exports;
        },
        e2d7: function (t, e, n) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                n("020c"), n("921b"), e(n("66fd")), t(e(n("cc56")).default);
            }).call(this, n("543d").createPage);
        },
        f959: function (t, e, n) {
            n.r(e);
            var a = n("9e3f"),
                u = n.n(a);
            for (var i in a) "default" !== i && function (t) {
                n.d(e, t, function () {
                    return a[t];
                });
            }(i);
            e.default = u.a;
        }
    },
    [
        ["e2d7", "common/runtime", "common/vendor"]
    ]
]);