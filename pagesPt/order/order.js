(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesPt/order/order"], {
        "03cc": function (e, t, a) {},
        3600: function (e, t, a) {
            a.r(t);
            var i = a("c581"),
                n = a.n(i);
            for (var s in i) "default" !== s && function (e) {
                a.d(t, e, function () {
                    return i[e];
                });
            }(s);
            t.default = n.a;
        },
        "747d": function (e, t, a) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                n = [];
            a.d(t, "a", function () {
                return i;
            }), a.d(t, "b", function () {
                return n;
            });
        },
        "8b25": function (e, t, a) {
            a.r(t);
            var i = a("747d"),
                n = a("3600");
            for (var s in n) "default" !== s && function (e) {
                a.d(t, e, function () {
                    return n[e];
                });
            }(s);
            a("ab1f");
            var o = a("2877"),
                r = Object(o.a)(n.default, i.a, i.b, !1, null, null, null);
            t.default = r.exports;
        },
        a99a: function (e, t, a) {
            (function (e) {
                function t(e) {
                    return e && e.__esModule ? e : {
                        default: e
                    };
                }
                a("020c"), a("921b"), t(a("66fd")), e(t(a("8b25")).default);
            }).call(this, a("543d").createPage);
        },
        ab1f: function (e, t, a) {
            var i = a("03cc");
            a.n(i).a;
        },
        c581: function (e, t, a) {
            (function (P) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var r = a("f571"),
                    e = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                $host: this.$host,
                                needAuth: !1,
                                needBind: !1,
                                baseinfo: "",
                                title: "订单提交",
                                yhq_hidden: 0,
                                yhq: ["不使用优惠券", "满100减10", "满200减30", "满500减100"],
                                yhq_i: 0,
                                yhq_tishi: 1,
                                yhq_u: 0,
                                nav: 1,
                                jqdjg: "请选择",
                                jifen_u: 0,
                                yhqid: 0,
                                yhdq: 0,
                                sfje: 0,
                                szmoney: 0,
                                dkmoney: 0,
                                dkscore: 0,
                                mjly: "",
                                px: 0,
                                yunfei: 0,
                                yfjian: 0,
                                pd_val: [],
                                money: 0,
                                zf_money: 0,
                                isview: 0,
                                again: 0,
                                mraddress: "",
                                kuaidi: "",
                                sid: 0,
                                fxsid: "",
                                orderid: "",
                                pro_city: "",
                                jsdata: [{
                                    baseinfo: {},
                                    proinfo: {},
                                    num: 0
                                }],
                                order_info: "",
                                jsprice: "",
                                score: "",
                                showModalStatus: !1,
                                couponlist: [{
                                    coupon: {}
                                }],
                                h5pay: 0,
                                dopay: "",
                                pay_type: 1,
                                show_pay_type: 0,
                                alipay: 1,
                                wxpay: 1,
                                yhqinfo: "",
                                datas: {},
                                forminfo: [],
                                myname: "",
                                mymobile: "",
                                myaddress: "",
                                wxmobile: "",
                                ttcxs: 0,
                                xuanz: -1,
                                lixuanz: -1,
                                xx: [],
                                formindex: -1,
                                arrs: [],
                                isover: 0,
                                formdescs: "",
                                newSessionKey: "",
                                zhixin: !1,
                                formset: 0,
                                gwc: "",
                                gmnum: "",
                                addressid: "",
                                yunfei2: "",
                                yue_price: "",
                                wx_price: "",
                                animationData: "",
                                formId: "",
                                shareid: "",
                                tz_bl: "",
                                youhl: "",
                                zg_type: "",
                                newsfje: "",
                                sw: "",
                                orderinfo: "",
                                m_address: "",
                                free_package: 0,
                                ischecked: !1,
                                storeall: "",
                                storeid: "",
                                is_submit: 1,
                                start_year: 2019,
                                end_year: 2060,
                                showPay: 0,
                                choosepayf: 0,
                                shop_id: 0,
                                shop_info: "",
                                self_taking_time: "",
                                default_concat: "",
                                mymoney: 0,
                                mymoney_pay: 1,
                                suid: "",
                                zf_type: 0,
                                h5_wxpay: 0,
                                h5_alipay: 0
                            };
                        },
                        components: {
                            datetime: function () {
                                return a.e("components/datetime/datetime").then(a.bind(null, "9dfc"));
                            }
                        },
                        onLoad: function (e) {
                            var t = this,
                                a = 0;
                            e.fxsid && (a = e.fxsid), this.fxsid = a, e.again && (this.again = 1);
                            var i = e.id;
                            null != i && (this.id = i), this.refreshSessionkey();
                            var n = e.addressid,
                                s = e.orderid;
                            n && (this.addressid = n);
                            var o = 0;
                            e.shareid && (o = e.shareid, this.shareid = o), e.kuaidi && (this.kuaidi = e.kuaidi,
                                    this.nav = 1 == e.kuaidi ? 2 : 1), e.nav && (this.nav = e.nav), s && (this.orderid = s),
                                this.suid = P.getStorageSync("suid"), this._baseMin(this), r.getOpenid(a, function () {
                                    t.getorder();
                                }, function () {
                                    t.needAuth = !0;
                                }, function () {
                                    t.needBind = !0;
                                }), P.request({
                                    url: this.$baseurl + "doPagecheckFreePackage",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: this.suid
                                    },
                                    success: function (e) {
                                        t.free_package = e.data.data;
                                    }
                                });
                        },
                        onPullDownRefresh: function () {
                            P.stopPullDownRefresh();
                        },
                        methods: {
                            closeAuth: function () {
                                console.log("closeAuth"), this.needAuth = !1, this._checkBindPhone(this), this.getorder();
                            },
                            closeBind: function () {
                                console.log("closeBind"), this.needBind = !1, this.getorder();
                            },
                            clickTab: function (e) {
                                if (this.nav == e.currentTarget.dataset.current) return !1;
                                this.nav = e.currentTarget.dataset.current;
                            },
                            chooseStore: function () {
                                P.navigateTo({
                                    url: "/pages/chooseStore/chooseStore?address_id=" + this.addressid + "&stid=" + this.shop_id + "&type=pt&gid=" + this.id + "&kuaidi=" + this.kuaidi
                                });
                            },
                            concat_input: function (e) {
                                this.default_concat = e.detail.value;
                            },
                            openDatetimePicker: function (e) {
                                this.$refs.myPicker.show();
                            },
                            closeDatetimePicker: function () {
                                this.$refs.myPicker.hide();
                            },
                            handleSubmit: function (e) {
                                this.self_taking_time = "".concat(e.year, "-").concat(e.month, "-").concat(e.day, " ").concat(e.hour, ":").concat(e.minute);
                            },
                            paybox: function () {
                                var e = this.nav;
                                if (console.log("nav=" + e), 2 == e) {
                                    if (!this.shop_info) return P.showModal({
                                        title: "提示",
                                        content: "请先选择门店",
                                        showCancel: !1
                                    }), !1;
                                    if (!this.self_taking_time) return P.showModal({
                                        title: "提示",
                                        content: "请先选择预约取件时间",
                                        showCancel: !1
                                    }), !1;
                                    var t = this.default_concat;
                                    if (!t) return P.showModal({
                                        title: "提示",
                                        content: "请先输入预留电话",
                                        showCancel: !1
                                    }), !1;
                                    if (!/^1[3456789]{1}\d{9}$/.test(t)) return P.showModal({
                                        title: "提醒",
                                        content: "请您输入正确的手机号码",
                                        showCancel: !1
                                    }), !1;
                                } else {
                                    if (!this.addressid) return P.showModal({
                                        title: "提示",
                                        content: "请先选择收件地址",
                                        showCancel: !1
                                    }), !1;
                                }
                                this.mymoney < this.newsfje && (this.mymoney_pay = 2, this.pay_type = 2, this.zf_type = 1,
                                    this.choosepayf = 1), 0 == this.showPay && (this.showPay = 1);
                            },
                            payboxclose: function () {
                                1 == this.showPay && (this.showPay = 0);
                            },
                            choosepay: function (e) {
                                this.zf_type = 1 == e.currentTarget.dataset.pay_type ? 0 : 1, this.choosepayf = e.currentTarget.dataset.type;
                            },
                            refreshSessionkey: function () {
                                var t = this;
                                P.login({
                                    success: function (e) {
                                        P.request({
                                            url: t.$baseurl + "doPagegetNewSessionkey",
                                            data: {
                                                uniacid: t.$uniacid,
                                                code: e.code
                                            },
                                            success: function (e) {
                                                t.newSessionKey = e.data.data;
                                            }
                                        });
                                    }
                                });
                            },
                            add_address: function () {
                                P.navigateTo({
                                    url: "/pages/address/address?shareid=" + this.shareid + "&pid=" + this.id + "&orderid=" + this.orderid + "&kuaidi=" + this.kuaidi
                                });
                            },
                            getorder: function () {
                                this.orderid;
                                var r = this,
                                    d = r.gmnum;
                                P.getStorage({
                                    key: "jsdata",
                                    success: function (e) {
                                        for (var t = e.data, a = 0, i = 0, n = 0; n < t.length; n++) {
                                            var s = t[n].num;
                                            if (d = 1 * d + 1 * s, 1 == (i = t[n].gmorpt)) var o = t[n].proinfo.price;
                                            else o = t[n].proinfo.dprice;
                                            0 == i && 0 != r.shareid && (r.shareid = 0), a = 1 * a + 1 * o * (1 * s);
                                        }
                                        r.gmnum = d, r.jsdata = e.data, r.jsprice = Math.round(100 * a) / 100, r.sfje = a,
                                            r.px = 0, r.gwc = i, r.tuanzyh();
                                    }
                                });
                            },
                            tuanzyh: function () {
                                var d = this,
                                    e = d.jsdata[0].pvid;
                                P.request({
                                    url: d.$baseurl + "doPagepttuanzyh",
                                    data: {
                                        uniacid: d.$uniacid,
                                        id: e
                                    },
                                    success: function (e) {
                                        var t = e.data.data.tz_yh,
                                            a = d.gwc,
                                            i = d.sfje,
                                            n = d.shareid,
                                            s = i,
                                            o = 0;
                                        if (1 == a && 0 == n && (i = (i * t / 10).toFixed(2), o = Math.round(100 * (1 * s - 1 * i)) / 100,
                                                d.tz_bl = t, d.sfje = i, d.youhl = o), d.nav, 1 == d.nav) {
                                            var r = d.addressid;
                                            r ? d.getmraddresszd(r) : d.getmraddress();
                                        } else d.myContactInfo();
                                        d.forminfo = e.data.forms, d.formset = e.data.data.formset, d.formdescs = e.data.formdescs;
                                    }
                                });
                            },
                            myContactInfo: function () {
                                var t = this;
                                P.request({
                                    url: t.$host + "/api/MainWxapp/getMyContactInfo",
                                    data: {
                                        uniacid: t.$uniacid,
                                        suid: t.suid,
                                        shop_id: t.shop_id,
                                        type: "pt"
                                    },
                                    success: function (e) {
                                        t.shop_info = e.data.data.shop_info, t.getmyinfo();
                                    }
                                });
                            },
                            getmraddresszd: function (e) {
                                var a = this;
                                P.getStorageSync("openid"), P.request({
                                    url: a.$baseurl + "dopagegetmraddresszd",
                                    data: {
                                        uniacid: a.$uniacid,
                                        suid: a.suid,
                                        id: e
                                    },
                                    success: function (e) {
                                        var t = e.data.data;
                                        "" != t ? (a.mraddress = t, a.pro_city = t.pro_city) : a.mraddress = "", a.getmyinfo();
                                    }
                                });
                            },
                            getmraddress: function () {
                                var a = this;
                                P.request({
                                    url: a.$baseurl + "dopagegetmraddress",
                                    data: {
                                        uniacid: a.$uniacid,
                                        suid: a.suid
                                    },
                                    success: function (e) {
                                        var t = e.data.data;
                                        t ? (a.mraddress = t, a.pro_city = t.pro_city, a.addressid = t.id) : a.mraddress = "",
                                            a.getmyinfo();
                                    }
                                });
                            },
                            getmyinfo: function () {
                                var n = this,
                                    s = (n.jsdata, n.sfje),
                                    e = n.suid;
                                P.request({
                                    url: n.$baseurl + "doPageMymoneyD",
                                    data: {
                                        uniacid: n.$uniacid,
                                        suid: e
                                    },
                                    success: function (e) {
                                        n.mymoney = parseFloat(e.data.data.money);
                                        var t = n.pro_city,
                                            a = n.free_package;
                                        "" != t && 0 == a ? P.request({
                                            url: n.$baseurl + "dopageyunfeigetnew",
                                            data: {
                                                uniacid: n.$uniacid,
                                                id: n.id,
                                                type: "pt",
                                                hjjg: n.jsprice,
                                                num: n.gmnum,
                                                pro_city: t
                                            },
                                            success: function (e) {
                                                var t, a = e.data.data,
                                                    i = n.sfje;
                                                t = "" != a.byou && i >= 1 * a.byou ? 0 : a.yfei, n.yunfei = t, n.newsfje = s + t;
                                            }
                                        }) : (n.yunfei = 0, n.newsfje = s);
                                    }
                                });
                            },
                            makePhoneCallC: function (e) {
                                var t = e.currentTarget.dataset.tel;
                                P.makePhoneCall({
                                    phoneNumber: t
                                });
                            },
                            showModal: function () {
                                this.showModalStatus = !0;
                                var e = P.createAnimation({
                                    duration: 200,
                                    timingFunction: "linear",
                                    delay: 0
                                });
                                (this.animation = e).translateY(300).step(), this.animationData = e.export(), setTimeout(function () {
                                    e.translateY(0).step(), this.animationData = e.export();
                                }.bind(this), 200);
                            },
                            hideModal: function () {
                                var e = P.createAnimation({
                                    duration: 200,
                                    timingFunction: "linear",
                                    delay: 0
                                });
                                (this.animation = e).translateY(300).step(), this.animationData = e.export(), setTimeout(function () {
                                    e.translateY(0).step(), this.animationData = e.export(), this.showModalStatus = !1;
                                }.bind(this), 200);
                            },
                            submit: r.throttle(function (e) {
                                if (console.log(e), 2 == this.is_submit) return !1;
                                var t = e.detail.formId;
                                this.formId = t, 0 < this.formset ? this.formSubmit() : this.dosubmit();
                            }, 2e3),
                            mjlys: function (e) {
                                this.mjly = e.detail.value;
                            },
                            dosubmit: function (e) {
                                for (var i = this, t = i.mjly, n = this.formId, a = P.getStorageSync("openid"), s = i.jsdata, o = s[0].pvid, r = [], d = 0; d < s.length; d++) {
                                    var c = {};
                                    c.baseinfo = s[d].baseinfo2.id, c.proinfo = s[d].proinfo.id, c.num = s[d].num, c.pvid = s[d].pvid,
                                        c.one_bili = s[d].one_bili, c.two_bili = s[d].two_bili, c.three_bili = s[d].three_bili,
                                        c.id = s[d].id, c.proval_ggz = s[d].proinfo.ggz, c.proval_price = s[d].proinfo.price,
                                        c.proval_dprice = s[d].proinfo.dprice, r.push(c);
                                }
                                var u = i.yhqid,
                                    l = i.newsfje,
                                    f = (i.money, i.nav),
                                    h = i.yunfei,
                                    p = i.yfjian;
                                h = h || 0;
                                var y = i.shareid,
                                    m = i.dkscore,
                                    g = (i.dkmoney, i.gwc);
                                console.log("gwc=" + g), h -= p;
                                var v = i.mraddress;
                                if (t = i.mjly, (null == v || 0 == v) && 1 == f && 0 == i.again) return P.showModal({
                                    title: "提醒",
                                    content: "请先选择/设置地址！",
                                    showCancel: !0,
                                    success: function (e) {
                                        if (!e.confirm) return !1;
                                        P.navigateTo({
                                            url: "/pages/address/address?shareid=" + i.shareid + "&pid=" + i.id + "&orderid=" + i.orderid
                                        });
                                    }
                                }), !1;
                                if (null != v && 0 != v || 2 != f) _ = v.id;
                                else var _ = "";
                                var w = i.px;
                                if (this.is_submit = 2, 0 == w) {
                                    var x = "",
                                        b = "",
                                        k = "",
                                        z = "";
                                    if ("" == i.storeall) x = i.baseinfo.name, b = i.baseinfo.tel, k = i.baseinfo.address;
                                    else {
                                        var $ = i.storeid,
                                            M = i.storeall;
                                        for (d = 0; d < M.length; d++)
                                            if ($ == M[d].id) {
                                                x = M[d].title, b = M[d].tel, k = M[d].province + M[d].city + M[d].country, z = M[d].times;
                                                break;
                                            }
                                    }
                                    P.request({
                                        url: i.$baseurl + "dopageptsetorder",
                                        data: {
                                            uniacid: i.$uniacid,
                                            openid: a,
                                            jsdata: JSON.stringify(r),
                                            couponid: u,
                                            price: l,
                                            dkscore: m,
                                            address: _,
                                            mjly: t,
                                            nav: f,
                                            gwc: g,
                                            shareid: y,
                                            pvid: o,
                                            suid: i.suid,
                                            yue_price: i.yue_price,
                                            wx_price: i.wx_price,
                                            source: P.getStorageSync("source"),
                                            yunfei: h,
                                            formId: n,
                                            fid: i.cid,
                                            store_name: x,
                                            store_tel: b,
                                            store_address: k,
                                            store_hours: z,
                                            zf_type: i.zf_type,
                                            shop_id: i.shop_id,
                                            self_taking_time: i.self_taking_time,
                                            default_concat: i.default_concat
                                        },
                                        success: function (e) {
                                            if (2 == e.data) P.showModal({
                                                title: "提醒",
                                                content: "你是拼团发起人，不能参团",
                                                showCancel: !1,
                                                success: function () {
                                                    P.navigateBack({
                                                        delta: 1
                                                    });
                                                }
                                            });
                                            else if (3 == e.data) P.showModal({
                                                title: "提醒",
                                                content: "你已参团，不能再次参团",
                                                showCancel: !1,
                                                success: function () {
                                                    P.navigateBack({
                                                        delta: 1
                                                    });
                                                }
                                            });
                                            else if (5 == e.data) P.showModal({
                                                title: "提醒",
                                                content: "此商品您有拼团订单未成功，无法再次开团",
                                                showCancel: !1,
                                                success: function () {
                                                    P.navigateBack({
                                                        delta: 1
                                                    });
                                                }
                                            });
                                            else if (4 == e.data) P.showModal({
                                                title: "提醒",
                                                content: "该团已满，无法参团",
                                                showCancel: !1,
                                                success: function () {
                                                    P.navigateBack({
                                                        delta: 1
                                                    });
                                                }
                                            });
                                            else if (6 == e.data) P.showModal({
                                                title: "提醒",
                                                content: "商品已经下架",
                                                showCancel: !1,
                                                success: function () {
                                                    P.redirectTo({
                                                        url: "/pagesPt/index/index"
                                                    });
                                                }
                                            });
                                            else {
                                                var t = e.data.order_id;
                                                if (i.orderid = t, 0 == i.zf_type) i.pay1(t);
                                                else {
                                                    if (console.log("formId" + n), 1 == g) var a = "/pagesPt/pt/pt?shareid=" + t;
                                                    else a = "/pagesPt/orderlist/orderlist";
                                                    i._showwxpay(i, l, "pt", t, n, a);
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    var j = i.orderid;
                                    g = i.gwc, P.request({
                                        url: i.$baseurl + "doPageptduoorderchangegg",
                                        data: {
                                            uniacid: i.$uniacid,
                                            orderid: j,
                                            shareid: y,
                                            couponid: u,
                                            price: l,
                                            dkscore: m,
                                            address: v.id,
                                            mjly: t,
                                            nav: f,
                                            gwc: g,
                                            openid: a,
                                            pvid: o,
                                            suid: i.suid,
                                            yunfei: h,
                                            formId: n
                                        },
                                        success: function (e) {
                                            5 == e.data ? P.showModal({
                                                title: "提醒",
                                                content: "此商品您有拼团订单未成功，无法再次开团",
                                                showCancel: !1,
                                                success: function () {
                                                    P.navigateBack({
                                                        delta: 1
                                                    });
                                                }
                                            }) : 6 == e.data ? P.showModal({
                                                title: "提醒",
                                                content: "您已参加此团，无法再次参团",
                                                showCancel: !1,
                                                success: function () {
                                                    P.navigateBack({
                                                        delta: 1
                                                    });
                                                }
                                            }) : 0 == i.zg_type ? i.pay1(j) : i._showwxpay(i, i.newsfje, "pt", j, n, "/pagesPt/orderlist/orderlist");
                                        }
                                    });
                                }
                            },
                            bindInputChange: function (e) {
                                var t = e.detail.value,
                                    a = e.currentTarget.dataset.index,
                                    i = this.forminfo;
                                i[a].val = t, this.forminfo = i;
                            },
                            weixinadd: function () {
                                var o = this;
                                P.chooseAddress({
                                    success: function (e) {
                                        for (var t = e.provinceName + " " + e.cityName + " " + e.countyName + " " + e.detailInfo, a = e.userName, i = e.telNumber, n = o.forminfo, s = 0; s < n.length; s++) 0 == n[s].type && 2 == n[s].tp_text[0].yval && (n[s].val = a),
                                            0 == n[s].type && 3 == n[s].tp_text[0].yval && (n[s].val = i), 0 == n[s].type && 4 == n[s].tp_text[0].yval && (n[s].val = t);
                                        o.myname = a, o.mymobile = i, o.myaddress = t, o.forminfo = n;
                                    }
                                });
                            },
                            getPhoneNumber: function (e) {
                                var i = this,
                                    t = e.detail.iv,
                                    a = e.detail.encryptedData;
                                "getPhoneNumber:ok" == e.detail.errMsg ? P.checkSession({
                                    success: function () {
                                        P.request({
                                            url: i.$baseurl + "doPagejiemiNew",
                                            data: {
                                                uniacid: i.$uniacid,
                                                newSessionKey: i.newSessionKey,
                                                iv: t,
                                                encryptedData: a
                                            },
                                            success: function (e) {
                                                if (e.data.data) {
                                                    for (var t = i.forminfo, a = 0; a < t.length; a++) 0 == t[a].type && 5 == t[a].tp_text[0] && (t[a].val = e.data.data);
                                                    i.wxmobile = e.data.data, i.forminfo = t;
                                                } else P.showModal({
                                                    title: "提示",
                                                    content: "sessionKey已过期，请下拉刷新！"
                                                });
                                            },
                                            fail: function (e) {
                                                console.log(e);
                                            }
                                        });
                                    },
                                    fail: function () {
                                        P.showModal({
                                            title: "提示",
                                            content: "sessionKey已过期，请下拉刷新！"
                                        });
                                    }
                                }) : P.showModal({
                                    title: "提示",
                                    content: "请先授权获取您的手机号！",
                                    showCancel: !1
                                });
                            },
                            checkboxChange: function (e) {
                                var t = e.detail.value,
                                    a = e.currentTarget.dataset.index,
                                    i = this.forminfo;
                                i[a].val = t, this.forminfo = i;
                            },
                            radioChange: function (e) {
                                var t = e.detail.value,
                                    a = e.currentTarget.dataset.index,
                                    i = this.forminfo;
                                i[a].val = t, this.forminfo = i;
                            },
                            delimg: function (e) {
                                var t = e.currentTarget.dataset.index,
                                    a = e.currentTarget.dataset.id,
                                    i = this.forminfo,
                                    n = i[t].z_val;
                                n.splice(a, 1), 0 == n.length && (n = ""), i[t].z_val = n, this.forminfo = i;
                            },
                            choiceimg: function (e) {
                                var s = this,
                                    t = 0,
                                    o = s.zhixin,
                                    r = e.currentTarget.dataset.index,
                                    d = s.forminfo,
                                    a = d[r].val,
                                    i = d[r].tp_text[0];
                                a ? t = a.length : (t = 0, a = []);
                                var n = i - t;
                                s.pd_val, P.chooseImage({
                                    count: n,
                                    sizeType: ["original", "compressed"],
                                    sourceType: ["album", "camera"],
                                    success: function (e) {
                                        o = !0, s.zhixin = o, P.showLoading({
                                            title: "图片上传中"
                                        });
                                        var a = e.tempFilePaths,
                                            i = 0,
                                            n = a.length;
                                        ! function t() {
                                            P.uploadFile({
                                                url: s.$baseurl + "wxupimg",
                                                formData: {
                                                    uniacid: s.$uniacid
                                                },
                                                filePath: a[i],
                                                name: "file",
                                                success: function (e) {
                                                    d[r].z_val.push(e.data), s.forminfo = d, ++i < n ? t() : (o = !1, s.zhixin = o,
                                                        P.hideLoading());
                                                }
                                            });
                                        }();
                                    }
                                });
                            },
                            bindPickerChange: function (e) {
                                var t = e.detail.value,
                                    a = e.currentTarget.dataset.index,
                                    i = this.forminfo,
                                    n = i[a].tp_text[t];
                                i[a].val = n, this.forminfo = i;
                            },
                            bindDateChange: function (e) {
                                var t = e.detail.value,
                                    a = e.currentTarget.dataset.index,
                                    i = this.forminfo;
                                i[a].val = t, this.forminfo = i;
                            },
                            bindTimeChange: function (e) {
                                var t = e.detail.value,
                                    a = e.currentTarget.dataset.index,
                                    i = this.forminfo;
                                i[a].val = t, this.forminfo = i;
                            },
                            namexz: function (e) {
                                for (var t = this, a = e.currentTarget.dataset.index, i = t.forminfo[a], n = [], s = 0; s < i.tp_text.length; s++) {
                                    var o = {};
                                    o.keys = i.tp_text[s].yval, o.val = 1, n.push(o);
                                }
                                t.ttcxs = 1, t.formindex = a, t.xx = n, console.log(t.xx), t.xuanz = 0, t.lixuanz = -1,
                                    t.riqi();
                            },
                            riqi: function () {
                                for (var e = this, t = new Date(), a = new Date(t.getTime()), i = a.getFullYear() + "-" + (a.getMonth() + 1) + "-" + a.getDate(), n = e.xx, s = 0; s < n.length; s++) n[s].val = 1;
                                e.xx = n, e.gettoday(i);
                                for (var o = [], r = [], d = new Date(), c = 0; c < 5; c++) {
                                    var u = new Date(d.getTime() + 24 * c * 3600 * 1e3),
                                        l = u.getFullYear(),
                                        f = u.getMonth() + 1,
                                        h = u.getDate(),
                                        p = f + "月" + h + "日",
                                        y = l + "-" + f + "-" + h;
                                    o.push(p), r.push(y);
                                }
                                e.arrs = o, e.fallarrs = r, e.today = i;
                            },
                            gettoday: function (e) {
                                var n = this,
                                    t = n.formindex,
                                    s = n.xx;
                                P.request({
                                    url: n.$baseurl + "dopageDuzhan",
                                    data: {
                                        uniacid: n.$uniacid,
                                        id: 0,
                                        types: "showOrder",
                                        days: e,
                                        pagedatekey: t
                                    },
                                    success: function (e) {
                                        for (var t = e.data.data, a = 0; a < t.length; a++) s[t[a]].val = 2;
                                        var i = 0;
                                        t.length == s.length && (i = 1), n.xx = s, n.isover = i;
                                    }
                                });
                            },
                            goux: function (e) {
                                this.lixuanz = e.currentTarget.dataset.index;
                            },
                            xuanzd: function (e) {
                                for (var t = e.currentTarget.dataset.index, a = this.fallarrs[t], i = this.xx, n = 0; n < i.length; n++) i[n].val = 1;
                                this.xuanz = t, this.today = a, this.lixuanz = -1, this.xx = i, console.log(i),
                                    this.gettoday(a);
                            },
                            quxiao: function () {
                                this.ttcxs = 0;
                            },
                            save: function () {
                                var e = this.today,
                                    t = this.xx,
                                    a = this.lixuanz;
                                if (-1 == a) return P.showModal({
                                    title: "提现",
                                    content: "请选择预约的选项",
                                    showCancel: !1
                                }), !1;
                                var i = "已选择" + e + "，" + t[a].keys,
                                    n = this.forminfo,
                                    s = this.formindex;
                                n[s].val = i, n[s].days = e, n[s].indexkey = s, n[s].xuanx = a, this.ttcxs = 0,
                                    this.forminfo = n;
                            },
                            formSubmit: function () {
                                var a = this,
                                    e = a.suid;
                                if (!e) return P.showModal({
                                    title: "提醒",
                                    content: "您尚未登陆/注册，请先登陆/注册",
                                    success: function (e) {
                                        e.confirm && P.navigateTo({
                                            url: "/pages/usercenter/usercenter"
                                        });
                                    }
                                }), !1;
                                for (var t = a.forminfo, i = 0; i < t.length; i++) {
                                    if (1 == t[i].ismust)
                                        if (5 == t[i].type) {
                                            if (!t[i].z_val) return P.showModal({
                                                title: "提醒",
                                                content: t[i].name + "为必填项！",
                                                showCancel: !1
                                            }), !1;
                                        } else {
                                            if ("" == t[i].val) return P.showModal({
                                                title: "提醒",
                                                content: t[i].name + "为必填项！",
                                                showCancel: !1
                                            }), !1;
                                            if (0 == t[i].type && 1 == t[i].tp_text[0].yval) {
                                                if (!/^1[3456789]{1}\d{9}$/.test(t[i].val)) return P.showModal({
                                                    title: "提醒",
                                                    content: "请您输入正确的手机号码",
                                                    showCancel: !1
                                                }), !1;
                                            } else if (0 == t[i].type && 7 == t[i].tp_text[0].yval) {
                                                if (!/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test(t[i].val)) return P.showModal({
                                                    title: "提醒",
                                                    content: "请您输入正确的身份证号",
                                                    showCancel: !1
                                                }), !1;
                                            }
                                        }
                                    if (5 == t[i].type && t[i].z_val && 0 < t[i].z_val.length)
                                        for (var n = 0; n < t[i].z_val.length; n++) t[i].z_val[n] = t[i].z_val[n].substr(t[i].z_val[n].indexOf("/upimages"));
                                }
                                this.is_submit = 2;
                                var s = P.getStorageSync("openid");
                                P.request({
                                    url: a.$baseurl + "doPageFormval",
                                    data: {
                                        uniacid: a.$uniacid,
                                        suid: e,
                                        id: a.id,
                                        pagedata: JSON.stringify(t),
                                        fid: a.formset,
                                        types: "pt",
                                        source: P.getStorageSync("source"),
                                        openid: s,
                                        form_id: a.formId ? a.formId : ""
                                    },
                                    success: function (e) {
                                        var t = e.data.data.id;
                                        a.cid = t, P.showModal({
                                            title: "提醒",
                                            content: e.data.data.con,
                                            showCancel: !1,
                                            success: function () {
                                                a.dosubmit();
                                            }
                                        });
                                    }
                                });
                            },
                            pay1: function (e) {
                                var t = this,
                                    a = e;
                                P.showModal({
                                    title: "请注意",
                                    content: "您将使用余额支付" + t.newsfje + "元",
                                    success: function (e) {
                                        e.confirm ? (t.payover_do(a), P.showLoading({
                                            title: "下单中...",
                                            mask: !0
                                        })) : P.redirectTo({
                                            url: "/pagesPt/orderlist/orderlist"
                                        });
                                    }
                                });
                            },
                            payover_do: function (t) {
                                var a = this,
                                    e = (a.comment, a.yhqid),
                                    i = (a.order_id, a.shareid),
                                    n = a.dkscore,
                                    s = a.zg_type,
                                    o = a.money,
                                    r = a.sfje,
                                    d = a.yue_price,
                                    c = a.wx_price;
                                if (0 == s) var u = r;
                                1 == s && (u = o);
                                var l = P.getStorageSync("openid");
                                P.request({
                                    url: a.$baseurl + "doPageptorderchange",
                                    data: {
                                        uniacid: a.$uniacid,
                                        order_id: t,
                                        openid: l,
                                        true_price: u,
                                        dkscore: n,
                                        couponid: e,
                                        shareid: i,
                                        formid: a.formId,
                                        yue_price: d,
                                        wx_price: c,
                                        suid: a.suid,
                                        source: P.getStorageSync("source")
                                    },
                                    success: function (e) {
                                        a.sendMail_order(t), 0 == e.data.data ? P.redirectTo({
                                            url: "/pagesPt/orderlist/orderlist"
                                        }) : P.redirectTo({
                                            url: "/pagesPt/pt/pt?shareid=" + e.data.data
                                        });
                                    }
                                });
                            },
                            sendMail_order: function (e) {
                                P.request({
                                    url: this.$baseurl + "doPagesendMail_order",
                                    data: {
                                        uniacid: this.$uniacid,
                                        order_id: e
                                    },
                                    success: function (e) {},
                                    fail: function (e) {}
                                });
                            },
                            showpay: function () {
                                var t = this;
                                P.request({
                                    url: this.$baseurl + "doPageGetH5payshow",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: t.suid
                                    },
                                    success: function (e) {
                                        0 == e.data.data.ali && 0 == e.data.data.wx ? P.showModal({
                                            title: "提示",
                                            content: "请联系管理员设置支付参数",
                                            showCancel: !1,
                                            success: function (e) {
                                                return !1;
                                            }
                                        }) : (0 == e.data.data.ali ? (t.alipay = 0, t.pay_type = 2) : (t.alipay = 1, t.pay_type = 1),
                                            0 == e.data.data.wx ? t.wxpay = 0 : t.wxpay = 1, t.show_pay_type = 1);
                                    }
                                });
                            },
                            changealipay: function () {
                                this.pay_type = 1;
                            },
                            changewxpay: function () {
                                this.pay_type = 2;
                            },
                            close_pay_type: function () {
                                this.show_pay_type = 0;
                            },
                            h5topay: function () {
                                var e = this.pay_type;
                                1 == e ? this._alih5pay(this, this.newsfje, 10, this.orderid) : 2 == e && this._wxh5pay(this, this.newsfje, "pt", this.orderid),
                                    this.show_pay_type = 0;
                            }
                        }
                    };
                t.default = e;
            }).call(this, a("543d").default);
        }
    },
    [
        ["a99a", "common/runtime", "common/vendor"]
    ]
]);