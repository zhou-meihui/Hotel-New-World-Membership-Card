(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesPt/orderlist/orderlist"], {
        "08d8": function (t, e, a) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                a("020c"), a("921b"), e(a("66fd")), t(e(a("0aa9")).default);
            }).call(this, a("543d").createPage);
        },
        "0aa9": function (t, e, a) {
            a.r(e);
            var i = a("7537"),
                s = a("d53f");
            for (var o in s) "default" !== o && function (t) {
                a.d(e, t, function () {
                    return s[t];
                });
            }(o);
            a("beb9");
            var n = a("2877"),
                r = Object(n.a)(s.default, i.a, i.b, !1, null, null, null);
            e.default = r.exports;
        },
        "4fc3": function (t, e, a) {
            (function (r) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var i = a("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                page_sign: "order",
                                page: 1,
                                morePro: !1,
                                baseinfo: "",
                                orderinfo: [{
                                    pt_tx: ""
                                }],
                                orderinfo_length: 0,
                                type: 9,
                                showhx: "",
                                hxmm: "",
                                hxmm_list: [{
                                    val: "",
                                    fs: !0
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }],
                                is_tuikuan: 2,
                                hx_choose: 0,
                                hx_ewm: "",
                                flag: 10,
                                showPay: 0,
                                choosepayf: 0,
                                mymoney: 0,
                                mymoney_pay: 1,
                                is_submit: 1,
                                order_id: 0,
                                h5_wxpay: 0,
                                h5_alipay: 0,
                                pay_type: 1,
                                pay_money: 0
                            };
                        },
                        onLoad: function (t) {
                            var e = this;
                            t.type && (this.type = t.type), this._baseMin(this);
                            var a = 0;
                            t.fxsid && (a = t.fxsid, this.fxsid = a), r.setNavigationBarTitle({
                                title: "我的拼团订单"
                            }), i.getOpenid(a, function () {
                                e.getList();
                            });
                        },
                        onPullDownRefresh: function () {
                            this.getList(), r.stopPullDownRefresh();
                        },
                        methods: {
                            paybox: function (t) {
                                var e = 0,
                                    a = this.orderinfo,
                                    i = t.currentTarget.dataset.order;
                                this.order_id = i;
                                for (var s = 0; s < a.length; s++) a[s].order_id == i && (e = a[s].price);
                                this.pay_money = e, this.mymoney < e && (this.mymoney_pay = 2, this.pay_type = 2,
                                    this.choosepayf = 1), 0 == this.showPay && (this.showPay = 1);
                            },
                            payboxclose: function () {
                                1 == this.showPay && (this.showPay = 0);
                            },
                            choosepay: function (t) {
                                this.pay_type = t.currentTarget.dataset.pay_type, this.choosepayf = t.currentTarget.dataset.type;
                            },
                            pay: function (t) {
                                if (2 == this.is_submit) return !1;
                                this.is_submit = 2, this.$uniacid, this.suid, this.source;
                                var e = this.pay_type,
                                    a = this.pay_money,
                                    i = this.order_id,
                                    s = t.detail.formId;
                                1 == e ? this.pay1(i) : this._showwxpay(this, a, "pt", i, s, "/pagesPt/orderlist/orderlist");
                            },
                            pay1: function (t) {
                                var e = this,
                                    a = t;
                                r.showModal({
                                    title: "请注意",
                                    content: "您将使用余额支付" + e.pay_money + "元",
                                    success: function (t) {
                                        t.confirm ? (e.payover_do(a), r.showLoading({
                                            title: "下单中...",
                                            mask: !0
                                        })) : r.redirectTo({
                                            url: "/pagesPt/orderlist/orderlist"
                                        });
                                    }
                                });
                            },
                            payover_do: function (e) {
                                var a = this,
                                    t = (a.order_id, a.shareid),
                                    i = (a.mymoney, a.pay_money),
                                    s = a.pay_money,
                                    o = i,
                                    n = r.getStorageSync("openid");
                                r.request({
                                    url: a.$baseurl + "doPageptorderchange",
                                    data: {
                                        uniacid: a.$uniacid,
                                        order_id: e,
                                        openid: n,
                                        true_price: o,
                                        dkscore: 0,
                                        couponid: 0,
                                        shareid: t,
                                        formid: a.formId,
                                        yue_price: s,
                                        wx_price: 0,
                                        suid: a.suid,
                                        source: r.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        a.sendMail_order(e), 0 == t.data.data ? r.redirectTo({
                                            url: "/pagesPt/orderlist/orderlist"
                                        }) : r.redirectTo({
                                            url: "/pagesPt/pt/pt?shareid=" + t.data.data
                                        });
                                    }
                                });
                            },
                            sendMail_order: function (t) {
                                r.request({
                                    url: this.$baseurl + "doPagesendMail_order",
                                    data: {
                                        uniacid: this.$uniacid,
                                        order_id: t
                                    },
                                    success: function (t) {},
                                    fail: function (t) {}
                                });
                            },
                            getList: function (t) {
                                var e = this,
                                    a = r.getStorageSync("suid");
                                r.request({
                                    url: e.$baseurl + "dopageptorderlist",
                                    data: {
                                        uniacid: e.$uniacid,
                                        suid: a
                                    },
                                    success: function (t) {
                                        e.orderinfo = t.data.data.orders, e.orderinfo_length = t.data.data.orders.length,
                                            e.is_tuikuan = t.data.data.is_tuikuan, e.mymoney = t.data.data.mymoney;
                                    }
                                });
                            },
                            tuikuan: function (t) {
                                var e = this,
                                    a = t.target.dataset.order,
                                    i = t.detail.formId;
                                r.showModal({
                                    title: "提示",
                                    content: "确认申请退款？",
                                    success: function (t) {
                                        t.confirm ? r.request({
                                            url: e.$baseurl + "doPagepttk",
                                            data: {
                                                uniacid: e.$uniacid,
                                                orderid: a,
                                                formId: i
                                            },
                                            success: function (t) {
                                                1 == t.data.data ? r.showModal({
                                                    title: "提示",
                                                    content: "退款申请成功",
                                                    showCancel: !1,
                                                    success: function (t) {
                                                        e.getList();
                                                    }
                                                }) : r.showModal({
                                                    title: "提示",
                                                    content: "该订单已申请或不存在",
                                                    showCancel: !1,
                                                    success: function (t) {
                                                        e.getList();
                                                    }
                                                });
                                            }
                                        }) : t.cancel;
                                    }
                                });
                            },
                            hxshow: function (t) {
                                this.showhx = 1, this.order = t.currentTarget.dataset.order;
                            },
                            hxhide: function () {
                                this.showhx = 0, this.hxmm = "";
                            },
                            hxmmInput: function (t) {
                                for (var e = t.target.value.length, a = 0; a < this.hxmm_list.length; a++) this.hxmm_list[a].fs = !1,
                                    this.hxmm_list[a].val = t.target.value[a];
                                e && (this.hxmm_list[e - 1].fs = !0), this.hxmm = t.target.value;
                            },
                            hxmmpass: function () {
                                var a = this,
                                    t = a.hxmm,
                                    e = a.order;
                                t ? r.request({
                                    url: a.$baseurl + "Hxmm",
                                    data: {
                                        uniacid: a.$uniacid,
                                        hxmm: t,
                                        order_id: e,
                                        is_more: 3,
                                        suid: r.getStorageSync("suid")
                                    },
                                    success: function (t) {
                                        if (0 == t.data.data) {
                                            r.showModal({
                                                title: "提示",
                                                content: "核销密码不正确！",
                                                showCancel: !1
                                            }), a.hxmm = "";
                                            for (var e = 0; e < a.hxmm_list.length; e++) a.hxmm_list[e].fs = !1, a.hxmm_list[e].val = "";
                                        } else r.showToast({
                                            title: "消费成功",
                                            icon: "success",
                                            duration: 2e3,
                                            success: function (t) {
                                                a.showhx = 0, a.hxmm = "", r.startPullDownRefresh(), a.page = 1, a.getList(), r.stopPullDownRefresh();
                                            }
                                        });
                                    }
                                }) : r.showModal({
                                    title: "提示",
                                    content: "请输入核销密码！",
                                    showCancel: !1
                                });
                            },
                            wlinfo: function (t) {
                                var e = t.currentTarget.dataset.kuaidi,
                                    a = t.currentTarget.dataset.kuaidihao;
                                r.navigateTo({
                                    url: "/pages/logistics_state/logistics_state?kuaidi=" + e + "&kuaidihao=" + a
                                });
                            },
                            qrshouh: function (t) {
                                var e = this,
                                    a = t.currentTarget.dataset.orderid;
                                console.log(a);
                                var i = r.getStorageSync("suid");
                                r.showModal({
                                    title: "提示",
                                    content: "确认收货吗？",
                                    success: function (t) {
                                        t.confirm && r.request({
                                            url: e.$baseurl + "doPageptshouhuo",
                                            data: {
                                                uniacid: e.$uniacid,
                                                suid: i,
                                                orderid: a
                                            },
                                            success: function (t) {
                                                r.showToast({
                                                    title: "收货成功！",
                                                    success: function (t) {
                                                        setTimeout(function () {
                                                            e.page = 1, e.getList();
                                                        }, 1500);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            gethxmima: function () {
                                this.hx_choose = 1;
                            },
                            gethxImg: function () {
                                var e = this;
                                r.request({
                                    url: this.$baseurl + "doPageHxEwm",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: r.getStorageSync("suid"),
                                        pageUrl: "products",
                                        orderid: this.order
                                    },
                                    success: function (t) {
                                        e.hx_ewm = t.data.data, e.hx_choose = 2;
                                    }
                                });
                            },
                            hxhide1: function () {
                                this.hx_choose = 0, this.hxmm = "";
                                for (var t = 0; t < this.hxmm_list.length; t++) this.hxmm_list[t].fs = !1, this.hxmm_list[t].val = "";
                            },
                            changflag: function (t) {
                                var e = this,
                                    a = t.currentTarget.dataset.flag;
                                this.flag = a;
                                var i = r.getStorageSync("suid");
                                r.request({
                                    url: e.$baseurl + "dopageptorderlist",
                                    data: {
                                        uniacid: e.$uniacid,
                                        suid: i,
                                        flag: a
                                    },
                                    success: function (t) {
                                        e.orderinfo = t.data.data.orders, e.orderinfo_length = t.data.data.orders.length,
                                            e.is_tuikuan = t.data.data.is_tuikuan, e.mymoney = t.data.data.mymoney;
                                    }
                                });
                            }
                        }
                    };
                e.default = t;
            }).call(this, a("543d").default);
        },
        7537: function (t, e, a) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                s = [];
            a.d(e, "a", function () {
                return i;
            }), a.d(e, "b", function () {
                return s;
            });
        },
        "813d": function (t, e, a) {},
        beb9: function (t, e, a) {
            var i = a("813d");
            a.n(i).a;
        },
        d53f: function (t, e, a) {
            a.r(e);
            var i = a("4fc3"),
                s = a.n(i);
            for (var o in i) "default" !== o && function (t) {
                a.d(e, t, function () {
                    return i[t];
                });
            }(o);
            e.default = s.a;
        }
    },
    [
        ["08d8", "common/runtime", "common/vendor"]
    ]
]);