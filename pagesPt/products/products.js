(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesPt/products/products"], {
        "0e96": function (t, e, i) {
            i.r(e);
            var a = i("4bed"),
                s = i("dc75");
            for (var n in s) "default" !== n && function (t) {
                i.d(e, t, function () {
                    return s[t];
                });
            }(n);
            i("a9a0");
            var o = i("2877"),
                r = Object(o.a)(s.default, a.a, a.b, !1, null, null, null);
            e.default = r.exports;
        },
        "23d4": function (t, e, i) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                i("020c"), i("921b"), e(i("66fd")), t(e(i("0e96")).default);
            }).call(this, i("543d").createPage);
        },
        "4bed": function (t, e, i) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                s = [];
            i.d(e, "a", function () {
                return a;
            }), i.d(e, "b", function () {
                return s;
            });
        },
        a9a0: function (t, e, i) {
            var a = i("ee4a");
            i.n(a).a;
        },
        ae63: function (t, i, a) {
            (function (v) {
                Object.defineProperty(i, "__esModule", {
                    value: !0
                }), i.default = void 0;
                var t, g = (t = a("3584")) && t.__esModule ? t : {
                    default: t
                };
                var n = a("f571"),
                    e = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                baseinfo: "",
                                needAuth: !1,
                                needBind: !1,
                                gid: "",
                                imgUrls: [],
                                indicatorDots: !0,
                                autoplay: !0,
                                interval: 3e3,
                                duration: 1e3,
                                circular: !0,
                                num: 1,
                                sc: 0,
                                guige: 0,
                                protab: 1,
                                gwc: 1,
                                gm: 0,
                                foot: 1,
                                nowcon: "con",
                                is_comment: 0,
                                comments: 2,
                                u_gwc: 0,
                                xzarr: [],
                                gwccount: 0,
                                overtime: [],
                                daojishi: [],
                                isview: 0,
                                vip_config: 0,
                                products: "",
                                datas: "",
                                strgrouparr: "",
                                proinfo: "",
                                newstr: "",
                                gzjson: "",
                                pingtcount: 0,
                                shareid: 0,
                                xunh: "",
                                pintuan: "",
                                pic_video: "",
                                isplay: !1,
                                currentSwiper: 0,
                                minHeight: 220,
                                heighthave: 0,
                                shareimg: 0,
                                share: 0,
                                shareimg_url: "",
                                system_w: 0,
                                system_h: 0,
                                img_w: 0,
                                img_h: 0,
                                personPt: 0,
                                fiexdBoxs: 0,
                                buy_person: 0
                            };
                        },
                        onPullDownRefresh: function () {
                            this.getPro(), v.stopPullDownRefresh();
                        },
                        onLoad: function (t) {
                            var e = this;
                            t.id && (this.gid = t.id), this._baseMin(this);
                            var i = 0;
                            t.fxsid && (i = t.fxsid, v.setStorageSync("fxsid", i)), this.fxsid = i, console.log("分销商--" + this.fxsid);
                            var a = t.shareid;
                            a || (a = 0), t.userid && (this.userid = t.userid), v.getStorageSync("suid"), n.getOpenid(i, function () {
                                e.getPro();
                            });
                            var s = v.getStorageSync("systemInfo");
                            this.img_w = parseInt((.65 * s.windowWidth).toFixed(0)), this.img_h = parseInt((1.875 * this.img_w).toFixed(0)),
                                this.system_w = parseInt(s.windowWidth), this.system_h = parseInt(s.windowHeight);
                        },
                        onShareAppMessage: function () {
                            var t, e = this,
                                i = v.getStorageSync("suid"),
                                a = e.gid;
                            return t = 1 == e.baseinfo.fxs ? "/pagesPt/products/products?id=" + a + "&userid=" + i : "/pagesPt/products/products?id=" + a + "&userid=" + i + "&fxsid=" + i, {
                                title: e.title,
                                path: t,
                                success: function (t) {}
                            };
                        },
                        methods: {
                            navback: function () {
                                v.navigateBack();
                            },
                            toTop: function () {
                                v.pageScrollTo({
                                    scrollTop: 0
                                });
                            },
                            onPageScroll: function (t) {
                                200 < t.scrollTop ? this.fiexdBoxs = 1 : this.fiexdBoxs = 0;
                            },
                            makePhoneCall: function (t) {
                                var e = this.baseinfo.tel;
                                v.makePhoneCall({
                                    phoneNumber: e
                                });
                            },
                            showPtBox: function () {
                                this.personPt = 1;
                            },
                            closePtBox: function () {
                                this.personPt = 0;
                            },
                            checkvip: function () {
                                var a = this,
                                    s = 0,
                                    t = v.getStorageSync("suid");
                                if (!this.getSuid()) return !1;
                                v.request({
                                    url: a.$baseurl + "doPagecheckvip",
                                    data: {
                                        uniacid: a.$uniacid,
                                        kwd: "pt",
                                        suid: t,
                                        id: a.gid,
                                        gz: 1
                                    },
                                    success: function (t) {
                                        if (!1 === t.data.data) return a.needvip = !0, v.showModal({
                                            title: "进入失败",
                                            content: "使用本功能需先开通vip!",
                                            showCancel: !1,
                                            success: function (t) {
                                                t.confirm && v.redirectTo({
                                                    url: "/pages/register/register"
                                                });
                                            }
                                        }), !1;
                                        if (0 < t.data.data.needgrade ? (a.needvip = !0, 0 < t.data.data.grade ? t.data.data.grade < t.data.data.needgrade ? v.showModal({
                                                title: "进入失败",
                                                content: "使用本功能需成为" + t.data.data.vipname + "(" + t.data.data.needgrade + ")以上等级会员,请先升级!",
                                                showCancel: !1,
                                                success: function (t) {
                                                    t.confirm && v.redirectTo({
                                                        url: "/pages/open1/open1"
                                                    });
                                                }
                                            }) : s = 1 : t.data.data.grade < t.data.data.needgrade ? v.showModal({
                                                title: "进入失败",
                                                content: "使用本功能需成为" + t.data.data.vipname + "(" + t.data.data.needgrade + ")以上等级会员,请先开通会员后再升级会员等级!",
                                                showCancel: !1,
                                                success: function (t) {
                                                    t.confirm && v.redirectTo({
                                                        url: "/pages/register/register"
                                                    });
                                                }
                                            }) : s = 1) : s = 1, 1 == s) {
                                            var e = v.getStorageSync("subscribe");
                                            if (0 < e.length) {
                                                var i = new Array();
                                                1 == a.gm ? ("" != e[0].mid && i.push(e[0].mid), "" != e[1].mid && i.push(e[1].mid),
                                                        "" != e[2].mid && i.push(e[2].mid)) : ("" != e[3].mid && i.push(e[3].mid), "" != e[4].mid && i.push(e[4].mid)),
                                                    0 < i.length ? v.requestSubscribeMessage({
                                                        tmplIds: i,
                                                        success: function (t) {
                                                            a.gmget();
                                                        }
                                                    }) : a.gmget();
                                            } else a.gmget();
                                        }
                                    },
                                    fail: function (t) {}
                                });
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1, this.vipinfo();
                            },
                            getSuid: function () {
                                if (v.getStorageSync("suid")) return !0;
                                return v.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                    !1;
                            },
                            swiperLoad: function (a) {
                                var s = this;
                                v.getSystemInfo({
                                    success: function (t) {
                                        var e = a.detail.width / a.detail.height,
                                            i = t.windowWidth / e;
                                        s.heighthave || (s.minHeight = i, s.heighthave = 1);
                                    }
                                });
                            },
                            swiperChange: function (t) {
                                this.autoplay = !0, this.currentSwiper = t.detail.current, this.isplay = !1, this.autoplay = this.autoplay;
                            },
                            playvideo: function () {
                                this.autoplay = !1, this.isplay = !0, this.autoplay = this.autoplay;
                            },
                            endvideo: function () {
                                this.autoplay = !0, this.isplay = !1, this.autoplay = this.autoplay;
                            },
                            getPro: function () {
                                var h = this;
                                v.request({
                                    url: h.$baseurl + "doPagePtproductinfo",
                                    data: {
                                        uniacid: h.$uniacid,
                                        id: h.gid,
                                        suid: v.getStorageSync("suid"),
                                        source: v.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        var e = t.data.data,
                                            i = e.products,
                                            a = e.pingtuan,
                                            s = e.overtime,
                                            n = e.pingtcount;
                                        1 == i.show_pro && v.showModal({
                                                title: "提示",
                                                content: "该商品已下架",
                                                showCancel: !1,
                                                success: function () {
                                                    v.redirectTo({
                                                        url: "/pagesPt/index/index"
                                                    });
                                                }
                                            }), 1 == e.collect ? h.sc = 1 : h.sc = 0, h.dodaojishi(), h.products = i, h.buy_person = 6 < i.xsl ? 6 : i.xsl,
                                            h.pintuan = a, h.imgUrls = i.imgtext, h.guiz = e.guiz, h.overtime = s, h.pingtcount = n,
                                            h.pic_video = t.data.data.products.video, h.products.texts && (h.products.texts = h.products.texts.replace(/\<img/gi, '<img style="width:100%;height:auto;display:block" '),
                                                h.products.texts = (0, g.default)(h.products.texts));
                                        for (var o = e.grouparr, r = "", u = 0; u < o.length; u++) r += o[u] + "、";
                                        var d = r.substring(0, r.length - 1);
                                        h.strgrouparr = d, h.grouparr = o;
                                        var c = e.grouparr_val;
                                        h.gzjson = c, h.getproinfo();
                                    }
                                });
                            },
                            getproinfo: function () {
                                for (var i = this, t = i.gzjson, e = i.grouparr, a = i.gid, s = "", n = 0; n < e.length; n++) {
                                    s += t[e[n]].val[t[e[n]].ck] + "/";
                                }
                                var o = s.substring(0, s.length - 1);
                                v.request({
                                    url: i.$baseurl + "doPageptpinfo",
                                    data: {
                                        str: o,
                                        uniacid: i.$uniacid,
                                        id: a
                                    },
                                    success: function (t) {
                                        var e = t.data.data;
                                        1 == e.baseinfo.show_pro && v.showModal({
                                            title: "提示",
                                            content: "该商品已下架",
                                            showCancel: !1,
                                            success: function () {
                                                v.redirectTo({
                                                    url: "/pagesPt/index/index"
                                                });
                                            }
                                        }), i.proinfo = e.proinfo, i.baseinfo2 = e.baseinfo, i.newstr = o;
                                    }
                                });
                            },
                            dodaojishi: function () {
                                for (var t = this, e = t.overtime, i = [], a = 0; a < e.length; a++) {
                                    var s, n, o, r, u = new Date().getTime(),
                                        d = 1e3 * parseInt(e[a]) - u;
                                    0 <= d ? (s = Math.floor(d / 1e3 / 60 / 60 / 24), n = Math.floor(d / 1e3 / 60 / 60 % 24) < 10 ? "0" + Math.floor(d / 1e3 / 60 / 60 % 24) : Math.floor(d / 1e3 / 60 / 60 % 24),
                                        o = Math.floor(d / 1e3 / 60 % 60) < 10 ? "0" + Math.floor(d / 1e3 / 60 % 60) : Math.floor(d / 1e3 / 60 % 60),
                                        r = Math.floor(d / 1e3 % 60) < 10 ? "0" + Math.floor(d / 1e3 % 60) : Math.floor(d / 1e3 % 60),
                                        i[a] = 0 < s ? s + "天" + n + ":" + o + ":" + r : n + ":" + o + ":" + r) : i[a] = "已结束";
                                }
                                t.daojishi = i;
                                var c = setTimeout(function () {
                                    t.dodaojishi();
                                }, 1e3);
                                t.xunh = c;
                            },
                            guige_hidden: function () {
                                this.guige = 0, this.hareid = 0;
                            },
                            color_change: function (t) {},
                            changepro: function (t) {
                                var e = t.currentTarget.dataset.id,
                                    i = (this.grouparr, t.currentTarget.dataset.index),
                                    a = this.gzjson;
                                a[e].ck = i, this.gzjson = a, this.getproinfo();
                            },
                            collect: function () {
                                if (!this.getSuid()) return !1;
                                var t = this.sc;
                                0 == t ? v.showLoading({
                                    title: "收藏中"
                                }) : v.showLoading({
                                    title: "取消收藏中"
                                }), this.collects(t);
                            },
                            collects: function (e) {
                                var i = this;
                                v.request({
                                    url: i.$baseurl + "doPageCollect",
                                    cachetime: "30",
                                    data: {
                                        uniacid: i.$uniacid,
                                        id: i.gid,
                                        suid: v.getStorageSync("suid"),
                                        types: "pt"
                                    },
                                    success: function (t) {
                                        i.sc = 1 == e ? 0 : 1, v.showToast({
                                            title: t.data.data,
                                            icon: "success",
                                            duration: 2e3
                                        }), setTimeout(function () {
                                            v.hideLoading();
                                        }, 1e3);
                                    }
                                });
                            },
                            gwc_100: function () {
                                this.gwc = 1, this.gm = 0, this.guige = 1, this.foot = 0;
                            },
                            gm_100: function () {
                                this.gwc = 0, this.gm = 1, this.guige = 1, this.foot = 0;
                            },
                            gmget: function () {
                                var t = this,
                                    e = t.proinfo,
                                    i = t.num,
                                    a = t.products,
                                    s = t.baseinfo2,
                                    n = t.gwc,
                                    o = t.shareid,
                                    r = t.guiz;
                                if (0 == e.kc) return v.showModal({
                                    title: "提醒",
                                    content: "您来晚了，已经卖完了！",
                                    showCancel: !1
                                }), !1;
                                for (var u = e.comment.split(","), d = "", c = 0; c < u.length; c++) {
                                    var h = c + 1;
                                    d += u[c] + ":" + e["type" + h] + ",";
                                }
                                if (d = d.substring(0, d.length - 1), e.ggz = d, 1 == n) var g = 1 * e.price,
                                    l = g * i;
                                else l = (g = 1 * e.dprice) * i;
                                var f = {};
                                f.cid = s.cid, f.id = s.id, f.title = s.title, f.thumb = s.thumb, a.baseinfo2 = f,
                                    a.proinfo = e, a.num = i, a.pvid = e.pid, a.one_bili = r.one_bili, a.two_bili = r.two_bili,
                                    a.three_bili = r.three_bili, a.gmorpt = n;
                                var p = [];
                                p.push(a), v.setStorage({
                                    key: "jsdata",
                                    data: p
                                }), v.setStorage({
                                    key: "jsprice",
                                    data: l
                                }), clearInterval(t.xunh);
                                var m = a.kuaidi;
                                v.navigateTo({
                                    url: "/pagesPt/order/order?shareid=" + o + "&id=" + t.gid + "&kuaidi=" + m
                                });
                            },
                            lijct: function (t) {
                                var e = this,
                                    i = t.currentTarget.dataset.index,
                                    a = v.getStorageSync("suid");
                                if (!this.getSuid()) return !1;
                                v.request({
                                    url: e.$baseurl + "doPagepdmytuanorcy",
                                    data: {
                                        uniacid: e.$uniacid,
                                        shareid: i,
                                        suid: a
                                    },
                                    success: function (t) {
                                        1 == t.data.data ? (clearInterval(e.xunh), v.navigateTo({
                                            url: "/pagesPt/pt/pt?shareid=" + i
                                        })) : (e.gwc_100(), e.shareid = i);
                                    }
                                });
                            },
                            num_add: function () {
                                var t = this.proinfo.kc,
                                    e = this.num;
                                t < (e += 1) && (v.showModal({
                                    title: "提醒",
                                    content: "您的购买数量超过了库存！",
                                    showCancel: !1
                                }), e--), this.num = e;
                            },
                            num_jian: function () {
                                var t = this.num;
                                1 == t ? this.num = 1 : (t -= 1, this.num = t);
                            },
                            open_share: function () {
                                this.share = 1;
                            },
                            share_close: function () {
                                this.share = 0, this.shareimg = 0;
                            },
                            h5ShareAppMessage: function () {
                                var e = this,
                                    t = v.getStorageSync("suid");
                                v.showModal({
                                    title: "长按复制链接后分享",
                                    content: this.$host + "/h5/index.html?id=" + this.$uniacid + "#/pagesPt/products/products?id=" + this.gid + "&fxsid=" + t + "&userid=" + t,
                                    showCancel: !1,
                                    success: function (t) {
                                        e.share = 0;
                                    }
                                });
                            },
                            getShareImg: function () {
                                v.showLoading({
                                    title: "海报生成中"
                                });
                                var e = this;
                                v.request({
                                    url: e.$baseurl + "dopageshareewm",
                                    data: {
                                        uniacid: e.$uniacid,
                                        suid: v.getStorageSync("suid"),
                                        gid: e.gid,
                                        types: "pt",
                                        source: v.getStorageSync("source"),
                                        pageUrl: "products"
                                    },
                                    success: function (t) {
                                        v.hideLoading(), 0 == t.data.data.error ? (e.shareimg = 1, e.shareimg_url = t.data.data.url) : v.showToast({
                                            title: t.data.data.msg,
                                            icon: "none"
                                        });
                                    }
                                });
                            },
                            closeShare: function () {
                                this.shareimg = 0;
                            },
                            saveImg: function () {
                                var e = this;
                                v.getImageInfo({
                                    src: e.shareimg_url,
                                    success: function (t) {
                                        v.saveImageToPhotosAlbum({
                                            filePath: t.path,
                                            success: function () {
                                                v.showToast({
                                                    title: "保存成功！",
                                                    icon: "none"
                                                }), e.shareimg = 0, e.share = 0;
                                            }
                                        });
                                    }
                                });
                            },
                            aliSaveImg: function () {
                                var e = this;
                                v.getImageInfo({
                                    src: e.shareimg_url,
                                    success: function (t) {
                                        my.saveImage({
                                            url: t.path,
                                            showActionSheet: !0,
                                            success: function () {
                                                my.alert({
                                                    title: "保存成功"
                                                }), e.shareimg = 0, e.share = 0;
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    };
                i.default = e;
            }).call(this, a("543d").default);
        },
        dc75: function (t, e, i) {
            i.r(e);
            var a = i("ae63"),
                s = i.n(a);
            for (var n in a) "default" !== n && function (t) {
                i.d(e, t, function () {
                    return a[t];
                });
            }(n);
            e.default = s.a;
        },
        ee4a: function (t, e, i) {}
    },
    [
        ["23d4", "common/runtime", "common/vendor"]
    ]
]);