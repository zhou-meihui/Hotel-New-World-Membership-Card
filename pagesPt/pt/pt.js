(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesPt/pt/pt"], {
        "178f": function (t, a, i) {
            i.r(a);
            var e = i("1b34"),
                o = i.n(e);
            for (var n in e) "default" !== n && function (t) {
                i.d(a, t, function () {
                    return e[t];
                });
            }(n);
            a.default = o.a;
        },
        "1b34": function (t, i, e) {
            (function (a) {
                Object.defineProperty(i, "__esModule", {
                    value: !0
                }), i.default = void 0;
                var o = e("f571"),
                    t = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                baseinfo: "",
                                imgUrls: [],
                                indicatorDots: !0,
                                autoplay: !0,
                                interval: 5e3,
                                duration: 1e3,
                                hx: 0,
                                products: "",
                                share: "",
                                lists: "",
                                min: "",
                                max: "",
                                now: "",
                                overtime: "",
                                hxinfo: "",
                                daojishi: "",
                                daojishi_d: "",
                                daojishi_h: "",
                                daojishi_m: "",
                                daojishi_s: "",
                                fxsid: "",
                                shareid: ""
                            };
                        },
                        onLoad: function (t) {
                            var a = this;
                            t.id && (this.gid = t.id), this._baseMin(this);
                            var i = t.shareid;
                            this.shareid = i, t.userid && (this.userid = t.userid);
                            var e = 0;
                            t.fxsid && (e = t.fxsid, this.fxsid = e), o.getOpenid(e, function () {
                                a.getpingt();
                            });
                        },
                        onPullDownRefresh: function () {
                            this.getpingt(), a.stopPullDownRefresh();
                        },
                        onShareAppMessage: function () {
                            var t = this.shareid,
                                a = this.products,
                                i = "/pagesPt/products/products?shareid=" + t + "&id=" + a.id;
                            return {
                                title: a.title,
                                path: i
                            };
                        },
                        methods: {
                            getpingt: function () {
                                var l = this,
                                    t = l.shareid;
                                a.request({
                                    url: l.$baseurl + "doPagepingtuan",
                                    data: {
                                        shareid: t,
                                        uniacid: l.$uniacid,
                                        suid: a.getStorageSync("suid")
                                    },
                                    success: function (t) {
                                        var a = t.data.data.products,
                                            i = t.data.data.lists,
                                            e = t.data.data.share,
                                            o = t.data.data.products.pt_min,
                                            n = t.data.data.products.pt_max,
                                            d = i.length,
                                            r = t.data.data.hx;
                                        if (1 == r) {
                                            var s = t.data.data.hxinfo;
                                            l.hxinfo = s;
                                        }
                                        var u = {};
                                        u.infoimg = l.$imgurl + "pe.png";
                                        for (var f = [], h = 0; h < e.pt_max; h++) i[h] ? f.push(i[h]) : f.push(u);
                                        l.products = a, l.share = e, l.lists = f, l.min = o, l.max = n, l.now = d, l.overtime = t.data.data.overtime,
                                            l.hxinfo = s, l.hx = r, l.dodaojishi();
                                    }
                                });
                            },
                            dodaojishi: function () {
                                var t, a, i, e, o, n, d, r, s, u = this,
                                    f = u.overtime,
                                    h = new Date().getTime(),
                                    l = 1e3 * parseInt(f) - h;
                                0 == l && clearInterval(c), 0 <= l ? (t = Math.floor(l / 1e3 / 60 / 60 / 24), a = Math.floor(l / 1e3 / 60 / 60 % 24) < 10 ? "0" + Math.floor(l / 1e3 / 60 / 60 % 24) : Math.floor(l / 1e3 / 60 / 60 % 24),
                                        i = Math.floor(l / 1e3 / 60 % 60) < 10 ? "0" + Math.floor(l / 1e3 / 60 % 60) : Math.floor(l / 1e3 / 60 % 60),
                                        e = Math.floor(l / 1e3 % 60) < 10 ? "0" + Math.floor(l / 1e3 % 60) : Math.floor(l / 1e3 % 60)) : (t = 0,
                                        e = i = a = "00"), o = t + "天" + a + ":" + i + ":" + e, n = t < 10 ? "0" + t : t,
                                    d = a, r = i, s = e, u.daojishi = o, u.daojishi_d = n, u.daojishi_h = d, u.daojishi_m = r,
                                    u.daojishi_s = s;
                                var c = setTimeout(function () {
                                    u.dodaojishi();
                                }, 1e3);
                            },
                            ptorder: function () {
                                a.navigateTo({
                                    url: "/pagesPt/orderlist/orderlist"
                                });
                            }
                        }
                    };
                i.default = t;
            }).call(this, e("543d").default);
        },
        "7da4": function (t, a, i) {
            i.r(a);
            var e = i("c1c1"),
                o = i("178f");
            for (var n in o) "default" !== n && function (t) {
                i.d(a, t, function () {
                    return o[t];
                });
            }(n);
            i("a62d");
            var d = i("2877"),
                r = Object(d.a)(o.default, e.a, e.b, !1, null, null, null);
            a.default = r.exports;
        },
        a62d: function (t, a, i) {
            var e = i("fd31");
            i.n(e).a;
        },
        c1c1: function (t, a, i) {
            var e = function () {
                    this.$createElement;
                    this._self._c;
                },
                o = [];
            i.d(a, "a", function () {
                return e;
            }), i.d(a, "b", function () {
                return o;
            });
        },
        d00d: function (t, a, i) {
            (function (t) {
                function a(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                i("020c"), i("921b"), a(i("66fd")), t(a(i("7da4")).default);
            }).call(this, i("543d").createPage);
        },
        fd31: function (t, a, i) {}
    },
    [
        ["d00d", "common/runtime", "common/vendor"]
    ]
]);