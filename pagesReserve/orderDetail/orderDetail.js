(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesReserve/orderDetail/orderDetail"], {
        "0c26": function (a, t, e) {
            var n = e("c197");
            e.n(n).a;
        },
        "2ad2": function (a, t, e) {
            e.r(t);
            var n = e("fefb"),
                i = e.n(n);
            for (var o in n) "default" !== o && function (a) {
                e.d(t, a, function () {
                    return n[a];
                });
            }(o);
            t.default = i.a;
        },
        5633: function (a, t, e) {
            e.r(t);
            var n = e("81b8"),
                i = e("2ad2");
            for (var o in i) "default" !== o && function (a) {
                e.d(t, a, function () {
                    return i[a];
                });
            }(o);
            e("0c26");
            var d = e("2877"),
                s = Object(d.a)(i.default, n.a, n.b, !1, null, null, null);
            t.default = s.exports;
        },
        "81b8": function (a, t, e) {
            var n = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            e.d(t, "a", function () {
                return n;
            }), e.d(t, "b", function () {
                return i;
            });
        },
        8510: function (a, t, e) {
            (function (a) {
                function t(a) {
                    return a && a.__esModule ? a : {
                        default: a
                    };
                }
                e("020c"), e("921b"), t(e("66fd")), a(t(e("5633")).default);
            }).call(this, e("543d").createPage);
        },
        c197: function (a, t, e) {},
        fefb: function (a, t, e) {
            (function (w) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var o = e("f571"),
                    a = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                id: "",
                                bg: "",
                                picList: [],
                                datas: {
                                    yhInfo: {
                                        mj: []
                                    },
                                    beizhu_val: [],
                                    store_info: []
                                },
                                comment: "",
                                jhsl: 1,
                                dprice: "",
                                yhje: 0,
                                hjjg: "",
                                sfje: "",
                                order: "",
                                pro_name: "",
                                pro_tel: "",
                                pro_address: "",
                                pro_txt: "",
                                my_num: "",
                                my_gml: "",
                                xg_num: "",
                                shengyu: "",
                                cdd: "",
                                chuydate: "",
                                chuytime: "",
                                num: {
                                    index: {
                                        index: ""
                                    }
                                },
                                xz_num: [],
                                couponprice: 0,
                                jqdjg: "请选择",
                                yhqid: "0",
                                oldsfje: "",
                                pagedata: {},
                                hxmm: "",
                                hxmm_list: [{
                                    val: "",
                                    fs: !0
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }, {
                                    val: "",
                                    fs: !1
                                }],
                                showhx: 0,
                                orderFormDisable: !0,
                                isChange: "",
                                formchangeBtn: 1,
                                dkscore: 0,
                                dkmoney: 0,
                                yhqmoney_s: 0,
                                zf_type: "",
                                tableis: 0,
                                orderid: "",
                                manjian_info: "",
                                moneyoffstr: "",
                                jifen_u: 0,
                                yue: 0,
                                mymoney: 0,
                                modify_date_begin: "",
                                animationData: "",
                                myscore: 0,
                                pay_price: 0,
                                pay_type: 1,
                                show_pay_type: 0,
                                alipay: 1,
                                wxpay: 1,
                                hx_choose: 0,
                                baseinfo: "",
                                hx_ewm: ""
                            };
                        },
                        onPullDownRefresh: function () {
                            var a = this.order;
                            this.getOrder(a), w.stopPullDownRefresh();
                        },
                        onLoad: function (a) {
                            var t = this,
                                e = this,
                                n = a.id;
                            e.order = n, a.tableis && (e.tableis = a.tableis), 0 == a.tsid ? e.tableis = 0 : e.tableis = 1;
                            var i = 0;
                            a.fxsid && (i = a.fxsid), this.fxsid = i, this._baseMin(this), o.getOpenid(i, function () {
                                var a = e.order;
                                e.getOrder(a);
                            }, function () {
                                t.needAuth = !0;
                            }, function () {
                                t.needBind = !0;
                            });
                        },
                        methods: {
                            makePhoneCall: function (a) {
                                var t = a.currentTarget.dataset.tel;
                                w.makePhoneCall({
                                    phoneNumber: t
                                });
                            },
                            changeOrderFormDisable: function () {
                                this.orderFormDisable = !1, this.isChange = "isChange", this.formchangeBtn = 3;
                            },
                            changeOrderFormConfirm: function () {
                                var t = this;
                                w.showModal({
                                    title: "确定提交吗",
                                    content: "只有一次修改的机会哦",
                                    success: function (a) {
                                        a.confirm && w.request({
                                            url: t.$baseurl + "doPageapplyModifyAppointInfo",
                                            data: {
                                                pro_name: t.pro_name,
                                                pro_tel: t.pro_tel,
                                                pro_address: t.pro_address,
                                                chuydate: t.chuydate,
                                                chuytime: t.chuytime,
                                                order_id: t.order,
                                                uniacid: t.$uniacid
                                            },
                                            success: function (a) {
                                                t.orderFormDisable = !0, t.isChange = "", t.formchangeBtn = 4, w.showModal({
                                                    title: "提示",
                                                    content: "信息修改成功，请等待后台管理员审核！",
                                                    showCancel: !1
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            changeOrderFormCancel: function () {
                                this.orderFormDisable = !0, this.isChange = "", this.formchangeBtn = 2;
                            },
                            ContactMerchant: function () {
                                var e = this;
                                w.showModal({
                                    title: "提示",
                                    content: "请联系商家咨询具体信息！",
                                    confirmText: "联系商家",
                                    success: function (a) {
                                        if (a.confirm) {
                                            var t = e.baseinfo.tel;
                                            w.makePhoneCall({
                                                phoneNumber: t
                                            });
                                        }
                                    }
                                });
                            },
                            getOrder: function (a) {
                                var c = this;
                                w.request({
                                    url: c.$baseurl + "doPagemycoupon",
                                    data: {
                                        suid: w.getStorageSync("suid"),
                                        uniacid: c.$uniacid
                                    },
                                    success: function (a) {
                                        c.couponlist = a.data.data;
                                    },
                                    fail: function (a) {}
                                }), w.request({
                                    url: c.$baseurl + "doPageOrderinfo",
                                    data: {
                                        order: a,
                                        suid: w.getStorageSync("suid"),
                                        uniacid: c.$uniacid
                                    },
                                    cachetime: "30",
                                    success: function (a) {
                                        c.yhje;
                                        var t = a.data.data.yhInfo,
                                            e = a.data.data.order_id;
                                        0 != t && (0 < t.score.money ? (c.ischecked = !0, c.jifen_u = 1, c.dkscore = t.score.msg.slice(0, t.score.msg.indexOf("积分")),
                                                c.dkmoney = t.score.money) : c.jifen_u = 0, console.log(t), console.log(t.mj), t.mj ? c.manjian_info = t.mj.msg : c.manjian_info = "无",
                                            t.yhq ? c.jqdjg = t.yhq.msg : c.jqdjg = "无");
                                        for (var n = a.data.data.more_type_x, i = [], o = {}, d = 0, s = 0; s < n.length; s++) o[s] = n[s][4],
                                            i.push(o), d = (100 * d + 100 * n[s][4]) / 100;
                                        if ("3" == a.data.data.flag && "1" == a.data.data.pro_flag_ding ? c.formchangeBtn = 0 : "1" != a.data.data.flag || 0 != a.data.data.can_modify || a.data.data.modify_info ? "1" != a.data.data.flag || 1 != a.data.data.can_modify || a.data.data.modify_info ? a.data.data.modify_info && 1 == a.data.data.modify_flag ? c.formchangeBtn = 4 : a.data.data.modify_info && 2 == a.data.data.modify_flag ? c.formchangeBtn = 5 : a.data.data.modify_info && 3 == a.data.data.modify_flag && (c.formchangeBtn = 6) : c.formchangeBtn = 2 : c.formchangeBtn = 1,
                                            c.id = a.data.data.pid, c.datas = a.data.data, c.dprice = a.data.data.price, c.jhsl = a.data.data.num,
                                            c.hjjg = a.data.data.price, c.sfje = a.data.data.true_price, c.pro_name = a.data.data.pro_user_name,
                                            c.pro_tel = a.data.data.pro_user_tel, c.pro_address = a.data.data.pro_user_add,
                                            c.pro_txt = a.data.data.pro_user_txt, c.my_num = a.data.data.my_num, c.xg_num = a.data.data.pro_xz,
                                            c.shengyu = a.data.data.pro_kc, c.my_gml = a.data.data.my_num, c.cdd = a.data.data.mcount,
                                            c.chuydate = a.data.data.chuydate, c.chuytime = a.data.data.chuytime, c.modify_date_begin = a.data.data.modify_date_begin,
                                            c.num = i, c.chooseNum = d, c.xz_num = a.data.data.more_type_num, c.oldsfje = a.data.data.price,
                                            c.yhqid = a.data.data.couponid, c.pagedata = a.data.data.beizhu_val, c.myscore = parseFloat(a.data.data.my_score),
                                            c.mymoney = parseFloat(a.data.data.my_money), c.zf_type = parseFloat(a.data.data.my_money) >= a.data.data.true_price ? 0 : 1,
                                            c.orderid = e, c.pay_price = a.data.data.pay_price, 1 == c.zf_type) {
                                            var r = (c.sfje - c.pay_price).toFixed(2);
                                            c.yue = r;
                                        }
                                        w.setNavigationBarTitle({
                                            title: c.datas.product
                                        }), w.setStorageSync("isShowLoading", !1), 0 == a.data.data.flag && w.request({
                                            url: c.$baseurl + "doPagegetmoneyoff",
                                            data: {
                                                uniacid: c.$uniacid
                                            },
                                            success: function (a) {
                                                for (var t = a.data.data, e = "", n = 0; n < t.length; n++) n == t.length - 1 ? e += "满" + t[n].reach + "减" + t[n].del : e += "满" + t[n].reach + "减" + t[n].del + "，";
                                                c.sfje, c.moneyoff = t, c.moneoffstr = t ? e : "";
                                            }
                                        });
                                    }
                                });
                            },
                            getmyinfo: function () {
                                var a = this,
                                    t = (w.getStorageSync("suid"), a.moneyoff),
                                    e = a.hjjg;
                                if (t)
                                    for (var n = t.length - 1; 0 <= n; n--)
                                        if (e >= parseFloat(t[n].reach)) {
                                            e -= parseFloat(t[n].del);
                                            break;
                                        }
                                a.sfje = e, a.zf_type = a.mymoney >= e ? 0 : 1;
                            },
                            jian: function (a) {
                                var t = this,
                                    e = t.yhje,
                                    n = a.currentTarget.dataset.testid,
                                    i = a.currentTarget.dataset.testkey,
                                    o = t.num[i][i],
                                    d = (t.duogg,
                                        t.sfje),
                                    s = t.hjjg,
                                    r = t.oldsfje;
                                if (--o < 0) o = 0;
                                else {
                                    var c = Math.round(100 * r - 100 * n * o + 100 * n * (o - 1)) / 100;
                                    d = c - e, r = s = c;
                                    var u = t.num;
                                    u[i][i] = o, t.num = u, t.sfje = d, t.hjjg = s, t.jqdjg = "请选择", t.oldsfje = r,
                                        t.yhqid = 0;
                                }
                                t.getmyinfo();
                            },
                            jia: function (a) {
                                var t = this,
                                    e = (t.yhje, a.currentTarget.dataset.testid),
                                    n = a.currentTarget.dataset.testkey,
                                    i = t.num[n][n],
                                    o = (t.duogg,
                                        t.oldsfje),
                                    d = t.hjjg;
                                if (t.xz_num[n].shennum < ++i) return i--, w.showModal({
                                    title: "提醒",
                                    content: "库存量不足！",
                                    showCancel: !1
                                }), !1;
                                var s = Math.round(100 * e * i + 100 * o - 100 * e * (i - 1)) / 100;
                                d = o = s;
                                var r = t.num;
                                r[n][n] = i;
                                var c = t.chooseNum + 1;
                                t.num = r, t.hjjg = d, t.jqdjg = "请选择", t.oldsfje = o, t.yhqid = 0, t.chooseNum = c,
                                    t.ischecked = !1, t.dkscore = 0, t.dkmoney = 0, t.jifen_u = 2, t.getmyinfo();
                            },
                            userNameInput: function (a) {
                                this.pro_name = a.detail.value;
                            },
                            userTelInput: function (a) {
                                this.pro_tel = a.detail.value;
                            },
                            userAddInput: function (a) {
                                this.pro_address = a.detail.value;
                            },
                            userTextInput: function (a) {
                                this.pro_txt = a.detail.value;
                            },
                            save: function () {
                                var e = this,
                                    a = e.jhsl,
                                    t = e.shengyu;
                                if (1 != e.tableis && t < a && -1 != t) return a--, w.showModal({
                                    title: "提醒",
                                    content: "库存量不足！",
                                    showCancel: !1
                                }), !1;
                                for (var n = e.sfje, i = w.getStorageSync("suid"), o = (e.duogg, e.num), d = (o[0],
                                        []), s = {}, r = 0; r < o.length; r++) {
                                    var c = parseInt(o[r][r]);
                                    s[r] = c, d.push(s);
                                }
                                var u = e.chuydate,
                                    h = e.chuytime,
                                    l = (e.yhje, e.id),
                                    f = e.order,
                                    m = e.pro_name,
                                    p = e.pro_tel,
                                    y = e.pro_address,
                                    g = e.pro_txt,
                                    _ = (e.id,
                                        e.yhqid),
                                    v = !0;
                                if (0 == n) return v = !1, w.showModal({
                                    title: "提醒",
                                    content: "请至少选择一个规格的商品！",
                                    showCancel: !1
                                }), !1;
                                if (!m && 2 == e.datas.pro_flag) return v = !1, w.showModal({
                                    title: "提醒",
                                    content: "姓名为必填！",
                                    showCancel: !1
                                }), !1;
                                if (!p && 2 == e.datas.pro_flag_tel) return v = !1, w.showModal({
                                    title: "提醒",
                                    content: "手机号为必填！",
                                    showCancel: !1
                                }), !1;
                                if (!/^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(16[0-9]{1})|(17[0-9]{1})|(18[0-9]{1})|(19[0-9]{1}))+\d{8})$/.test(p) && 2 == e.datas.pro_flag_tel) return w.showModal({
                                    title: "提醒",
                                    content: "请输入有效的手机号码！",
                                    showCancel: !1
                                }), !1;
                                if (!y && 2 == e.datas.pro_flag_add) return v = !1, w.showModal({
                                    title: "提醒",
                                    content: "地址为必填！",
                                    showCancel: !1
                                }), !1;
                                if ("选择日期" == u && 2 == e.datas.pro_flag_data) return v = !1, w.showModal({
                                    title: "提醒",
                                    content: "请选择日期！",
                                    showCancel: !1
                                }), !1;
                                if ("选择时间" == h && 2 == e.datas.pro_flag_time) return v = !1, w.showModal({
                                    title: "提醒",
                                    content: "请选择时间！",
                                    showCancel: !1
                                }), !1;
                                var j = e.pagedata;
                                for (r = 0; r < j.length; r++)
                                    if (1 == j[r].ismust && "" == j[r].val) return v = !1,
                                        w.showModal({
                                            title: "提醒",
                                            content: j[r].name + "为必填项！",
                                            showCancel: !1
                                        }), !1;
                                if (v) {
                                    var x = e.orderid;
                                    x ? n <= e.mymoney ? e.pay1(x) : e.pay2(x) : w.request({
                                        url: e.$baseurl + "doPagecreateorder",
                                        data: {
                                            suid: i,
                                            num: JSON.stringify(d),
                                            id: l,
                                            hjjg: e.hjjg,
                                            zhifu: n,
                                            order: f,
                                            pro_name: m,
                                            pro_tel: p,
                                            pro_address: y,
                                            pro_txt: g,
                                            chuydate: u,
                                            chuytime: h,
                                            yhqid: _,
                                            dkscore: e.dkscore,
                                            dkmoney: e.dkmoney,
                                            pagedata: JSON.stringify(j),
                                            types: "reserve",
                                            uniacid: e.$uniacid
                                        },
                                        header: {
                                            "content-type": "application/json"
                                        },
                                        success: function (a) {
                                            if ("1" == a.data.data.errcode) w.showModal({
                                                title: a.data.data.err,
                                                content: "请重新下单",
                                                showCancel: !1
                                            });
                                            else if ("3" == a.data.data.errcode) w.showModal({
                                                title: a.data.data.err,
                                                content: "当前库存为" + a.data.data.kc + "件",
                                                showCancel: !1
                                            });
                                            else {
                                                var t = a.data.data;
                                                e.orderid = t, n <= e.mymoney ? e.pay1(t) : e.pay2(t);
                                            }
                                        }
                                    });
                                }
                            },
                            showpay: function () {
                                var t = this;
                                w.request({
                                    url: this.$baseurl + "doPageGetH5payshow",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: w.getStorageSync("suid")
                                    },
                                    success: function (a) {
                                        0 == a.data.data.ali && 0 == a.data.data.wx ? w.showModal({
                                            title: "提示",
                                            content: "请联系管理员设置支付参数",
                                            success: function (a) {
                                                w.redirectTo({
                                                    url: "/pages/index/index"
                                                });
                                            }
                                        }) : (0 == a.data.data.ali ? (t.alipay = 0, t.pay_type = 2) : (t.alipay = 1, t.pay_type = 1),
                                            0 == a.data.data.wx ? t.wxpay = 0 : t.wxpay = 1, t.show_pay_type = 1);
                                    }
                                });
                            },
                            changealipay: function () {
                                this.pay_type = 1;
                            },
                            changewxpay: function () {
                                this.pay_type = 2;
                            },
                            close_pay_type: function () {
                                this.show_pay_type = 0;
                            },
                            h5topay: function () {
                                var a = this.pay_type;
                                1 == a ? this._alih5pay(this, this.pay_price, 9, this.orderid) : 2 == a && this._wxh5pay(this, this.pay_price, "reserve", this.orderid),
                                    this.show_pay_type = 0;
                            },
                            pay1: function (t) {
                                var e = this;
                                w.showModal({
                                    title: "请注意",
                                    content: "您将使用余额支付" + e.sfje + "元",
                                    success: function (a) {
                                        a.confirm && (e.payover_do(t), w.showLoading({
                                            title: "下单中...",
                                            mask: !0
                                        }));
                                    }
                                });
                            },
                            pay2: function (t) {
                                var e = this,
                                    a = w.getStorageSync("suid"),
                                    n = e.sfje;
                                w.request({
                                    url: e.$baseurl + "doPagebeforepay",
                                    data: {
                                        suid: a,
                                        price: n,
                                        order_id: t,
                                        types: "reserve",
                                        formId: e.formId,
                                        uniacid: e.$uniacid
                                    },
                                    header: {
                                        "content-type": "application/json"
                                    },
                                    success: function (a) {
                                        1 == a.data.data.errs && w.showModal({
                                            title: "支付失败",
                                            content: a.data.data.return_msg,
                                            showCancel: !1
                                        }); -
                                        1 != [1, 2, 3, 4].indexOf(a.data.data.err) && w.showModal({
                                            title: "支付失败",
                                            content: a.data.data.message,
                                            showCancel: !1
                                        }), 0 == a.data.data.err && (w.request({
                                            url: e.$baseurl + "doPagesavePrepayid",
                                            data: {
                                                types: "reserve",
                                                order_id: t,
                                                prepayid: a.data.data.package,
                                                uniacid: e.$uniacid
                                            },
                                            success: function (a) {},
                                            fail: function (a) {}
                                        }), w.requestPayment({
                                            timeStamp: a.data.data.timeStamp,
                                            nonceStr: a.data.data.nonceStr,
                                            package: a.data.data.package,
                                            signType: "MD5",
                                            paySign: a.data.data.paySign,
                                            success: function (a) {
                                                w.showToast({
                                                    title: "支付成功",
                                                    icon: "success",
                                                    duration: 3e3,
                                                    success: function (a) {
                                                        e.payover_fxs(t), w.showToast({
                                                            title: "购买成功！",
                                                            icon: "success",
                                                            success: function () {
                                                                setTimeout(function () {
                                                                    w.navigateBack({
                                                                        delta: 9
                                                                    }), w.navigateTo({
                                                                        url: "/pagesReserve/orderList/orderList"
                                                                    });
                                                                }, 1500);
                                                            }
                                                        });
                                                    }
                                                });
                                            },
                                            fail: function (a) {},
                                            complete: function (a) {}
                                        }));
                                    }
                                });
                            },
                            payover_do: function (a) {
                                var t = w.getStorageSync("suid"),
                                    e = this.sfje;
                                w.request({
                                    url: this.$baseurl + "doPagepaynotify",
                                    data: {
                                        out_trade_no: a,
                                        suid: t,
                                        payprice: e,
                                        types: "reserve",
                                        flag: 0,
                                        formId: this.formId,
                                        uniacid: this.$uniacid
                                    },
                                    success: function (a) {
                                        w.showToast({
                                            title: "购买成功！",
                                            icon: "success",
                                            success: function () {
                                                setTimeout(function () {
                                                    w.navigateBack({
                                                        delta: 9
                                                    }), w.navigateTo({
                                                        url: "/pagesReserve/orderList/orderList"
                                                    });
                                                }, 1500);
                                            }
                                        });
                                    }
                                });
                            },
                            passd: function () {
                                var t = this,
                                    e = t.order;
                                w.showModal({
                                    title: "提醒",
                                    content: "亲，您确定要删除该订单？",
                                    success: function (a) {
                                        a.confirm && w.request({
                                            url: t.$baseurl + "doPagedpass",
                                            data: {
                                                order: e,
                                                uniacid: t.$uniacid
                                            },
                                            header: {
                                                "content-type": "application/json"
                                            },
                                            success: function (a) {
                                                1 == a.data.data && w.showToast({
                                                    title: "订单取消成功",
                                                    icon: "success",
                                                    duration: 2e3,
                                                    success: function () {
                                                        w.redirectTo({
                                                            url: "/pagesReserve/orderList/orderList?type=9"
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            bindDateChange2: function (a) {
                                this.chuydate = a.detail.value;
                            },
                            bindTimeChange2: function (a) {
                                this.chuytime = a.detail.value;
                            },
                            getmoney: function (a) {
                                var t = this,
                                    e = a.currentTarget.id,
                                    n = a.currentTarget.dataset.index,
                                    i = n.coupon.pay_money,
                                    o = t.hjjg,
                                    d = t.sfje;
                                if (1 * o < 1 * i) w.showModal({
                                    title: "提示",
                                    content: "价格未满" + i + "元，不可使用该优惠券！",
                                    showCancel: !1
                                });
                                else {
                                    var s = (100 * d - 100 * e) / 100;
                                    t.ischecked = !1, t.switchChange({
                                            detail: {
                                                value: !1
                                            }
                                        }), (d = parseFloat(s.toPrecision(12)) + parseFloat(t.dkmoney)) < 0 && (d = 0),
                                        t.jqdjg = e, t.yhqid = n.id, t.sfje = d, t.oldsfje = o, t.yhqmoney_s = e, t.zf_type = t.mymoney >= d ? 0 : 1,
                                        t.hideModal();
                                }
                            },
                            qxyh: function () {
                                var a = this,
                                    t = a.jqdjg;
                                "请选择" == t && (t = 0), a.sfje, a.hideModal(), a.jqdjg = 0, a.yhqid = 0, a.sfje = huany,
                                    a.jqdjg = "请选择";
                            },
                            showModal: function () {
                                var a = w.createAnimation({
                                    duration: 200,
                                    timingFunction: "linear",
                                    delay: 0
                                });
                                (this.animation = a).translateY(300).step(), this.animationData = a.export(), this.showModalStatus = !0,
                                    setTimeout(function () {
                                        a.translateY(0).step(), this.animationData = a.export();
                                    }.bind(this), 200);
                            },
                            hideModal: function () {
                                var a = w.createAnimation({
                                    duration: 200,
                                    timingFunction: "linear",
                                    delay: 0
                                });
                                (this.animation = a).translateY(300).step(), this.animationData = a.export(), setTimeout(function () {
                                    a.translateY(0).step(), animationData = a.export(), showModalStatus = !1;
                                }.bind(this), 200);
                            },
                            switchChange: function (a) {
                                var o = this,
                                    t = a.detail.value,
                                    e = w.getStorageSync("suid"),
                                    d = o.sfje,
                                    s = 0,
                                    n = "table" == o.type ? o.select_num : o.chooseNum;
                                1 == t ? w.request({
                                    url: o.$baseurl + "doPagescoreDeduction",
                                    data: {
                                        id: o.id,
                                        num: n,
                                        suid: e,
                                        is_more: 1,
                                        uniacid: o.$uniacid
                                    },
                                    header: {
                                        "content-type": "application/json"
                                    },
                                    success: function (a) {
                                        var t = a.data.data;
                                        s = t.moneycl;
                                        var e = t.gzmoney,
                                            n = t.gzscore;
                                        if (d < s && (s = parseInt(d)), 0 == s) var i = 0;
                                        else i = s * n / e;
                                        d = Math.round(100 * (d - s)) / 100, o.sfje = d, o.dkmoney = s, o.dkscore = i, o.jifen_u = 1,
                                            o.zf_type = o.mymoney >= d ? 0 : 1;
                                    }
                                }) : (d = parseFloat(d) + parseFloat(o.dkmoney), o.dkmoney = 0, o.dkscore = 0, o.jifen_u = 0,
                                    o.zf_type = o.mymoney >= d ? 0 : 1);
                            },
                            bindInputChange: function (a) {
                                var t = a.detail.value,
                                    e = a.currentTarget.dataset.index,
                                    n = this.pagedata;
                                n[e].val = t, this.pagedata = n;
                            },
                            bindPickerChange: function (a) {
                                var t = a.detail.value,
                                    e = a.currentTarget.dataset.index,
                                    n = this.pagedata,
                                    i = n[e].tp_text[t];
                                n[e].val = i, this.pagedata = n;
                            },
                            bindDateChange: function (a) {
                                var t = a.detail.value,
                                    e = a.currentTarget.dataset.index,
                                    n = this.pagedata;
                                n[e].val = t, this.pagedata = n;
                            },
                            bindTimeChange: function (a) {
                                var t = a.detail.value,
                                    e = a.currentTarget.dataset.index,
                                    n = this.pagedata;
                                n[e].val = t, this.pagedata = n;
                            },
                            checkboxChange: function (a) {
                                var t = a.detail.value,
                                    e = a.currentTarget.dataset.index,
                                    n = this.pagedata;
                                n[e].val = t, this.pagedata = n;
                            },
                            radioChange: function (a) {
                                var t = a.detail.value,
                                    e = a.currentTarget.dataset.index,
                                    n = this.pagedata;
                                n[e].val = t, this.pagedata = n;
                            },
                            hxmmInput: function (a) {
                                for (var t = a.target.value.length, e = 0; e < this.hxmm_list.length; e++) this.hxmm_list[e].fs = !1,
                                    this.hxmm_list[e].val = a.target.value[e];
                                t && (this.hxmm_list[t - 1].fs = !0), this.hxmm = a.target.value;
                            },
                            hxmmpass: function () {
                                var e = this,
                                    a = e.hxmm,
                                    n = e.datas;
                                a ? w.request({
                                    url: e.$baseurl + "hxmm",
                                    data: {
                                        hxmm: a,
                                        order_id: n.order_id,
                                        uniacid: e.$uniacid,
                                        suid: w.getStorageSync("suid"),
                                        is_more: 1
                                    },
                                    header: {
                                        "content-type": "application/json"
                                    },
                                    success: function (a) {
                                        if (0 == a.data.data) {
                                            w.showModal({
                                                title: "提示",
                                                content: "核销密码不正确！",
                                                showCancel: !1
                                            }), e.hxmm = "";
                                            for (var t = 0; t < e.hxmm_list.length; t++) e.hxmm_list[t].fs = !1, e.hxmm_list[t].val = "";
                                        } else w.showToast({
                                            title: "消费成功",
                                            icon: "success",
                                            duration: 2e3,
                                            success: function (a) {
                                                n.flag = 2, e.datas = n, e.showhx = 0, e.hxmm = "";
                                                var t = e.order;
                                                e.getOrder(t);
                                            }
                                        });
                                    }
                                }) : w.showModal({
                                    title: "提示",
                                    content: "请输入核销密码！",
                                    showCancel: !1
                                });
                            },
                            hxshow: function () {
                                this.showhx = 1;
                            },
                            hxhide: function () {
                                this.showhx = 0, this.hxmm = "";
                            },
                            gethxmima: function () {
                                this.hx_choose = 1;
                            },
                            gethxImg: function () {
                                var t = this;
                                w.request({
                                    url: this.$baseurl + "doPageHxEwm",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: w.getStorageSync("suid"),
                                        pageUrl: "proDetail",
                                        orderid: this.order
                                    },
                                    success: function (a) {
                                        t.hx_ewm = a.data.data, t.hx_choose = 2;
                                    }
                                });
                            },
                            hxhide1: function () {
                                this.hx_choose = 0, this.hxmm = "";
                                for (var a = 0; a < this.hxmm_list.length; a++) this.hxmm_list[a].fs = !1, this.hxmm_list[a].val = "";
                            }
                        }
                    };
                t.default = a;
            }).call(this, e("543d").default);
        }
    },
    [
        ["8510", "common/runtime", "common/vendor"]
    ]
]);