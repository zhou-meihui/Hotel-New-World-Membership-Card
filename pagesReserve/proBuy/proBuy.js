(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesReserve/proBuy/proBuy"], {
        "300e": function (e, t, a) {},
        3800: function (e, t, n) {
            (function (P) {
                function a(e, t, a) {
                    return t in e ? Object.defineProperty(e, t, {
                        value: a,
                        enumerable: !0,
                        configurable: !0,
                        writable: !0
                    }) : e[t] = a, e;
                }
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var o = n("f571"),
                    e = {
                        data: function () {
                            return {
                                $imgurl: this.$imgurl,
                                $host: this.$host,
                                baseinfo: "",
                                id: "",
                                bg: "",
                                couponlist: [],
                                picList: [],
                                datas: {},
                                comment: "",
                                jhsl: 1,
                                dprice: "",
                                yhje: 0,
                                hjjg: 0,
                                sfje: 0,
                                order: "",
                                pro_name: "",
                                pro_tel: "",
                                pro_address: "",
                                pro_txt: "",
                                my_num: "",
                                xg_num: "",
                                shengyu: "",
                                userInfo: "",
                                chuydate: "选择日期",
                                chuytime: "选择时间",
                                num: {},
                                duogg: [],
                                xz_num: [],
                                couponprice: 0,
                                jqdjg: "请选择",
                                yhqid: "0",
                                oldsfje: "",
                                pagedata: {},
                                imgcount_xz: 0,
                                pagedata_set: [],
                                zhixin: !1,
                                xuanz: 0,
                                lixuanz: -1,
                                ttcxs: 0,
                                chooseNum: 0,
                                myscore: 0,
                                jifen_u: 2,
                                zf_money: 0,
                                dkmoney: 0,
                                type: "",
                                testKey: "",
                                testKeys: "",
                                order_id: "",
                                pd_val: [],
                                formId: "",
                                sw: !1,
                                showModalStatus: !1,
                                zf_type: 0,
                                mymoney: 0,
                                show_pay_type: 0,
                                h5pay: 0,
                                dopay: "",
                                pay_type: 1,
                                alipay: 1,
                                wxpay: 1,
                                needBind: !1,
                                needAuth: !1,
                                dkscore: 0,
                                xx: [],
                                arrs: [],
                                wxmobile: "",
                                myname: "",
                                mymobile: "",
                                myaddress: "",
                                discounts: 0,
                                ischecked: !1,
                                storeall: "",
                                storeid: "",
                                is_submit: 1
                            };
                        },
                        onLoad: function (e) {
                            var t = this,
                                a = this,
                                n = e.id;
                            a.id = n, null != e.testPrice && (a.testPrice = e.testPrice), null != e.testKey && (a.testKey = e.testKey),
                                null != e.testKeys && (a.testKeys = e.testKeys), "table" == e.type && (a.type = e.type,
                                    a.NowSelectStr = e.NowSelectStr, a.appoint_date = e.appoint_date);
                            var i = 0;
                            e.fxsid && (i = e.fxsid), this.fxsid = i, a.refreshSessionkey(), this._baseMin(this),
                                o.getOpenid(i, function () {
                                    var e = a.id;
                                    a.getShowPic(e);
                                }, function () {
                                    t.needAuth = !0;
                                }, function () {
                                    t.needBind = !0;
                                });
                        },
                        methods: {
                            closeAuth: function () {
                                this.needAuth = !1, this._checkBindPhone(this);
                            },
                            closeBind: function () {
                                this.needBind = !1, this.getShowPic(this.id);
                            },
                            getStore: function (e) {
                                this.storeid = e.currentTarget.dataset.id;
                            },
                            getShowPic: function (e) {
                                var x = this;
                                P.getStorageSync("openid"), P.request({
                                    url: x.$baseurl + "doPagemycoupon",
                                    data: {
                                        suid: P.getStorageSync("suid"),
                                        flag: 0,
                                        uniacid: x.$uniacid,
                                        id: x.id,
                                        type: "yuyue"
                                    },
                                    success: function (e) {
                                        x.couponlist = e.data.data;
                                    },
                                    fail: function (e) {}
                                }), P.request({
                                    url: x.$baseurl + "doPageshowPro",
                                    data: {
                                        id: e,
                                        suid: P.getStorageSync("suid"),
                                        uniacid: x.$uniacid
                                    },
                                    cachetime: "30",
                                    success: function (e) {
                                        var t = e.data.data.userinfo.grade,
                                            a = e.data.data.stores;
                                        "" != a && (x.storeall = a, x.storeid = a[0].id);
                                        var n = e.data.data.discount_status;
                                        if (0 < t && 0 < n) {
                                            var i = e.data.data.discount;
                                            if (2 == n) {
                                                if (0 < i.length)
                                                    for (var o = 0; o < i.length; o++)
                                                        if (t == i[o].grade) {
                                                            var s = i[o].discount;
                                                            break;
                                                        }
                                            } else s = i;
                                        } else s = 0;
                                        var r = e.data.data.userinfo.money,
                                            d = e.data.data.userinfo.score;
                                        if (e.data.data.price, "table" != x.type) {
                                            var u = x.yhje;
                                            if (0 == e.data.data.pro_xz) c = 1;
                                            else var c = e.data.data.pro_xz - e.data.data.my_num;
                                            var l = e.data.data.more_type_x,
                                                f = [],
                                                h = {},
                                                y = 0;
                                            for (o = 0; o < l.length; o++) h[o] = 0, f.push(h), y += 0 * l[o][1];
                                            x.bg = e.data.data.text[0], x.picList = e.data.data.text, x.title = e.data.data.title,
                                                x.datas = e.data.data, x.duogg = l, x.hjjg = y, x.zf_money = y, x.dprice = y, x.sfje = y - u,
                                                x.myscore = parseFloat(e.data.data.userinfo.score), x.mymoney = parseFloat(e.data.data.userinfo.money),
                                                x.xz_num = e.data.data.more_type_num, x.num = f, x.xg_num = e.data.data.pro_xz,
                                                x.shengyu = e.data.data.pro_kc, x.xg_buy = c, x.pagedata = e.data.data.forms, x.formdescs = e.data.data.formdescs,
                                                x.discounts = s, "" != x.testKey && x.jia();
                                            var p = x.testKeys;
                                            if (console.log(p), 1 == p && 0 < l.length)
                                                for (o = 0; o < l.length; o++) x.testPrice = l[o][1],
                                                    x.testKey = o, x.jia();
                                        } else {
                                            u = x.yhje;
                                            var g, m, v = x.NowSelectStr.split(","),
                                                _ = v.length;
                                            for (o = 0; o < _; o++) g = v[o].split("a"), m = e.data.data.table.rowstr[g[0] - 1] + "，",
                                                m += e.data.data.table.columnstr[g[1] - 1], v[o] = m;
                                            x.myscore = d, x.mymoney = r, x.select_arr = v, x.bg = e.data.data.text[0], x.title = e.data.data.title,
                                                x.datas = e.data.data, x.hjjg = e.data.data.price * _, x.zf_money = e.data.data.price * _,
                                                x.dprice = e.data.data.price, x.select_num = _, x.sfje = Math.floor(100 * (e.data.data.price * _ - u)) / 100,
                                                x.pagedata = e.data.data.forms, x.formdescs = e.data.data.formdescs, x.discounts = s;
                                        }
                                        P.setNavigationBarTitle({
                                                title: x.title
                                            }), P.setStorageSync("isShowLoading", !1), P.hideNavigationBarLoading(), P.stopPullDownRefresh(),
                                            P.request({
                                                url: x.$baseurl + "doPagegetmoneyoff",
                                                data: {
                                                    uniacid: x.$uniacid
                                                },
                                                success: function (e) {
                                                    for (var t = e.data.data, a = "", n = 0; n < t.length; n++) n == t.length - 1 ? a += "满" + t[n].reach + "减" + t[n].del : a += "满" + t[n].reach + "减" + t[n].del + "，";
                                                    x.sfje, x.moneyoff = t, x.moneyoffstr = t ? a : "", x.getmyinfo();
                                                }
                                            });
                                    }
                                });
                            },
                            getmyinfo: function () {
                                var e = this,
                                    t = (P.getStorageSync("openid"), e.moneyoff),
                                    a = e.hjjg,
                                    n = e.discounts;
                                if (0 < n && 0 < a)
                                    if ((a = a * n / 10) < .01) a = .01;
                                    else {
                                        var i = String(a).indexOf(".") + 1;
                                        if (2 < String(a).length - i) {
                                            var o = a.toFixed(2);
                                            a = .005 != (a.toFixed(3) - o).toFixed(3) ? o : 1 * o + .01;
                                        } else a = a.toFixed(2);
                                    }
                                if (t)
                                    for (var s = t.length - 1; 0 <= s; s--)
                                        if (a >= parseFloat(t[s].reach)) {
                                            a -= parseFloat(t[s].del);
                                            break;
                                        }
                                e.sfje = a, e.zf_type = parseFloat(e.mymoney) >= a ? 0 : 1, e.zf_money = parseFloat(e.mymoney) >= a ? a : Math.round(100 * (a - e.mymoney)) / 100;
                            },
                            jian: function (e) {
                                var t = this,
                                    a = (t.yhje, e.currentTarget.dataset.testid),
                                    n = e.currentTarget.dataset.testkey,
                                    i = t.num[n][n],
                                    o = (t.duogg,
                                        t.sfje),
                                    s = t.oldsfje,
                                    r = t.hjjg;
                                if (--i < 0) i = 0;
                                else {
                                    var d = Math.round(100 * s - 100 * a * i + 100 * a * (i - 1)) / 100;
                                    s = r = o = d;
                                    var u = t.num;
                                    u[n][n] = i;
                                    var c = t.chooseNum - 1;
                                    t.num = u, t.sfje = o, t.hjjg = r, t.jqdjg = "请选择", t.oldsfje = s, t.yhqid = 0,
                                        t.chooseNum = c, t.ischecked = !1, t.dkscore = 0, t.dkmoney = 0, t.jifen_u = 2,
                                        t.getmyinfo();
                                }
                            },
                            jia: function (e) {
                                var t = this;
                                if (t.yhje, null == e) {
                                    var a = t.testKey;
                                    if ("" !== a) var n = t.testPrice;
                                } else n = e.currentTarget.dataset.testid, a = e.currentTarget.dataset.testkey;
                                var i = t.num[a][a],
                                    o = (t.duogg, t.sfje, t.oldsfje),
                                    s = t.hjjg;
                                if (t.xz_num[a].shennum < ++i) return i--, P.showModal({
                                    title: "提醒",
                                    content: "库存量不足！",
                                    showCancel: !1
                                }), !1;
                                var r = Math.round(100 * n * i + 100 * o - 100 * n * (i - 1)) / 100;
                                s = o = r;
                                var d = t.num;
                                t.datas, d[a][a] = i;
                                var u = t.chooseNum + 1;
                                t.num = d, t.hjjg = s, t.jqdjg = "请选择", t.oldsfje = o, t.yhqid = 0, t.chooseNum = u,
                                    t.ischecked = !1, t.dkscore = 0, t.dkmoney = 0, t.jifen_u = 2, t.getmyinfo();
                            },
                            changealipay: function () {
                                this.pay_type = 1;
                            },
                            changewxpay: function () {
                                this.pay_type = 2;
                            },
                            close_pay_type: function () {
                                this.show_pay_type = 0, P.navigateTo({
                                    url: "/pagesReserve/orderList/orderList"
                                });
                            },
                            h5topay: function () {
                                var e = this.pay_type;
                                1 == e ? this.pay3(this.orderid) : 2 == e && this.pay5(this.orderid), this.show_pay_type = 0;
                            },
                            pay3: function (t) {
                                var e;
                                console.log("支付宝H5支付"), P.request({
                                    url: this.$baseurl + "doPageHfivepay",
                                    data: (e = {
                                        uniacid: this.$uniacid,
                                        order_id: t,
                                        suid: P.getStorageSync("suid"),
                                        types: 9
                                    }, a(e, "suid", P.getStorageSync("suid")), a(e, "money", this.zf_money), e),
                                    success: function (e) {
                                        1 == e.data.flag && P.navigateTo({
                                            url: "/pages/aliH5pay/aliH5pay?id=" + t + "&type=9"
                                        });
                                    }
                                });
                            },
                            pay5: function (t) {
                                console.log("微信H5支付"), P.request({
                                    url: this.$baseurl + "doPageHfiveWxpay",
                                    data: {
                                        uniacid: this.$uniacid,
                                        order_id: t,
                                        types: "reserve",
                                        suid: P.getStorageSync("suid"),
                                        money: this.zf_money
                                    },
                                    success: function (e) {
                                        e.data && (console.log(e.data), P.setStorageSync(t, e.data.mweb_url), P.navigateTo({
                                            url: "/pages/wxH5pay/wxH5pay?order_id=" + t
                                        }));
                                    }
                                });
                            },
                            userNameInput: function (e) {
                                this.pro_name = e.detail.value;
                            },
                            userTelInput: function (e) {
                                this.pro_tel = e.detail.value;
                            },
                            userAddInput: function (e) {
                                this.pro_address = e.detail.value;
                            },
                            userTextInput: function (e) {
                                this.pro_txt = e.detail.value;
                            },
                            submit: o.throttle(function (e) {
                                var a = this;
                                if (2 == this.is_submit) return !1;
                                var t = a.jhsl,
                                    n = a.shengyu,
                                    i = a.type,
                                    o = e.detail.formId;
                                if (a.formId = o, n < t && -1 != n && "table" != i) return t--, P.showModal({
                                    title: "提醒",
                                    content: "库存量不足！",
                                    showCancel: !1
                                }), !1;
                                var s = a.sfje,
                                    r = P.getStorageSync("openid"),
                                    d = (a.duogg, a.num),
                                    u = a.chuydate,
                                    c = a.chuytime,
                                    l = (a.yhje,
                                        a.id),
                                    f = a.order,
                                    h = a.pro_name,
                                    y = a.pro_tel,
                                    p = a.pro_address,
                                    g = a.pro_txt,
                                    m = (a.id,
                                        a.yhqid),
                                    v = !0,
                                    _ = a.hjjg;
                                if (0 == ("table" == i ? a.select_num : a.chooseNum)) return v = !1, P.showModal({
                                    title: "提醒",
                                    content: "您至少要选择1个产品或服务",
                                    showCancel: !1
                                }), !1;
                                if (!h && 2 == a.datas.pro_flag) return v = !1, P.showModal({
                                    title: "提醒",
                                    content: "姓名为必填！",
                                    showCancel: !1
                                }), !1;
                                if (!y && 2 == a.datas.pro_flag_tel) return v = !1, P.showModal({
                                    title: "提醒",
                                    content: "手机号为必填！",
                                    showCancel: !1
                                }), !1;
                                if (!/^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(16[0-9]{1})|(17[0-9]{1})|(18[0-9]{1})|(19[0-9]{1}))+\d{8})$/.test(y) && 2 == a.datas.pro_flag_tel) return P.showModal({
                                    title: "提醒",
                                    content: "请输入有效的手机号码！",
                                    showCancel: !1
                                }), !1;
                                if (!p && 2 == a.datas.pro_flag_add) return v = !1, P.showModal({
                                    title: "提醒",
                                    content: "地址为必填！",
                                    showCancel: !1
                                }), !1;
                                if (0 == a.datas.tableis) {
                                    if ("选择日期" == u && 2 == a.datas.pro_flag_data) return v = !1, P.showModal({
                                        title: "提醒",
                                        content: "请选择日期！",
                                        showCancel: !1
                                    }), !1;
                                    if ("选择时间" == c && 2 == a.datas.pro_flag_time) return v = !1, P.showModal({
                                        title: "提醒",
                                        content: "请选择时间！",
                                        showCancel: !1
                                    }), !1;
                                }
                                for (var x = a.pagedata, w = 0; w < x.length; w++) {
                                    if (1 == x[w].ismust)
                                        if (5 == x[w].type) {
                                            if ("" == x[w].z_val) return v = !1, P.showModal({
                                                title: "提醒",
                                                content: x[w].name + "为必填项！",
                                                showCancel: !1
                                            }), !1;
                                        } else {
                                            if ("" == x[w].val) return v = !1, P.showModal({
                                                title: "提醒",
                                                content: x[w].name + "为必填项！",
                                                showCancel: !1
                                            }), !1;
                                            if (0 == x[w].type && 1 == x[w].tp_text[0].yval) {
                                                if (!/^1[3456789]{1}\d{9}$/.test(x[w].val)) return P.showModal({
                                                    title: "提醒",
                                                    content: "请您输入正确的手机号码",
                                                    showCancel: !1
                                                }), !1;
                                            } else if (0 == x[w].type && 7 == x[w].tp_text[0].yval) {
                                                if (!/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test(x[w].val)) return P.showModal({
                                                    title: "提醒",
                                                    content: "请您输入正确的身份证号",
                                                    showCancel: !1
                                                }), !1;
                                            }
                                        }
                                    if (5 == x[w].type && 0 < x[w].z_val.length)
                                        for (var j = 0; j < x[w].z_val.length; j++) {
                                            var b = x[w].z_val[j].substr(x[w].z_val[j].indexOf("/upimages"));
                                            x[w].z_val[j] = b;
                                        }
                                }
                                if (this.is_submit = 2, v) {
                                    var S = "",
                                        z = "",
                                        k = "",
                                        M = "";
                                    if ("" == a.storeall) S = a.baseinfo.name, z = a.baseinfo.tel, k = a.baseinfo.address;
                                    else {
                                        var T = a.storeid,
                                            $ = a.storeall;
                                        for (w = 0; w < $.length; w++)
                                            if (T == $[w].id) {
                                                S = $[w].title, z = $[w].tel, k = $[w].province + $[w].city + $[w].country, M = $[w].times;
                                                break;
                                            }
                                    }
                                    P.request({
                                        url: a.$baseurl + "doPagecreateorder",
                                        data: {
                                            types: "reserve",
                                            suid: P.getStorageSync("suid"),
                                            num: JSON.stringify(d[0]),
                                            id: l,
                                            hjjg: _,
                                            zhifu: s,
                                            zf_money: a.zf_money,
                                            zf_type: a.zf_type,
                                            order: f,
                                            pro_name: h,
                                            pro_tel: y,
                                            pro_address: p,
                                            pro_txt: g,
                                            chuydate: u,
                                            chuytime: c,
                                            yhqid: m,
                                            type: "table" == a.type ? "table" : "",
                                            NowSelectStr: a.NowSelectStr,
                                            appoint_date: a.appoint_date,
                                            dkscore: a.dkscore,
                                            dkmoney: a.dkmoney,
                                            pagedata: JSON.stringify(x),
                                            uniacid: a.$uniacid,
                                            fid: a.datas.formset,
                                            source: P.getStorageSync("source"),
                                            openid: r,
                                            discounts: a.discounts,
                                            store_name: S,
                                            store_tel: z,
                                            store_address: k,
                                            store_hours: M
                                        },
                                        header: {
                                            "content-type": "application/x-www-form-urlencoded"
                                        },
                                        success: function (e) {
                                            if ("1" == e.data.data.errcode) return P.showModal({
                                                title: e.data.data.err,
                                                content: "请重新下单",
                                                showCancel: !1
                                            }), !1;
                                            if ("3" == e.data.data.errcode) return P.showModal({
                                                title: e.data.data.err,
                                                content: "当前库存为" + e.data.data.kc + "件",
                                                showCancel: !1
                                            }), !1;
                                            var t = e.data.data.orderid;
                                            a.orderid = t, s <= a.mymoney ? a.pay1(t) : a.pay2(t);
                                        }
                                    });
                                }
                            }, 2e3),
                            showpay: function () {
                                this.money = this.zf_money - this.mymoney;
                                var t = this;
                                P.request({
                                    url: this.$baseurl + "doPageGetH5payshow",
                                    data: {
                                        uniacid: this.$uniacid,
                                        suid: P.getStorageSync("suid")
                                    },
                                    success: function (e) {
                                        0 == e.data.data.ali && 0 == e.data.data.wx ? P.showModal({
                                            title: "提示",
                                            content: "请联系管理员设置支付参数",
                                            showCancel: !1,
                                            success: function (e) {
                                                return !1;
                                            }
                                        }) : (0 == e.data.data.ali ? (t.alipay = 0, t.pay_type = 2) : (t.alipay = 1, t.pay_type = 1),
                                            0 == e.data.data.wx ? t.wxpay = 0 : t.wxpay = 1, t.show_pay_type = 1);
                                    }
                                });
                            },
                            bindDateChange2: function (e) {
                                this.chuydate = e.detail.value;
                            },
                            bindTimeChange2: function (e) {
                                this.chuytime = e.detail.value;
                            },
                            getmoney: function (e) {
                                var t = this,
                                    a = e.currentTarget.dataset.price,
                                    n = e.currentTarget.dataset.index,
                                    i = n.pay_money,
                                    o = n.id,
                                    s = t.discounts;
                                if (0 == s) var r = t.hjjg;
                                else 0 < s && (r = (t.hjjg * s).toFixed(2));
                                if (1 * r < 1 * i) P.showModal({
                                    title: "提示",
                                    content: "价格未满" + i + "元，不可使用该优惠券！",
                                    showCancel: !1
                                });
                                else {
                                    var d = (100 * t.sfje - 100 * parseFloat(a) + 100 * t.dkmoney) / 100;
                                    t.ischecked = !1, t.switchChange("false");
                                    var u = parseFloat(d.toPrecision(12));
                                    u < 0 && (u = 0), t.jqdjg = a, console.log(a), t.yhqid = o, t.sfje = u, t.oldsfje = r,
                                        t.zf_type = parseFloat(t.mymoney) >= parseFloat(u) ? 0 : 1, t.zf_money = parseFloat(t.mymoney) >= parseFloat(u) ? parseFloat(u) : Math.round(100 * (parseFloat(u) - parseFloat(t.mymoney))) / 100;
                                }
                                t.hideModal();
                            },
                            qxyh: function () {
                                var e = this,
                                    t = e.jqdjg;
                                "请选择" == t && (t = 0);
                                var a = e.sfje,
                                    n = Math.round(100 * a + 100 * t) / 100;
                                e.hideModal(), e.jqdjg = 0, e.yhqid = 0, e.sfje = n, e.jqdjg = "请选择";
                            },
                            showModal: function () {
                                var e = this;
                                this.showModalStatus = !0;
                                var t = P.createAnimation({
                                    duration: 200,
                                    timingFunction: "linear",
                                    delay: 0
                                });
                                (this.animation = t).translateY(300).step(), this.animationData = t.export(), setTimeout(function () {
                                    t.translateY(0).step(), this.animationData = t.export();
                                }.bind(this), 200);
                                var a = e.jqdjg;
                                if (0 < a) {
                                    var n = (100 * parseFloat(e.sfje) + 100 * parseFloat(a)) / 100;
                                    e.jqdjg = 0, e.sfje = n, e.zf_type = e.mymoney >= n ? 0 : 1, e.zf_money = e.mymoney >= n ? n : Math.round(100 * (n - e.mymoney)) / 100;
                                }
                            },
                            hideModal: function () {
                                var e = wx.createAnimation({
                                    duration: 200,
                                    timingFunction: "linear",
                                    delay: 0
                                });
                                (this.animation = e).translateY(300).step(), this.animationData = e.export(), setTimeout(function () {
                                    e.translateY(0).step(), this.animationData = e.export(), this.showModalStatus = !1;
                                }.bind(this), 200);
                            },
                            bindInputChange: function (e) {
                                var t = e.detail.value,
                                    a = e.currentTarget.dataset.index,
                                    n = this.pagedata;
                                n[a].val = t, this.pagedata = n;
                            },
                            bindPickerChange: function (e) {
                                var t = e.detail.value,
                                    a = e.currentTarget.dataset.index,
                                    n = this.pagedata,
                                    i = n[a].tp_text[t];
                                n[a].val = i, this.pagedata = n;
                            },
                            bindDateChange: function (e) {
                                var t = e.detail.value,
                                    a = e.currentTarget.dataset.index,
                                    n = this.pagedata;
                                n[a].val = t, this.pagedata = n;
                            },
                            bindTimeChange: function (e) {
                                var t = e.detail.value,
                                    a = e.currentTarget.dataset.index,
                                    n = this.pagedata;
                                n[a].val = t, this.pagedata = n;
                            },
                            checkboxChange: function (e) {
                                var t = e.detail.value,
                                    a = e.currentTarget.dataset.index,
                                    n = this.pagedata;
                                n[a].val = t, this.pagedata = n;
                            },
                            radioChange: function (e) {
                                var t = e.detail.value,
                                    a = e.currentTarget.dataset.index,
                                    n = this.pagedata;
                                n[a].val = t, this.pagedata = n;
                            },
                            choiceimg: function (e) {
                                var o = this,
                                    t = 0,
                                    s = o.zhixin,
                                    r = e.currentTarget.dataset.index,
                                    d = o.pagedata,
                                    a = d[r].val,
                                    n = d[r].tp_text[0];
                                a ? t = a.length : (t = 0, a = []);
                                var i = n - t,
                                    u = o.$baseurl + "wxupimg";
                                o.pd_val, P.chooseImage({
                                    count: i,
                                    sizeType: ["original", "compressed"],
                                    sourceType: ["album", "camera"],
                                    success: function (e) {
                                        s = !0, o.zhixin = s, P.showLoading({
                                            title: "图片上传中"
                                        });
                                        var a = e.tempFilePaths,
                                            n = 0,
                                            i = a.length;
                                        ! function t() {
                                            P.uploadFile({
                                                url: u,
                                                filePath: a[n],
                                                name: "file",
                                                success: function (e) {
                                                    d[r].z_val.push(e.data), o.pagedata = d, ++n < i ? t() : (s = !1, o.zhixin = s,
                                                        P.hideLoading());
                                                }
                                            });
                                        }();
                                    }
                                });
                            },
                            delimg: function (e) {
                                var t = e.currentTarget.dataset.index,
                                    a = e.currentTarget.dataset.id,
                                    n = this.pagedata,
                                    i = n[t].z_val;
                                i.splice(a, 1), 0 == i.length && (i = ""), n[t].z_val = i, this.pagedata = n;
                            },
                            namexz: function (e) {
                                for (var t = this, a = e.currentTarget.dataset.index, n = t.pagedata[a], i = [], o = 0; o < n.tp_text.length; o++) {
                                    var s = {};
                                    s.keys = n.tp_text[o].yval, s.val = 1, i.push(s);
                                }
                                t.ttcxs = 1, t.formindex = a, t.xx = i, t.xuanz = 0, t.lixuanz = -1, t.riqi();
                            },
                            riqi: function () {
                                for (var e = this, t = new Date(), a = new Date(t.getTime()), n = a.getFullYear() + "-" + (a.getMonth() + 1) + "-" + a.getDate(), i = e.xx, o = 0; o < i.length; o++) i[o].val = 1;
                                e.xx = i, e.gettoday(n);
                                var s = [],
                                    r = [],
                                    d = new Date();
                                for (o = 0; o < 5; o++) {
                                    var u = new Date(d.getTime() + 24 * o * 3600 * 1e3),
                                        c = u.getFullYear(),
                                        l = u.getMonth() + 1,
                                        f = u.getDate(),
                                        h = l + "月" + f + "日",
                                        y = c + "-" + l + "-" + f;
                                    s.push(h), r.push(y);
                                }
                                e.arrs = s, e.fallarrs = r, e.today = n;
                            },
                            xuanzd: function (e) {
                                for (var t = this, a = e.currentTarget.dataset.index, n = t.fallarrs[a], i = t.xx, o = 0; o < i.length; o++) i[o].val = 1;
                                t.xuanz = a, t.today = n, t.lixuanz = -1, t.xx = i, t.gettoday(n);
                            },
                            goux: function (e) {
                                var t = e.currentTarget.dataset.index;
                                this.lixuanz = t;
                            },
                            gettoday: function (e) {
                                var i = this,
                                    t = i.id,
                                    a = i.formindex,
                                    o = i.xx;
                                P.request({
                                    url: i.$baseurl + "doPageDuzhan",
                                    data: {
                                        id: t,
                                        types: "showPro_lv_buy",
                                        days: e,
                                        pagedatekey: a,
                                        uniacid: i.$uniacid
                                    },
                                    header: {
                                        "content-type": "application/json"
                                    },
                                    success: function (e) {
                                        for (var t = e.data.data, a = 0; a < t.length; a++) o[t[a]].val = 2;
                                        var n = 0;
                                        t.length == o.length && (n = 1), i.xx = o, i.isover = n;
                                    }
                                });
                            },
                            save: function () {
                                var e = this,
                                    t = e.today,
                                    a = e.xx,
                                    n = e.lixuanz;
                                if (-1 == n) return P.showModal({
                                    title: "提现",
                                    content: "请选择预约的选项",
                                    showCancel: !1
                                }), !1;
                                var i = "已选择" + t + "，" + a[n].keys,
                                    o = e.pagedata,
                                    s = e.formindex;
                                o[s].val = i, o[s].days = t, o[s].indexkey = s, o[s].xuanx = n, e.ttcxs = 0, e.pagedata = o;
                            },
                            quxiao: function () {
                                this.ttcxs = 0;
                            },
                            weixinadd: function () {
                                var s = this;
                                P.chooseAddress({
                                    success: function (e) {
                                        for (var t = e.provinceName + " " + e.cityName + " " + e.countyName + " " + e.detailInfo, a = e.userName, n = e.telNumber, i = s.pagedata, o = 0; o < i.length; o++) 0 == i[o].type && 2 == i[o].tp_text[0].yval && (i[o].val = a),
                                            0 == i[o].type && 3 == i[o].tp_text[0].yval && (i[o].val = n), 0 == i[o].type && 4 == i[o].tp_text[0].yval && (i[o].val = t);
                                        s.myname = a, s.mymobile = n, s.myaddress = t, s.pagedata = i;
                                    },
                                    fail: function (e) {
                                        P.getSetting({
                                            success: function (e) {
                                                e.authSetting["scope.address"] || P.openSetting({
                                                    success: function (e) {}
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            switchChange: function (e) {
                                var o = this;
                                if ("false" == e) var t = !1;
                                else t = !o.ischecked;
                                P.getStorageSync("suid");
                                var s = o.sfje,
                                    r = 0,
                                    a = "table" == o.type ? o.select_num : o.chooseNum;
                                if (1 == t) P.request({
                                    url: o.$baseurl + "doPagescoreDeduction",
                                    data: {
                                        id: o.id,
                                        num: a,
                                        suid: P.getStorageSync("suid"),
                                        is_more: 1,
                                        uniacid: o.$uniacid
                                    },
                                    header: {
                                        "content-type": "application/json"
                                    },
                                    success: function (e) {
                                        var t = e.data;
                                        r = t.moneycl;
                                        var a = t.gzmoney,
                                            n = t.gzscore;
                                        if (s < r && (r = parseInt(s)), 0 == r) var i = 0;
                                        else i = r * n / a;
                                        s = Math.round(100 * (s - r)) / 100, o.sfje = s, o.dkmoney = r, o.dkscore = i, o.jifen_u = 1,
                                            o.zf_type = o.mymoney >= s ? 0 : 1, o.zf_money = o.mymoney >= s ? s : Math.round(100 * (s - o.mymoney)) / 100,
                                            o.ischecked = !0;
                                    }
                                });
                                else {
                                    var n = o.zf_money;
                                    r = o.dkmoney, n = Math.round(100 * n + 100 * r) / 100, s = n, o.dkmoney = 0, o.dkscore = 0,
                                        o.jifen_u = 0, o.zf_money = n, o.sfje = s, o.ischecked = !1;
                                }
                            },
                            pay1: function (t) {
                                var a = this;
                                P.showModal({
                                    title: "请注意",
                                    content: "您将使用余额支付" + a.sfje + "元",
                                    success: function (e) {
                                        e.confirm ? (a.payover_do(t), P.showLoading({
                                            title: "下单中...",
                                            mask: !0
                                        }), setTimeout(function () {
                                            P.hideLoading();
                                        }, 3e3)) : P.redirectTo({
                                            url: "/pagesReserve/orderList/orderList"
                                        });
                                    },
                                    fail: function (e) {
                                        P.redirectTo({
                                            url: "/pagesReserve/orderList/orderList"
                                        });
                                    }
                                });
                            },
                            pay2: function (t) {
                                var a = this,
                                    e = (P.getStorageSync("openid"), a.sfje),
                                    n = a.zf_money,
                                    i = a.hjjg;
                                P.request({
                                    url: a.$baseurl + "doPagebeforepay",
                                    data: {
                                        openid: P.getStorageSync("openid"),
                                        suid: P.getStorageSync("suid"),
                                        price: i,
                                        pay_price: n,
                                        true_price: e,
                                        order_id: t,
                                        types: "reserve",
                                        formId: a.formId,
                                        uniacid: a.$uniacid
                                    },
                                    header: {
                                        "content-type": "application/json"
                                    },
                                    success: function (e) {
                                        -1 != [1, 2, 3, 4].indexOf(e.data.data.err) && P.showModal({
                                            title: "支付失败",
                                            content: e.data.data.message,
                                            showCancel: !1
                                        }), 0 == e.data.data.err && (P.request({
                                            url: a.$baseurl + "doPagesavePrepayid",
                                            data: {
                                                types: "reserve",
                                                order_id: t,
                                                prepayid: e.data.data.package,
                                                uniacid: a.$uniacid
                                            },
                                            success: function (e) {},
                                            fail: function (e) {}
                                        }), P.requestPayment({
                                            timeStamp: e.data.data.timeStamp,
                                            nonceStr: e.data.data.nonceStr,
                                            package: e.data.data.package,
                                            signType: "MD5",
                                            paySign: e.data.data.paySign,
                                            success: function (e) {
                                                P.showToast({
                                                    title: "支付成功",
                                                    icon: "success",
                                                    mask: !0,
                                                    duration: 3e3,
                                                    success: function (e) {
                                                        P.showToast({
                                                            title: "购买成功！",
                                                            icon: "success",
                                                            success: function () {
                                                                P.navigateTo({
                                                                    url: "/pagesReserve/orderList/orderList"
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            },
                                            fail: function (e) {
                                                P.navigateTo({
                                                    url: "/pagesReserve/orderList/orderList"
                                                });
                                            },
                                            complete: function (e) {}
                                        }));
                                    }
                                });
                            },
                            payover_do: function (e) {
                                var t = this.sfje,
                                    a = P.getStorageSync("openid");
                                P.request({
                                    url: this.$baseurl + "doPagepaynotify",
                                    data: {
                                        out_trade_no: e,
                                        suid: P.getStorageSync("suid"),
                                        openid: a,
                                        source: P.getStorageSync("source"),
                                        payprice: t,
                                        types: "reserve",
                                        flag: 0,
                                        formId: this.formId,
                                        uniacid: this.$uniacid
                                    },
                                    success: function (e) {
                                        P.showToast({
                                            title: "购买成功！",
                                            icon: "success",
                                            mask: !0,
                                            success: function () {
                                                P.navigateTo({
                                                    url: "/pagesReserve/orderList/orderList"
                                                });
                                            }
                                        });
                                    }
                                });
                            },
                            refreshSessionkey: function () {
                                var t = this;
                                P.login({
                                    success: function (e) {
                                        P.request({
                                            url: t.$baseurl + "doPagegetNewSessionkey",
                                            data: {
                                                uniacid: t.$uniacid,
                                                code: e.code
                                            },
                                            success: function (e) {
                                                t.newSessionKey = e.data.data;
                                            }
                                        });
                                    }
                                });
                            },
                            getPhoneNumber: function (e) {
                                var n = this,
                                    t = e.detail.iv,
                                    a = e.detail.encryptedData;
                                "getPhoneNumber:ok" == e.detail.errMsg ? P.checkSession({
                                    success: function () {
                                        P.request({
                                            url: n.$baseurl + "doPagejiemiNew",
                                            data: {
                                                uniacid: n.$uniacid,
                                                newSessionKey: n.newSessionKey,
                                                iv: t,
                                                encryptedData: a
                                            },
                                            success: function (e) {
                                                if (e.data.data) {
                                                    for (var t = n.pagedata, a = 0; a < t.length; a++) 0 == t[a].type && 5 == t[a].tp_text[0].yval && (t[a].val = e.data.data);
                                                    n.wxmobile = e.data.data, n.pagedata = t;
                                                } else P.showModal({
                                                    title: "提示",
                                                    content: "sessionKey已过期，请下拉刷新！"
                                                });
                                            },
                                            fail: function (e) {}
                                        });
                                    },
                                    fail: function () {
                                        P.showModal({
                                            title: "提示",
                                            content: "sessionKey已过期，请下拉刷新！"
                                        });
                                    }
                                }) : P.showModal({
                                    title: "提示",
                                    content: "请先授权获取您的手机号！",
                                    showCancel: !1
                                });
                            }
                        }
                    };
                t.default = e;
            }).call(this, n("543d").default);
        },
        "4ffb": function (e, t, a) {
            a.r(t);
            var n = a("c8ee"),
                i = a("a657");
            for (var o in i) "default" !== o && function (e) {
                a.d(t, e, function () {
                    return i[e];
                });
            }(o);
            a("aacb");
            var s = a("2877"),
                r = Object(s.a)(i.default, n.a, n.b, !1, null, null, null);
            t.default = r.exports;
        },
        a657: function (e, t, a) {
            a.r(t);
            var n = a("3800"),
                i = a.n(n);
            for (var o in n) "default" !== o && function (e) {
                a.d(t, e, function () {
                    return n[e];
                });
            }(o);
            t.default = i.a;
        },
        aacb: function (e, t, a) {
            var n = a("300e");
            a.n(n).a;
        },
        bfd6: function (e, t, a) {
            (function (e) {
                function t(e) {
                    return e && e.__esModule ? e : {
                        default: e
                    };
                }
                a("020c"), a("921b"), t(a("66fd")), e(t(a("4ffb")).default);
            }).call(this, a("543d").createPage);
        },
        c8ee: function (e, t, a) {
            var n = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            a.d(t, "a", function () {
                return n;
            }), a.d(t, "b", function () {
                return i;
            });
        }
    },
    [
        ["bfd6", "common/runtime", "common/vendor"]
    ]
]);