(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesShake/index/index"], {
        "0d8a": function (e, i, t) {
            (function (e) {
                function i(e) {
                    return e && e.__esModule ? e : {
                        default: e
                    };
                }
                t("020c"), t("921b"), i(t("66fd")), e(i(t("77a5")).default);
            }).call(this, t("543d").createPage);
        },
        "0e4c": function (e, i, t) {
            var a = t("36c6");
            t.n(a).a;
        },
        "36c6": function (e, i, t) {},
        "4ae1": function (e, i, t) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                n = [];
            t.d(i, "a", function () {
                return a;
            }), t.d(i, "b", function () {
                return n;
            });
        },
        "77a5": function (e, i, t) {
            t.r(i);
            var a = t("4ae1"),
                n = t("77b7b");
            for (var s in n) "default" !== s && function (e) {
                t.d(i, e, function () {
                    return n[e];
                });
            }(s);
            t("0e4c");
            var o = t("2877"),
                r = Object(o.a)(n.default, a.a, a.b, !1, null, null, null);
            i.default = r.exports;
        },
        "77b7b": function (e, i, t) {
            t.r(i);
            var a = t("f5c9"),
                n = t.n(a);
            for (var s in a) "default" !== s && function (e) {
                t.d(i, e, function () {
                    return a[e];
                });
            }(s);
            i.default = n.a;
        },
        f5c9: function (e, t, o) {
            (function (n) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var e, a = (e = o("3584")) && e.__esModule ? e : {
                    default: e
                };
                var s = o("f571");

                function l(e, i) {
                    var t, a, n, s, o, r, d, c, u = (t = i, a = Math.floor(t / 1e3), n = Math.floor(a / 3600),
                        s = Math.floor(n / 24), o = f(n % 24), r = f(Math.floor((a - 3600 * n) / 60)), d = f(a - 3600 * n - 60 * r),
                        f(Math.floor(t % 1e3 / 10)), (c = new Array())[0] = s, c[1] = o, c[2] = r, c[3] = d,
                        c);
                    e.day = u[0], e.hour = u[1], e.min = u[2], e.sec = u[3], i <= 0 ? e.clock = "已经截止" : setTimeout(function () {
                        l(e, i -= 1e3);
                    }, 1e3);
                }

                function f(e) {
                    return e < 10 ? "0" + e : e;
                }
                var i = {
                    data: function () {
                        return {
                            $imgurl: this.$imgurl,
                            last_index: 0,
                            amplification_index: 0,
                            roll_flag: !0,
                            max_number: 8,
                            speed: 300,
                            finalindex: 0,
                            myInterval: "",
                            max_speed: 40,
                            minturns: 12,
                            runs_now: 0,
                            flag: !1,
                            is_button: !1,
                            isShow: !1,
                            need_fillinfo: !1,
                            is_show: !0,
                            times: 1,
                            close: 0,
                            prize_text: "",
                            prize_img: "",
                            sj_mb: 0,
                            hr: "",
                            min: "",
                            sec: "",
                            share_open: "",
                            activity: {
                                userinfo: []
                            },
                            day: "",
                            hour: "",
                            threeRecord: "",
                            prizes: {
                                1: [],
                                2: [],
                                3: [],
                                4: [],
                                5: [],
                                6: [],
                                7: [],
                                8: []
                            },
                            is_rolling: !1,
                            remain_times: "",
                            base: [],
                            rule: "",
                            id: "",
                            userinfo_name: "",
                            userinfo_mobile: "",
                            userinfo_address: "",
                            needAuth: !1,
                            needBind: !1
                        };
                    },
                    onShareAppMessage: function () {
                        var t = this,
                            i = n.getStorageSync("suid");
                        if ("1" == t.base.share_type) var e = "/pagesShake/index/index?id=" + t.id;
                        else e = "/pagesShake/index/index?id=" + t.id + "&suid=" + i;
                        return {
                            title: t.activity.title,
                            path: e,
                            success: function (e) {
                                t.activity.share_num >= parseInt(t.base.everyday_share) ? n.showModal({
                                    title: "注意",
                                    content: "分享获赠次数已达每日上限！",
                                    showCancel: !1
                                }) : t.activity.total_share_num >= parseInt(t.base.total_share) ? n.showModal({
                                    title: "注意",
                                    content: "分享获赠次数已达本次活动上限！",
                                    showCancel: !1
                                }) : (n.request({
                                    url: t.$baseurl + "dopageaddsharenum",
                                    data: {
                                        uniacid: t.$uniacid,
                                        id: t.id
                                    },
                                    success: function (e) {
                                        var i = t.activity;
                                        i.share += 1, t.activity = i;
                                    }
                                }), "1" == t.base.share_type ? n.request({
                                    url: t.$baseurl + "dopageshareSuccess",
                                    data: {
                                        uniacid: t.$uniacid,
                                        id: t.id,
                                        suid: i
                                    },
                                    success: function (e) {
                                        if (e.data.data) {
                                            var i = t.activity;
                                            i.share_num += 1, i.total_share_num += 1, t.sactivity = i, n.showModal({
                                                title: "分享成功！",
                                                content: "恭喜您获得" + t.base.share_add + "次抽奖机会！",
                                                showCancel: !1,
                                                success: function () {
                                                    t.sclose = 0, t.ssj_mb = 0;
                                                }
                                            });
                                        }
                                    }
                                }) : n.showModal({
                                    title: "分享成功！",
                                    content: "分享被点进您可获得" + t.base.share_add + "次抽奖机会！",
                                    showCancel: !1,
                                    success: function () {
                                        t.close = 0, t.sj_mb = 0;
                                    }
                                }));
                            }
                        };
                    },
                    onPullDownRefresh: function () {
                        this.times = 1, this.getConfig(this.id), n.stopPullDownRefresh();
                    },
                    onShow: function () {
                        var i = this;
                        0 == i.is_button && (i.isShow = !0, n.onAccelerometerChange(function (e) {
                            i.isShow && 1 < e.x && 1 < e.y && (n.getStorageSync("suid") ? i.startrolling() : n.showModal({
                                title: "抽奖失败",
                                content: "请先登录/注册",
                                showCancel: !1,
                                success: function (e) {
                                    n.navigateTo({
                                        url: "/pages/usercenter/usercenter"
                                    });
                                }
                            }));
                        }));
                    },
                    onHide: function () {
                        this.isShow = !1;
                    },
                    onLoad: function (e) {
                        var i = this,
                            t = this;
                        e.id && (t.id = e.id), e.times && (t.times = e.times);
                        var a = 0;
                        e.fxsid && (a = e.fxsid, n.setStorageSync("fxsid", a), t.fxsid = a), e.suid && n.request({
                                url: t.$baseurl + "dopageshareSuccess",
                                data: {
                                    uniacid: t.$uniacid,
                                    id: t.id,
                                    suid: e.suid
                                },
                                success: function (e) {}
                            }), t.showThreeLucky(e.id), t.getPrizes(e.id), t.getConfig(e.id), this._baseMin(this),
                            n.getStorageSync("suid"), s.getOpenid(a, function () {}, function () {
                                i.needAuth = !0;
                            }, function () {
                                i.needBind = !0;
                            });
                    },
                    methods: {
                        getConfig: function (e) {
                            var t = this,
                                i = n.getStorageSync("suid");
                            n.request({
                                url: t.$baseurl + "dopagegetConfig",
                                data: {
                                    uniacid: t.$uniacid,
                                    suid: i,
                                    id: e,
                                    source: n.getStorageSync("source")
                                },
                                success: function (e) {
                                    0 == e.data.data.flag && t.showModal("进入失败", "活动未开启"), 1 == e.data.data.flag && t.showModal("进入失败", "活动尚未开始！"),
                                        2 == e.data.data.flag && t.showModal("进入失败", "活动已结束！"), n.setNavigationBarColor({
                                            frontColor: "#ffffff",
                                            backgroundColor: e.data.data.nav_color ? e.data.data.nav_color : "#FEA049"
                                        }), t.is_button = "1" == e.data.data.base.means, t.remain_times = parseInt(e.data.data.base.every_join) + e.data.data.share_num * parseInt(e.data.data.base.share_add) - e.data.data.record_num + e.data.data.prizescount,
                                        t.base = e.data.data.base, t.activity = e.data.data;
                                    var i = e.data.data.descp.replace(/\<img/gi, '<img style="max-width:100%;height:auto;display:block" ');
                                    i = (0, a.default)(i), t.rule = i, console.log(t.rule), 1 == t.times && (l(t, e.data.data.remaintime),
                                        t.userinfo_mobile = t.activity.userinfo.mobile, "0" == t.base.fill_time && (1 == t.activity.is_vip ? (t.userinfo_name = t.activity.userinfo.realname,
                                            t.userinfo_address = t.activity.userinfo.address, t.close = 8) : "0" == t.base.users_type && 1 == t.activity.fill_info && (t.userinfo_name = t.activity.userinfo.realname,
                                            t.userinfo_address = t.activity.userinfo.address, t.close = 8)));
                                }
                            });
                        },
                        showModal: function (e, i) {
                            n.showModal({
                                title: e,
                                content: i,
                                showCancel: !1,
                                success: function (e) {
                                    e.confirm && n.navigateBack({
                                        delta: 1
                                    });
                                }
                            });
                        },
                        getPrizes: function (e) {
                            var i = this;
                            n.request({
                                url: i.$baseurl + "dopagegetPrizes",
                                data: {
                                    uniacid: i.$uniacid,
                                    id: e
                                },
                                success: function (e) {
                                    i.prizes = e.data.data;
                                }
                            });
                        },
                        startrolling: function () {
                            var i = this,
                                e = n.getStorageSync("suid");
                            if (!this.getSuid()) return !1;
                            var t = !1;
                            if (i.close = 0, i.sj_mb = 1, i.activity.record_num >= parseInt(i.base.every_join) + i.activity.share_num * parseInt(i.base.share_add)) this.close = 2;
                            else if ("1" == i.base.just_one && 1 <= i.activity.win_num) n.showModal({
                                title: "很抱歉",
                                content: "您已中过奖",
                                showCancel: !1
                            });
                            else if (i.activity.user_jifen < parseInt(i.base.jifen)) this.close = 1;
                            else if (0 == i.activity.is_vip ? "1" == i.base.users_type ? n.showModal({
                                    title: "很抱歉",
                                    content: "参与活动需要开通会员",
                                    success: function (e) {
                                        e.confirm && n.navigateTo({
                                            url: "/pages/register/register?from=lottery"
                                        });
                                    }
                                }) : "0" == i.base.fill_time && 0 == i.activity.fill_info ? n.showModal({
                                    title: "提示",
                                    content: "抽奖需要您的信息",
                                    cancelText: "填信息",
                                    confirmText: "开会员",
                                    success: function (e) {
                                        e.confirm && (i.sj_mb = 0, n.navigateTo({
                                            url: "/pages/register/register?from=lottery"
                                        })), e.cancel && (i.close = 8);
                                    }
                                }) : t = !0 : t = !0, t) {
                                var a = i.activity;
                                a.user_jifen -= i.base.jifen, i.activity = a, i.isShow = !1, e = n.getStorageSync("suid"),
                                    n.request({
                                        url: i.$baseurl + "dopagedrawLottery",
                                        data: {
                                            uniacid: i.$uniacid,
                                            id: i.id,
                                            suid: e
                                        },
                                        success: function (e) {
                                            e.data.data.flag ? i.finalindex = e.data.data.index : i.finalindex = e.data.data.empty,
                                                i.flag = e.data.data.flag;
                                        }
                                    }), i.runs_now = 0, i.roll_flag && (i.roll_flag = !1, i.is_rolling = !0, i.last_index = 0,
                                        i.amplification_index = 0, i.max_number = 8, i.speed = 300, i.finalindex = 0, i.myInterval = "",
                                        i.max_speed = 40, i.minturns = 12, i.runs_now = 0, i.flag = !1, i.rolling());
                            }
                        },
                        rolling: function (e) {
                            var i = this;
                            i.myInterval = setTimeout(function () {
                                i.rolling();
                            }, i.speed), i.runs_now++, i.amplification_index++;
                            var t = i.minturns * i.max_number + i.finalindex - i.last_index;
                            i.runs_now <= t / 3 * 2 ? (i.speed -= 30, i.speed <= i.max_speed && (i.speed = i.max_speed)) : i.runs_now >= t ? (i.is_rolling = !1,
                                    i.times = 2, i.getConfig(i.id), clearInterval(i.myInterval), i.roll_flag = !0, i.flag && i.prizes[i.finalindex] ? setTimeout(function () {
                                        "1" == i.base.fill_time ? 0 == i.activity.is_vip && 0 == i.activity.fill_info ? (i.prizes[i.finalindex].detail,
                                                i.close = 9) : (i.prizes[i.finalindex].detail, i.close = 10) : (i.prizes[i.finalindex].detail,
                                                i.close = 6), i.prize_text = i.prizes[i.finalindex].detail, i.prize_img = i.prizes[i.finalindex].thumb,
                                            i.showThreeLucky(i.id);
                                    }, 800) : setTimeout(function () {
                                        i.close = 5;
                                    }, 800), i.is_button || (i.isShow = !0)) : t - i.runs_now <= 10 ? i.speed += 20 : (i.speed += 10,
                                    100 <= i.speed && (i.speed = 100)), i.amplification_index > i.max_number && (i.amplification_index = 1),
                                i.data = i.data;
                        },
                        addus1: function () {
                            this.close = 8;
                        },
                        addus2: function () {
                            this.sj_mb = 0, n.redirectTo({
                                url: "/pages/register/register?from=lottery"
                            });
                        },
                        addus3: function () {
                            var e = this;
                            e.userinfo_name = e.activity.userinfo.realname, e.userinfo_mobile = e.activity.userinfo.mobile,
                                e.userinfo_address = e.activity.userinfo.address, e.close = 8;
                        },
                        changeName: function (e) {
                            this.userinfo_name = e.detail.value;
                        },
                        changeMobile: function (e) {
                            this.userinfo_mobile = e.detail.value;
                        },
                        changeAddress: function (e) {
                            this.userinfo_address = e.detail.value;
                        },
                        changeUserinfo: function () {
                            var i = this,
                                e = n.getStorageSync("suid");
                            if (!this.getSuid()) return !1;
                            i.userinfo_name ? i.userinfo_mobile ? i.userinfo_address ? n.request({
                                url: i.$baseurl + "dopagechangeUserinfo",
                                data: {
                                    uniacid: i.$uniacid,
                                    suid: e,
                                    name: i.userinfo_name,
                                    mobile: i.userinfo_mobile,
                                    address: i.userinfo_address
                                },
                                success: function (e) {
                                    1 == i.activity.fill_info ? 0 != i.finalindex ? (i.prizes[i.finalindex].detail,
                                        i.close = 6, i.prize_text = i.prizes[i.finalindex].detail, i.prize_img = i.prizes[i.finalindex].thumb) : i.close = 0 : n.showToast({
                                        title: "提交成功！",
                                        icon: "success",
                                        success: function () {
                                            setTimeout(function () {
                                                i.activity.fill_info = !0, 0 != i.finalindex ? (i.prizes[i.finalindex].detail, i.close = 6,
                                                    i.prize_text = i.prizes[i.finalindex].detail, i.prize_img = i.prizes[i.finalindex].thumb) : i.close = 0;
                                            }, 1500);
                                        }
                                    });
                                }
                            }) : n.showModal({
                                title: "地址不能为空",
                                content: "请重新填写",
                                showCancel: !1
                            }) : n.showModal({
                                title: "手机号不能为空",
                                content: "请重新填写",
                                showCancel: !1
                            }) : n.showModal({
                                title: "姓名不能为空",
                                content: "请重新填写",
                                showCancel: !1
                            });
                        },
                        checkUserinfo: function () {
                            var e = this;
                            0 != e.finalindex ? (e.prizes[e.finalindex].detail, e.close = 6, e.prize_text = e.prizes[e.finalindex].detail,
                                e.prize_img = e.prizes[e.finalindex].thumb) : e.close = 0;
                        },
                        toRegisterSuccess: function () {
                            n.navigateTo({
                                url: "/pagesShake/register_success/register_success?from=lottery"
                            });
                        },
                        setUserinfo: function (e, i, t) {
                            this.userinfo_name = e, this.userinfo_mobile = i, this.userinfo_address = t;
                        },
                        setTrueIsvip: function () {
                            var e = this.activity;
                            e.is_vip = !0, this.activity = e;
                        },
                        closeBox: function () {
                            this.close = 0, this.sj_mb = 0;
                        },
                        share: function () {
                            this.close = 3;
                        },
                        look_regular: function () {
                            this.close = 7;
                        },
                        chooseAdress: function () {
                            var i = this;
                            n.chooseAddress({
                                success: function (e) {
                                    console.log(e), i.userinfo_name = e.userName, i.userinfo_address = e.provinceName + e.cityName + e.countyName + e.detailInfo;
                                },
                                fail: function () {
                                    n.showModal({
                                        title: "提示",
                                        content: "一键获取需要您的授权",
                                        showCancel: !1
                                    });
                                }
                            });
                        },
                        showThreeLucky: function (e) {
                            var i = this;
                            n.request({
                                url: i.$baseurl + "dopageshowThreeLucky",
                                data: {
                                    uniacid: i.$uniacid,
                                    id: e,
                                    source: n.getStorageSync("source")
                                },
                                success: function (e) {
                                    i.threeRecord = e.data.data;
                                }
                            });
                        },
                        toPrizelist: function () {
                            n.navigateTo({
                                url: "/pagesShake/prize/prize?id=" + this.id + "&nav_color=" + this.activity.nav_color
                            });
                        },
                        toRecordlist: function () {
                            if (n.getStorageSync("suid"), !this.getSuid()) return !1;
                            n.navigateTo({
                                url: "/pagesShake/win_prize/win_prize?id=" + this.id + "&nav_color=" + this.activity.nav_color
                            });
                        },
                        toGetscore: function () {
                            n.navigateTo({
                                url: "/pagesShake/integral_collect/integral_collect"
                            });
                        },
                        h5ShareAppMessage: function () {
                            var e = n.getStorageSync("suid");
                            n.showModal({
                                title: "长按复制链接后分享",
                                content: this.$host + "/h5/index.html?id=" + this.$uniacid + "#/pagesShake/index/index?id=" + this.id + "&fxsid=" + e + "&userid=" + e,
                                showCancel: !1
                            });
                        },
                        cell: function () {
                            this.needAuth = !1;
                        },
                        closeAuth: function () {
                            this.needAuth = !1, this.needBind = !0;
                        },
                        closeBind: function () {
                            this.needBind = !1;
                        },
                        getSuid: function () {
                            if (n.getStorageSync("suid")) return !0;
                            return n.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                !1;
                        }
                    }
                };
                t.default = i;
            }).call(this, o("543d").default);
        }
    },
    [
        ["0d8a", "common/runtime", "common/vendor"]
    ]
]);