(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesShake/prize/prize"], {
        "0084": function (n, t, e) {
            e.r(t);
            var i = e("643d"),
                a = e.n(i);
            for (var o in i) "default" !== o && function (n) {
                e.d(t, n, function () {
                    return i[n];
                });
            }(o);
            t.default = a.a;
        },
        "0710": function (n, t, e) {
            var i = e("b057");
            e.n(i).a;
        },
        5665: function (n, t, e) {
            var i = function () {
                    this.$createElement;
                    this._self._c;
                },
                a = [];
            e.d(t, "a", function () {
                return i;
            }), e.d(t, "b", function () {
                return a;
            });
        },
        "643d": function (n, t, i) {
            (function (e) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0, i("f571");
                var n = {
                    data: function () {
                        return {
                            $imgurl: this.$imgurl,
                            prizes: []
                        };
                    },
                    onPullDownRefresh: function () {
                        e.stopPullDownRefresh();
                    },
                    onLoad: function (n) {
                        n.id && (this.id = n.id), e.setNavigationBarTitle({
                            title: "奖品列表"
                        }), e.setNavigationBarColor({
                            frontColor: "#ffffff",
                            backgroundColor: n.nav_color ? n.nav_color : "#FEA049"
                        }), this.getPriceList();
                    },
                    methods: {
                        getPriceList: function () {
                            var t = this;
                            e.request({
                                url: t.$baseurl + "doPagegetPrizeList",
                                data: {
                                    uniacid: t.$uniacid,
                                    id: t.id
                                },
                                success: function (n) {
                                    t.prizes = n.data.data;
                                }
                            });
                        }
                    }
                };
                t.default = n;
            }).call(this, i("543d").default);
        },
        b057: function (n, t, e) {},
        b0a2: function (n, t, e) {
            (function (n) {
                function t(n) {
                    return n && n.__esModule ? n : {
                        default: n
                    };
                }
                e("020c"), e("921b"), t(e("66fd")), n(t(e("e83f")).default);
            }).call(this, e("543d").createPage);
        },
        e83f: function (n, t, e) {
            e.r(t);
            var i = e("5665"),
                a = e("0084");
            for (var o in a) "default" !== o && function (n) {
                e.d(t, n, function () {
                    return a[n];
                });
            }(o);
            e("0710");
            var u = e("2877"),
                r = Object(u.a)(a.default, i.a, i.b, !1, null, null, null);
            t.default = r.exports;
        }
    },
    [
        ["b0a2", "common/runtime", "common/vendor"]
    ]
]);