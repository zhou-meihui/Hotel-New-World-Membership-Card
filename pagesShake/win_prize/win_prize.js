(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesShake/win_prize/win_prize"], {
        3776: function (n, t, e) {
            var o = e("4c9b");
            e.n(o).a;
        },
        "4c9b": function (n, t, e) {},
        "51b7": function (n, t, e) {
            e.r(t);
            var o = e("cbf1"),
                a = e.n(o);
            for (var r in o) "default" !== r && function (n) {
                e.d(t, n, function () {
                    return o[n];
                });
            }(r);
            t.default = a.a;
        },
        7868: function (n, t, e) {
            e.r(t);
            var o = e("a603"),
                a = e("51b7");
            for (var r in a) "default" !== r && function (n) {
                e.d(t, n, function () {
                    return a[n];
                });
            }(r);
            e("3776");
            var u = e("2877"),
                i = Object(u.a)(a.default, o.a, o.b, !1, null, null, null);
            t.default = i.exports;
        },
        8772: function (n, t, e) {
            (function (n) {
                function t(n) {
                    return n && n.__esModule ? n : {
                        default: n
                    };
                }
                e("020c"), e("921b"), t(e("66fd")), n(t(e("7868")).default);
            }).call(this, e("543d").createPage);
        },
        a603: function (n, t, e) {
            var o = function () {
                    this.$createElement;
                    this._self._c;
                },
                a = [];
            e.d(t, "a", function () {
                return o;
            }), e.d(t, "b", function () {
                return a;
            });
        },
        cbf1: function (n, t, e) {
            (function (e) {
                Object.defineProperty(t, "__esModule", {
                    value: !0
                }), t.default = void 0;
                var n = {
                    data: function () {
                        return {
                            records: [],
                            userinfo: []
                        };
                    },
                    onPullDownRefresh: function () {
                        e.stopPullDownRefresh();
                    },
                    onLoad: function (n) {
                        n.id && (this.id = n.id), e.setNavigationBarTitle({
                            title: "中奖列表"
                        }), e.setNavigationBarColor({
                            frontColor: "#ffffff",
                            backgroundColor: n.nav_color ? n.nav_color : "#FEA049"
                        }), this.getRecordList();
                    },
                    methods: {
                        getRecordList: function () {
                            var t = this;
                            e.request({
                                url: t.$baseurl + "doPagegetRecordList",
                                data: {
                                    uniacid: t.$uniacid,
                                    suid: e.getStorageSync("suid"),
                                    id: t.id,
                                    source: e.getStorageSync("source")
                                },
                                success: function (n) {
                                    t.records = n.data.data.records, t.userinfo = n.data.data.userinfo;
                                }
                            });
                        }
                    }
                };
                t.default = n;
            }).call(this, e("543d").default);
        }
    },
    [
        ["8772", "common/runtime", "common/vendor"]
    ]
]);