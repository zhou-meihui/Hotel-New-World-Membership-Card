(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesSign/index/index"], {
        2058: function (t, e, i) {
            var n = function () {
                    this.$createElement;
                    this._self._c;
                },
                a = [];
            i.d(e, "a", function () {
                return n;
            }), i.d(e, "b", function () {
                return a;
            });
        },
        "3d27": function (t, e, i) {},
        4633: function (t, e, i) {
            i.r(e);
            var n = i("2058"),
                a = i("9d9c");
            for (var s in a) "default" !== s && function (t) {
                i.d(e, t, function () {
                    return a[t];
                });
            }(s);
            i("c6f5");
            var r = i("2877"),
                c = Object(r.a)(a.default, n.a, n.b, !1, null, null, null);
            e.default = c.exports;
        },
        "50c2": function (t, e, i) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                i("020c"), i("921b"), e(i("66fd")), t(e(i("4633")).default);
            }).call(this, i("543d").createPage);
        },
        "9d9c": function (t, e, i) {
            i.r(e);
            var n = i("fce3"),
                a = i.n(n);
            for (var s in n) "default" !== s && function (t) {
                i.d(e, t, function () {
                    return n[t];
                });
            }(s);
            e.default = a.a;
        },
        c6f5: function (t, e, i) {
            var n = i("3d27");
            i.n(n).a;
        },
        fce3: function (t, e, i) {
            (function (c) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0;
                var u = i("f571"),
                    t = {
                        data: function () {
                            return {
                                bg: "",
                                userinfo: "",
                                hasEmptyGrid: !1,
                                showPicker: !1,
                                jilu: "",
                                page_signs: "/pagesSign/index/index",
                                tongj: {
                                    score: {}
                                },
                                choose_year: null,
                                choose_month: null,
                                weeks_ch: [],
                                cur_year: "",
                                cur_month: "",
                                empytGrids: [],
                                days: {},
                                picker_value: [],
                                picker_year: [],
                                picker_month: [],
                                needBind: !1,
                                needAuth: !1,
                                baseinfo: {},
                                $imgurl: this.$imgurl,
                                signsuccess: 0,
                                is_today: 0
                            };
                        },
                        onShareAppMessage: function () {
                            return {
                                title: "积分签到"
                            };
                        },
                        onPullDownRefresh: function () {
                            this.getsign(), c.stopPullDownRefresh();
                        },
                        onLoad: function (t) {
                            var e = this,
                                i = this;
                            c.setNavigationBarTitle({
                                title: "积分签到"
                            });
                            var n = 0;
                            t.fxsid && (n = t.fxsid), this.fxsid = n;
                            var a = new Date(),
                                s = a.getFullYear(),
                                r = a.getMonth() + 1;
                            this.calculateEmptyGrids(s, r), this.calculateDays(s, r), this.cur_year = s, this.cur_month = r,
                                this.weeks_ch = ["日", "一", "二", "三", "四", "五", "六"], this._baseMin(this), c.getStorageSync("suid"),
                                u.getOpenid(n, function () {
                                    i.getsign();
                                }, function () {
                                    e.needAuth = !0;
                                }, function () {
                                    e.needBind = !0;
                                });
                        },
                        methods: {
                            pulls: function (t) {
                                c.redirectTo({
                                    url: "/pagesSign/index/index"
                                });
                            },
                            redirectto: function (t) {
                                var e = t.currentTarget.dataset.link,
                                    i = t.currentTarget.dataset.linktype;
                                this._redirectto(e, i);
                            },
                            checkvip: function () {
                                var e = this,
                                    t = c.getStorageSync("openid");
                                c.request({
                                    url: e.$baseurl + "doPagecheckvip",
                                    data: {
                                        uniacid: e.$uniacid,
                                        kwd: "sign",
                                        openid: t
                                    },
                                    success: function (t) {
                                        t.data.data || (e.needvip = !0, c.showModal({
                                            title: "进入失败",
                                            content: "使用本功能需先开通vip!",
                                            showCancel: !1,
                                            success: function (t) {
                                                t.confirm && c.redirectTo({
                                                    url: "/pages/register/register"
                                                });
                                            }
                                        }));
                                    },
                                    fail: function (t) {}
                                });
                            },
                            getThisMonthDays: function (t, e) {
                                return new Date(t, e, 0).getDate();
                            },
                            getFirstDayOfWeek: function (t, e) {
                                return new Date(Date.UTC(t, e - 1, 1)).getDay();
                            },
                            calculateEmptyGrids: function (t, e) {
                                var i = this.getFirstDayOfWeek(t, e),
                                    n = [];
                                if (0 < i) {
                                    for (var a = 0; a < i; a++) n.push(a);
                                    this.hasEmptyGrid = !0, this.empytGrids = n;
                                } else this.hasEmptyGrid = !1, this.empytGrids = [];
                            },
                            calculateDays: function (t, e) {
                                var i = (this.getThisMonthDays(t, e), c.getStorageSync("openid"));
                                c.request({
                                    url: this.$baseurl + "doPageMysign",
                                    data: {
                                        uniacid: this.$uniacid,
                                        openid: i,
                                        year: t,
                                        month: e
                                    },
                                    success: function (t) {
                                        t.data.data;
                                    }
                                });
                            },
                            handleCalendar: function (t) {
                                var e = t.currentTarget.dataset.handle,
                                    i = this.cur_year,
                                    n = this.cur_month;
                                if ("prev" === e) {
                                    var a = n - 1,
                                        s = i;
                                    a < 1 && (s = i - 1, a = 12), this.calculateDays(s, a), this.calculateEmptyGrids(s, a),
                                        this.cur_year = s, this.cur_month = a;
                                } else {
                                    var r = n + 1,
                                        c = i;
                                    12 < r && (c = i + 1, r = 1), this.calculateDays(c, r), this.calculateEmptyGrids(c, r),
                                        this.cur_year = c, this.cur_month = r;
                                }
                            },
                            tapDayItem: function (t) {
                                var e = t.currentTarget.dataset.idx,
                                    i = this.days;
                                i[e].choosed = !i[e].choosed, that.days = i;
                            },
                            chooseYearAndMonth: function () {
                                for (var t = this.cur_year, e = this.cur_month, i = [], n = [], a = 1900; a <= 2100; a++) i.push(a);
                                for (var s = 1; s <= 12; s++) n.push(s);
                                var r = i.indexOf(t),
                                    c = n.indexOf(e);
                                this.picker_value = [r, c], this.picker_year = i, this.picker_month = n, this.showPicker = !0;
                            },
                            pickerChange: function (t) {
                                var e = t.detail.value;
                                choose_year = this.picker_year[e[0]], choose_month = this.picker_month[e[1]];
                            },
                            tapPickerBtn: function (t) {
                                var e = {
                                    showPicker: !1
                                };
                                "confirm" === t.currentTarget.dataset.type && (e.cur_year = choose_year, e.cur_month = choose_month,
                                        this.calculateEmptyGrids(choose_year, choose_month), this.calculateDays(choose_year, choose_month)),
                                    this.o = e;
                            },
                            getsign: function () {
                                var i = this,
                                    t = c.getStorageSync("suid");
                                c.request({
                                    url: i.$baseurl + "doPageMysign",
                                    data: {
                                        uniacid: i.$uniacid,
                                        suid: t
                                    },
                                    success: function (t) {
                                        var e = t.data.data;
                                        i.days = e;
                                    }
                                }), c.request({
                                    url: i.$baseurl + "doPageMysignjl",
                                    data: {
                                        uniacid: i.$uniacid,
                                        suid: t,
                                        source: c.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        var e = t.data.data;
                                        i.userinfo = e.userinfo, i.jilu = e.alls;
                                    }
                                }), c.request({
                                    url: i.$baseurl + "doPagemysigntj",
                                    data: {
                                        uniacid: i.$uniacid,
                                        suid: t,
                                        source: c.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        var e = t.data.data;
                                        i.tongj = e, i.is_today = e.is_today;
                                    }
                                });
                            },
                            qiandao: function () {
                                var e = this,
                                    t = c.getStorageSync("suid");
                                if (!this.getSuid()) return !1;
                                c.request({
                                    url: e.$baseurl + "doPageQiandao",
                                    data: {
                                        uniacid: e.$uniacid,
                                        suid: t,
                                        source: c.getStorageSync("source")
                                    },
                                    success: function (t) {
                                        1 == t.data.data ? c.showModal({
                                            title: "提醒",
                                            content: "您今天已经签过到了！",
                                            showCancel: !1
                                        }) : (e.signsuccess = 1, e.is_today = 1);
                                    }
                                });
                            },
                            cell: function () {
                                this.needAuth = !1;
                            },
                            closeAuth: function () {
                                this.needAuth = !1, this.needBind = !0;
                            },
                            closeBind: function () {
                                this.needBind = !1;
                            },
                            getSuid: function () {
                                if (c.getStorageSync("suid")) return !0;
                                return c.getStorageSync("golobeuser") ? this.needBind = !0 : this.needAuth = !0,
                                    !1;
                            }
                        }
                    };
                e.default = t;
            }).call(this, i("543d").default);
        }
    },
    [
        ["50c2", "common/runtime", "common/vendor"]
    ]
]);