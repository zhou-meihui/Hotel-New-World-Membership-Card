(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesSign/new/new"], {
        "0ab3": function (t, e, n) {
            n.r(e);
            var a = n("2496"),
                u = n.n(a);
            for (var i in a) "default" !== i && function (t) {
                n.d(e, t, function () {
                    return a[t];
                });
            }(i);
            e.default = u.a;
        },
        2496: function (t, e, a) {
            (function (n) {
                Object.defineProperty(e, "__esModule", {
                    value: !0
                }), e.default = void 0, a("f571");
                var t = {
                    data: function () {
                        return {
                            page: 1,
                            collectlist: "",
                            baseinfo: [],
                            arr: ""
                        };
                    },
                    onShareAppMessage: function () {
                        return {
                            title: "最新签到"
                        };
                    },
                    onPullDownRefresh: function () {
                        this.getCollect(), wx.stopPullDownRefresh();
                    },
                    onLoad: function (t) {
                        wx.setNavigationBarTitle({
                            title: "最新签到"
                        });
                        var e = 0;
                        t.fxsid && (e = t.fxsid), this.fxsid = e, this._baseMin(this), this.getCollect();
                    },
                    methods: {
                        redirectto: function (t) {
                            var e = t.currentTarget.dataset.link,
                                n = t.currentTarget.dataset.linktype;
                            this._redirectto(e, n);
                        },
                        getCollect: function () {
                            var e = this,
                                t = wx.getStorageSync("suid");
                            wx.request({
                                url: e.$baseurl + "dopageZxqd",
                                data: {
                                    uniacid: e.$uniacid,
                                    suid: t,
                                    source: n.getStorageSync("source")
                                },
                                success: function (t) {
                                    e.arr = t.data.data;
                                }
                            });
                        }
                    }
                };
                e.default = t;
            }).call(this, a("543d").default);
        },
        5197: function (t, e, n) {
            (function (t) {
                function e(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                n("020c"), n("921b"), e(n("66fd")), t(e(n("cf06")).default);
            }).call(this, n("543d").createPage);
        },
        af5e: function (t, e, n) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                u = [];
            n.d(e, "a", function () {
                return a;
            }), n.d(e, "b", function () {
                return u;
            });
        },
        be57: function (t, e, n) {},
        cf06: function (t, e, n) {
            n.r(e);
            var a = n("af5e"),
                u = n("0ab3");
            for (var i in u) "default" !== i && function (t) {
                n.d(e, t, function () {
                    return u[t];
                });
            }(i);
            n("d980");
            var r = n("2877"),
                o = Object(r.a)(u.default, a.a, a.b, !1, null, null, null);
            e.default = o.exports;
        },
        d980: function (t, e, n) {
            var a = n("be57");
            n.n(a).a;
        }
    },
    [
        ["5197", "common/runtime", "common/vendor"]
    ]
]);