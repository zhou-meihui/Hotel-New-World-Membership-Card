(global.webpackJsonp = global.webpackJsonp || []).push([
    ["pagesSign/ranking/ranking"], {
        "019a": function (t, n, e) {
            (function (t) {
                function n(t) {
                    return t && t.__esModule ? t : {
                        default: t
                    };
                }
                e("020c"), e("921b"), n(e("66fd")), t(n(e("029e")).default);
            }).call(this, e("543d").createPage);
        },
        "029e": function (t, n, e) {
            e.r(n);
            var a = e("331f"),
                i = e("06a2");
            for (var u in i) "default" !== u && function (t) {
                e.d(n, t, function () {
                    return i[t];
                });
            }(u);
            e("02b4");
            var r = e("2877"),
                o = Object(r.a)(i.default, a.a, a.b, !1, null, null, null);
            n.default = o.exports;
        },
        "02b4": function (t, n, e) {
            var a = e("d4f9");
            e.n(a).a;
        },
        "06a2": function (t, n, e) {
            e.r(n);
            var a = e("3aa9"),
                i = e.n(a);
            for (var u in a) "default" !== u && function (t) {
                e.d(n, t, function () {
                    return a[t];
                });
            }(u);
            n.default = i.a;
        },
        "331f": function (t, n, e) {
            var a = function () {
                    this.$createElement;
                    this._self._c;
                },
                i = [];
            e.d(n, "a", function () {
                return a;
            }), e.d(n, "b", function () {
                return i;
            });
        },
        "3aa9": function (t, n, e) {
            (function (a) {
                Object.defineProperty(n, "__esModule", {
                    value: !0
                }), n.default = void 0, e("f571");
                var t = {
                    data: function () {
                        return {
                            $imgurl: this.$imgurl,
                            baseinfo: "",
                            minHeight: 220,
                            bg: "",
                            userinfo: "",
                            hasEmptyGrid: !1,
                            showPicker: !1,
                            paixu: []
                        };
                    },
                    onShareAppMessage: function () {
                        return {
                            title: "签到排行榜"
                        };
                    },
                    onPullDownRefresh: function () {
                        this.getsign(), a.stopPullDownRefresh();
                    },
                    onLoad: function (t) {
                        a.setNavigationBarTitle({
                            title: "签到排行榜"
                        });
                        var n = 0;
                        t.fxsid && (n = t.fxsid), this.fxsid = n, this._baseMin(this), this.getsign();
                    },
                    methods: {
                        redirectto: function (t) {
                            var n = t.currentTarget.dataset.link,
                                e = t.currentTarget.dataset.linktype;
                            this._redirectto(n, e);
                        },
                        getsign: function () {
                            var e = this,
                                t = a.getStorageSync("suid");
                            a.request({
                                url: e.$baseurl + "dopagePaihb",
                                data: {
                                    uniacid: e.$uniacid,
                                    suid: t
                                },
                                success: function (t) {
                                    var n = t.data.data;
                                    e.paixu = n;
                                }
                            });
                        }
                    }
                };
                n.default = t;
            }).call(this, e("543d").default);
        },
        d4f9: function (t, n, e) {}
    },
    [
        ["019a", "common/runtime", "common/vendor"]
    ]
]);